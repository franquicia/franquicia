package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.PeriodoDAO;
import com.gruposalinas.franquicia.domain.PeriodoDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PeriodoBI {

    private static Logger logger = LogManager.getLogger(PeriodoBI.class);
    @Autowired
    PeriodoDAO periodoDAO;

    List<PeriodoDTO> listaGrupo = null;

    public List<PeriodoDTO> obtienePeriodo(String idPeriodo) {

        try {
            listaGrupo = periodoDAO.obtienePeriodo(idPeriodo);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Evidencia");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaGrupo;
    }

    public int insertaPeriodo(PeriodoDTO bean) {

        int idGrupo = 0;

        try {
            idGrupo = periodoDAO.insertaPeriodo(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Grupo");
            UtilFRQ.printErrorLog(null, e);
        }

        return idGrupo;
    }

    public boolean actualizaPeriodo(PeriodoDTO bean) throws Exception {

        boolean respuesta = false;

        try {
            respuesta = periodoDAO.actualizaPeriodo(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Grupo");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaPeriodo(int idPeriodo) {

        boolean respuesta = false;

        try {
            respuesta = periodoDAO.eliminaPeriodo(idPeriodo);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Grupo");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

}
