package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.ArchiveroDAO;
import com.gruposalinas.franquicia.domain.ArchiveroDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ArchiveroBI {

    private static Logger logger = LogManager.getLogger(ArchiveroBI.class);

    @Autowired
    ArchiveroDAO archiveroDAO;

    List<ArchiveroDTO> listArchivero = null;

    public List<ArchiveroDTO> obtieneArchivero(int idArchivero) {
        try {
            listArchivero = archiveroDAO.obtieneArchivero(idArchivero);
        } catch (Exception e) {
            logger.info("No fue posible obtener el archivero");
            UtilFRQ.printErrorLog(null, e);
        }
        return listArchivero;
    }

    public boolean insertaArchivero(ArchiveroDTO bean) {
        boolean flag = false;
        try {
            flag = archiveroDAO.insertaArchivero(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar el archivero");
            UtilFRQ.printErrorLog(null, e);
            flag = false;
        }
        return flag;
    }

    public boolean modificaArchivero(ArchiveroDTO bean) {
        boolean flag = false;
        try {
            flag = archiveroDAO.modificaArchivero(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el archivero");
            UtilFRQ.printErrorLog(null, e);
            flag = false;
        }
        return flag;
    }

    public boolean eliminaArchivero(int idArchivero, int idForm) {
        boolean flag = false;
        try {
            flag = archiveroDAO.eliminaArchivero(idArchivero, idForm);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el archivero");
            UtilFRQ.printErrorLog(null, e);
            flag = false;
        }
        return flag;
    }

    public List<ArchiveroDTO> obtieneArchiveroFormato(int idFormArchiveros) {
        try {
            listArchivero = archiveroDAO.obtieneArchiveroFormato(idFormArchiveros);
        } catch (Exception e) {
            logger.info("No fue posible obtener los archiveros por id formato");
            UtilFRQ.printErrorLog(null, e);
            logger.info("Salida: " + e.getMessage());
        }
        return listArchivero;
    }
}
