package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.CecoDAO;
import com.gruposalinas.franquicia.domain.CecoDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CecoBI {

    private static Logger logger = LogManager.getLogger(CecoBI.class);

    @Autowired
    CecoDAO cecoDAO;

    public boolean insertaCeco(CecoDTO bean) {

        boolean resultado = false;

        try {
            resultado = cecoDAO.insertaCeco(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar el CECO ");
            UtilFRQ.printErrorLog(null, e);
        }

        return resultado;
    }

    public boolean actualizaCeco(CecoDTO bean) {

        boolean resultado = false;

        try {
            resultado = cecoDAO.actualizaCeco(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el CECO ");
            UtilFRQ.printErrorLog(null, e);
        }

        return resultado;
    }

    public boolean eliminaCeco(int ceco) {

        boolean resultado = false;

        try {
            resultado = cecoDAO.eliminaCeco(ceco);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el CECO ");
            UtilFRQ.printErrorLog(null, e);
        }

        return resultado;
    }

    public List<CecoDTO> buscaCeco(int ceco) {

        List<CecoDTO> listaCeco = null;

        try {
            listaCeco = cecoDAO.buscaCeco(ceco);
        } catch (Exception e) {
            logger.info("No fue posible consultar el CECO : " + ceco);
            UtilFRQ.printErrorLog(null, e);
        }

        return listaCeco;
    }

    public List<CecoDTO> buscaCecos(int activo) {
        List<CecoDTO> listaCecos = null;

        try {
            listaCecos = cecoDAO.buscaCecos(activo);
        } catch (Exception e) {
            logger.info("No fue posible consultar los ceco");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaCecos;
    }

    public boolean cargaCecos() {
        boolean resultado = false;

        try {
            resultado = cecoDAO.cargaCecos();
        } catch (Exception e) {
            logger.info("No fue posible cargar los CECOS");
            UtilFRQ.printErrorLog(null, e);
        }

        return resultado;
    }

    public boolean cargaGeografia() {
        boolean resultado = false;

        try {
            resultado = cecoDAO.cargaGeografia();
        } catch (Exception e) {
            logger.info("No fue posible cargar la Geografia");
            UtilFRQ.printErrorLog(null, e);
        }

        return resultado;
    }

    public boolean cargaCecos2() {
        boolean resultado = false;

        try {
            resultado = cecoDAO.cargaCecos2();
        } catch (Exception e) {
            logger.info("No fue posible cargar los CECOS 2");
            UtilFRQ.printErrorLog(null, e);
        }

        return resultado;
    }

    public boolean updateCecos() {
        boolean resultado = false;

        try {
            resultado = cecoDAO.updateCecos();
        } catch (Exception e) {
            logger.info("No fue posible actualizar los CECOS");
            UtilFRQ.printErrorLog(null, e);
        }

        return resultado;
    }

    public List<CecoDTO> buscaCecoPaso(String ceco, String cecoPadre, String descripcion) {

        if (ceco.equals("NULL")) {
            ceco = null;
        }
        if (cecoPadre.equals("NULL")) {
            cecoPadre = null;
        }
        if (descripcion.equals("NULL")) {
            descripcion = null;
        }
        List<CecoDTO> listaCeco = null;

        try {
            listaCeco = cecoDAO.buscaCecoPaso(ceco, cecoPadre, descripcion);
        } catch (Exception e) {
            logger.info("No fue posible consultar el CECO : " + ceco);
            UtilFRQ.printErrorLog(null, e);
        }

        return listaCeco;
    }

    public List<CecoDTO> buscaCecosPaso() {
        List<CecoDTO> listaCecos = null;

        try {
            listaCecos = cecoDAO.buscaCecosPaso();
        } catch (Exception e) {
            logger.info("No fue posible consultar los ceco");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaCecos;
    }

    public List<CecoDTO> buscaTerritorios(int idPais) {

        List<CecoDTO> listaTerritorios = null;

        try {
            listaTerritorios = cecoDAO.buscaTerritorios(idPais);
        } catch (Exception e) {
            logger.info("No fue posible consultar los territorios ");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaTerritorios;
    }

    public List<CecoDTO> buscaCecosPasoP(int idCecoPadre) {

        List<CecoDTO> listaCecosP = null;

        try {
            listaCecosP = cecoDAO.buscaCecosPasoP(idCecoPadre);
        } catch (Exception e) {
            logger.info("No fue posible consultar los cecos por geografia ");
            UtilFRQ.printErrorLog(null, e);
        }
        return listaCecosP;
    }

    public int eliminaCecosTrabajo() {

        int afectados = 0;

        try {
            afectados = cecoDAO.eliminaCecosTrabajo();
        } catch (Exception e) {
            logger.info("No fue posible eliminar cecos de trabajo  ");
            UtilFRQ.printErrorLog(null, e);
        }

        return afectados;

    }

    public int[] cargaCecosGuatemala() {

        int[] respuestaSF = new int[2];
        int[] respuestaCom = new int[2];
        int[] respuestaGuatemala = new int[4];

        try {

            respuestaSF = cecoDAO.cargaCecosSFGuatemala();
            respuestaCom = cecoDAO.cargaCecosCOMGuatemala();

            respuestaGuatemala[0] = respuestaSF[0];
            respuestaGuatemala[1] = respuestaSF[1];
            respuestaGuatemala[2] = respuestaCom[0];
            respuestaGuatemala[3] = respuestaCom[1];

        } catch (Exception e) {
            logger.info("No fue posible cargar Sector Financiero y/o Comercio Guatemala");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuestaGuatemala;
    }

    public int[] cargaCecosPeru() {

        int[] respuestaSF = new int[2];
        int[] respuestaCom = new int[2];
        int[] respuestaPeru = new int[4];

        try {

            respuestaSF = cecoDAO.cargaCecosSFPeru();
            respuestaCom = cecoDAO.cargaCecosCOMPeru();

            respuestaPeru[0] = respuestaSF[0];
            respuestaPeru[1] = respuestaSF[1];
            respuestaPeru[2] = respuestaCom[0];
            respuestaPeru[3] = respuestaCom[1];

        } catch (Exception e) {
            logger.info("No fue posible cargar Sector Financiero y/o Comercio Peru");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuestaPeru;
    }

    public int[] cargaCecosHonduras() {

        int[] respuestaSF = new int[2];
        int[] respuestaCom = new int[2];
        int[] respuestaHonduras = new int[4];

        try {

            respuestaSF = cecoDAO.cargaCecosSFHonduras();
            respuestaCom = cecoDAO.cargaCecosCOMHonduras();

            respuestaHonduras[0] = respuestaSF[0];
            respuestaHonduras[1] = respuestaSF[1];
            respuestaHonduras[2] = respuestaCom[0];
            respuestaHonduras[3] = respuestaCom[1];

        } catch (Exception e) {
            logger.info("No fue posible cargar Sector Financiero y/o Comercio Peru");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuestaHonduras;
    }

    public int[] cargaCecosPanama() {

        int[] respuestaSF = new int[2];
        int[] respuestaPanama = new int[4];

        try {

            respuestaSF = cecoDAO.cargaCecosSFPanama();

            respuestaPanama[0] = respuestaSF[0];
            respuestaPanama[1] = respuestaSF[1];

        } catch (Exception e) {
            logger.info("No fue posible cargar Sector Financiero y/o Comercio Panama");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuestaPanama;
    }

    public int[] cargaCecosSalvador() {

        int[] respuestaSF = new int[2];
        int[] respuestaSalvador = new int[4];

        try {

            respuestaSF = cecoDAO.cargaCecosSFSalvador();

            respuestaSalvador[0] = respuestaSF[0];
            respuestaSalvador[1] = respuestaSF[1];

        } catch (Exception e) {
            logger.info("No fue posible cargar Sector Financiero y/o Comercio Salvador");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuestaSalvador;
    }

    public Map<String, Integer> cargaCecosLAM() {

        Map<String, Integer> respuestaLAM = new HashMap<String, Integer>();

        try {
            int[] resGuatemalaSF = cecoDAO.cargaCecosSFGuatemala();
            respuestaLAM.put("ejecucionSFGuatemala", resGuatemalaSF[0]);
            respuestaLAM.put("registrosSFGuatemala", resGuatemalaSF[1]);

            int[] resGuatemalaCom = cecoDAO.cargaCecosCOMGuatemala();
            respuestaLAM.put("ejecucionCOMGuatemala", resGuatemalaCom[0]);
            respuestaLAM.put("registrosCOMGuatemala", resGuatemalaCom[1]);

            int[] resPeruSF = cecoDAO.cargaCecosSFPeru();
            respuestaLAM.put("ejecucionSFPeru", resPeruSF[0]);
            respuestaLAM.put("registrosSFPeru", resPeruSF[1]);

            int[] resPeruCom = cecoDAO.cargaCecosCOMPeru();
            respuestaLAM.put("ejecucionCOMPeru", resPeruCom[0]);
            respuestaLAM.put("registrosCOMPeru", resPeruCom[1]);

            int[] resHondurasSF = cecoDAO.cargaCecosSFHonduras();
            respuestaLAM.put("ejecucionSFHonduras", resHondurasSF[0]);
            respuestaLAM.put("registrosSFHonduras", resHondurasSF[1]);

            int[] resHondurasCom = cecoDAO.cargaCecosCOMHonduras();
            respuestaLAM.put("ejecucionCOMHonduras", resHondurasCom[0]);
            respuestaLAM.put("registrosCOMHonduras", resHondurasCom[1]);

            int[] resPanamaSF = cecoDAO.cargaCecosSFPanama();
            respuestaLAM.put("ejecucionSFPanama", resPanamaSF[0]);
            respuestaLAM.put("registrosSFPanama", resPanamaSF[1]);

            int[] resSalvadorSF = cecoDAO.cargaCecosSFSalvador();
            respuestaLAM.put("ejecucionSFSalvador", resSalvadorSF[0]);
            respuestaLAM.put("registrosSFSalvador", resSalvadorSF[1]);

        } catch (Exception e) {
            logger.info("No fue posible cargar Sector Financiero y/o LAM");
            UtilFRQ.printErrorLog(null, e);

        }

        return respuestaLAM;

    }

    public boolean eliminaDuplicados() {
        boolean respuesta = false;

        try {
            respuesta = cecoDAO.eliminaDuplicados();
        } catch (Exception e) {

            logger.info("No fue posible eliminar duplicados de la tabla de paso.");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }
}
