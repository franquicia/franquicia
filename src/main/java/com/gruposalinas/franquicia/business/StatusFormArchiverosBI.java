package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.StatusFormArchiveroDAO;
import com.gruposalinas.franquicia.domain.StatusFormArchiveroDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class StatusFormArchiverosBI {

    private static Logger logger = LogManager.getLogger(StatusFormArchiverosBI.class);

    @Autowired
    StatusFormArchiveroDAO statusDAO;

    List<StatusFormArchiveroDTO> listStatus = null;

    public List<StatusFormArchiveroDTO> obtieneArchiveros(int idStatus) {
        try {
            listStatus = statusDAO.obtieneStatusFormArchiveros(idStatus);
        } catch (Exception e) {
            logger.info("No fue posible obtener el status");
            UtilFRQ.printErrorLog(null, e);
        }
        return listStatus;
    }

    public boolean insertaFormArchiveros(StatusFormArchiveroDTO bean) {
        boolean flag = false;
        try {
            flag = statusDAO.insertaStatusFormarchivero(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar el status");
            UtilFRQ.printErrorLog(null, e);
            flag = false;
        }
        return flag;
    }

    public boolean modificaFormArchiveros(StatusFormArchiveroDTO bean) {
        boolean flag = false;
        try {
            flag = statusDAO.modificaStatusFormarchivero(bean);
        } catch (Exception e) {
            logger.info("No fue posible monidicar el status");
            UtilFRQ.printErrorLog(null, e);
            flag = false;
        }
        return flag;
    }

    public boolean eliminaFormArchiveros(int idStatus) {
        boolean flag = false;
        try {
            flag = statusDAO.eliminaStatusFormarchivero(idStatus);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el status");
            UtilFRQ.printErrorLog(null, e);
            flag = false;
        }
        return flag;
    }

}
