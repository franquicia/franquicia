package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.PreguntaDAO;
import com.gruposalinas.franquicia.domain.PreguntaDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PreguntaBI {

    private static Logger logger = LogManager.getLogger(PreguntaBI.class);

    @Autowired
    PreguntaDAO preguntaDAO;

    List<PreguntaDTO> listaPregunta = null;
    List<PreguntaDTO> listaPreguntas = null;

    public List<PreguntaDTO> obtienePregunta() {
        try {
            listaPreguntas = preguntaDAO.obtienePregunta();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Pregunta");
            UtilFRQ.printErrorLog(null, e);

        }
        return listaPreguntas;
    }

    public List<PreguntaDTO> obtienePregunta(int idPreg) {
        try {
            listaPregunta = preguntaDAO.obtienePregunta(idPreg);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Preguntas");
            UtilFRQ.printErrorLog(null, e);
        }
        return listaPregunta;
    }

    public int insertaPreguntas(PreguntaDTO preguntas) {
        int idPregunta = 0;
        try {
            idPregunta = preguntaDAO.insertaPregunta(preguntas);
        } catch (Exception e) {
            logger.info("No fue posible insertar la Pregunta");
            UtilFRQ.printErrorLog(null, e);
        }

        return idPregunta;
    }

    public boolean actualizaPregunta(PreguntaDTO pregunta) {
        boolean respuesta = false;
        try {
            respuesta = preguntaDAO.actualizaPregunta(pregunta);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la Pregunta");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaPregunta(int idPreg) {
        boolean respuesta = false;

        try {
            respuesta = preguntaDAO.eliminaPregunta(idPreg);
        } catch (Exception e) {
            logger.info("No fue posible eliminar la Pregunta");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }
}
