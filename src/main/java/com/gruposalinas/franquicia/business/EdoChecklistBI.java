package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.EdoChecklistDAO;
import com.gruposalinas.franquicia.domain.EdoChecklistDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class EdoChecklistBI {

    private static Logger logger = LogManager.getLogger(EdoChecklistBI.class);

    @Autowired

    EdoChecklistDAO edoChecklistDAO;

    List<EdoChecklistDTO> listaedoChecklist = null;

    List<EdoChecklistDTO> listaedosChecklist = null;

    public List<EdoChecklistDTO> obtieneEdoChecklist() {
        try {
            listaedosChecklist = edoChecklistDAO.obtieneEdoChecklist();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Estado del Checklist");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaedosChecklist;
    }

    public List<EdoChecklistDTO> obtieneEdoChecklist(int idEdoCK) {
        try {
            listaedoChecklist = edoChecklistDAO.obtieneEdoChecklist(idEdoCK);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Estado del Checklist");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaedoChecklist;
    }

    public int insertaEdoChecklist(EdoChecklistDTO edoCheck) {

        int idEdoCK = 0;
        try {
            idEdoCK = edoChecklistDAO.insertaEdoChecklist(edoCheck);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Estado del Checklist");
            UtilFRQ.printErrorLog(null, e);
        }

        return idEdoCK;
    }

    public boolean actualizaEdoChecklist(EdoChecklistDTO edoCheck) {
        boolean respuesta = false;
        try {
            respuesta = edoChecklistDAO.actualizaEdoChecklist(edoCheck);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Estado del Checklist");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaEdoChecklist(int idEdoCK) {
        boolean respuesta = false;

        try {
            respuesta = edoChecklistDAO.eliminaEdoCheckList(idEdoCK);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Estado del Checklist");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }
}
