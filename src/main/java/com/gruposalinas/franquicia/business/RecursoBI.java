package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.RecursoDAO;
import com.gruposalinas.franquicia.domain.RecursoDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class RecursoBI {

    private static Logger logger = LogManager.getLogger(ReporteBI.class);

    @Autowired
    RecursoDAO recursoDAO;

    List<RecursoDTO> listaRecurso = null;

    public List<RecursoDTO> buscaRecurso(String idRecurso) {

        try {

            if (idRecurso.equals("")) {
                idRecurso = null;
            }

            listaRecurso = recursoDAO.buscaRecurso(idRecurso);

        } catch (Exception e) {
            UtilFRQ.printErrorLog(null, e);
        }

        return listaRecurso;
    }

    public int insertaRecurso(RecursoDTO bean) {

        int idRecurso = 0;

        try {
            idRecurso = recursoDAO.insertaRecurso(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Recurso");
            UtilFRQ.printErrorLog(null, e);
        }

        return idRecurso;

    }

    public boolean actualizaRecurso(RecursoDTO bean) {

        boolean respuesta = false;

        try {
            respuesta = recursoDAO.actualizaRecurso(bean);

        } catch (Exception e) {
            logger.info("No fue posible insertar el Recurso");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaRecurso(int idRecurso) {

        boolean respuesta = false;

        try {
            respuesta = recursoDAO.eliminaRecurso(idRecurso);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Recurso");
        }

        return respuesta;
    }
}
