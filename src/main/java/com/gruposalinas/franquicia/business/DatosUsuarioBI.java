package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.DatosUsuarioDAO;
import com.gruposalinas.franquicia.domain.DatosPerfilDTO;
import com.gruposalinas.franquicia.domain.DatosUsuarioDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class DatosUsuarioBI {

    private Logger logger = LogManager.getLogger(DatosUsuarioBI.class);

    @Autowired
    DatosUsuarioDAO datosUsuarioDAO;

    public DatosUsuarioDTO buscaUsuario(String usuario) {

        List<DatosUsuarioDTO> datosUsuario = null;

        try {

            datosUsuario = datosUsuarioDAO.obtieneDatos(usuario);

            if (datosUsuario.size() > 0) {
                if (datosUsuario.get(0).getUsuario() != null) {
                    return datosUsuario.get(0);
                }

            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.info("Ocurrio algo al obtener los datos del usuario");
            UtilFRQ.printErrorLog(null, e);
            return null;
        }

        return null;

    }

    public Map<String, Object> buscaUsuario(String usuario, int tipo) {

        List<DatosUsuarioDTO> datosUsuario = null;
        List<DatosPerfilDTO> datosPerfil = null;
        Map<String, Object> lista = null;

        try {

            datosUsuario = datosUsuarioDAO.obtieneDatos(usuario);

            if (datosUsuario.size() > 0) {
                if (datosUsuario.get(0).getUsuario() != null) {
                    lista = new HashMap<String, Object>();
                    datosPerfil = new ArrayList<DatosPerfilDTO>();

                    for (DatosUsuarioDTO dUsuario : datosUsuario) {

                        if (dUsuario.getIdPerfil() != 0) {
                            DatosPerfilDTO perfil = new DatosPerfilDTO();
                            perfil.setIdPerfil(dUsuario.getIdPerfil());
                            perfil.setDescPerfil(dUsuario.getDescPerfil());
                            datosPerfil.add(perfil);
                        }
                    }
                    lista.put("datosUsuario", datosUsuario.get(0));
                    lista.put("datosPerfil", datosPerfil);
                    return lista;
                }

            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.info("Ocurrio algo al obtener los datos del usuario");

            return null;
        }

        return null;

    }

}
