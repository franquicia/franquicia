package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.FiltrosCecoDAO;
import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.FiltrosCecoDTO;
import com.gruposalinas.franquicia.domain.PaisDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class FiltrosCecoBI {

    private Logger logger = LogManager.getLogger(FiltrosCecoBI.class);

    @Autowired
    FiltrosCecoDAO filtrosCecoDAO;

    public Map<String, Object> obtieneFiltros(int idUsuario) {
        Map<String, Object> filtros = new HashMap<String, Object>();

        try {
            filtros = filtrosCecoDAO.obtieneFiltros(idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible obtener los filtros");
            UtilFRQ.printErrorLog(null, e);
        }

        return filtros;

    }

    public List<FiltrosCecoDTO> obtieneCecos(int idCeco, int tipoBusqueda) {

        List<FiltrosCecoDTO> cecos = null;

        try {
            cecos = filtrosCecoDAO.obtieneCecos(idCeco, tipoBusqueda);
        } catch (Exception e) {
            logger.info("No fue posible obtener los cecos");
            UtilFRQ.printErrorLog(null, e);
        }

        return cecos;
    }

    public List<ChecklistDTO> obtieneChecklist(int idUsuario) {

        List<ChecklistDTO> cecos = null;

        try {
            cecos = filtrosCecoDAO.obtieneChecklist(idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible obtener los checklist");
            UtilFRQ.printErrorLog(null, e);
        }

        return cecos;
    }

    public List<PaisDTO> obtienePaises(int negocio) {

        List<PaisDTO> paises = null;

        try {
            paises = filtrosCecoDAO.obtienePaises(negocio);
        } catch (Exception e) {
            logger.info("No fue posible obtener los paises");
            UtilFRQ.printErrorLog(null, e);
        }

        return paises;

    }

    public List<PaisDTO> obtienePaises(int negocio, int idUsuario) {

        List<PaisDTO> paises = null;

        try {
            paises = filtrosCecoDAO.obtienePaises(negocio, idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible obtener los paises");
            UtilFRQ.printErrorLog(null, e);
        }

        return paises;

    }

    public List<FiltrosCecoDTO> obtieneTerritorios(int pais, int negocio) {

        List<FiltrosCecoDTO> cecos = null;

        try {
            cecos = filtrosCecoDAO.obtieneTerritorios(pais, negocio);
        } catch (Exception e) {
            logger.info("No fue posible obtener los cecos");
            UtilFRQ.printErrorLog(null, e);
        }

        return cecos;
    }

}
