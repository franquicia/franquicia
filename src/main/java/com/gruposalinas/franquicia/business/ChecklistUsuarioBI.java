package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.ChecklistUsuarioDAO;
import com.gruposalinas.franquicia.domain.ChecklistUsuarioDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ChecklistUsuarioBI {

    private static Logger logger = LogManager.getLogger(ChecklistUsuarioBI.class);

    @Autowired
    ChecklistUsuarioDAO checklistUsuarioDAO;

    List<ChecklistUsuarioDTO> listaCheckUsuario;
    List<ChecklistUsuarioDTO> listaCheckU;

    public List<ChecklistUsuarioDTO> obtieneCheckUsuario(int idCheckU) {
        try {
            listaCheckUsuario = checklistUsuarioDAO.obtieneCheckUsuario(idCheckU);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Checklist Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaCheckUsuario;
    }

    public List<ChecklistUsuarioDTO> obtieneCheckU(String idUsuario) {
        try {
            listaCheckUsuario = checklistUsuarioDAO.obtieneCheckU(idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Checklist Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaCheckUsuario;
    }

    public List<ChecklistUsuarioDTO> obtieneCheckUsua(String idUsuario, String ceco, String fechaInicio) {
        try {
            listaCheckUsuario = checklistUsuarioDAO.obtieneCheckUsua(idUsuario, ceco, fechaInicio);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Checklist Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaCheckUsuario;
    }

    public int insertaCheckUsuario(ChecklistUsuarioDTO checkUsuario) {
        int idcheckU = 0;

        try {
            idcheckU = checklistUsuarioDAO.insertaCheckUsuario(checkUsuario);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Checklist Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return idcheckU;
    }

    public boolean actualizaChecklistUsuario(ChecklistUsuarioDTO checkUsuario) {
        boolean respuesta = false;
        try {
            respuesta = checklistUsuarioDAO.actualizaCheckUsuario(checkUsuario);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el  Checklist Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaChecklistUsuario(int idCheckU) {
        boolean respuesta = false;
        try {
            respuesta = checklistUsuarioDAO.eliminaCheckUsuario(idCheckU);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Checklist Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean desactivaChecklist(int idChecklist) {

        boolean respuesta = false;

        try {
            respuesta = checklistUsuarioDAO.desactivaChecklist(idChecklist);
        } catch (Exception e) {
            logger.info("No fue posible desactivar los Checklist-Usuarios");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;

    }

    public boolean desactivaCheckUsua(int idCeco, int idChecklist) {

        boolean respuesta = false;

        try {
            respuesta = checklistUsuarioDAO.desactivaCheckUsua(idCeco, idChecklist);
        } catch (Exception e) {
            logger.info("No fue posible desactivar los Checklist-Usuarios");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;

    }

    public boolean insertaCheckusuaEspecial(int idUsuario, int idChecklist, String ceco) {

        boolean respuesta = false;

        try {
            respuesta = checklistUsuarioDAO.insertaCheckusuaEspecial(idUsuario, idChecklist, ceco);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Checklist-Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;

    }

}
