package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.NivelDAO;
import com.gruposalinas.franquicia.domain.NivelDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class NivelBI {

    private static Logger logger = LogManager.getLogger(NivelBI.class);

    @Autowired
    NivelDAO nivelDAO;

    List<NivelDTO> listaniveles = null;
    List<NivelDTO> listanivel = null;

    public List<NivelDTO> obtieneNivel() {

        try {
            listaniveles = nivelDAO.obtieneNivel();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Nivel");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaniveles;
    }

    public List<NivelDTO> obtieneNivel(int idNivel) {

        try {
            listanivel = nivelDAO.obtieneNivel(idNivel);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Nivel");
            UtilFRQ.printErrorLog(null, e);
        }

        return listanivel;
    }

    public int insertaNivel(NivelDTO nivel) {

        int idNivel = 0;

        try {
            idNivel = nivelDAO.insertaNivel(nivel);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Nivel");
            UtilFRQ.printErrorLog(null, e);
        }

        return idNivel;
    }

    public boolean actualizaNivel(NivelDTO nivel) {

        boolean respuesta = false;

        try {
            respuesta = nivelDAO.actualizaNivel(nivel);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Nivel");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaNivel(int idNivel) {

        boolean respuesta = false;

        try {
            respuesta = nivelDAO.eliminaNivel(idNivel);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Nivel");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }
}
