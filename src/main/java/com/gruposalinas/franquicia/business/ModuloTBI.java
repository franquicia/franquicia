package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.ModuloTDAO;
import com.gruposalinas.franquicia.domain.ModuloDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ModuloTBI {

    private static Logger logger = LogManager.getLogger(ModuloTBI.class);

    @Autowired
    ModuloTDAO moduloDAO;
    List<ModuloDTO> listaModulo = null;

    public List<ModuloDTO> obtieneModuloTemp(String idModulo) {
        try {
            listaModulo = moduloDAO.obtieneModuloTemp(idModulo);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Temporal");
            UtilFRQ.printErrorLog(null, e);

        }
        return listaModulo;
    }

    public int insertaModulo(ModuloDTO modulo) {

        int respuesta = 0;
        try {
            respuesta = moduloDAO.insertaModuloTemp(modulo);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Modulo");
            UtilFRQ.printErrorLog(null, e);
        }
        return respuesta;
    }

    public boolean actualizaModulo(ModuloDTO modulo) {

        boolean respuesta = false;

        try {
            respuesta = moduloDAO.actualizaModuloTemp(modulo);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la tabla Modulo");
            UtilFRQ.printErrorLog(null, e);

        }

        return respuesta;
    }

    public boolean eliminaModulo(int idModulo) {
        boolean respuesta = false;

        try {
            respuesta = moduloDAO.eliminaModuloTemp(idModulo);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Modulo en la tabla temporal");
            UtilFRQ.printErrorLog(null, e);

        }
        return respuesta;
    }
}
