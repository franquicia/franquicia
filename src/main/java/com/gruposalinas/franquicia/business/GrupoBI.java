package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.GrupoDAO;
import com.gruposalinas.franquicia.domain.GrupoDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class GrupoBI {

    private static Logger logger = LogManager.getLogger(GrupoBI.class);

    @Autowired
    GrupoDAO grupoDAO;

    List<GrupoDTO> listaGrupo = null;

    public List<GrupoDTO> obtieneGrupo(String idGrupo) {

        try {
            listaGrupo = grupoDAO.obtieneGrupo(idGrupo);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Evidencia");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaGrupo;
    }

    public int insertaGrupo(GrupoDTO bean) {

        int idGrupo = 0;

        try {
            idGrupo = grupoDAO.insertaGrupo(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Grupo");
            UtilFRQ.printErrorLog(null, e);
        }

        return idGrupo;
    }

    public boolean actualizaGrupo(GrupoDTO bean) throws Exception {

        boolean respuesta = false;

        try {
            respuesta = grupoDAO.actualizaGrupo(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Grupo");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaGrupo(int idGrupo) {

        boolean respuesta = false;

        try {
            respuesta = grupoDAO.eliminaGrupo(idGrupo);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Grupo");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

}
