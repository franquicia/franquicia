package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.ModuloDAO;
import com.gruposalinas.franquicia.domain.ModuloDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ModuloBI {

    private static Logger logger = LogManager.getLogger(ModuloBI.class);

    @Autowired
    ModuloDAO moduloDAO;

    List<ModuloDTO> listaModulos = null;

    List<ModuloDTO> listaModulo = null;

    public List<ModuloDTO> obtieneModulo() {
        try {
            listaModulos = moduloDAO.obtieneModulo();
        } catch (Exception e) {
            logger.info("No fue posible obtener los datos de la tabla Modulo");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaModulos;
    }

    public List<ModuloDTO> obtieneModulo(int idModulo) {
        try {
            listaModulo = moduloDAO.obtieneModulo(idModulo);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Modulo");
            UtilFRQ.printErrorLog(null, e);

        }
        return listaModulo;
    }

    public int insertaModulo(ModuloDTO modulo) {

        int idModulo = 0;
        try {
            idModulo = moduloDAO.insertaModulo(modulo);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Modulo");
            UtilFRQ.printErrorLog(null, e);
        }
        return idModulo;
    }

    public boolean actualizaModulo(ModuloDTO modulo) {

        boolean respuesta = false;

        try {
            respuesta = moduloDAO.actualizaModulo(modulo);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la tabla Modulo");
            UtilFRQ.printErrorLog(null, e);

        }

        return respuesta;
    }

    public boolean eliminaModulo(int idModulo) {
        boolean respuesta = false;

        try {
            respuesta = moduloDAO.eliminaModulo(idModulo);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Modulo");
            UtilFRQ.printErrorLog(null, e);

        }
        return respuesta;
    }
}
