package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.LoginDAO;
import com.gruposalinas.franquicia.domain.LoginDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class LoginBI {

    private static final Logger logger = LogManager.getLogger(LoginBI.class);

    @Autowired
    LoginDAO loginDAO;

    List<LoginDTO> listaUsuario = null;
    Map<String, Object> listaUsuarioChecks = null;

    public LoginDTO consultaUsuario(int noUsuario) throws Exception {
        try {
            listaUsuario = loginDAO.buscaUsuario(noUsuario);
        } catch (Exception e) {
            logger.info(e.getMessage());
            UtilFRQ.printErrorLog(null, e);
        }

        if (listaUsuario.size() == 0) {
            return null;
        } else {
            return listaUsuario.get(0);
        }
    }

    public Map<String, Object> buscaUsuarioCheck(int noEmpleado) throws Exception {
        try {
            listaUsuarioChecks = loginDAO.buscaUsuarioCheck(noEmpleado);
        } catch (Exception e) {
            logger.info(e.getMessage());
        }

        if (listaUsuarioChecks.size() == 0) {
            return Collections.<String, Object>emptyMap();
        } else {
            return listaUsuarioChecks;
        }
    }
}
