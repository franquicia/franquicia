package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.AsignacionDAO;
import com.gruposalinas.franquicia.domain.AsignacionDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class AsignacionBI {

    @Autowired
    AsignacionDAO asignacionDAO;

    private Logger logger = LogManager.getLogger(AsignacionBI.class);

    public List<AsignacionDTO> obtieneAsignaciones(String idChecklist, String idCeco, String idPuesto, String activo) {

        List<AsignacionDTO> respuesta = null;

        try {
            respuesta = asignacionDAO.obtieneAsignaciones(idChecklist, idCeco, idPuesto, activo);
        } catch (Exception e) {
            logger.info("No fue posible consultar las asignaciones");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean insertaAsignacion(AsignacionDTO asignacion) {

        boolean respuesta = false;

        try {
            respuesta = asignacionDAO.insertaAsignacion(asignacion);
        } catch (Exception e) {
            logger.info("No fue posible insertar la asignacion");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean actualizaAsignacion(AsignacionDTO asignacion) {

        boolean respuesta = false;

        try {
            respuesta = asignacionDAO.actualizaAsignacion(asignacion);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la asignacion");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminarAsignacion(AsignacionDTO asignacion) {

        boolean respuesta = false;

        try {
            respuesta = asignacionDAO.eliminaAsignacion(asignacion);
        } catch (Exception e) {
            logger.info("No fue posible eliminar la asignacion");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public Map<String, Object> ejecutaAsignacion() {

        Map<String, Object> respuesta = null;

        try {
            respuesta = asignacionDAO.ejecutaAsignacion();
        } catch (Exception e) {
            logger.info("No fue posible eliminar la asignacion");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

}
