package com.gruposalinas.franquicia.business;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.franquicia.domain.DocumentoSucursalDTO;
import com.gruposalinas.franquicia.domain.TipoDocumentoDTO;
import com.gruposalinas.franquicia.domain.TipoDocumentoUrlsDTO;
import com.gruposalinas.franquicia.servicios.cliente.WS_JAC_PEDESTAL_DIGITALStub;
import com.gruposalinas.franquicia.servicios.cliente.WS_JAC_PEDESTAL_DIGITALStub.ArrayOfInfoDocSucursal;
import com.gruposalinas.franquicia.servicios.cliente.WS_JAC_PEDESTAL_DIGITALStub.InfoDocSucursal;
import com.gruposalinas.franquicia.servicios.cliente.WS_JAC_PEDESTAL_DIGITALStub.ObtenerDocumentosSucursal;
import com.gruposalinas.franquicia.servicios.cliente.WS_JAC_PEDESTAL_DIGITALStub.ObtenerDocumentosSucursalResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FilesSucursalPedestalBI {

    private Logger logger = LogManager.getLogger(FilesSucursalPedestalBI.class);

    public InfoDocSucursal[] getFilesSucursal(int sucursal) {

        InfoDocSucursal[] documentosSucursal = null;

        try {

            WS_JAC_PEDESTAL_DIGITALStub clienteFiles = new WS_JAC_PEDESTAL_DIGITALStub();

            ObtenerDocumentosSucursal objetoParam = new ObtenerDocumentosSucursal();
            objetoParam.setNumEconomicoSucursal(sucursal);

            ObtenerDocumentosSucursalResponse response = clienteFiles.obtenerDocumentosSucursal(objetoParam);

            ArrayOfInfoDocSucursal result = response.getObtenerDocumentosSucursalResult();

            documentosSucursal = result.getInfoDocSucursal();

        } catch (Exception e) {

            documentosSucursal = null;
        }

        return documentosSucursal;

    }

    public ArrayList<DocumentoSucursalDTO> getDocumentoBySucursal(int sucursal) {

        ArrayList<DocumentoSucursalDTO> arrayListDocumentos = new ArrayList<>();

        try {

            DocumentoSucursalDTO documentoDTO = null;

            WS_JAC_PEDESTAL_DIGITALStub clienteFiles = new WS_JAC_PEDESTAL_DIGITALStub();

            ObtenerDocumentosSucursal objetoParam = new ObtenerDocumentosSucursal();
            objetoParam.setNumEconomicoSucursal(sucursal);

            ObtenerDocumentosSucursalResponse response = clienteFiles.obtenerDocumentosSucursal(objetoParam);

            ArrayOfInfoDocSucursal result = response.getObtenerDocumentosSucursalResult();

            InfoDocSucursal[] documentosSucursal = result.getInfoDocSucursal();

            if (documentosSucursal != null) {

                for (InfoDocSucursal documento : documentosSucursal) {

                    String archivoBase64 = "";
                    documentoDTO = new DocumentoSucursalDTO();
                    documentoDTO.setTipo(documento.getDocumento());
                    documentoDTO.setNegocio(documento.getNegocio());
                    documentoDTO.setMensaje(documento.getMensaje());

                    if (documento.getUrlImagen() != null && documento.getUrlImagen().length() > 0) {
                        archivoBase64 = downloadFile(documento.getUrlImagen());
                    }

                    documentoDTO.setFileBase64(archivoBase64);

                    arrayListDocumentos.add(documentoDTO);
                }

            }

        } catch (Exception e) {

            arrayListDocumentos = new ArrayList<>();

            e.printStackTrace();
        }

        return arrayListDocumentos;

    }

    public ArrayList<DocumentoSucursalDTO> getDocumentoBySucursal(String jsonContenido) {

        InfoDocSucursal[] documentosSucursal = null;
        ArrayList<DocumentoSucursalDTO> documentosNoticados = null;
        ArrayList<TipoDocumentoDTO> arrayDocumentosSucursal = null;

        try {

            boolean notificaDocumento = false;
            boolean existeDocumentoEnArray = false;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");

            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(jsonContenido).getAsJsonObject();

            String sucursal = jsonObject.get("sucursal").getAsString();

            JsonArray arrayJson = jsonObject.getAsJsonArray("data");

            documentosSucursal = getFilesSucursal(Integer.parseInt(sucursal));

            //Convertir JPG a PDF
            arrayDocumentosSucursal = getTempFiles(documentosSucursal);

            if (arrayDocumentosSucursal != null && arrayDocumentosSucursal.size() > 0) {

                documentosNoticados = new ArrayList<>();

                //Recorre los documento que la tableta deberia tener
                for (TipoDocumentoDTO tipoDocumentoSucursal : arrayDocumentosSucursal) {

                    notificaDocumento = false;
                    existeDocumentoEnArray = false;

                    DocumentoSucursalDTO documentoSucursalDTO = null;

                    Date fechaDocumentoTableta = null;
                    Date fechaDocumentoSucursal = null;

                    if (arrayJson != null && arrayJson.size() > 0) {

                        //Recorre los documentos que tiene la tableta
                        for (int i = 0; i < arrayJson.size(); i++) {

                            JsonObject documentoJson = arrayJson.get(i).getAsJsonObject();

                            int idTipoDocumentoTableta = documentoJson.get("tipo_documento").getAsInt();
                            String fechaDoctoString = documentoJson.get("fecha_ultima_actualizacion").getAsString();

                            //Verificamos que exista el tipo de documento que debe tener
                            if (idTipoDocumentoTableta == tipoDocumentoSucursal.getIdTipoDocumento()) {

                                existeDocumentoEnArray = true;

                                if (tipoDocumentoSucursal.getArchivo() != null) {

                                    fechaDocumentoSucursal = sdf.parse(tipoDocumentoSucursal.getUltimaActualizacion());
                                    fechaDocumentoTableta = sdf.parse(fechaDoctoString);

                                    //Validamos que la sucrusal tenga el mismo documento
                                    if (fechaDocumentoSucursal.getTime() > fechaDocumentoTableta.getTime()) {

                                        notificaDocumento = true;

                                    }

                                } else {
                                    notificaDocumento = true;
                                }

                                break;
                            }

                        }

                    } else {
                        notificaDocumento = true;
                    }

                    if (!existeDocumentoEnArray) {
                        notificaDocumento = true;
                    }

                    if (notificaDocumento) {
                        //Agerga documento a la lista de respuesta

                        documentoSucursalDTO = new DocumentoSucursalDTO();

                        documentoSucursalDTO.setIdTipo(tipoDocumentoSucursal.getIdTipoDocumento());
                        documentoSucursalDTO.setTipo(tipoDocumentoSucursal.getDocumento());
                        documentoSucursalDTO.setMensaje(tipoDocumentoSucursal.getMensaje());
                        documentoSucursalDTO.setNegocio(tipoDocumentoSucursal.getNegocio());
                        documentoSucursalDTO.setFileBase64(fileToBase64(tipoDocumentoSucursal.getArchivo()));
                        documentoSucursalDTO.setUltimaActulizacion(tipoDocumentoSucursal.getUltimaActualizacion());

                        documentosNoticados.add(documentoSucursalDTO);

                    }

                }

            } else {
                documentosNoticados = new ArrayList<>();
            }

        } catch (Exception e) {

            documentosNoticados = new ArrayList<>();

        } finally {

            if (arrayDocumentosSucursal != null) {
                eliminaTemporales(arrayDocumentosSucursal);
            }

        }

        return documentosNoticados;
    }

    public ArrayList<TipoDocumentoUrlsDTO> getDocumentoBySucursalUrls(String jsonContenido) {

        InfoDocSucursal[] documentosSucursal = null;
        ArrayList<TipoDocumentoUrlsDTO> documentosNoticados = null;
        ArrayList<TipoDocumentoUrlsDTO> arrayDocumentosUrlsSucursal = null;

        try {

            boolean notificaDocumento = false;
            boolean existeDocumentoEnArray = false;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");

            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(jsonContenido).getAsJsonObject();

            String sucursal = jsonObject.get("sucursal").getAsString();

            JsonArray arrayJson = jsonObject.getAsJsonArray("data");

            documentosSucursal = getFilesSucursal(Integer.parseInt(sucursal));

            //Convertir JPG a PDF
            arrayDocumentosUrlsSucursal = getArrayUrls(documentosSucursal);

            if (arrayDocumentosUrlsSucursal != null && arrayDocumentosUrlsSucursal.size() > 0) {

                documentosNoticados = new ArrayList<>();

                //Recorre los documento que la tableta deberia tener
                for (TipoDocumentoUrlsDTO tipoDocumentoUrlsSucursal : arrayDocumentosUrlsSucursal) {

                    notificaDocumento = false;
                    existeDocumentoEnArray = false;

                    TipoDocumentoUrlsDTO documentoSucursalDTO = null;

                    Date fechaDocumentoTableta = null;
                    Date fechaDocumentoSucursal = null;

                    if (arrayJson != null && arrayJson.size() > 0) {

                        //Recorre los documentos que tiene la tableta
                        for (int i = 0; i < arrayJson.size(); i++) {

                            JsonObject documentoJson = arrayJson.get(i).getAsJsonObject();

                            int idTipoDocumentoTableta = documentoJson.get("tipo_documento").getAsInt();
                            String fechaDoctoString = documentoJson.get("fecha_ultima_actualizacion").getAsString();

                            //Verificamos que exista el tipo de documento que debe tener
                            if (idTipoDocumentoTableta == tipoDocumentoUrlsSucursal.getIdTipoDocumento()) {

                                existeDocumentoEnArray = true;

                                if (tipoDocumentoUrlsSucursal.getUrls() != null && tipoDocumentoUrlsSucursal.getUrls().size() > 0) {

                                    fechaDocumentoSucursal = sdf.parse(tipoDocumentoUrlsSucursal.getUltimaActualizacion());
                                    fechaDocumentoTableta = sdf.parse(fechaDoctoString);

                                    //Validamos que la sucrusal tenga el mismo documento
                                    if (fechaDocumentoSucursal.getTime() > fechaDocumentoTableta.getTime()) {

                                        notificaDocumento = true;

                                    }

                                } else {
                                    notificaDocumento = true;
                                }

                                break;
                            }

                        }

                    } else {
                        notificaDocumento = true;
                    }

                    if (!existeDocumentoEnArray) {
                        notificaDocumento = true;
                    }

                    if (notificaDocumento) {
                        //Agerga documento a la lista de respuesta

                        documentosNoticados.add(tipoDocumentoUrlsSucursal);

                    }

                }

            } else {
                documentosNoticados = new ArrayList<>();
            }

        } catch (Exception e) {

            documentosNoticados = new ArrayList<>();

        }

        return documentosNoticados;

    }

    private ArrayList<TipoDocumentoDTO> getTempFiles(InfoDocSucursal[] documentosSucursal) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");

        int contador = 0;
        InfoDocSucursal docSucursalAnterior = null;

        int numeroDocumentos = 0;

        File tmp = null;

        ArrayList<TipoDocumentoDTO> arrayListTipoDocumentos = null;
        TipoDocumentoDTO tipoDocumento = null;

        ArrayList<File> archivos = null;

        try {

            arrayListTipoDocumentos = new ArrayList<>();

            while (contador < documentosSucursal.length) {

                numeroDocumentos = 1;

                docSucursalAnterior = documentosSucursal[contador];
                archivos = new ArrayList<>();

                while (contador < documentosSucursal.length && docSucursalAnterior.getIdDocumento() == documentosSucursal[contador].getIdDocumento()) {

                    System.out.println("URL (" + documentosSucursal[contador].getUrlImagen() + ")");

                    if (documentosSucursal[contador].getUrlImagen() != null && !documentosSucursal[contador].getUrlImagen().equals("")) {

                        if (documentosSucursal[contador].getUrlImagen().contains(".jpg")
                                || documentosSucursal[contador].getUrlImagen().contains(".jpeg")) {

                            tmp = convertImageToPdf(documentosSucursal[contador].getIdDocumento(),
                                    documentosSucursal[contador].getDocumento(),
                                    documentosSucursal[contador].getEconomico() + "",
                                    numeroDocumentos,
                                    documentosSucursal[contador].getUrlImagen());

                        } else {

                            tmp = File.createTempFile("converted_" + documentosSucursal[contador].getIdDocumento()
                                    + "_" + documentosSucursal[contador].getDocumento() + "_" + documentosSucursal[contador].getEconomico() + "_" + numeroDocumentos, ".pdf");

                            FileUtils.copyURLToFile(new URL(documentosSucursal[contador].getUrlImagen()), tmp);

                        }

                    } else {
                        tmp = null;
                    }

                    if (tmp != null) {
                        archivos.add(tmp);
                    }

                    docSucursalAnterior.setUltimaFechaModificacion(documentosSucursal[contador].getUltimaFechaModificacion());
                    numeroDocumentos++;

                    contador++;

                }

                tipoDocumento = new TipoDocumentoDTO();

                tipoDocumento.setIdTipoDocumento(docSucursalAnterior.getIdDocumento());
                tipoDocumento.setDocumento(docSucursalAnterior.getDocumento());
                tipoDocumento.setMensaje(docSucursalAnterior.getMensaje());
                tipoDocumento.setNegocio(docSucursalAnterior.getNegocio());

                File fileTipoDocument = null;
                if (archivos != null && archivos.size() > 1) {
                    fileTipoDocument = mergePDFs(archivos, docSucursalAnterior.getIdDocumento());
                } else {
                    fileTipoDocument = tmp;
                }

                tipoDocumento.setArchivo(fileTipoDocument);

                String fechaUltimaActualizacion = "";

                if (docSucursalAnterior.getUltimaFechaModificacion().equals("")) {
                    fechaUltimaActualizacion = sdf.format(new Date());
                } else {
                    fechaUltimaActualizacion = docSucursalAnterior.getUltimaFechaModificacion();
                }

                tipoDocumento.setUltimaActualizacion(fechaUltimaActualizacion);

                arrayListTipoDocumentos.add(tipoDocumento);
            }
        } catch (Exception e) {

            System.out.println("Error ");

            e.printStackTrace();

        }

        return arrayListTipoDocumentos;
    }

    private ArrayList<TipoDocumentoUrlsDTO> getArrayUrls(InfoDocSucursal[] documentosSucursal) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");

        int contador = 0;
        InfoDocSucursal docSucursalAnterior = null;

        File tmp = null;

        ArrayList<TipoDocumentoUrlsDTO> arrayListTipoDocumentosURLS = null;
        TipoDocumentoUrlsDTO tipoDocumentoUrls = null;

        try {
            if (documentosSucursal != null) {

                arrayListTipoDocumentosURLS = new ArrayList<>();

                while (contador < documentosSucursal.length) {

                    docSucursalAnterior = documentosSucursal[contador];

                    ArrayList<String> urls = new ArrayList<>();

                    while (contador < documentosSucursal.length && docSucursalAnterior.getIdDocumento() == documentosSucursal[contador].getIdDocumento()) {

                        System.out.println("URL (" + documentosSucursal[contador].getUrlImagen() + ")");

                        if (documentosSucursal[contador].getUrlImagen() != null && !documentosSucursal[contador].getUrlImagen().equals("")) {
                            urls.add(documentosSucursal[contador].getUrlImagen());
                        }

                        docSucursalAnterior.setUltimaFechaModificacion(documentosSucursal[contador].getUltimaFechaModificacion());
                        contador++;

                    }

                    tipoDocumentoUrls = new TipoDocumentoUrlsDTO();

                    tipoDocumentoUrls.setIdTipoDocumento(docSucursalAnterior.getIdDocumento());
                    tipoDocumentoUrls.setDocumento(docSucursalAnterior.getDocumento());
                    tipoDocumentoUrls.setMensaje(docSucursalAnterior.getMensaje());
                    tipoDocumentoUrls.setNegocio(docSucursalAnterior.getNegocio());
                    tipoDocumentoUrls.setUrls(urls);
                    tipoDocumentoUrls.setEconomico(docSucursalAnterior.getEconomico());

                    String fechaUltimaActualizacion = "";

                    if (docSucursalAnterior.getUltimaFechaModificacion().equals("")) //fechaUltimaActualizacion = sdf.format(new Date());
                    {
                        fechaUltimaActualizacion = "0001-01-01-00:00:00";
                    } else {
                        fechaUltimaActualizacion = docSucursalAnterior.getUltimaFechaModificacion();
                    }

                    tipoDocumentoUrls.setUltimaActualizacion(fechaUltimaActualizacion);

                    arrayListTipoDocumentosURLS.add(tipoDocumentoUrls);

                }
            } else {
                logger.info("documento sucursal es nulo");
            }
        } catch (Exception e) {

            System.out.println("Error ");

            e.printStackTrace();

        }

        return arrayListTipoDocumentosURLS;
    }

    private String downloadFile(String imgUrl) {

        String archivoBase64 = "";

        try {
            URL url = new URL(imgUrl);
            ByteArrayOutputStream out;
            try (InputStream in = new BufferedInputStream(url.openStream())) {
                out = new ByteArrayOutputStream();
                byte[] buf = new byte[1024];
                int n = 0;
                while (-1 != (n = in.read(buf))) {
                    out.write(buf, 0, n);
                }
                out.close();
            }
            byte[] response = out.toByteArray();

            archivoBase64 = new String(Base64.encodeBase64(response), "UTF-8");

        } catch (Exception ex) {
            archivoBase64 = "";
        }

        return archivoBase64;
    }

    private File convertImageToPdf(int idTipo, String documento, String sucursal, int consecutivo, String urlImage) {

        File temp = null;

        try {

            temp = File.createTempFile("converted_" + idTipo + "_" + documento + "_" + sucursal + "_" + consecutivo, ".pdf");

            //Create Document Object
            Document convertJpgToPdf = new Document();

            //Create PdfWriter for Document to hold physical file
            PdfWriter.getInstance(convertJpgToPdf, new FileOutputStream(temp));
            convertJpgToPdf.open();
            //Get the input image to Convert to PDF
            com.itextpdf.text.Image convertJpg = com.itextpdf.text.Image.getInstance(urlImage);
            //Image convertJpg= Image.getInstance("/Users/b191312/Documents/america_camepon.jpg");

            convertJpg.setAbsolutePosition(0, 0);
            convertJpg.scaleAbsolute(PageSize.A4);

            convertJpg.setBorderWidth(0);
            //Add image to Document
            convertJpgToPdf.add(convertJpg);

            //Close Document
            convertJpgToPdf.close();
            System.out.println("Successfully Converted JPG to PDF in iText");
        } catch (Exception i1) {
            i1.printStackTrace();
        }

        return temp;

    }

    private File mergePDFs(ArrayList<File> tmpFiles, int documento) {

        File tempMerged = null;

        try {

            tempMerged = File.createTempFile("merged_" + documento, ".pdf");
            Document document = new Document();
            PdfCopy copy = new PdfCopy(document, new FileOutputStream(tempMerged));

            document.open();

            for (File file : tmpFiles) {

                InputStream inputstream = new FileInputStream(file);
                PdfReader reader = new PdfReader(inputstream);

                copy.addDocument(reader);
                copy.freeReader(reader);
                reader.close();
            }
            document.close();

        } catch (Exception e) {

            if (tempMerged != null) {
                if (tempMerged.exists()) {
                    tempMerged.delete();
                }
            }

            tempMerged = null;
        } finally {

            for (File file : tmpFiles) {

                if (file != null && file.exists()) {
                    file.delete();
                }

            }

        }

        return tempMerged;
    }

    private String fileToBase64(File file) {

        String encodedString = "";

        try {
            if (file != null) {
                byte[] fileContent = FileUtils.readFileToByteArray(file);
                encodedString = new String(Base64.encodeBase64(fileContent), "UTF-8");
            }
        } catch (Exception e) {
            encodedString = "";
        }

        return encodedString;
    }

    private void eliminaTemporales(ArrayList<TipoDocumentoDTO> temporales) {

        for (TipoDocumentoDTO archivo : temporales) {

            if (archivo.getArchivo() != null && archivo.getArchivo().exists()) {
                archivo.getArchivo().delete();
            }

        }

    }

    public String getFilesBase64(String jsonContenido) {

        ArrayList<File> archivostmp = new ArrayList<>();

        String base64File = "";

        File tmp = null;

        File fileTipoDocument = null;

        try {

            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(jsonContenido).getAsJsonObject();

            JsonArray urls = jsonObject.get("urls").getAsJsonArray();

            int tipoDocumento = jsonObject.get("idTipoDocumento").getAsInt();
            String documento = jsonObject.get("documento").getAsString();
            String economico = jsonObject.get("economico").getAsInt() + "";

            for (int i = 0; i < urls.size(); i++) {

                String urlImagen = urls.get(i).getAsString();

                if (urlImagen.contains(".jpg")
                        || urlImagen.contains(".jpeg")) {

                    tmp = convertImageToPdf(tipoDocumento,
                            documento,
                            economico,
                            i,
                            urls.get(i).getAsString());

                } else {

                    tmp = File.createTempFile("converted_" + tipoDocumento
                            + "_" + documento + "_" + economico + "_" + i, ".pdf");

                    FileUtils.copyURLToFile(new URL(urlImagen), tmp);

                }

                archivostmp.add(tmp);

            }

            if (archivostmp != null && archivostmp.size() > 1) {
                fileTipoDocument = mergePDFs(archivostmp, tipoDocumento);
            } else {
                fileTipoDocument = tmp;
            }

            base64File = fileToBase64(fileTipoDocument);

        } catch (Exception e) {
            base64File = "";
        } finally {

            if (fileTipoDocument != null) {

                if (fileTipoDocument.exists()) {
                    fileTipoDocument.delete();
                }

            }

        }

        return base64File;

    }

}
