package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.PaisNegocioDAO;
import com.gruposalinas.franquicia.domain.PaisNegocioDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PaisNegocioBI {

    private static Logger logger = LogManager.getLogger(PaisNegocioBI.class);

    @Autowired
    PaisNegocioDAO paisNegocioDAO;

    public List<PaisNegocioDTO> obtienePaisNegocio(String negocio, String pais) {

        List<PaisNegocioDTO> consulta = null;

        try {
            consulta = paisNegocioDAO.obtienePaisNegocio(negocio, pais);
        } catch (Exception e) {
            logger.info("No fue posible obtener informacion Negocio-Pais");
            UtilFRQ.printErrorLog(null, e);
        }

        return consulta;
    }

    public boolean insertaPaisNegocio(int negocio, int pais) {
        boolean respuesta = false;

        try {
            respuesta = paisNegocioDAO.insertaPaisNegocio(negocio, pais);
        } catch (Exception e) {
            logger.info("No fue posible insertar informacion Negocio-Pais");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaPaisNegocio(int negocio, int pais) {
        boolean respuesta = false;

        try {
            respuesta = paisNegocioDAO.eliminaPaisNegocio(negocio, pais);
        } catch (Exception e) {
            logger.info("No fue posible eliminar informacion Negocio-Pais");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

}
