package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.TareasDAOImpl;
import com.gruposalinas.franquicia.domain.TareaDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class TareasBI {

    private static Logger logger = LogManager.getLogger(TareasBI.class);

    private List<TareaDTO> listaTareas;

    @Autowired
    TareasDAOImpl tareaDAO;

    public boolean insertaTarea(TareaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = tareaDAO.insertaTarea(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar el registro de la Tarea");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean actualizaTarea(TareaDTO bean) {
        boolean respuesta = false;

        try {
            respuesta = tareaDAO.actualizaTarea(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el registro de la Tarea");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaTarea(int idTarea) {
        boolean respuesta = false;

        try {
            respuesta = tareaDAO.eliminaTarea(idTarea);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el registro de la Tarea");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public List<TareaDTO> obtieneTareas() {

        try {
            listaTareas = tareaDAO.obtieneTareas();
        } catch (Exception e) {
            logger.info("No fue posible obtener el registro de la Tarea");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaTareas;
    }

    public List<TareaDTO> obtieneTareasActivas() {

        try {
            listaTareas = tareaDAO.obtieneActivas();
        } catch (Exception e) {
            logger.info("No fue posible obtener el registro de la Tarea");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaTareas;
    }
}
