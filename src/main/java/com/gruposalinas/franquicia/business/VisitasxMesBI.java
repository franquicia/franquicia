package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.VisitasxMesDAO;
import com.gruposalinas.franquicia.domain.VisitasxMesDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class VisitasxMesBI {

    private Logger logger = LogManager.getLogger(VisitasxMesBI.class);

    @Autowired
    VisitasxMesDAO visitasxMesDAO;

    public List<VisitasxMesDTO> obtieneVisitas(int checklist, String fechaInicio, String fechaFin) {

        List<VisitasxMesDTO> lista = null;

        try {
            lista = visitasxMesDAO.obtieneVisitas(checklist, fechaInicio, fechaFin);
        } catch (Exception e) {

            logger.info("No fue posible obtener las visitas ");
            UtilFRQ.printErrorLog(null, e);
        }

        return lista;
    }

}
