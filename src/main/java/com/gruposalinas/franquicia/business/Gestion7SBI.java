package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.resources.FRQConstantes;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;

public class Gestion7SBI {

    private Logger logger = LogManager.getLogger(Gestion7SBI.class);

    public String obtieneProgramaLimpieza(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getDetalleProgLimpieza.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {

                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {

                    response.append(inputLine);

                }

                respuesta = response.toString();
                if (!respuesta.startsWith("{")) {
                    respuesta = null;
                }
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneDetalleProgLimp(int idPrograma) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getDetalleProgLimpiezaAreas.json?idUsuario=331952&idProgLimpieza=" + idPrograma;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneActivoFijo(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getDepuraActivos.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneCartaActFijo(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getCartasActivos.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(inputLine)));
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneGuiaDHL(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getGuiasDHL.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneBitMantenimiento(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getDetalleBitaMtto.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneFotoPlantilla(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getFotoPlantilla.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtienePlantillaCedula(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getPlantillaCedula.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;
        BufferedReader in = null;

        try {
            URL url = new URL(urlStr);
            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();
            if (responseCode != 200) {
                //
            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }
        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return respuesta;
    }

    public String obtieneDetalleCedula(String idUser) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getDetalleCedula.json?idUsuario=331952&idUsu=" + idUser;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;
    }

    public String obtieneActasMinucias(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getObtieneActasMinucias.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneRepAreasApoyo(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getFormatosAreasApuyo.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneDetalleApoyo(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getFormatosAreasApuyo.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String estandarizacionPuesto(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getDetalleEntandPuesto.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneEvidenciasFotograficas1(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getFotoAntesDespuesAllCECO.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtienePlantillaRev7S(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getPlantillaRevMet7S.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneReporteChecksSemana(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLFranquicia() + "servicios/getReporteChecksSemana.json?idUsuario=331952&ceco=" + ceco + "&idcheck=313";
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

    public String obtieneDetalleFormatoRev7S(String ceco) {

        String respuesta = null;
        String urlStr = FRQConstantes.getURLGestion() + "servicios/getDetalleFormato7S.json?idUsuario=331952&ceco=" + ceco;
        HttpURLConnection conexion = null;

        BufferedReader in = null;
        try {

            URL url = new URL(urlStr);

            conexion = (HttpURLConnection) url.openConnection();
            conexion.setRequestMethod("GET");
            conexion.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            int responseCode = conexion.getResponseCode();

            if (responseCode != 200) {

            } else {
                in = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

                String inputLine;

                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                respuesta = response.toString();
            }

        } catch (Exception e) {
            respuesta = null;
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        return respuesta;

    }

}
