package com.gruposalinas.franquicia.business;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.gruposalinas.franquicia.domain.CompromisoDTO;
import com.gruposalinas.franquicia.domain.UsuarioDTO;
import com.gruposalinas.franquicia.servicios.servidor.CorreosLotus;
import com.gruposalinas.franquicia.util.UtilDate;
import com.gruposalinas.franquicia.util.UtilMail;
import com.gruposalinas.franquicia.util.UtilObject;
import com.gruposalinas.franquicia.util.UtilResource;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;

public class CorreoBI {

    @Autowired
    RespuestaPdfBI respuestaPdfBI;

    private static final Logger logger = LogManager.getLogger(CorreoBI.class);

    private String titulo = "Alta de Checklist";
    private String NombreContacto = "Administracion Checklist";
    private String extContacto = "72911";
    private String celContacto = "1234567890";
    // private String mailContactoCesar = "cvidal@bancoazteca.com.mx";
    private String mailContacto = "cvidal@bancoazteca.com.mx";
    @SuppressWarnings("unused")
    private String mailPersonal = "jsepulveda@elektra.com.mx";
    @SuppressWarnings("unused")
    private String mailContactoAdmin = "sistemas@franquicia.com.mx";
    private List<String> copiados = null;

    public boolean sendMailAutorizacion(String idEmpleado, UsuarioDTO user, String nombreCheck) {

        copiados = new ArrayList<String>();
        Resource correoColab = null;
        CorreosLotus correo = null;

        try {
            // CORREO AL AUTORIZADOR
            //if (user.getIdUsuario().equals("99999999"))
            //user.setIdUsuario("664899");

            correoColab = UtilResource.getResourceFromInternet("mailSolicitud.html");

            try {
                logger.info("new CorreosLotus()");
                correo = new CorreosLotus();
                this.mailContacto = correo.validaUsuarioLotusCorreos("664899");
                logger.info("RETORNO VALIDA_USUARIO_LOTUS_CORREO" + this.mailContacto);
            } catch (Exception e) {
                logger.info(e.getMessage());
            }

            String strCorreo = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreo = strCorreo.replace("@idEmpleado", idEmpleado);
            strCorreo = strCorreo.replace("@NombreSocio", user.getNombre());
            strCorreo = strCorreo.replace("@Titulo", "Tu solicitud de alta del checklist:" + nombreCheck + ", ha sido enviada para su autorización");
            strCorreo = strCorreo.replaceAll("null", "Sin Dato");
            //EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo("cvidal@bancoazteca.com.mx", copiados, "Franquicia - Autorizacion: " + titulo, strCorreo, null);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }

        correoColab = UtilResource.getResourceFromInternet("mailNotificacion.html");
        try {

            try {
                correo = new CorreosLotus();
                this.mailContacto = correo.validaUsuarioLotusCorreos("664899");
                logger.info("RETORNO VALIDA_USUARIO_LOTUS_CORREO" + this.mailContacto);
            } catch (Exception e) {
                logger.info(e.getMessage());
            }

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@NombreSocio", user.getNombre());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "Tu solicitud de alta del checklist: " + nombreCheck + ", ha sido enviada para su autorización");
            strCorreoOwner = strCorreoOwner.replace("@NombreContacto", NombreContacto);
            strCorreoOwner = strCorreoOwner.replace("@extension", extContacto);
            strCorreoOwner = strCorreoOwner.replace("@telefono", celContacto);
            strCorreoOwner = strCorreoOwner.replace("@email", "");
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");

            //EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo("cvidal@bancoazteca.com.mx", copiados, "Franquicia - Solicitud: " + titulo, strCorreoOwner, null);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }

        return true;
    }

    public boolean sendMailAprobacion(boolean autori, UsuarioDTO user, String nombreCheck) {
        copiados = new ArrayList<String>();
        String titulo = "";
        if (autori) {
            titulo = "Autorizada";
        } else {
            titulo = "Rechazada";
        }

        //if (user.getIdUsuario().equals("99999999"))
        //user.setIdUsuario("664899");
        CorreosLotus correo = null;

        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailAutorizacion.html");

            try {
                logger.info("new CorreosLotus()");
                correo = new CorreosLotus();
                this.mailContacto = correo.validaUsuarioLotusCorreos("664899");
                logger.info("RETORNO VALIDA_USUARIO_LOTUS_CORREO" + this.mailContacto);
            } catch (Exception e) {
                logger.info(e.getMessage());
            }

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@NombreSocio", user.getNombre());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "Tu solicitud de Modificación del checklist:" + nombreCheck + ", ha sido " + titulo);
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");
            //EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo("cvidal@bancoazteca.com.mx", copiados, "Franquicia - Solicitud: " + titulo, strCorreoOwner, null);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;

    }

    public boolean sendMailAcuerdos(UsuarioDTO user, List<CompromisoDTO> listaCompromiso) throws DocumentException, IOException {
        //copiados.add(user.getNombre());
        //logger.info("copiados- " + copiados);
        copiados = new ArrayList<String>();
        String titulo = "";
        titulo = "Acuerdos Checklist Triunfo  -  Sucursal: " + user.getNombre();
        String html = "";
        String fechaCierre = "";
        String lon = "150";
        int lonPreg = 0;

        for (int i = 0; i < listaCompromiso.size(); i++) {
            fechaCierre = listaCompromiso.get(0).getFechaCompromiso();
            lonPreg = listaCompromiso.get(i).getDesPregunta().length() + listaCompromiso.get(i).getDescripcion().length();

            if (lonPreg > 500) {
                lon = "350";
            }

            html += "<tr>"
                    + "<td style='background-color:white; width:5%; height:" + lon + "px; border: #F0F0F0 25px solid; border-radius:20px;'>"
                    + "<div style='width:100%; height:" + lon + "px'>"
                    + "<table style='width:100%; align:center;'>"
                    + "<tr>"
                    + "<td style='border:white 1px solid; width:1300px;'>" + (i + 1) + ".- " + listaCompromiso.get(i).getDesPregunta() + "</td>"
                    + "<td style='color:red; border: white 1px solid; width:75px;'></td>"//Respuesta NO
                    + "</tr>"
                    + "<tr>"
                    + " <td colspan='10' style='border: white 5px solid;'>ACUERDO: " + listaCompromiso.get(i).getDescripcion() + "</td>"
                    + "</tr>"
                    + "</table>"
                    + "</div>"
                    + "</td>"
                    + "</tr>";
        }

        html = creaDocumento(html, false, null, fechaCierre);
        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailAcuerdos.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "");
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");
            strCorreoOwner = strCorreoOwner.replace("@Lista", html);

            CorreosLotus correosLotus = new CorreosLotus();
            String destinatario = "";
            String correoSucursal = "";

            List<String> docHtml = new ArrayList<String>();
            docHtml.add(html);

            logger.info("_______________________________ANTES DE ESCRIBIR PDF_______________________________");
            File adjunto = escribirPDFHtml(docHtml);
            logger.info("_______________________________DESPUES DE ESCRIBIR PDF_______________________________");

            try {
                destinatario = correosLotus.validaUsuarioLotusCorreos(user.getIdUsuario());
                correoSucursal = correosLotus.validaUsuarioLotusCorreos(user.getNombre());
                copiados.add(correoSucursal);
                copiados.add("amorales@elektra.com.mx");
                //copiados.add("jsepulveda@elektra.com.mx");
                logger.info("correo Sucursal - " + correoSucursal);
            } catch (Exception e) {
                logger.info("Algo ocurrió al consultar el correoLotus " + e);
                destinatario = "amorales@elektra.com.mx";
                correoSucursal = "sepulveda@elektra.com.mx";
            }

            //EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo(destinatario, copiados, "" + titulo, strCorreoOwner, adjunto);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;

    }

    //Va a recibir un list con el size de los documentos que se tienen que crear
    public File escribirPDFHtml(List<String> html) {
        File temp = null;
        try {
            //create a temp file
            temp = File.createTempFile("compromisos", ".pdf");
            FileOutputStream file = new FileOutputStream(temp);
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, file);
            document.open();

            java.util.Iterator<String> it = html.iterator();

            while (it.hasNext()) {
                String k = it.next();
                XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
                ByteArrayInputStream is = new ByteArrayInputStream(k.getBytes());
                worker.parseXHtml(writer, document, is);
                document.newPage();
            }
            document.close();
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    public boolean sendMailBuzon(UsuarioDTO user, String asunto, String idCeco, String puesto, String lugar, String telefono, String comentario, String copiadosParam) {

        copiados = new ArrayList<String>();
        String fechaPendientes = UtilDate.getSysDate("dd/MM/yyyy");
        String titulo = "";
        titulo = user.getNombre() + " - " + asunto;
        String arr[] = null;

        if (copiadosParam != "") {
            arr = copiadosParam.split(",");
            if (arr.length > 0) {
                for (int i = 0; i < arr.length; i++) {
                    copiados.add(arr[i]);
                }
            }
        }
        logger.info("copiados... " + copiados);

        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailBuzon.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "Buzón dudas o comentarios");
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");
            strCorreoOwner = strCorreoOwner.replaceAll("@Fecha", fechaPendientes);
            strCorreoOwner = strCorreoOwner.replaceAll("@Nombre", user.getNombre());
            strCorreoOwner = strCorreoOwner.replaceAll("@IdCeco", idCeco);
            strCorreoOwner = strCorreoOwner.replaceAll("@Puesto", puesto);
            strCorreoOwner = strCorreoOwner.replaceAll("@NoEmpleado", user.getIdUsuario());
            strCorreoOwner = strCorreoOwner.replaceAll("@Zona", lugar);
            strCorreoOwner = strCorreoOwner.replaceAll("@Telefono", telefono);
            strCorreoOwner = strCorreoOwner.replace("@Comentario", comentario);

            CorreosLotus correosLotus = new CorreosLotus();
            String destinatario = "";

            try {
                destinatario = correosLotus.validaUsuarioLotusCorreos(user.getIdUsuario());
                logger.info("correo destinatario - " + destinatario);
            } catch (Exception e) {
                logger.info("Algo ocurrió al consultar el correoLotus " + e);
                destinatario = "jsepulveda@elektra.com.mx";
            }

            //EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo(destinatario, copiados, "" + titulo, strCorreoOwner, null);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;

    }

    public boolean sendMailCompromisosChecklist(UsuarioDTO user, String ceco) throws DocumentException, IOException {
        logger.info("SI ENTRO!!");
        //copiados.add(user.getNombre());
        //logger.info("copiados- " + copiados);
        copiados = new ArrayList<String>();
        String titulo = "";
        titulo = "Acuerdos Checklist Triunfo  -  Sucursal: " + user.getNombre();
        String html = "";

        logger.info("DENTRO DE SENDMAILCOMPROMISOS " + "USER ---> " + user.getIdUsuario() + " ceco: " + ceco);

        JsonArray res = respuestaPdfBI.obtieneRespuestas(Integer.parseInt(user.getIdUsuario()), ceco);

        logger.info("size () " + res.size());
        //String n+"1" = "";
        List<String> docHtml = new ArrayList<String>();
        List<String> generales = null;

        for (int i = 0; i < res.size(); i++) {
            logger.info("LO QUE TRAE MI RESPUESTA" + res.get(i).toString());
            if (!res.get(i).toString().equals("{}")) {
                String data = llenaDocumento(res.get(i));
                generales = new ArrayList<String>();
                generales.add(res.get(i).getAsJsonObject().get("nombrecheck").toString());
                generales.add(res.get(i).getAsJsonObject().get("nombreCeco").toString());
                generales.add(res.get(i).getAsJsonObject().get("fechaInicio").toString());
                generales.add(res.get(i).getAsJsonObject().get("fechaFin").toString());
                generales.add(res.get(i).getAsJsonObject().get("gerente").toString());
                generales.add(res.get(i).getAsJsonObject().get("regional").toString());
                String htmlCheck = creaDocumento(data, true, generales, "");
                html += htmlCheck;
                docHtml.add(htmlCheck);
                System.out.println("DOCUMENTO " + i + "CREADO");
                System.out.println(htmlCheck);
                System.out.println("********************************************************");
            }
        }

        try {
            Resource correoColab = UtilResource.getResourceFromInternet("mailAcuerdos.html");

            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
            strCorreoOwner = strCorreoOwner.replace("@Titulo", "");
            strCorreoOwner = strCorreoOwner.replaceAll("null", "Sin Dato");
            strCorreoOwner = strCorreoOwner.replace("@Lista", html);

            CorreosLotus correosLotus = new CorreosLotus();
            String destinatario = "";
            String correoSucursal = "";

            logger.info("_______________________________ANTES DE ESCRIBIR PDF_______________________________");
            //Ahora se le enviara un list con el size de los checklist a escribir
            File adjunto = escribirPDFHtml(docHtml);
            logger.info("_______________________________DESPUES DE ESCRIBIR PDF_______________________________");

            try {
                destinatario = correosLotus.validaUsuarioLotusCorreos(user.getIdUsuario());
                correoSucursal = correosLotus.validaUsuarioLotusCorreos(user.getNombre());
                logger.info("DESTINATARIO ----- >>>>" + destinatario);
                logger.info("CORREO SUCURSAL ----- >>>>" + correoSucursal);
                logger.info("CECO QUE ESTOY TRAYENDO ----- >>>>" + ceco);

                copiados.add(correoSucursal);
                copiados.add("amorales@elektra.com.mx");
                //copiados.add("cvidal@bancoazteca.com.mx");
                //copiados.add("jsepulveda@elektra.com.mx");
                logger.info("correo Sucursal - " + correoSucursal);
            } catch (Exception e) {
                logger.info("Algo ocurrió al consultar el correoLotus " + e);
                destinatario = "amorales@elektra.com.mx";
                correoSucursal = "jsepulveda@elektra.com.mx";
            }

            //EXISTE UN ERROR AL OBTENER LOS DATOS DEL USUARIO VALIDA_USUARIO_LOTUS_CORREO
            UtilMail.enviaCorreo(destinatario, copiados, "" + titulo, strCorreoOwner, adjunto);

        } catch (IOException e) {
            logger.info("Algo ocurrió... " + e);
        }
        return true;

    }

    //Se agrega la fecha para los acuerdos, dado que necesitan este parametro
    @SuppressWarnings("unused")
    public String creaDocumento(String params, boolean bandera, List<String> generales, String fecha) {
        String html = "";
        String fechaPendientes = UtilDate.getSysDate("dd/MM/yyyy");

        if (bandera) {
            html += "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><html>"
                    + "<style> </style>"
                    + "<body>"
                    + "<div id='generalContainer' style='width:100%; background:#F0F0F0; height:100%; margin:0 auto; border:1px solid #E4E4E4;'>"
                    + "<div id='tituloContainer' style='width:100%; height:10%;  background:#F0F0F0; margin-left:0px;'>"
                    + "<h3 id='checklist' style='background:F0F0F0; text-align:center; color:#929292; font-family:Arial, Helvetica, sans-serif; font-weight: 100;'>" + generales.get(0).toString().replace("\"", "") + "<br></br>" + generales.get(1).toString().replace("\"", "") + "</h3>"
                    + "</div>";

            html += "<div id='generalesContanier' style='width:100%; height:11%; background-color:#FFFFFF; margin:0 auto; align-text:center; border:1px solid #E4E4E4; border-radius: 25px;'>"
                    + "<div id='observacionesContainer' style='width:95%; height:100%; background-color:#FFFFFF; margin:0 auto; align-text:center;'>"
                    + "<p>Fecha Inicio: " + generales.get(2).toString().replace("\"", "") + ""
                    + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    + " Fecha Fin:	" + generales.get(3).toString().replace("\"", "") + "  </p>"
                    + "<p>Gerente: " + generales.get(4).toString().replace("\"", "") + "</p>"
                    + "<p>Regional: " + generales.get(5).toString().replace("\"", "") + "</p>"
                    + "</div>"
                    + "</div>"
                    + "<br></br>"
                    + "<div id='observacionesContainer' style='width:100%; height:9%; background-color:#FFFFFF; margin:0 auto; align-text:center; border:1px solid #E4E4E4; border-radius: 25px;'>"
                    + "<div id='observacionesContainer' style='width:95%; height:100%; background-color:#FFFFFF; margin:0 auto; align-text:center;'>"
                    + "	<p>Observaciones:</p>"
                    + "</div>"
                    + "</div>"
                    + "<br></br>";
        } else {
            html += "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'><html>"
                    + "<style> </style>"
                    + "<body>"
                    + "<div id='generalContainer' style='width:100%; background:#F0F0F0; height:100%; margin:0 auto; border:1px solid #E4E4E4;'>"
                    + "<div id='tituloContainer' style='width:100%; height:10%;  background:#F0F0F0; margin-left:0px;'>"
                    + "<h3 id='checklist' style='background:F0F0F0; text-align:center; color:#929292; font-family:Arial, Helvetica, sans-serif; font-weight: 100;'>Acuerdos Checklist Apertura<br></br>" + fecha.split(" ")[0] + "</h3>"
                    + "</div>";
        }

        html += "<div id='detalleContainer' style='width:100%; height:6%; background-color:#FFFFFF; margin:0 auto; align-text:center; border:1px solid #E4E4E4; border-radius: 25px;'>"
                + "<div id='detalleTextoContainer' style='width:95%; height:100%; background-color:#FFFFFF; margin:0 auto; align-text:center;'>"
                + "	<p style='text-align:center;'>A continuaci&oacute;n se presentan los reactivos a corregirlos, acci&oacute;n y fecha Recuerda mantener correctamente los reactivos que tuviste bien en el Check List</p>"
                + "</div>"
                + "</div>"
                + "<br></br>"
                + "<div id='respuestasContanier' style='width:100%; height:100%; background:#F0F0F0; margin:0 auto; align-text:center;'>"
                + "<div id='dataRespuestasContainer' style='width:100%; height:100%; background:#F0F0F0; margin:0 auto; align-text:center;'>"
                + "<table style='background-color:#F0F0F0; width:100%;'>";

        html += params;

        html += "</table></div></div></div></body></html>";

        return html;
    }

    public String llenaDocumento(JsonElement listaCompromiso) {
        String html = "";
        String interno = "";
        String motivo = "";

        logger.info("LO QUE TRAEN MIS COMPROMISOS!!!" + listaCompromiso);

        JsonArray data = listaCompromiso.getAsJsonObject().get("preguntas").getAsJsonArray();

        if (!data.toString().equals("[]")) {
            for (int i = 0; i < data.size(); i++) {
                String preguntaPadre = data.get(i).getAsJsonObject().get("preguntaPadre").toString().replace("\"", "");
                String respuestaPadre = data.get(i).getAsJsonObject().get("respuestaPadre").toString().replace("\"", "");

                html += "<tr>"
                        + "<td style='background-color:white; width:5%; height:150px; border: #F0F0F0 25px solid; border-radius:20px;'>"
                        + "<div style='width:100%; height:150px; background-color:white;'>"
                        + "<table style='width:100%; height:150px; align:center; background-color:white;'>"
                        + "<tr>"
                        + "<td style='border:white 1px solid; width:1400px; background-color:white;'>" + (i + 1) + ".-" + preguntaPadre.toString().replace("\"", "") + "</td>"
                        + "<td style='color:red; border: white 1px solid; width:75px; height:15px; background-color:white;'>" + respuestaPadre.toString().replace("\"", "") + "</td>"//Respuesta NO
                        + "</tr>";

                JsonArray motivos = data.get(i).getAsJsonObject().get("respuestas").getAsJsonArray();

                interno = "";
                motivo = "";
                for (int j = 0; j < motivos.size(); j++) {
                    motivo += motivos.get(j).getAsJsonObject().get("pregunta").toString().replace("\"", "") + ":" + motivos.get(j).getAsJsonObject().get("respuesta").toString().replace("\"", "") + "<br></br>";
                }
                html += "<tr><td colspan='10' style='border: white 5px solid; background-color:white;'>" + motivo + "</td>"
                        + "</tr>" + interno
                        + "</table>"
                        + "</div>"
                        + "</td>"
                        + "</tr>";
            }

            /*System.out.println(res.get(i).getAsJsonObject().get("idCheck"));
				JsonArray res2 = res.get(i).getAsJsonObject().get("preguntas").getAsJsonArray();
				System.out.println(res2.size());
				System.out.println(res2.get(0).toString());
				System.out.println(res2.get(0).getAsJsonObject().get("preguntaPadre"));
				System.out.println(res2.get(0).getAsJsonObject().get("respuestas").getAsJsonArray().get(i).getAsJsonObject().get("pregunta"));
             */
 /*
				for (int i = 0; i < listaCompromiso.size(); i++) {
					html += "<tr>"
					+"<td style='background-color:white; width:5%; height:150px; border: #F0F0F0 25px solid; border-radius:20px;'>"
						+ "<div style='width:100%; height:150px;'>"
							+"<table style='width:100%; align:center;'>"
		    						+"<tr>"
									  +"<td style='border:white 1px solid; width:1300px;'>"+(i+1)+".- "+listaCompromiso.get(i).getDesPregunta()+"</td>"
									  +"<td style='color:red; border: white 1px solid; width:75px;'></td>"//Respuesta NO
									+"</tr>"
		    						
		    						+"<tr>"
										+" <td colspan='10' style='border: white 5px solid;'>ACUERDO: "+listaCompromiso.get(i).getDescripcion()+"</td>"
									+"</tr>"
							+"</table>"
						+"</div>"
					+ "</td>"	
					+"</tr>";
				}
             */
        } else {
            html += "<tr>"
                    + "<td style='background-color:white; width:5%; height:150px; border: #F0F0F0 25px solid; border-radius:20px;'>"
                    + "<div style='width:100%; height:150px;'>"
                    + "<table style='width:100%; align:center;'>"
                    + "<tr>"
                    + "<td style='border:white 1px solid; width:1300px;'></td>"
                    + "<td style='color:red; border: white 1px solid; width:75px;'></td>"//Respuesta NO
                    + "</tr>"
                    + "<tr><td colspan='10' style='border: white 5px solid;'></td>"
                    + "</tr>"
                    + "</table>"
                    + "</div>"
                    + "</td>"
                    + "</tr>";
        }
        return html;

    }
}
