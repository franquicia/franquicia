package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.SoportePDDAO;
import org.springframework.beans.factory.annotation.Autowired;

public class SoportePDBI {

    @Autowired
    SoportePDDAO soportePD;

    public String insertCecoInfo(int op, int idCeco, int numSucursal, String nombre, int cecoPadre, int pais, int estado, int municipio, String calle, int nivelC, int negocio, int canal, int responsable, String fccp, int geografia,
            String estado2, String municipio2, String colonia, String numExterior, String numInterior, int activo) {
        String resp = soportePD.insertCecoInfo(op, idCeco, numSucursal, nombre, cecoPadre, pais, estado, municipio, calle, nivelC, negocio, canal, responsable, fccp, geografia, estado2, municipio2, colonia, numExterior, numInterior, activo);
        return resp;
    }

}
