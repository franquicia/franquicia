package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.PaisDAO;
import com.gruposalinas.franquicia.domain.PaisDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PaisBI {

    private static Logger logger = LogManager.getLogger(PaisBI.class);

    @Autowired
    PaisDAO paisDAO;

    List<PaisDTO> listaPaises = null;
    List<PaisDTO> listaPais = null;

    public List<PaisDTO> obtienePais() {
        try {
            listaPaises = paisDAO.obtienePais();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Pais");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaPaises;
    }

    public List<PaisDTO> obtienePaisVisualizador() {
        try {
            listaPaises = paisDAO.obtienePaisVisualizador();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Pais para el Visualizador");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaPaises;
    }

    public List<PaisDTO> obtienePais(int idPais) {
        try {
            listaPais = paisDAO.obtienePais(idPais);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Pais");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaPais;
    }

    public int insertaPais(PaisDTO pais) {
        int idPais = 0;
        try {
            idPais = paisDAO.insertaPais(pais);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Pais");
            UtilFRQ.printErrorLog(null, e);
        }

        return idPais;
    }

    public boolean actualizaPais(PaisDTO pais) {
        boolean respuesta = false;
        try {
            respuesta = paisDAO.actualizaPais(pais);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Pais");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaPais(int idPais) {
        boolean respuesta = false;
        try {
            respuesta = paisDAO.eliminaPais(idPais);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Pais");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }
}
