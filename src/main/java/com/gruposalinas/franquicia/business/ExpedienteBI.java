package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.ExpedienteDAO;
import com.gruposalinas.franquicia.domain.ExpedientesDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ExpedienteBI {

    private static Logger logger = LogManager.getLogger(CanalBI.class);

    @Autowired
    ExpedienteDAO expedienteDAO;

    List<ExpedientesDTO> listaExpedientes = null;
    List<ExpedientesDTO> listaResponsables = null;

    public List<ExpedientesDTO> buscaExpediente(String idExpediente, String idEstatus, String idCeco, String idUsuario, String fechaI, String fechaF, String tipoActa) {

        try {
            listaExpedientes = expedienteDAO.buscaExpediente(idExpediente, idEstatus, idCeco, idUsuario, fechaI, fechaF, tipoActa);
        } catch (Exception e) {
            logger.info("No fue posible consultar el Expediente");
        }

        return listaExpedientes;
    }

    public List<ExpedientesDTO> buscaResponsable(String idExpediente, String idEstatus, String idUsuario) {

        try {
            listaResponsables = expedienteDAO.buscaResponsable(idExpediente, idEstatus, idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible consultar los Responsables");
        }

        return listaResponsables;
    }

    public int insertaExpediente(ExpedientesDTO bean) {

        int respuesta = 0;

        try {
            respuesta = expedienteDAO.insertaExpediente(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Expediente");
        }

        return respuesta;
    }

    public int insertaExpediente(ExpedientesDTO bean, int tipoActa) {

        int respuesta = 0;

        try {
            respuesta = expedienteDAO.insertaExpediente(bean, tipoActa);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Expediente");
        }

        return respuesta;
    }

    public boolean insertaResponsable(ExpedientesDTO bean) {

        boolean respuesta = false;

        try {
            respuesta = expedienteDAO.insertaResponsable(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Responsable");
        }

        return respuesta;
    }

    public boolean actualizaEstatus(int estatus, int idExpediente) {

        boolean respuesta = false;

        try {
            respuesta = expedienteDAO.actualizaEstatus(estatus, idExpediente);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el estatus");
        }

        return respuesta;
    }

    public boolean eliminaExpediente(int idExpediente) {

        boolean respuesta = false;

        try {
            respuesta = expedienteDAO.eliminaExpediente(idExpediente);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el Expediente");
        }

        return respuesta;
    }

    public boolean eliminaResponsables(int idExpediente) {

        boolean respuesta = false;

        try {
            respuesta = expedienteDAO.eliminaResponsables(idExpediente);
        } catch (Exception e) {
            logger.info("No fue posible eliminar a los Responsables");
        }

        return respuesta;
    }
}
