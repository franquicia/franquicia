package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.ArbolDecisionDAO;
import com.gruposalinas.franquicia.dao.ChecklistModificacionesDAO;
import com.gruposalinas.franquicia.domain.ArbolDecisionDTO;
import com.gruposalinas.franquicia.domain.ChecklistActualDTO;
import com.gruposalinas.franquicia.domain.ChecklistModificacionesDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ArbolDecisionBI {

    private static Logger logger = LogManager.getLogger(ArbolDecisionBI.class);

    List<ArbolDecisionDTO> listaArbolDecision = null;

    @Autowired
    ArbolDecisionDAO arbolDecisionDAO;
    @Autowired
    ChecklistModificacionesDAO checklistModificacionesDAO;

    public int insertaArbolDecision(ArbolDecisionDTO bean) {

        int respuesta = 0;

        try {

            respuesta = arbolDecisionDAO.insertaArbolDecision(bean);

        } catch (Exception e) {
            logger.info("No fue posible insertar el registro del Arbol de Decision");
            UtilFRQ.printErrorLog(null, e);

        }

        return respuesta;
    }

    public boolean actualizaArbolDecision(ArbolDecisionDTO bean) {

        boolean respuesta = false;

        try {

            respuesta = arbolDecisionDAO.actualizaArbolDecision(bean);

        } catch (Exception e) {
            logger.info("No fue posible actualizarel registro del Arbol de Decision");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;

    }

    public boolean eliminaArbolDecision(int idArbolDecision) {

        boolean respuesta = false;

        try {
            respuesta = arbolDecisionDAO.eliminaArbolDecision(idArbolDecision);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el registro del Arbol de Decision");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;

    }

    public List<ArbolDecisionDTO> buscaArbolDecision(int idChecklist) {

        try {
            listaArbolDecision = arbolDecisionDAO.buscaArbolDecision(idChecklist);
        } catch (Exception e) {
            logger.info("No fue posible obtener el Arbol de Decision");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaArbolDecision;

    }

    @SuppressWarnings("unchecked")
    public List<ArbolDecisionDTO> buscaArbolModificaciones(int idChecklist) {

        try {
            listaArbolDecision = new ArrayList<ArbolDecisionDTO>();
            Map<String, Object> respuesta = checklistModificacionesDAO.obtieneChecklist(idChecklist);

            List<ChecklistModificacionesDTO> checklistModificaciones = (List<ChecklistModificacionesDTO>) respuesta.get("checklistModificaciones");
            List<ChecklistActualDTO> checklistActual = (List<ChecklistActualDTO>) respuesta.get("checklistActual");

            if (checklistModificaciones.size() > 0) {
                System.out.println("TENGO MODIFICACIONES!!!! " + checklistModificaciones.size());
                int cont = 0;

                while (cont < checklistModificaciones.size()) {

                    ArbolDecisionDTO arbol = new ArbolDecisionDTO();
                    arbol.setIdArbolDesicion(checklistModificaciones.get(cont).getIdArbol());
                    arbol.setIdCheckList(checklistModificaciones.get(cont).getIdChecklist());
                    arbol.setIdPregunta(checklistModificaciones.get(cont).getIdPregunta());
                    arbol.setRespuesta(String.valueOf(checklistModificaciones.get(cont).getIdPosible()));
                    arbol.setEstatusEvidencia(checklistModificaciones.get(cont).getEstatusEvidencia());
                    arbol.setOrdenCheckRespuesta(checklistModificaciones.get(cont).getSiguientePregunta());
                    arbol.setReqAccion(checklistModificaciones.get(cont).getRequiereAccion());
                    arbol.setReqObservacion(checklistModificaciones.get(cont).getRequiereObsv());
                    arbol.setDescEvidencia(checklistModificaciones.get(cont).getEtiquetaEvidencia());

                    listaArbolDecision.add(arbol);

                    cont++;
                }

            } else {

                int cont = 0;

                while (cont < checklistActual.size()) {

                    ArbolDecisionDTO arbol = new ArbolDecisionDTO();
                    arbol.setIdArbolDesicion(checklistActual.get(cont).getIdArbol());
                    arbol.setIdCheckList(checklistActual.get(cont).getIdChecklist());
                    arbol.setIdPregunta(checklistActual.get(cont).getIdPregunta());
                    arbol.setRespuesta(String.valueOf(checklistActual.get(cont).getIdPosible()));
                    arbol.setEstatusEvidencia(checklistActual.get(cont).getEstatusEvidencia());
                    arbol.setOrdenCheckRespuesta(checklistActual.get(cont).getSiguientePregunta());
                    arbol.setReqAccion(checklistActual.get(cont).getRequiereAccion());
                    arbol.setReqObservacion(checklistActual.get(cont).getRequiereObsv());
                    arbol.setDescEvidencia(checklistActual.get(cont).getEtiquetaEvidencia());

                    listaArbolDecision.add(arbol);
                    cont++;
                }

            }

        } catch (Exception e) {
            logger.info("No fue posible obtener el Arbol de Decision");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaArbolDecision;
    }

    public boolean cambiaRespuestasPorId() {
        boolean res = false;

        try {
            res = arbolDecisionDAO.cambiaRespuestasPorId();
        } catch (Exception e) {
            logger.info("No fue posible cambiar las respuestas por Ids Posibles");
            UtilFRQ.printErrorLog(null, e);

        }

        return res;
    }

}
