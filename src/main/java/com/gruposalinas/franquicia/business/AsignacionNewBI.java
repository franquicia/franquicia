package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.AsignacionNewDAO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class AsignacionNewBI {

    @Autowired
    AsignacionNewDAO asignacionNewDAO;

    private Logger logger = LogManager.getLogger(AsignacionNewBI.class);

    public Map<String, Object> ejecutaAsignaciones() {

        Map<String, Object> respuesta = null;

        try {
            respuesta = asignacionNewDAO.ejecutaAsignaciones();
        } catch (Exception e) {
            logger.info("Ocurrio un error al ejecutar las asignaciones");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public Map<String, Object> asignacionEspecial(String userNew, String userOld) {

        Map<String, Object> respuesta = null;

        try {
            respuesta = asignacionNewDAO.asignacionEspecial(userNew, userOld);
        } catch (Exception e) {
            logger.info("Ocurrio un error al ejecutar la asignacion especial");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public Map<String, Object> ejecutaAsignacion(String ceco, int puesto, int checklist) {

        Map<String, Object> respuesta = null;

        try {
            respuesta = asignacionNewDAO.ejecutaAsignacion(ceco, puesto, checklist);
        } catch (Exception e) {
            logger.info("Ocurrio un error al ejecutar la asignacion");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

}
