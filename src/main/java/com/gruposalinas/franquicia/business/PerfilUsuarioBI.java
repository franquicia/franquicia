package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.PerfilUsuarioDAO;
import com.gruposalinas.franquicia.domain.PerfilUsuarioDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PerfilUsuarioBI {

    private static Logger logger = LogManager.getLogger(PerfilBI.class);

    @Autowired
    PerfilUsuarioDAO perfilUsuarioDAO;

    public List<PerfilUsuarioDTO> obtienePerfiles(String idUsuario, String idPerfil) {

        List<PerfilUsuarioDTO> listaPerfiles = null;

        try {
            listaPerfiles = perfilUsuarioDAO.obtienePerfiles(idUsuario, idPerfil);
        } catch (Exception e) {
            logger.info("No fue posible obtener los perfiles");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaPerfiles;
    }

    public boolean insertaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) {

        boolean respuesta = false;

        try {
            respuesta = perfilUsuarioDAO.insertaPerfilUsuario(perfilUsuarioDTO);
        } catch (Exception e) {
            logger.info("No fue posible insertar el perfil");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;

    }

    public boolean actualizaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) {

        boolean respuesta = false;

        try {
            respuesta = perfilUsuarioDAO.actualizaPerfilUsuario(perfilUsuarioDTO);
        } catch (Exception e) {
            logger.info("No fue posible actualziar el perfil");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaPerfilUsaurio(PerfilUsuarioDTO perfilUsuarioDTO) {
        boolean respuesta = false;

        try {
            respuesta = perfilUsuarioDAO.eliminaPerfilUsaurio(perfilUsuarioDTO);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el perfil");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }
}
