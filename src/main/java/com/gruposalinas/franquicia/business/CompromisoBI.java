package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.CompromisoDAO;
import com.gruposalinas.franquicia.domain.CompromisoDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CompromisoBI {

    private static Logger logger = LogManager.getLogger(CompromisoBI.class);

    @Autowired
    CompromisoDAO compromisoDAO;

    List<CompromisoDTO> listaCompromisos = null;
    List<CompromisoDTO> listaCompromiso = null;

    public List<CompromisoDTO> obtieneCompromiso() {

        try {
            listaCompromisos = compromisoDAO.obtieneCompromiso();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Compromiso");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaCompromisos;
    }

    public List<CompromisoDTO> obtieneCompromiso(int idCompromiso) {

        try {
            listaCompromiso = compromisoDAO.obtieneCompromiso(idCompromiso);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Compromiso");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaCompromiso;
    }

    public int insertaCompromiso(CompromisoDTO compromiso) {

        int idCompromiso = 0;

        try {
            idCompromiso = compromisoDAO.insertaCompromiso(compromiso);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Compromiso");
            UtilFRQ.printErrorLog(null, e);
        }

        return idCompromiso;
    }

    public boolean actualizaCompromiso(CompromisoDTO compromiso) {

        boolean respuesta = false;

        try {
            respuesta = compromisoDAO.actualizaCompromiso(compromiso);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Compromiso");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaCompromiso(int idCompromiso) {

        boolean respuesta = false;

        try {
            respuesta = compromisoDAO.eliminaCompromiso(idCompromiso);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Compromiso");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }
}
