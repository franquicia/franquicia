package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.HorarioDAO;
import com.gruposalinas.franquicia.domain.HorarioDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class HorarioBI {

    private static Logger logger = LogManager.getLogger(HorarioBI.class);

    @Autowired

    HorarioDAO horarioDAO;

    List<HorarioDTO> listaHorarios = null;
    List<HorarioDTO> listaHorario = null;

    public List<HorarioDTO> obtieneHorario() {
        try {
            listaHorarios = horarioDAO.obtieneHorario();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Horarios");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaHorarios;
    }

    public List<HorarioDTO> obtienehorario(int idHorario) {
        try {
            listaHorario = horarioDAO.obtenerHorario(idHorario);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Horarios");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaHorario;
    }

    public int insertaHorario(HorarioDTO horario) {
        int idHorario = 0;
        try {
            idHorario = horarioDAO.insertaHorario(horario);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Horario");
            UtilFRQ.printErrorLog(null, e);
        }

        return idHorario;
    }

    public boolean actualizaHorario(HorarioDTO horario) {

        boolean respuesta = false;
        try {
            respuesta = horarioDAO.actualizaHorario(horario);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Horario");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaHorario(int idHorario) {
        boolean respuesta = false;
        try {
            respuesta = horarioDAO.eliminaHorario(idHorario);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Horario");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }
}
