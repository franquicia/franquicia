package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.SucursalDAO;
import com.gruposalinas.franquicia.domain.SucursalDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class SucursalBI {

    private static Logger logger = LogManager.getLogger(SucursalBI.class);

    @Autowired
    SucursalDAO sucursalDAO;

    List<SucursalDTO> listaSucursales = null;
    List<SucursalDTO> listaSucursal = null;

    public List<SucursalDTO> obtieneSucursal() {
        try {
            listaSucursales = sucursalDAO.obtieneSucursal();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Sucursales");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaSucursales;
    }

    public List<SucursalDTO> obtieneSucursal(String idSucursal) {
        try {
            listaSucursal = sucursalDAO.obtieneSucursal(idSucursal);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Sucursal");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaSucursal;
    }

    public List<SucursalDTO> obtieneSucursalPaso(String idSucursal) {
        try {
            listaSucursal = sucursalDAO.obtieneSucursalPaso(idSucursal);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Sucursal");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaSucursal;
    }

    public int insertaSucursal(SucursalDTO sucursal) {
        int idSucursal = 0;

        try {
            idSucursal = sucursalDAO.insertaSucursal(sucursal);
        } catch (Exception e) {
            logger.info("No fue posible insertar la Sucursal");
            UtilFRQ.printErrorLog(null, e);
        }

        return idSucursal;
    }

    public boolean cargaSucursales() {

        boolean respuesta = false;

        try {
            respuesta = sucursalDAO.cargaSucursales();
        } catch (Exception e) {
            logger.info("No fue posible realizar la Carga de las Sucursales");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean actualizaSucursal(SucursalDTO sucursal) {
        boolean respuesta = false;
        try {
            respuesta = sucursalDAO.actualizaSucursal(sucursal);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la Sucursal");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaSucursal(int idSucursal) {
        boolean respuesta = false;
        try {
            respuesta = sucursalDAO.eliminaSucursal(idSucursal);
        } catch (Exception e) {
            logger.info("No fue posible borrar la Sucursal");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    //Sucursal GCC
    public List<SucursalDTO> obtieneSucursalGCC(String idSucursal) {
        try {
            listaSucursal = sucursalDAO.obtieneSucursalGCC(idSucursal);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Sucursal");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaSucursal;
    }

    public int insertaSucursalGCC(SucursalDTO sucursal) {
        int idSucursal = 0;

        try {
            idSucursal = sucursalDAO.insertaSucursalGCC(sucursal);
        } catch (Exception e) {
            logger.info("No fue posible insertar la Sucursal");
            UtilFRQ.printErrorLog(null, e);
        }

        return idSucursal;
    }

    public boolean actualizaSucursalGCC(SucursalDTO sucursal) {
        boolean respuesta = false;
        try {
            respuesta = sucursalDAO.actualizaSucursalGCC(sucursal);
        } catch (Exception e) {
            logger.info("No fue posible actualizar la Sucursal");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaSucursalGCC(int idSucursal) {
        boolean respuesta = false;
        try {
            respuesta = sucursalDAO.eliminaSucursalGCC(idSucursal);
        } catch (Exception e) {
            logger.info("No fue posible borrar la Sucursal");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

}
