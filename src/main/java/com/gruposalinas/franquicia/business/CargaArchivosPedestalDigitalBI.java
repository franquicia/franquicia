package com.gruposalinas.franquicia.business;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.gruposalinas.franquicia.dao.CargaArchivosPedestalDAO;
import com.gruposalinas.franquicia.dao.EstatusGeneralDAO;
import com.gruposalinas.franquicia.domain.BusquedaCECOsDTO;
import com.gruposalinas.franquicia.domain.BusquedaGeoDTO;
import com.gruposalinas.franquicia.domain.CategoriasPDDTO;
import com.gruposalinas.franquicia.domain.CecosWSPDDTO;
import com.gruposalinas.franquicia.domain.DocCorruptosPDDTO;
import com.gruposalinas.franquicia.domain.DocInCategoriaPDDTO;
import com.gruposalinas.franquicia.domain.DocInCategoryPDDTO;
import com.gruposalinas.franquicia.domain.DocsBeforeUploadPDDTO;
import com.gruposalinas.franquicia.domain.DoctoDataPDDTO;
import com.gruposalinas.franquicia.domain.FileChecksumPDDTO;
import com.gruposalinas.franquicia.domain.FileDataPDDTO;
import com.gruposalinas.franquicia.domain.FileWithoutChecksumPDDTO;
import com.gruposalinas.franquicia.domain.FolioDataPDDTO;
import com.gruposalinas.franquicia.domain.FolioFilterDataPDDTO;
import com.gruposalinas.franquicia.domain.GeoDataPDDTO;
import com.gruposalinas.franquicia.domain.ListaCECOXGEOPDDTO;
import com.gruposalinas.franquicia.domain.ListaCECOsPDDTO;
import com.gruposalinas.franquicia.domain.ListaCategoriasPDDTO;
import com.gruposalinas.franquicia.domain.ListaCecoPorCSVPDDTO;
import com.gruposalinas.franquicia.domain.ListaDistribucionPDDTO;
import com.gruposalinas.franquicia.domain.ParamCecoPDDTO;
import com.gruposalinas.franquicia.domain.TabletDataForExcelPDDTO;
import com.gruposalinas.franquicia.domain.TabletDataPDDTO;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class CargaArchivosPedestalDigitalBI {

    @Autowired
    CargaArchivosPedestalDAO cargaArchivo;

    @Autowired
    EstatusGeneralBI estatusGeneralBI;

    @Autowired
    EstatusGeneralDAO estatusGeneral;

    private static Logger logger = LogManager.getLogger(CargaArchivosPedestalDigitalBI.class);

    private String PATH_PROD = "http://10.53.33.82/franquicia/pedestal_digital/cargas/";
    private String PATH_DESA = "http://localhost:8888/franquicia/pedestal_digital/cargas/";

    List<BusquedaGeoDTO> list = null;
    List<BusquedaCECOsDTO> listCecos = null;
    List<ListaDistribucionPDDTO> listDistribucion = null;
    List<ListaCECOsPDDTO> listasCecos = null;
    List<ListaCecoPorCSVPDDTO> listaCecosPorCSV = null;
    List<ListaCECOXGEOPDDTO> listaCecosPorGeo = null;
    List<ListaCategoriasPDDTO> listaCategorias = null;

    String respuesta = "";

    public List<BusquedaGeoDTO> geolocaclizacion(int id_opcion, String id_geo, int id_tipo, int negocio) throws Exception {
        list = cargaArchivo.busquedaGeolocalizacion(id_opcion, id_geo, id_tipo, negocio);
        return list;
    }

    public List<BusquedaCECOsDTO> cecos(int id_opcion, String id_ceco, int id_tipo, int negocio) throws Exception {
        listCecos = cargaArchivo.busquedaCECOs(id_opcion, id_ceco, id_tipo, negocio);
        return listCecos;
    }

    public List<ListaDistribucionPDDTO> consultaListaDistribucion(int idLista, int id_usuario, int negocio) throws Exception {
        listDistribucion = cargaArchivo.consultaListaDistribucion(idLista, id_usuario, negocio);
        return listDistribucion;
    }

    public List<ListaDistribucionPDDTO> insertDistribucion(String nombre, int id_usuario, int negocio) throws Exception {
        listDistribucion = cargaArchivo.insertDistribucion(nombre, id_usuario, negocio);
        return listDistribucion;
    }

    public String actualizaDistribucion(int id_lista, String nombre, int id_usuario, int negocio, int id_activo) throws Exception {
        respuesta = cargaArchivo.actualizaDistribucion(id_lista, nombre, id_usuario, negocio, id_activo);
        return respuesta;
    }

    public List<ListaCECOsPDDTO> consultaListaCECOS(int idLista, String id_ceco) throws Exception {
        listasCecos = cargaArchivo.consultaListaCECOS(idLista, id_ceco);
        return listasCecos;
    }

    public List<ListaCECOsPDDTO> insertaListaCECOS(int idLista, String id_ceco) throws Exception {
        listasCecos = cargaArchivo.insertaListaCECOS(idLista, id_ceco);
        return listasCecos;
    }

    public List<ListaCECOsPDDTO> actualizaListaCECOS(int id_listaCecos, int idLista, String id_ceco, int id_activo) throws Exception {
        listasCecos = cargaArchivo.actualizaListaCECOS(id_listaCecos, idLista, id_ceco, id_activo);
        return listasCecos;
    }

    public List<ListaCecoPorCSVPDDTO> obtieneCecosPorCSV(String csvList, String negocio) throws Exception {
        listaCecosPorCSV = cargaArchivo.obtieneCecosPorCSV(csvList, negocio);
        return listaCecosPorCSV;
    }

    public List<ListaCECOXGEOPDDTO> obtieneCecoConGeo(int op, String geo, int tipo, int negocio) throws Exception {
        listaCecosPorGeo = cargaArchivo.obtieneCecoConGeo(op, geo, tipo, negocio);
        return listaCecosPorGeo;
    }

    public List<ListaCategoriasPDDTO> obtieneCategorias(int op, int nivel, int dependeDe) throws Exception {
        listaCategorias = cargaArchivo.obtieneCategorias(op, nivel, dependeDe);
        return listaCategorias;
    }

    public String insertaLanzamientosPorCecos(int op, String cecos, int negocio, int idDocumento) throws Exception {
        String resp = cargaArchivo.insertaLanzamientosPorCecos(op, cecos, negocio, idDocumento);
        return resp;
    }

    public String insertaLanzamientosPorGeo(int op, String geo, int negocio, int tipo, int idDocumento) throws Exception {
        String resp = cargaArchivo.insertaLanzamientosPorGeo(op, geo, negocio, tipo, idDocumento);
        return resp;
    }

    public String insertaLanzamientosPorListaDD(int op, int idLista, int negocio, int idDocumento) throws Exception {
        String resp = cargaArchivo.insertaLanzamientosPorListaDD(op, idLista, negocio, idDocumento);
        return resp;
    }

    public String insertCategory(int op, int idCategoria, String descripcion, String rutaIcono, String nombreIcono, int orden) {
        String resp = cargaArchivo.insertCategory(op, idCategoria, descripcion, rutaIcono, nombreIcono, orden);
        return resp;
    }

    public String updateCategory(int op, int idCategoria, String descripcion, String rutaIcono, String nombreIcono, int orden) {
        String resp = cargaArchivo.updateCategory(op, idCategoria, descripcion, rutaIcono, nombreIcono, orden);
        return resp;
    }

    public List<CategoriasPDDTO> getCategory(int op, int idCategoria) {
        List<CategoriasPDDTO> resp = cargaArchivo.getCategory(op, idCategoria);
        return resp;
    }

    public String insertDocInCategory(int op, int idDoc, String descripcion, int idCategoria, String tipo, int activo, int orden) {
        String resp = cargaArchivo.insertDocInCategory(op, idDoc, descripcion, idCategoria, tipo, activo, orden);
        return resp;
    }

    public String updateDocInCategory(int op, int idDoc, String descripcion, int idCategoria, String tipo, int activo, int orden) {
        String resp = cargaArchivo.updateDocInCategory(op, idDoc, descripcion, idCategoria, tipo, activo, orden);
        return resp;
    }

    public List<DocInCategoriaPDDTO> getDocInCategory(int op, int idDoc, int idCategoria) {
        List<DocInCategoriaPDDTO> resp = cargaArchivo.getDocInCategory(op, idDoc, idCategoria);
        return resp;
    }

    public String insertFileInfo(int op, int idArchivo, int idItem, String ruta, String nombre, int activo) {
        String resp = cargaArchivo.insertFileInfo(op, idArchivo, idItem, ruta, nombre, activo);
        return resp;
    }

    public String updateFileInfo(int op, int idArchivo, int idItem, String ruta, String nombre, int activo) {
        String resp = cargaArchivo.updateFileInfo(op, idArchivo, idItem, ruta, nombre, activo);
        return resp;
    }

    public List<FileDataPDDTO> getFileInfo(int op, int idArchivo) {
        List<FileDataPDDTO> resp = cargaArchivo.getFileInfo(op, idArchivo);
        return resp;
    }

    public String insertTablet(int op, int idTableta, String numSerie, String idCeco, int idUsuario, int idEstatus, String fechaInstalacion, int activo) {
        String resp = cargaArchivo.insertTablet(op, idTableta, numSerie, idCeco, idUsuario, idEstatus, fechaInstalacion, activo);
        return resp;
    }

    public String updateTablet(int op, int idTableta, String numSerie, String idCeco, int idUsuario, int idEstatus, String fechaInstalacion, int activo) {
        String resp = cargaArchivo.updateTablet(op, idTableta, numSerie, idCeco, idUsuario, idEstatus, fechaInstalacion, activo);
        return resp;
    }

    public String updateTabletCriticStatus(String numSerie, int idEstatus) {
        String resp = cargaArchivo.updateTabletCriticStatus(2, numSerie, idEstatus);
        return resp;
    }

    public List<TabletDataPDDTO> getTablet(int op, int idTableta, String numSerie) {
        List<TabletDataPDDTO> resp = cargaArchivo.getTablet(op, idTableta, numSerie);
        return resp;
    }

    public String insertFolio(int op, int idFolio, String tipo, String fecha, int activo) {
        String resp = cargaArchivo.insertFolio(op, idFolio, tipo, fecha, activo);
        return resp;
    }

    public String updateFolio(int op, int idFolio, String tipo, String fecha, int activo) {
        String resp = cargaArchivo.updateFolio(op, idFolio, tipo, fecha, activo);
        return resp;
    }

    public List<FolioDataPDDTO> getFolio(int op, int idFolio) {
        List<FolioDataPDDTO> resp = cargaArchivo.getFolio(op, idFolio);
        return resp;
    }

    public String insertDocto(int op, int idDocumento, int idFolio, String nombreArchivo, int idTipoDocto, int idUsuario, long pesoBytes, String origen, String vigenciaIni, String vigenciaFin, String visibleSuc, int idEstatus, int activo) {
        String resp = cargaArchivo.insertDocto(op, idDocumento, idFolio, nombreArchivo, idTipoDocto, idUsuario, pesoBytes, origen, vigenciaIni, vigenciaFin, visibleSuc, idEstatus, activo);
        return resp;
    }

    public String updateDocto(int op, int idDocumento, int idFolio, String nombreArchivo, int idTipoDocto, int idUsuario, long pesoBytes, String origen, String vigenciaIni, String vigenciaFin, String visibleSuc, int idEstatus, int activo) {
        String resp = cargaArchivo.updateDocto(op, idDocumento, idFolio, nombreArchivo, idTipoDocto, idUsuario, pesoBytes, origen, vigenciaIni, vigenciaFin, visibleSuc, idEstatus, activo);
        return resp;
    }

    public List<DoctoDataPDDTO> getDocto(int op, int idDocumento) {
        List<DoctoDataPDDTO> resp = cargaArchivo.getDocto(op, idDocumento);
        return resp;
    }

    public List<ListaCategoriasPDDTO> obtieneDocumentos(int op, int nivel, String dependeDe) {
        listaCategorias = cargaArchivo.obtieneDocumentos(op, nivel, dependeDe);
        return listaCategorias;
    }

    public List<FolioFilterDataPDDTO> getFolioDataPorFolio(int op, int idFolio) {
        List<FolioFilterDataPDDTO> resp = cargaArchivo.getFolioDataPorFolio(op, idFolio);
        return resp;
    }

    public List<FolioFilterDataPDDTO> getFolioDataPorTerritorio(int op, String ceco, String categoria, String fecha, String estatus, String tipo) {
        List<FolioFilterDataPDDTO> resp = cargaArchivo.getFolioDataPorTerritorio(op, ceco, categoria, fecha, estatus, tipo);
        return resp;
    }

    public List<FolioFilterDataPDDTO> getFolioDataPorGeografia(int op, String geo, int nivel, String categoria, String fecha, String estatus, String tipo) {
        List<FolioFilterDataPDDTO> resp = cargaArchivo.getFolioDataPorGeografia(op, geo, nivel, categoria, fecha, estatus, tipo);
        return resp;
    }

    public List<String> convertEconNumToCeco(int op, String numEco) {
        List<String> resp = cargaArchivo.convertEconNumToCeco(op, numEco);
        return resp;
    }

    public String getLastestDoc(int op, int idDocto) {
        String resp = cargaArchivo.getLastestDoc(op, idDocto);
        return resp;
    }

    public String updateOldDocument(int idDoc, String vigenciaFin, String visibleSuc) {
        String resp = cargaArchivo.updateOldDocument(idDoc, vigenciaFin, visibleSuc);
        return resp;
    }

    public String insertAdmonTipoDoc(int op, int idTipoDoc, String nombre, int nivel, int dependeDe, int activo) {
        String resp = cargaArchivo.insertAdmonTipoDoc(op, idTipoDoc, nombre, nivel, dependeDe, activo);
        return resp;
    }

    public String updateAdmonTipoDoc(int op, int idTipoDoc, String nombre, int nivel, int dependeDe, int activo) {
        String resp = cargaArchivo.updateAdmonTipoDoc(op, idTipoDoc, nombre, nivel, dependeDe, activo);
        return resp;
    }

    public List<DocInCategoryPDDTO> getAdmonTipoDoc(int op, int idTipoDoc) {
        List<DocInCategoryPDDTO> resp = cargaArchivo.getAdmonTipoDoc(op, idTipoDoc);
        return resp;
    }

    public List<ListaCecoPorCSVPDDTO> getListaCecosFD(String csvList, String negocio, int idFolio) {
        List<ListaCecoPorCSVPDDTO> lista = null;

        try {
            List<ListaCecoPorCSVPDDTO> listaA = cargaArchivo.obtieneCecosPorCSV(csvList, negocio);
            String cecos = "";

            for (ListaCecoPorCSVPDDTO listaCecoPorCSVPDDTO : listaA) {
                cecos += listaCecoPorCSVPDDTO.getIdCeco() + ",";
            }

            if (cecos.length() > 0 && idFolio > 0) {
                lista = estatusGeneralBI.addSucursalesFolio(1, idFolio, cecos);
            } else {
                return new ArrayList<ListaCecoPorCSVPDDTO>();
            }
        } catch (Exception e) {
            lista = new ArrayList<ListaCecoPorCSVPDDTO>();
            e.printStackTrace();
        }

        return lista;
    }

    public String readExcelForGeoData(String geoData) {

        JsonObject _respJO = new JsonObject();
        String resp = null;
        Gson gson = new Gson();
        GeoJson _obj = new GeoJson();

        try {
            JsonElement je = gson.fromJson(geoData, JsonElement.class);
            JsonArray ja = je.getAsJsonArray();

            int nGeoUpdates = 0;
            int nGeoUpdatesErrors = 0;
            int nGeoInserts = 0;
            int nGeoInsertsErrors = 0;

            String descGeoUpdatesErrors = new String("");
            String descGeoInsertsErrors = new String("");

            for (JsonElement jsonElement : ja) {
                JsonObject _jsonObj = jsonElement.getAsJsonObject();

                if (_jsonObj.get("FIID_GEOGRAFIA").getAsInt() > 0) {
                    // Consulta objeto para definir si se debe actualizar o insertar
                    List<GeoDataPDDTO> _geoData = estatusGeneral.getGeo(2,
                            _jsonObj.get("FIID_GEOGRAFIA").getAsInt(),
                            _jsonObj.get("FITIPO").getAsInt(),
                            _jsonObj.get("FIDEPENDE_DE").getAsInt());

                    JsonElement auxJE = null;
                    JsonObject auxJO = null;

                    if (_geoData.isEmpty()) {
                        // INSERT GEO DATA

                        String insertResp = estatusGeneral.insertGeo(0,
                                _jsonObj.get("FIID_GEOGRAFIA").getAsInt(),
                                _jsonObj.get("FITIPO").getAsInt(),
                                _jsonObj.get("FIIDCC").getAsInt(),
                                _jsonObj.get("FCDESCRIPCION").getAsString(),
                                _jsonObj.get("FIDEPENDE_DE").getAsInt(),
                                _jsonObj.get("FIACTIVO").getAsInt());

                        auxJE = gson.fromJson(insertResp, JsonElement.class);
                        auxJO = auxJE.getAsJsonObject();

                        if (auxJO.get("respuesta").getAsInt() == 0) {
                            // SI SE INSERTO
                            nGeoInserts++;
                        } else if (auxJO.get("respuesta").getAsInt() == 1) {
                            // NO SE INSERTO
                            nGeoInsertsErrors++;
                            descGeoInsertsErrors += "EL FIID_GEOGRAFIA " + _jsonObj.get("FIID_GEOGRAFIA").getAsInt() + " NO FUE INSERTADO,";
                        }
                    } else {
                        // UPDATE GEO DATA

                        String updateResp = estatusGeneral.updateGeo(1,
                                _jsonObj.get("FIID_GEOGRAFIA").getAsInt(),
                                _jsonObj.get("FITIPO").getAsInt(),
                                _jsonObj.get("FIIDCC").getAsInt(),
                                _jsonObj.get("FCDESCRIPCION").getAsString(),
                                _jsonObj.get("FIDEPENDE_DE").getAsInt(),
                                _jsonObj.get("FIACTIVO").getAsInt());

                        auxJE = gson.fromJson(updateResp, JsonElement.class);
                        auxJO = auxJE.getAsJsonObject();

                        if (auxJO.get("respuesta").getAsInt() == 0) {
                            // SI SE ACTUALIZO
                            nGeoUpdates++;
                        } else if (auxJO.get("respuesta").getAsInt() == 1) {
                            // NO SE ACTUALIZO
                            nGeoUpdatesErrors++;
                            descGeoUpdatesErrors += "EL FIID_GEOGRAFIA " + _jsonObj.get("FIID_GEOGRAFIA").getAsInt() + " NO FUE ACTUALIZADO,";
                        }
                    }
                } else {
                    List<GeoDataPDDTO> _geoData = estatusGeneral.getGeo(2,
                            _jsonObj.get("FIID_GEOGRAFIA").getAsInt(),
                            0 /**
                             * _jsonObj.get("FITIPO").getAsInt()
                             */
                            ,
                             0 /**
                     * _jsonObj.get("FIDEPENDE_DE").getAsInt()
                     */
                    );

                    JsonElement auxJE = null;
                    JsonObject auxJO = null;

                    if (_geoData.isEmpty()) {
                        // INSERT GEO DATA
                        String insertResp = null;

                        if (_jsonObj.get("FIIDCC").getAsString().length() < 1) {
                            insertResp = estatusGeneral.insertGeo(0,
                                    _jsonObj.get("FIID_GEOGRAFIA").getAsInt(),
                                    _jsonObj.get("FITIPO").getAsInt(),
                                    0 /**
                                     * _jsonObj.get("FIIDCC").getAsInt()
                                     */
                                    ,
                                     _jsonObj.get("FCDESCRIPCION").getAsString(),
                                    0 /**
                                     * _jsonObj.get("FIDEPENDE_DE").getAsInt()
                                     */
                                    ,
                                     _jsonObj.get("FIACTIVO").getAsInt());
                        } else {
                            insertResp = estatusGeneral.insertGeo(0,
                                    _jsonObj.get("FIID_GEOGRAFIA").getAsInt(),
                                    _jsonObj.get("FITIPO").getAsInt(),
                                    _jsonObj.get("FIIDCC").getAsInt(),
                                    _jsonObj.get("FCDESCRIPCION").getAsString(),
                                    _jsonObj.get("FIDEPENDE_DE").getAsInt(),
                                    _jsonObj.get("FIACTIVO").getAsInt());
                        }

                        auxJE = gson.fromJson(insertResp, JsonElement.class);
                        auxJO = auxJE.getAsJsonObject();

                        if (auxJO.get("respuesta").getAsInt() == 0) {
                            // SI SE INSERTO
                            nGeoInserts++;
                        } else if (auxJO.get("respuesta").getAsInt() == 1) {
                            // NO SE INSERTO
                            nGeoInsertsErrors++;
                            descGeoInsertsErrors += "EL FIID_GEOGRAFIA " + _jsonObj.get("FIID_GEOGRAFIA").getAsInt() + " NO FUE INSERTADO,";
                        }
                    } else {
                        // UPDATE GEO DATA

                        String updateResp = estatusGeneral.updateGeo(1,
                                _jsonObj.get("FIID_GEOGRAFIA").getAsInt(),
                                _jsonObj.get("FITIPO").getAsInt(),
                                0 /**
                                 * _jsonObj.get("FIIDCC").getAsInt()
                                 */
                                ,
                                 _jsonObj.get("FCDESCRIPCION").getAsString(),
                                0 /**
                                 * _jsonObj.get("FIDEPENDE_DE").getAsInt()
                                 */
                                ,
                                 _jsonObj.get("FIACTIVO").getAsInt());

                        auxJE = gson.fromJson(updateResp, JsonElement.class);
                        auxJO = auxJE.getAsJsonObject();

                        if (auxJO.get("respuesta").getAsInt() == 0) {
                            // SI SE ACTUALIZO
                            nGeoUpdates++;
                        } else if (auxJO.get("respuesta").getAsInt() == 1) {
                            // NO SE ACTUALIZO
                            nGeoUpdatesErrors++;
                            descGeoUpdatesErrors += "EL FIID_GEOGRAFIA " + _jsonObj.get("FIID_GEOGRAFIA").getAsInt() + " NO FUE ACTUALIZADO,";
                        }
                    }
                }
            }

            // Una vez terminado el ciclo retornar la informacion al front para construir las tablas de resultados
            _obj.setMsj(0);
            _obj.setnGeoUpdates(nGeoUpdates);
            _obj.setnGeoUpdatesErrors(nGeoUpdatesErrors);
            _obj.setDescGeoUpdatesErrors(descGeoUpdatesErrors);
            _obj.setnGeoInserts(nGeoInsertsErrors);
            _obj.setnGeoInsertsErrors(nGeoInsertsErrors);
            _obj.setDescGeoInsertsErrors(descGeoInsertsErrors);
            resp = gson.toJson(_obj, GeoJson.class);
        } catch (Exception e) {
            // e.printStackTrace();
            resp = "{\"msj\":1}";
        }

        return resp;
    }

    public List<TabletDataForExcelPDDTO> getAllTabletsForExcel(int op) {
        List<TabletDataForExcelPDDTO> resp = cargaArchivo.getAllTabletsForExcel(op);
        return resp;
    }

    public String insertUpdateFileChecksum(Integer op, Integer idEncArch, Integer idRef, String origen, String checksum, Integer activo) {
        String resp = cargaArchivo.insertUpdateFileChecksum(op, idEncArch, idRef, origen, checksum, activo);
        return resp;
    }

    public List<FileChecksumPDDTO> getFileChecksum(Integer op, Integer idEncArch, Integer idRef, String origen, String checksum, Integer activo) {
        List<FileChecksumPDDTO> resp = cargaArchivo.getFileChecksum(op, idEncArch, idRef, origen, checksum, activo);
        return resp;
    }

    public List<FileWithoutChecksumPDDTO> getFilesWithoutChecksum(Integer op) {
        List<FileWithoutChecksumPDDTO> resp = cargaArchivo.getFileChecksum(op);
        return resp;
    }

    public String createChecksumOfFiles(Integer op) {
        List<FileWithoutChecksumPDDTO> resp = cargaArchivo.getFileChecksum(op);
        int count = 0;

        for (FileWithoutChecksumPDDTO foo : resp) {
            try {

                String _filePath = null;
                if (FRQConstantes.PRODUCCION) {
                    _filePath = PATH_PROD;
                } else {
                    _filePath = PATH_DESA;
                }

                String _url = _filePath + foo.getCategoria() + "/" + foo.getFcnombre_arch();
                MessageDigest md5Digest = MessageDigest.getInstance("MD5");
                BufferedInputStream in = new BufferedInputStream(new URL(_url).openStream());
                String _checksum = getFileChecksum(md5Digest, in);

                String _resp = cargaArchivo.insertUpdateFileChecksum(0, null, foo.getFiid_documento(), "ADMON", _checksum, 1);
                logger.info("Se genero el checksum: " + _checksum + " para el documento con el ID: " + foo.getFiid_documento());
                count++;

            } catch (Exception e) {
                // e.printStackTrace();
                logger.info("No fue posible generar checksum del documento con el ID: " + foo.getFiid_documento());
            }
        }

        return "Count: " + count;
    }

    /**
     * Metodo que devuelve File Checksum de un archivo (MD5)
     */
    private static String getFileChecksum(MessageDigest digest, BufferedInputStream fis) throws IOException {

        // Get file input stream for reading the file content
        // FileInputStream fis = new FileInputStream(file);
        // Create byte array to read data in chunks
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        // Read file data and update in message digest
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        }

        // close the stream; We don't need it now.
        fis.close();

        // Get the hash's bytes
        byte[] bytes = digest.digest();

        // This bytes[] has bytes in decimal format;
        // Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        // return complete hash
        return sb.toString();
    }

    public String truncateTPasoCeco() {
        String resp = cargaArchivo.truncateTPasoCeco();
        return resp;
    }

    public String insertTPasoCeco(CecosWSPDDTO ceco) {
        String resp = cargaArchivo.insertTPasoCeco(ceco);
        return resp;
    }

    public List<ParamCecoPDDTO> getCecoParameters() {
        List<ParamCecoPDDTO> resp = cargaArchivo.getCecoParameters();
        return resp;
    }

    public String updateCecosByParam(Integer op, String negocio, Integer idCeco) {
        String resp = cargaArchivo.updateCecosByParam(op, negocio, idCeco);
        return resp;
    }

    public List<DocCorruptosPDDTO> getDocCorruptos(Integer op, String idCeco, Integer folio, Integer idDocto, Integer fecha) {
        List<DocCorruptosPDDTO> resp = cargaArchivo.getDocCorruptos(op, idCeco, folio, idDocto, fecha);
        return resp;
    }

    public List<DocsBeforeUploadPDDTO> getDocsBeforeUpload(Integer op, Integer tipoDoc) {
        List<DocsBeforeUploadPDDTO> resp = cargaArchivo.getDocsBeforeUpload(op, tipoDoc);
        return resp;
    }

    public List<DocsBeforeUploadPDDTO> getExpiringDocs(int op) {
        List<DocsBeforeUploadPDDTO> resp = cargaArchivo.getExpiringDocs(op);
        return resp;
    }

    public List<DocsBeforeUploadPDDTO> getHistoryOfDocs(int op) {
        List<DocsBeforeUploadPDDTO> resp = cargaArchivo.getHistoryOfDocs(op);
        return resp;
    }

    public List<DocsBeforeUploadPDDTO> getAllFoliosData(int op) {
        List<DocsBeforeUploadPDDTO> resp = cargaArchivo.getAllFoliosData(op);
        return resp;
    }
}

class GeoJson {

    private int msj;
    private int nGeoUpdates;
    private int nGeoUpdatesErrors;
    private String descGeoUpdatesErrors;
    private int nGeoInserts;
    private int nGeoInsertsErrors;
    private String descGeoInsertsErrors;

    public int getMsj() {
        return msj;
    }

    public void setMsj(int msj) {
        this.msj = msj;
    }

    public int getnGeoUpdates() {
        return nGeoUpdates;
    }

    public void setnGeoUpdates(int nGeoUpdates) {
        this.nGeoUpdates = nGeoUpdates;
    }

    public int getnGeoUpdatesErrors() {
        return nGeoUpdatesErrors;
    }

    public void setnGeoUpdatesErrors(int nGeoUpdatesErrors) {
        this.nGeoUpdatesErrors = nGeoUpdatesErrors;
    }

    public String getDescGeoUpdatesErrors() {
        return descGeoUpdatesErrors;
    }

    public void setDescGeoUpdatesErrors(String descGeoUpdatesErrors) {
        this.descGeoUpdatesErrors = descGeoUpdatesErrors;
    }

    public int getnGeoInserts() {
        return nGeoInserts;
    }

    public void setnGeoInserts(int nGeoInserts) {
        this.nGeoInserts = nGeoInserts;
    }

    public int getnGeoInsertsErrors() {
        return nGeoInsertsErrors;
    }

    public void setnGeoInsertsErrors(int nGeoInsertsErrors) {
        this.nGeoInsertsErrors = nGeoInsertsErrors;
    }

    public String getDescGeoInsertsErrors() {
        return descGeoInsertsErrors;
    }

    public void setDescGeoInsertsErrors(String descGeoInsertsErrors) {
        this.descGeoInsertsErrors = descGeoInsertsErrors;
    }
}
