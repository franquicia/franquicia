package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.NegocioDAO;
import com.gruposalinas.franquicia.domain.NegocioDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class NegocioBI {

    private static Logger logger = LogManager.getLogger(NegocioBI.class);

    @Autowired

    NegocioDAO negocioDAO;

    List<NegocioDTO> listaNegocios = null;
    List<NegocioDTO> listaNegocio = null;

    public List<NegocioDTO> obtieneNegocio() {
        try {
            listaNegocios = negocioDAO.obtieneNegocio();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Negocio");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaNegocios;
    }

    public List<NegocioDTO> obtieneNegocio(int idNegocio) {
        try {
            listaNegocio = negocioDAO.obtieneNegocio(idNegocio);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Negocio");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaNegocio;
    }

    public int insertaNegocio(NegocioDTO negocio) {
        int idNegocio = 0;

        try {
            idNegocio = negocioDAO.insertaNegocio(negocio);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Negocio");
            UtilFRQ.printErrorLog(null, e);
        }

        return idNegocio;
    }

    public boolean actualizaNegocio(NegocioDTO negocio) {
        boolean respuesta = false;
        try {
            respuesta = negocioDAO.actualizaNegocio(negocio);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Negocio");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaNegocio(int idNegocio) {
        boolean respuesta = false;

        try {
            respuesta = negocioDAO.eliminaNegocio(idNegocio);
        } catch (Exception e) {
            logger.info("No fue posible borrar Negocio");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }
}
