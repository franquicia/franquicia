package com.gruposalinas.franquicia.business;

import com.google.gson.JsonObject;
import com.gruposalinas.franquicia.dao.ParametroDAO;
import com.gruposalinas.franquicia.dao.Usuario_ADAO;
import com.gruposalinas.franquicia.domain.DatosUsuarioDTO;
import com.gruposalinas.franquicia.domain.ParametroDTO;
import com.gruposalinas.franquicia.domain.Usuario_ADTO;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;

public class Usuario_ABI {

    private static Logger logger = LogManager.getLogger(Usuario_ABI.class);

    @Autowired
    Usuario_ADAO usuario_ADAO;

    @Autowired
    ParametroDAO parametroDAO;

    List<Usuario_ADTO> listaUsuarios = null;
    List<Usuario_ADTO> listaUsuario = null;
    List<Usuario_ADTO> listaUsuariosPaso = null;
    List<Usuario_ADTO> listaUsuarioPaso = null;

    public List<Usuario_ADTO> obtieneUsuario() {

        try {
            listaUsuarios = usuario_ADAO.obtieneUsuario();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaUsuarios;
    }

    public List<Usuario_ADTO> obtieneUsuario(int idUsuario) {

        try {
            listaUsuario = usuario_ADAO.obtieneUsuario(idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaUsuario;
    }

    public List<Usuario_ADTO> obtieneUsuarioPaso() {

        try {
            listaUsuarios = usuario_ADAO.obtieneUsuarioPaso();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaUsuarios;
    }

    public List<Usuario_ADTO> obtieneUsuarioPaso(String idUsuario, String idPuesto, String idCeco) {

        if (idUsuario.equals("NULL")) {
            idUsuario = null;
        }
        if (idPuesto.equals("NULL")) {
            idPuesto = null;
        }
        if (idCeco.equals("NULL")) {
            idCeco = null;
        }

        try {
            listaUsuario = usuario_ADAO.obtieneUsuarioPaso(idUsuario, idPuesto, idCeco);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaUsuario;
    }

    public boolean insertaUsuario(Usuario_ADTO usuario) {

        boolean respuesta = false;

        try {
            respuesta = usuario_ADAO.insertaUsuario(usuario);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean[] cargaUsuarios() {

        boolean resCargaUsuario = false;
        boolean resBajaUsuario = false;
        boolean resCargaPerfilU = false;
        boolean[] respuestaPerfilUsua = new boolean[3];

        try {
            resCargaUsuario = usuario_ADAO.cargaUsuarios();
            resBajaUsuario = usuario_ADAO.bajaUsuarios();
            resCargaPerfilU = usuario_ADAO.cargaPerfilUsuarios();

            respuestaPerfilUsua[0] = resCargaUsuario;
            respuestaPerfilUsua[1] = resBajaUsuario;
            respuestaPerfilUsua[2] = resCargaPerfilU;

        } catch (Exception e) {
            logger.info("No fue posible realizar la Carga de Usuarios");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuestaPerfilUsua;
    }

    public boolean actualizaUsuario(Usuario_ADTO usuario) {

        boolean respuesta = false;

        try {
            respuesta = usuario_ADAO.actualizaUsuario(usuario);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaUsuario(int idUsuario) {

        boolean respuesta = false;

        try {
            respuesta = usuario_ADAO.eliminaUsuario(idUsuario);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Usuario");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public DatosUsuarioDTO buscaUsuario(String usuario) {

        List<DatosUsuarioDTO> datosUsuario = null;

        try {

            datosUsuario = usuario_ADAO.obtieneDatos(usuario);
            //datosUsuario=usuario_ADAO.obtieneUsuario(usuario);

            if (datosUsuario.size() > 0) {
                if (datosUsuario.get(0).getUsuario() != null) {
                    return datosUsuario.get(0);
                }

            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            logger.info("Ocurrio algo al obtener los datos del usuario");
            return null;
        }

        return null;

    }

    public JsonObject validaUsuario(String usuario, String token) {

        logger.info("---------validaUsuario WSUnix metodo getData()------------");
        JsonObject jsonResult = new JsonObject();
        boolean valida = false;
        String validaToken = null;
        try {
            //Obtiene el parametro
            List<ParametroDTO> parametros = parametroDAO.obtieneParametro("validaToken");

            if (parametros != null && parametros.size() > 0) {
                validaToken = parametros != null ? parametros.get(0).getValor() : null;
            }

            if (validaToken != null) {
                if (validaToken.equals("1")) {
                    valida = true;
                } else {
                    jsonResult.addProperty("resultado", "0");
                }
            } else {
                valida = true;
            }

            if (valida) {
                String xmlResult = obtieneXmlResultGetData(usuario, token);

                if (xmlResult != null && !xmlResult.isEmpty()) {

                    try {

                        jsonResult.addProperty("resultado", xmlResult);

                    } catch (Exception e) {
                        logger.info("Ocurrio un fallo al recuperar valores de -> " + xmlResult + "  -> " + e.getMessage());
                        jsonResult = null;
                    }
                }
            }

        } catch (Exception e) {
            jsonResult = null;
            logger.info("Ocurrio algo al validar el Usuario " + e.getMessage());
        }

        return jsonResult;

    }

    public String obtieneXmlResultGetData(String usuario, String token) {

        String cadenaXml = null;
        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;
        StringBuilder strBuild = new StringBuilder();

        try {

            URL url = new URL(FRQConstantes.getURLWSUnixValidaUsuario() + "?WSDL");

            rc = (HttpURLConnection) url.openConnection();
            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:grup=\"GrupoSalinas\">"
                    + "<soapenv:Header/>"
                    + "<soapenv:Body>"
                    + "<grup:GetData>"
                    + "<grup:no_empleado>" + usuario + "</grup:no_empleado>"
                    + "<grup:token>" + token + "</grup:token>"
                    + "<grup:entidad>" + 1 + "</grup:entidad>"
                    + "</grup:GetData>"
                    + "</soapenv:Body>"
                    + "</soapenv:Envelope>";

            logger.info("PETICION SOA: " + xml);

            rc.setRequestProperty("Content-Length", Integer.toString(xml.length()));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.connect();

            OutputStream outputStream = rc.getOutputStream();

            try {
                wr = new OutputStreamWriter(outputStream);
                wr.write(xml, 0, xml.length());
                wr.flush();
            } finally {
                wr.close();
                wr = null;
            }

            InputStream inputSream = rc.getInputStream();

            try {
                rd = new BufferedReader(new InputStreamReader(inputSream));
            } catch (Exception e) {
                rd = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
            }

            String line;

            while ((line = rd.readLine()) != null) {
                strBuild.append(line);
            }

            inputSream.close();
            inputSream = null;

            StringReader stringReader = new StringReader(strBuild.toString().trim());
            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(stringReader);

            Element rootNode = document.getRootElement();
            Element body = rootNode.getChild("Body", Namespace.getNamespace("http://schemas.xmlsoap.org/soap/envelope/"));
            Element response = body.getChild("GetDataResponse", Namespace.getNamespace("GrupoSalinas"));
            Element result = response.getChild("GetDataResult", Namespace.getNamespace("GrupoSalinas"));

            cadenaXml = result.getValue();

            logger.info("CADENA -> " + cadenaXml);

        } catch (Exception e) {
            logger.info(" Ocurrio algo en obtieneXmlResult: " + e.getMessage());
            cadenaXml = null;
            e.printStackTrace();
        } finally {
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Ocurrio un fallo al invocar WSValidaUsuario: " + e.getMessage());
            }
        }
        return cadenaXml;
    }
}
