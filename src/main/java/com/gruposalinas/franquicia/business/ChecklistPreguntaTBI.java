package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.ChecklistPreguntaTDAO;
import com.gruposalinas.franquicia.domain.ChecklistPreguntaDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ChecklistPreguntaTBI {

    private static Logger logger = LogManager.getLogger(ChecklistPreguntaTBI.class);

    @Autowired
    ChecklistPreguntaTDAO checklistPreguntaTDAO;

    List<ChecklistPreguntaDTO> listaChecklistPregunta = null;

    public List<ChecklistPreguntaDTO> obtienePregCheckTemp(int idChecklist) {

        try {
            listaChecklistPregunta = checklistPreguntaTDAO.obtienePregCheckTemp(idChecklist);
        } catch (Exception e) {
            logger.info("No fue posible obtener Pregunta-CheckList Temporal");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaChecklistPregunta;

    }

    public boolean insertaChecklistPreguntaTemp(ChecklistPreguntaDTO bean) {

        boolean respuesta = false;
        try {
            respuesta = checklistPreguntaTDAO.insertaChecklistPreguntaTemp(bean);

        } catch (Exception e) {
            logger.info("No fue posible insertar Pregunta-CheckList temporal");
            UtilFRQ.printErrorLog(null, e);
        }
        return respuesta;

    }

    public boolean actualizaChecklistPreguntaTemp(ChecklistPreguntaDTO bean) {
        boolean respuesta = false;
        try {
            respuesta = checklistPreguntaTDAO.actualizaChecklistPreguntaTemp(bean);
        } catch (Exception e) {
            logger.info("No fue posible actualizar Pregunta-CheckList Temporal");
            UtilFRQ.printErrorLog(null, e);
        }
        return respuesta;
    }

    public boolean eliminaChecklistPreguntaTemp(int idChecklist, int idPregunta) {

        boolean respuesta = false;
        try {
            respuesta = checklistPreguntaTDAO.eliminaChecklistPreguntaTemp(idChecklist, idPregunta);
        } catch (Exception e) {
            logger.info("No fue posible eliminar Pregunta-CheckList Temporal");
            UtilFRQ.printErrorLog(null, e);
        }
        return respuesta;
    }

}
