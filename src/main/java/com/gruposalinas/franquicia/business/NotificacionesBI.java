package com.gruposalinas.franquicia.business;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.franquicia.domain.ChecklistPilaDTO;
import com.gruposalinas.franquicia.domain.MovilInfoDTO;
import com.gruposalinas.franquicia.util.Content;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class NotificacionesBI {

    static String apiKey = "AIzaSyCN-msi853al5iJrm0ihXErHXj9k2SYOzI";
    private final String FCM_SERVICE = "https://fcm.googleapis.com/fcm/send";
    @Autowired
    private NotificacionBI notificacionBI;

    private static Logger logger = LogManager.getLogger(NotificacionesBI.class);

    public void enviarNotificacionJSON(String token, String titulo, String mensaje) {

        String json = "";

        json = "{ " + "\"notification\":{ " + "\"sound\": \"OverrideSound\" " + "\"title\":\"" + titulo + "\", "
                + "\"text\":\"" + mensaje + "\" " + "}, " + "\"registration_ids\":[\"" + token + "\"] " + "}";
        System.out.println("JSON: " + json);
        HttpURLConnection conn = null;
        OutputStream os = null;
        BufferedReader rd = null;
        try {
            URL url = new URL(FCM_SERVICE);
            /*System.setProperty("java.net.useSystemProxies", "true");
			System.setProperty("http.proxyHost", "10.50.8.20");
			System.setProperty("http.proxyPort", "8080");
			// System.setProperty("http.proxyUser", "B196228");
			// System.setProperty("http.proxyPassword", "Bancoazteca2345");
			System.setProperty("http.proxySet", "true");

			System.setProperty("https.proxyHost", "10.50.8.20");
			System.setProperty("https.proxyPort", "8080");
			// System.setProperty("https.proxyUser", "B196228");
			// System.setProperty("https.proxyPassword", "Bancoazteca2345");
			System.setProperty("https.proxySet", "true");*/

            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");

            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key=" + apiKey);
            // conn.setRequestProperty("Content-Type", "text/html");

            os = conn.getOutputStream();
            os.write(json.getBytes("UTF-8"));

            System.out.println("CODIGO: " + conn.getResponseCode());

            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            // rd=new BufferedInputStream(conn.getInputStream());

            StringBuffer response = new StringBuffer();
            String line = "";

            while ((line = rd.readLine()) != null) {
                response.append(line);
            }

            System.out.println(response.toString());

            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();

            int totSucces = jsonObject.get("success").getAsInt();
            int totFail = jsonObject.get("failure").getAsInt();

            System.out.println("Success: " + totSucces);
            System.out.println("Success: " + totFail);

        } catch (Exception e) {
            System.out.println("ERROR: " + e.getMessage());

            System.out.println("ERROR: " + e);

        } finally {
            try {
                if (conn != null) {
                    conn.disconnect();
                }
            } catch (Exception e) {
                System.out.println("ERROR: l 101 " + e.getMessage());
                e.printStackTrace();
            }
            try {
                if (os != null) {
                    os.close();
                }
            } catch (Exception e) {
                System.out.println("ERROR: l 111 " + e.getMessage());
                e.printStackTrace();
            }
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                System.out.println("ERROR: l 120 " + e.getMessage());
                e.printStackTrace();
            }
        }

    }

    public boolean enviarNotificacionPush(Content content) {

        boolean salida = false;
        HttpURLConnection conn = null;
        BufferedReader in = null;
        DataOutputStream wr = null;
        try {

            // 1. URL
            URL url = new URL("https://fcm.googleapis.com/fcm/send");

            /*
			 * System.setProperty("java.net.useSystemProxies", "true");
			 * System.setProperty("http.proxyHost", "10.50.8.20");
			 * System.setProperty("http.proxyPort", "8080");
			 * //System.setProperty("http.proxyUser", "B196228");
			 * //System.setProperty("http.proxyPassword", "Bancoazteca2345");
			 * System.setProperty("http.proxySet", "true");
			 *
			 * System.setProperty("https.proxyHost", "10.50.8.20");
			 * System.setProperty("https.proxyPort", "8080");
			 * //System.setProperty("https.proxyUser", "B196228");
			 * //System.setProperty("https.proxyPassword", "Bancoazteca2345");
			 * System.setProperty("https.proxySet", "true");
             */

 /*System.setProperty("java.net.useSystemProxies", "true");
			System.setProperty("http.proxyHost", "10.50.8.20");
			System.setProperty("http.proxyPort", "8080");
			// System.setProperty("http.proxyUser", "B196228");
			// System.setProperty("http.proxyPassword", "Bancoazteca2345");
			System.setProperty("http.proxySet", "true");

			System.setProperty("https.proxyHost", "10.50.8.20");
			System.setProperty("https.proxyPort", "8080");
			// System.setProperty("https.proxyUser", "B196228");
			// System.setProperty("https.proxyPassword", "Bancoazteca2345");
			System.setProperty("https.proxySet", "true");*/
            // 2. Open connection
            conn = (HttpURLConnection) url.openConnection();

            // 3. Specify POST method
            conn.setRequestMethod("POST");

            // 4. Set the headers
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "key=" + apiKey);

            conn.setDoOutput(true);

            // 5. Add JSON data into POST request body
            // `5.1 Use Jackson object mapper to convert Contnet object into
            // JSON
            //ObjectMapper mapper = new ObjectMapper(); erickrojo
            // 5.2 Get connection output stream
            wr = new DataOutputStream(conn.getOutputStream());

            // 5.3 Copy Content "JSON" into
            //mapper.writeValue(wr, content);  erickrojo
            // System.out.println("JSON: " + content.toString());
            // 5.4 Send the request
            wr.flush();

            // 5.5 close
            wr.close();

            // 6. Get the response
            int responseCode = conn.getResponseCode();
            logger.info("\nSending 'POST' request to URL : " + url);
            logger.info("Response Code : " + responseCode);

            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // 7. Print result
            logger.info(response.toString());
            salida = true;

        } catch (Exception e) {
            logger.info("ocurrio algo al enviar la notificaciÃ³n: " + e.getMessage());
            logger.info("ocurrio algo al enviar la notificaciÃ³n: " + e);
            e.printStackTrace();

            salida = false;
        } finally {
            try {
                if (wr != null) {
                    wr.close();
                }
            } catch (Exception e) {
                logger.info("ocurrio algo al cerrar conexiones l 230: " + e.getMessage());
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e) {
                logger.info("ocurrio algo al cerrar conexiones l 239: " + e.getMessage());
                e.printStackTrace();
            }
            try {
                if (conn != null) {
                    conn.disconnect();
                }

            } catch (Exception e) {
                logger.info("ocurrio algo al cerrar conexiones l 247: " + e.getMessage());
                e.printStackTrace();
            }

        }
        return salida;
    }

    public static Content createContent(String tokenUsuario, String titulo, String mensaje) {

        Content c = new Content();

        c.addRegId(tokenUsuario);
        c.createData(titulo, mensaje);

        return c;
    }

    public boolean enviarNotificacion(String tokenUsuario, String titulo, String mensaje) {

        boolean resultado = false;
        Content content = createContent(tokenUsuario, titulo, mensaje);

        resultado = enviarNotificacionPush(content);

        return resultado;
    }

    @SuppressWarnings({"unused", "unchecked"})
    public boolean enviarReporteAndroid() {

        String mensaje = "";
        String titulo = "Mi GestiÃ³n";
        boolean respuesta = false;
        // notificacionBI=new NotificacionBI();
        List<MovilInfoDTO> listaUsuario = notificacionBI.obtieneUsuariosNoti("ANDROID");
        try {
            if (listaUsuario != null) {
                for (MovilInfoDTO usr : listaUsuario) {

                    Map<String, Object> reporte = notificacionBI.NotificacionPorcentaje(usr.getIdUsuario());
                    int porcentaje = (Integer) reporte.get("porcentaje");

                    mensaje = "";
                    mensaje += "Avance:\n";
                    //mensaje += "Hoy debes estar al 25%\n";

                    List<ChecklistPilaDTO> listaReporte = (List<ChecklistPilaDTO>) reporte.get("listaReporte");

                    //mensaje += "Estas en: \n";
                    for (ChecklistPilaDTO cPila : listaReporte) {
                        mensaje += "\n";

                        if (cPila.getEstatus() < 25) {
                            mensaje += "ðŸ‘Ž" + cPila.getNombreCheck() + ": " + cPila.getEstatus() + "% ";

                        } else {
                            mensaje += "ðŸ‘�" + cPila.getNombreCheck() + ": " + cPila.getEstatus() + "% ";

                        }

                    }
                    logger.info("MENSAJE: " + mensaje);
                    logger.info("TOKEN ANDROID: " + usr.getToken());

                    enviarNotificacion(usr.getToken(), titulo, mensaje);
                }
            }
            respuesta = true;
        } catch (Exception e) {
            logger.info("ocurrio algo: " + e.getMessage());
            respuesta = false;

        }
        return respuesta;

    }

    @SuppressWarnings({"unused", "unchecked"})
    public boolean enviarReporteIOS() {

        String mensaje = "";
        String titulo = "Mi GestiÃ³n";
        boolean respuesta = false;
        // notificacionBI=new NotificacionBI();
        List<MovilInfoDTO> listaUsuario = notificacionBI.obtieneUsuariosNoti("IOS");
        try {
            if (listaUsuario != null) {
                for (MovilInfoDTO usr : listaUsuario) {

                    Map<String, Object> reporte = notificacionBI.NotificacionPorcentaje(usr.getIdUsuario());
                    int porcentaje = (Integer) reporte.get("porcentaje");

                    mensaje = "";
                    mensaje += "Avance:\n";
                    //mensaje += "Hoy debes estar al 25%\n";

                    List<ChecklistPilaDTO> listaReporte = (List<ChecklistPilaDTO>) reporte.get("listaReporte");

                    //mensaje += "Estas en:";
                    for (ChecklistPilaDTO cPila : listaReporte) {
                        mensaje += "\n";

                        if (cPila.getEstatus() < 25) {
                            mensaje += "ðŸ‘Ž" + cPila.getNombreCheck() + ": " + cPila.getEstatus() + "% ";

                        } else {
                            mensaje += "ðŸ‘�" + cPila.getNombreCheck() + ": " + cPila.getEstatus() + "% ";

                        }

                    }
                    logger.info("MENSAJE: " + mensaje);
                    logger.info("TOKEN IOS: " + usr.getToken());

                    enviarNotificacionJSON(usr.getToken(), titulo, mensaje);
                }
            }
            respuesta = true;
        } catch (Exception e) {
            logger.info("ocurrio algo: " + e.getMessage());
            respuesta = false;

        }
        return respuesta;

    }

}
