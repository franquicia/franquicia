package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.FormArchiverosDAO;
import com.gruposalinas.franquicia.domain.FormArchiverosDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class FormArchiverosBI {

    private static Logger logger = LogManager.getLogger(FormArchiverosBI.class);

    @Autowired
    FormArchiverosDAO formArchiverosDAO;

    List<FormArchiverosDTO> listFormArchiveros = null;

    public List<FormArchiverosDTO> obtieneArchiveros(int idFormato) {
        try {
            listFormArchiveros = formArchiverosDAO.obtieneFormArchiveros(idFormato);
        } catch (Exception e) {
            logger.info("No fue posible obtener el formato de archiveros");
            UtilFRQ.printErrorLog(null, e);
        }
        return listFormArchiveros;
    }

    public int insertaFormArchiveros(FormArchiverosDTO bean) {
        int flag = 0;
        try {
            flag = formArchiverosDAO.insertaFormarchiveros(bean);
        } catch (Exception e) {
            logger.info("No fue posible insertar el formato de archiveros");
            UtilFRQ.printErrorLog(null, e);
            flag = 0;
        }
        return flag;
    }

    public boolean modificaFormArchiveros(FormArchiverosDTO bean) {
        boolean flag = false;
        try {
            flag = formArchiverosDAO.modificaFormarchiveros(bean);
        } catch (Exception e) {
            logger.info("No fue posible monidicar el formato de archiveros");
            UtilFRQ.printErrorLog(null, e);
            flag = false;
        }
        return flag;
    }

    public boolean eliminaFormArchiveros(int idFormato) {
        boolean flag = false;
        try {
            flag = formArchiverosDAO.eliminaFormarchiveros(idFormato);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el formato de archiveros");
            UtilFRQ.printErrorLog(null, e);
            flag = false;
        }
        return flag;
    }

    public List<FormArchiverosDTO> obtieneFormCeco(String idCeco) {
        try {
            listFormArchiveros = formArchiverosDAO.obtieneFormCeco(idCeco);
        } catch (Exception e) {
            logger.info("No fue posible obtener el formato de archiveros por ceco");
            UtilFRQ.printErrorLog(null, e);
            logger.info("Salida: " + e.getMessage());
        }
        return listFormArchiveros;
    }
}
