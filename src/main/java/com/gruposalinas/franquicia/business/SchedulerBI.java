package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.domain.ParametroDTO;
import com.gruposalinas.franquicia.domain.SchedulerDTO;
import com.gruposalinas.franquicia.domain.TareaDTO;
import com.gruposalinas.franquicia.resources.FRQAppContextProvider;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SchedulerBI implements Job {

    @Autowired
    TareasBI tareasBI;

    private static final Logger logger = LogManager.getLogger(SchedulerBI.class);
    private static SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
    private static Map<String, Scheduler> schedArray = new LinkedHashMap<String, Scheduler>();
    private static Map<String, JobDetail> jobDetailArray = new LinkedHashMap<String, JobDetail>();
    private static int aut = 0;

    public void execute(JobExecutionContext cntxt) throws JobExecutionException {

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("eliminaToken")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO eliminaToken... ");
            try {
                TokenBI tokenBI = (TokenBI) FRQAppContextProvider.getApplicationContext().getBean("tokenBI");
                logger.info("Token... " + tokenBI.eliminaTokenTodos());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaCecos")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaCecos... ");
            try {
                ParametroBI paramBI = (ParametroBI) FRQAppContextProvider.getApplicationContext()
                        .getBean("parametroBI");
                int paramTotal = Integer.parseInt(paramBI.obtieneParametros("numCargas").get(0).getValor());
                for (int j = 1; j <= paramTotal; j++) {
                    ParametroDTO parametroDTO = new ParametroDTO();
                    parametroDTO.setClave("tipoCarga");
                    parametroDTO.setValor("" + j);
                    parametroDTO.setActivo(1);
                    ParametroBI parametroBI = (ParametroBI) FRQAppContextProvider.getApplicationContext()
                            .getBean("parametroBI");
                    logger.info("Actualiza Parametro... " + parametroBI.actualizaParametro(parametroDTO));
                    CecoBI cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
                    logger.info("Carga Ceco... " + cecoBI.cargaCecos());
                    logger.info("Carga Ceco 2... " + cecoBI.cargaCecos2());
                }
                CecoBI cecoActBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
                logger.info("Actualiza Cecos... " + cecoActBI.updateCecos());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaUsuarios")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaUsuarios... ");
            try {
                Usuario_ABI usuario_ABI = (Usuario_ABI) FRQAppContextProvider.getApplicationContext()
                        .getBean("usuario_ABI");
                logger.info("Carga Usuarios... " + usuario_ABI.cargaUsuarios());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaSucursales")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaSucursales... ");
            try {
                SucursalBI sucursalBI = (SucursalBI) FRQAppContextProvider.getApplicationContext().getBean("sucursalBI");
                logger.info("Carga Sucursales... " + sucursalBI.cargaSucursales());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaCecosLAM")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaCecosLAM... ");
            try {
                CecoBI cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
                logger.info("Carga Cecos LAM... " + cecoBI.cargaCecosLAM());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("test")) {
            logger.info("... EJECUTO SCHEDULER TEST ... ");
        }
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("ejecutaAsignaciones")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO ejecutaAsignaciones... ");
            try {
                AsignacionBI asignacionBI = (AsignacionBI) FRQAppContextProvider.getApplicationContext().getBean("asignacionBI");
                logger.info("AsignacionBI... " + asignacionBI.ejecutaAsignacion());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cargaGeografia")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO cargaGeografia... ");
            try {
                CecoBI cecoBI = (CecoBI) FRQAppContextProvider.getApplicationContext().getBean("cecoBI");
                logger.info("Carga Geografia... " + cecoBI.cargaGeografia());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("notificacionRegionalAndroid")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO notificacionRegionalAndroid... ");
            try {
                NotificacionesBI notificacion = (NotificacionesBI) FRQAppContextProvider.getApplicationContext().getBean("notificacionesBI");
                logger.info("Enviando notificaciones... " + notificacion.enviarReporteAndroid());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("notificacionRegionalIOS")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO notificacionRegionalIOS... ");
            try {
                NotificacionesBI notificacion = (NotificacionesBI) FRQAppContextProvider.getApplicationContext().getBean("notificacionesBI");
                logger.info("Enviando notificaciones... " + notificacion.enviarReporteIOS());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("eliminaRespuestasDuplicadas08")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO eliminaRespuestasDuplicadas... ");
            try {
                RespuestaBI respuesta = (RespuestaBI) FRQAppContextProvider.getApplicationContext().getBean("respuestaBI");
                logger.info("Elimina Respuestas... " + respuesta.eliminaRespuestasDuplicadas());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("eliminaRespuestasDuplicadas15")) {
            logger.info("...EJECUTO EL PROCEDIMIENTO eliminaRespuestasDuplicadas... ");
            try {
                RespuestaBI respuesta = (RespuestaBI) FRQAppContextProvider.getApplicationContext().getBean("respuestaBI");
                logger.info("Elimina Respuestas... " + respuesta.eliminaRespuestasDuplicadas());
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }

        /**
         * *************************************
         */
        /**
         * PROCESOS BATCH DE PEDESTAL DIGITAL *
         */
        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("actualizaEstatusFoliosPD")) {
            logger.info("..... EJECUTANDO PROCEDIMIENTO actualizaEstatusFoliosPD .....");
            try {
                Date _date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                EstatusGeneralBI estatusGeneralBI = (EstatusGeneralBI) FRQAppContextProvider.getApplicationContext().getBean("estatusGeneralBI");
                String aux = estatusGeneralBI.actualizaEstatusFoliosPD(dateFormat.format(_date));
                String[] split = aux.split("-.-.-");
                logger.info("... SE EJECUTO EL PROCEDIMIENTO actualizaEstatusFoliosPD - ACTUALIZADOS: " + split[0] + " ...");
                logger.info("... SE EJECUTO EL PROCEDIMIENTO actualizaEstatusFoliosPD - NO ACTUALIZADOS: " + split[1] + " ...");
            } catch (Exception e) {
                logger.info("Algo ocurrio al ejecutar el servicio actualizaEstatusFoliosPD");
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cambiaEstatusTabletasAPK")) {
            logger.info("..... EJECUTANDO PROCEDIMIENTO cambiaEstatusTabletasAPK .....");
            try {
                Date _date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                EstatusGeneralBI estatusGeneralBI = (EstatusGeneralBI) FRQAppContextProvider.getApplicationContext().getBean("estatusGeneralBI");
                String aux = estatusGeneralBI.actualizaEstatusTabletasAPK(dateFormat.format(_date));
                logger.info("... SE EJECUTO EL PROCEDIMIENTO cambiaEstatusTabletasAPK - " + aux + " ...");
            } catch (Exception e) {
                logger.info("Algo ocurrio al ejecutar el servicio cambiaEstatusTabletasPD");
            }
        }

        if (cntxt.getJobDetail().getJobDataMap().get("type").equals("cambiaEstatusTabletasDOC")) {
            logger.info("..... EJECUTANDO PROCEDIMIENTO cambiaEstatusTabletasAPK .....");
            try {
                Date _date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
                EstatusGeneralBI estatusGeneralBI = (EstatusGeneralBI) FRQAppContextProvider.getApplicationContext().getBean("estatusGeneralBI");
                String aux = estatusGeneralBI.actualizaEstatusTabletasDOC(dateFormat.format(_date));
                logger.info("... SE EJECUTO EL PROCEDIMIENTO cambiaEstatusTabletasDOC - " + aux + " ...");
            } catch (Exception e) {
                logger.info("Algo ocurrio al ejecutar el servicio cambiaEstatusTabletasPD");
            }
        }
    }

    public void run() throws SchedulerException, UnknownHostException {
        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        //if (aut == 0 && ip.equals("10.53.29.153")) {
        if (aut == 0 && ip.equals(FRQConstantes.getIpHost())) {
            CronTrigger trigger = null;
            List<TareaDTO> listaTareas = null;
            try {
                listaTareas = tareasBI.obtieneTareas();
                Scheduler sched = null;
                JobDetail jobDetail = new JobDetail();
                Iterator<TareaDTO> it = listaTareas.iterator();
                while (it.hasNext()) {
                    /* INICIA EL SCHEDULER */
                    TareaDTO o = it.next();
                    if (o.getActivo() == 1) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + o.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", o.getCveTarea());
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression("*/10 * * * * ?");
                        sched.scheduleJob(jobDetail, trigger);
                        sched.start();
                        /* MODIFICA EL SCHEDULER CON LOS DATOS DE LA BD */
                        sched.unscheduleJob("trigger" + o.getCveTarea(), "Trigger");
                        sched = schedFact.getScheduler();
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression(o.getStrFechaTarea());
                        sched.scheduleJob(jobDetail, trigger);
                        sched.start();
                    } else if (o.getActivo() == 0) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + o.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", o.getCveTarea());
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression("*/10 * * * * ?");
                        /* MODIFICA EL SCHEDULER CON LOS DATOS DE LA BD */
                        sched.unscheduleJob("trigger" + o.getCveTarea(), "Trigger");
                        sched = schedFact.getScheduler();
                        trigger = new CronTrigger("trigger" + o.getCveTarea(), "Trigger");
                        trigger.setCronExpression(o.getStrFechaTarea());
                    }
                    schedArray.put(o.getCveTarea(), sched);
                    jobDetailArray.put(o.getCveTarea(), jobDetail);
                }
            } catch (ParseException e) {
                logger.info("Algo Ocurrió... ", e);
            }
        }
        aut++;
    }

    // AGREGA UNA TAREA
    public boolean newTask(SchedulerDTO scheduler, HttpServletRequest request)
            throws ParseException, UnknownHostException {

        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        //if (ip.equals("10.53.29.153")) {
        if (ip.equals(FRQConstantes.getIpHost())) {
            try {
                //
                String date = "";
                String h = scheduler.getHora(), m = scheduler.getMinutos(), s = scheduler.getSegundos(),
                        d = scheduler.getDia(), month = scheduler.getMes(), diaSemana = scheduler.getDiaSemana();

                if (request.getParameter("h") != null && request.getParameter("h").equals("on")) {
                    h = "*/" + scheduler.getHora();
                }
                if (request.getParameter("m") != null && request.getParameter("m").equals("on")) {
                    m = "*/" + scheduler.getMinutos();
                }
                if (request.getParameter("s") != null && request.getParameter("s").equals("on")) {
                    s = "*/" + scheduler.getSegundos();
                }
                if (request.getParameter("d") != null && request.getParameter("d").equals("on")) {
                    d = "*/" + scheduler.getDia();
                }
                if (request.getParameter("month") != null && request.getParameter("month").equals("on")) {
                    month = "*/" + scheduler.getMes();
                }
                if (request.getParameter("diaSemana") != null && request.getParameter("diaSemana").equals("on")) {
                    month = "*/" + scheduler.getDiaSemana();
                }
                if (!request.getParameter("diaSemana").equals("?")) {
                    d = "?";
                }

                date = s + " " + m + " " + h + " " + d + " " + month + " " + diaSemana;

                TareaDTO tarea = new TareaDTO();
                tarea.setActivo(Integer.parseInt(request.getParameter("nTareaActiva")));
                tarea.setCveTarea(request.getParameter("nombreTarea"));// nombre
                tarea.setStrFechaTarea(date);

                CronTrigger trigger = null;
                @SuppressWarnings("unused")
                List<TareaDTO> listaTareas = null;
                try {
                    listaTareas = tareasBI.obtieneTareas();
                    Scheduler sched = null;
                    JobDetail jobDetail = new JobDetail();
                    if (tarea.getActivo() == 1) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + tarea.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", tarea.getCveTarea());
                        trigger = new CronTrigger("trigger" + tarea.getCveTarea(), "Trigger");
                        trigger.setCronExpression(tarea.getStrFechaTarea());
                        sched.scheduleJob(jobDetail, trigger);
                        sched.start();
                    } else if (tarea.getActivo() == 0) {
                        sched = schedFact.getScheduler();
                        jobDetail = new JobDetail("Job" + tarea.getCveTarea(), "Ejemplo", SchedulerBI.class);
                        jobDetail.getJobDataMap().put("type", tarea.getCveTarea());
                        trigger = new CronTrigger("trigger" + tarea.getCveTarea(), "Trigger");
                        trigger.setCronExpression(tarea.getStrFechaTarea());
                    }
                    schedArray.put(tarea.getCveTarea(), sched);
                    jobDetailArray.put(tarea.getCveTarea(), jobDetail);
                } catch (SchedulerException e) {
                    logger.info("Algo Ocurrió... ", e);
                    return false;
                }
                try {
                    tareasBI.insertaTarea(tarea);
                } catch (Exception e) {
                    logger.info("Algo Ocurrió... ", e);
                    return false;
                }
            } catch (Exception e) {
                logger.info("Algo Ocurrió... ", e);
                return false;
            }
        }
        return true;
    }

    // MODIFICA UNA TAREA
    public boolean task(SchedulerDTO scheduler, HttpServletRequest request)
            throws ParseException, UnknownHostException {

        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        // if (ip.equals("10.53.29.153") ) {
        if (ip.equals(FRQConstantes.getIpHost())) {
            try {
                String date = "";
                String identifier = scheduler.getId();
                String h = scheduler.getHora(), m = scheduler.getMinutos(), s = scheduler.getSegundos(),
                        d = scheduler.getDia(), month = scheduler.getMes(), diaSemana = scheduler.getDiaSemana();

                if (request.getParameter("h") != null && request.getParameter("h").equals("on")) {
                    h = "*/" + scheduler.getHora();
                }
                if (request.getParameter("m") != null && request.getParameter("m").equals("on")) {
                    m = "*/" + scheduler.getMinutos();
                }
                if (request.getParameter("s") != null && request.getParameter("s").equals("on")) {
                    s = "*/" + scheduler.getSegundos();
                }
                if (request.getParameter("d") != null && request.getParameter("d").equals("on")) {
                    d = "*/" + scheduler.getDia();
                }
                if (request.getParameter("month") != null && request.getParameter("month").equals("on")) {
                    month = "*/" + scheduler.getMes();
                }
                if (request.getParameter("diaSemana") != null && request.getParameter("diaSemana").equals("on")) {
                    month = "*/" + scheduler.getDiaSemana();
                }
                if (!request.getParameter("diaSemana").equals("?")) {
                    d = "?";
                }

                date = s + " " + m + " " + h + " " + d + " " + month + " " + diaSemana;

                TareaDTO tarea = new TareaDTO();
                tarea.setActivo(Integer.parseInt(request.getParameter("act" + identifier)));

                if (Integer.parseInt(request.getParameter("act" + identifier)) == 0) {
                    tarea.setStrFechaTarea(request.getParameter("fecha" + identifier));
                } else {
                    tarea.setStrFechaTarea(date);
                }

                tarea.setCveTarea(request.getParameter("idTarea" + identifier));
                tarea.setIdTarea(Integer.parseInt(scheduler.getId()));

                try {
                    tareasBI.actualizaTarea(tarea);
                } catch (Exception e) {
                    logger.info("Algo Ocurrió...", e);
                    return false;
                }

                CronTrigger trigger = null;
                List<TareaDTO> listaTareas = null;
                try {
                    listaTareas = tareasBI.obtieneTareas();
                    Iterator<TareaDTO> it = listaTareas.iterator();
                    @SuppressWarnings("unused")
                    Scheduler sched = null;
                    while (it.hasNext()) {
                        TareaDTO o = it.next();
                        if (o.getIdTarea() == tarea.getIdTarea() && o.getActivo() == 1) {
                            schedArray.get(tarea.getCveTarea()).unscheduleJob("trigger" + tarea.getCveTarea(),
                                    "Trigger");
                            sched = schedFact.getScheduler();
                            trigger = new CronTrigger("trigger" + tarea.getCveTarea(), "Trigger");
                            trigger.setCronExpression(tarea.getStrFechaTarea());
                            schedArray.get(tarea.getCveTarea()).scheduleJob(jobDetailArray.get(tarea.getCveTarea()),
                                    trigger);
                            schedArray.get(tarea.getCveTarea()).start();
                        } else if (o.getIdTarea() == tarea.getIdTarea() && o.getActivo() == 0) {
                            schedArray.get(tarea.getCveTarea()).unscheduleJob("trigger" + tarea.getCveTarea(),
                                    "Trigger");
                        }
                    }
                } catch (SchedulerException e) {
                    logger.info("Algo Ocurrió...", e);
                    return false;
                }

            } catch (Exception e) {
                logger.info("Algo Ocurrió...", e);
                return false;
            }
        }
        return true;
    }

    // ELIMINA UNA TAREA
    public boolean eliminaTask(int idTarea, String cveTarea) throws ParseException, UnknownHostException {
        String ip = InetAddress.getLocalHost().getHostAddress();
        logger.info("IP HOST...... " + ip);
        // if (ip.equals("10.53.29.153") ) {
        if (ip.equals(FRQConstantes.getIpHost())) {
            List<TareaDTO> listaTareas = null;
            try {
                listaTareas = tareasBI.obtieneTareas();
                Iterator<TareaDTO> it = listaTareas.iterator();
                while (it.hasNext()) {
                    TareaDTO o = it.next();
                    if (o.getIdTarea() == idTarea) {
                        schedArray.get(cveTarea).unscheduleJob("trigger" + cveTarea, "Trigger");
                        schedArray.get(cveTarea).deleteJob("Job" + cveTarea, "Ejemplo");
                        schedArray.remove(cveTarea);
                        jobDetailArray.remove(cveTarea);
                    }
                }
                try {
                    tareasBI.eliminaTarea(idTarea);
                } catch (Exception e) {
                    logger.info("Algo Ocurrió... ", e);
                    return false;
                }
            } catch (SchedulerException e) {
                logger.info("Algo Ocurrió... ", e);
                return false;
            }
        }
        return true;
    }
}
