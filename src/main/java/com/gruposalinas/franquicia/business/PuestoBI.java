package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.PuestoDAO;
import com.gruposalinas.franquicia.domain.PuestoDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PuestoBI {

    private static Logger logger = LogManager.getLogger(PuestoBI.class);

    @Autowired
    PuestoDAO puestoDAO;

    List<PuestoDTO> listaPuesto = null;

    List<PuestoDTO> listaPuestos = null;

    public List<PuestoDTO> obtienePuesto() {
        try {
            listaPuestos = puestoDAO.obtienePuesto();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Puesto");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaPuestos;
    }

    public List<PuestoDTO> obtienePuestoGeo() {
        try {
            listaPuestos = puestoDAO.obtienePuestoGeo();
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Puesto");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaPuestos;
    }

    public List<PuestoDTO> obtienePuesto(int idPuesto) {
        try {
            listaPuesto = puestoDAO.obtienePuesto(idPuesto);
        } catch (Exception e) {
            logger.info("No fue posible obtener datos de la tabla Puesto");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaPuesto;
    }

    public int insertaPuesto(PuestoDTO puesto) {
        int idPuesto = 0;
        try {
            idPuesto = puestoDAO.insertaPuesto(puesto);
        } catch (Exception e) {
            logger.info("No fue posible insertar el Puesto");
            UtilFRQ.printErrorLog(null, e);
        }

        return idPuesto;
    }

    public boolean actualizaPuesto(PuestoDTO puesto) {
        boolean respuesta = false;
        try {
            respuesta = puestoDAO.actualizaPuesto(puesto);
        } catch (Exception e) {
            logger.info("No fue posible actualizar el Puesto");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }

    public boolean eliminaPuesto(int idPuesto) {
        boolean respuesta = false;
        try {
            respuesta = puestoDAO.eliminaPuesto(idPuesto);
        } catch (Exception e) {
            logger.info("No fue posible borrar el Puesto");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;
    }
}
