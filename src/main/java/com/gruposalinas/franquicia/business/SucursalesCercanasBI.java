package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.SucursalesCercanasDAO;
import com.gruposalinas.franquicia.domain.SucursalesCercanasDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class SucursalesCercanasBI {

    private Logger logger = LogManager.getLogger(SucursalesCercanasBI.class);

    @Autowired
    SucursalesCercanasDAO sucursalesCercanasDAO;

    public List<SucursalesCercanasDTO> obtieneSucursales(String latitud, String longitud) {

        List<SucursalesCercanasDTO> sucursales = null;

        try {
            sucursales = sucursalesCercanasDAO.obtieneSucursales(latitud, longitud);

        } catch (Exception e) {

            logger.info("No fue posible obtener las sucursales cercanas");
            UtilFRQ.printErrorLog(null, e);
            return null;

        }

        return sucursales;
    }

}
