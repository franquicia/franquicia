package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.PedestalDigitalDAO;
import com.gruposalinas.franquicia.domain.AdminCanalPDDTO;
import com.gruposalinas.franquicia.domain.AdminMenuPDDTO;
import com.gruposalinas.franquicia.domain.AdminNegocioPDDTO;
import com.gruposalinas.franquicia.domain.AdminParametroPDDTO;
import com.gruposalinas.franquicia.domain.AdminTipoDocPDDTO;
import com.gruposalinas.franquicia.domain.AdmonEstatusTabletaPDDTO;
import com.gruposalinas.franquicia.domain.AdmonTabletPDDTO;
import com.gruposalinas.franquicia.domain.BachDescargasPDDTO;
import com.gruposalinas.franquicia.domain.CanalDTO;
import com.gruposalinas.franquicia.domain.CargaArchivoDTO;
import com.gruposalinas.franquicia.domain.LoginPDDTO;
import com.gruposalinas.franquicia.domain.MenuPedestalDigital;
import com.gruposalinas.franquicia.domain.NegocioDTO;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class PedestalDigitalBI {

    private static Logger logger = LogManager.getLogger(PedestalDigitalBI.class);

    @Autowired
    PedestalDigitalDAO pdDAO;

    LoginPDDTO login = null;
    List<MenuPedestalDigital> menu = null;
    List<CargaArchivoDTO> listaArchivo = null;
    List<AdmonTabletPDDTO> listaAdmonTablet = null;

    List<AdminNegocioPDDTO> negocioDTO = null;
    List<AdminCanalPDDTO> canalDTO = null;
    List<AdminParametroPDDTO> parametros = null;

    List<AdminMenuPDDTO> menuDTO = null;

    List<AdminTipoDocPDDTO> tipoDocDTO = null;
    List<AdmonEstatusTabletaPDDTO> estatusDTO = null;

    BachDescargasPDDTO descargas = null;
    List<BachDescargasPDDTO> desc = null;

    public LoginPDDTO login1(int id_usuario) throws Exception {
        try {
            login = pdDAO.login1(id_usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return login;
    }

    public List<MenuPedestalDigital> login2(int id_usuario, int id_perfil) throws Exception {
        try {
            menu = pdDAO.login2(id_usuario, id_perfil);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return menu;
    }

    public int cargaArchivoGeneral(CargaArchivoDTO archivo) throws Exception {
        int idArchivo = 0;
        idArchivo = pdDAO.cargaArchivosGeneral(archivo);
        return idArchivo;
    }

    public boolean actualizaArchivoGeneral(CargaArchivoDTO archivo) throws Exception {
        boolean respuesta = pdDAO.actualizaArchivo(archivo);
        return respuesta;
    }

    public List<CargaArchivoDTO> consultaArchivo(int idArchivo) throws Exception {
        listaArchivo = pdDAO.consulta(idArchivo);
        return listaArchivo;
    }

    public int cargaDatosAdmonTablet(AdmonTabletPDDTO datosTablet) throws Exception {
        int idArchivo = 0;
        idArchivo = pdDAO.cargaDatosAdmonTablet(datosTablet);
        return idArchivo;
    }

    public boolean actualizaDatosAdmonTablet(AdmonTabletPDDTO datosTablet) throws Exception {
        boolean respuesta = pdDAO.actualizaDatosAdmonTablet(datosTablet);
        return respuesta;
    }

    public List<AdmonTabletPDDTO> consultaDatosAdmonTablet(int idDatosTablet) throws Exception {
        listaAdmonTablet = pdDAO.consultaDatosAdmonTablet(idDatosTablet);
        return listaAdmonTablet;
    }

    public List<AdminNegocioPDDTO> admonNegocio(NegocioDTO negocio, int opcion) throws Exception {
        negocioDTO = pdDAO.admonNegocio(negocio, opcion);
        return negocioDTO;
    }

    public List<AdminCanalPDDTO> admonCanal(CanalDTO canal, int opcion) throws Exception {
        canalDTO = pdDAO.admonCanal(canal, opcion);
        return canalDTO;
    }

    public List<AdminParametroPDDTO> admonParametros(AdminParametroPDDTO parametro, int opcion) throws Exception {
        parametros = pdDAO.admonParametros(parametro, opcion);
        return parametros;
    }

    public List<AdminMenuPDDTO> admonMenu(AdminMenuPDDTO menu, int opcion) throws Exception {
        menuDTO = pdDAO.admonMenu(menu, opcion);
        return menuDTO;
    }

    public List<AdminTipoDocPDDTO> admonTipoDoc(AdminTipoDocPDDTO tipoDoc, int opcion) throws Exception {
        tipoDocDTO = pdDAO.admonTipoDoc(tipoDoc, opcion);
        return tipoDocDTO;
    }

    public List<AdmonEstatusTabletaPDDTO> admonEstatusTablet(AdmonEstatusTabletaPDDTO estatusTablet, int opcion) throws Exception {
        estatusDTO = pdDAO.admonEstatusTablet(estatusTablet, opcion);
        return estatusDTO;
    }

    public BachDescargasPDDTO admonDescarga(BachDescargasPDDTO descarga, int opcion) throws Exception {
        descargas = pdDAO.admonDescarga(descarga, opcion);
        return descargas;
    }

    public List<BachDescargasPDDTO> admonDesc(BachDescargasPDDTO descarga, int opcion) throws Exception {
        desc = pdDAO.admonDesc(descarga, opcion);
        return desc;
    }

    public List<AdminParametroPDDTO> admonParametrosByDescRef(Integer op, AdminParametroPDDTO obj) throws Exception {
        parametros = pdDAO.admonParametrosByDescRef(op, obj);
        return parametros;
    }
}
