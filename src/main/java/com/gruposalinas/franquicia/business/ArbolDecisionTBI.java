package com.gruposalinas.franquicia.business;

import com.gruposalinas.franquicia.dao.ArbolDecisionTDAO;
import com.gruposalinas.franquicia.domain.ArbolDecisionDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ArbolDecisionTBI {

    private static Logger logger = LogManager.getLogger(ArbolDecisionTBI.class);

    List<ArbolDecisionDTO> listaArbolDecision = null;
    @Autowired
    ArbolDecisionTDAO arbolDecisionDAO;

    public List<ArbolDecisionDTO> buscaArbolDecisionTemp(int idChecklist) {

        try {
            listaArbolDecision = arbolDecisionDAO.buscaArbolDecisionTemp(idChecklist);
        } catch (Exception e) {
            logger.info("No fue posible obtener el Arbol de Decision");
            UtilFRQ.printErrorLog(null, e);
        }

        return listaArbolDecision;

    }

    public int insertaArbolDecisionTemp(ArbolDecisionDTO bean) {

        int respuesta = 0;

        try {

            respuesta = arbolDecisionDAO.insertaArbolDecisionTemp(bean);

        } catch (Exception e) {
            logger.info("No fue posible insertar el registro del Arbol de Decision");
            UtilFRQ.printErrorLog(null, e);

        }

        return respuesta;
    }

    public boolean actualizaArbolDecisionTemp(ArbolDecisionDTO bean) {

        boolean respuesta = false;

        try {

            respuesta = arbolDecisionDAO.actualizaArbolDecisionTemp(bean);

        } catch (Exception e) {
            logger.info("No fue posible actualizarel registro del Arbol de Decision");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;

    }

    public boolean eliminaArbolDecisionTemp(int idArbolDecision) {

        boolean respuesta = false;

        try {
            respuesta = arbolDecisionDAO.eliminaArbolDecisionTemp(idArbolDecision);
        } catch (Exception e) {
            logger.info("No fue posible eliminar el registro del Arbol de Decision");
            UtilFRQ.printErrorLog(null, e);
        }

        return respuesta;

    }

}
