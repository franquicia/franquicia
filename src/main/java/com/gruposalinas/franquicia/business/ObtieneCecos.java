/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.franquicia.business;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gruposalinas.franquicia.dao.CecoWSDAO;
import com.gruposalinas.franquicia.domain.ListaTablaEKT;
import com.gruposalinas.franquicia.domain.RespuestaEstructurasWSDTO;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

/**
 *
 * @author kramireza
 */
@Component
public class ObtieneCecos {

    private static Logger logger = LogManager.getLogger(ObtieneCecos.class);

    @Autowired

    CecoWSDAO cecoDAO;

    public String getTokenCecos() {

        String usuario = FRQConstantes.getUsuarioWSCeco();
        String password = FRQConstantes.getPasswordWSCeco();
        String salida = null;

        BufferedReader rd = null;
        OutputStreamWriter ou = null;
        InputStreamReader inputStream = null;
        try {

            String data = URLEncoder.encode("usuario", "UTF-8") + "=" + URLEncoder.encode(usuario, "UTF-8");
            data += "&" + URLEncoder.encode("contrasena", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");
            URL url = new URL(FRQConstantes.getURLTokenCecos() + "?" + data);
            // URL url = new URL(http://10.50.109.68:8080/cecows/rest/token");
            //logger.info("url : " +url);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("charset", "utf-8");
            conn.setUseCaches(false);

            // Obtener el estado del recurso
            int statusCode = conn.getResponseCode();

            System.out.println("statusCode " + statusCode);

            StringBuffer res = new StringBuffer();
            inputStream = new InputStreamReader(conn.getInputStream());
            rd = new BufferedReader(inputStream);

            String line = "";

            while ((line = rd.readLine()) != null) {
                res.append(line);
            }
            salida = res.toString();
        } catch (Exception e) {
            salida = null;
        } finally {
            try {

                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }

            try {
                if (ou != null) {
                    ou.close();
                }
            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }
            try {
                if (inputStream != null) {
                    inputStream.close();
                }

            } catch (Exception e) {
                //logger.info("Algo ocurrio al intentar cerrar los Streams " + e.getMessage());
            }
        }
        return salida;
    }

    @SuppressWarnings("unused")
    public void getData(String token) throws IOException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        URL urlConstant = new URL(FRQConstantes.getURLConsultaCecos());
        Map params = new LinkedHashMap<>();
        params.put("token", token);
        StringBuilder postData = new StringBuilder();
        for (Iterator it = params.entrySet().iterator(); it.hasNext();) {
            Map.Entry param = (Map.Entry) it.next();
            if (postData.length() != 0) {
                postData.append('&');
            }
            postData.append(URLEncoder.encode((String) param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");
        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        HttpURLConnection conn = (HttpURLConnection) urlConstant.openConnection();
        RespuestaEstructurasWSDTO respuesta = new RespuestaEstructurasWSDTO();
        try {
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(50000);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            String output;
            stopWatch.stop();
            System.out.println("Tiempo de conexión:" + stopWatch.getTotalTimeMillis() + " Milisegundos");
            StringBuilder stringBuilder = new StringBuilder();
            while ((output = br.readLine()) != null) {
                stringBuilder.append(output);
            }

            respuesta = gson.fromJson(stringBuilder.toString(), RespuestaEstructurasWSDTO.class);
            HashMap<String, String> subNegocios = new HashMap<>();
            HashMap<Integer, String> unidadNegocio = new HashMap<>();
            HashMap<Integer, String> estatusCeco = new HashMap<>();
            boolean resultado = false;
            if (!respuesta.getListaTablaEKT().isEmpty()) {
                //PRIMERO DEPURAMOS LA TABLA ANTES DE INGRESAR LOS DATOS
                boolean depuro = cecoDAO.depuraTabla(FRQConstantes.getDepuraTabla());
                System.out.println("Termina depura tabla: " + depuro);
                if (depuro) {

                    System.out.println("Tamaño de arreglo:: " + respuesta.getListaTablaEKT().size());
                    for (ListaTablaEKT cecoEstruc : respuesta.getListaTablaEKT()) {
                        //          subNegocios.put(cecoEstruc.getIdSubnegocio(), cecoEstruc.getDesSubnegocio());
                        //        unidadNegocio.put(cecoEstruc.getIdUnidadNegocio(), cecoEstruc.getNomunidadnegocio());
                        //    estatusCeco.put(cecoEstruc.getIdEstatus(), cecoEstruc.getNomEstatus());

                        resultado = cecoDAO.insertaCeco(cecoEstruc);
                    }
                    //     subNegocios.forEach((k, v) -> System.out.println("Key: " + k + ": Value: " + v));
                    //     System.out.println("------------------------");
                    //     unidadNegocio.forEach((k, v) -> System.out.println("Key: " + k + ": Value: " + v));
                    //     System.out.println("------------------------");
                    //     estatusCeco.forEach((k, v) -> System.out.println("Key: " + k + ": Value: " + v));
                    System.out.println("resultado ::" + resultado);
                }
            } else {
                System.out.println("La lista de los cecos viene vacia.");
            }
        } catch (Exception e) {
            logger.info("Ocurrió una excepcion en la extracción de la información de los CECOS de Estructura.:" + e);
        } finally {
            conn.disconnect();
        }

    }
}
