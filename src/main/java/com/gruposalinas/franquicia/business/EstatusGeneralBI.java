package com.gruposalinas.franquicia.business;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.franquicia.dao.CargaArchivosPedestalDAO;
import com.gruposalinas.franquicia.dao.EstatusGeneralDAO;
import com.gruposalinas.franquicia.dao.PedestalDigitalDAO;
import com.gruposalinas.franquicia.domain.BachDescargasPDDTO;
import com.gruposalinas.franquicia.domain.BatchDoctoPDDTO;
import com.gruposalinas.franquicia.domain.CatalogoEstatusDocPDDTO;
import com.gruposalinas.franquicia.domain.CecoDataPDDTO;
import com.gruposalinas.franquicia.domain.CecoInfoGraphPDDTO;
import com.gruposalinas.franquicia.domain.CecosByNegPDDTO;
import com.gruposalinas.franquicia.domain.DataTabletPDDTO;
import com.gruposalinas.franquicia.domain.DatosTablaEGDTO;
import com.gruposalinas.franquicia.domain.FolioDetailPDDTO;
import com.gruposalinas.franquicia.domain.FoliosEstatusPDDTO;
import com.gruposalinas.franquicia.domain.GeoDataPDDTO;
import com.gruposalinas.franquicia.domain.HistoricTabletDataPDDTO;
import com.gruposalinas.franquicia.domain.IndicadoresDataEGDTO;
import com.gruposalinas.franquicia.domain.IndicadoresDataETDTO;
import com.gruposalinas.franquicia.domain.ListaCecoPorCSVPDDTO;
import com.gruposalinas.franquicia.domain.ListaEstatusTabletaPDDTO;
import com.gruposalinas.franquicia.domain.ParamNegocioPDDTO;
import com.gruposalinas.franquicia.domain.QuejasPDDTO;
import com.gruposalinas.franquicia.domain.RepDetaCecoPDDTO;
import com.gruposalinas.franquicia.domain.RepGralCecoPDDTO;
import com.gruposalinas.franquicia.domain.SucEstTabPDDTO;
import com.gruposalinas.franquicia.domain.SucursalInfoPDDTO;
import com.gruposalinas.franquicia.domain.SucursalTareasListPDDTO;
import com.gruposalinas.franquicia.domain.TablaEstatusGeneralDTO;
import com.gruposalinas.franquicia.domain.TabletDataFilterETDTO;
import com.gruposalinas.franquicia.domain.TabletInformationPDDTO;
import com.gruposalinas.franquicia.domain.TabletPerformancePDDTO;
import com.gruposalinas.franquicia.domain.TaskInTabletPDDAO;
import com.gruposalinas.franquicia.domain.TipoQuejaPDDTO;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class EstatusGeneralBI {

    @Autowired
    EstatusGeneralDAO estatusGeneral;

    @Autowired
    CargaArchivosPedestalDAO cargaArchivos;

    @Autowired
    PedestalDigitalDAO pdDAO;

    List<IndicadoresDataEGDTO> indicadores = null;
    List<IndicadoresDataETDTO> indicadoresET = null;
    List<DatosTablaEGDTO> datos = null;
    List<TablaEstatusGeneralDTO> tabla = null;
    List<ListaEstatusTabletaPDDTO> lista = null;
    List<TabletDataFilterETDTO> tabletData = null;

    private static final Logger logger = LogManager.getLogger(EstatusGeneralBI.class);

    public List<IndicadoresDataEGDTO> getIndicadoresEG(int op, String negocio) {
        indicadores = estatusGeneral.getIndicadoresEG(op, negocio);
        return indicadores;
    }

    public List<DatosTablaEGDTO> getDatosGraficaEG(int op, int anio, String negocio) {
        datos = estatusGeneral.getDatosGraficaEG(op, anio, negocio);
        return datos;
    }

    public List<TablaEstatusGeneralDTO> getDataForExcelEG(int op, int anio, String negocio) {
        tabla = estatusGeneral.getDataForExcelEG(op, anio, negocio);
        return tabla;
    }

    public List<ListaEstatusTabletaPDDTO> getListaEstatusTableta(int op) {
        lista = estatusGeneral.getListaEstatusTableta(op);
        return lista;
    }

    @SuppressWarnings("unused")
    public String insertEstatusTableta(int op, List<ListaEstatusTabletaPDDTO> listaEstatus, String data) throws Exception {

        String resp = null;
        List<ListaEstatusTabletaPDDTO> list = listaEstatus;
        JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

        DataTabletPDDTO internData = new DataTabletPDDTO(
                jsonObject.get("numSerie").getAsString(),
                jsonObject.get("fabricante").getAsString(),
                jsonObject.get("modelo").getAsString(),
                jsonObject.get("sistemaOperativo").getAsString(),
                jsonObject.get("versionApp").getAsString(),
                jsonObject.get("totalEspacioAlmacenamiento").getAsString(),
                jsonObject.get("ramTotal").getAsString(),
                jsonObject.get("espacioLibreAlmacenamiento").getAsString(),
                jsonObject.get("ramLibre").getAsString()
        );

        DataTabletPDDTO historicData = new DataTabletPDDTO(
                jsonObject.get("numSerie").getAsString(),
                jsonObject.get("alimentacion").getAsString(),
                jsonObject.get("intensidadSenial").getAsString(),
                jsonObject.get("nivelBateria").getAsString(),
                jsonObject.get("saludBateria").getAsString(),
                jsonObject.get("temperaturaBateria").getAsString(),
                "1"
        );

        // Inserta datos internos de la tableta
        resp = insertInternalDataTablet(0, internData);
        boolean isCritical = false;

        if (resp.equals("0")) {
            // Se insertan datos historicos
            op = 1; // Se cambia el valor de op, para consumir insertHistoricDataTablet

            // 1 - Intensidad de señal - intensidadSenial - [ MALA - 2 || DEBIL - 3 || MEDIA - 4 || BUENA - 5 || EXCELENTE - 6 ]
            if (historicData.getIntensidadSenial().equals("MALA")) {
                resp = estatusGeneral.insertHistoricDataTablet(1, historicData.getNumSerie(), 2, historicData.getActivo());
                // isCritical = true;
            } else if (historicData.getIntensidadSenial().equals("DEBIL")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 3, historicData.getActivo());
                // isCritical = true;
            } else if (historicData.getIntensidadSenial().equals("MEDIA")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 4, historicData.getActivo());
            } else if (historicData.getIntensidadSenial().equals("BUENA")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 5, historicData.getActivo());
            } else if (historicData.getIntensidadSenial().equals("EXCELENTE")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 6, historicData.getActivo());
            }

            // 8 - Nivel de batería - nivelBateria - [ 71-100 - 9 || 21-70 - 10 || 0-20 - 11]
            if (historicData.getNivelBateria() != "") {
                int nivelBateria = Integer.parseInt(historicData.getNivelBateria());

                if (nivelBateria >= 71 && nivelBateria <= 100) {
                    resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 9, historicData.getActivo());
                } else if (nivelBateria >= 21 && nivelBateria <= 70) {
                    resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 10, historicData.getActivo());
                } else if (nivelBateria >= 0 && nivelBateria <= 20) {
                    resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 11, historicData.getActivo());
                    isCritical = true;
                }
            }

            // 12 - Alimentación - alimentacion - [ CONECTADO - 13 || NO CONECTADO - 14 ]
            if (historicData.getAlimentacion().equals("CONECTADO")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 13, historicData.getActivo());
            } else if (historicData.getAlimentacion().equals("NO CONECTADO")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 14, historicData.getActivo());
                isCritical = true;
            }

            // 15 - Temperatura - temperatura [ 76-100 - 16 || 26-75 - 17 || 0-25 - 18 ]
            if (historicData.getTemperaturaBateria() != "") {
                int temperaturaBateria = Integer.parseInt(historicData.getTemperaturaBateria());

                if (temperaturaBateria >= 76 && temperaturaBateria <= 100) {
                    resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 16, historicData.getActivo());
                    isCritical = true;
                } else if (temperaturaBateria >= 26 && temperaturaBateria <= 75) {
                    resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 17, historicData.getActivo());
                } else if (temperaturaBateria >= 0 && temperaturaBateria <= 25) {
                    resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 18, historicData.getActivo());
                }
            }

            // 19 - Salud de la batería - saludBateria - [ COLD - 20 || GOOD - 21 || UNKNOWN - 22 || DEAD - 23 || OVERHEAT - 24 || OVER_VOLTAGE - 25 ]
            if (historicData.getSaludBateria().equals("COLD")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 20, historicData.getActivo());
            } else if (historicData.getSaludBateria().equals("GOOD")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 21, historicData.getActivo());
            } else if (historicData.getSaludBateria().equals("UNKNOWN")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 22, historicData.getActivo());
            } else if (historicData.getSaludBateria().equals("DEAD")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 23, historicData.getActivo());
                isCritical = true;
            } else if (historicData.getSaludBateria().equals("OVERHEAT")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 24, historicData.getActivo());
                isCritical = true;
            } else if (historicData.getSaludBateria().equals("OVER_VOLTAGE")) {
                resp = estatusGeneral.insertHistoricDataTablet(op, historicData.getNumSerie(), 25, historicData.getActivo());
                isCritical = true;
            }

            if (isCritical) {
                String _resp = cargaArchivos.updateTabletCriticStatus(0, historicData.getNumSerie(), 2);
            } else {
                String _resp = cargaArchivos.updateTabletCriticStatus(0, historicData.getNumSerie(), 1);
            }
        } else {
            // Error, no se insertaron los datos; Se interrumpe el flujo
            //throw new Exception("insertEstatusTableta - No se pudo realizar la inserción de los datos internos de la tableta");
            logger.info("insertEstatusTableta - No se pudo realizar la inserción de los datos internos de la tableta. " + " Datos de internData: " + "Numero de Serie: " + internData.getNumSerie() + "- Fabricante: "
                    + internData.getFabricante() + "- Modelo: " + internData.getModelo() + "- Sistema Operativo: " + internData.getSistemaOperativo() + "- VersionApp: " + internData.getVersionApp() + "- Espacio Total Almacenamiento: "
                    + internData.getTotalEspacioAlmacenamiento() + "- Ram Total: " + internData.getRamTotal() + "- Espacio Libre almacenamiento:  " + internData.getEspacioDisp() + "- Ram Disponible: "
                    + internData.getRamDisp() + "Datos de historicData: " + "Numero de Serie: " + historicData.getNumSerie() + "- Alimentacion: " + historicData.getAlimentacion() + "- Intensidad Señal: "
                    + historicData.getIntensidadSenial() + "- Nivel Bateria: " + historicData.getNivelBateria() + "- Salud Bateria: " + historicData.getSaludBateria() + "- Temperatura Bateria: " + historicData.getTemperaturaBateria());

        }
        return resp;
    }

    public String insertInternalDataTablet(int op, DataTabletPDDTO data) {
        String resp = null;
        resp = estatusGeneral.insertInternalDataTablet(op, data);
        return resp;
    }

    public String insertHistoricDataTablet(int op, DataTabletPDDTO data) {
        String resp = null;
        // resp = estatusGeneral.insertHistoricDataTablet(op, numSerie, idTabletEdo, activo);
        return resp;
    }

    public List<IndicadoresDataETDTO> getIndicadoresET(int op) {
        indicadoresET = estatusGeneral.getIndicadoresET(op);
        return indicadoresET;
    }

    public List<TabletDataFilterETDTO> getTabletBySucursal(int op, int numSucursal) {
        tabletData = estatusGeneral.getTabletBySucursal(op, numSucursal);
        return tabletData;
    }

    public List<TabletDataFilterETDTO> getTabletByTerritorio(int op, String ceco, int estatus, int rendimiento) {
        tabletData = estatusGeneral.getTabletByTerritorio(op, ceco, estatus, rendimiento);
        return tabletData;
    }

    public List<TabletDataFilterETDTO> getTabletByPais(int op, String geo, int estatus, int rendimiento) {
        tabletData = estatusGeneral.getTabletByPais(op, geo, estatus, rendimiento);
        return tabletData;
    }

    public String insertCriticTablet(int op, String numSerie, int estatus) {
        String resp = estatusGeneral.insertCriticTablet(op, numSerie, estatus);
        return resp;
    }

    public List<TabletInformationPDDTO> getTabletInfo(int op, int idTableta) {
        List<TabletInformationPDDTO> resp = estatusGeneral.getTabletInfo(op, idTableta);
        return resp;
    }

    public List<TabletPerformancePDDTO> getTabletPerformance(int op, int idTableta) {
        List<TabletPerformancePDDTO> resp = estatusGeneral.getTabletPerformance(op, idTableta);
        return resp;
    }

    public List<SucursalInfoPDDTO> getSucursalInfo(int op, int idTableta) {
        List<SucursalInfoPDDTO> resp = estatusGeneral.getSucursalInfo(op, idTableta);
        return resp;
    }

    public List<SucursalTareasListPDDTO> getSucursalTable(int op, int idTableta) {
        List<SucursalTareasListPDDTO> resp = estatusGeneral.getSucursalTable(op, idTableta);
        return resp;
    }

    public List<TaskInTabletPDDAO> getTasksInTablet(int op, int sucursal, int negocio) {
        List<TaskInTabletPDDAO> resp = estatusGeneral.getTasksInTablet(op, sucursal, negocio);
        return resp;
    }

    public List<TaskInTabletPDDAO> getLatestTaskInTablet(int op, int sucursal, int tipoDocumento) {
        List<TaskInTabletPDDAO> resp = estatusGeneral.getLatestTaskInTablet(op, sucursal, tipoDocumento);
        return resp;
    }

    public List<FolioDetailPDDTO> getFolioDetail(int op, int idFolio) {
        List<FolioDetailPDDTO> resp = estatusGeneral.getFolioDetail(op, idFolio);
        return resp;
    }

    public String updateFechaVisualizacionFolio(int op, int idFolio, String fecha) {
        String resp = estatusGeneral.updateFechaVisualizacionFolio(op, idFolio, fecha);
        return resp;
    }

    public List<ListaCecoPorCSVPDDTO> addSucursalesFolio(int op, int idFolio, String cecos) {
        List<ListaCecoPorCSVPDDTO> resp = estatusGeneral.addSucursalesFolio(op, idFolio, cecos);
        return resp;
    }

    public List<String> changeDocumentFolio(int op, int documentoNuevo, int documentoAnterior) {
        List<String> resp = estatusGeneral.changeDocumentFolio(op, documentoNuevo, documentoAnterior);
        return resp;
    }

    public String validaDocumentoDeFolio(int op, int idDocumentoAnt) {
        String resp = estatusGeneral.validaDocumentoDeFolio(op, idDocumentoAnt);
        return resp;
    }

    public String updateStatusOfTaskInTablet(int op, int sucursal, int negocio, int tipoDocumento, int idDocumento, int idDocPedestal, int idEstatus, int activo) {
        String resp = estatusGeneral.updateStatusOfTaskInTablet(op, sucursal, negocio, tipoDocumento, idDocumento, idDocPedestal, idEstatus, activo);
        return resp;
    }

    public String updateStatusOfTaskInTablet2(int op, int estatus, int idDocPedestal) {
        String resp = estatusGeneral.updateStatusOfTaskInTablet2(op, estatus, idDocPedestal);
        return resp;
    }

    public String inactivaLanzamientosDocFolio(int op, int idDoc, int activo) {
        String resp = estatusGeneral.inactivaLanzamientosDocFolio(op, idDoc, activo);
        return resp;
    }

    public List<QuejasPDDTO> getQuejas(int op, int idComentario) {
        List<QuejasPDDTO> resp = estatusGeneral.getQuejas(op, idComentario);
        return resp;
    }

    public String insertQueja(int op, String data) {

        Gson gson = new Gson();
        String resp = null;

        try {
            JsonElement jsonEl = gson.fromJson(data, JsonElement.class);
            JsonObject jsonObj = jsonEl.getAsJsonObject();
            resp = estatusGeneral.insertQueja(op, jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    public List<RepGralCecoPDDTO> filtroRepGralCec(int op, String fechaIni, String fechaEnv, String doctoVis, String categoria, String ceco) {
        List<RepGralCecoPDDTO> resp = estatusGeneral.filtroRepGralCec(op, fechaIni, fechaEnv, doctoVis, categoria, ceco);
        return resp;
    }

    public List<RepGralCecoPDDTO> filtroRepGralGeo(int op, String fechaIni, String fechaEnv, String doctoVis, String categoria, String geo) {
        List<RepGralCecoPDDTO> resp = estatusGeneral.filtroRepGralGeo(op, fechaIni, fechaEnv, doctoVis, categoria, geo);
        return resp;
    }

    public List<RepDetaCecoPDDTO> filtroRepDetaCec(int op, String fechaIni, String fechaEnv, String doctoVis, String tipoEnvio, String categoria, String ceco) {
        List<RepDetaCecoPDDTO> resp = estatusGeneral.filtroRepDetaCec(op, fechaIni, fechaEnv, doctoVis, tipoEnvio, categoria, ceco);
        return resp;
    }

    public List<RepDetaCecoPDDTO> filtroRepDetaGeo(int op, String fechaIni, String fechaEnv, String doctoVis, String tipoEnvio, String categoria, String geo) {
        List<RepDetaCecoPDDTO> resp = estatusGeneral.filtroRepDetaGeo(op, fechaIni, fechaEnv, doctoVis, tipoEnvio, categoria, geo);
        return resp;
    }

    public List<RepDetaCecoPDDTO> filtroRepDetaIdFolio(int op, int idFolio) {
        List<RepDetaCecoPDDTO> resp = estatusGeneral.filtroRepDetaIdFolio(op, idFolio);
        return resp;
    }

    public String insertGeo(int op, int idGeo, int tipo, int idcc, String desc, int dependeDe, int activo) {
        String resp = estatusGeneral.insertGeo(op, idGeo, tipo, idcc, desc, dependeDe, activo);
        return resp;
    }

    public String updateGeo(int op, int idGeo, int tipo, int idcc, String desc, int dependeDe, int activo) {
        String resp = estatusGeneral.updateGeo(op, idGeo, tipo, idcc, desc, dependeDe, activo);
        return resp;
    }

    public List<GeoDataPDDTO> getGeo(int op, int idGeo, int tipo, int dependeDe) {
        List<GeoDataPDDTO> resp = estatusGeneral.getGeo(op, idGeo, tipo, dependeDe);
        return resp;
    }

    public String insertCecoData(Integer op, String data) {

        String _json = data.replaceAll("}", "},");
        _json = _json.replace("\n", "").replace("\r", "");
        _json = _json.substring(0, _json.length() - 1);
        _json = "[" + _json + "]";

        CecosJson _logs = new CecosJson();
        Gson gson = new Gson();

        JsonElement je = gson.fromJson(_json, JsonElement.class);
        JsonArray ja = je.getAsJsonArray();

        try {
            ArrayList<String> cecoDataResp = new ArrayList<String>();
            ArrayList<String> geoUpdateResp = new ArrayList<String>();

            for (JsonElement jsonElement : ja) {
                JsonObject js = jsonElement.getAsJsonObject();

                String cecoResp = estatusGeneral.insertCecoData(op,
                        js.get("idCeco").getAsString(),
                        js.get("idSucursal").getAsInt(),
                        js.get("nombre").getAsString(),
                        js.get("cecoSuperior").getAsString(),
                        js.get("idPais").getAsInt(),
                        js.get("idEstado").getAsInt(),
                        js.get("idMunicipio").getAsInt(),
                        js.get("calle").getAsString(),
                        js.get("idNivel").getAsInt(),
                        js.get("Negocio").getAsInt(),
                        js.get("Canal").getAsInt(),
                        js.get("nombreContacto").getAsInt(),
                        js.get("cp").getAsString(),
                        js.get("idGeografia").getAsInt(),
                        js.get("estado").getAsString(),
                        js.get("municipio").getAsString(),
                        js.get("colonia").getAsString(),
                        js.get("numeroExt").getAsString(),
                        js.get("numeroInt").getAsString(),
                        js.get("activo").getAsInt());

                JsonElement _jsonElement = gson.fromJson(cecoResp, JsonElement.class);
                JsonObject _jsonObject = _jsonElement.getAsJsonObject();

                if (_jsonObject.get("respuesta").getAsInt() == 0) {
                    cecoDataResp.add(_jsonObject.get("idCeco").getAsString());

                    String geoResp = estatusGeneral.updateGeoInCeco(1,
                            js.get("idCeco").getAsString(),
                            js.get("idPais").getAsInt(),
                            js.get("idEstado").getAsInt(),
                            js.get("idMunicipio").getAsInt());

                    JsonElement _je = gson.fromJson(geoResp, JsonElement.class);
                    JsonObject _jo = _je.getAsJsonObject();

                    if (_jo.get("respuesta").getAsInt() == 0) {
                        geoUpdateResp.add(_jo.get("idGeo").getAsString());
                    } else {
                        geoUpdateResp.add("El idGeo del ceco " + js.get("idCeco").getAsString() + " no fue insertado / actualizado");
                    }
                } else {
                    cecoDataResp.add("El ceco " + js.get("idCeco").getAsString() + " no fue insertado / actualizado");
                    geoUpdateResp.add("El idGeo del ceco " + js.get("idCeco").getAsString() + " no fue insertado / actualizado");
                }
            }

            _logs.setMsj("0");
            _logs.setCecoLogs(cecoDataResp);
            _logs.setGeoLogs(geoUpdateResp);
        } catch (Exception e) {
            logger.info("insertCecoData - Algo ocurrio al insertar los cecos");
            _logs.setMsj("1");
            _logs.setCecoLogs(new ArrayList<String>());
            _logs.setGeoLogs(new ArrayList<String>());
            return gson.toJson(_logs, CecosJson.class);
        }

        return gson.toJson(_logs, CecosJson.class);
    }

    public String updateGeoInCeco(int op, String idCeco, int idPais, int idEstado, int idMunicipio) {
        String resp = estatusGeneral.updateGeoInCeco(op, idCeco, idPais, idEstado, idMunicipio);
        return resp;
    }

    public List<GeoDataPDDTO> getGeoDataById(int op, int idGeo) {
        List<GeoDataPDDTO> resp = estatusGeneral.getGeoDataById(op, idGeo);
        return resp;
    }

    public String updateGeoData(int op, String data) {

        String resp = null;
        Gson gson = new Gson();
        JsonElement _jsonElement = gson.fromJson(data, JsonElement.class);
        JsonArray _jsonArray = _jsonElement.getAsJsonArray();

        for (JsonElement jsonElement : _jsonArray) {
            // Consulta si existe ese valor en BD.
            JsonObject _jo = jsonElement.getAsJsonObject();
            List<GeoDataPDDTO> geo = estatusGeneral.getGeoDataById(2, _jo.get("idGeografia").getAsInt());

            if (geo.get(0).getIdGeografia() > 0) {
                // Actualiza
            } else {
                // Inserta
            }
        }

        return resp;
    }

    public String insertTipoQueja(int op, int idComent, String coment, int activo) {
        String resp = estatusGeneral.insertTipoQueja(op, idComent, coment, activo);
        return resp;
    }

    public String updateTipoQueja(int op, int idComent, String coment, int activo) {
        String resp = estatusGeneral.updateTipoQueja(op, idComent, coment, activo);
        return resp;
    }

    public List<TipoQuejaPDDTO> getTipoQueja(int op, int idTipoCat) {
        List<TipoQuejaPDDTO> resp = estatusGeneral.getTipoQueja(op, idTipoCat);
        return resp;
    }

    /**
     * *****************************
     */
    /**
     * PEDESTAL DIGITAL SCHEDULER *
     */
    /**
     * *****************************
     */
    public String actualizaEstatusFoliosPD(String fecha) {

        List<BatchDoctoPDDTO> list = null;

        try {
            list = estatusGeneral.actualizaEstatusFoliosPD(0, fecha);

            if (list.size() > 0) {
                for (BatchDoctoPDDTO batchDoctoPDDTO : list) {
                    String _resp = estatusGeneral.actualizaEstatusDoctoPD(1,
                            batchDoctoPDDTO.getIdDocumento(),
                            batchDoctoPDDTO.getIdTipoDocto());
                    logger.info("actualizaEstatusFoliosPD - " + _resp);
                }
            }
        } catch (Exception e) {
            // e.printStackTrace();
            logger.info("actualizaEstatusFoliosPD - Algo ocurrio al ejecutar el proceso batch");
            return "{\"msj\":1}";
        }

        return "{\"msj\":0}";
    }

    public String insertEstatusDocInCat(int op, int idEstatusDoc, String descEstatus, int activo) {
        String _resp = estatusGeneral.insertEstatusDocInCat(op, idEstatusDoc, descEstatus, activo);
        return _resp;
    }

    public String updateEstatusDocInCat(int op, int idEstatusDoc, String descEstatus, int activo) {
        String _resp = estatusGeneral.updateEstatusDocInCat(op, idEstatusDoc, descEstatus, activo);
        return _resp;
    }

    public List<CatalogoEstatusDocPDDTO> getEstatusDocInCat(int op) {
        List<CatalogoEstatusDocPDDTO> _resp = estatusGeneral.getEstatusDocInCat(op);
        return _resp;
    }

    public List<CecoDataPDDTO> getCecoDataByCecoPadre(int op, int dependeDe) {
        List<CecoDataPDDTO> _resp = estatusGeneral.getCecoDataByCecoPadre(op, dependeDe);
        return _resp;
    }

    public String actualizaEstatusTabletasAPK(String fecha) {

        BachDescargasPDDTO param = new BachDescargasPDDTO();
        param.setFechaActu(fecha);
        param.setTipoDescarga("A");

        try {
            BachDescargasPDDTO resp = pdDAO.admonDescarga(param, 0);
        } catch (Exception e) {
            // e.printStackTrace();
            logger.info("actualizaEstatusFoliosPD - Algo ocurrio al ejecutar el proceso batch");
            return "{\"msj\":1}";
        }

        return "{\"msj\":0}";
    }

    public String actualizaEstatusTabletasDOC(String fecha) {

        BachDescargasPDDTO param = new BachDescargasPDDTO();
        param.setFechaActu(fecha);
        param.setTipoDescarga("D");

        try {
            BachDescargasPDDTO resp = pdDAO.admonDescarga(param, 1);
        } catch (Exception e) {
            // e.printStackTrace();
            logger.info("actualizaEstatusFoliosPD - Algo ocurrio al ejecutar el proceso batch");
            return "{\"msj\":1}";
        }

        return "{\"msj\":0}";
    }

    public String validaMantenimientoTableta(int op, String sucursal) {

        if (sucursal.length() == 2) {
            sucursal = "00" + sucursal;
        } else if (sucursal.length() == 3) {
            sucursal = "0" + sucursal;
        }

        String _resp = estatusGeneral.validaMantenimientoTableta(op, sucursal);
        return _resp;
    }

    public List<FoliosEstatusPDDTO> getEstatusFoliosTableta(int op, String sucursal, int idCategoria) {
        List<FoliosEstatusPDDTO> _resp = estatusGeneral.getEstatusFoliosTableta(op, sucursal, idCategoria);
        return _resp;
    }

    public List<HistoricTabletDataPDDTO> getHistoricTabletData(int op, int idSucursal) {
        List<HistoricTabletDataPDDTO> _resp = estatusGeneral.getHistoricTabletData(op, idSucursal);
        return _resp;
    }

    public String actualizaRaizEnCategoria(int op, String idTipoDoc) {
        String _resp = estatusGeneral.actualizaRaizEnCategoria(op, idTipoDoc);
        return _resp;
    }

    public String updateTabletsEstatus() {
        String _resp = estatusGeneral.updateTabletsEstatus();
        return _resp;
    }

    public ArrayList<ParamNegocioPDDTO> getParamNegocios() {
        ArrayList<ParamNegocioPDDTO> resp = estatusGeneral.getParamNegocios();
        return resp;
    }

    public List<IndicadoresDataETDTO> getIndiByNeg(int op, String negocio) {
        indicadoresET = estatusGeneral.getIndiByNeg(op, negocio);
        return indicadoresET;
    }

    public List<CecosByNegPDDTO> getCecosNegocio(String tipo, String ceco, String negocio) {
        List<CecosByNegPDDTO> list = estatusGeneral.getCecosNegocio(tipo, ceco, negocio);
        return list;
    }

    public List<CecoInfoGraphPDDTO> getCecoInfoGraph(String op, String sucursal, String ceco) {
        List<CecoInfoGraphPDDTO> list = estatusGeneral.getCecoInfoGraph(op, sucursal, ceco);
        return list;
    }

    public List<SucEstTabPDDTO> getSucEstTabByCeco(String op, String negocio, String ceco) {
        List<SucEstTabPDDTO> list = estatusGeneral.getSucEstTabByCeco(op, negocio, ceco);
        return list;
    }
}

class CecosJson {

    private String msj;
    private List<String> cecoLogs;
    private List<String> geoLogs;

    public String getMsj() {
        return msj;
    }

    public void setMsj(String msj) {
        this.msj = msj;
    }

    public List<String> getCecoLogs() {
        return cecoLogs;
    }

    public void setCecoLogs(List<String> cecoLogs) {
        this.cecoLogs = cecoLogs;
    }

    public List<String> getGeoLogs() {
        return geoLogs;
    }

    public void setGeoLogs(List<String> geoLogs) {
        this.geoLogs = geoLogs;
    }
}
