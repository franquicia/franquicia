package com.gruposalinas.franquicia.business;

import java.io.BufferedReader;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SesionFirmaBI {

    private Logger logger = LogManager.getLogger(SesionFirmaBI.class);

    public static boolean wsTokenUsuario(String urlStr, String cadena) throws IOException {
        UtilServicios utilServ = new UtilServicios();
        boolean valido = false;
        BufferedReader read = null;
        String line = "";
        read = utilServ.consumeWebService(urlStr, cadena);

        try {
            while ((line = read.readLine()) != null) {
                if (line.contains("<GetDataResult>0</GetDataResult>")) {
                    valido = true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            read.close();
        }
        return valido;
    }

}
