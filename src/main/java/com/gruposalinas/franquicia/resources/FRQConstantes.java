package com.gruposalinas.franquicia.resources;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FRQConstantes {

    private static Logger logger = LogManager.getLogger(FRQConstantes.class);
    /*
	 * 0.- Produccion = (true y false) ó (true true)
	 * 1.- Desarrollo = (false y true)
	 * 2.- Local = (false y false)
     */
    public static final boolean PRODUCCION = true;
    public static final boolean DESARROLLO = false;

    //public static final String URL_SERVIDOR_DESARROLLO 		= "http://10.50.109.39:8080"; //SOCIOSdesarrollobienestar.socio.gs
    //public static final String URL_SERVIDOR_DESARROLLO 			= "http://desarrollobienestar.socio.gs:8080";
    public static final String URL_SERVIDOR_DESARROLLO = "http://10.50.109.47:8080"; //Aquí va la ip de ambiente de desarrollo
    public static final String URL_SERVIDOR_LOCAL = "http://" + FRQConstantes.getIPLocal() + ":8080"; //Aquí va la ip del desarrollador
    //public static final String URL_SERVIDOR_PRODUCCION 			= "http://10.53.29.67:9991";
    public static final String URL_SERVIDOR_PRODUCCION = "http://10.53.33.83:80";

    //public static final String URL_SERVIDOR_A_DESARROLLO 		= "http://10.50.109.39";
    public static final String URL_SERVIDOR_A_DESARROLLO = "http://10.50.109.47"; //Aquí va la ip de ambiente de desarrollo
    public static final String URL_SERVIDOR_A_LOCAL = "http://" + FRQConstantes.getIPLocal(); //Aquí va la ip del desarrollador
    //public static final String URL_SERVIDOR_A_PRODUCCION 		= "http://10.53.29.67";
    public static final String URL_SERVIDOR_A_PRODUCCION = "http://10.53.33.83";

    //public static final String IP_ORIGEN_DESARROLLO 			= "10.50.109.39";
    public static final String IP_ORIGEN_DESARROLLO = "10.50.109.47";
    public static final String IP_ORIGEN_LOCAL = FRQConstantes.getIPLocal();
    //public static final String IP_ORIGEN_PRODUCCION 			= "10.53.29.67";
    public static final String IP_ORIGEN_PRODUCCION = "10.53.33.83";

    //public static final String IP_HOST_DESARROLLO 				= "10.50.109.47"; //Ip para pruebas en desarrollo
    public static final String IP_HOST_DESARROLLO = "192.168.2.1"; //Ip para pruebas en equipo local
    //public static final String IP_HOST_PRODUCCION 				= "10.53.29.153";//Esta es la ip del servidor donde sera ejecutado el Scheduler
    public static final String IP_HOST_PRODUCCION = "10.53.33.76";//Esta es la ip del servidor donde sera ejecutado el Scheduler

    //public static final String LLAVE_ENCRIPCION_DESARROLLO 		= "fce099383332d9ffa34c4f8a50dda9a0="; //desarrollo original se debe comentar para activar simulacion de producción
    public static final String LLAVE_ENCRIPCION_DESARROLLO = "Ux+Z68QHllMW6jyUfPaO2f5h7Oost8U="; //desarrollo nueva se debe comentar para activar simulacion de producción
    //public static final String LLAVE_ENCRIPCION_PRODUCCION 		= "fce099383332d9ffa34c4f8a50dda9a0=";
    public static final String LLAVE_ENCRIPCION_PRODUCCION = "Ux+Z68QHllMW6jyUfPaO2f5h7Oost8U=";//llave nueva de produccion
    public static final String LLAVE_ENCRIPCION_LOCAL = "fce099383332d9ffa34c4f8a50dda9a0=";//id=666
    public static final String LLAVE_ENCRIPCION_LOCAL_IOS = "fce099383332d9ff";//id=7

    //public static final String URL_WSTOKEN_DESARROLLO			= "http://desarrollo.socio.gs/Login5/token_esp/token_esp.asmx?WSDL"; //unreacheable
    //public static final String URL_WSTOKEN_PRODUCCION			= "http://portal.socio.gs/Login5/token_esp/token_esp.asmx?WSDL"; //unreacheable
    public static final String URL_WSTOKEN_DESARROLLO = "https://10.50.158.63/Login5/token_esp/token_esp.asmx?WSDL"; //desarrollo original se debe comentar para activar simulacion de producción
    public static final String URL_WSTOKEN_PRODUCCION = "https://authapps.socio.gs/Login5/token_esp/token_esp.asmx?wsdl";

    //public static final String URL_LOGIN_LLAVE_DESARROLLO		= "http://desarrollo.socio.gs/Login5/login/default.aspx?token="; //unreacheable
    //public static final String URL_LOGIN_LLAVE_PRODUCCION		= "http://portal.socio.gs/Login5/login/default.aspx?token="; //unreacheable
    public static final String URL_LOGIN_LLAVE_DESARROLLO = "https://10.50.158.63/Login5/login/default.aspx?token=";  //desarrollo original se debe comentar para activar simulacion de producción
    public static final String URL_LOGIN_LLAVE_PRODUCCION = "https://authapps.socio.gs/Login5/login/default.aspx?token=";

    public static final String ID_APP_PORTALES_DESARROLLO = "2"; //desarrollo original se debe comentar para activar simulacion de producción
    //public static final String ID_APP_PORTALES_DESARROLLO 		= "59";
    public static final String ID_APP_PORTALES_PRODUCCION = "59";
    public static final String ID_APP_PORTALES_ID_LLAVE = "666";
    public static final String ID_APP_PORTALES_ID_LLAVE_IOS = "7";

    public static final String RUTA_IMAGEN_DESARROLLO = "http://10.50.109.39/Sociounico"; //Aquí va la url de desarrollo
    //public static final String RUTA_IMAGEN_PRODUCCION 			= "http://imgsbienestar.socio.gs/sociounico";
    public static final String RUTA_IMAGEN_PRODUCCION = "http://10.53.33.82";

    public static final String RUTA_IMAGEN_DESARROLLO_MOVIL = "http://10.50.109.39/Sociounico"; //Aquí va la url de desarrollo
    //public static final String RUTA_IMAGEN_PRODUCCION_MOVIL		= "https://200.38.122.186:8443/sociounico";
    public static final String RUTA_IMAGEN_PRODUCCION_MOVIL = "http://10.53.33.82";

    public final static String URL_FIRMA_DESARROLLO = "https://10.51.146.186:8443/YakanaAuthServer/webresources/";
    public final static String URL_FIRMA_PRODUCCION = "https://10.63.11.173:8443/YakanaAuthServer/webresources/";//Servicio yakana

    //constantes de firma azteca
    public final static String XML_WS_FIRMA_AZTECA = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soap:Header/><soap:Body><tem:GetData><tem:no_empleado>@idEmpleado</tem:no_empleado><tem:token>@token</tem:token><tem:entidad>1</tem:entidad></tem:GetData></soap:Body></soap:Envelope>";
    public final static String URL_WS_FIRMA_AZTECA = "http://10.50.169.43/wsByPass/WsByPass.asmx?wsdl";

    /*Constantes para el envío de correo*/
 /*De momento solo como prueba*/
    public static final String CTE_HOST_CORREO = "10.63.200.79";
    public static final String CTE_REMITENTE_SG = "sistemas-franquicia@bancoazteca.com.mx";

    public final static String MASTEREY_ENCRYPT = "4WMJz+AdWabMjaiV";
    public final static String MASTERKEY_URL = "http://10.50.169.89/wsAppLoginParam/Service.asmx?WSDL";

    public final static String URL_GESTION_DESARROLLO = "http://10.51.218.144:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=";
    public final static String URL_GESTION_PRODUCCION = "http://10.53.33.83:80/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=";

    public final static String URL_FRANQUICIA_DESARROLLO = "http://10.51.218.144:8080/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=";
    public final static String URL_FRANQUICIA_PRODUCCION = "http://10.53.33.83:80/checklist/ejecutaServiciosEncriptados/ejecutaServicio.json?service=";

    public final static String URL_WS_GENERA_TOKEN_CECO_DESARROLLO = "http://10.50.109.69:8080/cecows/rest/token";
    public final static String URL_WS_GENERA_TOKEN_CECO_PRODUCCION = "http://10.50.109.68:8080/cecows/rest/token";

    public final static String URL_WS_CONSULTA_CECOS_DESARROLLO = "http://10.50.109.69:8080/cecows/rest/ekt/unificada";
    public final static String URL_WS_CONSULTA_CECOS_PRODUCCION = "http://10.50.109.68:8080/cecows/rest/ekt/unificada";

    public final static String usuarioWSCecoTokenDes = "USRSISF";
    public final static String passwordWSCecoTokenDes = "UsrFrQc71";

    public final static String usuarioWSCecoTokenProd = "USRSISF";
    public final static String passwordWSCecoTokenPro = "UsrFrQc71";

    public final static String depuraTablaDesarrollo = "TAPASO_CECO";
    public final static String depuraTablaProduccion = "TAPASO_CECO";

    public final static String URL_WS_VALIDAUSUARIO_DESARROLLO = "http://10.63.14.80/wslinuxvalidausuario.asmx";
    public final static String URL_WS_VALIDAUSUARIO_PRODUCCION = "http://10.63.14.80/wslinuxvalidausuario.asmx";

    public static String getURLGestion() {
        String server = URL_GESTION_DESARROLLO;
        if (DESARROLLO) {
            server = URL_GESTION_DESARROLLO;
        }
        if (PRODUCCION) {
            server = URL_GESTION_PRODUCCION;
        }
        return server;
    }

    public static String getURLFranquicia() {
        String server = URL_FRANQUICIA_DESARROLLO;
        if (DESARROLLO) {
            server = URL_FRANQUICIA_DESARROLLO;
        }
        if (PRODUCCION) {
            server = URL_FRANQUICIA_PRODUCCION;
        }
        return server;
    }

    public static String getURLServer() {
        String server = URL_SERVIDOR_LOCAL;
        if (DESARROLLO) {
            server = URL_SERVIDOR_DESARROLLO;
        }
        if (PRODUCCION) {
            server = URL_SERVIDOR_PRODUCCION;
        }
        return server;
    }

    public static String getURLApacheServer() {
        String server = URL_SERVIDOR_A_LOCAL;
        if (DESARROLLO) {
            server = URL_SERVIDOR_A_DESARROLLO;
        }
        if (PRODUCCION) {
            server = URL_SERVIDOR_A_PRODUCCION;
        }
        return server;
    }

    public static String getIpOrigen() {
        String ip = IP_ORIGEN_LOCAL;
        if (DESARROLLO) {
            ip = IP_ORIGEN_DESARROLLO;
        }
        if (PRODUCCION) {
            ip = IP_ORIGEN_PRODUCCION;
        }
        return ip;
    }

    public static String getUrlWsToken() {
        String url = URL_WSTOKEN_DESARROLLO;
        if (PRODUCCION) {
            url = URL_WSTOKEN_PRODUCCION;
        }
        return url;
    }

    public static String getUrlLoginLlave() {
        String url = URL_LOGIN_LLAVE_DESARROLLO;
        if (PRODUCCION) {
            url = URL_LOGIN_LLAVE_PRODUCCION;
        }
        return url;
    }

    public static String getIdApp() {
        String url = ID_APP_PORTALES_DESARROLLO;
        if (PRODUCCION) {
            url = ID_APP_PORTALES_PRODUCCION;
        }
        return url;
    }

    public static String getLLave() {
        String llave = LLAVE_ENCRIPCION_DESARROLLO;
        if (PRODUCCION) {
            llave = LLAVE_ENCRIPCION_PRODUCCION;
        }
        return llave;
    }

    public static String getLlaveEncripcionLocal() {
        return LLAVE_ENCRIPCION_LOCAL;
    }

    public static String getLlaveEncripcionLocalIOS() {
        return LLAVE_ENCRIPCION_LOCAL_IOS;
    }

    public static String getIpHost() {
        String ip = IP_HOST_DESARROLLO;
        if (PRODUCCION) {
            ip = IP_HOST_PRODUCCION;
        }
        return ip;
    }

    public static String getRutaImagen() {
        String server = RUTA_IMAGEN_DESARROLLO;
        if (PRODUCCION) {
            server = RUTA_IMAGEN_PRODUCCION;
        }
        return server;
    }

    public static String getRutaImagenMovil() {
        String server = RUTA_IMAGEN_DESARROLLO_MOVIL;
        if (PRODUCCION) {
            server = RUTA_IMAGEN_PRODUCCION_MOVIL;
        }
        return server;
    }

    public static String getURLFirma() {
        String server = URL_FIRMA_DESARROLLO;
        if (PRODUCCION) {
            server = URL_FIRMA_PRODUCCION;
        }
        return server;
    }
    //GENERA TOKEN CECOS

    public static String getURLTokenCecos() {
        String server = URL_WS_GENERA_TOKEN_CECO_DESARROLLO;
        if (PRODUCCION) {
            server = URL_WS_GENERA_TOKEN_CECO_PRODUCCION;
        }
        return server;
    }

    //CONSULTA CECOS
    public static String getURLConsultaCecos() {
        String server = URL_WS_CONSULTA_CECOS_DESARROLLO;
        if (PRODUCCION) {
            server = URL_WS_CONSULTA_CECOS_PRODUCCION;
        }
        return server;
    }

    //Usuario ws ceco
    public static String getUsuarioWSCeco() {
        String usuario = usuarioWSCecoTokenDes;

        if (PRODUCCION) {
            usuario = usuarioWSCecoTokenProd;
        }
        return usuario;
    }

    //Password ws ceco
    public static String getPasswordWSCeco() {
        String password = passwordWSCecoTokenDes;

        if (PRODUCCION) {
            password = passwordWSCecoTokenPro;
        }
        return password;
    }

    //Depurar Tabla de Paso
    public static String getDepuraTabla() {
        String password = depuraTablaDesarrollo;

        if (PRODUCCION) {
            password = depuraTablaProduccion;
        }
        return password;
    }

    public static String getIPLocal() {
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            return inetAddress.getHostAddress();
        } catch (UnknownHostException ex) {
            logger.error("Error", ex);
        }
        return "127.0.0.1";
    }

    public static String getURLWSUnixValidaUsuario() {

        String server = URL_WS_VALIDAUSUARIO_DESARROLLO;

        if (PRODUCCION) {
            server = URL_WS_VALIDAUSUARIO_PRODUCCION;
        }
        return server;
    }

}
