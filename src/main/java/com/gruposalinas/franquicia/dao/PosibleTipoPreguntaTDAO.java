package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PosiblesTipoPreguntaDTO;
import java.util.List;

public interface PosibleTipoPreguntaTDAO {

    public List<PosiblesTipoPreguntaDTO> obtienePosiblesRespuestasTemp(String idTipoPregunta) throws Exception;

    public boolean insertaPosibleTipoPregunta(PosiblesTipoPreguntaDTO bean) throws Exception;

    public boolean actualizaPosibleTipoPregunta(PosiblesTipoPreguntaDTO bean) throws Exception;

    public boolean eliminaPosibleTipoPregunta(int idPosibleTipoPregunta) throws Exception;

}
