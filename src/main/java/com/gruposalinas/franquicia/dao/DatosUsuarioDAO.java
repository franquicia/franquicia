package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.DatosUsuarioDTO;
import java.util.List;

public interface DatosUsuarioDAO {

    public List<DatosUsuarioDTO> obtieneDatos(String usuario) throws Exception;

}
