package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PerfilUsuarioDTO;
import java.util.List;

public interface PerfilUsuarioDAO {

    public List<PerfilUsuarioDTO> obtienePerfiles(String idUsuario, String idPerfil) throws Exception;

    public boolean insertaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception;

    public boolean actualizaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception;

    public boolean eliminaPerfilUsaurio(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception;

}
