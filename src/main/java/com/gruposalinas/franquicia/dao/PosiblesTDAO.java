package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PosiblesDTO;
import java.util.List;

public interface PosiblesTDAO {

    public List<PosiblesDTO> buscaPosibleTemp() throws Exception;

    public boolean insertaPosibleTemp(PosiblesDTO bean) throws Exception;

    public boolean actualizaPosibleTemp(PosiblesDTO bean) throws Exception;

    public boolean eliminaPosibleTemp(int idPosible) throws Exception;
}
