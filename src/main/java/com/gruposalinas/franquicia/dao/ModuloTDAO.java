package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ModuloDTO;
import java.util.List;

public interface ModuloTDAO {

    public List<ModuloDTO> obtieneModuloTemp(String idModulo) throws Exception;

    public int insertaModuloTemp(ModuloDTO bean) throws Exception;

    public boolean actualizaModuloTemp(ModuloDTO bean) throws Exception;

    public boolean eliminaModuloTemp(int idModulo) throws Exception;
}
