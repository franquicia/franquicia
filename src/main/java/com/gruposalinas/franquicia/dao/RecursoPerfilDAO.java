package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.RecursoPerfilDTO;
import java.util.List;

public interface RecursoPerfilDAO {

    public List<RecursoPerfilDTO> buscaRecursoP(String idRecurso, String idPerfil) throws Exception;

    public boolean insertaRecursoP(RecursoPerfilDTO bean) throws Exception;

    public boolean actualizaRecursoP(RecursoPerfilDTO bean) throws Exception;

    public boolean eliminaRecursoP(int idRecursoP, int idPerfil) throws Exception;

    public List<RecursoPerfilDTO> obtienePermisos(int idRecurso, int idUsuario) throws Exception;

}
