package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ChecklistPreguntaDTO;
import java.util.List;

public interface ChecklistPreguntaDAO {

    public boolean insertaChecklistPregunta(ChecklistPreguntaDTO bean) throws Exception;

    public boolean actualizaChecklistPregunta(ChecklistPreguntaDTO bean) throws Exception;

    public boolean eliminaChecklistPregunta(int idChecklist, int idPregunta) throws Exception;

    public List<ChecklistPreguntaDTO> buscaPreguntas(int idChecklist) throws Exception;

    public List<ChecklistPreguntaDTO> obtienePregXcheck(int idChecklist) throws Exception;

    public boolean cambiaEstatus() throws Exception;
}
