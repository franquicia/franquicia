package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.AdminCanalPDDTO;
import com.gruposalinas.franquicia.domain.AdminMenuPDDTO;
import com.gruposalinas.franquicia.domain.AdminNegocioPDDTO;
import com.gruposalinas.franquicia.domain.AdminParametroPDDTO;
import com.gruposalinas.franquicia.domain.AdminTipoDocPDDTO;
import com.gruposalinas.franquicia.domain.AdmonEstatusTabletaPDDTO;
import com.gruposalinas.franquicia.domain.AdmonTabletPDDTO;
import com.gruposalinas.franquicia.domain.BachDescargasPDDTO;
import com.gruposalinas.franquicia.domain.CanalDTO;
import com.gruposalinas.franquicia.domain.CargaArchivoDTO;
import com.gruposalinas.franquicia.domain.ErrorPDDTO;
import com.gruposalinas.franquicia.domain.LoginPDDTO;
import com.gruposalinas.franquicia.domain.MenuPedestalDigital;
import com.gruposalinas.franquicia.domain.NegocioDTO;
import com.gruposalinas.franquicia.mappers.AdmonCanalRowMapperPD;
import com.gruposalinas.franquicia.mappers.AdmonDescargaRowMapperPD;
import com.gruposalinas.franquicia.mappers.AdmonEstatusTabletaRowMapper;
import com.gruposalinas.franquicia.mappers.AdmonMenuPerfilRowMapperPD;
import com.gruposalinas.franquicia.mappers.AdmonMenuRowMapperPD;
import com.gruposalinas.franquicia.mappers.AdmonNegocioRowMapperPD;
import com.gruposalinas.franquicia.mappers.AdmonParametrosRowMapperPD;
import com.gruposalinas.franquicia.mappers.AdmonTabletRowMapperPD;
import com.gruposalinas.franquicia.mappers.AdmonTipoDocRowMapperPD;
import com.gruposalinas.franquicia.mappers.ArchivosRowMapperPD;
import com.gruposalinas.franquicia.mappers.CursorRowMapperPD;
import com.gruposalinas.franquicia.mappers.ErrorRowMapperPD;
import com.gruposalinas.franquicia.mappers.LoginRowMapperPD;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PedestalDigitalDAOImpl extends DefaultDAO implements PedestalDigitalDAO {

    private static Logger logger = LogManager.getLogger(PedestalDigitalDAOImpl.class);

    private DefaultJdbcCall jdbcLoginUsuario;
    private DefaultJdbcCall jdbcLoginUsuarioPerfil;
    // ------- carga archivos ---------
    private DefaultJdbcCall jdbcAgregaDatosArchivoGeneral;
    private DefaultJdbcCall jdbcActualizaDatosArchivoGeneral;
    private DefaultJdbcCall jdbcConsultaDatosArchivoGeneral;
    // -------- admon tablet -----------
    private DefaultJdbcCall jdbcAgregaAdmonTablet;
    private DefaultJdbcCall jdbcActualizarAdmonTablet;
    private DefaultJdbcCall jdbcConsultarAdmonTablet;

//	-----------admin negocio---------------
    private DefaultJdbcCall jdbcInsertaNegocio;
    private DefaultJdbcCall jdbcConsultaNegocio;

//	-----------admin canal---------------
    private DefaultJdbcCall jdbcInsertaCanal;
    private DefaultJdbcCall jdbcActualizaCanal;
    private DefaultJdbcCall jdbcConsultaCanal;

//	-----------admin parametros---------------
    private DefaultJdbcCall jdbcInsertaParametros;
    private DefaultJdbcCall jdbcActualizaParametros;
    private DefaultJdbcCall jdbcConsultaParametros;

//	--------------admin menu-----------------
    private DefaultJdbcCall jdbcInsertaMenu;
    private DefaultJdbcCall jdbcActualizaMenu;
    private DefaultJdbcCall jdbcObtieneMenu;
    private DefaultJdbcCall jdbcObtieneMenuPerfil;

//	---------------administracion de tipo de documento---------
    private DefaultJdbcCall jdbcInsertaActualizaTipoDoc;
    private DefaultJdbcCall jdbcConsultaTipoDoc;

//	---------------administracion del estatus de la tablet---------
    private DefaultJdbcCall jdbcInsertaActualizaEstatusTablet;
    private DefaultJdbcCall jdbcConsultaEstatusTablet;

//	---------------bach de desgarga en APK y documentos
    private DefaultJdbcCall jdbcdescargaApkDoc;
    private DefaultJdbcCall jdbcInsertaActualizaApKDoc;
    private DefaultJdbcCall jdbcInsertActualizaDescarga;
    private DefaultJdbcCall jdbcConsultaDescarga;

    public void init() {
        jdbcLoginUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_USR_PERFIL")
                .withProcedureName("SP_USR_PERFIL")
                .returningResultSet("PA_CURSORCON", new LoginRowMapperPD());

        jdbcLoginUsuarioPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_USR_PERFIL")
                .withProcedureName("SP_USR_PERFIL")
                .returningResultSet("PA_CURSORCON", new CursorRowMapperPD());

        // ------- carga archivos ---------
        jdbcAgregaDatosArchivoGeneral = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ARCHIVO_CARGA")
                .withProcedureName("SP_CARGA_ARCHIVO");

        jdbcActualizaDatosArchivoGeneral = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ARCHIVO_CARGA")
                .withProcedureName("SP_CARGA_ARCHIVO");

        jdbcConsultaDatosArchivoGeneral = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ARCHIVO_CARGA")
                .withProcedureName("SP_CARGA_ARCHIVO")
                .returningResultSet("PA_CURSORCON", new ArchivosRowMapperPD());

        // ------- carga archivos ---------
        jdbcAgregaAdmonTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ARCHIVO_TABLET")
                .withProcedureName("SP_ARC_CARGA_TABLET");

        jdbcActualizarAdmonTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ARCHIVO_TABLET")
                .withProcedureName("SP_ARC_CARGA_TABLET");

        jdbcConsultarAdmonTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ARCHIVO_TABLET")
                .withProcedureName("SP_ARC_CARGA_TABLET")
                .returningResultSet("PA_CURSORCON", new AdmonTabletRowMapperPD());

//		-----------admin negocio---------------
        jdbcInsertaNegocio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_NEGOC")
                .withProcedureName("SP_ADMON_NEGOC")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcConsultaNegocio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_NEGOC")
                .withProcedureName("SP_ADMON_NEGOC")
                .returningResultSet("PA_CURSORCON", new AdmonNegocioRowMapperPD());

//		--------------- admin canal -------------
        jdbcInsertaCanal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_CANAL")
                .withProcedureName("SP_ADMON_CANAL")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcActualizaCanal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_CANAL")
                .withProcedureName("SP_ADMON_CANAL")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcConsultaCanal = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_CANAL")
                .withProcedureName("SP_ADMON_CANAL")
                .returningResultSet("PA_CURSORCON", new AdmonCanalRowMapperPD());

//		--------------- admin parametros -------------
        jdbcInsertaParametros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_PARAM")
                .withProcedureName("SP_ADMON_PARAMETRO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcActualizaParametros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_PARAM")
                .withProcedureName("SP_ADMON_PARAMETRO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcConsultaParametros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_PARAM")
                .withProcedureName("SP_ADMON_PARAMETRO")
                .returningResultSet("PA_CURSORCON", new AdmonParametrosRowMapperPD());

//		----------------admin menu--------------------
        jdbcInsertaMenu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_MENUPER")
                .withProcedureName("SP_ADMON_MENUPER")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcActualizaMenu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_MENUPER")
                .withProcedureName("SP_ADMON_MENUPER")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcObtieneMenu = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_MENUPER")
                .withProcedureName("SP_ADMON_MENUPER")
                .returningResultSet("PA_CURSORCON", new AdmonMenuRowMapperPD());

        jdbcObtieneMenuPerfil = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_MENUPER")
                .withProcedureName("SP_ADMON_MENUPER")
                .returningResultSet("PA_CURSORCON", new AdmonMenuPerfilRowMapperPD());

//		--------------- admin Administracion de tipo de documento -------------
        jdbcInsertaActualizaTipoDoc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMONTIPODOC")
                .withProcedureName("SP_ADMON_TIPODOC")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcConsultaTipoDoc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMONTIPODOC")
                .withProcedureName("SP_ADMON_TIPODOC")
                .returningResultSet("PA_CURSORCON", new AdmonTipoDocRowMapperPD());

//		--------------- admin Administracion del estatus de la tableta -------------
        jdbcInsertaActualizaEstatusTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CAT_EDO_TAB")
                .withProcedureName("SP_CAT_EDO_TAB")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcConsultaEstatusTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CAT_EDO_TAB")
                .withProcedureName("SP_CAT_EDO_TAB")
                .returningResultSet("PA_CURSORCON", new AdmonEstatusTabletaRowMapper());

        // bach de descarga de APK y documentos
        jdbcdescargaApkDoc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_BATCH_DESCAR")
                .withProcedureName("SP_BATCH_DESCAR")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD()); // new ExistRowMapperPD());

        jdbcInsertaActualizaApKDoc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_BATCH_DESCAR")
                .withProcedureName("SP_BATCH_DESCAR")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcInsertActualizaDescarga = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_DESCAR")
                .withProcedureName("SP_ADMON_DESCAR")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcConsultaDescarga = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_DESCAR")
                .withProcedureName("SP_ADMON_DESCAR")
                .returningResultSet("PA_CURSORCON", new AdmonDescargaRowMapperPD());

    }

    @Override
    public LoginPDDTO login1(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        LoginPDDTO login = null;
        List<LoginPDDTO> aux = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 1)
                    .addValue("PA_USUARIO", idUsuario)
                    .addValue("PA_ID_MENU", null)
                    .addValue("PA_PERFIL", null);

            out = jdbcLoginUsuario.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_USR_PERFIL.SP_USR_PERFIL}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            aux = (List<LoginPDDTO>) out.get("PA_CURSORCON");
            login = aux.get(0);

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion del usuario opcion1 " + idUsuario);
            } else {
                return login;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al ingresar el login2. " + e.getMessage());
        }

        return null;
    }

    @Override
    public List<MenuPedestalDigital> login2(int id_usuario, int id_perfil) throws Exception {
        Map<String, Object> out = null;
        List<MenuPedestalDigital> menu = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 2)
                    .addValue("PA_USUARIO", id_usuario)
                    .addValue("PA_ID_MENU", null)
                    .addValue("PA_PERFIL", id_perfil);

            out = jdbcLoginUsuarioPerfil.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_USR_PERFIL.SP_USR_PERFIL2}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            menu = (List<MenuPedestalDigital>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion del usuario opcion2 " + id_usuario);
            } else {
                return menu;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al ingresar el login2. " + e.getMessage());
        }
        return null;
    }

    @Override
    public int cargaArchivosGeneral(CargaArchivoDTO cargaArchivo) throws Exception {
        Map<String, Object> out = null;
        int error = 0, id_Archivo = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 0)
                    .addValue("PA_ID_ARCHIVO", 0)
                    .addValue("PA_USUARIO", cargaArchivo.getId_usuario())
                    .addValue("PA_NOMBRE", cargaArchivo.getNombre())
                    .addValue("PA_VISIBLE", cargaArchivo.getVisibleSucursal())
                    .addValue("PA_TIPO", cargaArchivo.getTipoArchivo())
                    .addValue("PA_DESCRIBE", cargaArchivo.getDescribe())
                    .addValue("PA_ACTIVO", 1);

            out = jdbcAgregaDatosArchivoGeneral.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_ARCHIVO_CARGA.SP_CARGA_ARCHIVO.OP0}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            String m = out.get("PA_CURSORCON").toString();
            m = m.replace("[{ID_ARCHIVO=", "");
            m = m.replace("}]", "");
            id_Archivo = Integer.parseInt(m);

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion del usuario opcion0 ");
            } else {
                return id_Archivo;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al agregar informacion del archivo " + e.getMessage());
        }

        return 0;
    }

    @Override
    public boolean actualizaArchivo(CargaArchivoDTO cargaArchivo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 1)
                    .addValue("PA_ID_ARCHIVO", cargaArchivo.getId_cargaArchivo())
                    .addValue("PA_USUARIO", cargaArchivo.getId_usuario())
                    .addValue("PA_NOMBRE", cargaArchivo.getNombre())
                    .addValue("PA_VISIBLE", cargaArchivo.getVisibleSucursal())
                    .addValue("PA_TIPO", cargaArchivo.getTipoArchivo())
                    .addValue("PA_DESCRIBE", cargaArchivo.getDescribe())
                    .addValue("PA_ACTIVO", 1);

            out = jdbcActualizaDatosArchivoGeneral.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_ARCHIVO_CARGA.SP_CARGA_ARCHIVO.OP1}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al actualizar la informacion del archivo opcion1 " + cargaArchivo.getId_cargaArchivo());
            } else {
                return true;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al agregar informacion del archivo " + e.getMessage());
        }

        return false;
    }

    @Override
    public List<CargaArchivoDTO> consulta(int idArchivo) throws Exception {
        Map<String, Object> out = null;
        List<CargaArchivoDTO> archivo = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 2)
                    .addValue("PA_ID_ARCHIVO", idArchivo)
                    .addValue("PA_USUARIO", 0)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_DESCRIBE", null)
                    .addValue("PA_VISIBLE", 0)
                    .addValue("PA_TIPO", null)
                    .addValue("PA_ACTIVO", 0);

            out = jdbcConsultaDatosArchivoGeneral.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_ARCHIVO_CARGA.SP_CARGA_ARCHIVO.OP2}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            archivo = (List<CargaArchivoDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("Algo ocurrió al consultar los datos del archivo opcion2 " + idArchivo);
            } else {
                return archivo;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al agregar informacion del archivo " + e.getMessage());
        }

        return null;
    }

    @Override
    public int cargaDatosAdmonTablet(AdmonTabletPDDTO listAdmonTablet) throws Exception {
        Map<String, Object> out = null;
        int error = 0, id_Archivo = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 0)
                    .addValue("PA_ID_ADMON_TAB", listAdmonTablet.getIdAdmonTable())
                    .addValue("PA_ID_ARCHIVO", listAdmonTablet.getIdArchivo())
                    .addValue("PA_ETAPA", listAdmonTablet.getEtapa())
                    .addValue("PA_NUM_ECO", listAdmonTablet.getNumEconomico())
                    .addValue("PA_DESCRIBE_S", listAdmonTablet.getDescribeSucursal())
                    .addValue("PA_NUM_SERIE", listAdmonTablet.getNumSerieTablet())
                    .addValue("PA_FECHA_INSTALA", listAdmonTablet.getFechaInstalacion())
                    .addValue("PA_DETECTA_F", listAdmonTablet.getDetectaFalla())
                    .addValue("PA_ACTIVO", listAdmonTablet.getActivo())
                    .addValue("PA_FECHA_FA", listAdmonTablet.getFechaFalla())
                    .addValue("PA_FOLIO_FALLA", listAdmonTablet.getFolioFalla())
                    .addValue("PA_FECHA_CORRIGE", listAdmonTablet.getFechaCorrige())
                    .addValue("PA_RESPONSABLE", listAdmonTablet.getResponsable());

            out = jdbcAgregaAdmonTablet.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_ARCHIVO_TABLET.SP_ARC_CARGA_TABLET.OP0}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion del usuario opcion0 ");
            } else {
                return id_Archivo;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al agregar informacion del administrador de tablet " + e.getMessage());
        }

        return 0;
    }

    @Override
    public boolean actualizaDatosAdmonTablet(AdmonTabletPDDTO listAdmonTablet) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 0)
                    .addValue("PA_ID_ADMON_TAB", listAdmonTablet.getIdAdmonTable())
                    .addValue("PA_ID_ARCHIVO", listAdmonTablet.getIdArchivo())
                    .addValue("PA_ETAPA", listAdmonTablet.getEtapa())
                    .addValue("PA_NUM_ECO", listAdmonTablet.getNumEconomico())
                    .addValue("PA_DESCRIBE_S", listAdmonTablet.getDescribeSucursal())
                    .addValue("PA_NUM_SERIE", listAdmonTablet.getNumSerieTablet())
                    .addValue("PA_FECHA_INSTALA", listAdmonTablet.getFechaInstalacion())
                    .addValue("PA_DETECTA_F", listAdmonTablet.getDetectaFalla())
                    .addValue("PA_ACTIVO", listAdmonTablet.getActivo())
                    .addValue("PA_FECHA_FA", listAdmonTablet.getFechaFalla())
                    .addValue("PA_FOLIO_FALLA", listAdmonTablet.getFolioFalla())
                    .addValue("PA_FECHA_CORRIGE", listAdmonTablet.getFechaCorrige())
                    .addValue("PA_RESPONSABLE", listAdmonTablet.getResponsable());

            out = jdbcActualizarAdmonTablet.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_ARCHIVO_TABLET.SP_ARC_CARGA_TABLET.OP1}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion del usuario opcion0 ");
            } else {
                return true;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al actualizar informacion del administrador de tablet " + e.getMessage());
        }

        return false;
    }

    @Override
    public List<AdmonTabletPDDTO> consultaDatosAdmonTablet(int idAdmonTablet) throws Exception {
        Map<String, Object> out = null;
        List<AdmonTabletPDDTO> admonTablet = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 2)
                    .addValue("PA_ID_ADMON_TAB", idAdmonTablet)
                    .addValue("PA_ID_ARCHIVO", 0)
                    .addValue("PA_ETAPA", 0)
                    .addValue("PA_NUM_ECO", 0)
                    .addValue("PA_DESCRIBE_S", null)
                    .addValue("PA_NUM_SERIE", null)
                    .addValue("PA_FECHA_INSTALA", null)
                    .addValue("PA_DETECTA_F", null)
                    .addValue("PA_ACTIVO", null)
                    .addValue("PA_FECHA_FA", null)
                    .addValue("PA_FOLIO_FALLA", 0)
                    .addValue("PA_FECHA_CORRIGE", null)
                    .addValue("PA_RESPONSABLE", null);

            out = jdbcConsultarAdmonTablet.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_ARCHIVO_TABLET.SP_ARC_CARGA_TABLET.OP2}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion del usuario opcion0 ");
            } else {
                admonTablet = (List<AdmonTabletPDDTO>) out.get("PA_CURSORCON");
                return admonTablet;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al consulta informacion del administrador de tablet " + e.getMessage());
        }

        return null;
    }

    public List<AdminNegocioPDDTO> admonNegocio(NegocioDTO negocio, int opcion) throws Exception {
        Map<String, Object> out = null;
        List<AdminNegocioPDDTO> negocioDTO = null;
        int error = 0;
        List<ErrorPDDTO> mensaje = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", opcion)
                    .addValue("PA_ID_NEGOCIO", negocio.getIdNegocio())
                    .addValue("PA_DESC_NEGOCIO", negocio.getDescripcion())
                    .addValue("PA_ACTIVO", negocio.getActivo());

            switch (opcion) {
                case 0:
                    out = jdbcInsertaNegocio.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_NEGOC.SP_ADMON_NEGOC.Inserta}");

                    break;
                case 1:
                    out = jdbcInsertaNegocio.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_NEGOC.SP_ADMON_NEGOC.Actualiza}");

                    break;
                case 2:
                    out = jdbcConsultaNegocio.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_NEGOC.SP_ADMON_NEGOC.Obtiene}");
                    break;
            }

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al realizar el escript con la opcion " + opcion);
            } else {

                switch (opcion) {
                    case 0:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminNegocioPDDTO insert = new AdminNegocioPDDTO();
                        negocioDTO = new ArrayList<AdminNegocioPDDTO>();

                        insert.setIdNegocio(Integer.parseInt(mensaje.get(0).getMsj()));
                        insert.setNegocio("INSERT");
                        negocioDTO.add(insert);
                        break;

                    case 1:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminNegocioPDDTO actualiza = new AdminNegocioPDDTO();
                        negocioDTO = new ArrayList<AdminNegocioPDDTO>();

                        actualiza.setIdNegocio(Integer.parseInt(mensaje.get(0).getMsj()));
                        actualiza.setNegocio("UPDATE");
                        negocioDTO.add(actualiza);
                        break;

                    case 2:
                        negocioDTO = (List<AdminNegocioPDDTO>) out.get("PA_CURSORCON");
                        break;
                }
                return negocioDTO;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al consulta informacion del administrador de negocio " + e.getMessage());
        }
        return null;
    }

    public List<AdminCanalPDDTO> admonCanal(CanalDTO canal, int opcion) throws Exception {
        Map<String, Object> out = null;
        List<AdminCanalPDDTO> canalDTO = null;
        int error = 0;
        List<ErrorPDDTO> mensaje = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", opcion)
                    .addValue("PA_ID_CANAL", canal.getIdCanal())
                    .addValue("PA_CANAL", canal.getDescrpicion())
                    .addValue("PA_ACTIVO", canal.getActivo());

            switch (opcion) {
                case 0:
                    out = jdbcInsertaCanal.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_CANAL.SP_ADMON_CANAL.Inserta}");

                    break;
                case 1:
                    out = jdbcActualizaCanal.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_CANAL.SP_ADMON_CANAL.Actualiza}");

                    break;
                case 2:
                    out = jdbcConsultaCanal.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_CANAL.SP_ADMON_CANAL.Obtiene}");
                    break;
            }

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al realizar el escript con la opcion " + opcion);
            } else {

                switch (opcion) {
                    case 0:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminCanalPDDTO insert = new AdminCanalPDDTO();
                        canalDTO = new ArrayList<AdminCanalPDDTO>();

//					 insert.setIdCanal(Integer.parseInt(mensaje.get(0).getMsj()));
                        insert.setCanal("INSERT");
                        canalDTO.add(insert);
                        break;

                    case 1:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminCanalPDDTO actualiza = new AdminCanalPDDTO();
                        canalDTO = new ArrayList<AdminCanalPDDTO>();

                        actualiza.setCanal("UPDATE-" + mensaje.get(0).getMsj());
                        canalDTO.add(actualiza);
                        break;

                    case 2:
                        canalDTO = (List<AdminCanalPDDTO>) out.get("PA_CURSORCON");
                        break;
                }
                return canalDTO;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al consulta informacion del administrador de canal " + e.getMessage());
        }
        return null;
    }

    public List<AdminParametroPDDTO> admonParametros(AdminParametroPDDTO parametros, int opcion) throws Exception {
        Map<String, Object> out = null;
        List<AdminParametroPDDTO> parametrosDTO = null;
        int error = 0;
        List<ErrorPDDTO> mensaje = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", opcion)
                    .addValue("PA_ID_PARAMETRO", parametros.getIdParametro())
                    .addValue("PA_PARAMETRO", parametros.getParametro())
                    .addValue("PA_DESC_PARAMETRO", parametros.getDescParametro())
                    .addValue("PA_VALOR", parametros.getValor())
                    .addValue("PA_REFERENCIA", parametros.getReferencia())
                    .addValue("PA_ACTIVO", parametros.getActivo());

            switch (opcion) {
                case 0:
                    out = jdbcInsertaParametros.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_PARAM.SP_ADMON_PARAMETRO.Inserta}");

                    break;
                case 1:
                    out = jdbcActualizaParametros.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_PARAM.SP_ADMON_PARAMETRO.Actualiza}");

                    break;
                case 2:
                    out = jdbcConsultaParametros.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_PARAM.SP_ADMON_PARAMETRO.Obtiene}");
                    break;
            }

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al realizar el escript con la opcion " + opcion);
            } else {

                switch (opcion) {
                    case 0:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminParametroPDDTO insert = new AdminParametroPDDTO();
                        parametrosDTO = new ArrayList<AdminParametroPDDTO>();

                        insert.setIdParametro(Integer.parseInt(mensaje.get(0).getMsj()));
                        insert.setParametro("INSERT");
                        parametrosDTO.add(insert);
                        break;

                    case 1:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminParametroPDDTO actualiza = new AdminParametroPDDTO();
                        parametrosDTO = new ArrayList<AdminParametroPDDTO>();

                        actualiza.setParametro("UPDATE-" + mensaje.get(0).getMsj());
                        parametrosDTO.add(actualiza);
                        break;

                    case 2:
                        parametrosDTO = (List<AdminParametroPDDTO>) out.get("PA_CURSORCON");
                        break;
                }
                return parametrosDTO;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al consulta informacion del administrador de parametros " + e.getMessage());
        }
        return null;
    }

    public List<AdminMenuPDDTO> admonMenu(AdminMenuPDDTO menu, int opcion) throws Exception {
        Map<String, Object> out = null;
        List<AdminMenuPDDTO> menuDTO = null;
        int error = 0;
        List<ErrorPDDTO> mensaje = null;
        SqlParameterSource in = null;

        try {
            if (menu.getDependeDe() == 0) {
                in = new MapSqlParameterSource()
                        .addValue("PA_OPC", opcion)
                        .addValue("PA_ID_MENU", menu.getIdMenu())
                        .addValue("PA_DESCRIPCION", menu.getDescripcion())
                        .addValue("PA_NIVEL", menu.getNivel())
                        .addValue("PA_ACTIVO", menu.getActivo())
                        .addValue("PA_DEPENDE_DE", null)
                        .addValue("PA_ID_PERFIL", menu.getIdPerfil());
            } else {
                in = new MapSqlParameterSource()
                        .addValue("PA_OPC", opcion)
                        .addValue("PA_ID_MENU", menu.getIdMenu())
                        .addValue("PA_DESCRIPCION", menu.getDescripcion())
                        .addValue("PA_NIVEL", menu.getNivel())
                        .addValue("PA_ACTIVO", menu.getActivo())
                        .addValue("PA_DEPENDE_DE", menu.getDependeDe())
                        .addValue("PA_ID_PERFIL", menu.getIdPerfil());
            }

            switch (opcion) {
                case 0:
                    out = jdbcInsertaMenu.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_MENUPER.SP_ADMON_MENUPER.Inserta-opcopn0}");

                    break;
                case 1:
                    out = jdbcActualizaMenu.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_MENUPER.SP_ADMON_MENUPER.Actualiza-opcopn1}");

                    break;
                case 2:
                    out = jdbcObtieneMenu.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_MENUPER.SP_ADMON_MENUPER.Obtiene-opcopn2}");
                    break;

                case 3:
                    out = jdbcInsertaMenu.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_MENUPER.SP_ADMON_MENUPER.Inserta-opcopn3}");

                    break;
                case 4:
                    out = jdbcActualizaMenu.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_MENUPER.SP_ADMON_MENUPER.Actualiza-opcopn4}");

                    break;
                case 5:
                    out = jdbcObtieneMenuPerfil.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_MENUPER.SP_ADMON_MENUPER.Obtiene-opcopn5}");
                    break;
            }

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al realizar el escript con la opcion " + opcion);
            } else {

                switch (opcion) {
                    case 0:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminMenuPDDTO insert = new AdminMenuPDDTO();
                        menuDTO = new ArrayList<AdminMenuPDDTO>();

                        insert.setDescripcion("INSERT " + mensaje.get(0).getMsj());
                        menuDTO.add(insert);
                        break;

                    case 1:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminMenuPDDTO actualiza = new AdminMenuPDDTO();
                        menuDTO = new ArrayList<AdminMenuPDDTO>();

                        actualiza.setDescripcion("UPDATE-" + mensaje.get(0).getMsj());
                        menuDTO.add(actualiza);
                        break;

                    case 2:
                        menuDTO = (List<AdminMenuPDDTO>) out.get("PA_CURSORCON");
                        break;

                    case 3:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminMenuPDDTO insertP = new AdminMenuPDDTO();
                        menuDTO = new ArrayList<AdminMenuPDDTO>();

                        insertP.setDescripcion("INSERT " + mensaje.get(0).getMsj());
                        menuDTO.add(insertP);
                        break;

                    case 4:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminMenuPDDTO actualizaP = new AdminMenuPDDTO();
                        menuDTO = new ArrayList<AdminMenuPDDTO>();

                        actualizaP.setDescripcion("UPDATE-" + mensaje.get(0).getMsj());
                        menuDTO.add(actualizaP);
                        break;

                    case 5:
                        menuDTO = (List<AdminMenuPDDTO>) out.get("PA_CURSORCON");
                        break;
                }
                return menuDTO;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al consulta informacion del administrador de menu " + e.getMessage());
        }
        return null;
    }

    public List<AdminTipoDocPDDTO> admonTipoDoc(AdminTipoDocPDDTO tipoDoc, int opcion) throws Exception {
        Map<String, Object> out = null;
        List<AdminTipoDocPDDTO> parametrosDTO = null;
        int error = 0;
        List<ErrorPDDTO> mensaje = null;
        SqlParameterSource in = null;
        try {
            if (tipoDoc.getNivel() == 0 && opcion == 1) {
                in = new MapSqlParameterSource()
                        .addValue("PA_OPC", opcion)
                        .addValue("PA_ID_TIPODOC", tipoDoc.getIdTipoDoc())
                        .addValue("PA_NOMBRE", tipoDoc.getNombre())
                        .addValue("PA_NIVEL", tipoDoc.getNivel())
                        .addValue("PA_DEPENDE_DE", null)
                        .addValue("PA_ACTIVO", tipoDoc.getActivo());
            } else {
                if (tipoDoc.getActivo() == 2) {
                    in = new MapSqlParameterSource()
                            .addValue("PA_OPC", opcion)
                            .addValue("PA_ID_TIPODOC", null)
                            .addValue("PA_NOMBRE", null)
                            .addValue("PA_NIVEL", null)
                            .addValue("PA_DEPENDE_DE", null)
                            .addValue("PA_ACTIVO", null);
                } else {
                    in = new MapSqlParameterSource()
                            .addValue("PA_OPC", opcion)
                            .addValue("PA_ID_TIPODOC", tipoDoc.getIdTipoDoc())
                            .addValue("PA_NOMBRE", tipoDoc.getNombre())
                            .addValue("PA_NIVEL", tipoDoc.getNivel())
                            .addValue("PA_DEPENDE_DE", tipoDoc.getDependeDe())
                            .addValue("PA_ACTIVO", tipoDoc.getActivo());
                }
            }

            switch (opcion) {
                case 3:
                    out = jdbcInsertaActualizaTipoDoc.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMONTIPODOC.SP_ADMON_TIPODOC.Inserta}");

                    break;
                case 1:
                    out = jdbcInsertaActualizaTipoDoc.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMONTIPODOC.SP_ADMON_TIPODOC.Actualiza}");

                    break;
                case 2:
                    out = jdbcConsultaTipoDoc.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMONTIPODOC.SP_ADMON_TIPODOC.Obtiene}");
                    break;
            }

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al realizar el escript con la opcion " + opcion);
            } else {

                switch (opcion) {
                    case 3:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminTipoDocPDDTO insert = new AdminTipoDocPDDTO();
                        parametrosDTO = new ArrayList<AdminTipoDocPDDTO>();

                        insert.setIdTipoDoc(Integer.parseInt(mensaje.get(0).getMsj()));
                        insert.setDescDependeDe("INSERT");
                        parametrosDTO.add(insert);
                        break;

                    case 1:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdminTipoDocPDDTO actualiza = new AdminTipoDocPDDTO();
                        parametrosDTO = new ArrayList<AdminTipoDocPDDTO>();

                        actualiza.setDescDependeDe("UPDATE-" + mensaje.get(0).getMsj());
                        parametrosDTO.add(actualiza);
                        break;

                    case 2:
                        parametrosDTO = (List<AdminTipoDocPDDTO>) out.get("PA_CURSORCON");
                        break;
                }
                return parametrosDTO;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al consulta informacion del administrador de tipo de documentos " + e.getMessage());
        }
        return null;
    }

    @Override
    public List<AdmonEstatusTabletaPDDTO> admonEstatusTablet(AdmonEstatusTabletaPDDTO estatusTablet, int opcion)
            throws Exception {
        Map<String, Object> out = null;
        List<AdmonEstatusTabletaPDDTO> parametrosDTO = null;
        int error = 0;
        List<ErrorPDDTO> mensaje = null;
        SqlParameterSource in = null;

        try {
            if (estatusTablet.getIdEstadoTableta() == 0) {
                in = new MapSqlParameterSource()
                        .addValue("PA_OPC", opcion)
                        .addValue("PA_ID_ESTADO_TAB", estatusTablet.getIdEstadoTableta())
                        .addValue("PA_DESC_WEB", estatusTablet.getDescWEB())
                        .addValue("PA_DESC_JSON", estatusTablet.getDescJSON())
                        .addValue("PA_VISIBLE", estatusTablet.getVisible())
                        .addValue("PA_ID_ESTATUS", estatusTablet.getIdStatus())
                        .addValue("PA_DEPENDE_DE", null)
                        .addValue("PA_ACTIVO", estatusTablet.getActivo());
            } else {
                in = new MapSqlParameterSource()
                        .addValue("PA_OPC", opcion)
                        .addValue("PA_ID_ESTADO_TAB", estatusTablet.getIdEstadoTableta())
                        .addValue("PA_DESC_WEB", estatusTablet.getDescWEB())
                        .addValue("PA_DESC_JSON", estatusTablet.getDescJSON())
                        .addValue("PA_VISIBLE", estatusTablet.getVisible())
                        .addValue("PA_ID_ESTATUS", estatusTablet.getIdStatus())
                        .addValue("PA_DEPENDE_DE", estatusTablet.getDependeDe())
                        .addValue("PA_ACTIVO", estatusTablet.getActivo());
            }

            switch (opcion) {
                case 0:
                    out = jdbcInsertaActualizaEstatusTablet.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_CAT_EDO_TAB.SP_CAT_EDO_TAB.Inserta}");

                    break;
                case 1:
                    out = jdbcInsertaActualizaEstatusTablet.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_CAT_EDO_TAB.SP_CAT_EDO_TAB.Actualiza}");

                    break;
                case 2:
                    out = jdbcConsultaEstatusTablet.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_CAT_EDO_TAB.SP_CAT_EDO_TAB.Obtiene}");
                    break;
            }

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al realizar el escript con la opcion " + opcion);
            } else {

                switch (opcion) {
                    case 0:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdmonEstatusTabletaPDDTO insert = new AdmonEstatusTabletaPDDTO();
                        parametrosDTO = new ArrayList<AdmonEstatusTabletaPDDTO>();

                        //insert.setIdEstadoTableta(Integer.parseInt(mensaje.get(0).getMsj()));
                        insert.setDescWEB("INSERT " + mensaje.get(0).getMsj());
                        parametrosDTO.add(insert);
                        break;

                    case 1:
                        mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                        AdmonEstatusTabletaPDDTO actualiza = new AdmonEstatusTabletaPDDTO();
                        parametrosDTO = new ArrayList<AdmonEstatusTabletaPDDTO>();

                        actualiza.setDescWEB("UPDATE-" + mensaje.get(0).getMsj());
                        parametrosDTO.add(actualiza);
                        break;

                    case 2:
                        parametrosDTO = (List<AdmonEstatusTabletaPDDTO>) out.get("PA_CURSORCON");
                        break;
                }
                return parametrosDTO;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al consulta informacion del administrador de tipo de documentos " + e.getMessage());
        }
        return null;
    }

    @Override
    public BachDescargasPDDTO admonDescarga(BachDescargasPDDTO descarga, int opcion) throws Exception {

        Map<String, Object> out = null;
        BachDescargasPDDTO descargaBach = null;
        int error = 0;
        List<ErrorPDDTO> mensaje = null;
        List<ErrorPDDTO> mensaje2 = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", opcion)
                    .addValue("PA_TIPO_DESCARGA", descarga.getTipoDescarga())
                    .addValue("PA_NUM_SERIE", descarga.getNumSerie())
                    .addValue("PA_ID_ESTATUS", descarga.getIdEstatus())
                    .addValue("PA_FECHA_ACTU", descarga.getFechaActu());

            switch (opcion) {
                case 0:
                    out = jdbcInsertaActualizaApKDoc.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_BATCH_DESCAR.SP_BATCH_DESCAR.Inserta}");
                    break;
                case 1:
                    out = jdbcInsertaActualizaApKDoc.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_BATCH_DESCAR.SP_BATCH_DESCAR.Actualiza}");
                    break;
                case 2:
                    out = jdbcInsertaActualizaApKDoc.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_BATCH_DESCAR.SP_BATCH_DESCAR.Obtiene}");
                    break;
                case 3:
                    out = jdbcdescargaApkDoc.execute(in);
                    logger.info("Función ejecutada:{PEDESTALDIG.PA_BATCH_DESCAR.SP_BATCH_DESCAR.Obtiene}");
                    break;
            }

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al realizar el escript con la opcion " + opcion);
            } else {
                if (opcion == 3) {
                    mensaje2 = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                    descargaBach = new BachDescargasPDDTO();
                    descargaBach.setRespuesta(mensaje2.get(0).getMsj());
                    logger.info("Con la opcion " + opcion + " se contaron : " + mensaje2.get(0).getMsj());
                } else {
                    mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                    descargaBach = new BachDescargasPDDTO();
                    descargaBach.setRespuesta(mensaje.get(0).getMsj());
                    logger.info("Opcion " + opcion + ": " + mensaje.get(0).getMsj());
                }
                return descargaBach;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al consulta informacion del bach de descargas " + e.getMessage());
        }

        return null;
    }

    @Override
    public List<BachDescargasPDDTO> admonDesc(BachDescargasPDDTO descarga, int opcion) throws Exception {

        Map<String, Object> out = null;
        List<BachDescargasPDDTO> desc = null;
        int error = 0;
        List<ErrorPDDTO> mensaje = null;
        BachDescargasPDDTO descc = new BachDescargasPDDTO();

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", opcion)
                    .addValue("PA_IDCTROL_DECAR", descarga.getIdControlDesc())
                    .addValue("PA_TIPO_DESCARGA", descarga.getTipoDescarga())
                    .addValue("PA_ID_TABLETA", descarga.getIdTablet())
                    .addValue("PA_ID_ESTATUS", descarga.getIdEstatus())
                    .addValue("PA_FECHA_CAMBIO", descarga.getFechaActu())
                    .addValue("PA_ACTIVO", descarga.getActivo());

            logger.info(opcion + "*" + descarga.getTipoDescarga() + "-" + descarga.getNumSerie() + "-" + descarga.getIdEstatus() + "-" + descarga.getFechaActu() + "-" + descarga.getIdEstatus() + "-" + descarga.getFechaActu());

            switch (opcion) {
                case 0:
                    out = jdbcInsertActualizaDescarga.execute(in);
                    mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                    desc = new ArrayList<BachDescargasPDDTO>();
                    descc = new BachDescargasPDDTO();
                    descc.setRespuesta("INSERT-" + mensaje.get(0).getMsj());
                    desc = new ArrayList<BachDescargasPDDTO>();
                    desc.add(descc);
                    logger.info("Con la opcion " + opcion + " se inserto : " + mensaje.get(0).getMsj());
                    break;
                case 1:
                    out = jdbcInsertActualizaDescarga.execute(in);
                    mensaje = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                    descc = new BachDescargasPDDTO();
                    descc.setRespuesta("UPDATE-" + mensaje.get(0).getMsj());
                    desc = new ArrayList<BachDescargasPDDTO>();
                    desc.add(descc);
                    logger.info("Con la opcion " + opcion + " se actualizo : " + mensaje.get(0).getMsj());
                    break;
                case 2:
                    out = jdbcInsertActualizaDescarga.execute(in);
                    desc = (List<BachDescargasPDDTO>) out.get("PA_CURSORCON");
                    break;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al consulta informacion de descargas " + e.getMessage());
        }

        return desc;
    }

    @Override
    public List<AdminParametroPDDTO> admonParametrosByDescRef(Integer op, AdminParametroPDDTO obj) {

        Map<String, Object> out = null;
        List<AdminParametroPDDTO> parametrosDTO = null;
        int error = 0;
        List<ErrorPDDTO> mensaje = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op != null ? op.intValue() : null)
                    .addValue("PA_ID_PARAMETRO", obj != null ? obj.getIdParametro() : null)
                    .addValue("PA_PARAMETRO", obj != null ? obj.getParametro() : null)
                    .addValue("PA_DESC_PARAMETRO", obj != null ? obj.getDescParametro() : null)
                    .addValue("PA_VALOR", obj != null ? obj.getValor() : null)
                    .addValue("PA_REFERENCIA", obj != null ? obj.getReferencia() : null)
                    .addValue("PA_ACTIVO", obj != null ? obj.getActivo() : null);

            out = jdbcConsultaParametros.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_ADMON_PARAM.SP_ADMON_PARAMETRO.Obtiene}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al realizar el escript con la opcion " + op.intValue());
            } else {
                parametrosDTO = (List<AdminParametroPDDTO>) out.get("PA_CURSORCON");
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al consulta informacion del administrador de parametros " + e.getMessage());
        }

        return parametrosDTO;
    }
}
