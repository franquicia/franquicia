package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ChecklistDTO;
import java.util.List;

public interface ChecklistTDAO {

    public List<ChecklistDTO> buscaChecklistTemp(int idChecklist) throws Exception;

    public boolean actualizaChecklistTemp(ChecklistDTO bean) throws Exception;

    public boolean eliminaChecklistTemp(int idCheckList) throws Exception;

    public int insertaChecklist(ChecklistDTO bean) throws Exception;

    public boolean actualizaEdo(int idChecklist, int idEdo) throws Exception;

    public boolean ejecutaAutorizados() throws Exception;

}
