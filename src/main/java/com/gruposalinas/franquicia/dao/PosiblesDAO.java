package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PosiblesDTO;
import java.util.List;

public interface PosiblesDAO {

    public List<PosiblesDTO> buscaPosible() throws Exception;

    public int insertaPosible(String posible) throws Exception;

    public boolean actualizaPosible(PosiblesDTO bean) throws Exception;

    public boolean eliminaPosible(int idPosible) throws Exception;

}
