package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.StatusFormArchiveroDTO;
import com.gruposalinas.franquicia.mappers.StatusFormArchiverosRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class StatusFormArchiverosDAOImpl extends DefaultDAO implements StatusFormArchiveroDAO {

    private static Logger logger = LogManager.getLogger(StatusFormArchiverosDAOImpl.class);

    DefaultJdbcCall jdbcObtieneStatusFormArchiveros;
    DefaultJdbcCall jdbcInsertaStatusFormArchiveros;
    DefaultJdbcCall jdbcActualizaStatusFormArchiveros;
    DefaultJdbcCall jdbcEliminaStatusFormArchiveros;

    public void init() {

        jdbcObtieneStatusFormArchiveros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMStatusFormArchiveros")
                .withProcedureName("SP_SEL_STATUS")
                .returningResultSet("RCL_STATUS", new StatusFormArchiverosRowMapper());

        jdbcInsertaStatusFormArchiveros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSTATUS")
                .withProcedureName("SP_INS_STATUS");

        jdbcActualizaStatusFormArchiveros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSTATUS")
                .withProcedureName("SP_ACT_STATUS");

        jdbcEliminaStatusFormArchiveros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMSTATUS")
                .withProcedureName("SP_DEL_STATUS");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<StatusFormArchiveroDTO> obtieneStatusFormArchiveros(int idStatus) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<StatusFormArchiveroDTO> listaArch = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDSTATUS", idStatus);

        out = jdbcObtieneStatusFormArchiveros.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAADMSTATUS.SP_SEL_STATUS");

        listaArch = (List<StatusFormArchiveroDTO>) out.get("RCL_ARCHIVEROS");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el formato de entrega de archiveros");
        } else {
            return listaArch;
        }

        return listaArch;
    }

    @Override
    public boolean insertaStatusFormarchivero(StatusFormArchiveroDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDSTATUS", bean.getIdStatus())
                .addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jdbcInsertaStatusFormArchiveros.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMSTATUS.SP_INS_STATUS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el formato archiveros");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean modificaStatusFormarchivero(StatusFormArchiveroDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDSTATUS", bean.getIdStatus())
                .addValue("PA_DESCRIPCION", bean.getDescripcion());

        out = jdbcActualizaStatusFormArchiveros.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMSTATUS.SP_ACT_STATUS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el formato de archiveros");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean eliminaStatusFormarchivero(int idStatus) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDSTATUS", idStatus);

        out = jdbcEliminaStatusFormArchiveros.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMSTATUS.SP_DEL_STATUS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al eliminar el Formato de Archiveros");
        } else {
            return true;
        }
        return false;
    }

}
