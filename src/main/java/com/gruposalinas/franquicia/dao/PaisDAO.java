package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PaisDTO;
import java.util.List;

public interface PaisDAO {

    public List<PaisDTO> obtienePais() throws Exception;

    public List<PaisDTO> obtienePaisVisualizador() throws Exception;

    public List<PaisDTO> obtienePais(int idPais) throws Exception;

    public int insertaPais(PaisDTO bean) throws Exception;

    public boolean actualizaPais(PaisDTO bean) throws Exception;

    public boolean eliminaPais(int idPais) throws Exception;
}
