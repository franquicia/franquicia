package com.gruposalinas.franquicia.dao;

import com.google.gson.JsonObject;
import com.gruposalinas.franquicia.domain.BatchDoctoPDDTO;
import com.gruposalinas.franquicia.domain.CatalogoEstatusDocPDDTO;
import com.gruposalinas.franquicia.domain.CecoDataPDDTO;
import com.gruposalinas.franquicia.domain.CecoInfoGraphPDDTO;
import com.gruposalinas.franquicia.domain.CecosByNegPDDTO;
import com.gruposalinas.franquicia.domain.DataTabletPDDTO;
import com.gruposalinas.franquicia.domain.DatosTablaEGDTO;
import com.gruposalinas.franquicia.domain.FolioDetailPDDTO;
import com.gruposalinas.franquicia.domain.FoliosEstatusPDDTO;
import com.gruposalinas.franquicia.domain.GeoDataPDDTO;
import com.gruposalinas.franquicia.domain.HistoricTabletDataPDDTO;
import com.gruposalinas.franquicia.domain.IndicadoresDataEGDTO;
import com.gruposalinas.franquicia.domain.IndicadoresDataETDTO;
import com.gruposalinas.franquicia.domain.ListaCecoPorCSVPDDTO;
import com.gruposalinas.franquicia.domain.ListaEstatusTabletaPDDTO;
import com.gruposalinas.franquicia.domain.ParamNegocioPDDTO;
import com.gruposalinas.franquicia.domain.QuejasPDDTO;
import com.gruposalinas.franquicia.domain.RepDetaCecoPDDTO;
import com.gruposalinas.franquicia.domain.RepGralCecoPDDTO;
import com.gruposalinas.franquicia.domain.SucEstTabPDDTO;
import com.gruposalinas.franquicia.domain.SucursalInfoPDDTO;
import com.gruposalinas.franquicia.domain.SucursalTareasListPDDTO;
import com.gruposalinas.franquicia.domain.TablaEstatusGeneralDTO;
import com.gruposalinas.franquicia.domain.TabletDataFilterETDTO;
import com.gruposalinas.franquicia.domain.TabletInformationPDDTO;
import com.gruposalinas.franquicia.domain.TabletPerformancePDDTO;
import com.gruposalinas.franquicia.domain.TaskInTabletPDDAO;
import com.gruposalinas.franquicia.domain.TipoQuejaPDDTO;
import java.util.ArrayList;
import java.util.List;

public interface EstatusGeneralDAO {

    List<IndicadoresDataEGDTO> getIndicadoresEG(int op, String negocio);

    List<DatosTablaEGDTO> getDatosGraficaEG(int op, int anio, String negocio);

    List<TablaEstatusGeneralDTO> getDataForExcelEG(int op, int anio, String negocio);

    List<ListaEstatusTabletaPDDTO> getListaEstatusTableta(int op);

    String insertInternalDataTablet(int op, DataTabletPDDTO data);

    String insertHistoricDataTablet(int op, String numSerie, int idTabletEdo, String activo);

    List<IndicadoresDataETDTO> getIndicadoresET(int op);

    List<TabletDataFilterETDTO> getTabletBySucursal(int op, int numSucursal);

    List<TabletDataFilterETDTO> getTabletByTerritorio(int op, String ceco, int estatus, int rendimiento);

    List<TabletDataFilterETDTO> getTabletByPais(int op, String geo, int estatus, int rendimiento);

    String insertCriticTablet(int op, String numSerie, int estatus);

    List<TabletInformationPDDTO> getTabletInfo(int op, int idTableta);

    List<TabletPerformancePDDTO> getTabletPerformance(int op, int idTableta);

    List<SucursalInfoPDDTO> getSucursalInfo(int op, int idTableta);

    List<SucursalTareasListPDDTO> getSucursalTable(int op, int idTableta);

    List<TaskInTabletPDDAO> getTasksInTablet(int op, int sucursal, int negocio);

    List<TaskInTabletPDDAO> getLatestTaskInTablet(int op, int sucursal, int tipoDocumento);

    List<FolioDetailPDDTO> getFolioDetail(int op, int idFolio);

    String updateFechaVisualizacionFolio(int op, int idFolio, String fecha);

    List<ListaCecoPorCSVPDDTO> addSucursalesFolio(int op, int idFolio, String cecos);

    List<String> changeDocumentFolio(int op, int documentoNuevo, int documentoAnterior);

    String validaDocumentoDeFolio(int op, int idDocumentoAnt);

    String updateStatusOfTaskInTablet(int op, int sucursal, int negocio, int tipoDocumento, int idDocumento, int idDocPedestal, int idEstatus, int activo);

    String updateStatusOfTaskInTablet2(int op, int sucursal, int idDocPedestal);

    String inactivaLanzamientosDocFolio(int op, int idDoc, int activo);

    List<QuejasPDDTO> getQuejas(int op, int idComentario);

    String insertQueja(int op, JsonObject jsonObj);

    List<RepGralCecoPDDTO> filtroRepGralCec(int op, String fechaIni, String fechaEnv, String doctoVis, String categoria, String ceco);

    List<RepGralCecoPDDTO> filtroRepGralGeo(int op, String fechaIni, String fechaEnv, String doctoVis, String categoria, String geo);

    List<RepDetaCecoPDDTO> filtroRepDetaCec(int op, String fechaIni, String fechaEnv, String doctoVis, String tipoEnvio, String categoria, String ceco);

    List<RepDetaCecoPDDTO> filtroRepDetaGeo(int op, String fechaIni, String fechaEnv, String doctoVis, String tipoEnvio, String categoria, String geo);

    List<RepDetaCecoPDDTO> filtroRepDetaIdFolio(int op, int idFolio);

    String insertGeo(int op, int idGeo, int tipo, int idcc, String desc, int dependeDe, int activo);

    String updateGeo(int op, int idGeo, int tipo, int idcc, String desc, int dependeDe, int activo);

    List<GeoDataPDDTO> getGeo(int op, int idGeo, int tipo, int dependeDe);

    String insertCecoData(Integer op, String asString, Integer asInt, String asString2, String asString3, Integer asInt2, Integer asInt3, Integer asInt4, String asString4, Integer asInt5, Integer asInt6, Integer asInt7, Integer asInt8, String asString5, Integer asInt9,
            String asString6, String asString7, String asString8, String asString9, String asString10, Integer asInt10);

    String updateGeoInCeco(int op, String idCeco, int idPais, int idEstado, int idMunicipio);

    List<GeoDataPDDTO> getGeoDataById(int op, int idGeo);

    String insertTipoQueja(int op, int idComent, String coment, int activo);

    String updateTipoQueja(int op, int idComent, String coment, int activo);

    List<TipoQuejaPDDTO> getTipoQueja(int op, int idTipoCat);

    List<BatchDoctoPDDTO> actualizaEstatusFoliosPD(int op, String fecha);

    String actualizaEstatusDoctoPD(int op, int idDoc, int idTipoDocto);

    List<GeoDataPDDTO> getGeoForNull(int op, int idGeo, int tipo, int dependeDe);

    String insertEstatusDocInCat(int op, int idEstatusDoc, String descEstatus, int activo);

    String updateEstatusDocInCat(int op, int idEstatusDoc, String descEstatus, int activo);

    List<CatalogoEstatusDocPDDTO> getEstatusDocInCat(int op);

    List<CecoDataPDDTO> getCecoDataByCecoPadre(int op, int dependeDe);

    String validaMantenimientoTableta(int op, String sucursal);

    List<FoliosEstatusPDDTO> getEstatusFoliosTableta(int op, String sucursal, int idCategoria);

    List<HistoricTabletDataPDDTO> getHistoricTabletData(int op, int idSucursal);

    String actualizaRaizEnCategoria(int op, String idTipoDoc);

    String updateTabletsEstatus();

    ArrayList<ParamNegocioPDDTO> getParamNegocios();

    List<IndicadoresDataETDTO> getIndiByNeg(int op, String negocio);

    List<CecosByNegPDDTO> getCecosNegocio(String tipo, String ceco, String negocio);

    List<CecoInfoGraphPDDTO> getCecoInfoGraph(String op, String sucursal, String ceco);

    List<SucEstTabPDDTO> getSucEstTabByCeco(String op, String negocio, String ceco);

}
