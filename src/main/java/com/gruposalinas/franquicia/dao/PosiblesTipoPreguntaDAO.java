package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PosiblesTipoPreguntaDTO;
import java.util.List;

public interface PosiblesTipoPreguntaDAO {

    public List<PosiblesTipoPreguntaDTO> obtienePosiblesRespuestas(String idTipoPregunta) throws Exception;

    public int insertaPosibleTipoPregunta(int idPosible, int idTipoPregunta) throws Exception;

    public boolean actualizaPosibleTipoPregunta(PosiblesTipoPreguntaDTO posibleTipoPreguntaDTO) throws Exception;

    public boolean eliminaPosibleTipoPregunta(int idPosibleTipoPregunta) throws Exception;

}
