package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.TipoChecklistDTO;
import java.util.List;

public interface TipoChecklistDAO {

    public int insertaTipoChecklist(TipoChecklistDTO bean) throws Exception;

    public boolean eliminaTipoChecklist(int idTipoCK) throws Exception;

    public boolean actualizaTipoChecklist(TipoChecklistDTO bean) throws Exception;

    public List<TipoChecklistDTO> obtieneTipoChecklist() throws Exception;

    public List<TipoChecklistDTO> obtieneTipoChecklist(int idTipoCK) throws Exception;

}
