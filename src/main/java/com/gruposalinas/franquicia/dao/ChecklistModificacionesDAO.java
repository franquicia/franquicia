package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ChecklistActualDTO;
import java.util.List;
import java.util.Map;

public interface ChecklistModificacionesDAO {

    public List<ChecklistActualDTO> obtieneChecklistActual(int idChecklist) throws Exception;

    public Map<String, Object> obtieneChecklist(int idChecklist) throws Exception;

    public boolean autorizaChecklist(int autoriza, int idChecklist) throws Exception;

}
