package com.gruposalinas.franquicia.dao;

import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SoportePDDAOImpl extends DefaultDAO implements SoportePDDAO {

    private static Logger logger = LogManager.getLogger(SoportePDDAOImpl.class);

    private DefaultJdbcCall jdbcInsertCecoInfo;

    public void init() {
        jdbcInsertCecoInfo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_CECO")
                .withProcedureName("SP_ADMON_CECO");
    }

    @Override
    public String insertCecoInfo(int op, int idCeco, int numSucursal, String nombre, int cecoPadre, int pais, int estado, int municipio, String calle, int nivelC, int negocio, int canal, int responsable, String fccp, int geografia,
            String estado2, String municipio2, String colonia, String numExterior, String numInterior, int activo) {

        Map<String, Object> out = null;
        String resp = null;
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_CECO", idCeco)
                    .addValue("PA_SUCURSAL", numSucursal)
                    .addValue("PA_NOMBRE", nombre)
                    .addValue("PA_CECO_PADRE", cecoPadre)
                    .addValue("PA_ID_PAIS", pais)
                    .addValue("PA_ID_ESTADO", estado)
                    .addValue("PA_ID_MUNICIPIO", municipio)
                    .addValue("PA_CALLE", calle)
                    .addValue("PA_NIVEL_C", nivelC)
                    .addValue("PA_ID_NEGOCIO", negocio)
                    .addValue("PA_ID_CANAL", canal)
                    .addValue("PA_RESPONSABLE", responsable)
                    .addValue("PA_FCCP", fccp)
                    .addValue("PA_ID_GEOGRAFIA", geografia)
                    .addValue("PA_ESTADO", estado2)
                    .addValue("PA_MUNICIPIO", municipio2)
                    .addValue("PA_COLONIA", colonia)
                    .addValue("PA_NUM_EXTERIOR", numExterior)
                    .addValue("PA_NUM_INTERIOR", numInterior)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcInsertCecoInfo.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_CECO.SP_ADMON_CECO.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertCecoInfo - Algo ocurrio al insertar la informacion del ceco");
                resp = "" + error;
            } else {
                error = 0;
                return "" + error;
            }
        } catch (Exception e) {
            logger.info("insertCecoInfo - Algo ocurrio al insertar la informacion del ceco\n" + e.getMessage());
        }

        return resp;
    }
}
