package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.SucursalDTO;
import java.util.List;

public interface SucursalDAO {

    public List<SucursalDTO> obtieneSucursal() throws Exception;

    public List<SucursalDTO> obtieneSucursal(String idSucursal) throws Exception;

    public List<SucursalDTO> obtieneSucursalPaso(String idSucursal) throws Exception;

    public int insertaSucursal(SucursalDTO bean) throws Exception;

    public boolean actualizaSucursal(SucursalDTO bean) throws Exception;

    public boolean eliminaSucursal(int idSucursal) throws Exception;

    public boolean cargaSucursales() throws Exception;

    //GCC
    public List<SucursalDTO> obtieneSucursalGCC(String idSucursal) throws Exception;

    public int insertaSucursalGCC(SucursalDTO bean) throws Exception;

    public boolean actualizaSucursalGCC(SucursalDTO bean) throws Exception;

    public boolean eliminaSucursalGCC(int idSucursal) throws Exception;
}
