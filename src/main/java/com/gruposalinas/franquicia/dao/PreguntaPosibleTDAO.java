package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PreguntaPosibleTDTO;
import java.util.List;

public interface PreguntaPosibleTDAO {

    public List<PreguntaPosibleTDTO> obtienePreguntasPosibleTemp(String idPregunta) throws Exception;

    public boolean insertaPreguntaPosibleTemp(PreguntaPosibleTDTO bean) throws Exception;

    public boolean eliminaPreguntaPosibleTemp(int idPregPos, int idPregunta) throws Exception;

}
