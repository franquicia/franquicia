package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.OrdenGrupoDTO;
import java.util.List;

public interface OrdenGrupoDAO {

    public List<OrdenGrupoDTO> obtieneOrdenGrupo(String idOrdenGrupo, String idChecklist, String idGrupo) throws Exception;

    public int insertaOrdenGrupo(OrdenGrupoDTO bean) throws Exception;

    public boolean actualizaOrdenGrupo(OrdenGrupoDTO bean) throws Exception;

    public boolean eliminaOrdenGrupo(int idOrdenGrupo) throws Exception;

}
