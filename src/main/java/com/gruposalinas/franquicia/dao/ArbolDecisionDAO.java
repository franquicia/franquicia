package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ArbolDecisionDTO;
import java.util.List;

public interface ArbolDecisionDAO {

    public int insertaArbolDecision(ArbolDecisionDTO bean) throws Exception;

    public boolean actualizaArbolDecision(ArbolDecisionDTO bean) throws Exception;

    public boolean eliminaArbolDecision(int idArbolDecision) throws Exception;

    public List<ArbolDecisionDTO> buscaArbolDecision(int idChecklist) throws Exception;

    public boolean cambiaRespuestasPorId() throws Exception;

}
