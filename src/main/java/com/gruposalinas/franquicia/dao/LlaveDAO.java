package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.LlaveDTO;
import java.util.List;

public interface LlaveDAO {

    public boolean insertaLllave(LlaveDTO bean) throws Exception;

    public boolean actualizaLlave(LlaveDTO bean) throws Exception;

    public boolean eliminaLlave(int idLlave) throws Exception;

    public List<LlaveDTO> obtieneLlave() throws Exception;

    public List<LlaveDTO> obtieneLlave(int idLlave) throws Exception;

}
