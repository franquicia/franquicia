package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.CecoDTO;
import java.util.List;

public interface CecoDAO {

    public boolean insertaCeco(CecoDTO bean) throws Exception;

    public boolean actualizaCeco(CecoDTO bean) throws Exception;

    public boolean eliminaCeco(int ceco) throws Exception;

    public List<CecoDTO> buscaCeco(int ceco) throws Exception;

    public List<CecoDTO> buscaCecos(int activo) throws Exception;

    public boolean cargaCecos() throws Exception;

    public boolean cargaCecos2() throws Exception;

    public boolean cargaGeografia() throws Exception;

    public boolean updateCecos() throws Exception;

    public List<CecoDTO> buscaCecoPaso(String ceco, String cecoPadre, String descripcion) throws Exception;

    public List<CecoDTO> buscaCecosPaso() throws Exception;

    public List<CecoDTO> buscaTerritorios(int idPais) throws Exception;

    public List<CecoDTO> buscaCecosPasoP(int idCecoPadre) throws Exception;

    public int eliminaCecosTrabajo() throws Exception;

    public int[] cargaCecosSFGuatemala() throws Exception;

    public int[] cargaCecosCOMGuatemala() throws Exception;

    public int[] cargaCecosSFPeru() throws Exception;

    public int[] cargaCecosCOMPeru() throws Exception;

    public int[] cargaCecosSFHonduras() throws Exception;

    public int[] cargaCecosCOMHonduras() throws Exception;

    public int[] cargaCecosSFPanama() throws Exception;

    public int[] cargaCecosSFSalvador() throws Exception;

    public boolean eliminaDuplicados() throws Exception;
}
