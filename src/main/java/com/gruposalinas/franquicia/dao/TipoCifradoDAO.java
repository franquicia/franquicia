package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.TipoCifradoDTO;
import java.util.List;

public interface TipoCifradoDAO {

    public boolean insertaTipoCifrado(TipoCifradoDTO bean) throws Exception;

    public boolean actualizaTipoCifrado(TipoCifradoDTO bean) throws Exception;

    public boolean eliminaTipoCifrado(int bean) throws Exception;

    public List<TipoCifradoDTO> buscaTipoCifrado(TipoCifradoDTO bean) throws Exception;
}
