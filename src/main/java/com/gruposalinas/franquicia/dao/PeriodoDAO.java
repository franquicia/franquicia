package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PeriodoDTO;
import java.util.List;

public interface PeriodoDAO {

    public List<PeriodoDTO> obtienePeriodo(String idPeriodo) throws Exception;

    public int insertaPeriodo(PeriodoDTO bean) throws Exception;

    public boolean actualizaPeriodo(PeriodoDTO bean) throws Exception;

    public boolean eliminaPeriodo(int idPeriodo) throws Exception;

}
