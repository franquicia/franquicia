package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ChecklistPreguntaDTO;
import java.util.List;

public interface ChecklistPreguntaTDAO {

    public List<ChecklistPreguntaDTO> obtienePregCheckTemp(int idChecklist) throws Exception;

    public boolean insertaChecklistPreguntaTemp(ChecklistPreguntaDTO bean) throws Exception;

    public boolean actualizaChecklistPreguntaTemp(ChecklistPreguntaDTO bean) throws Exception;

    public boolean eliminaChecklistPreguntaTemp(int idChecklist, int idPregunta) throws Exception;

}
