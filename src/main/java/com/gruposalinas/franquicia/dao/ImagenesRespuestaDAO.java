package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ImagenesRespuestaDTO;
import java.util.List;

public interface ImagenesRespuestaDAO {

    public List<ImagenesRespuestaDTO> obtieneImagenes(String idArbol, String idImagen) throws Exception;

    public int insertaImagen(ImagenesRespuestaDTO imagen) throws Exception;

    public boolean eliminaImagen(int idImagen) throws Exception;

}
