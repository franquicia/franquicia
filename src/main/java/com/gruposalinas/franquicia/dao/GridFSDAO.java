package com.gruposalinas.franquicia.dao;
//import org.apache.commons.io.IOUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.mongodb.gridfs.GridFsTemplate;
//import org.springframework.stereotype.Repository;
//import org.springframework.data.mongodb.core.query.Query;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.query.Criteria;
//
//import com.gruposalinas.franquicia.util.UtilFRQ;
//import com.mongodb.BasicDBObject;
//import com.mongodb.DBCursor;
//import com.mongodb.DBObject;
//import com.mongodb.gridfs.GridFSDBFile;

//@Repository
public class GridFSDAO {
//
//	@Autowired
//	GridFsTemplate gridFsTemplate;
//
//	@Autowired
//	public MongoTemplate mongoTemplate;
//
//	public static final String COLLECTION_NAME = "fs.files";
//	public static final String JOIN_COLLECTION_NAME = "searchHistory";
//
//	// Almacena en GridFS el archivo
//	public String store(InputStream inputStream, String fileName, String contentType, DBObject metaData) {
//		return this.gridFsTemplate.store(inputStream, fileName, contentType, metaData).getId().toString();
//	}
//
//	public GridFSDBFile getById(String id) {
//		return this.gridFsTemplate.findOne(new Query(Criteria.where("_id").is(id)));
//	}
//
//	public GridFSDBFile getByFilename(String fileName) {
//		return gridFsTemplate.findOne(new Query(Criteria.where("filename").is(fileName)));
//	}
//
//	public GridFSDBFile retrive(String fileName) {
//		return gridFsTemplate.findOne(new Query(Criteria.where("filename").is(fileName)));
//	}
//
//	public List<?> findAll() {
//		return gridFsTemplate.find(null);
//	}
//
//	// Obtiene "InputStream" a partir del nombre del archivo que deseamos
//	// recuperar de GridFS
//	public InputStream getByArchiveName(String fileName) {
//		InputStream inStream = null;
//		try {
//			inStream = gridFsTemplate.getResource(fileName).getInputStream();
//
//		} catch (IllegalStateException e) {
//			// TODO Auto-generated catch block
//			UtilFRQ.printErrorLog(null, e);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			UtilFRQ.printErrorLog(null, e);
//		}
//		return inStream;
//	}
//
//	//RETURN A FILE
//	public File getFile(String fileName) {
//		try{
//			File f = null;
//			InputStream inputStream = gridFsTemplate.getResource(fileName).getInputStream();
//			try {
//				f = stream2file( inputStream );
//			} catch (IllegalStateException e) {
//				// TODO Auto-generated catch block
//				UtilFRQ.printErrorLog(null, e);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				UtilFRQ.printErrorLog(null, e);
//			} finally {
//				inputStream.close();
//			}
//			return f;
//		} catch (IOException e) {
//			return null;
//		}
//	}
//	//TEMP FILE
//		 public static final String PREFIX = "temporal";
//		    public static final String SUFFIX = ".pdf";
//
//		    public static File stream2file (InputStream in) throws IOException {
//		        final File tempFile = File.createTempFile(PREFIX, SUFFIX);
//		        tempFile.deleteOnExit();
//		        try{
//		        	FileOutputStream out = new FileOutputStream(tempFile);
//		        	try {
//		        		IOUtils.copy(in, out);
//		        	} finally {
//		        		out.close();
//		        	}
//
//		        }catch(Exception e){
//
//		        } finally {
//
//		        }
//		        return tempFile;
//		    }
//
//	// MAS VISITADOS EFECTIVO
//	public DBObject getMetadata(String fileName) {
//		GridFSDBFile gridFsdbFile = gridFsTemplate.findOne(new Query(Criteria.where("filename").is(fileName)));
//		return gridFsdbFile.getMetaData();
//	}
//
//	// MAS VISITADOS BETA
//	public DBCursor findByField(String fieldName, String fieldValue, String projectField) {
//		mongoTemplate.getCollection(COLLECTION_NAME).find();
//		DBObject findCommand = new BasicDBObject(fieldName, fieldValue);
//		DBObject projectCommand = new BasicDBObject(projectField, 1);
//		DBCursor result = mongoTemplate.getCollection(COLLECTION_NAME).find(findCommand, projectCommand);
//		return result;
//	}
//
//	/**
//	 * Elimina elementos del conjunto de resultados devueltos tras consultar
//	 * sobre el campo metadata. Si el documento en "metadataItr" ya existe en
//	 * "pageTextItr", entonces se retorna una lista con dicho elemento removido.
//	 * Se consideran duplicados cuando tienen el mismo valor para el campo
//	 * fileName.
//	 *
//	 * @param metadataItr
//	 *            resultados devueltos tras consultar sobre el campo metadata
//	 * @param pageTextItr
//	 *            resultados devueltos tras consultar sobre el campo pageText
//	 * @return lista de resultados de consulta sobre metadata con "duplicados"
//	 *         removidos
//	 */
//	public List<DBObject> removeDupMetadata(Iterable<DBObject> metadataItr, Iterable<DBObject> pageTextItr) {
//		  Iterator<DBObject> searchMDItr     = metadataItr.iterator();
//          List<DBObject> searchMDLst         = new ArrayList<DBObject>();
//          List<DBObject> searchMDLstCpy     = new ArrayList<DBObject>();
//
//          while(searchMDItr.hasNext()) {
//              DBObject objSearchMD = (DBObject) searchMDItr.next();
//              searchMDLst.add(objSearchMD);
//              searchMDLstCpy.add(objSearchMD);
//          }
//
//          for( DBObject objSearchMD : searchMDLstCpy ) {
//              DBObject searchID = (DBObject) objSearchMD.get("_id");
//              String filename = searchID.get("fileName").toString();
//
//              for ( DBObject objMainSearch : pageTextItr ) {
//
//                  DBObject mainSearchId = (DBObject) objMainSearch.get("_id");
//                  String mainSearchFN =  mainSearchId.get("fileName").toString();
//
//                  if( filename.equals(mainSearchFN) ){
//                      searchMDLst.remove(objSearchMD);
//                  }
//              }
//
//          }
//          return searchMDLst;
//	}
//
//	/**
//	 * Búsqueda en el campo metadata. Obtiene el título, descripción y el nombre
//	 * del archivo en donde encontró coincidencias tras buscar texto en los
//	 * campos metadata.title y metadata.description. Los resultados se agrupan
//	 * por nombre de archivo (filename).
//	 *
//	 * @param find
//	 *            cadena con los términos a buscar.
//	 * @return título, descripción, evaluación promedio y nombre de archivo
//	 */
//	public Iterable<DBObject> searchMetadata(String find) {
//		BasicDBObject $lookup = new BasicDBObject("$lookup", new BasicDBObject("from", JOIN_COLLECTION_NAME)
//				.append("localField", "filename").append("foreignField", "fileName").append("as", "scores"));
//
//		BasicDBObject $project = new BasicDBObject("$project",
//				new BasicDBObject("filename", 1).append("metadata", 1)
//						.append("score", new BasicDBObject("$meta", "textScore")).append("average",
//								new BasicDBObject("$avg", "$scores.score")));
//
//		BasicDBObject $groupId = new BasicDBObject("_id", new BasicDBObject("fileName", "$filename"));
//
//		$groupId = $groupId.append("title", new BasicDBObject("$first", "$metadata.title"));
//		$groupId = $groupId.append("description", new BasicDBObject("$first", "$metadata.description"));
//		$groupId = $groupId.append("count", new BasicDBObject("$sum", 1));
//		$groupId = $groupId.append("score", new BasicDBObject("$first", "$score"));
//		$groupId = $groupId.append("average", new BasicDBObject("$first", "$average"));
//		BasicDBObject $group = new BasicDBObject("$group", $groupId);
//
//		Iterable<DBObject> output = (Iterable<DBObject>) mongoTemplate.getCollection(COLLECTION_NAME)
//				.aggregate(Arrays.asList(
//						(DBObject) new BasicDBObject("$match",
//								new BasicDBObject("$text", new BasicDBObject("$search", find))),
//						(DBObject) new BasicDBObject("$sort",
//								new BasicDBObject("score", new BasicDBObject("$meta", "textScore"))),
//						$lookup, $project, (DBObject) $group,
//						(DBObject) new BasicDBObject("$sort", new BasicDBObject("score", -1))))
//				.results();
//		return output;
//	}
//	/*
//	 * 	Iterator<DBObject> searchMDItr = metadataItr.iterator();
//		List<DBObject> searchMDLst = new ArrayList<DBObject>();
//		List<DBObject> searchMDLstCpy = new ArrayList<DBObject>();
//
//	 while (searchMDItr.hasNext()) {
//			DBObject objSearchMD = (DBObject) searchMDItr.next();
//			searchMDLst.add(objSearchMD);
//			searchMDLstCpy.add(objSearchMD);
//		}
//		System.out.println("removeDupMetadata size before: " + searchMDLst.size());
//		System.out.println("TITLE");
//		for (DBObject dbo : searchMDLst) {
//			System.out.println("TITLE" + dbo.get("title"));
//		}
//		for (DBObject objSearchMD : searchMDLstCpy) {
//			DBObject searchID = (DBObject) objSearchMD.get("_id");
//			String filename = searchID.get("fileName").toString();
//
//			for (DBObject objMainSearch : pageTextItr) {
//
//				DBObject mainSearchId = (DBObject) objMainSearch.get("_id");
//				String mainSearchFN = mainSearchId.get("fileName").toString();
//
//				if (filename.equals(mainSearchFN)) {
//					searchMDLst.remove(objSearchMD);
//				}
//			}
//		}
//		System.out.println("______________");
//		for (DBObject dbo : searchMDLst) {
//			System.out.println(dbo.get("title"));
//		}
//		System.out.println("removeDupMetadata size after: " + searchMDLst.size());
//		return searchMDLst;
//	 */
}
