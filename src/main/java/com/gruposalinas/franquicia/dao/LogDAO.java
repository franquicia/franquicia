package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.LogDTO;
import java.util.List;

public interface LogDAO {

    public List<LogDTO> obtieneErrores(String fecha) throws Exception;
}
