package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.DatosUsuarioDTO;
import com.gruposalinas.franquicia.domain.Usuario_ADTO;
import java.util.List;

public interface Usuario_ADAO {

    public List<Usuario_ADTO> obtieneUsuario() throws Exception;

    public List<Usuario_ADTO> obtieneUsuario(int idUsuario) throws Exception;

    public List<Usuario_ADTO> obtieneUsuarioPaso() throws Exception;

    public List<Usuario_ADTO> obtieneUsuarioPaso(String idUsuario, String idPuesto, String idCeco) throws Exception;

    public boolean insertaUsuario(Usuario_ADTO bean) throws Exception;

    public boolean actualizaUsuario(Usuario_ADTO bean) throws Exception;

    public boolean eliminaUsuario(int idUsuario) throws Exception;

    public boolean cargaUsuarios() throws Exception;

    public boolean cargaPerfilUsuarios() throws Exception;

    public boolean bajaUsuarios() throws Exception;

    public List<DatosUsuarioDTO> obtieneDatos(String usuario) throws Exception;
}
