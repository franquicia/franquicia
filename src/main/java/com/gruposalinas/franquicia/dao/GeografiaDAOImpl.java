package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.GeografiaDTO;
import com.gruposalinas.franquicia.mappers.GeografiaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class GeografiaDAOImpl extends DefaultDAO implements GeografiaDAO {

    private static Logger logger = LogManager.getLogger(GeografiaDAOImpl.class);

    DefaultJdbcCall jdbcObtieneGeo;
    DefaultJdbcCall jdbcInsertaGeo;
    DefaultJdbcCall jdbcActualizaGeo;
    DefaultJdbcCall jdbcEliminaGeo;

    public void init() {

        jdbcObtieneGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMGEOGRAFIA")
                .withProcedureName("SP_SEL_GEO")
                .returningResultSet("RCL_GEOGRAFIA", new GeografiaRowMapper());

        jdbcInsertaGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMGEOGRAFIA")
                .withProcedureName("SP_INS_GEO");

        jdbcActualizaGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMGEOGRAFIA")
                .withProcedureName("SP_ACT_GEO");

        jdbcEliminaGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMGEOGRAFIA")
                .withProcedureName("SP_DEL_GEO");

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<GeografiaDTO> obtieneGeografia(String idCeco, String idRegion, String idZona, String idTerritorio) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<GeografiaDTO> listaGeo = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCECO", idCeco)
                .addValue("PA_IDREG", idRegion)
                .addValue("PA_IDZONA", idZona)
                .addValue("PA_IDTERRITORIO", idTerritorio);

        out = jdbcObtieneGeo.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAADMGEOGRAFIA.SP_SEL_GEO");

        listaGeo = (List<GeografiaDTO>) out.get("RCL_GEOGRAFIA");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener la Geografia");
        } else {
            return listaGeo;
        }

        return listaGeo;
    }

    @Override
    public boolean insertaGeografia(GeografiaDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCECO", bean.getIdCeco())
                .addValue("PA_IDREGION", bean.getIdRegion())
                .addValue("PA_REGION", bean.getRegion())
                .addValue("PA_IDZONA", bean.getIdZona())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_IDTERR", bean.getIdTerritorio())
                .addValue("PA_TERRITORIO", bean.getTerritorio())
                .addValue("PA_COMMIT", bean.getZona());

        out = jdbcInsertaGeo.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMGEOGRAFIA.SP_INS_GEO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Geografia");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean actualizaGeografia(GeografiaDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCECO", bean.getIdCeco())
                .addValue("PA_IDREGION", bean.getIdRegion())
                .addValue("PA_REGION", bean.getRegion())
                .addValue("PA_IDZONA", bean.getIdZona())
                .addValue("PA_ZONA", bean.getZona())
                .addValue("PA_IDTERR", bean.getIdTerritorio())
                .addValue("PA_TERRITORIO", bean.getTerritorio())
                .addValue("PA_COMMIT", bean.getZona());

        out = jdbcActualizaGeo.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMGEOGRAFIA.SP_ACT_GEO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar ChecklistPregunta");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean eliminaGeografia(int idCeco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_IDCECO", idCeco);

        out = jdbcEliminaGeo.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMGEOGRAFIA.SP_DEL_GEO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al eliminar la Geografia");
        } else {
            return true;
        }
        return false;
    }

}
