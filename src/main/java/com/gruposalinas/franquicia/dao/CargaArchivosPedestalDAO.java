package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.BusquedaCECOsDTO;
import com.gruposalinas.franquicia.domain.BusquedaGeoDTO;
import com.gruposalinas.franquicia.domain.CategoriasPDDTO;
import com.gruposalinas.franquicia.domain.CecosWSPDDTO;
import com.gruposalinas.franquicia.domain.DocCorruptosPDDTO;
import com.gruposalinas.franquicia.domain.DocInCategoriaPDDTO;
import com.gruposalinas.franquicia.domain.DocInCategoryPDDTO;
import com.gruposalinas.franquicia.domain.DocsBeforeUploadPDDTO;
import com.gruposalinas.franquicia.domain.DoctoDataPDDTO;
import com.gruposalinas.franquicia.domain.FileChecksumPDDTO;
import com.gruposalinas.franquicia.domain.FileDataPDDTO;
import com.gruposalinas.franquicia.domain.FileWithoutChecksumPDDTO;
import com.gruposalinas.franquicia.domain.FolioDataPDDTO;
import com.gruposalinas.franquicia.domain.FolioFilterDataPDDTO;
import com.gruposalinas.franquicia.domain.ListaCECOXGEOPDDTO;
import com.gruposalinas.franquicia.domain.ListaCECOsPDDTO;
import com.gruposalinas.franquicia.domain.ListaCategoriasPDDTO;
import com.gruposalinas.franquicia.domain.ListaCecoPorCSVPDDTO;
import com.gruposalinas.franquicia.domain.ListaDistribucionPDDTO;
import com.gruposalinas.franquicia.domain.ParamCecoPDDTO;
import com.gruposalinas.franquicia.domain.TabletDataForExcelPDDTO;
import com.gruposalinas.franquicia.domain.TabletDataPDDTO;
import java.util.List;

public interface CargaArchivosPedestalDAO {

    public List<BusquedaGeoDTO> busquedaGeolocalizacion(int id_opcion, String id_geo, int id_tipo, int negocio) throws Exception;

    public List<BusquedaCECOsDTO> busquedaCECOs(int id_opcion, String ceco, int id_tipo, int negocio) throws Exception;

    public List<ListaDistribucionPDDTO> consultaListaDistribucion(int idLista, int id_usuario, int negocio) throws Exception;

    public List<ListaDistribucionPDDTO> insertDistribucion(String nombre, int id_usuario, int negocio) throws Exception;

    public String actualizaDistribucion(int id_lista, String nombre, int id_usuario, int negocio, int id_activo) throws Exception;

    public List<ListaCECOsPDDTO> consultaListaCECOS(int idLista, String id_ceco) throws Exception;

    public List<ListaCECOsPDDTO> insertaListaCECOS(int idLista, String id_ceco) throws Exception;

    public List<ListaCECOsPDDTO> actualizaListaCECOS(int id_listaCecos, int idLista, String id_ceco, int id_activo) throws Exception;

    public List<ListaCecoPorCSVPDDTO> obtieneCecosPorCSV(String csvList, String negocio) throws Exception;

    List<ListaCECOXGEOPDDTO> obtieneCecoConGeo(int op, String geo, int tipo, int negocio) throws Exception;

    List<ListaCategoriasPDDTO> obtieneCategorias(int op, int nivel, int dependeDe) throws Exception;

    public String insertaLanzamientosPorCecos(int op, String cecos, int negocio, int idDocumento);

    public String insertaLanzamientosPorGeo(int op, String geo, int negocio, int tipo, int idDocumento);

    public String insertaLanzamientosPorListaDD(int op, int idLista, int negocio, int idDocumento);

    public String insertCategory(int op, int idCategoria, String descripcion, String rutaIcono, String nombreIcono, int orden);

    public String updateCategory(int op, int idCategoria, String descripcion, String rutaIcono, String nombreIcono, int orden);

    public List<CategoriasPDDTO> getCategory(int op, int idCategoria);

    public String insertDocInCategory(int op, int idDoc, String descripcion, int idCategoria, String tipo, int activo, int orden);

    public String updateDocInCategory(int op, int idDoc, String descripcion, int idCategoria, String tipo, int activo, int orden);

    public List<DocInCategoriaPDDTO> getDocInCategory(int op, int idDoc, int idCategoria);

    public String insertFileInfo(int op, int idArchivo, int idItem, String ruta, String nombre, int activo);

    public String updateFileInfo(int op, int idArchivo, int idItem, String ruta, String nombre, int activo);

    public List<FileDataPDDTO> getFileInfo(int op, int idArchivo);

    public String insertTablet(int op, int idTableta, String numSerie, String idCeco, int idUsuario, int idEstatus, String fechaInstalacion, int activo);

    public String updateTablet(int op, int idTableta, String numSerie, String idCeco, int idUsuario, int idEstatus, String fechaInstalacion, int activo);

    public List<TabletDataPDDTO> getTablet(int op, int idTableta, String numSerie);

    public String insertFolio(int op, int idFolio, String tipo, String fecha, int activo);

    public String updateFolio(int op, int idFolio, String tipo, String fecha, int activo);

    public List<FolioDataPDDTO> getFolio(int op, int idFolio);

    public String insertDocto(int op, int idDocumento, int idFolio, String nombreArchivo, int idTipoDocto, int idUsuario, long pesoBytes, String origen, String vigenciaIni, String vigenciaFin, String visibleSuc, int idEstatus, int activo);

    public String updateDocto(int op, int idDocumento, int idFolio, String nombreArchivo, int idTipoDocto, int idUsuario, long pesoBytes, String origen, String vigenciaIni, String vigenciaFin, String visibleSuc, int idEstatus, int activo);

    public List<DoctoDataPDDTO> getDocto(int op, int idDocumento);

    public List<ListaCategoriasPDDTO> obtieneDocumentos(int op, int nivel, String dependeDe);

    public List<FolioFilterDataPDDTO> getFolioDataPorFolio(int op, int idFolio);

    public List<FolioFilterDataPDDTO> getFolioDataPorTerritorio(int op, String ceco, String categoria, String fecha, String estatus, String tipo);

    public List<FolioFilterDataPDDTO> getFolioDataPorGeografia(int op, String geo, int nivel, String categoria, String fecha, String estatus, String tipo);

    public List<String> convertEconNumToCeco(int op, String numEco);

    public String getLastestDoc(int op, int idDocto);

    String updateOldDocument(int idDocumento, String vigenciaFin, String visibleSuc);

    public String insertAdmonTipoDoc(int op, int idTipoDoc, String nombre, int nivel, int dependeDe, int activo);

    public String updateAdmonTipoDoc(int op, int idTipoDoc, String nombre, int nivel, int dependeDe, int activo);

    public List<DocInCategoryPDDTO> getAdmonTipoDoc(int op, int idTipoDoc);

    public String updateTabletCriticStatus(int i, String numSerie, int idEstatus);

    public List<TabletDataForExcelPDDTO> getAllTabletsForExcel(int op);

    public String insertUpdateFileChecksum(Integer op, Integer idEncArch, Integer idRef, String origen, String checksum, Integer activo);

    public List<FileChecksumPDDTO> getFileChecksum(Integer op, Integer idEncArch, Integer idRef, String origen, String checksum, Integer activo);

    public List<FileWithoutChecksumPDDTO> getFileChecksum(Integer op);

    public String truncateTPasoCeco();

    public String insertTPasoCeco(CecosWSPDDTO ceco);

    public List<ParamCecoPDDTO> getCecoParameters();

    public String updateCecosByParam(Integer op, String negocio, Integer idCeco);

    public List<DocCorruptosPDDTO> getDocCorruptos(Integer op, String idCeco, Integer folio, Integer idDocto, Integer fecha);

    public List<DocsBeforeUploadPDDTO> getDocsBeforeUpload(Integer op, Integer tipoDoc);

    public List<DocsBeforeUploadPDDTO> getExpiringDocs(int op);

    public List<DocsBeforeUploadPDDTO> getHistoryOfDocs(int op);

    public List<DocsBeforeUploadPDDTO> getAllFoliosData(int op);
}
