package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.BusquedaCECOsDTO;
import com.gruposalinas.franquicia.domain.BusquedaGeoDTO;
import com.gruposalinas.franquicia.domain.CategoriasPDDTO;
import com.gruposalinas.franquicia.domain.CecosWSPDDTO;
import com.gruposalinas.franquicia.domain.DocCorruptosPDDTO;
import com.gruposalinas.franquicia.domain.DocInCategoriaPDDTO;
import com.gruposalinas.franquicia.domain.DocInCategoryPDDTO;
import com.gruposalinas.franquicia.domain.DocsBeforeUploadPDDTO;
import com.gruposalinas.franquicia.domain.DoctoDataPDDTO;
import com.gruposalinas.franquicia.domain.ErrorPDDTO;
import com.gruposalinas.franquicia.domain.FileChecksumPDDTO;
import com.gruposalinas.franquicia.domain.FileDataPDDTO;
import com.gruposalinas.franquicia.domain.FileWithoutChecksumPDDTO;
import com.gruposalinas.franquicia.domain.FolioDataPDDTO;
import com.gruposalinas.franquicia.domain.FolioFilterDataPDDTO;
import com.gruposalinas.franquicia.domain.ListaCECOXGEOPDDTO;
import com.gruposalinas.franquicia.domain.ListaCECOsPDDTO;
import com.gruposalinas.franquicia.domain.ListaCategoriasPDDTO;
import com.gruposalinas.franquicia.domain.ListaCecoPorCSVPDDTO;
import com.gruposalinas.franquicia.domain.ListaDistribucionPDDTO;
import com.gruposalinas.franquicia.domain.ParamCecoPDDTO;
import com.gruposalinas.franquicia.domain.TabletDataForExcelPDDTO;
import com.gruposalinas.franquicia.domain.TabletDataPDDTO;
import com.gruposalinas.franquicia.mappers.BusquedaCECOsRowMapperPD;
import com.gruposalinas.franquicia.mappers.BusquedaGeoRowMapperPD;
import com.gruposalinas.franquicia.mappers.CategoriasRowMapperPD;
import com.gruposalinas.franquicia.mappers.DocCorruptosRowMapperPD;
import com.gruposalinas.franquicia.mappers.DocInCategory2RowMapperPD;
import com.gruposalinas.franquicia.mappers.DocInCategoryRowMapperPD;
import com.gruposalinas.franquicia.mappers.DocsBeforeUploadRowMapperPD;
import com.gruposalinas.franquicia.mappers.DoctoDataRowMapperPD;
import com.gruposalinas.franquicia.mappers.ErrorRowMapperPD;
import com.gruposalinas.franquicia.mappers.FileChecksumRowMapperPD;
import com.gruposalinas.franquicia.mappers.FileDataRowMapperPD;
import com.gruposalinas.franquicia.mappers.FileWithoutChecksumRowMapperPD;
import com.gruposalinas.franquicia.mappers.FolioDataRowMapperPD;
import com.gruposalinas.franquicia.mappers.FolioFilterDataRowMapperPD;
import com.gruposalinas.franquicia.mappers.ListaCECOXGEORowMapperPD;
import com.gruposalinas.franquicia.mappers.ListaCECOsRowMapperPD;
import com.gruposalinas.franquicia.mappers.ListaCategoriasRowMapperPD;
import com.gruposalinas.franquicia.mappers.ListaCecoPorCSVRowMapperPD;
import com.gruposalinas.franquicia.mappers.ListaDistribucionRowMapperPD;
import com.gruposalinas.franquicia.mappers.NumEcoCecoRowMapperPD;
import com.gruposalinas.franquicia.mappers.ParamCecoRowMapperPD;
import com.gruposalinas.franquicia.mappers.TabletDataForExcelRowMapperPD;
import com.gruposalinas.franquicia.mappers.TabletDataRowMapperPD;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CargaArchivosPedestalDAOImpl extends DefaultDAO implements CargaArchivosPedestalDAO {

    private static Logger logger = LogManager.getLogger(CargaArchivosPedestalDAOImpl.class);

    private DefaultJdbcCall jdbcConsultaGeolocalizacion, jdbcConsultaCECOs, jdbcConsultaCECOxGEO, jdbclistaDistribucion, jdbcinsertListaDistribucion, jdbcgetListaCecosPorCSV;
    private DefaultJdbcCall jdbcConsultaListaCECOS, jdbcinsertListaCECOS;
    private DefaultJdbcCall jdbcgetListaCategorias, jdbcgetListaDocumentos;
    private DefaultJdbcCall jdbcinsertLanzamientoPorCecos, jdbcinsertLanzamientoPorGeo, jdbcinsertLanzamientoPorDD;
    private DefaultJdbcCall jdbcInsertCategory, jdbcUpdateCategory, jdbcSelectCategory;
    private DefaultJdbcCall jdbcInsertDocInCategory, jdbcUpdateDocInCategory, jdbcSelectDocInCategory;
    private DefaultJdbcCall jdbcInsertFileData, jdbcUpdateFileData, jdbcSelectFileData;
    private DefaultJdbcCall jbdcInsertTablet, jbdcUpdateTablet, jbdcGetTablet, jbdcGetAllTabletsForExcel;
    private DefaultJdbcCall jdbcInsertFolio, jdbcUpdateFolio, jdbcGetFolio;
    private DefaultJdbcCall jdbcInsertDocto, jdbcUpdateDocto, jdbcGetDocto;
    private DefaultJdbcCall jdbcGetFolioData, jdbcGetFolioPorTerritorio, jdbcGetFolioPorGeografia, jdbcGetCecoPorNumEco;
    private DefaultJdbcCall jdbcInsertAdmonTipoDoc, jdbcUpdateAdmonTipoDoc, jdbcSelectAdmonTipoDoc;
    private DefaultJdbcCall jdbcInsertUpdateFileChecksum, jdbcGetFileChecksum, jdbcGetFilesWithoutChecksum;
    private DefaultJdbcCall jdbcGetDocCorruptos;
    private DefaultJdbcCall jdbcGetDocsBeforeUpload, jdbcGetExpiringDocs, jdbcGetHistoryOfDocs, jdbcGetAllFoliosData;

    /**
     * Variables para proceso que trunca e inserta cecos en tabla PDTP_PASO_CECO
     */
    private DefaultJdbcCall jdbcTruncateTPasoCeco, jdbcInsertUpdateTPasoCeco;
    private DefaultJdbcCall jdbcGetCecoParamters, jdbcUpdateCecosByParam;

    public void init() {
        jdbcConsultaGeolocalizacion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_GEO")
                .withProcedureName("SP_CONSUL_GEO")
                .returningResultSet("PA_CURSORCON", new BusquedaGeoRowMapperPD());

        jdbcConsultaCECOs = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_GEO")
                .withProcedureName("SP_CONSUL_GEO")
                .returningResultSet("PA_CURSORCON", new BusquedaCECOsRowMapperPD());

        jdbcConsultaCECOxGEO = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_GEO")
                .withProcedureName("SP_CONSUL_GEO")
                .returningResultSet("PA_CURSORCON", new ListaCECOXGEORowMapperPD());

        jdbclistaDistribucion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_LISTA_DISTRIB")
                .withProcedureName("SP_LISTA_DISTRIB")
                .returningResultSet("PA_CURSORCON", new ListaDistribucionRowMapperPD());

        jdbcinsertListaDistribucion = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_LISTA_DISTRIB")
                .withProcedureName("SP_LISTA_DISTRIB");

        jdbcConsultaListaCECOS = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_LISTA_D_CECO")
                .withProcedureName("SP_LISTA_CECO")
                .returningResultSet("PA_CURSORCON", new ListaCECOsRowMapperPD());

        jdbcinsertListaCECOS = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_LISTA_D_CECO")
                .withProcedureName("SP_LISTA_CECO");

        jdbcgetListaCecosPorCSV = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_CECO")
                .withProcedureName("SP_CONSUL_CECO")
                .returningResultSet("PA_CURSORCON", new ListaCecoPorCSVRowMapperPD());

        jdbcgetListaCategorias = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMONTIPODOC")
                .withProcedureName("SP_ADMON_TIPODOC")
                .returningResultSet("PA_CURSORCON", new ListaCategoriasRowMapperPD());

        jdbcgetListaDocumentos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSULTIPODOC")
                .withProcedureName("SP_CONSUL_TIPODOC")
                .returningResultSet("PA_CURSORCON", new ListaCategoriasRowMapperPD());

        jdbcinsertLanzamientoPorCecos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_INS_DOCPEDES")
                .withProcedureName("SP_INSER_D_PEDES");

        jdbcinsertLanzamientoPorGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_INS_DOCPEDES")
                .withProcedureName("SP_INSER_D_PEDES");

        jdbcinsertLanzamientoPorDD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_INS_DOCPEDES")
                .withProcedureName("SP_INSER_D_PEDES");

        /**
         * *************************
         */
        /**
         * Servicios para tabletas
         */
        /**
         * *************************
         */
        jdbcInsertCategory = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PA_ADMON_CATEGORIA")
                .withProcedureName("SP_ADMON_CATEGORIA");

        jdbcUpdateCategory = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PA_ADMON_CATEGORIA")
                .withProcedureName("SP_ADMON_CATEGORIA");

        jdbcSelectCategory = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PA_ADMON_CATEGORIA")
                .withProcedureName("SP_ADMON_CATEGORIA")
                .returningResultSet("PA_CURSORCON", new CategoriasRowMapperPD());

        jdbcInsertDocInCategory = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PA_ADMONMENUCAT")
                .withProcedureName("SP_ADMONMENUCAT");

        jdbcUpdateDocInCategory = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PA_ADMONMENUCAT")
                .withProcedureName("SP_ADMONMENUCAT")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcSelectDocInCategory = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PA_ADMONMENUCAT")
                .withProcedureName("SP_ADMONMENUCAT")
                .returningResultSet("PA_CURSORCON", new DocInCategoryRowMapperPD());

        jdbcInsertFileData = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PA_ADMON_ARCHIVO")
                .withProcedureName("SP_ADMON_ARCHIVO");

        jdbcUpdateFileData = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PA_ADMON_ARCHIVO")
                .withProcedureName("SP_ADMON_ARCHIVO");

        jdbcSelectFileData = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("GESTION")
                .withCatalogName("PA_ADMON_ARCHIVO")
                .withProcedureName("SP_ADMON_ARCHIVO")
                .returningResultSet("PA_CURSORCON", new FileDataRowMapperPD());

        jbdcInsertTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_TABLETA")
                .withProcedureName("SP_ADMON_TABLET");

        jbdcUpdateTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_TABLETA")
                .withProcedureName("SP_ADMON_TABLET");

        jbdcGetTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_TABLETA")
                .withProcedureName("SP_ADMON_TABLET")
                .returningResultSet("PA_CURSORCON", new TabletDataRowMapperPD());

        jbdcGetAllTabletsForExcel = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_TABLETA")
                .withProcedureName("SP_ADMON_TABLET")
                .returningResultSet("PA_CURSORCON", new TabletDataForExcelRowMapperPD());

        /**
         * *******************************
         */
        /**
         * Servicios para obtener folio *
         */
        /**
         * *******************************
         */
        jdbcInsertFolio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_FOLIO")
                .withProcedureName("SP_ADMON_FOLIO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcUpdateFolio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_FOLIO")
                .withProcedureName("SP_ADMON_FOLIO");

        jdbcGetFolio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_FOLIO")
                .withProcedureName("SP_ADMON_FOLIO")
                .returningResultSet("PA_CURSORCON", new FolioDataRowMapperPD());

        /**
         * **********************************************
         */
        /**
         * Servicios para administracion de documentos *
         */
        /**
         * **********************************************
         */
        jdbcInsertDocto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_DOCTO")
                .withProcedureName("SP_ADMON_DOCTO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcUpdateDocto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_DOCTO")
                .withProcedureName("SP_ADMON_DOCTO");

        jdbcGetDocto = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_DOCTO")
                .withProcedureName("SP_ADMON_DOCTO")
                .returningResultSet("PA_CURSORCON", new DoctoDataRowMapperPD());

        /**
         * ***************************************************
         */
        jdbcGetFolioData = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_FOLIO")
                .withProcedureName("SP_CONSUL_FOLIO")
                .returningResultSet("PA_CURSORCON", new FolioFilterDataRowMapperPD());

        jdbcGetFolioPorTerritorio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_TERRI")
                .withProcedureName("SP_CONSUL_TERRI")
                .returningResultSet("PA_CURSORCON", new FolioFilterDataRowMapperPD());

        jdbcGetCecoPorNumEco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_TERRI")
                .withProcedureName("SP_CONSUL_TERRI")
                .returningResultSet("PA_CURSORCON", new NumEcoCecoRowMapperPD());

        jdbcGetFolioPorGeografia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_PAIS")
                .withProcedureName("SP_CONSUL_PAIS")
                .returningResultSet("PA_CURSORCON", new FolioFilterDataRowMapperPD());

        /**
         * *********************************************************************
         */
        /**
         * *********** SERVICIOS PARA EL FLUJO DE CARGA DE ARCHIVOS
         * ************
         */
        /**
         * *********************************************************************
         */
        jdbcInsertAdmonTipoDoc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMONTIPODOC")
                .withProcedureName("SP_ADMON_TIPODOC")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcUpdateAdmonTipoDoc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMONTIPODOC")
                .withProcedureName("SP_ADMON_TIPODOC")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcSelectAdmonTipoDoc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMONTIPODOC")
                .withProcedureName("SP_ADMON_TIPODOC")
                .returningResultSet("PA_CURSORCON", new DocInCategory2RowMapperPD());

        /**
         * *****************************************************
         */
        /**
         * SERVICIOS PARA REGISTRO Y CONSULTA DE CHECKSUM MD5 *
         */
        /**
         * *****************************************************
         */
        jdbcInsertUpdateFileChecksum = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_ENCARC")
                .withProcedureName("SP_ADMON_ENCARC")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcGetFileChecksum = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_ENCARC")
                .withProcedureName("SP_SELE_ENCARC")
                .returningResultSet("PA_CURSORCON", new FileChecksumRowMapperPD());

        jdbcGetFilesWithoutChecksum = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_ENCARC")
                .withProcedureName("SP_SELE_DOCENC")
                .returningResultSet("PA_CURSORCON", new FileWithoutChecksumRowMapperPD());

        /**
         * Variables para proceso que trunca e inserta cecos en tabla
         * PDTP_PASO_CECO
         */
        jdbcTruncateTPasoCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_TPCECO")
                .withProcedureName("SP_TRUCA_TPCECO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcInsertUpdateTPasoCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_TPCECO")
                .withProcedureName("SP_INSERT_TPCECO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcGetCecoParamters = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ACTUALIZACECO")
                .withProcedureName("SP_CECO_INICIO")
                .returningResultSet("PA_CURSORCON", new ParamCecoRowMapperPD());

        jdbcUpdateCecosByParam = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ACTUALIZACECO")
                .withProcedureName("SP_ACTUALIZACECO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcGetDocCorruptos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_DOCTO_SUC")
                .withProcedureName("SP_CONS_DOC_SUC")
                .returningResultSet("PA_CURSORCON", new DocCorruptosRowMapperPD());

        jdbcGetDocsBeforeUpload = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_DOC")
                .withProcedureName("SP_CONSUL_DOC")
                .returningResultSet("PA_CURSORCON", new DocsBeforeUploadRowMapperPD());

        jdbcGetExpiringDocs = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_DOC")
                .withProcedureName("SP_CONSUL_DOC")
                .returningResultSet("PA_CURSORCON", new DocsBeforeUploadRowMapperPD());

        jdbcGetHistoryOfDocs = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_DOC")
                .withProcedureName("SP_CONSUL_DOC")
                .returningResultSet("PA_CURSORCON", new DocsBeforeUploadRowMapperPD());

        jdbcGetAllFoliosData = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_DOC")
                .withProcedureName("SP_CONSUL_DOC")
                .returningResultSet("PA_CURSORCON", new DocsBeforeUploadRowMapperPD());

    }

    @Override
    public List<BusquedaGeoDTO> busquedaGeolocalizacion(int id_opcion, String id_geo, int id_tipo, int negocio) throws Exception {
        Map<String, Object> out = null;
        List<BusquedaGeoDTO> listGeo = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 0)
                    .addValue("PA_ID_GEOGRAF", id_geo)
                    .addValue("PA_CECO", null)
                    .addValue("PA_TIPO", id_tipo)
                    .addValue("PA_NEGOCIO", negocio);

            out = jdbcConsultaGeolocalizacion.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_CONSUL_GEO.SP_CONSUL_GEO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            listGeo = (List<BusquedaGeoDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion de la consulta a geografía");
            } else {
                return listGeo;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al obtener la consulta geolocalización " + e.getMessage());
        }

        return null;
    }

    @Override
    public List<BusquedaCECOsDTO> busquedaCECOs(int id_opcion, String ceco, int id_tipo, int negocio) throws Exception {
        Map<String, Object> out = null;
        List<BusquedaCECOsDTO> listGeo = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 1)
                    .addValue("PA_ID_GEOGRAF", null)
                    .addValue("PA_CECO", ceco)
                    .addValue("PA_TIPO", id_tipo)
                    .addValue("PA_NEGOCIO", negocio);

            out = jdbcConsultaCECOs.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_CONSUL_GEO.SP_CONSUL_GEO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            listGeo = (List<BusquedaCECOsDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion de la consulta a geografía");
            } else {
                return listGeo;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al obtener la consulta geolicalización " + e.getMessage());
        }

        return null;
    }

    @Override
    public List<ListaCECOXGEOPDDTO> obtieneCecoConGeo(int op, String geo, int tipo, int negocio) throws Exception {
        Map<String, Object> out = null;
        List<ListaCECOXGEOPDDTO> list = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 2)
                    .addValue("PA_ID_GEOGRAF", geo)
                    .addValue("PA_CECO", null)
                    .addValue("PA_TIPO", tipo)
                    .addValue("PA_NEGOCIO", negocio);

            out = jdbcConsultaCECOxGEO.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_CONSUL_GEO.SP_CONSUL_GEO.OP_2}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion de la consulta a geografía");
            } else {
                list = (List<ListaCECOXGEOPDDTO>) out.get("PA_CURSORCON");
                return list;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al obtener la consulta geolicalización " + e.getMessage());
        }

        return null;
    }

    public List<ListaDistribucionPDDTO> consultaListaDistribucion(int idLista, int id_usuario, int negocio) throws Exception {

        Map<String, Object> out = null;
        List<ListaDistribucionPDDTO> listDistribucion = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 1)
                    .addValue("PA_ID_LISTA", idLista)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_NEGOCIO", negocio)
                    .addValue("PA_USUARIO", id_usuario)
                    .addValue("PA_ACTIVO", 0);

            out = jdbclistaDistribucion.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_LISTA_DISTRIB.SP_LISTA_DISTRIB.consulta}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            listDistribucion = (List<ListaDistribucionPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("Algo ocurrió al consulta listas de distribucion -");
            } else {
                return listDistribucion;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al obtener la consulta geolocalizacion " + e.getMessage());
        }

        return null;
    }

    public List<ListaDistribucionPDDTO> insertDistribucion(String nombre, int id_usuario, int negocio) throws Exception {

        Map<String, Object> out = null;
        List<ListaDistribucionPDDTO> listDistribucion = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 0)
                    .addValue("PA_ID_LISTA", 0)
                    .addValue("PA_NOMBRE", nombre)
                    .addValue("PA_NEGOCIO", negocio)
                    .addValue("PA_USUARIO", id_usuario)
                    .addValue("PA_ACTIVO", 1);

            out = jdbcinsertListaDistribucion.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_LISTA_DISTRIB.SP_LISTA_DISTRIB.insert}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            listDistribucion = (List<ListaDistribucionPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("Algo ocurrió al insertar la lista de distribucion -");
            } else {
                return listDistribucion;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al obtener la consulta geolocalizacion " + e.getMessage());
        }

        return null;
    }

    public String actualizaDistribucion(int id_lista, String nombre, int id_usuario, int negocio, int id_activo) throws Exception {

        Map<String, Object> out = null;
        String respuesta;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 0)
                    .addValue("PA_ID_LISTA", id_lista)
                    .addValue("PA_NOMBRE", nombre)
                    .addValue("PA_NEGOCIO", negocio)
                    .addValue("PA_USUARIO", id_usuario)
                    .addValue("PA_ACTIVO", id_activo);

            out = jdbcinsertListaDistribucion.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_LISTA_DISTRIB.SP_LISTA_DISTRIB}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al actualizar la lista de distribucion - ");
            } else {
                respuesta = "{\"status\":\"" + error + "\",\"msj\":\"Se actualizo correctamente.\"}";
                return respuesta;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al obtener la consulta geolocalizacion " + e.getMessage());
        }

        return null;
    }

    public List<ListaCECOsPDDTO> consultaListaCECOS(int idLista, String id_ceco) throws Exception {

        Map<String, Object> out = null;
        List<ListaCECOsPDDTO> listcecos = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 1)
                    .addValue("PA_ID_LISTA_C", 0)
                    .addValue("PA_ID_LISTA_DIS", idLista)
                    .addValue("PA_CECO", id_ceco)
                    .addValue("PA_ACTIVO", 0);

            out = jdbcConsultaListaCECOS.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_LISTA_D_CECO.SP_LISTA_CECO.consulta}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            listcecos = (List<ListaCECOsPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion de la consulta a geografia");
            } else {
                return listcecos;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al obtener la consulta geolocalizacion " + e.getMessage());
        }

        return null;
    }

    public List<ListaCECOsPDDTO> insertaListaCECOS(int idLista, String id_ceco) throws Exception {

        Map<String, Object> out = null;
        List<ListaCECOsPDDTO> listcecos = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 0)
                    .addValue("PA_ID_LISTA_C", 0)
                    .addValue("PA_ID_LISTA_DIS", idLista)
                    .addValue("PA_CECO", id_ceco)
                    .addValue("PA_ACTIVO", 1);

            out = jdbcinsertListaCECOS.execute(in);
            logger.info("Función ejecutada:{PEDESTALDIG.PA_LISTA_D_CECO.SP_LISTA_CECO.insertar}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            listcecos = (List<ListaCECOsPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion de la consulta a geografia");
            } else {
                return listcecos;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al obtener la consulta geolocalizacion " + e.getMessage());
        }

        return null;
    }

    public List<ListaCECOsPDDTO> actualizaListaCECOS(int id_listaCeco, int idLista, String id_ceco, int id_activo) throws Exception {

        Map<String, Object> out = null;
        List<ListaCECOsPDDTO> listcecos = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 0)
                    .addValue("PA_ID_LISTA_C", id_listaCeco)
                    .addValue("PA_ID_LISTA_DIS", idLista)
                    .addValue("PA_CECO", id_ceco)
                    .addValue("PA_ACTIVO", id_activo);

            out = jdbcinsertListaCECOS.execute(in);
            logger.info("Funcion ejecutada:{PEDESTALDIG.PA_LISTA_D_CECO.SP_LISTA_CECO.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            listcecos = (List<ListaCECOsPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion de la consulta a geografia");
            } else {
                return listcecos;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al obtener la consulta geolocalizacion " + e.getMessage());
        }

        return null;
    }

    public List<ListaCecoPorCSVPDDTO> obtieneCecosPorCSV(String csvList, String negocio) {

        Map<String, Object> out = null;
        List<ListaCecoPorCSVPDDTO> listaCecos = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 0)
                    .addValue("PA_CECO", csvList != null ? csvList : null)
                    .addValue("PA_NEGOCIO", negocio != null ? negocio : null);

            out = jdbcgetListaCecosPorCSV.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_CONSUL_CECO.SP_CONSUL_CECO.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            listaCecos = (List<ListaCecoPorCSVPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion de la consulta a geografia");
            } else {
                return listaCecos;
            }
        } catch (Exception e) {
            logger.info("Catch - Algo ocurrio al obtener la consulta geolocalizacion " + e.getMessage());
        }

        return listaCecos;
    }

    @Override
    public List<ListaCategoriasPDDTO> obtieneCategorias(int op, int nivel, int dependeDe) throws Exception {

        Map<String, Object> out = null;
        List<ListaCategoriasPDDTO> lista = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_NIVEL", nivel)
                    .addValue("PA_DEPENDE_DE", dependeDe)
                    .addValue("PA_ID_TIPODOC", null)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcgetListaCategorias.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMONTIPODOC.SP_ADMON_TIPODOC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            lista = (List<ListaCategoriasPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("obtieneCategorias - Algo ocurrió al obtener la informacion de la consulta a geografia");
            } else {
                return lista;
            }
        } catch (Exception e) {
            logger.info("obtieneCategorias - Algo ocurrio al obtener la consulta geolocalizacion\n" + e.getMessage());
        }

        return lista;
    }

    @Override
    public String insertaLanzamientosPorCecos(int op, String cecos, int negocio, int idDocumento) {

        Map<String, Object> out = null;
        String resp = "Algo ocurrio al realizar los lanzamientos por ceco";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_CECO", cecos)
                    .addValue("PA_ID_GEO", null)
                    .addValue("PA_NEGOCIO", negocio)
                    .addValue("PA_TIPO", null)
                    .addValue("PA_ID_DOCTO", idDocumento)
                    .addValue("PA_ID_LISTA", null);

            out = jdbcinsertLanzamientoPorCecos.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMONTIPODOC.SP_ADMON_TIPODOC.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertaLanzamientosPorCecos - Algo ocurrio al realizar los lanzamientos por ceco");
            } else {
                resp = "{\n\t\"respuesta\":" + error + ",\n\t\"msj\":\"Lanzamiento por ceco realizado correctamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertaLanzamientosPorCecos - Algo ocurrio al realizar los lanzamientos por ceco\n" + e.getMessage());
        }

        return "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
    }

    @Override
    public String insertaLanzamientosPorGeo(int op, String geo, int negocio, int tipo, int idDocumento) {

        Map<String, Object> out = null;
        String resp = "Algo ocurrio al realizar los lanzamientos por geografia";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_CECO", null)
                    .addValue("PA_ID_GEO", geo)
                    .addValue("PA_NEGOCIO", negocio)
                    .addValue("PA_TIPO", tipo)
                    .addValue("PA_ID_DOCTO", idDocumento)
                    .addValue("PA_ID_LISTA", null);

            out = jdbcinsertLanzamientoPorGeo.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMONTIPODOC.SP_ADMON_TIPODOC.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertaLanzamientosPorGeo - Algo ocurrió al realizar los lanzamientos por geografía");
            } else {
                error = 0;
                resp = "{\n\t\"respuesta\":" + error + ",\n\t\"msj\":\"Lanzamiento por geografia realizado correctamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertaLanzamientosPorGeo - Algo ocurrio al realizar los lanzamientos por geografia\n" + e.getMessage());
        }

        return "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
    }

    @Override
    public String insertaLanzamientosPorListaDD(int op, int idLista, int negocio, int idDocumento) {

        Map<String, Object> out = null;
        String resp = "Algo ocurrio al realizar los lanzamientos por lista de distribucion";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_CECO", null)
                    .addValue("PA_ID_GEO", null)
                    .addValue("PA_NEGOCIO", negocio)
                    .addValue("PA_TIPO", null)
                    .addValue("PA_ID_DOCTO", idDocumento)
                    .addValue("PA_ID_LISTA", idLista);

            out = jdbcinsertLanzamientoPorDD.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMONTIPODOC.SP_ADMON_TIPODOC.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertaLanzamientosPorListaDD - Algo ocurrio al realizar los lanzamientos por lista de distribucion");
            } else {
                error = 0;
                resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"Lanzamiento por lista de distribucion realizado correctamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertaLanzamientosPorListaDD - Algo ocurrio al realizar los lanzamientos por lista de distribucion\n" + e.getMessage());
        }

        return "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
    }

    /**
     * *************************************************************************
     */
    /**
     * *********************** SERVICIOS PARA LA TABLET
     * ************************
     */
    /**
     * *************************************************************************
     */
    @Override
    public String insertCategory(int op, int idCategoria, String descripcion, String rutaIcono, String nombreIcono, int orden) {

        Map<String, Object> out = null;
        String resp = "Algo ocurrio al insertar la categoria";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_CATEGORIA", idCategoria)
                    .addValue("PA_DESCRIPCION", descripcion)
                    .addValue("PA_RUTA_ICONO", rutaIcono)
                    .addValue("PA_NOMBRE_ICONO", nombreIcono)
                    .addValue("PA_ORDEN", orden);

            out = jdbcInsertCategory.execute(in);
            logger.info("Función ejecutada: {GESTION.PA_ADMON_CATEGORIA.SP_ADMON_CATEGORIA.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTA");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertaCategory - Algo ocurrio al insertar la categoría");
            } else {
                error = 0;
                resp = "{\n\t\"resp\":\"" + error + "\",\n\t\"msj\":\"Categoria insertada correctamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertaCategory - Algo ocurrio al insertar la categoría\n" + e.getMessage());
        }

        resp = "{\n\t\"resp\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public String updateCategory(int op, int idCategoria, String descripcion, String rutaIcono, String nombreIcono, int orden) {

        Map<String, Object> out = null;
        String resp = "Algo ocurrio al actualizar la categoría";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_CATEGORIA", idCategoria)
                    .addValue("PA_DESCRIPCION", descripcion)
                    .addValue("PA_RUTA_ICONO", rutaIcono)
                    .addValue("PA_NOMBRE_ICONO", nombreIcono)
                    .addValue("PA_ORDEN", orden);

            out = jdbcUpdateCategory.execute(in);
            logger.info("Función ejecutada: {GESTION.PA_ADMON_CATEGORIA.SP_ADMON_CATEGORIA.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTA");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateCategory - Algo ocurrio al actualizar la categoría");
            } else {
                error = 0;
                resp = "{\n\t\"resp\":\"" + error + "\",\n\t\"msj\":\"Categoria actualizada correctamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("updateCategory - Algo ocurrio al actualizar la categoría\n" + e.getMessage());
        }

        resp = "{\n\t\"resp\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public List<CategoriasPDDTO> getCategory(int op, int idCategoria) {

        Map<String, Object> out = null;
        List<CategoriasPDDTO> lista = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_CATEGORIA", idCategoria)
                    .addValue("PA_DESCRIPCION", null)
                    .addValue("PA_RUTA_ICONO", null)
                    .addValue("PA_NOMBRE_ICONO", null)
                    .addValue("PA_ORDEN", null);

            out = jdbcSelectCategory.execute(in);
            logger.info("Función ejecutada: {GESTION.PA_ADMON_CATEGORIA.SP_ADMON_CATEGORIA.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTA");
            error = errorReturn.intValue();
            lista = (List<CategoriasPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("Algo ocurrió al obtener la informacion de la consulta a geografia");
            } else {
                return lista;
            }
        } catch (Exception e) {
            logger.info("insertaCategory - Algo ocurrio al insertar la categoría\n" + e.getMessage());
        }

        return lista;
    }

    @Override
    public String insertDocInCategory(int op, int idDoc, String descripcion, int idCategoria, String tipo, int activo, int orden) {

        Map<String, Object> out = null;
        String resp = "No se pudo insertar la informacion del documento";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_ITEM", idDoc)
                    .addValue("PA_DESCRIPCION", descripcion)
                    .addValue("PA_ID_CATEGORIA", idCategoria)
                    .addValue("PA_TIPO", tipo)
                    .addValue("PA_ACTIVO", activo)
                    .addValue("PA_ORDEN", orden);

            out = jdbcInsertDocInCategory.execute(in);
            logger.info("Función ejecutada: {GESTION.PA_ADMONMENUCAT.SP_ADMONMENUCAT.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTA");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertDocInCategory - Algo ocurrio al insertar la información del documento");
            } else {
                error = 0;
                resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"La informacion del documento se inserto correctamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertDocInCategory - Algo ocurrio al insertar la información del documento\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public String updateDocInCategory(int op, int idDoc, String descripcion, int idCategoria, String tipo, int activo, int orden) {

        Map<String, Object> out = null;
        String resp = "Ocurrio un error al actualizar la informacion del documento";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_ITEM", idDoc)
                    .addValue("PA_DESCRIPCION", descripcion)
                    .addValue("PA_ID_CATEGORIA", idCategoria)
                    .addValue("PA_TIPO", tipo)
                    .addValue("PA_ACTIVO", activo)
                    .addValue("PA_ORDEN", orden);

            out = jdbcUpdateDocInCategory.execute(in);
            logger.info("Función ejecutada: {GESTION.PA_ADMONMENUCAT.SP_ADMONMENUCAT.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTA");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateDocInCategory - Algo ocurrio al actualizar la información del documento");
            } else {
                error = 0;
                resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"Se actualizo correctamente la informacion del documento\"\n}";
                return resp;
            }
        } catch (Exception e) {
            // logger.info("updateDocInCategory - Algo ocurrio al actualizar la información del documento\n" + e.getMessage());
            logger.info("updateDocInCategory - Algo ocurrio al actualizar la información del documento");
        }

        resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":" + resp + "\n}";
        return resp;
    }

    @Override
    public List<DocInCategoriaPDDTO> getDocInCategory(int op, int idDoc, int idCategoria) {

        Map<String, Object> out = null;
        List<DocInCategoriaPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_ITEM", idDoc)
                    .addValue("PA_DESCRIPCION", null)
                    .addValue("PA_ID_CATEGORIA", idCategoria)
                    .addValue("PA_TIPO", null)
                    .addValue("PA_ACTIVO", null)
                    .addValue("PA_ORDEN", null);

            out = jdbcSelectDocInCategory.execute(in);
            logger.info("Función ejecutada: {GESTION.PA_ADMONMENUCAT.SP_ADMONMENUCAT.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTA");
            error = errorReturn.intValue();
            resp = (List<DocInCategoriaPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("selectDocInCategory - Algo ocurrio al obtener la información del documento");
            } else {
                return resp;
            }
        } catch (Exception e) {
            logger.info("selectDocInCategory - Algo ocurrio al obtener la información del documento\n" + e.getMessage());
        }

        return null;
    }

    @Override
    public String insertFileInfo(int op, int idArchivo, int idItem, String ruta, String nombre, int activo) {

        Map<String, Object> out = null;
        String resp = "Error al insertar la informacion";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_ARCHIVO", idArchivo)
                    .addValue("PA_ID_ITEM", idItem)
                    .addValue("PA_RUTA_ARCHIVO", ruta)
                    .addValue("PA_NOMBRE", nombre)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcInsertFileData.execute(in);
            logger.info("Funcion ejecutada: {GESTION.PA_ADMON_ARCHIVO.SP_ADMON_ARCHIVO.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTA");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertFileInfo - Algo ocurrio al insertar la información del archivo");
            } else {
                error = 0;
                resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"Se inserto la informacion correctamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertFileInfo - Algo ocurrio al insertar la información del archivo\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public String updateFileInfo(int op, int idArchivo, int idItem, String ruta, String nombre, int activo) {

        Map<String, Object> out = null;
        String resp = "No se pudo actualizar la informacion";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_ARCHIVO", idArchivo)
                    .addValue("PA_ID_ITEM", idItem)
                    .addValue("PA_RUTA_ARCHIVO", ruta)
                    .addValue("PA_NOMBRE", nombre)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcUpdateFileData.execute(in);
            logger.info("Función ejecutada: {GESTION.PA_ADMON_ARCHIVO.SP_ADMON_ARCHIVO.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTA");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateFileInfo - Algo ocurrio al actualizar la información del archivo");
            } else {
                resp = "{\n\t\"respuesta\":" + error + ",\n\t\"msj\":\"Se actualizo la informacion exitosamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("updateFileInfo - Algo ocurrio al actualizar la información del archivo\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":" + error + ",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public List<FileDataPDDTO> getFileInfo(int op, int idArchivo) {

        Map<String, Object> out = null;
        List<FileDataPDDTO> lista = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_ARCHIVO", idArchivo)
                    .addValue("PA_ID_ITEM", null)
                    .addValue("PA_RUTA_ARCHIVO", null)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcSelectFileData.execute(in);
            logger.info("Función ejecutada: {GESTION.PA_ADMON_ARCHIVO.SP_ADMON_ARCHIVO.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTA");
            error = errorReturn.intValue();
            lista = (List<FileDataPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("getFileInfo - Algo ocurrio al obtener la información del archivo");
            } else {
                return lista;
            }
        } catch (Exception e) {
            logger.info("getFileInfo - Algo ocurrio al obtener la información del archivo\n" + e.getMessage());
        }

        return lista;
    }

    @Override
    public String insertTablet(int op, int idTableta, String numSerie, String idCeco, int idUsuario, int idEstatus, String fechaInstalacion, int activo) {

        Map<String, Object> out = null;
        String resp = "Error al insertar la informacion";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TABLETA", idTableta)
                    .addValue("PA_NUM_SERIE", numSerie)
                    .addValue("PA_ID_CECO", idCeco)
                    .addValue("PA_ID_USUARIO", idUsuario)
                    .addValue("PA_ID_ESTATUS", idEstatus)
                    .addValue("PA_FEC_INTALA", fechaInstalacion)
                    .addValue("PA_ACTIVO", activo);

            out = jbdcInsertTablet.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_TABLETA.SP_ADMON_TABLET.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertTablet - Algo ocurrio al insertar la información de la tableta");
            } else {
                error = 0;
                resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"Se inserto la informacion correctamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertTablet - Algo ocurrio al insertar la información de la tableta\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public String updateTablet(int op, int idTableta, String numSerie, String idCeco, int idUsuario, int idEstatus, String fechaInstalacion, int activo) {

        Map<String, Object> out = null;
        String resp = "Error al actualizar la informacion";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TABLETA", idTableta)
                    .addValue("PA_NUM_SERIE", numSerie)
                    .addValue("PA_ID_CECO", idCeco)
                    .addValue("PA_ID_USUARIO", idUsuario)
                    .addValue("PA_ID_ESTATUS", idEstatus)
                    .addValue("PA_FEC_INTALA", fechaInstalacion)
                    .addValue("PA_ACTIVO", activo);

            out = jbdcUpdateTablet.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_TABLETA.SP_ADMON_TABLET.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateTablet - Algo ocurrio al actualizar la información de la tableta");
            } else {
                error = 0;
                resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"Se actualizo la informacion correctamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("updateTablet - Algo ocurrio al actualizar la información de la tableta\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public List<TabletDataPDDTO> getTablet(int op, int idTableta, String numSerie) {

        Map<String, Object> out = null;
        List<TabletDataPDDTO> lista = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TABLETA", idTableta)
                    .addValue("PA_NUM_SERIE", numSerie)
                    .addValue("PA_ID_CECO", null)
                    .addValue("PA_ID_USUARIO", null)
                    .addValue("PA_ID_ESTATUS", null)
                    .addValue("PA_FEC_INTALA", null)
                    .addValue("PA_ACTIVO", null);

            out = jbdcGetTablet.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_TABLETA.SP_ADMON_TABLET.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            lista = (List<TabletDataPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("getTablet - Algo ocurrio al obtener la información de la tableta");
            } else {
                return lista;
            }
        } catch (Exception e) {
            logger.info("getTablet - Algo ocurrio al obtener la información de la tableta\n" + e.getMessage());
        }

        return lista;
    }

    @Override
    public String insertFolio(int op, int idFolio, String tipo, String fecha, int activo) {

        Map<String, Object> out = null;
        String resp = "Error al insertar el folio";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FOLIO", idFolio)
                    .addValue("PA_TIPO", tipo)
                    .addValue("PA_FECHA", fecha)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcInsertFolio.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_FOLIO.SP_ADMON_DOCTO.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertFolio - Algo ocurrio al insertar el folio");
            } else {
                List<ErrorPDDTO> cursorReturn = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                resp = "0-.-.-" + cursorReturn.get(0).getMsj();
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertFolio - Algo ocurrio al insertar el folio\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public String updateFolio(int op, int idFolio, String tipo, String fecha, int activo) {

        Map<String, Object> out = null;
        String resp = "Error al insertar el folio";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FOLIO", idFolio)
                    .addValue("PA_TIPO", tipo)
                    .addValue("PA_FECHA", fecha)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcUpdateFolio.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_FOLIO.SP_ADMON_DOCTO.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertFolio - Algo ocurrio al insertar el folio");
            } else {
                error = 0;
                resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"Se inserto el folio correctamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertFolio - Algo ocurrio al insertar el folio\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public List<FolioDataPDDTO> getFolio(int op, int idFolio) {

        Map<String, Object> out = null;
        List<FolioDataPDDTO> lista = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FOLIO", idFolio)
                    .addValue("PA_TIPO", null)
                    .addValue("PA_FECHA", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetFolio.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_FOLIO.SP_ADMON_DOCTO.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            lista = (List<FolioDataPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("getFolio - Algo ocurrio al obtener el folio");
            } else {
                return lista;
            }
        } catch (Exception e) {
            logger.info("getFolio - Algo ocurrio al obtener el folio\n" + e.getMessage());
        }

        return null;
    }

    @Override
    public String insertDocto(int op, int idDocumento, int idFolio, String nombreArchivo, int idTipoDocto, int idUsuario, long pesoBytes, String origen, String vigenciaIni, String vigenciaFin, String visibleSuc, int idEstatus, int activo) {

        Map<String, Object> out = null;
        String resp = "Error al insertar el documento";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_DOCTO", idDocumento)
                    .addValue("PA_FOLIO", idFolio)
                    .addValue("PA_NOMBRE", nombreArchivo)
                    .addValue("PA_TIPO", idTipoDocto)
                    .addValue("PA_USUARIO", idUsuario)
                    .addValue("PA_PESO", pesoBytes)
                    .addValue("PA_ORIGEN", origen)
                    .addValue("PA_VIG_INI", vigenciaIni)
                    .addValue("PA_VIG_FIN", vigenciaFin)
                    .addValue("PA_VISIBLE_SUC", visibleSuc)
                    .addValue("PA_ESTATUS", idEstatus)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcInsertDocto.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_DOCTO.SP_ADMON_DOCTO.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertDocto - Algo ocurrio al insertar el documento");
            } else {
                List<ErrorPDDTO> cursorReturn = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                resp = "0-.-.-" + cursorReturn.get(0).getMsj();
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertFolio - Algo ocurrio al insertar el documento\n" + e.getMessage());
        }

        resp = "1-.-.-0";
        return resp;
    }

    @Override
    public String updateDocto(int op, int idDocumento, int idFolio, String nombreArchivo, int idTipoDocto, int idUsuario, long pesoBytes, String origen, String vigenciaIni, String vigenciaFin, String visibleSuc, int idEstatus, int activo) {

        Map<String, Object> out = null;
        String resp = "Error al actualizar el documento";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_DOCTO", idDocumento)
                    .addValue("PA_FOLIO", idFolio)
                    .addValue("PA_NOMBRE", nombreArchivo)
                    .addValue("PA_TIPO", idTipoDocto)
                    .addValue("PA_USUARIO", idUsuario)
                    .addValue("PA_PESO", pesoBytes)
                    .addValue("PA_ORIGEN", origen)
                    .addValue("PA_VIG_INI", vigenciaIni)
                    .addValue("PA_VIG_FIN", vigenciaFin)
                    .addValue("PA_VISIBLE_SUC", visibleSuc)
                    .addValue("PA_ESTATUS", idEstatus)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcUpdateDocto.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_DOCTO.SP_ADMON_DOCTO.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertDocto - Algo ocurrio al actualizar el documento");
            } else {
                error = 0;
                resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + "Documento actualizado" + "\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertFolio - Algo ocurrio al actualizar el documento\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public String updateOldDocument(int idDocumento, String vigenciaFin, String visibleSuc) {

        Map<String, Object> out = null;
        String resp = "Error al actualizar el documento";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", 1)
                    .addValue("PA_ID_DOCTO", idDocumento)
                    .addValue("PA_FOLIO", null)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_TIPO", null)
                    .addValue("PA_USUARIO", null)
                    .addValue("PA_PESO", null)
                    .addValue("PA_ORIGEN", null)
                    .addValue("PA_VIG_INI", null)
                    .addValue("PA_VIG_FIN", vigenciaFin)
                    .addValue("PA_VISIBLE_SUC", visibleSuc)
                    .addValue("PA_ESTATUS", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcUpdateDocto.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_DOCTO.SP_ADMON_DOCTO.UPDATE_OLD_DOCTO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateOldDocument - Algo ocurrio al actualizar el documento");
            } else {
                error = 0;
                resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + "Documento actualizado" + "\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("updateOldDocument - Algo ocurrio al actualizar el documento\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public List<DoctoDataPDDTO> getDocto(int op, int idDocumento) {

        Map<String, Object> out = null;
        List<DoctoDataPDDTO> lista = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_DOCTO", idDocumento)
                    .addValue("PA_FOLIO", null)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_TIPO", null)
                    .addValue("PA_USUARIO", null)
                    .addValue("PA_PESO", null)
                    .addValue("PA_ORIGEN", null)
                    .addValue("PA_VIG_INI", null)
                    .addValue("PA_VIG_FIN", null)
                    .addValue("PA_VISIBLE_SUC", null)
                    .addValue("PA_ESTATUS", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetDocto.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_DOCTO.SP_ADMON_DOCTO.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertDocto - Algo ocurrio al actualizar el documento");
            } else {
                lista = (List<DoctoDataPDDTO>) out.get("PA_CURSORCON");
                return lista;
            }
        } catch (Exception e) {
            logger.info("insertFolio - Algo ocurrio al actualizar el documento\n" + e.getMessage());
        }

        return null;
    }

    @Override
    public List<ListaCategoriasPDDTO> obtieneDocumentos(int op, int nivel, String dependeDe) {

        Map<String, Object> out = null;
        List<ListaCategoriasPDDTO> lista = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TIPODOC", null)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_NIVEL", nivel)
                    .addValue("PA_DEPENDE_DE", dependeDe)
                    .addValue("PA_ACTIVO", null);

            out = jdbcgetListaDocumentos.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_CONSULTIPODOC.SP_CONSUL_TIPODOC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            lista = (List<ListaCategoriasPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("obtieneDocumentos - Algo ocurrió al obtener la informacion de la consulta a geografia");
            } else {
                return lista;
            }
        } catch (Exception e) {
            logger.info("obtieneDocumentos - Algo ocurrio al obtener la consulta geolocalizacion\n" + e.getMessage());
        }

        return lista;
    }

    /**
     * *****************************************************************************
     */
    /**
     * *************** SERVICIOS MODULO DE ADMINISTRACION DE ENVIOS
     * ****************
     */
    /**
     * *****************************************************************************
     */
    @Override
    public List<FolioFilterDataPDDTO> getFolioDataPorFolio(int op, int idFolio) {

        Map<String, Object> out = null;
        List<FolioFilterDataPDDTO> lista = new ArrayList<FolioFilterDataPDDTO>();
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FOLIO", idFolio);

            out = jdbcGetFolioData.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_CONSUL_FOLIO.SP_CONSUL_FOLIO.GET_FOLIO_DATA}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getFolioDataPorFolio - Algo ocurrio al obtener la información del folio");
            } else {
                lista = (List<FolioFilterDataPDDTO>) out.get("PA_CURSORCON");
                return lista;
            }
        } catch (Exception e) {
            logger.info("getFolioDataPorFolio - Algo ocurrio al obtener la información del folio\n" + e.getMessage());
        }

        return lista;
    }

    @Override
    public List<FolioFilterDataPDDTO> getFolioDataPorTerritorio(int op, String ceco, String categoria, String fecha, String estatus, String tipo) {

        Map<String, Object> out = null;
        List<FolioFilterDataPDDTO> lista = new ArrayList<FolioFilterDataPDDTO>();
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_CECO", ceco)
                    .addValue("PA_CATEGORIA", categoria)
                    .addValue("PA_FECHA", fecha) // ddMMyyyy
                    .addValue("PA_ESTATUS", estatus)
                    .addValue("PA_TIPO", tipo);

            out = jdbcGetFolioPorTerritorio.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_CONSUL_TERRI.SP_CONSUL_TERRI.GET_FOLIO_DATA}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getFolioDataPorTerritorio - Algo ocurrio al obtener la información del folio");
                // lista = out.get("PA_CURSORCON") != null ? (List<FolioFilterDataPDDTO>) out.get("PA_CURSORCON") : null;
            } else {
                lista = (List<FolioFilterDataPDDTO>) out.get("PA_CURSORCON");
                return lista;
            }
        } catch (Exception e) {
            logger.info("getFolioDataPorTerritorio - Algo ocurrio al obtener la información del folio\n" + e.getMessage());
        }

        return lista;
    }

    @Override
    public List<FolioFilterDataPDDTO> getFolioDataPorGeografia(int op, String geo, int nivel, String categoria, String fecha, String estatus, String tipo) {

        Map<String, Object> out = null;
        List<FolioFilterDataPDDTO> lista = new ArrayList<FolioFilterDataPDDTO>();
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_GEO", geo)
                    .addValue("PA_NIVEL", nivel)
                    .addValue("PA_CATEGORIA", categoria)
                    .addValue("PA_FECHA", fecha) // ddmmyyyy
                    .addValue("PA_ESTATUS", estatus)
                    .addValue("PA_TIPO", tipo);

            out = jdbcGetFolioPorGeografia.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_CONSUL_PAIS.SP_CONSUL_PAIS.GET_FOLIO_DATA}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getFolioDataPorGeografia - Algo ocurrio al obtener la información del folio");
            } else {
                lista = (List<FolioFilterDataPDDTO>) out.get("PA_CURSORCON");
                return lista;
            }
        } catch (Exception e) {
            logger.info("getFolioDataPorGeografia - Algo ocurrio al obtener la información del folio\n" + e.getMessage());
        }

        return lista;
    }

    @Override
    public List<String> convertEconNumToCeco(int op, String numEco) {

        Map<String, Object> out = null;
        List<String> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_CECO", numEco)
                    .addValue("PA_CATEGORIA", null)
                    .addValue("PA_FECHA", null) // ddmmyyyy
                    .addValue("PA_ESTATUS", null)
                    .addValue("PA_TIPO", null);

            out = jdbcGetCecoPorNumEco.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_CONSUL_TERRI.SP_CONSUL_TERRI.GET_CECO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("convertEconNumToCeco - Algo ocurrio al obtener el ceco");
                resp = new ArrayList<String>();
            } else {
                resp = (List<String>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("convertEconNumToCeco - Algo ocurrio al obtener el ceco\n" + e.getMessage());
            resp = new ArrayList<String>();
        }

        return resp;
    }

    @Override
    public String getLastestDoc(int op, int idDocto) {

        Map<String, Object> out = null;
        String resp = "";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_DOCTO", null)
                    .addValue("PA_FOLIO", null)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_TIPO", idDocto)
                    .addValue("PA_USUARIO", null)
                    .addValue("PA_PESO", null)
                    .addValue("PA_ORIGEN", null)
                    .addValue("PA_VIG_INI", null)
                    .addValue("PA_VIG_FIN", null)
                    .addValue("PA_VISIBLE_SUC", null)
                    .addValue("PA_ESTATUS", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcInsertDocto.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_DOCTO.SP_ADMON_DOCTO.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getLastestDoc - Algo ocurrio al obtener el id del documento");
            } else {
                List<ErrorPDDTO> cursorReturn = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                resp = "0-.-.-" + cursorReturn.get(0).getMsj();
                return resp;
            }
        } catch (Exception e) {
            logger.info("getLastestDoc - Algo ocurrio al obtener el id del documento\n" + e.getMessage());
        }

        resp = "1-.-.-0";
        return resp;
    }

    /**
     * *********************************************************************
     */
    /**
     * *********** SERVICIOS PARA EL FLUJO DE CARGA DE ARCHIVOS ************
     */
    /**
     * *********************************************************************
     */
    @Override
    public String insertAdmonTipoDoc(int op, int idTipoDoc, String nombre, int nivel, int dependeDe, int activo) {

        Map<String, Object> out = null;
        String resp = "";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TIPODOC", idTipoDoc)
                    .addValue("PA_NOMBRE", nombre)
                    .addValue("PA_NIVEL", nivel)
                    .addValue("PA_DEPENDE_DE", dependeDe)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcInsertAdmonTipoDoc.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMONTIPODOC.SP_ADMON_TIPODOC.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertAdmonTipoDoc - Algo ocurrio al insertar el documento en el catalogo");
            } else {
                List<ErrorPDDTO> cursorReturn = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                resp = "0-.-.-" + cursorReturn.get(0).getMsj();
                return resp;
            }
        } catch (Exception e) {
            logger.info("insertAdmonTipoDoc - Algo ocurrio al insertar el documento en el catalogo\n" + e.getMessage());
        }

        return "1-.-.-0";
    }

    @Override
    public String updateAdmonTipoDoc(int op, int idTipoDoc, String nombre, int nivel, int dependeDe, int activo) {

        Map<String, Object> out = null;
        String resp = "";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TIPODOC", idTipoDoc)
                    .addValue("PA_NOMBRE", nombre)
                    .addValue("PA_NIVEL", nivel)
                    .addValue("PA_DEPENDE_DE", dependeDe)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcUpdateAdmonTipoDoc.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMONTIPODOC.SP_ADMON_TIPODOC.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateAdmonTipoDoc - Algo ocurrio al actualizar la informacion del documento en el catalogo");
            } else {
                List<ErrorPDDTO> cursorReturn = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                resp = "0-.-.-" + cursorReturn.get(0).getMsj();
                return resp;
            }
        } catch (Exception e) {
            logger.info("updateAdmonTipoDoc - Algo ocurrio al actualizar la informacion del documento en el catalogo\n" + e.getMessage());
        }

        return "1-.-.-0";
    }

    @Override
    public List<DocInCategoryPDDTO> getAdmonTipoDoc(int op, int idTipoDoc) {

        Map<String, Object> out = null;
        List<DocInCategoryPDDTO> resp = new ArrayList<DocInCategoryPDDTO>();
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TIPODOC", idTipoDoc)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_NIVEL", null)
                    .addValue("PA_DEPENDE_DE", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcSelectAdmonTipoDoc.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMONTIPODOC.SP_ADMON_TIPODOC.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getAdmonTipoDoc - Algo ocurrio al obtener la informacion del documento en el catalogo");
            } else {
                List<DocInCategoryPDDTO> cursorReturn = (List<DocInCategoryPDDTO>) out.get("PA_CURSORCON");
                return cursorReturn;
            }
        } catch (Exception e) {
            logger.info("getAdmonTipoDoc - Algo ocurrio al obtener la informacion del documento en el catalogo\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public String updateTabletCriticStatus(int op, String numSerie, int idEstatus) {

        Map<String, Object> out = null;
        String resp = "Error al actualizar la informacion";
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TABLETA", null)
                    .addValue("PA_NUM_SERIE", numSerie)
                    .addValue("PA_ID_CECO", null)
                    .addValue("PA_ID_USUARIO", null)
                    .addValue("PA_ID_ESTATUS", idEstatus)
                    .addValue("PA_FEC_INTALA", null)
                    .addValue("PA_ACTIVO", 1);

            out = jbdcUpdateTablet.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_TABLETA.SP_ADMON_TABLET.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateTablet - Algo ocurrio al actualizar la información de la tableta");
            } else {
                error = 0;
                resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"Se actualizo la informacion correctamente\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("updateTablet - Algo ocurrio al actualizar la información de la tableta\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + resp + "\"\n}";
        return resp;
    }

    @Override
    public List<TabletDataForExcelPDDTO> getAllTabletsForExcel(int op) {

        Map<String, Object> out = null;
        List<TabletDataForExcelPDDTO> lista = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TABLETA", null)
                    .addValue("PA_NUM_SERIE", null)
                    .addValue("PA_ID_CECO", null)
                    .addValue("PA_ID_USUARIO", null)
                    .addValue("PA_ID_ESTATUS", null)
                    .addValue("PA_FEC_INTALA", null)
                    .addValue("PA_ACTIVO", null);

            out = jbdcGetAllTabletsForExcel.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_TABLETA.SP_ADMON_TABLET.SELECT_OP3}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            lista = (List<TabletDataForExcelPDDTO>) out.get("PA_CURSORCON");

            if (error == 1) {
                logger.info("getTablet - Algo ocurrio al obtener la información de la tableta");
            } else {
                return lista;
            }
        } catch (Exception e) {
            logger.info("getTablet - Algo ocurrio al obtener la información de la tableta\n" + e.getMessage());
        }

        return lista;
    }

    @Override
    public String insertUpdateFileChecksum(Integer op, Integer idEncArch, Integer idRef, String origen, String checksum, Integer activo) {
        Map<String, Object> out = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op.intValue())
                    .addValue("PA_ENCR_ARCH", idEncArch != null ? idEncArch.intValue() : null)
                    .addValue("PA_REFERENCIA", idRef != null ? idRef.intValue() : null)
                    .addValue("PA_ORIGEN", origen != null ? origen : null)
                    .addValue("PA_HASH", checksum != null ? checksum : null)
                    .addValue("PA_ACTIVO", activo != null ? activo.intValue() : null);

            out = jdbcInsertUpdateFileChecksum.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_ENCARC.SP_ADMON_ENCARC}");

            int error = 0;
            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrio al insertar o actualizar la visita  de interés");
                return "{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"Algo ocurrio al insertar o actualizar la informacion del puesto " + op.intValue() + "\"\n}";
            } else {
                List<ErrorPDDTO> pa_cursor = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                return "{\"respuesta\":\"0\",\"msj\":\"" + pa_cursor.get(0).getMsj() + "\"}";
            }
        } catch (Exception e) {
            logger.info("updateAdmonTipoDoc - Algo ocurrio al actualizar la informacion del documento en el catalogo\n" + e.getMessage());
        }

        return "1-.-.-0";
    }

    @Override
    public List<FileChecksumPDDTO> getFileChecksum(Integer op, Integer idEncArch, Integer idRef, String origen, String checksum, Integer activo) {
        Map<String, Object> out = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op != null ? op.intValue() : null)
                    .addValue("PA_ENCR_ARCH", idEncArch != null ? idEncArch.intValue() : null)
                    .addValue("PA_REFERENCIA", idRef != null ? idRef.intValue() : null)
                    .addValue("PA_ORIGEN", origen != null ? origen : null)
                    .addValue("PA_HASH", checksum != null ? checksum : null)
                    .addValue("PA_ACTIVO", activo != null ? activo.intValue() : null);

            out = jdbcGetFileChecksum.execute(in);
            logger.info("Funcion ejecutada:{PEDESTALDIG.PA_ADMON_ENCARC.SP_SELE_ENCARC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            int error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrio al obtener el checksum del archivo");
                return new ArrayList<FileChecksumPDDTO>();
            } else {
                Object cursor = out.get("PA_CURSORCON");
                List<FileChecksumPDDTO> user = (List<FileChecksumPDDTO>) cursor;
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Algo ocurrio al obtener el checksum del archivo " + e.getMessage());
            return new ArrayList<FileChecksumPDDTO>();
        }
    }

    @Override
    public List<FileWithoutChecksumPDDTO> getFileChecksum(Integer op) {
        Map<String, Object> out = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op != null ? op.intValue() : null);

            out = jdbcGetFilesWithoutChecksum.execute(in);
            logger.info("Funcion ejecutada:{REVISTADIG.PA_ADMON_ENCARC.SP_SELE_DOCENC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            int error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrio al obtener el checksum del archivo");
                return new ArrayList<FileWithoutChecksumPDDTO>();
            } else {
                List<FileWithoutChecksumPDDTO> user = (List<FileWithoutChecksumPDDTO>) out.get("PA_CURSORCON");
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Algo ocurrio al obtener el checksum del archivo " + e.getMessage());
            return new ArrayList<FileWithoutChecksumPDDTO>();
        }
    }

    @Override
    public String truncateTPasoCeco() {
        Map<String, Object> out = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource();

            out = jdbcTruncateTPasoCeco.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_TPCECO.SP_TRUCA_TPCECO}");

            int error = 0;
            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrio al truncar la tabla de PDTP_PASO_CECO");
                return "{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"Algo ocurrio al truncar la tabla de PDTP_PASO_CECO\"\n}";
            } else {
                List<ErrorPDDTO> pa_cursor = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                return "{\"respuesta\":\"0\",\"msj\":\"" + pa_cursor.get(0).getMsj() + "\"}";
            }
        } catch (Exception e) {
            logger.info("updateAdmonTipoDoc - Algo ocurrio al truncar la tabla de PDTP_PASO_CECO\n" + e.getMessage());
        }

        return "1-.-.-0";
    }

    @Override
    public String insertTPasoCeco(CecosWSPDDTO ceco) {
        Map<String, Object> out = null;

        try {
            if (ceco == null) {
                logger.info("{\"respuesta\":\"1\",\"msj\":\"Algo ocurrio al insertar el ceco\"}");
                return "{\"respuesta\":\"1\",\"msj\":\"Algo ocurrio al insertar el ceco\"}";
            }

            String formatDateTime = null;

            try {
                if (ceco.getFechaCreacion() != null) {
                    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.ENGLISH);
                    Date date = format.parse(ceco.getFechaCreacion());
                    format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
                    formatDateTime = format.format(date);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_FCCCID", ceco.getIdCC() != null ? ceco.getIdCC() : null)
                    .addValue("PA_FIENTIDADID", ceco.getIdEntidad() != null ? ceco.getIdEntidad() : null)
                    .addValue("PA_FINUM_ECONO", ceco.getNumEconomico() != null ? ceco.getNumEconomico() : null)
                    .addValue("PA_FCNOMBRECC", ceco.getNomCC() != null ? ceco.getNomCC() : null)
                    .addValue("PA_FCNOMBRE_ENT", ceco.getNomEntidad() != null ? ceco.getNomEntidad() : null)
                    .addValue("PA_FCCCID_PADRE", ceco.getIdCCPa() != null ? ceco.getIdCCPa() : null)
                    .addValue("PA_FIENTDID_PAD", ceco.getIdEntidadPa() != null ? ceco.getIdEntidadPa() : null)
                    .addValue("PA_FCNOMBRECC_PAD", ceco.getNomCCPa() != null ? ceco.getNomCCPa() : null)
                    .addValue("PA_FCTIPOCANAL", ceco.getTipoCanal() != null ? ceco.getTipoCanal() : null)
                    .addValue("PA_FCNOMBTIPCANAL", null)
                    .addValue("PA_FCCANAL", ceco.getNomCanal() != null ? ceco.getNomCanal() : null)
                    .addValue("PA_FISTATUSCCID", ceco.getIdEstatus() != null ? ceco.getIdEstatus() : null)
                    .addValue("PA_FCSTATUSCC", ceco.getNomEstatus() != null ? ceco.getNomEstatus() : null)
                    .addValue("PA_FITIPOCC", ceco.getIdTipoCentro() != null ? ceco.getIdTipoCentro() : null)
                    .addValue("PA_FIMUNICIPIOID", ceco.getIdMunicipio() != null ? ceco.getIdMunicipio() : null)
                    .addValue("PA_FIESTADOID", ceco.getIdEstado() != null ? ceco.getIdEstado() : 0)
                    .addValue("PA_FCMUNICIPIO", ceco.getNomMunicipio() != null ? ceco.getNomMunicipio() : null)
                    .addValue("PA_FCTIPOOPERA", ceco.getNomTipoOperacion() != null ? ceco.getNomTipoOperacion() : null)
                    .addValue("PA_FCTIPOSUCUR", ceco.getIdTipoSucursal() != null ? ceco.getIdTipoSucursal() : null)
                    .addValue("PA_FCNOMTIPOSUC", ceco.getNomTipoSucursal() != null ? ceco.getNomTipoSucursal() : null)
                    .addValue("PA_FCCALLE", ceco.getNomCalle() != null ? ceco.getNomCalle() : null)
                    .addValue("PA_FIRESPONSABLE", ceco.getIdResponsable() != null ? ceco.getIdResponsable() : null)
                    .addValue("PA_FCRESPONSABLE", ceco.getNomResponsable() != null ? ceco.getNomResponsable() : null)
                    .addValue("PA_FCCP", ceco.getCodigoPostal() != null ? ceco.getCodigoPostal() : null)
                    .addValue("PA_FCTELEFONOS", ceco.getTelefono() != null ? ceco.getTelefono() : null)
                    .addValue("PA_FICLASIF_SIEID", ceco.getIdSubnegocio() != null ? ceco.getIdSubnegocio() : null)
                    .addValue("PA_FCCLASIF_SIE", ceco.getDesSubnegocio() != null ? ceco.getDesSubnegocio() : null)
                    .addValue("PA_FIGASTOXNEGID", null)
                    .addValue("PA_FCGASTOXNEGOCIO", null)
                    .addValue("PA_FDAPERTURA", ceco.getFechaApertura() != null ? ceco.getFechaApertura().substring(0, 10) : null)
                    .addValue("PA_FDCIERRE", ceco.getFechaCierre() != null ? ceco.getFechaCierre().substring(0, 10) : null)
                    .addValue("PA_FIPAISID", ceco.getIdPais() != null ? ceco.getIdPais() : null)
                    .addValue("PA_FCPAIS", ceco.getNomPais() != null ? ceco.getNomPais() : null)
                    .addValue("PA_FDCARGA", formatDateTime != null ? formatDateTime : null)
                    .addValue("PA_FCESTADO", ceco.getNomEstado() != null ? ceco.getNomEstado() : "-")
                    .addValue("PA_FCCOLONIA", ceco.getNomColonia() != null ? ceco.getNomColonia() : null)
                    .addValue("PA_FCNU_EXTER", ceco.getNumExterior() != null ? ceco.getNumExterior() : null)
                    .addValue("PA_FCNUM_INTER", ceco.getNumInterior() != null ? ceco.getNumInterior() : null);

            out = jdbcInsertUpdateTPasoCeco.execute(in);
            // logger.info("Funcion ejecutada:{REVISTADIG.PA_ADM_PASOCECO.SP_INSERT_TPCECO}");

            int error = 0;
            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                List<ErrorPDDTO> pa_cursor = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                logger.info("{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"" + pa_cursor.get(0).getMsj() + "\"\n}");
                return "{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"" + pa_cursor.get(0).getMsj() + "\"\n}";
            } else {
                List<ErrorPDDTO> pa_cursor = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                return "{\"respuesta\":\"0\",\"msj\":\"" + pa_cursor.get(0).getMsj() + "\"}";
            }
        } catch (Exception e) {
            logger.info("Algo ocurrio al insertar el ceco...");
            e.printStackTrace();
            return "{\"respuesta\":\"1\",\"msj\":\"Algo ocurrio al insertar el ceco\"}";
        }
    }

    @Override
    public List<ParamCecoPDDTO> getCecoParameters() {
        Map<String, Object> out = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource();

            out = jdbcGetCecoParamters.execute(in);
            logger.info("Funcion ejecutada:{REVISTADIG.PA_ACTUALIZACECO.SP_CECO_INICIO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            int error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrio al obtener los parametros de cecos");
                return new ArrayList<ParamCecoPDDTO>();
            } else {
                List<ParamCecoPDDTO> user = (List<ParamCecoPDDTO>) out.get("PA_CURSORCON");
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Algo ocurrio al obtener los parametros de cecos " + e.getMessage());
            return new ArrayList<ParamCecoPDDTO>();
        }
    }

    @Override
    public String updateCecosByParam(Integer op, String negocio, Integer idCeco) {
        Map<String, Object> out = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op != null ? op.intValue() : null)
                    .addValue("PA_ID_CECO", idCeco != null ? idCeco.intValue() : null)
                    .addValue("PA_NEGOCIO", negocio != null ? negocio : null);

            out = jdbcUpdateCecosByParam.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ACTUALIZACECO.SP_ACTUALIZACECO}");

            int error = 0;
            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrio al actualizar los cecos");
                return "{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"Algo ocurrio al actualizar los cecos\"\n}";
            } else {
                List<ErrorPDDTO> pa_cursor = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                return "{\"respuesta\":\"0\",\"msj\":\"" + pa_cursor.get(0).getMsj() + "\"}";
            }
        } catch (Exception e) {
            logger.info("Algo ocurrio al actualizar los cecos\n" + e.getMessage());
        }

        return "1-.-.-0";
    }

    @Override
    public List<DocCorruptosPDDTO> getDocCorruptos(Integer op, String idCeco, Integer folio, Integer idDocto, Integer fecha) {
        Map<String, Object> out = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op != null ? op.intValue() : null)
                    .addValue("PA_CECO", idCeco != null ? idCeco : null)
                    .addValue("PA_FOLIO", folio != null ? folio.intValue() : null)
                    .addValue("PA_ID_DOCTO", idDocto != null ? idDocto.intValue() : null)
                    .addValue("PA_FECHA", fecha != null ? fecha.intValue() : null);

            out = jdbcGetDocCorruptos.execute(in);
            logger.info("Funcion ejecutada:{REVISTADIG.PA_DOCTO_SUC.SP_CONS_DOC_SUC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            int error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrio al obtener los parametros de cecos");
                return new ArrayList<DocCorruptosPDDTO>();
            } else {
                List<DocCorruptosPDDTO> user = (List<DocCorruptosPDDTO>) out.get("PA_CURSORCON");
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Algo ocurrio al obtener los parametros de cecos " + e.getMessage());
            return new ArrayList<DocCorruptosPDDTO>();
        }
    }

    @Override
    public List<DocsBeforeUploadPDDTO> getDocsBeforeUpload(Integer op, Integer tipoDoc) {
        Map<String, Object> out = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op != null ? op.intValue() : null)
                    .addValue("PA_TIPO_DOC", tipoDoc != null ? tipoDoc.intValue() : null);

            out = jdbcGetDocsBeforeUpload.execute(in);
            logger.info("Funcion ejecutada:{REVISTADIG.PA_CONSUL_DOC.SP_CONSUL_DOC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            int error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrio al obtener los parametros de cecos");
                return new ArrayList<DocsBeforeUploadPDDTO>();
            } else {
                List<DocsBeforeUploadPDDTO> user = (List<DocsBeforeUploadPDDTO>) out.get("PA_CURSORCON");
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Algo ocurrio al obtener los parametros de cecos " + e.getMessage());
            return new ArrayList<DocsBeforeUploadPDDTO>();
        }
    }

    @Override
    public List<DocsBeforeUploadPDDTO> getExpiringDocs(int op) {
        Map<String, Object> out = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_TIPO_DOC", null);

            out = jdbcGetExpiringDocs.execute(in);
            logger.info("Funcion ejecutada:{REVISTADIG.PA_CONSUL_DOC.SP_CONSUL_DOC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            int error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrio al obtener los parametros de cecos");
                return new ArrayList<DocsBeforeUploadPDDTO>();
            } else {
                List<DocsBeforeUploadPDDTO> user = (List<DocsBeforeUploadPDDTO>) out.get("PA_CURSORCON");
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Algo ocurrio al obtener los parametros de cecos " + e.getMessage());
            return new ArrayList<DocsBeforeUploadPDDTO>();
        }
    }

    @Override
    public List<DocsBeforeUploadPDDTO> getHistoryOfDocs(int op) {
        Map<String, Object> out = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_TIPO_DOC", null);

            out = jdbcGetHistoryOfDocs.execute(in);
            logger.info("Funcion ejecutada:{REVISTADIG.PA_CONSUL_DOC.SP_CONSUL_DOC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            int error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrio al obtener los parametros de cecos");
                return new ArrayList<DocsBeforeUploadPDDTO>();
            } else {
                List<DocsBeforeUploadPDDTO> user = (List<DocsBeforeUploadPDDTO>) out.get("PA_CURSORCON");
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Algo ocurrio al obtener los parametros de cecos " + e.getMessage());
            return new ArrayList<DocsBeforeUploadPDDTO>();
        }
    }

    @Override
    public List<DocsBeforeUploadPDDTO> getAllFoliosData(int op) {
        Map<String, Object> out = null;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_TIPO_DOC", null);

            out = jdbcGetAllFoliosData.execute(in);
            logger.info("Funcion ejecutada:{REVISTADIG.PA_CONSUL_DOC.SP_CONSUL_DOC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            int error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrio al obtener los parametros de cecos");
                return new ArrayList<DocsBeforeUploadPDDTO>();
            } else {
                List<DocsBeforeUploadPDDTO> user = (List<DocsBeforeUploadPDDTO>) out.get("PA_CURSORCON");
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Algo ocurrio al obtener los parametros de cecos " + e.getMessage());
            return new ArrayList<DocsBeforeUploadPDDTO>();
        }
    }
}
