package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.FormArchiverosDTO;
import com.gruposalinas.franquicia.mappers.FormArchiverosRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class FormArchiverosDAOImpl extends DefaultDAO implements FormArchiverosDAO {

    private static Logger logger = LogManager.getLogger(FormArchiverosDAOImpl.class);

    DefaultJdbcCall jdbcObtieneFormArchiveros;
    DefaultJdbcCall jdbcInsertaFormArchiveros;
    DefaultJdbcCall jdbcActualizaFormArchiveros;
    DefaultJdbcCall jdbcEliminaFormArchiveros;
    DefaultJdbcCall jdbcObtieneFormArchiverosCeco;

    public void init() {

        jdbcObtieneFormArchiveros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ARCH")
                .withProcedureName("SPGETARCHIVERO")
                .returningResultSet("PA_CONSULTA", new FormArchiverosRowMapper());

        jdbcInsertaFormArchiveros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ARCH")
                .withProcedureName("SPINS_ARCHI");

        jdbcActualizaFormArchiveros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ARCH")
                .withProcedureName("SPACT_ARCHI");

        jdbcEliminaFormArchiveros = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ARCH")
                .withProcedureName("SPELIM_ARCHI");

        jdbcObtieneFormArchiverosCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADM_ARCH")
                .withProcedureName("SPGETARCHIVERO")
                .returningResultSet("PA_CONSULTA", new FormArchiverosRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<FormArchiverosDTO> obtieneFormArchiveros(int idFormato) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<FormArchiverosDTO> listaArch = null;
        String aux = null;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIARCHIVERO_ID", idFormato)
                .addValue("PA_FIID_USUARIO", aux)
                .addValue("PA_FCID_CECO", aux);

        out = jdbcObtieneFormArchiverosCeco.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAADM_ARCH.SP_SEL_FORMARCHIVEROSCECO");

        listaArch = (List<FormArchiverosDTO>) out.get("PA_CONSULTA");

        //BigDecimal resultado =(BigDecimal) out.get("PA_RESEJECUCION");
        //error = resultado.intValue();
        if (listaArch != null) {
            error = 1;
        }

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el formato de entrega de archiveros");
        } else {
            return listaArch;
        }

        return listaArch;
    }

    @Override
    public int insertaFormarchiveros(FormArchiverosDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        int archiv = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIARCHIVERO_ID", bean.getIdFromArchivero())
                .addValue("PA_FIID_USUARIO", bean.getIdUsuario())
                .addValue("PA_FCNOMBRE", bean.getNombre())
                .addValue("PA_FCID_CECO", Integer.parseInt(bean.getIdCeco()))
                .addValue("PA_FIID_USRECIBE", bean.getIdRecibe())
                .addValue("PA_FCNOMRECIBE", bean.getNomRecibe())
                .addValue("PA_FISTATUS_ID", bean.getIdStatus());

        out = jdbcInsertaFormArchiveros.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADM_ARCH.SPINS_ARCHI}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_RESEJECUCION");
        error = errorReturn.intValue();
        BigDecimal valArchReturn = (BigDecimal) out.get("PA_FIARCHIVERO");
        archiv = valArchReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el formato archiveros");
        } else {
            return archiv;
        }
        return archiv;
    }

    @Override
    public boolean modificaFormarchiveros(FormArchiverosDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIARCHIVERO_ID", bean.getIdFromArchivero())
                .addValue("PA_FIID_USUARIO", bean.getIdUsuario())
                .addValue("PA_FCNOMBRE", bean.getNombre())
                .addValue("PA_FCID_CECO", Integer.parseInt(bean.getIdCeco()))
                .addValue("PA_FIID_USRECIBE", bean.getIdRecibe())
                .addValue("PA_FCNOMRECIBE", bean.getNomRecibe())
                .addValue("PA_FISTATUS_ID", bean.getIdStatus());

        out = jdbcActualizaFormArchiveros.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADM_ARCH.SP_ACT_ARCHIVEROS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_RESEJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el formato de archiveros");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean eliminaFormarchiveros(int idFormato) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIARCHIVERO_ID", idFormato);

        out = jdbcEliminaFormArchiveros.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADM_ARCH.SP_DEL_FORMARCHIVEROS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_RESEJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al eliminar el Formato de Archiveros");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public List<FormArchiverosDTO> obtieneFormCeco(String idCeco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<FormArchiverosDTO> listaArch = null;
        String aux = null;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIARCHIVERO_ID", aux)
                .addValue("PA_FIID_USUARIO", aux)
                .addValue("PA_FCID_CECO", idCeco);

        out = jdbcObtieneFormArchiverosCeco.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAADM_ARCH.SP_SEL_FORMARCHIVEROSCECO");

        listaArch = (List<FormArchiverosDTO>) out.get("PA_CONSULTA");

        //BigDecimal resultado =(BigDecimal) out.get("PA_RESEJECUCION");
        //error = resultado.intValue();
        if (listaArch != null) {
            error = 1;
        }

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el formato de entrega de archiveros");
        } else {
            return listaArch;
        }

        return listaArch;
    }

}
