package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.BitacoraDTO;
import java.util.List;

public interface BitacoraDAO {

    public int insertaBitacora(BitacoraDTO bean) throws Exception;

    public boolean actualizaBitacora(BitacoraDTO bean) throws Exception;

    public boolean eliminaBitacora(int idBitacora) throws Exception;

    public List<BitacoraDTO> buscaBitacora() throws Exception;

    public List<BitacoraDTO> buscaBitacora(String checkUsua, String idBitacora, String fechaI, String fechaF) throws Exception;

    public int verificaIdBitacora(int checkUsua) throws Exception;

    public boolean cierraBitacora() throws Exception;

    public List<BitacoraDTO> buscaBitacoraCerradas(String idChecklist) throws Exception;

    public List<BitacoraDTO> buscaBitacoraCerradasR(String idChecklist) throws Exception;
}
