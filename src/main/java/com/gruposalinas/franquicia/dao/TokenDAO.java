package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.TokenDTO;
import java.util.List;

public interface TokenDAO {

    public int insertaToken(TokenDTO bean) throws Exception;

    public boolean actualizaToken(TokenDTO bean) throws Exception;

    public boolean eliminaToken(int idToken) throws Exception;

    public List<TokenDTO> buscaToken() throws Exception;

    public List<TokenDTO> buscaToken(String token) throws Exception;

    public List<TokenDTO> buscaTokenF(String fecha) throws Exception;

    public boolean eliminaTokenTodos() throws Exception;

}
