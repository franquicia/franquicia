package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.RecursoDTO;
import java.util.List;

public interface RecursoDAO {

    public List<RecursoDTO> buscaRecurso(String idRecurso) throws Exception;

    public int insertaRecurso(RecursoDTO bean) throws Exception;

    public boolean actualizaRecurso(RecursoDTO bean) throws Exception;

    public boolean eliminaRecurso(int idRecurso) throws Exception;
}
