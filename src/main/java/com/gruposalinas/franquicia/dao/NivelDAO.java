package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.NivelDTO;
import java.util.List;

public interface NivelDAO {

    public List<NivelDTO> obtieneNivel() throws Exception;

    public List<NivelDTO> obtieneNivel(int idNivel) throws Exception;

    public int insertaNivel(NivelDTO bean) throws Exception;

    public boolean actualizaNivel(NivelDTO bean) throws Exception;

    public boolean eliminaNivel(int idNivel) throws Exception;

}
