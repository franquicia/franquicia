package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ExpedientesDTO;
import com.gruposalinas.franquicia.mappers.ExpedienteRowMapper;
import com.gruposalinas.franquicia.mappers.ResponsableRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ExpedienteDAOImpl extends DefaultDAO implements ExpedienteDAO {

    private static Logger logger = LogManager.getLogger(ExpedienteDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaExpediente;
    private DefaultJdbcCall jdbcInsertaResponsable;
    private DefaultJdbcCall jdbcActualizaEstatus;
    private DefaultJdbcCall jdbcConsultaExpediente;
    private DefaultJdbcCall jdbcConsultaResponsable;
    private DefaultJdbcCall jdbcEliminaExpediente;
    private DefaultJdbcCall jdbcEliminaResponsable;
    private DefaultJdbcCall jdbcInsertaExpedienteTipoActa;

    public void init() {

        jdbcInsertaExpediente = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPEDIENTEPASIVO")
                .withProcedureName("SP_INS_EXPEDENTE");

        jdbcInsertaExpedienteTipoActa = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPEDIENTEPASIVO")
                .withProcedureName("SP_INS_TIPOACTA");

        jdbcInsertaResponsable = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPEDIENTEPASIVO")
                .withProcedureName("SP_INS_RESPONSABLE");

        jdbcActualizaEstatus = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPEDIENTEPASIVO")
                .withProcedureName("SP_ACT_ESTATUS");

        jdbcConsultaExpediente = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPEDIENTEPASIVO")
                .withProcedureName("SP_SEL_EXPEDENTE")
                .returningResultSet("RCL_EXPEDIENTE", new ExpedienteRowMapper());

        jdbcConsultaResponsable = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPEDIENTEPASIVO")
                .withProcedureName("SP_SEL_RESPONSABLE")
                .returningResultSet("RCL_RESPONSABLE", new ResponsableRowMapper());

        jdbcEliminaExpediente = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPEDIENTEPASIVO")
                .withProcedureName("SP_DEL_EXP");

        jdbcEliminaResponsable = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAEXPEDIENTEPASIVO")
                .withProcedureName("SP_DEL_RESPONSABLES");
    }

    @Override
    public int insertaExpediente(ExpedientesDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        int idExpediente = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", bean.getIdGerente())
                .addValue("PA_IDCECO", bean.getIdCeco())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_ESTATUS", bean.getEstatusExpediente())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_CIUDAD", bean.getCiudad());

        out = jdbcInsertaExpediente.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASIVO.SP_INS_EXPEDENTE}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        BigDecimal expediente = (BigDecimal) out.get("PA_EXPEDIENTE");
        idExpediente = expediente.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar el Expediente");
        } else {
            return idExpediente;
        }

        return idExpediente;
    }

    @Override
    public boolean insertaResponsable(ExpedientesDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDENTIFICADOR", bean.getIdentificador())
                .addValue("PA_NOMBRERESP", bean.getNombreResp())
                .addValue("PA_ESTATUSRESP", bean.getEstatusResp())
                .addValue("PA_IDEXPEDIENTE", bean.getIdExpediente());

        out = jdbcInsertaResponsable.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASIVO.SP_INS_RESPONSABLE}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar el Responsable");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaEstatus(int estatus, int idExpediente) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ESTATUS", estatus)
                .addValue("PA_EXPEDIENTE", idExpediente);

        out = jdbcActualizaEstatus.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASIVO.SP_ACT_ESTATUS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al actualizar el estatus");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public List<ExpedientesDTO> buscaExpediente(String idExpediente, String idEstatus, String idCeco, String idUsuario,
            String fechaI, String fechaF, String tipoActa) throws Exception {
        Map<String, Object> out = null;

        List<ExpedientesDTO> listaExpediente = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_EXPEDIENTE", idExpediente)
                .addValue("PA_ESTATUS", idEstatus)
                .addValue("PA_IDCECO", idCeco)
                .addValue("PA_IDUSUA", idUsuario)
                .addValue("PA_FECHAI", fechaI)
                .addValue("PA_FECHAF", fechaF)
                .addValue("PA_TIPOACTA", tipoActa);

        out = jdbcConsultaExpediente.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASIVO.SP_SEL_EXPEDENTE}");

        listaExpediente = (List<ExpedientesDTO>) out.get("RCL_EXPEDIENTE");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener el Expediente");
        } else {
            return listaExpediente;
        }

        return null;
    }

    @Override
    public List<ExpedientesDTO> buscaResponsable(String idExpediente, String idEstatus, String idUsuario)
            throws Exception {
        Map<String, Object> out = null;

        List<ExpedientesDTO> listaExpediente = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_EXPEDIENTE", idExpediente)
                .addValue("PA_ESTATUS", idEstatus)
                .addValue("PA_IDUSUA", idUsuario);

        out = jdbcConsultaResponsable.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASIVO.SP_SEL_RESPONSABLE}");

        listaExpediente = (List<ExpedientesDTO>) out.get("RCL_RESPONSABLE");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al obtener el Expediente");
        } else {
            return listaExpediente;
        }

        return null;
    }

    @Override
    public boolean eliminaExpediente(int idExpediente) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDEXPEDIENTE", idExpediente);

        out = jdbcEliminaExpediente.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASIVO.SP_DEL_EXP}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al eliminar el expediente");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaResponsables(int idExpediente) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDEXPEDIENTE", idExpediente);

        out = jdbcEliminaResponsable.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASIVO.SP_DEL_RESPONSABLES}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al eliminar los responsables");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public int insertaExpediente(ExpedientesDTO bean, int tipoActa) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        int idExpediente = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", bean.getIdGerente())
                .addValue("PA_IDCECO", bean.getIdCeco())
                .addValue("PA_FECHA", bean.getFecha())
                .addValue("PA_ESTATUS", bean.getEstatusExpediente())
                .addValue("PA_DESCRIPCION", bean.getDescripcion())
                .addValue("PA_CIUDAD", bean.getCiudad())
                .addValue("PA_TIPO_ACTA", tipoActa);

        out = jdbcInsertaExpedienteTipoActa.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAEXPEDIENTEPASIVO.SP_INS_TIPOACTA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        BigDecimal expediente = (BigDecimal) out.get("PA_EXPEDIENTE");
        idExpediente = expediente.intValue();

        if (error == 0) {
            logger.info("Algo ocurrió al insertar el Expediente");
        } else {
            return idExpediente;
        }

        return idExpediente;

    }

}
