package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ExpedientesDTO;
import java.util.List;

public interface ExpedienteDAO {

    public List<ExpedientesDTO> buscaExpediente(String idExpediente, String idEstatus, String idCeco, String idUsuario, String fechaI, String fechaF, String tipoActa) throws Exception;

    public List<ExpedientesDTO> buscaResponsable(String idExpediente, String idEstatus, String idUsuario) throws Exception;

    public int insertaExpediente(ExpedientesDTO bean) throws Exception;

    public int insertaExpediente(ExpedientesDTO bean, int tipoActa) throws Exception;

    public boolean insertaResponsable(ExpedientesDTO bean) throws Exception;

    public boolean actualizaEstatus(int estatus, int idExpediente) throws Exception;

    public boolean eliminaExpediente(int idExpediente) throws Exception;

    public boolean eliminaResponsables(int idExpediente) throws Exception;
}
