package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.LoginDTO;
import com.gruposalinas.franquicia.mappers.ChecklistComboRowMapper;
import com.gruposalinas.franquicia.mappers.LoginRowMapper;
import com.gruposalinas.franquicia.mappers.LoginRowMapperN;
import com.gruposalinas.franquicia.util.UtilFRQ;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class LoginDAOImpl extends DefaultDAO implements LoginDAO {

    private static final Logger logger = LogManager.getLogger(LoginDAOImpl.class);
    private DefaultJdbcCall jdbcBuscaUsuario;
    private DefaultJdbcCall jdbcBuscaUsuarioCheck;

    public void init() {
        jdbcBuscaUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_LOGIN")
                .withProcedureName("SP_BUSCA_USUARIO")
                .returningResultSet("PA_CUR_DATOS", new LoginRowMapper());

        jdbcBuscaUsuarioCheck = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_LOGIN")
                .withProcedureName("SP_BUSCA_USUARIO_N")
                .returningResultSet("PA_CUR_DATOS", new LoginRowMapperN())
                .returningResultSet("PA_CUR_CHECK", new ChecklistComboRowMapper());
    }

    @SuppressWarnings("unchecked")
    public List<LoginDTO> buscaUsuario(int usuario) throws Exception {
        Map<String, Object> out = null;
        List<LoginDTO> datosUsuario = null;
        int respuesta = 0;

        try {
            logger.info("Función ejecutada: FRANQUICIA.PA_LOGIN.SP_BUSCA_USUARIO");

            SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_USUARIO", usuario);
            out = jdbcBuscaUsuario.execute(in);

            datosUsuario = (List<LoginDTO>) out.get("PA_CUR_DATOS");
            BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
            respuesta = respuestaEjec.intValue();

            if (respuesta == 1) {
                throw new Exception("Algo ocurrió en el SP");
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            UtilFRQ.printErrorLog(null, e);
        }

        return datosUsuario;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> buscaUsuarioCheck(int noEmpleado) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<LoginDTO> datosUsuario = null;
        List<ChecklistDTO> listaChecklist = null;
        int respuesta = 0;

        logger.info("Función ejecutada: FRANQUICIA.PAPRUEBA2.SP_BUSCA_USUARIO");

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_USUARIO", noEmpleado);
        out = jdbcBuscaUsuarioCheck.execute(in);

        datosUsuario = (List<LoginDTO>) out.get("PA_CUR_DATOS");
        listaChecklist = (List<ChecklistDTO>) out.get("PA_CUR_CHECK");
        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el SP");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaUsuarios", datosUsuario);
        lista.put("listaChecklist", listaChecklist);

        return lista;
    }
}
