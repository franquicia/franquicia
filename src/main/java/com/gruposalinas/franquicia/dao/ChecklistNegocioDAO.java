package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ChecklistNegocioDTO;
import java.util.List;

public interface ChecklistNegocioDAO {

    public List<ChecklistNegocioDTO> obtieneChecklistNegocio(String checklist, String negocio) throws Exception;

    public boolean insertaChecklistNegocio(int checklist, int negocio) throws Exception;

    public boolean eliminaChecklistNegocio(int checklist, int negocio) throws Exception;

}
