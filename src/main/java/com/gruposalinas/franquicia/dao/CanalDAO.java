package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.CanalDTO;
import java.util.List;

public interface CanalDAO {

    public int insertaCanal(CanalDTO bean) throws Exception;

    public boolean actualizaCanal(CanalDTO bean) throws Exception;

    public boolean eliminaCanal(int idCanal) throws Exception;

    public List<CanalDTO> buscaCanal(int idCanal) throws Exception;

    public List<CanalDTO> buscaCanales(int activo) throws Exception;

}
