package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.CompromisoDTO;
import java.util.List;

public interface CompromisoDAO {

    public List<CompromisoDTO> obtieneCompromiso() throws Exception;

    public List<CompromisoDTO> obtieneCompromiso(int idCompromiso) throws Exception;

    public int insertaCompromiso(CompromisoDTO bean) throws Exception;

    public boolean actualizaCompromiso(CompromisoDTO bean) throws Exception;

    public boolean eliminaCompromiso(int idCompromiso) throws Exception;
}
