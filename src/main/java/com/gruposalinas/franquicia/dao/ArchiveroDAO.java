package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ArchiveroDTO;
import java.util.List;

public interface ArchiveroDAO {

    public List<ArchiveroDTO> obtieneArchivero(int idArchivero) throws Exception;

    public boolean insertaArchivero(ArchiveroDTO bean) throws Exception;

    public boolean modificaArchivero(ArchiveroDTO bean) throws Exception;

    public boolean eliminaArchivero(int idArchivero, int idForm) throws Exception;

    public List<ArchiveroDTO> obtieneArchiveroFormato(int idArchivero) throws Exception;
}
