package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.GrupoDTO;
import java.util.List;

public interface GrupoDAO {

    public List<GrupoDTO> obtieneGrupo(String idGrupo) throws Exception;

    public int insertaGrupo(GrupoDTO bean) throws Exception;

    public boolean actualizaGrupo(GrupoDTO bean) throws Exception;

    public boolean eliminaGrupo(int idGrupo) throws Exception;
}
