package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ArchiveroDTO;
import com.gruposalinas.franquicia.mappers.ArchiveroRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ArchiveroDAOImpl extends DefaultDAO implements ArchiveroDAO {

    private static Logger logger = LogManager.getLogger(ArchiveroDAOImpl.class);

    DefaultJdbcCall jdbcObtieneArchivero;
    DefaultJdbcCall jdbcInsertaArchivero;
    DefaultJdbcCall jdbcActualizaArchivero;
    DefaultJdbcCall jdbcEliminaArchivero;
    DefaultJdbcCall jdbcObtieneArchiveroForm;

    public void init() {

        jdbcObtieneArchivero = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMDETARCH")
                .withProcedureName("SPGET_DETALLE")
                .returningResultSet("PA_CONSULTA", new ArchiveroRowMapper());

        jdbcInsertaArchivero = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMDETARCH")
                .withProcedureName("SPINS_DETALLE");

        jdbcActualizaArchivero = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMDETARCH")
                .withProcedureName("SPACT_DETALLE");

        jdbcEliminaArchivero = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMDETARCH")
                .withProcedureName("SPDEL_DETALLE");

        jdbcObtieneArchiveroForm = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMDETARCH")
                .withProcedureName("SPGETARCHIVERO")
                .returningResultSet("PA_CONSULTA", new ArchiveroRowMapper());

    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ArchiveroDTO> obtieneArchivero(int idArchivero) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ArchiveroDTO> listaArch = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDARCHIVERO", idArchivero);

        out = jdbcObtieneArchivero.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAADMDETARCH.SP_SEL_ARCHIVERO");

        listaArch = (List<ArchiveroDTO>) out.get("PA_CONSULTA");

        BigDecimal resultado = (BigDecimal) out.get("PA_RESEJECUCION");
        error = resultado.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al obtener el archiveros");
        } else {
            return listaArch;
        }

        return listaArch;
    }

    @Override
    public boolean insertaArchivero(ArchiveroDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIARCHIVERO_ID", bean.getIdFormArchivero())
                .addValue("PA_FCPLACA", bean.getPlaca())
                .addValue("PA_FCMARCA", bean.getMarca())
                .addValue("PA_FCDESCRIPCION", bean.getDescripcion())
                .addValue("PA_FCOBSERVACIONES", bean.getObservaciones());

        out = jdbcInsertaArchivero.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMDETARCH.SPINS_DETALLE}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_RESEJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al insertar el archivero");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean modificaArchivero(ArchiveroDTO bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIARCHIVERO_ID", bean.getIdArchivero())
                .addValue("PA_FCPLACA", bean.getPlaca())
                .addValue("PA_FCMARCA", bean.getMarca())
                .addValue("PA_FCDESCRIPCION", bean.getDescripcion())
                .addValue("PA_FCOBSERVACIONES", bean.getObservaciones())
                .addValue("PA_IDFORMARCHIVEROS", bean.getIdFormArchivero());

        out = jdbcActualizaArchivero.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMDETARCH.SPACT_DETALLE}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_RESEJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al actualizar el archivero");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean eliminaArchivero(int idArchivero, int idForm) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIDETARCHIVE_ID", idArchivero)
                .addValue("PA_FIARCHIVERO_ID", idForm);

        out = jdbcEliminaArchivero.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PAADMDETARCH.SP_DEL_ARCHIVERO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_RESEJECUCION");
        error = errorReturn.intValue();

        if (error != 1) {
            logger.info("Algo ocurrió al eliminar el Archivero");
        } else {
            return true;
        }
        return false;
    }

    @Override
    public List<ArchiveroDTO> obtieneArchiveroFormato(int idFormArchivero) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ArchiveroDTO> listaArch = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIARCHIVERO_ID", idFormArchivero);

        out = jdbcObtieneArchiveroForm.execute(in);

        logger.info("Función ejecutada: FRANQUICIA.PAADMDETARCH.SP_SEL_ARCHIVEROFORM");

        listaArch = (List<ArchiveroDTO>) out.get("PA_CONSULTA");

        //BigDecimal resultado =(BigDecimal) out.get("PA_RESEJECUCION");
        //error = resultado.intValue();
        if (listaArch != null) {
            error = 1;
        }
        if (error != 1) {
            logger.info("Algo ocurrió al obtener la lista de archiveros");
        } else {
            return listaArch;
        }

        return listaArch;
    }

}
