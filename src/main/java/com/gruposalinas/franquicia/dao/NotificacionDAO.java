package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.MovilInfoDTO;
import java.util.List;
import java.util.Map;

public interface NotificacionDAO {

    public Map<String, Object> notificacionPorcentaje(int idUsuario) throws Exception;

    public List<MovilInfoDTO> obtieneUsuariosNoti(String so) throws Exception;
}
