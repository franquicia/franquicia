package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.VisitasxMesDTO;
import java.util.List;

public interface VisitasxMesDAO {

    public List<VisitasxMesDTO> obtieneVisitas(int checklist, String fechaInicio, String fechaFin) throws Exception;

}
