package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.SucursalesCercanasDTO;
import java.util.List;

public interface SucursalesCercanasDAO {

    public List<SucursalesCercanasDTO> obtieneSucursales(String latitud, String longitud) throws Exception;

}
