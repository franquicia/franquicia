package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.NegocioDTO;
import java.util.List;

public interface NegocioDAO {

    public List<NegocioDTO> obtieneNegocio() throws Exception;

    public List<NegocioDTO> obtieneNegocio(int idNegocio) throws Exception;

    public int insertaNegocio(NegocioDTO bean) throws Exception;

    public boolean actualizaNegocio(NegocioDTO bean) throws Exception;

    public boolean eliminaNegocio(int idNegocio) throws Exception;
}
