package com.gruposalinas.franquicia.dao;

import java.util.Map;

public interface ChecksOfflineDAO {

    public Map<String, Object> obtieneChecks(int idUsuario) throws Exception;

    public Map<String, Object> obtieneChecksNuevo(int idUsuario) throws Exception;

}
