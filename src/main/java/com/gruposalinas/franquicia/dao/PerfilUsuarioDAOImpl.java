package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PerfilUsuarioDTO;
import com.gruposalinas.franquicia.mappers.PerfilUsuarioRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PerfilUsuarioDAOImpl extends DefaultDAO implements PerfilUsuarioDAO {

    private static Logger logger = LogManager.getLogger(PerfilDAOImpl.class);

    private DefaultJdbcCall jdbcObtienePerfiles;
    private DefaultJdbcCall jdbcInsertaPerfilUsuario;
    private DefaultJdbcCall jdbcActualizaPerfilUsuario;
    private DefaultJdbcCall jdbcEliminaPerfilUsuario;

    public void init() {

        jdbcObtienePerfiles = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILUSU")
                .withProcedureName("SP_SEL_PERFILUSU")
                .returningResultSet("RCL_USUARIO", new PerfilUsuarioRowMapper());

        jdbcInsertaPerfilUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILUSU")
                .withProcedureName("SP_INS_PERFILUSU");

        jdbcActualizaPerfilUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILUSU")
                .withProcedureName("SP_ACT_PERFILUSU");

        jdbcEliminaPerfilUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAADMPERFILUSU")
                .withProcedureName("SP_DEL_PERFILUSU");
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PerfilUsuarioDTO> obtienePerfiles(String idUsuario, String idPerfil) throws Exception {

        Map<String, Object> out = null;
        List<PerfilUsuarioDTO> listaUsuarioPerfil = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ID_USUARIO", idUsuario).addValue("PA_ID_PERFIL", idPerfil);

        out = jdbcObtienePerfiles.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPERFILUSU.SP_SEL_PERFILUSU}");

        listaUsuarioPerfil = (List<PerfilUsuarioDTO>) out.get("RCL_USUARIO");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al obtener los perfiles ");
        } else {
            return listaUsuarioPerfil;
        }

        return null;
    }

    @Override
    public boolean insertaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_USUARIO", perfilUsuarioDTO.getIdUsuario())
                .addValue("PA_ID_PERFIL", perfilUsuarioDTO.getIdPerfil());

        out = jdbcInsertaPerfilUsuario.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPERFILUSU.SP_INS_PERFILUSU}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al insertar Perfil Usuario");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaPerfilUsuario(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_USUARIO", perfilUsuarioDTO.getIdUsuario())
                .addValue("PA_ID_PERFIL", perfilUsuarioDTO.getIdPerfil());

        out = jdbcActualizaPerfilUsuario.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPERFILUSU.SP_ACT_PERFILUSU}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al actualizar Perfil Usuario");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean eliminaPerfilUsaurio(PerfilUsuarioDTO perfilUsuarioDTO) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_ID_USUARIO", perfilUsuarioDTO.getIdUsuario())
                .addValue("PA_ID_PERFIL", perfilUsuarioDTO.getIdPerfil());

        out = jdbcEliminaPerfilUsuario.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAADMPERFILUSU.SP_DEL_PERFILUSU}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 1) {
            logger.info("Algo ocurrió al eliminar Perfil Usuario");
        } else {
            return true;
        }

        return false;
    }

}
