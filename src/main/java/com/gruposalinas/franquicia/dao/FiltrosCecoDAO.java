package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.FiltrosCecoDTO;
import com.gruposalinas.franquicia.domain.PaisDTO;
import java.util.List;
import java.util.Map;

public interface FiltrosCecoDAO {

    public Map<String, Object> obtieneFiltros(int idUsuario) throws Exception;

    public List<FiltrosCecoDTO> obtieneCecos(int idCeco, int tipoBusqueda) throws Exception;

    public List<PaisDTO> obtienePaises(int negocio) throws Exception;

    public List<FiltrosCecoDTO> obtieneTerritorios(int pais, int negocio) throws Exception;

    public List<ChecklistDTO> obtieneChecklist(int idUsuario) throws Exception;

    public List<PaisDTO> obtienePaises(int negocio, int idUsuario) throws Exception;
}
