package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.AsignacionDTO;
import java.util.List;
import java.util.Map;

public interface AsignacionDAO {

    public List<AsignacionDTO> obtieneAsignaciones(String idChecklist, String idCeco, String idPuesto, String activo) throws Exception;

    public boolean insertaAsignacion(AsignacionDTO asignacion) throws Exception;

    public boolean actualizaAsignacion(AsignacionDTO asignacion) throws Exception;

    public boolean eliminaAsignacion(AsignacionDTO asignacion) throws Exception;

    public Map<String, Object> ejecutaAsignacion() throws Exception;

}
