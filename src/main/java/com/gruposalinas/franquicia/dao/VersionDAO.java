package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.VersionDTO;
import java.util.List;

public interface VersionDAO {

    public List<VersionDTO> obtieneVersion(String version, String sistema, String so) throws Exception;

    public List<String> obtieneUltVersion(String so) throws Exception;

    public int insertaVersion(VersionDTO bean) throws Exception;

    public boolean actualizaVersion(VersionDTO bean) throws Exception;

    public boolean eliminaVersion(int idVersion) throws Exception;

}
