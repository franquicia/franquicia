package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.RespuestaAdDTO;
import java.util.List;

public interface RespuestaAdDAO {

    public List<RespuestaAdDTO> buscaRespADP(String idRespAD, String idResp) throws Exception;

    public int insertaRespAD(RespuestaAdDTO bean) throws Exception;

    public boolean actualizaRespAD(RespuestaAdDTO bean) throws Exception;

    public boolean eliminaRespAD(int idRespAD) throws Exception;

}
