package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ArbolDecisionDTO;
import java.util.List;

public interface ArbolDecisionTDAO {

    public List<ArbolDecisionDTO> buscaArbolDecisionTemp(int idChecklist) throws Exception;

    public int insertaArbolDecisionTemp(ArbolDecisionDTO bean) throws Exception;

    public boolean actualizaArbolDecisionTemp(ArbolDecisionDTO bean) throws Exception;

    public boolean eliminaArbolDecisionTemp(int idArbolDecision) throws Exception;

}
