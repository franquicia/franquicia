package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.AdminCanalPDDTO;
import com.gruposalinas.franquicia.domain.AdminMenuPDDTO;
import com.gruposalinas.franquicia.domain.AdminNegocioPDDTO;
import com.gruposalinas.franquicia.domain.AdminParametroPDDTO;
import com.gruposalinas.franquicia.domain.AdminTipoDocPDDTO;
import com.gruposalinas.franquicia.domain.AdmonEstatusTabletaPDDTO;
import com.gruposalinas.franquicia.domain.AdmonTabletPDDTO;
import com.gruposalinas.franquicia.domain.BachDescargasPDDTO;
import com.gruposalinas.franquicia.domain.CanalDTO;
import com.gruposalinas.franquicia.domain.CargaArchivoDTO;
import com.gruposalinas.franquicia.domain.LoginPDDTO;
import com.gruposalinas.franquicia.domain.MenuPedestalDigital;
import com.gruposalinas.franquicia.domain.NegocioDTO;
import java.util.List;

public interface PedestalDigitalDAO {

    public LoginPDDTO login1(int idUsuario) throws Exception;

    public List<MenuPedestalDigital> login2(int id_usuario, int id_perfil) throws Exception;

    public int cargaArchivosGeneral(CargaArchivoDTO cargaArchivo) throws Exception;

    public boolean actualizaArchivo(CargaArchivoDTO cargaArchivo) throws Exception;

    public List<CargaArchivoDTO> consulta(int idArchivo) throws Exception;

    public int cargaDatosAdmonTablet(AdmonTabletPDDTO listAdmonTablet) throws Exception;

    public boolean actualizaDatosAdmonTablet(AdmonTabletPDDTO listAdmonTablet) throws Exception;

    public List<AdmonTabletPDDTO> consultaDatosAdmonTablet(int idAdmonTablet) throws Exception;

    public List<AdminNegocioPDDTO> admonNegocio(NegocioDTO negocio, int opcion) throws Exception;

    public List<AdminCanalPDDTO> admonCanal(CanalDTO canal, int opcion) throws Exception;

    public List<AdminParametroPDDTO> admonParametros(AdminParametroPDDTO parametros, int opcion) throws Exception;

    public List<AdminMenuPDDTO> admonMenu(AdminMenuPDDTO menu, int opcion) throws Exception;

    public List<AdminTipoDocPDDTO> admonTipoDoc(AdminTipoDocPDDTO tipoDoc, int opcion) throws Exception;

    public List<AdmonEstatusTabletaPDDTO> admonEstatusTablet(AdmonEstatusTabletaPDDTO estatusTablet, int opcion) throws Exception;

    public BachDescargasPDDTO admonDescarga(BachDescargasPDDTO descarga, int opcion) throws Exception;

    public List<BachDescargasPDDTO> admonDesc(BachDescargasPDDTO descarga, int opcion) throws Exception;

    public List<AdminParametroPDDTO> admonParametrosByDescRef(Integer op, AdminParametroPDDTO obj);
}
