package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PuestoDTO;
import java.util.List;

public interface PuestoDAO {

    public List<PuestoDTO> obtienePuesto() throws Exception;

    public List<PuestoDTO> obtienePuestoGeo() throws Exception;

    public List<PuestoDTO> obtienePuesto(int idPuesto) throws Exception;

    public int insertaPuesto(PuestoDTO bean) throws Exception;

    public boolean actualizaPuesto(PuestoDTO bean) throws Exception;

    public boolean eliminaPuesto(int idPuesto) throws Exception;

}
