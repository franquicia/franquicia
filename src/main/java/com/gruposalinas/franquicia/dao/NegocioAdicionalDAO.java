package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.NegocioAdicionalDTO;
import java.util.List;

public interface NegocioAdicionalDAO {

    public List<NegocioAdicionalDTO> obtieneNegocioAdicional(String usuario, String negocio) throws Exception;

    public boolean insertaNegocioAdicional(int usuario, int negocio) throws Exception;

    public boolean eliminaNegocioAdicional(int usuario, int negocio) throws Exception;

}
