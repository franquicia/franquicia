package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.TipoArchivoDTO;
import java.util.List;

public interface TipoArchivoDAO {

    public int insertaTipoArchivo(TipoArchivoDTO bean) throws Exception;

    public boolean eliminaTipoArchivo(int idTipo) throws Exception;

    public boolean actualizaTipoArchivo(TipoArchivoDTO bean) throws Exception;

    public List<TipoArchivoDTO> obtieneTipoArchivo() throws Exception;

    public List<TipoArchivoDTO> obtieneTipoArchivo(int idTipo) throws Exception;

}
