package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PreguntaDTO;
import java.util.List;

public interface PreguntaTDAO {

    public List<PreguntaDTO> obtienePreguntaTemp(int idPregunta) throws Exception;

    public int insertaPreguntaTemp(PreguntaDTO bean) throws Exception;

    public boolean actualizaPreguntaTemp(PreguntaDTO bean) throws Exception;

    public boolean eliminaPreguntaTemp(int idPregunta) throws Exception;

}
