package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.SucursalesCercanasDTO;
import com.gruposalinas.franquicia.mappers.SucursalesCercanasRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class SucursalesCercanasDAOImpl extends DefaultDAO implements SucursalesCercanasDAO {

    private Logger logger = LogManager.getLogger(SucursalesCercanasDAOImpl.class);

    private DefaultJdbcCall jdbcObtienSucursales;

    public void init() {

        jdbcObtienSucursales = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PASUCCERCANAS")
                .withProcedureName("SP_OBTIENE_SUC")
                .returningResultSet("RCL_CONSULTA", new SucursalesCercanasRowMapper());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<SucursalesCercanasDTO> obtieneSucursales(String latitud, String longitud) throws Exception {

        List<SucursalesCercanasDTO> lista = null;
        Map<String, Object> out = null;
        int ejecucion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_LATITUD", latitud).addValue("PA_LONGITUD", longitud);

        out = jdbcObtienSucursales.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PASUCCERCANAS.SP_OBTIENE_SUC}");

        BigDecimal ejecucionReturn = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = ejecucionReturn.intValue();

        if (ejecucion == 0) {
            logger.info("Algo paso al obtener las sucursales cercanas");
        } else {
            lista = (List<SucursalesCercanasDTO>) out.get("RCL_CONSULTA");
        }

        return lista;
    }

}
