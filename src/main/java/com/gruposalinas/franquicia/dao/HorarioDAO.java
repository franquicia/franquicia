package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.HorarioDTO;
import java.util.List;

public interface HorarioDAO {

    public List<HorarioDTO> obtieneHorario() throws Exception;

    public List<HorarioDTO> obtenerHorario(int idHorario) throws Exception;

    public int insertaHorario(HorarioDTO bean) throws Exception;

    public boolean actualizaHorario(HorarioDTO bean) throws Exception;

    public boolean eliminaHorario(int idHorario) throws Exception;
}
