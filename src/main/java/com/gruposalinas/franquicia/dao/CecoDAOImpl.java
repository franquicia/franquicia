package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.CecoDTO;
import com.gruposalinas.franquicia.mappers.CecoGeografiaRowMapper;
import com.gruposalinas.franquicia.mappers.CecoPasoRowMapper;
import com.gruposalinas.franquicia.mappers.CecoRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class CecoDAOImpl extends DefaultDAO implements CecoDAO {

    private static Logger logger = LogManager.getLogger(CecoDAOImpl.class);

    private DefaultJdbcCall jdbcInsertaCeco;
    private DefaultJdbcCall jdbcActualizaCeco;
    private DefaultJdbcCall jdbcEliminaCeco;
    private DefaultJdbcCall jdbcBuscaCeco;
    private DefaultJdbcCall jdbcBuscaCecos;
    private DefaultJdbcCall jdbcCargaCecos;
    private DefaultJdbcCall jdbcCargaCecos2;
    private DefaultJdbcCall jdbcCargaGeografia;
    private DefaultJdbcCall jdbcUpdateCecos;
    private DefaultJdbcCall jdbcBuscaCecoPaso;
    private DefaultJdbcCall jdbcBuscaCecosPaso;
    private DefaultJdbcCall jdbcElimnaCecosTrabajo;
    private DefaultJdbcCall jdbcCargaSFGuatemala;
    private DefaultJdbcCall jdbcCargaComGuatemala;
    private DefaultJdbcCall jdbcCargaSFPeru;
    private DefaultJdbcCall jdbcCargaComPeru;
    private DefaultJdbcCall jdbcCargaSFHonduras;
    private DefaultJdbcCall jdbcCargaComHonduras;
    private DefaultJdbcCall jdbcCargaSFPanama;
    private DefaultJdbcCall jdbcCargaSFSalvador;
    private DefaultJdbcCall jdbcBuscaTerritorio;
    private DefaultJdbcCall jdbcBuscaCecosP;
    private DefaultJdbcCall jdbcEliminaDuplicados;

    public void init() {

        jdbcInsertaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CECO").withProcedureName("SP_INS_CECO");

        jdbcActualizaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CECO").withProcedureName("SP_ACT_CECO");

        jdbcEliminaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CECO").withProcedureName("SP_DEL_CECO");

        jdbcBuscaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CECO").withProcedureName("SP_SEL_CECO")
                .returningResultSet("RCL_CECO", new CecoRowMapper());

        jdbcBuscaCecos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CECO").withProcedureName("SP_SEL_G_CC")
                .returningResultSet("RCL_CECO", new CecoRowMapper());

        jdbcCargaCecos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CECO").withProcedureName("SPAUTOCECOS_SF");

        jdbcCargaCecos2 = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PACARGACECOS").withProcedureName("SPMAINCECOS");

        jdbcCargaGeografia = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PAREPORTE4").withProcedureName("SP_CARGA_GEOGRAFIA");

        jdbcUpdateCecos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CECO").withProcedureName("SPUPDATE_CECOS");

        jdbcBuscaCecosPaso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PATABPASO").withProcedureName("SP_SEL_G_CECOS")
                .returningResultSet("RCL_PCECOS", new CecoPasoRowMapper());

        jdbcBuscaCecoPaso = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PATABPASO").withProcedureName("SP_SEL_CECOS")
                .returningResultSet("RCL_PCECOS", new CecoPasoRowMapper());

        jdbcElimnaCecosTrabajo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA").withCatalogName("PAELIMINACECOS").withProcedureName("SPELIMINACECOS");

        jdbcBuscaTerritorio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CECO").withProcedureName("SP_SEL_TERRITORIO")
                .returningResultSet("RCL_TERRITORIO", new CecoGeografiaRowMapper());

        jdbcBuscaCecosP = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CECO").withProcedureName("SP_SEL_CECOP")
                .returningResultSet("RCL_CECO", new CecoGeografiaRowMapper());

        jdbcCargaSFGuatemala = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PACECOSGUATE").withProcedureName("SPCECOSSF");

        jdbcCargaComGuatemala = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PACECOSGUATE").withProcedureName("SPCECOSCOM");

        jdbcCargaSFPeru = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PACECOSPERU").withProcedureName("SPCECOSSF");

        jdbcCargaComPeru = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PACECOSPERU").withProcedureName("SPCECOSCOM");

        jdbcCargaSFHonduras = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PACECOSHONDURAS").withProcedureName("SPCECOSSF");

        jdbcCargaComHonduras = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PACECOSHONDURAS").withProcedureName("SPCECOSCOM");

        jdbcCargaSFPanama = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PACECOSPANAMA").withProcedureName("SPCECOSSF");

        jdbcCargaSFSalvador = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PACECOSALVADOR").withProcedureName("SPCECOSSF");

        jdbcEliminaDuplicados = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PATABPASO").withProcedureName("SPELIMINA_DUPLI");
    }

    public boolean insertaCeco(CecoDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_FIID_PAIS", bean.getIdPais())
                .addValue("PA_FIID_NEGOCIO", bean.getIdNegocio())
                .addValue("PA_FIID_CANAL", bean.getIdCanal())
                .addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_FCNOMBRE", bean.getDescCeco())
                .addValue("PA_FICECO_SUPERIOR", bean.getIdCecoSuperior())
                .addValue("PA_FIACTIVO", bean.getActivo())
                .addValue("PA_FIID_NIVEL", bean.getIdNivel())
                .addValue("PA_FCCALLE", bean.getCalle())
                .addValue("PA_FCCIUDAD", bean.getCiudad())
                .addValue("PA_FCCP", bean.getCp())
                .addValue("PA_FCNOMBRE_CTO", bean.getNombreContacto())
                .addValue("PA_FCPUESTO_CTO", bean.getPuestoContacto())
                .addValue("PA_FCTELEFONO_CTO", bean.getTelefonoContacto())
                .addValue("PA_FCFAX_CTO", bean.getFaxContacto())
                .addValue("PA_FCUSUARIO_MOD", bean.getUsuarioModifico())
                .addValue("PA_FDFECHA_MOD", bean.getFechaModifico());

        out = jdbcInsertaCeco.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_CECO.SP_INS_CECO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar CECO");
        } else {
            return true;
        }

        return false;
    }

    public boolean actualizaCeco(CecoDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CECO", bean.getIdCeco())
                .addValue("PA_FIID_PAIS", bean.getIdPais()).addValue("PA_FIID_NEGOCIO", bean.getIdNegocio())
                .addValue("PA_FIID_CANAL", bean.getIdCanal()).addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_FCNOMBRE", bean.getDescCeco()).addValue("PA_FICECO_SUPERIOR", bean.getIdCecoSuperior())
                .addValue("PA_FIACTIVO", bean.getActivo()).addValue("PA_FIID_NIVEL", bean.getIdNivel())
                .addValue("PA_FCCALLE", bean.getCalle()).addValue("PA_FCCIUDAD", bean.getCiudad())
                .addValue("PA_FCCP", bean.getCp()).addValue("PA_FCNOMBRE_CTO", bean.getNombreContacto())
                .addValue("PA_FCPUESTO_CTO", bean.getPuestoContacto())
                .addValue("PA_FCTELEFONO_CTO", bean.getTelefonoContacto())
                .addValue("PA_FCFAX_CTO", bean.getFaxContacto()).addValue("PA_FCUSUARIO_MOD", bean.getUsuarioModifico())
                .addValue("PA_FDFECHA_MOD", bean.getFechaModifico());

        out = jdbcActualizaCeco.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_CECO.SP_ACT_CECO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar CECO");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaCeco(int ceco) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCID_CECO", ceco);

        out = jdbcEliminaCeco.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_CECO.SP_DEL_CECO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al elimnar CECO");
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<CecoDTO> buscaCeco(int ceco) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<CecoDTO> listaCeco = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CECO", ceco);

        out = jdbcBuscaCeco.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_CECO.SP_SEL_CECO}");

        listaCeco = (List<CecoDTO>) out.get("RCL_CECO");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al concultar el CECO : " + ceco);
        } else {
            return listaCeco;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<CecoDTO> buscaCecos(int activo) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CecoDTO> listaCeco = null;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_ACTIVO", activo);
        out = jdbcBuscaCecos.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_CECO.SP_SEL_G_CC}");

        listaCeco = (List<CecoDTO>) out.get("RCL_CECO");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al consultar los CECO");
        } else {
            return listaCeco;
        }

        return null;
    }

    public boolean cargaCecos() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaCecos.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_CECO.SPAUTOCECOS_SF}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al cargar los CECOS ");
        } else {
            return true;
        }

        return false;

    }

    public boolean cargaCecos2() throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;

        out = jdbcCargaCecos2.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PACARGACECOS.SPMAINCECOS}");

        BigDecimal resReturn = (BigDecimal) out.get("PA_EJECUCCION");
        respuesta = resReturn.intValue();

        if (respuesta == 0) {
            logger.info("Algo ocurrió al cargar los CECOS 2 ");
        } else {
            return true;
        }

        return false;

    }

    public boolean updateCecos() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcUpdateCecos.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_CECO.SPUPDATE_CECOS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar los CECOS ");
        } else {
            return true;
        }

        return false;

    }

    @SuppressWarnings("unchecked")
    public List<CecoDTO> buscaCecosPaso() throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CecoDTO> listaCeco = null;

        out = jdbcBuscaCecosPaso.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PATABPASO.SP_SEL_G_CECOS}");

        listaCeco = (List<CecoDTO>) out.get("RCL_PCECOS");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al consultar los CECO");
        } else {
            return listaCeco;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<CecoDTO> buscaCecoPaso(String ceco, String cecoPadre, String descripcion) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<CecoDTO> listaCeco = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCECO", ceco)
                .addValue("PA_IDCECOP", cecoPadre).addValue("PA_DESCRIPCION", descripcion);

        out = jdbcBuscaCecoPaso.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PATABPASO.SP_SEL_CECOS}");

        listaCeco = (List<CecoDTO>) out.get("RCL_PCECOS");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al concultar el CECO : " + ceco);
        } else {
            return listaCeco;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<CecoDTO> buscaTerritorios(int idPais) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CecoDTO> listaTerritorio = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDPAIS", idPais);

        out = jdbcBuscaTerritorio.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_CECO.SP_SEL_TERRITORIO}");

        listaTerritorio = (List<CecoDTO>) out.get("RCL_TERRITORIO");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al consultar los Territorios");
        } else {
            return listaTerritorio;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<CecoDTO> buscaCecosPasoP(int idCecoPadre) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CecoDTO> listaCeco = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_CECOP", idCecoPadre);

        out = jdbcBuscaCecosP.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_CECO.SP_SEL_CECOP}");

        listaCeco = (List<CecoDTO>) out.get("RCL_CECO");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al consultar los Territorios");
        } else {
            return listaCeco;
        }

        return null;
    }

    public int eliminaCecosTrabajo() throws Exception {

        Map<String, Object> out = null;
        int ejecuccion = 0;
        int regAfectados = 0;

        out = jdbcElimnaCecosTrabajo.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PAELIMINACECOS.SPELMINACECOS}");

        BigDecimal returnEjeccucion = (BigDecimal) out.get("PA_EJECUCCION");
        ejecuccion = returnEjeccucion.intValue();

        BigDecimal returnAfectados = (BigDecimal) out.get("PA_NUMROWS");
        regAfectados = returnAfectados.intValue();

        if (ejecuccion == 1) {
            return regAfectados;
        }

        return 0;

    }

    public int[] cargaCecosSFGuatemala() throws Exception {

        Map<String, Object> out = null;
        int[] params = new int[2];
        int ejecucion = 0;
        int regAfectados = 0;

        out = jdbcCargaSFGuatemala.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PACECOSGUATE.SPCECOSSF}");

        BigDecimal returnEjeccucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjeccucion.intValue();

        BigDecimal returnAfectados = (BigDecimal) out.get("PA_REGISTROS");
        regAfectados = returnAfectados.intValue();

        params[0] = ejecucion;
        params[1] = regAfectados;

        if (ejecucion == 0) {
            logger.info("Algo ocurrió en el SP FRANQUICIA.PACECOSGUATE.SPCECOSSF");
        }

        return params;

    }

    public int[] cargaCecosCOMGuatemala() throws Exception {

        Map<String, Object> out = null;
        int[] params = new int[2];
        int ejecucion = 0;
        int regAfectados = 0;

        out = jdbcCargaComGuatemala.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PACECOSGUATE.SPCECOSCOM}");

        BigDecimal returnEjeccucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjeccucion.intValue();

        BigDecimal returnAfectados = (BigDecimal) out.get("PA_REGISTROS");
        regAfectados = returnAfectados.intValue();

        params[0] = ejecucion;
        params[1] = regAfectados;

        if (ejecucion == 0) {
            logger.info("Algo ocurrió en el SP FRANQUICIA.PACECOSGUATE.SPCECOSCOM");
        }

        return params;

    }

    public int[] cargaCecosSFPeru() throws Exception {

        Map<String, Object> out = null;
        int[] params = new int[2];
        int ejecucion = 0;
        int regAfectados = 0;

        out = jdbcCargaSFPeru.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PACECOSPERU.SPCECOSSF}");

        BigDecimal returnEjeccucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjeccucion.intValue();

        BigDecimal returnAfectados = (BigDecimal) out.get("PA_REGISTROS");
        regAfectados = returnAfectados.intValue();

        params[0] = ejecucion;
        params[1] = regAfectados;

        if (ejecucion == 0) {
            logger.info("Algo ocurrió en el SP FRANQUICIA.PACECOSPERU.SPCECOSSF");
        }

        return params;

    }

    public int[] cargaCecosCOMPeru() throws Exception {

        Map<String, Object> out = null;
        int[] params = new int[2];
        int ejecucion = 0;
        int regAfectados = 0;

        out = jdbcCargaComPeru.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PACECOSPERU.SPCECOSCOM}");

        BigDecimal returnEjeccucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjeccucion.intValue();

        BigDecimal returnAfectados = (BigDecimal) out.get("PA_REGISTROS");
        regAfectados = returnAfectados.intValue();

        params[0] = ejecucion;
        params[1] = regAfectados;

        if (ejecucion == 0) {
            logger.info("Algo ocurrió en el SP FRANQUICIA.PACECOSPERU.SPCECOSCOM");
        }

        return params;

    }

    public int[] cargaCecosSFHonduras() throws Exception {

        Map<String, Object> out = null;
        int[] params = new int[2];
        int ejecucion = 0;
        int regAfectados = 0;

        out = jdbcCargaSFHonduras.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PACECOSHONDURAS.SPCECOSSF}");

        BigDecimal returnEjeccucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjeccucion.intValue();

        BigDecimal returnAfectados = (BigDecimal) out.get("PA_REGISTROS");
        regAfectados = returnAfectados.intValue();

        params[0] = ejecucion;
        params[1] = regAfectados;

        if (ejecucion == 0) {
            logger.info("Algo ocurrió en el SP FRANQUICIA.PACECOSHONDURAS.SPCECOSSF");
        }

        return params;

    }

    public int[] cargaCecosCOMHonduras() throws Exception {

        Map<String, Object> out = null;
        int[] params = new int[2];
        int ejecucion = 0;
        int regAfectados = 0;

        out = jdbcCargaComHonduras.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PACECOSHONDURAS.SPCECOSCOM}");

        BigDecimal returnEjeccucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjeccucion.intValue();

        BigDecimal returnAfectados = (BigDecimal) out.get("PA_REGISTROS");
        regAfectados = returnAfectados.intValue();

        params[0] = ejecucion;
        params[1] = regAfectados;

        if (ejecucion == 0) {
            logger.info("Algo ocurrió en el SP FRANQUICIA.PACECOSHONDURAS.SPCECOSCOM");
        }

        return params;

    }

    public int[] cargaCecosSFPanama() throws Exception {
        Map<String, Object> out = null;
        int[] params = new int[2];
        int ejecucion = 0;
        int regAfectados = 0;

        out = jdbcCargaSFPanama.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PACECOSPANAMA.SPCECOSSF}");

        BigDecimal returnEjeccucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjeccucion.intValue();

        BigDecimal returnAfectados = (BigDecimal) out.get("PA_REGISTROS");
        regAfectados = returnAfectados.intValue();

        params[0] = ejecucion;
        params[1] = regAfectados;

        if (ejecucion == 0) {
            logger.info("Algo ocurrió en el SP FRANQUICIA.PACECOSPANAMA.SPCECOSSF");
        }

        return params;

    }

    public int[] cargaCecosSFSalvador() throws Exception {
        Map<String, Object> out = null;
        int[] params = new int[2];
        int ejecucion = 0;
        int regAfectados = 0;

        out = jdbcCargaSFSalvador.execute();

        logger.info("Funcion ejecutada:{FRANQUICIA.PACECOSALVADOR.SPCECOSSF}");

        BigDecimal returnEjeccucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjeccucion.intValue();

        BigDecimal returnAfectados = (BigDecimal) out.get("PA_REGISTROS");
        regAfectados = returnAfectados.intValue();

        params[0] = ejecucion;
        params[1] = regAfectados;

        if (ejecucion == 0) {
            logger.info("Algo ocurrió en el SP FRANQUICIA.PACECOSALVADOR.SPCECOSSF");
        }

        return params;
    }

    @Override
    public boolean eliminaDuplicados() throws Exception {
        Map<String, Object> out = null;

        int ejecucion = 0;
        int regAfectados = 0;

        out = jdbcEliminaDuplicados.execute();

        logger.info("Funcion Ejecutada : {FRANQUICIA.PATABPASO.SPELIMINA_DUPLI}");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        BigDecimal returnAfectados = (BigDecimal) out.get("PA_AFECTADOS");
        regAfectados = returnAfectados.intValue();

        logger.info("Se eliminaron " + regAfectados + " registros. ");

        if (ejecucion == 0) {
            logger.info("Algo ocurrió en el SP FRANQUICIA.PATABPASO.SPELIMINA_DUPLI");
            return false;
        }

        return true;
    }

    public boolean cargaGeografia() throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        out = jdbcCargaGeografia.execute();

        logger.info("Funcion ejecutada: {FRANQUICIA.PAREPORTE4.SP_CARGA_GEOGRAFIA}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al Cargar la geografia");
        } else {
            return true;
        }

        return false;
    }
}
