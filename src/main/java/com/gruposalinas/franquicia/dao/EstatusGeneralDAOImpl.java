package com.gruposalinas.franquicia.dao;

import com.google.gson.JsonObject;
import com.gruposalinas.franquicia.domain.BatchDoctoPDDTO;
import com.gruposalinas.franquicia.domain.CatalogoEstatusDocPDDTO;
import com.gruposalinas.franquicia.domain.CecoDataPDDTO;
import com.gruposalinas.franquicia.domain.CecoInfoGraphPDDTO;
import com.gruposalinas.franquicia.domain.CecosByNegPDDTO;
import com.gruposalinas.franquicia.domain.DataTabletPDDTO;
import com.gruposalinas.franquicia.domain.DatosTablaEGDTO;
import com.gruposalinas.franquicia.domain.ErrorPDDTO;
import com.gruposalinas.franquicia.domain.FolioDetailPDDTO;
import com.gruposalinas.franquicia.domain.FoliosEstatusPDDTO;
import com.gruposalinas.franquicia.domain.GeoDataPDDTO;
import com.gruposalinas.franquicia.domain.HistoricTabletDataPDDTO;
import com.gruposalinas.franquicia.domain.IndicadoresDataEGDTO;
import com.gruposalinas.franquicia.domain.IndicadoresDataETDTO;
import com.gruposalinas.franquicia.domain.ListaCecoPorCSVPDDTO;
import com.gruposalinas.franquicia.domain.ListaEstatusTabletaPDDTO;
import com.gruposalinas.franquicia.domain.ParamNegocioPDDTO;
import com.gruposalinas.franquicia.domain.QuejasPDDTO;
import com.gruposalinas.franquicia.domain.RepDetaCecoPDDTO;
import com.gruposalinas.franquicia.domain.RepGralCecoPDDTO;
import com.gruposalinas.franquicia.domain.SucEstTabPDDTO;
import com.gruposalinas.franquicia.domain.SucursalInfoPDDTO;
import com.gruposalinas.franquicia.domain.SucursalTareasListPDDTO;
import com.gruposalinas.franquicia.domain.TablaEstatusGeneralDTO;
import com.gruposalinas.franquicia.domain.TabletDataFilterETDTO;
import com.gruposalinas.franquicia.domain.TabletInformationPDDTO;
import com.gruposalinas.franquicia.domain.TabletPerformancePDDTO;
import com.gruposalinas.franquicia.domain.TaskInTabletPDDAO;
import com.gruposalinas.franquicia.domain.TipoQuejaPDDTO;
import com.gruposalinas.franquicia.mappers.BatchDoctoRowMapperPD;
import com.gruposalinas.franquicia.mappers.CatalogoEstatusDocRowMapperPD;
import com.gruposalinas.franquicia.mappers.CecoDataRowMapperPD;
import com.gruposalinas.franquicia.mappers.CecoInfoGraphRowMapperPD;
import com.gruposalinas.franquicia.mappers.CecosByNegRowMapperPD;
import com.gruposalinas.franquicia.mappers.DatosTablaEGRowMapperPD;
import com.gruposalinas.franquicia.mappers.ErrorRowMapperPD;
import com.gruposalinas.franquicia.mappers.FolioDetailRowMapperPD;
import com.gruposalinas.franquicia.mappers.FoliosEstatusRowMapperPD;
import com.gruposalinas.franquicia.mappers.GeoDataRowMapperPD;
import com.gruposalinas.franquicia.mappers.IndicadoresDataEGRowMapperPD;
import com.gruposalinas.franquicia.mappers.IndicadoresDataETRowMapperPD;
import com.gruposalinas.franquicia.mappers.ListaCecoPorCSVRowMapperPD;
import com.gruposalinas.franquicia.mappers.ListaEstatusTabletaRowMapper;
import com.gruposalinas.franquicia.mappers.ParamNegocioRowMapperPD;
import com.gruposalinas.franquicia.mappers.QuejasRowMapperPD;
import com.gruposalinas.franquicia.mappers.RepDetaCecoRowMapperPD;
import com.gruposalinas.franquicia.mappers.RepDetaGeoRowMapperPD;
import com.gruposalinas.franquicia.mappers.RepGralCecoRowMapperPD;
import com.gruposalinas.franquicia.mappers.SucEstTabRowMapperPD;
import com.gruposalinas.franquicia.mappers.SucursalInfoRowMapperPD;
import com.gruposalinas.franquicia.mappers.SucursalTareasListRowMapperPD;
import com.gruposalinas.franquicia.mappers.TablaEstatusGeneralRowMapperPD;
import com.gruposalinas.franquicia.mappers.TabletDataFilterETRowMapperPD;
import com.gruposalinas.franquicia.mappers.TaskInTabletRowMapperPD;
import com.gruposalinas.franquicia.mappers.TipoQuejaRowMapperPD;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class EstatusGeneralDAOImpl extends DefaultDAO implements EstatusGeneralDAO {

    private static Logger logger = LogManager.getLogger(EstatusGeneralDAOImpl.class);

    private DefaultJdbcCall jdbcGetDataTableEG, jdbcGetDataTableForExcelExport;
    private DefaultJdbcCall jdbcGetIndicadoresEG, jdbcGetIndicadoresET;
    private DefaultJdbcCall jdbcGetListaEstatusTableta, jdbcInsertCriticEstatusTableta;
    private DefaultJdbcCall jdbcInsertSelfDataTablet, jdbcInsertHistoricDataTablet;
    private DefaultJdbcCall jdbcGetTabletDataByFilter;
    private DefaultJdbcCall jdbcGetSucursalInfo, jdbcGetSucursalDataTable;
    private DefaultJdbcCall jdbcGetTasksInTablet, jdbcGetLatestTaskInTablet, jdbcUpdateStatusOfTaskInTablet;
    private DefaultJdbcCall jdbcGetFolioDetail, jdbcInactiveLanzaDocFolio;
    private DefaultJdbcCall jdbcUpdateFechaVisualizacionFolio, jdbcAddSucursalesFolio, jdbcChangeDocumentFolio, jdbcValidaDocumentFolio;
    private DefaultJdbcCall jdbcGetQuejas, jdbcInsertQueja, jdbcInsertTipoComentario, jdbcUpdateTipoComentario, jdbcGetTipoComentario;
    private DefaultJdbcCall jdbcRepGralCeco, jdbcRepGralGeo, jdbcRepDetaCeco, jdbcRepDetaGeo, jdbcRepDetaIdFolio;
    private DefaultJdbcCall jdbcInsertGeo, jdbcUpdateGeo, jdbcGetGeo;
    private DefaultJdbcCall jdbcInsertCecosExcel, jdbcUpdateGeoInCecos, jdbcGetCecosData;
    private DefaultJdbcCall jdbcInsertBachDescargas, jdbcUpdateBachDescargas;
    private DefaultJdbcCall jdbcInsertEstatusDoc, jdbcUpdateEstatusDoc, jdbcGetEstatusDoc;
    private DefaultJdbcCall jdbcValidaTabletaMantenimiento;
    private DefaultJdbcCall jdbcGetEstatusDocumentosTableta;
    private DefaultJdbcCall jdbcGetHistoricTabletData;
    private DefaultJdbcCall jdbcUpdateRaizCategoria;
    private DefaultJdbcCall jdbcUpdateTabletsStatus;

    private DefaultJdbcCall jdbcGetParamNegocios, jdbcCecosByNeg, jdbcGetCecoInfoGraph, jdbcGetSucEstTabByCeco;

    /**
     * ********
     */
    /**
     * BATCH *
     */
    /**
     * ********
     */
    private DefaultJdbcCall jdbcactualizaEstatusFoliosPD, jdbcactualizaEstatusDoctoPD;

    public void init() {

        jdbcGetIndicadoresEG = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ESTATUS_GRAL")
                .withProcedureName("SP_ESTATUS_GRAL")
                .returningResultSet("PA_CURSORCON", new IndicadoresDataEGRowMapperPD());

        jdbcGetIndicadoresET = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ESTATUS_GRAL")
                .withProcedureName("SP_ESTATUS_GRAL")
                .returningResultSet("PA_CURSORCON", new IndicadoresDataETRowMapperPD());

        jdbcGetDataTableEG = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_REP_TABLETA")
                .withProcedureName("SP_REP_TABLETA")
                .returningResultSet("PA_CURSORCON", new DatosTablaEGRowMapperPD());

        jdbcGetDataTableForExcelExport = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_REP_TABLETA")
                .withProcedureName("SP_REP_TABLETA")
                .returningResultSet("PA_CURSORCON", new TablaEstatusGeneralRowMapperPD());

        jdbcGetListaEstatusTableta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_EDO_TAB")
                .withProcedureName("SP_ADMON_EDO_TAB")
                .returningResultSet("PA_CURSORCON", new ListaEstatusTabletaRowMapper());

        jdbcInsertCriticEstatusTableta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_EDO_TAB")
                .withProcedureName("SP_ADMON_EDO_TAB")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcInsertSelfDataTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_INFO_TABLET")
                .withProcedureName("SP_ESTATUS_TABLET");

        jdbcInsertHistoricDataTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_EDO_TAB")
                .withProcedureName("SP_ADMON_EDO_TAB");

        jdbcGetTabletDataByFilter = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ESTATUS_TABLETA")
                .withProcedureName("SP_ESTATUS_TABLETA")
                .returningResultSet("PA_CURSORCON", new TabletDataFilterETRowMapperPD());

        jdbcGetSucursalInfo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_INFO_SUCURSAL")
                .withProcedureName("SP_INFO_SUCURSAL")
                .returningResultSet("PA_CURSORCON", new SucursalInfoRowMapperPD());

        jdbcGetSucursalDataTable = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_INFO_SUCURSAL")
                .withProcedureName("SP_INFO_SUCURSAL")
                .returningResultSet("PA_CURSORCON", new SucursalTareasListRowMapperPD());

        jdbcGetTasksInTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_D_PEDES")
                .withProcedureName("SP_ADMON_D_PEDES")
                .returningResultSet("PA_CURSORCON", new TaskInTabletRowMapperPD());

        jdbcGetLatestTaskInTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_D_PEDES")
                .withProcedureName("SP_ADMON_D_PEDES")
                .returningResultSet("PA_CURSORCON", new TaskInTabletRowMapperPD());

        jdbcUpdateStatusOfTaskInTablet = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_D_PEDES")
                .withProcedureName("SP_ADMON_D_PEDES")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcGetFolioDetail = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_CONSUL_FOLIO")
                .withProcedureName("SP_CONSUL_FOLIO")
                .returningResultSet("PA_CURSORCON", new FolioDetailRowMapperPD());

        /**
         * *********************************
         */
        /**
         * SERVICIOS PARA MODIFICAR FOLIO *
         */
        /**
         * *********************************
         */
        jdbcUpdateFechaVisualizacionFolio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_EDIT_FOLIO")
                .withProcedureName("SP_EDIT_FOLIO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcAddSucursalesFolio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_EDIT_FOLIO")
                .withProcedureName("SP_EDIT_FOLIO")
                .returningResultSet("PA_CURSORCON", new ListaCecoPorCSVRowMapperPD());

        jdbcChangeDocumentFolio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_EDIT_FOLIO")
                .withProcedureName("SP_EDIT_FOLIO")
                .returningResultSet("PA_CURSORCON", new FolioDetailRowMapperPD());

        jdbcValidaDocumentFolio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_EDIT_FOLIO")
                .withProcedureName("SP_EDIT_FOLIO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcInactiveLanzaDocFolio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_D_PEDES")
                .withProcedureName("SP_ADMON_D_PEDES")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        /**
         * *************************************
         */
        jdbcGetQuejas = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_COMENT")
                .withProcedureName("SP_ADMON_COMENT")
                .returningResultSet("PA_CURSORCON", new QuejasRowMapperPD());

        jdbcInsertQueja = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_COMENT")
                .withProcedureName("SP_ADMON_COMENT")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcInsertTipoComentario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_COMENT")
                .withProcedureName("SP_ADMON_COMENT");

        jdbcUpdateTipoComentario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_COMENT")
                .withProcedureName("SP_ADMON_COMENT")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcGetTipoComentario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_COMENT")
                .withProcedureName("SP_ADMON_COMENT")
                .returningResultSet("PA_CURSORCON", new TipoQuejaRowMapperPD());

        /**
         * **********************************************
         */
        /**
         * SERVICIOS DE REPORTERIA GENERAL Y DETALLADA *
         */
        /**
         * **********************************************
         */
        jdbcRepGralCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_REP_GRAL_CEC")
                .withProcedureName("SP_REP_GRAL_CEC")
                .returningResultSet("PA_CURSORCON", new RepGralCecoRowMapperPD());

        jdbcRepGralGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_REP_GRAL_GEO")
                .withProcedureName("SP_REP_GRAL_GEO")
                .returningResultSet("PA_CURSORCON", new RepGralCecoRowMapperPD());

        jdbcRepDetaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_REP_DET_CEC")
                .withProcedureName("SP_REP_DET_CEC")
                .returningResultSet("PA_CURSORCON", new RepDetaCecoRowMapperPD());

        jdbcRepDetaGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_REP_DET_GEO")
                .withProcedureName("SP_REP_DET_GEO")
                .returningResultSet("PA_CURSORCON", new RepDetaGeoRowMapperPD());

        jdbcRepDetaIdFolio = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_REP_DET_GEO")
                .withProcedureName("SP_REP_DET_GEO")
                .returningResultSet("PA_CURSORCON", new RepDetaGeoRowMapperPD());

        /**
         * *************************************************************************
         */
        /**
         * SERVICIOS PARA INSERTAR, ACTUALIZAR Y OBTENER INFORMACION DE
         * GEOGRAFIA *
         */
        /**
         * *************************************************************************
         */
        jdbcInsertGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_CAT_GEO")
                .withProcedureName("SP_ADMON_CAT_GEO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcUpdateGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_CAT_GEO")
                .withProcedureName("SP_ADMON_CAT_GEO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcGetGeo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_CAT_GEO")
                .withProcedureName("SP_ADMON_CAT_GEO")
                .returningResultSet("PA_CURSORCON", new GeoDataRowMapperPD());

        /**
         * ************************************************************
         */
        /**
         * SERVICIOS PARA INSERTAR / ACTUALIZAR INFORMACION DE CECOS *
         */
        /**
         * ************************************************************
         */
        jdbcInsertCecosExcel = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_CECO")
                .withProcedureName("SP_ADMON_CECO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcUpdateGeoInCecos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_CECO")
                .withProcedureName("SP_ADMON_CECO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcGetCecosData = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_CECO")
                .withProcedureName("SP_ADMON_CECO")
                .returningResultSet("PA_CURSORCON", new CecoDataRowMapperPD());

        /**
         * *******************
         */
        /**
         * BATCH *
         */
        /**
         * ********
         */
        jdbcactualizaEstatusFoliosPD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_BATCH_DOCTO")
                .withProcedureName("SP_BATCH_DOCTO")
                .returningResultSet("PA_CURSORCON", new BatchDoctoRowMapperPD());

        jdbcactualizaEstatusDoctoPD = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_BATCH_DOCTO")
                .withProcedureName("SP_BATCH_DOCTO")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        /**
         * ***********************************************************
         */
        /**
         * SERVICIOS PARA INSERTAR CATALOGO DE ESTATUS DE DOCUMENTO *
         */
        /**
         * ***********************************************************
         */
        jdbcInsertEstatusDoc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMONEDO_DOC")
                .withProcedureName("SP_ADMONEDO_DOC")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcUpdateEstatusDoc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMONEDO_DOC")
                .withProcedureName("SP_ADMONEDO_DOC")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcGetEstatusDoc = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMONEDO_DOC")
                .withProcedureName("SP_ADMONEDO_DOC")
                .returningResultSet("PA_CURSORCON", new CatalogoEstatusDocRowMapperPD());

        /**
         * *************************************************************
         */
        jdbcValidaTabletaMantenimiento = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_TABLETA")
                .withProcedureName("SP_ADMON_TABLET")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        jdbcGetEstatusDocumentosTableta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_D_PEDES")
                .withProcedureName("SP_ADMON_D_PEDES")
                .returningResultSet("PA_CURSORCON", new FoliosEstatusRowMapperPD());

        /**
         * *************************************************************
         */
        jdbcGetHistoricTabletData = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMON_EDO_TAB")
                .withProcedureName("SP_ADMON_EDO_TAB")
                .returningResultSet("PA_CURSORCON", new ListaEstatusTabletaRowMapper());

        /**
         * *************************************************************
         */
        jdbcUpdateRaizCategoria = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ADMONTIPODOC")
                .withProcedureName("SP_ADMON_TIPODOC")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        /**
         * *************************************************************
         */
        jdbcUpdateTabletsStatus = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_ACTUALCONEC")
                .withProcedureName("SP_TSINCONEXION")
                .returningResultSet("PA_CURSORCON", new ErrorRowMapperPD());

        /**
         * SERVICIOS NUEVO MODULO DE ESTATUS DE TABLETAS
         */
        jdbcGetParamNegocios = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_EDO_TABLET_N")
                .withProcedureName("SP_OBTIENE_NEG")
                .returningResultSet("PA_CURSORCON", new ParamNegocioRowMapperPD());

        jdbcCecosByNeg = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_EDO_TABLET_N")
                .withProcedureName("SP_NEGOCIO_CECO")
                .returningResultSet("PA_CURSORCON", new CecosByNegRowMapperPD());

        jdbcGetCecoInfoGraph = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_EDO_TABLET_N")
                .withProcedureName("SP_CECOS_TABLETA")
                .returningResultSet("PA_CURSORCON", new CecoInfoGraphRowMapperPD());

        jdbcGetSucEstTabByCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_EXPOT_TABLET")
                .withProcedureName("SP_CONS_EDO_TAB")
                .returningResultSet("PA_CURSORCON", new SucEstTabRowMapperPD());

    }

    @Override
    public List<IndicadoresDataEGDTO> getIndicadoresEG(int op, String negocio) {

        Map<String, Object> out = null;
        List<IndicadoresDataEGDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_NEGOCIO", negocio != null ? negocio : null);

            out = jdbcGetIndicadoresEG.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ESTATUS_GRAL.SP_ESTATUS_GRAL}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getIndicadoresEG - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<IndicadoresDataEGDTO>();
            } else {
                resp = (List<IndicadoresDataEGDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getIndicadoresEG - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<DatosTablaEGDTO> getDatosGraficaEG(int op, int anio, String negocio) {

        Map<String, Object> out = null;
        List<DatosTablaEGDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ANIO", anio)
                    .addValue("PA_NEGOCIO", negocio);

            out = jdbcGetDataTableEG.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_REP_TABLETA.SP_REP_TABLETA}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getDatosGraficaEG - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<DatosTablaEGDTO>();
            } else {
                resp = (List<DatosTablaEGDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getDatosGraficaEG - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<TablaEstatusGeneralDTO> getDataForExcelEG(int op, int anio, String negocio) {

        Map<String, Object> out = null;
        List<TablaEstatusGeneralDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ANIO", anio)
                    .addValue("PA_NEGOCIO", negocio);

            out = jdbcGetDataTableForExcelExport.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_REP_TABLETA.SP_REP_TABLETA}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getDataForExcelEG - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<TablaEstatusGeneralDTO>();
            } else {
                resp = (List<TablaEstatusGeneralDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getDataForExcelEG - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<ListaEstatusTabletaPDDTO> getListaEstatusTableta(int op) {

        Map<String, Object> out = null;
        List<ListaEstatusTabletaPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_NUM_SERIE", null)
                    .addValue("PA_ID_TABLET_EDO", null)
                    .addValue("PA_ID_TABLETA", null)
                    .addValue("PA_ID_ESTADO_TAB", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetListaEstatusTableta.execute(in);
            logger.info("Funcion ejecutada: [getListaEstatusTableta - PEDESTALDIG.PA_ADMON_EDO_TAB.SP_ADMON_EDO_TAB]");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getListaEstatusTableta - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<ListaEstatusTabletaPDDTO>();
            } else {
                resp = (List<ListaEstatusTabletaPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getListaEstatusTableta - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public String insertInternalDataTablet(int op, DataTabletPDDTO data) {

        Map<String, Object> out = null;
        String resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TABLETA", null)
                    .addValue("PA_NUM_SERIE", data.getNumSerie())
                    .addValue("PA_FABRICANTE", data.getFabricante())
                    .addValue("PA_MODELO", data.getModelo())
                    .addValue("PA_SISITEMA_O", data.getSistemaOperativo())
                    .addValue("PA_VERSION_APP", data.getVersionApp())
                    .addValue("PA_ESPACIO_TOT", data.getTotalEspacioAlmacenamiento())
                    .addValue("PA_ESPACIO_DISP", data.getEspacioDisp())
                    .addValue("PA_RAM_TOTAL", data.getRamTotal())
                    .addValue("PA_RAM_DISP", data.getRamDisp());

            out = jdbcInsertSelfDataTablet.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_INFO_TABLET.SP_ESTATUS_TABLET}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            resp = Integer.toString(error);

            if (error == 1) {
                logger.info("insertDataTablet - Algo ocurrio al obtener la informacion");
            }
        } catch (Exception e) {
            logger.info("insertDataTablet - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public String insertHistoricDataTablet(int op, String numSerie, int idTabletEdo, String activo) {

        Map<String, Object> out = null;
        String resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_NUM_SERIE", numSerie)
                    .addValue("PA_ID_TABLET_EDO", null)
                    .addValue("PA_ID_TABLETA", null)
                    .addValue("PA_ID_ESTADO_TAB", idTabletEdo)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcInsertHistoricDataTablet.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_INFO_TABLET.SP_ESTATUS_TABLET}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();
            resp = Integer.toString(error);

            if (error == 1) {
                logger.info("insertHistoricDataTablet - Algo ocurrio al obtener la informacion");
            }
        } catch (Exception e) {
            logger.info("insertHistoricDataTablet - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<IndicadoresDataETDTO> getIndicadoresET(int op) {

        Map<String, Object> out = null;
        List<IndicadoresDataETDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_NEGOCIO", null);

            out = jdbcGetIndicadoresET.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ESTATUS_GRAL.SP_ESTATUS_GRAL}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getIndicadoresEG - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<IndicadoresDataETDTO>();
            } else {
                resp = (List<IndicadoresDataETDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getIndicadoresEG - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<TabletDataFilterETDTO> getTabletBySucursal(int op, int numSucursal) {

        Map<String, Object> out = null;
        List<TabletDataFilterETDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_SUCURSAL", numSucursal)
                    .addValue("PA_CECO", null)
                    .addValue("PA_ID_GEO", null)
                    .addValue("PA_ESTATUS_TAB", null)
                    .addValue("PA_RENDIMIENTO", null);

            out = jdbcGetTabletDataByFilter.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ESTATUS_TABLETA.SP_ESTATUS_TABLETA}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getTabletBySucursal - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<TabletDataFilterETDTO>();
            } else {
                resp = (List<TabletDataFilterETDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getTabletBySucursal - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<TabletDataFilterETDTO> getTabletByTerritorio(int op, String ceco, int estatus, int rendimiento) {

        Map<String, Object> out = null;
        List<TabletDataFilterETDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_SUCURSAL", null)
                    .addValue("PA_CECO", ceco)
                    .addValue("PA_ID_GEO", null)
                    .addValue("PA_ESTATUS_TAB", estatus)
                    .addValue("PA_RENDIMIENTO", rendimiento);

            out = jdbcGetTabletDataByFilter.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ESTATUS_TABLETA.SP_ESTATUS_TABLETA}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getTabletByTerritorio - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<TabletDataFilterETDTO>();
            } else {
                resp = (List<TabletDataFilterETDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getTabletByTerritorio - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<TabletDataFilterETDTO> getTabletByPais(int op, String geo, int estatus, int rendimiento) {

        Map<String, Object> out = null;
        List<TabletDataFilterETDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_SUCURSAL", null)
                    .addValue("PA_CECO", null)
                    .addValue("PA_ID_GEO", geo)
                    .addValue("PA_ESTATUS_TAB", estatus)
                    .addValue("PA_RENDIMIENTO", rendimiento);

            out = jdbcGetTabletDataByFilter.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ESTATUS_TABLETA.SP_ESTATUS_TABLETA}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getTabletByTerritorio - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<TabletDataFilterETDTO>();
            } else {
                resp = (List<TabletDataFilterETDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getTabletByTerritorio - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public String insertCriticTablet(int op, String numSerie, int estatus) {

        Map<String, Object> out = null;
        String resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_NUM_SERIE", numSerie)
                    .addValue("PA_ID_TABLET_EDO", null)
                    .addValue("PA_ID_TABLETA", null)
                    .addValue("PA_ID_ESTADO_TAB", estatus)
                    .addValue("PA_ACTIVO", 1);

            out = jdbcInsertCriticEstatusTableta.execute(in);
            logger.info("Funcion ejecutada: [insertCriticTablet - PEDESTALDIG.PA_ADMON_EDO_TAB.SP_ADMON_EDO_TAB]");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getListaEstatusTableta - Algo ocurrio al obtener la informacion");
                resp = "1";
            } else {
                resp = "0";
                return resp;
            }
        } catch (Exception e) {
            logger.info("getListaEstatusTableta - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<TabletInformationPDDTO> getTabletInfo(int op, int idTableta) {

        Map<String, Object> out = null;
        List<TabletInformationPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TABLETA", idTableta)
                    .addValue("PA_NUM_SERIE", null)
                    .addValue("PA_FABRICANTE", null)
                    .addValue("PA_MODELO", null)
                    .addValue("PA_SISITEMA_O", null)
                    .addValue("PA_VERSION_APP", null)
                    .addValue("PA_ESPACIO_TOT", null)
                    .addValue("PA_ESPACIO_DISP", null)
                    .addValue("PA_RAM_TOTAL", null)
                    .addValue("PA_RAM_DISP", null);

            out = jdbcInsertSelfDataTablet.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_INFO_TABLET.SP_ESTATUS_TABLET}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getTabletInfo - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<TabletInformationPDDTO>();
            } else {
                resp = (List<TabletInformationPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getTabletInfo - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<TabletPerformancePDDTO> getTabletPerformance(int op, int idTableta) {

        Map<String, Object> out = null;
        List<TabletPerformancePDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TABLETA", idTableta)
                    .addValue("PA_NUM_SERIE", null)
                    .addValue("PA_FABRICANTE", null)
                    .addValue("PA_MODELO", null)
                    .addValue("PA_SISITEMA_O", null)
                    .addValue("PA_VERSION_APP", null)
                    .addValue("PA_ESPACIO_TOT", null)
                    .addValue("PA_ESPACIO_DISP", null)
                    .addValue("PA_RAM_TOTAL", null)
                    .addValue("PA_RAM_DISP", null);

            out = jdbcInsertSelfDataTablet.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_INFO_TABLET.SP_ESTATUS_TABLET}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getTabletPerformance - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<TabletPerformancePDDTO>();
            } else {
                resp = (List<TabletPerformancePDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getTabletPerformance - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<SucursalInfoPDDTO> getSucursalInfo(int op, int idTableta) {

        Map<String, Object> out = null;
        List<SucursalInfoPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TABLETA", idTableta);

            out = jdbcGetSucursalInfo.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_INFO_SUCURSAL.SP_INFO_SUCURSAL}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getSucursalTable - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<SucursalInfoPDDTO>();
            } else {
                resp = (List<SucursalInfoPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getSucursalTable - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<SucursalTareasListPDDTO> getSucursalTable(int op, int idTableta) {

        Map<String, Object> out = null;
        List<SucursalTareasListPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TABLETA", idTableta);

            out = jdbcGetSucursalDataTable.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_INFO_SUCURSAL.SP_INFO_SUCURSAL}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getSucursalTable - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<SucursalTareasListPDDTO>();
            } else {
                resp = (List<SucursalTareasListPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getSucursalTable - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<TaskInTabletPDDAO> getTasksInTablet(int op, int sucursal, int negocio) {

        Map<String, Object> out = null;
        List<TaskInTabletPDDAO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_SUCURSAL", sucursal)
                    .addValue("PA_NEGOCIO", negocio)
                    .addValue("PA_TIPO_DOCTO", null)
                    .addValue("PA_ID_DOCTO", null)
                    .addValue("PA_ID_DOCPEDESTAL", null)
                    .addValue("PA_ID_ESTATUS", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetTasksInTablet.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_D_PEDES.SP_ADMON_D_PEDES.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getTasksInTablet - Algo ocurrio al obtener el documento");
                resp = new ArrayList<TaskInTabletPDDAO>();
            } else {
                resp = (List<TaskInTabletPDDAO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getTasksInTablet - Algo ocurrio al obtener el documento\n" + e.getMessage());
        }

        return new ArrayList<TaskInTabletPDDAO>();
    }

    @Override
    public List<TaskInTabletPDDAO> getLatestTaskInTablet(int op, int sucursal, int tipoDocumento) {

        Map<String, Object> out = null;
        List<TaskInTabletPDDAO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_SUCURSAL", sucursal)
                    .addValue("PA_NEGOCIO", null)
                    .addValue("PA_TIPO_DOCTO", tipoDocumento)
                    .addValue("PA_ID_DOCTO", null)
                    .addValue("PA_ID_DOCPEDESTAL", null)
                    .addValue("PA_ID_ESTATUS", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetLatestTaskInTablet.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_D_PEDES.SP_ADMON_D_PEDES.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getTasksInTablet - Algo ocurrio al obtener el documento");
                resp = new ArrayList<TaskInTabletPDDAO>();
            } else {
                resp = (List<TaskInTabletPDDAO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getTasksInTablet - Algo ocurrio al obtener el documento\n" + e.getMessage());
        }

        return new ArrayList<TaskInTabletPDDAO>();
    }

    @Override
    public String updateStatusOfTaskInTablet(int op, int sucursal, int negocio, int tipoDocumento, int idDocumento, int idDocPedestal, int idEstatus, int activo) {

        Map<String, Object> out = null;
        String resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_SUCURSAL", sucursal)
                    .addValue("PA_NEGOCIO", negocio)
                    .addValue("PA_TIPO_DOCTO", tipoDocumento)
                    .addValue("PA_ID_DOCTO", idDocumento)
                    .addValue("PA_ID_DOCPEDESTAL", idDocPedestal)
                    .addValue("PA_ID_ESTATUS", idEstatus)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcUpdateStatusOfTaskInTablet.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_D_PEDES.SP_ADMON_D_PEDES.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateStatusOfTaskInTablet - Algo ocurrio al actualizar esl estatus del documento");
                resp = "{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"0\"\n}";
            } else {
                List<ErrorPDDTO> cursorReturn = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                return "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + cursorReturn.get(0).getMsj() + "\"\n}";
            }
        } catch (Exception e) {
            logger.info("updateStatusOfTaskInTablet - Algo ocurrio al actualizar esl estatus del documento\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"0\"\n}";
        return resp;
    }

    @Override
    public String updateStatusOfTaskInTablet2(int op, int estatus, int idDocPedestal) {

        Map<String, Object> out = null;
        String resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_SUCURSAL", null)
                    .addValue("PA_NEGOCIO", null)
                    .addValue("PA_TIPO_DOCTO", null)
                    .addValue("PA_ID_DOCTO", null)
                    .addValue("PA_ID_DOCPEDESTAL", idDocPedestal)
                    .addValue("PA_ID_ESTATUS", estatus)
                    .addValue("PA_ACTIVO", null);

            out = jdbcUpdateStatusOfTaskInTablet.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_D_PEDES.SP_ADMON_D_PEDES.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateStatusOfTaskInTablet - Algo ocurrio al actualizar esl estatus del documento");
                resp = "{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"0\"\n}";
            } else {
                List<ErrorPDDTO> cursorReturn = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                return "{\n\t\"respuesta\":\"" + error + "\",\n\t\"msj\":\"" + cursorReturn.get(0).getMsj() + "\"\n}";
            }
        } catch (Exception e) {
            logger.info("updateStatusOfTaskInTablet - Algo ocurrio al actualizar esl estatus del documento\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"0\"\n}";
        return resp;
    }

    @Override
    public List<FolioDetailPDDTO> getFolioDetail(int op, int idFolio) {

        Map<String, Object> out = null;
        List<FolioDetailPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FOLIO", idFolio);

            out = jdbcGetFolioDetail.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_CONSUL_FOLIO.SP_CONSUL_FOLIO.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getFolioDetail - Algo ocurrio al obtener el detalle del folio");
                resp = new ArrayList<FolioDetailPDDTO>();
            } else {
                resp = (List<FolioDetailPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getFolioDetail - Algo ocurrio al obtener el detalle del folio\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public String updateFechaVisualizacionFolio(int op, int idFolio, String fecha) {

        Map<String, Object> out = null;
        String resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FOLIO", idFolio)
                    .addValue("PA_FECHA_A", fecha)
                    .addValue("PA_CECO", null)
                    .addValue("PA_ID_DOCTO", null)
                    .addValue("PA_ID_DOCTOANT", null);

            out = jdbcUpdateFechaVisualizacionFolio.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_EDIT_FOLIO.SP_EDIT_FOLIO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateFechaVisualizacionFolio - Algo ocurrio al actualizar la fecha de visualizacion del folio");
            } else {
                List<ErrorPDDTO> cursorReturn = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                resp = "{\n\t\"respuesta\":\"" + error + "\",\n\t\"idFolio\":\"" + cursorReturn.get(0).getMsj() + "\"\n}";
                return resp;
            }
        } catch (Exception e) {
            logger.info("updateFechaVisualizacionFolio - Algo ocurrio al actualizar la fecha de visualizacion del folio\n" + e.getMessage());
        }

        resp = "{\n\t\"respuesta\":\"1\",\n\t\"idFolio\":\"0\"\n}";
        return resp;
    }

    @Override
    public List<ListaCecoPorCSVPDDTO> addSucursalesFolio(int op, int idFolio, String cecos) {

        Map<String, Object> out = null;
        List<ListaCecoPorCSVPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FOLIO", idFolio)
                    .addValue("PA_FECHA_A", null)
                    .addValue("PA_CECO", cecos)
                    .addValue("PA_ID_DOCTO", null)
                    .addValue("PA_ID_DOCTOANT", null);

            out = jdbcAddSucursalesFolio.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_EDIT_FOLIO.SP_EDIT_FOLIO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("addSucursalesFolio - Algo ocurrio al agregar las sucursales al folio");
                resp = new ArrayList<ListaCecoPorCSVPDDTO>();
                return resp;
            } else {
                resp = (List<ListaCecoPorCSVPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("addSucursalesFolio - Algo ocurrio al agregar las sucursales al folio\n" + e.getMessage());
        }

        return new ArrayList<ListaCecoPorCSVPDDTO>();
    }

    @Override
    public List<String> changeDocumentFolio(int op, int documentoNuevo, int documentoAnterior) {

        Map<String, Object> out = null;
        List<String> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FOLIO", null)
                    .addValue("PA_FECHA_A", null)
                    .addValue("PA_CECO", null)
                    .addValue("PA_ID_DOCTO", documentoNuevo)
                    .addValue("PA_ID_DOCTOANT", documentoAnterior);

            out = jdbcChangeDocumentFolio.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_EDIT_FOLIO.SP_EDIT_FOLIO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("changeDocumentFolio - Algo ocurrio al sustituir el documento del folio");
                resp = new ArrayList<String>();
            } else {
                resp = (List<String>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("changeDocumentFolio - Algo ocurrio al sustituir el documento del folio\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public String validaDocumentoDeFolio(int op, int idDocumentoAnt) {

        Map<String, Object> out = null;
        List<ErrorPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FOLIO", null)
                    .addValue("PA_FECHA_A", null)
                    .addValue("PA_CECO", null)
                    .addValue("PA_ID_DOCTO", null)
                    .addValue("PA_ID_DOCTOANT", idDocumentoAnt);

            out = jdbcValidaDocumentFolio.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_EDIT_FOLIO.SP_EDIT_FOLIO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("validaDocumentoDeFolio - Algo ocurrio al obtener la informacion del documento");
                return "{\n\t\"respuesta\":\"1\",\n\t\"lanzamientos\":\"0\"\n}";
            } else {
                resp = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                ErrorPDDTO aux = (ErrorPDDTO) resp.get(0);
                return "{\n\t\"respuesta\":\"0\",\n\t\"lanzamientos\":\"" + aux.getMsj() + "\"\n}";
            }
        } catch (Exception e) {
            logger.info("validaDocumentoDeFolio - Algo ocurrio al obtener la informacion del documento\n" + e.getMessage());
        }

        return "{\n\t\"respuesta\":\"1\",\n\t\"lanzamientos\":\"0\"\n}";
    }

    @Override
    public String inactivaLanzamientosDocFolio(int op, int idDoc, int activo) {

        Map<String, Object> out = null;
        List<ErrorPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_SUCURSAL", null)
                    .addValue("PA_NEGOCIO", null)
                    .addValue("PA_TIPO_DOCTO", null)
                    .addValue("PA_ID_DOCTO", idDoc)
                    .addValue("PA_ID_DOCPEDESTAL", null)
                    .addValue("PA_ID_ESTATUS", null)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcInactiveLanzaDocFolio.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_D_PEDES.SP_ADMON_D_PEDES.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("inactivaLanzamientosDocFolio - Algo ocurrio al inactivar los lanzamientos del documento");
                return "{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"0\"\n}";
            } else {
                return "{\n\t\"respuesta\":\"0\",\n\t\"msj\":\"0\"\n}";
            }
        } catch (Exception e) {
            logger.info("inactivaLanzamientosDocFolio - Algo ocurrio al inactivar los lanzamientos del documento\n" + e.getMessage());
            return "{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"1\"\n}";
        }
    }

    @Override
    public List<QuejasPDDTO> getQuejas(int op, int idComentario) {

        Map<String, Object> out = null;
        List<QuejasPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_IDCOMENTARIO", idComentario)
                    .addValue("PA_ID_CECO", null)
                    .addValue("PA_NOM_CLIE", null)
                    .addValue("PA_TELEFONO", null)
                    .addValue("PA_MAIL", null)
                    .addValue("PA_IDCAT_COMENT", null)
                    .addValue("PA_DESCRIPCION", null)
                    .addValue("PA_CONTACTO", null)
                    .addValue("PA_ESTATUS", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetQuejas.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_COMENT.SP_ADMON_COMENT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getQuejas - Algo ocurrio al obtener las quejas");
                resp = new ArrayList<QuejasPDDTO>();
            } else {
                resp = (List<QuejasPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getQuejas - Algo ocurrio al obtener las quejas\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public String insertQueja(int op, JsonObject jsonObj) {

        Map<String, Object> out = null;
        List<ErrorPDDTO> resp = null;
        int error = 0;

        String tmp = jsonObj.get("descripcion").getAsString();
        if (jsonObj.get("descripcion").getAsString().length() > 200) {
            tmp = tmp.substring(0, 199);
        }
        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_IDCOMENTARIO", null)
                    .addValue("PA_ID_CECO", jsonObj.get("idCeco").getAsString())
                    .addValue("PA_NOM_CLIE", jsonObj.get("nombre").getAsString())
                    .addValue("PA_TELEFONO", jsonObj.get("telefono").getAsString())
                    .addValue("PA_MAIL", jsonObj.get("email").getAsString())
                    .addValue("PA_IDCAT_COMENT", jsonObj.get("idCatComentario").getAsString())
                    .addValue("PA_DESCRIPCION", tmp)
                    .addValue("PA_CONTACTO", jsonObj.get("contacto").getAsInt())
                    .addValue("PA_ESTATUS", jsonObj.get("estatus").getAsInt())
                    .addValue("PA_ACTIVO", jsonObj.get("activo").getAsInt());

            out = jdbcInsertQueja.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_COMENT.SP_ADMON_COMENT.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertQueja - Algo ocurrio al insertar la queja");
                return "{\n\t\"respuesta\":\"1\",\n\t\"idQueja\":\"0\"\n}";
            } else {
                resp = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                ErrorPDDTO aux = (ErrorPDDTO) resp.get(0);
                return "{\n\t\"respuesta\":\"0\",\n\t\"idQueja\":\"" + aux.getMsj() + "\"\n}";
            }
        } catch (Exception e) {
            logger.info("insertQueja - Algo ocurrio al insertar la queja\n" + e.getMessage());
            return "{\n\t\"respuesta\":\"1\",\n\t\"idQueja\":\"0\"\n}";
        }
    }

    @Override
    public List<RepGralCecoPDDTO> filtroRepGralCec(int op, String fechaIni, String fechaEnv, String doctoVis, String categoria, String ceco) {

        Map<String, Object> out = null;
        List<RepGralCecoPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FECHA_INI", fechaIni)
                    .addValue("PA_FECHA_ENV", fechaEnv)
                    .addValue("PA_DOCTO_VIS", doctoVis)
                    .addValue("PA_CATEGORIA", categoria)
                    .addValue("PA_CECO", ceco);

            out = jdbcRepGralCeco.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_REP_GRAL_CEC.SP_REP_GRAL_CEC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("filtroRepGralCec - Algo ocurrio al obtener los resultados del filtro");
                resp = new ArrayList<RepGralCecoPDDTO>();
            } else {
                resp = (List<RepGralCecoPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("filtroRepGralCec - Algo ocurrio al obtener los resultados del filtro\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<RepGralCecoPDDTO> filtroRepGralGeo(int op, String fechaIni, String fechaEnv, String doctoVis, String categoria, String geo) {

        Map<String, Object> out = null;
        List<RepGralCecoPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FECHA_INI", fechaIni)
                    .addValue("PA_FECHA_ENV", fechaEnv)
                    .addValue("PA_DOCTO_VIS", doctoVis)
                    .addValue("PA_CATEGORIA", categoria)
                    .addValue("PA_GEO", geo);

            out = jdbcRepGralGeo.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_REP_GRAL_GEO.SP_REP_GRAL_GEO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("filtroRepGralGeo - Algo ocurrio al obtener los resultados del filtro por geo");
                resp = new ArrayList<RepGralCecoPDDTO>();
            } else {
                resp = (List<RepGralCecoPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("filtroRepGralGeo - Algo ocurrio al obtener los resultados del filtro por geo\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<RepDetaCecoPDDTO> filtroRepDetaCec(int op, String fechaIni, String fechaEnv, String doctoVis, String tipoEnvio, String categoria, String ceco) {

        Map<String, Object> out = null;
        List<RepDetaCecoPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FECHA_INI", fechaIni)
                    .addValue("PA_FECHA_ENV", fechaEnv)
                    .addValue("PA_DOCTO_VIS", doctoVis)
                    .addValue("PA_CATEGORIA", categoria)
                    .addValue("PA_TIPO_ENVIO", tipoEnvio)
                    .addValue("PA_CECO", ceco);

            out = jdbcRepDetaCeco.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_REP_DET_CEC.SP_REP_DET_CEC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("filtroRepDetaCec - Algo ocurrio al obtener los resultados del filtro");
                resp = new ArrayList<RepDetaCecoPDDTO>();
            } else {
                resp = (List<RepDetaCecoPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("filtroRepDetaCec - Algo ocurrio al obtener los resultados del filtro\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<RepDetaCecoPDDTO> filtroRepDetaGeo(int op, String fechaIni, String fechaEnv, String doctoVis, String tipoEnvio, String categoria, String geo) {

        Map<String, Object> out = null;
        List<RepDetaCecoPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FECHA_INI", fechaIni)
                    .addValue("PA_FECHA_ENV", fechaEnv)
                    .addValue("PA_DOCTO_VIS", doctoVis)
                    .addValue("PA_CATEGORIA", categoria)
                    .addValue("PA_TIPO_ENVIO", tipoEnvio)
                    .addValue("PA_FOLIO", null)
                    .addValue("PA_ID_GEO", geo);

            out = jdbcRepDetaGeo.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_REP_DET_GEO.SP_REP_DET_GEO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("filtroRepDetaGeo - Algo ocurrio al obtener los resultados del filtro");
                resp = new ArrayList<RepDetaCecoPDDTO>();
            } else {
                resp = (List<RepDetaCecoPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("filtroRepDetaGeo - Algo ocurrio al obtener los resultados del filtro\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<RepDetaCecoPDDTO> filtroRepDetaIdFolio(int op, int idFolio) {

        Map<String, Object> out = null;
        List<RepDetaCecoPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_FECHA_INI", null)
                    .addValue("PA_FECHA_ENV", null)
                    .addValue("PA_DOCTO_VIS", null)
                    .addValue("PA_CATEGORIA", null)
                    .addValue("PA_TIPO_ENVIO", null)
                    .addValue("PA_FOLIO", idFolio)
                    .addValue("PA_ID_GEO", null);

            out = jdbcRepDetaIdFolio.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_REP_DET_GEO.SP_REP_DET_GEO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("filtroRepDetaIdFolio - Algo ocurrio al obtener los resultados del filtro");
                resp = new ArrayList<RepDetaCecoPDDTO>();
            } else {
                resp = (List<RepDetaCecoPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("filtroRepDetaIdFolio - Algo ocurrio al obtener los resultados del filtro\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public String insertGeo(int op, int idGeo, int tipo, int idcc, String desc, int dependeDe, int activo) {

        Map<String, Object> out = null;
        List<ErrorPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = null;

            if (dependeDe == 0) {
                in = new MapSqlParameterSource()
                        .addValue("PA_OPC", op)
                        .addValue("PA_ID_GEOGRAFIA", idGeo)
                        .addValue("PA_TIPO", tipo)
                        .addValue("PA_IDCC", idcc)
                        .addValue("PA_DESCRIPCION", desc)
                        .addValue("PA_DEPENDE_DE", null)
                        .addValue("PA_ACTIVO", activo);
            } else if (dependeDe > 0) {
                in = new MapSqlParameterSource()
                        .addValue("PA_OPC", op)
                        .addValue("PA_ID_GEOGRAFIA", idGeo)
                        .addValue("PA_TIPO", tipo)
                        .addValue("PA_IDCC", idcc)
                        .addValue("PA_DESCRIPCION", desc)
                        .addValue("PA_DEPENDE_DE", dependeDe)
                        .addValue("PA_ACTIVO", activo);
            }

            out = jdbcInsertGeo.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_CAT_GEO.SP_ADMON_CAT_GEO.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertGeo - Algo ocurrio al insertar la geografia");
                return "{\n\t\"respuesta\":\"1\",\n\t\"idGeo\":\"0\"\n}";
            } else {
                resp = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                ErrorPDDTO aux = (ErrorPDDTO) resp.get(0);
                return "{\n\t\"respuesta\":\"0\",\n\t\"idGeo\":\"" + aux.getMsj() + "\"\n}";
            }
        } catch (Exception e) {
            logger.info("insertGeo - Algo ocurrio al insertar la geografia\n" + e.getMessage());
            return "{\n\t\"respuesta\":\"1\",\n\t\"idGeo\":\"0\"\n}";
        }
    }

    @Override
    public String updateGeo(int op, int idGeo, int tipo, int idcc, String desc, int dependeDe, int activo) {

        Map<String, Object> out = null;
        List<ErrorPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = null;

            if (idGeo == 0) {
                in = new MapSqlParameterSource()
                        .addValue("PA_OPC", op)
                        .addValue("PA_ID_GEOGRAFIA", idGeo)
                        .addValue("PA_TIPO", tipo)
                        .addValue("PA_IDCC", idcc)
                        .addValue("PA_DESCRIPCION", desc)
                        .addValue("PA_DEPENDE_DE", null)
                        .addValue("PA_ACTIVO", activo);
            } else if (idGeo > 0) {
                in = new MapSqlParameterSource()
                        .addValue("PA_OPC", op)
                        .addValue("PA_ID_GEOGRAFIA", idGeo)
                        .addValue("PA_TIPO", tipo)
                        .addValue("PA_IDCC", idcc)
                        .addValue("PA_DESCRIPCION", desc)
                        .addValue("PA_DEPENDE_DE", dependeDe)
                        .addValue("PA_ACTIVO", activo);
            }

            out = jdbcUpdateGeo.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_CAT_GEO.SP_ADMON_CAT_GEO.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertGeo - Algo ocurrio al actualizar la geografia");
                return "{\n\t\"respuesta\":\"1\",\n\t\"idGeo\":\"0\"\n}";
            } else {
                resp = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                ErrorPDDTO aux = (ErrorPDDTO) resp.get(0);
                return "{\n\t\"respuesta\":\"0\",\n\t\"idGeo\":\"" + aux.getMsj() + "\"\n}";
            }
        } catch (Exception e) {
            logger.info("insertGeo - Algo ocurrio al actualizar la geografia\n" + e.getMessage());
            return "{\n\t\"respuesta\":\"1\",\n\t\"idGeo\":\"0\"\n}";
        }
    }

    @Override
    public List<GeoDataPDDTO> getGeo(int op, int idGeo, int tipo, int dependeDe) {

        Map<String, Object> out = null;
        List<GeoDataPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_GEOGRAFIA", idGeo)
                    .addValue("PA_TIPO", tipo)
                    .addValue("PA_IDCC", null)
                    .addValue("PA_DESCRIPCION", null)
                    .addValue("PA_DEPENDE_DE", dependeDe)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetGeo.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_CAT_GEO.SP_ADMON_CAT_GEO.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getGeo - Algo ocurrio al obtener los resultados de geografia");
                resp = new ArrayList<GeoDataPDDTO>();
            } else {
                resp = (List<GeoDataPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getGeo - Algo ocurrio al obtener los resultados de geografia\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<GeoDataPDDTO> getGeoForNull(int op, int idGeo, int tipo, int dependeDe) {

        Map<String, Object> out = null;
        List<GeoDataPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_GEOGRAFIA", idGeo)
                    .addValue("PA_TIPO", tipo)
                    .addValue("PA_IDCC", null)
                    .addValue("PA_DESCRIPCION", null)
                    .addValue("PA_DEPENDE_DE", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetGeo.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_CAT_GEO.SP_ADMON_CAT_GEO.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getGeo - Algo ocurrio al obtener los resultados de geografia");
                resp = new ArrayList<GeoDataPDDTO>();
            } else {
                resp = (List<GeoDataPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getGeo - Algo ocurrio al obtener los resultados de geografia\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public String insertCecoData(Integer op, String idCeco, Integer idSucursal, String nombre, String cecoSuperior, Integer idPais, Integer idEstado, Integer idMunicipio, String calle, Integer idNivel, Integer negocio, Integer canal,
            Integer nombreContacto, String cp, Integer idGeografia, String estado, String municipio, String colonia, String numeroExt, String numeroInt, Integer activo) {

        Map<String, Object> out = null;
        List<ErrorPDDTO> resp = null;
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op != null ? op.intValue() : null)
                    .addValue("PA_ID_CECO", idCeco != null ? idCeco : null)
                    .addValue("PA_SUCURSAL", idSucursal != null ? idSucursal.intValue() : null)
                    .addValue("PA_NOMBRE", nombre != null ? nombre : null)
                    .addValue("PA_CECO_PADRE", cecoSuperior != null ? cecoSuperior : null)
                    .addValue("PA_ID_PAIS", idPais != null ? idPais.intValue() : null)
                    .addValue("PA_ID_ESTADO", idEstado != null ? idEstado.intValue() : null)
                    .addValue("PA_ID_MUNICIPIO", idMunicipio != null ? idMunicipio.intValue() : null)
                    .addValue("PA_CALLE", calle != null ? calle : null)
                    .addValue("PA_NIVEL_C", idNivel != null ? idNivel.intValue() : null)
                    .addValue("PA_ID_NEGOCIO", negocio != null ? negocio.intValue() : null)
                    .addValue("PA_ID_CANAL", canal != null ? canal.intValue() : null)
                    .addValue("PA_RESPONSABLE", nombreContacto != null ? nombreContacto.intValue() : null)
                    .addValue("PA_FCCP", cp != null ? cp : null)
                    .addValue("PA_ID_GEOGRAFIA", idGeografia != null ? idGeografia.intValue() : null)
                    .addValue("PA_ESTADO", estado != null ? estado : null)
                    .addValue("PA_MUNICIPIO", municipio != null ? municipio : null)
                    .addValue("PA_COLONIA", colonia != null ? colonia : null)
                    .addValue("PA_NUM_EXTERIOR", numeroExt != null ? numeroExt : null)
                    .addValue("PA_NUM_INTERIOR", numeroInt != null ? numeroInt : null)
                    .addValue("PA_ACTIVO", activo != null ? activo.intValue() : null);

            out = jdbcInsertCecosExcel.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_CECO.SP_ADMON_CECO.INSERT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertGeo - Algo ocurrio al insertar la informacion del ceco");
                return "{\n\t\"respuesta\":\"1\",\n\t\"idCeco\":\"0\"\n}";
            } else {
                resp = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                ErrorPDDTO aux = (ErrorPDDTO) resp.get(0);
                return "{\n\t\"respuesta\":\"0\",\n\t\"idCeco\":\"" + aux.getMsj() + "\"\n}";
            }
        } catch (Exception e) {
            logger.info("insertGeo - Algo ocurrio al insertar la informacion del ceco\n" + e.getMessage());
            return "{\n\t\"respuesta\":\"1\",\n\t\"idCeco\":\"0\"\n}";
        }
    }

    @Override
    public String updateGeoInCeco(int op, String idCeco, int idPais, int idEstado, int idMunicipio) {

        Map<String, Object> out = null;
        List<ErrorPDDTO> resp = null;
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_CECO", idCeco)
                    .addValue("PA_SUCURSAL", null)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_CECO_PADRE", null)
                    .addValue("PA_ID_PAIS", idPais)
                    .addValue("PA_ID_ESTADO", idEstado)
                    .addValue("PA_ID_MUNICIPIO", idMunicipio)
                    .addValue("PA_CALLE", null)
                    .addValue("PA_NIVEL_C", null)
                    .addValue("PA_ID_NEGOCIO", null)
                    .addValue("PA_ID_CANAL", null)
                    .addValue("PA_RESPONSABLE", null)
                    .addValue("PA_FCCP", null)
                    .addValue("PA_ID_GEOGRAFIA", null)
                    .addValue("PA_ESTADO", null)
                    .addValue("PA_MUNICIPIO", null)
                    .addValue("PA_COLONIA", null)
                    .addValue("PA_NUM_EXTERIOR", null)
                    .addValue("PA_NUM_INTERIOR", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcUpdateGeoInCecos.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMON_CECO.SP_ADMON_CECO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateGeoInCeco - Algo ocurrio al actualizar el idGeo del ceco");
                return "{\n\t\"respuesta\":\"1\",\n\t\"idGeo\":\"0\"\n}";
            } else {
                resp = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                ErrorPDDTO aux = (ErrorPDDTO) resp.get(0);
                return "{\n\t\"respuesta\":\"0\",\n\t\"idGeo\":\"" + aux.getMsj() + "\"\n}";
            }
        } catch (Exception e) {
            logger.info("updateGeoInCeco - Algo ocurrio al actualizar el idGeo del ceco\n" + e.getMessage());
            return "{\n\t\"respuesta\":\"1\",\n\t\"idGeo\":\"0\"\n}";
        }
    }

    @Override
    public List<GeoDataPDDTO> getGeoDataById(int op, int idGeo) {

        Map<String, Object> out = null;
        List<GeoDataPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_GEOGRAFIA", idGeo)
                    .addValue("PA_TIPO", null)
                    .addValue("PA_IDCC", null)
                    .addValue("PA_DESCRIPCION", null)
                    .addValue("PA_DEPENDE_DE", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetGeo.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_CAT_GEO.SP_ADMON_CAT_GEO.SELECT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getGeoDataById - Algo ocurrio al obtener el registro de geografia");
                resp = new ArrayList<GeoDataPDDTO>();
            } else {
                resp = (List<GeoDataPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getGeoDataById - Algo ocurrio al obtener el registro de geografia\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public String insertTipoQueja(int op, int idComent, String coment, int activo) {

        Map<String, Object> out = null;
        // List<ErrorPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_IDCOMENTARIO", null)
                    .addValue("PA_ID_CECO", null)
                    .addValue("PA_NOM_CLIE", null)
                    .addValue("PA_TELEFONO", null)
                    .addValue("PA_MAIL", null)
                    .addValue("PA_IDCAT_COMENT", idComent)
                    .addValue("PA_DESCRIPCION", coment)
                    .addValue("PA_CONTACTO", null)
                    .addValue("PA_ESTATUS", null)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcInsertTipoComentario.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_COMENT.SP_ADMON_COMENT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertTipoQueja - Algo ocurrio al insertar la informacion");
                return "{\"respuesta\":\"1\"}";
            } else {
                return "{\"respuesta\":\"0\"}";
            }
        } catch (Exception e) {
            logger.info("insertTipoQueja - Algo ocurrio al insertar la informacion\n" + e.getMessage());
            return "{\"respuesta\":\"1\"}";
        }
    }

    @Override
    public String updateTipoQueja(int op, int idComent, String coment, int activo) {

        Map<String, Object> out = null;
        // List<ErrorPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_IDCOMENTARIO", null)
                    .addValue("PA_ID_CECO", null)
                    .addValue("PA_NOM_CLIE", null)
                    .addValue("PA_TELEFONO", null)
                    .addValue("PA_MAIL", null)
                    .addValue("PA_IDCAT_COMENT", idComent)
                    .addValue("PA_DESCRIPCION", coment)
                    .addValue("PA_CONTACTO", null)
                    .addValue("PA_ESTATUS", null)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcUpdateTipoComentario.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_COMENT.SP_ADMON_COMENT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateTipoQueja - Algo ocurrio al actualizar la informacion");
                return "{\"respuesta\":\"1\"}";
            } else {
                return "{\"respuesta\":\"0\"}";
            }
        } catch (Exception e) {
            logger.info("updateTipoQueja - Algo ocurrio al actualizar la informacion\n" + e.getMessage());
            return "{\"respuesta\":\"1\"}";
        }
    }

    @Override
    public List<TipoQuejaPDDTO> getTipoQueja(int op, int idTipoCat) {

        Map<String, Object> out = null;
        List<TipoQuejaPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_IDCOMENTARIO", null)
                    .addValue("PA_ID_CECO", null)
                    .addValue("PA_NOM_CLIE", null)
                    .addValue("PA_TELEFONO", null)
                    .addValue("PA_MAIL", null)
                    .addValue("PA_IDCAT_COMENT", idTipoCat)
                    .addValue("PA_DESCRIPCION", null)
                    .addValue("PA_CONTACTO", null)
                    .addValue("PA_ESTATUS", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetTipoComentario.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_COMENT.SP_ADMON_COMENT}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateTipoQueja - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<TipoQuejaPDDTO>();
            } else {
                resp = (List<TipoQuejaPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("updateTipoQueja - Algo ocurrio al obtener la informacion\n" + e.getMessage());
            resp = new ArrayList<TipoQuejaPDDTO>();
        }

        return resp;
    }

    @Override
    public List<BatchDoctoPDDTO> actualizaEstatusFoliosPD(int op, String fecha) {

        Map<String, Object> out = null;
        List<BatchDoctoPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_DOCUMENTO", null)
                    .addValue("PA_TIPO_DOCTO", null)
                    .addValue("PA_FECHA_INI", fecha)
                    .addValue("PA_FECHA_FIN", null)
                    .addValue("PA_VISIBLE", null);

            out = jdbcactualizaEstatusFoliosPD.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_BATCH_DOCTO.SP_BATCH_DOCTO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("actualizaEstatusFoliosPD - Algo ocurrio al ejecutar el proceso batch");
                resp = new ArrayList<BatchDoctoPDDTO>();
            } else {
                resp = (List<BatchDoctoPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("actualizaEstatusFoliosPD - Algo ocurrio al ejecutar el proceso batch\n" + e.getMessage());
            resp = new ArrayList<BatchDoctoPDDTO>();
        }

        return resp;
    }

    @Override
    public String actualizaEstatusDoctoPD(int op, int idDoc, int idTipoDocto) {

        Map<String, Object> out = null;
        String resp = "1-.-.-0";
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_DOCUMENTO", idDoc)
                    .addValue("PA_TIPO_DOCTO", idTipoDocto)
                    .addValue("PA_FECHA_INI", null)
                    .addValue("PA_FECHA_FIN", null)
                    .addValue("PA_VISIBLE", null);

            out = jdbcactualizaEstatusDoctoPD.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_BATCH_DOCTO.SP_BATCH_DOCTO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("actualizaEstatusDoctoPD - Algo ocurrio al actualizar el estatus del documento");
                resp = "1-.-.-0";
            } else {
                List<ErrorPDDTO> aux = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                resp = "0-.-.-" + aux.get(0).getMsj();
            }
        } catch (Exception e) {
            logger.info("actualizaEstatusDoctoPD - Algo ocurrio al actualizar el estatus del documento\n" + e.getMessage());
            resp = "1-.-.-0";
        }

        return resp;
    }

    @Override
    public String insertEstatusDocInCat(int op, int idEstatusDoc, String descEstatus, int activo) {

        Map<String, Object> out = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_ESTATUS_DOC", idEstatusDoc)
                    .addValue("PA_DESC_ESTATUS", descEstatus)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcInsertEstatusDoc.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMONEDO_DOC.SP_ADMONEDO_DOC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("insertEstatusDocInCat - Algo ocurrio al insertar la informacion");
                return "{\"msj\":\"1\",\"respuesta\":\"0\"}";
            } else {
                List<ErrorPDDTO> aux = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                return "{\"msj\":\"0\",\"respuesta\":\"" + aux.get(0).getMsj() + "\"}";
            }
        } catch (Exception e) {
            logger.info("insertEstatusDocInCat - Algo ocurrio al insertar la informacion\n" + e.getMessage());
            return "{\"msj\":\"1\",\"respuesta\":\"0\"}";
        }
    }

    @Override
    public String updateEstatusDocInCat(int op, int idEstatusDoc, String descEstatus, int activo) {

        Map<String, Object> out = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_ESTATUS_DOC", idEstatusDoc)
                    .addValue("PA_DESC_ESTATUS", descEstatus)
                    .addValue("PA_ACTIVO", activo);

            out = jdbcUpdateEstatusDoc.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMONEDO_DOC.SP_ADMONEDO_DOC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateEstatusDocInCat - Algo ocurrio al insertar la informacion");
                return "{\"msj\":\"1\",\"respuesta\":\"0\"}";
            } else {
                List<ErrorPDDTO> aux = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                return "{\"msj\":\"0\",\"respuesta\":\"" + aux.get(0).getMsj() + "\"}";
            }
        } catch (Exception e) {
            logger.info("updateEstatusDocInCat - Algo ocurrio al insertar la informacion\n" + e.getMessage());
            return "{\"msj\":\"1\",\"respuesta\":\"0\"}";
        }
    }

    @Override
    public List<CatalogoEstatusDocPDDTO> getEstatusDocInCat(int op) {

        Map<String, Object> out = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_ESTATUS_DOC", null)
                    .addValue("PA_DESC_ESTATUS", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetEstatusDoc.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMONEDO_DOC.SP_ADMONEDO_DOC}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateEstatusDocInCat - Algo ocurrio al insertar la informacion");
                return new ArrayList<CatalogoEstatusDocPDDTO>();
            } else {
                List<CatalogoEstatusDocPDDTO> resp = (List<CatalogoEstatusDocPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("updateEstatusDocInCat - Algo ocurrio al insertar la informacion\n" + e.getMessage());
            return new ArrayList<CatalogoEstatusDocPDDTO>();
        }
    }

    @Override
    public List<CecoDataPDDTO> getCecoDataByCecoPadre(int op, int dependeDe) {

        Map<String, Object> out = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_CECO", null)
                    .addValue("PA_SUCURSAL", null)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_CECO_PADRE", dependeDe)
                    .addValue("PA_ID_PAIS", null)
                    .addValue("PA_ID_ESTADO", null)
                    .addValue("PA_ID_MUNICIPIO", null)
                    .addValue("PA_CALLE", null)
                    .addValue("PA_NIVEL_C", null)
                    .addValue("PA_ID_NEGOCIO", null)
                    .addValue("PA_ID_CANAL", null)
                    .addValue("PA_RESPONSABLE", null)
                    .addValue("PA_FCCP", null)
                    .addValue("PA_ID_GEOGRAFIA", null)
                    .addValue("PA_ESTADO", null)
                    .addValue("PA_MUNICIPIO", null)
                    .addValue("PA_COLONIA", null)
                    .addValue("PA_NUM_EXTERIOR", null)
                    .addValue("PA_NUM_INTERIOR", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetCecosData.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_CECO.SP_ADMON_CECO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateEstatusDocInCat - Algo ocurrio al insertar la informacion");
                return new ArrayList<CecoDataPDDTO>();
            } else {
                List<CecoDataPDDTO> resp = (List<CecoDataPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("updateEstatusDocInCat - Algo ocurrio al insertar la informacion\n" + e.getMessage());
            return new ArrayList<CecoDataPDDTO>();
        }
    }

    @Override
    public String validaMantenimientoTableta(int op, String sucursal) {

        Map<String, Object> out = null;
        String resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TABLETA", null)
                    .addValue("PA_NUM_SERIE", null)
                    .addValue("PA_ID_CECO", sucursal)
                    .addValue("PA_ID_USUARIO", null)
                    .addValue("PA_ID_ESTATUS", null)
                    .addValue("PA_FEC_INTALA", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcValidaTabletaMantenimiento.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_TABLETA.SP_ADMON_TABLET}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("validaMantenimientoTableta - Algo ocurrio al realizar el proceso");
                return "{\"msj\":\"1\",\"respuesta\":\"0\"}";
            } else {
                List<ErrorPDDTO> aux = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                return "{\"msj\":\"0\",\"respuesta\":\"" + aux.get(0).getMsj() + "\"}";
            }
        } catch (Exception e) {
            logger.info("validaMantenimientoTableta - Algo ocurrio al realizar el proceso\n" + e.getMessage());
            return "{\"msj\":\"1\",\"respuesta\":\"0\"}";
        }
    }

    @Override
    public List<FoliosEstatusPDDTO> getEstatusFoliosTableta(int op, String sucursal, int idCategoria) {

        Map<String, Object> out = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_SUCURSAL", sucursal)
                    .addValue("PA_NEGOCIO", null)
                    .addValue("PA_TIPO_DOCTO", idCategoria)
                    .addValue("PA_ID_DOCTO", null)
                    .addValue("PA_ID_DOCPEDESTAL", null)
                    .addValue("PA_ID_ESTATUS", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetEstatusDocumentosTableta.execute(in);
            logger.info("Funcion ejecutada: {PEDESTALDIG.PA_ADMON_D_PEDES.SP_ADMON_D_PEDES}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getEstatusFoliosTableta - Algo ocurrio al obtener la informacion");
                return new ArrayList<FoliosEstatusPDDTO>();
            } else {
                List<FoliosEstatusPDDTO> resp = (List<FoliosEstatusPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getEstatusFoliosTableta - Algo ocurrio al obtener la informacion\n" + e.getMessage());
            return new ArrayList<FoliosEstatusPDDTO>();
        }
    }

    @Override
    public List<HistoricTabletDataPDDTO> getHistoricTabletData(int op, int idSucursal) {

        Map<String, Object> out = null;
        List<HistoricTabletDataPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_NUM_SERIE", null)
                    .addValue("PA_ID_TABLET_EDO", null)
                    .addValue("PA_ID_TABLETA", idSucursal)
                    .addValue("PA_ID_ESTADO_TAB", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcGetListaEstatusTableta.execute(in);
            logger.info("Funcion ejecutada: [getListaEstatusTableta - PEDESTALDIG.PA_ADMON_EDO_TAB.SP_ADMON_EDO_TAB]");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getHistoricTabletData - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<HistoricTabletDataPDDTO>();
            } else {
                resp = (List<HistoricTabletDataPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getHistoricTabletData - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public String actualizaRaizEnCategoria(int op, String idTipoDoc) {

        Map<String, Object> out = null;
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_ID_TIPODOC", idTipoDoc != null ? Integer.parseInt(idTipoDoc) : 0)
                    .addValue("PA_NOMBRE", null)
                    .addValue("PA_NIVEL", null)
                    .addValue("PA_DEPENDE_DE", null)
                    .addValue("PA_ACTIVO", null);

            out = jdbcUpdateRaizCategoria.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ADMONTIPODOC.SP_ADMON_TIPODOC.UPDATE}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("actualizaRaizEnCategoria - Algo ocurrio al actualizar la informacion");
                return "{\"msj\":\"1\",\"respuesta\":\"1\"}";
            } else {
                List<ErrorPDDTO> cursorReturn = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                return "{\"msj\":\"0\",\"respuesta\":\"" + cursorReturn.get(0).getMsj() + "\"}";
            }
        } catch (Exception e) {
            logger.info("actualizaRaizEnCategoria - Algo ocurrio al actualizar la informacion\n" + e.getMessage());
        }

        return "{\"msj\":\"1\",\"respuesta\":\"1\"}";
    }

    @Override
    public String updateTabletsEstatus() {

        Map<String, Object> out = null;
        int error = 1;

        try {
            SqlParameterSource in = new MapSqlParameterSource();

            out = jdbcUpdateTabletsStatus.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ACTUALCONEC.SP_TSINCONEXION}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("updateTabletsEstatus - Algo ocurrio al actualizar el estatus de las tabletas con problema de conexion");
                return "{\"msj\":\"1\",\"respuesta\":\"1\"}";
            } else {
                List<ErrorPDDTO> cursorReturn = (List<ErrorPDDTO>) out.get("PA_CURSORCON");
                return "{\"msj\":\"0\",\"respuesta\":\"" + cursorReturn.get(0).getMsj() + "\"}";
            }
        } catch (Exception e) {
            logger.info("updateTabletsEstatus - Algo ocurrio al actualizar el estatus de las tabletas con problema de conexion\n" + e.getMessage());
        }

        return "{\"msj\":\"1\",\"respuesta\":\"1\"}";
    }

    @Override
    public ArrayList<ParamNegocioPDDTO> getParamNegocios() {

        Map<String, Object> out = null;
        ArrayList<ParamNegocioPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource();

            out = jdbcGetParamNegocios.execute(in);
            logger.info("Funcion ejecutada: [PEDESTALDIG.PA_EDO_TABLET_N.SP_OBTIENE_NEG]");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getHistoricTabletData - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<ParamNegocioPDDTO>();
            } else {
                resp = (ArrayList<ParamNegocioPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getParamNegocios - Algo ocurrio al obtener la informacion\n" + e.getMessage());
            return new ArrayList<ParamNegocioPDDTO>();
        }

        return resp;
    }

    @Override
    public List<IndicadoresDataETDTO> getIndiByNeg(int op, String negocio) {

        Map<String, Object> out = null;
        List<IndicadoresDataETDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op)
                    .addValue("PA_NEGOCIO", negocio != null ? negocio : null);

            out = jdbcGetIndicadoresET.execute(in);
            logger.info("Función ejecutada: {PEDESTALDIG.PA_ESTATUS_GRAL.SP_ESTATUS_GRAL}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getIndiByNeg - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<IndicadoresDataETDTO>();
            } else {
                resp = (List<IndicadoresDataETDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getIndiByNeg - Algo ocurrio al obtener la informacion\n" + e.getMessage());
        }

        return resp;
    }

    @Override
    public List<CecosByNegPDDTO> getCecosNegocio(String tipo, String ceco, String negocio) {

        Map<String, Object> out = null;
        ArrayList<CecosByNegPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_TIPO", tipo != null ? tipo : null)
                    .addValue("PA_CECO", ceco != null ? ceco : null)
                    .addValue("PA_NEGOCIO", negocio != null ? negocio : null);

            out = jdbcCecosByNeg.execute(in);
            logger.info("Funcion ejecutada: [PEDESTALDIG.PA_EDO_TABLET_N.SP_NEGOCIO_CECO]");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getHistoricTabletData - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<CecosByNegPDDTO>();
            } else {
                resp = (ArrayList<CecosByNegPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getCecosNegocio - Algo ocurrio al obtener la informacion\n" + e.getMessage());
            return new ArrayList<CecosByNegPDDTO>();
        }

        return resp;
    }

    @Override
    public List<CecoInfoGraphPDDTO> getCecoInfoGraph(String op, String sucursal, String ceco) {

        Map<String, Object> out = null;
        ArrayList<CecoInfoGraphPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op != null ? op : null)
                    .addValue("PA_NEGOCIO", sucursal != null ? sucursal : null)
                    .addValue("PA_CECO", ceco != null ? ceco : null);

            out = jdbcGetCecoInfoGraph.execute(in);
            logger.info("Funcion ejecutada: [PEDESTALDIG.PA_EDO_TABLET_N.SP_CECOS_TABLETA]");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getHistoricTabletData - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<CecoInfoGraphPDDTO>();
            } else {
                resp = (ArrayList<CecoInfoGraphPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getCecoInfoGraph - Algo ocurrio al obtener la informacion\n" + e.getMessage());
            return new ArrayList<CecoInfoGraphPDDTO>();
        }

        return resp;
    }

    @Override
    public List<SucEstTabPDDTO> getSucEstTabByCeco(String op, String negocio, String ceco) {

        Map<String, Object> out = null;
        ArrayList<SucEstTabPDDTO> resp = null;
        int error = 0;

        try {
            SqlParameterSource in = new MapSqlParameterSource()
                    .addValue("PA_OPC", op != null ? op : null)
                    .addValue("PA_NEGOCIO", negocio != null ? negocio : null)
                    .addValue("PA_CECO", ceco != null ? ceco : null);

            out = jdbcGetSucEstTabByCeco.execute(in);
            logger.info("Funcion ejecutada: [PEDESTALDIG.PA_EDO_TABLET_N.SP_CONS_EDO_TAB]");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("getHistoricTabletData - Algo ocurrio al obtener la informacion");
                resp = new ArrayList<SucEstTabPDDTO>();
            } else {
                resp = (ArrayList<SucEstTabPDDTO>) out.get("PA_CURSORCON");
                return resp;
            }
        } catch (Exception e) {
            logger.info("getSucEstTabByCeco - Algo ocurrio al obtener la informacion\n" + e.getMessage());
            return new ArrayList<SucEstTabPDDTO>();
        }

        return resp;
    }
}
