package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.MovilInfoDTO;
import java.util.List;

public interface MovilInfoDAO {

    public List<MovilInfoDTO> obtieneInfoMovil(MovilInfoDTO bean) throws Exception;

    public boolean insertaInfoMovil(MovilInfoDTO bean) throws Exception;

    public boolean actualizaInfoMovil(MovilInfoDTO bean) throws Exception;

    public boolean eliminaInfoMovil(int idMovil) throws Exception;

}
