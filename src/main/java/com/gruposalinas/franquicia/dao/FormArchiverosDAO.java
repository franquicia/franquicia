package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.FormArchiverosDTO;
import java.util.List;

public interface FormArchiverosDAO {

    public List<FormArchiverosDTO> obtieneFormArchiveros(int idFormato) throws Exception;

    public int insertaFormarchiveros(FormArchiverosDTO bean) throws Exception;

    public boolean modificaFormarchiveros(FormArchiverosDTO bean) throws Exception;

    public boolean eliminaFormarchiveros(int idFormato) throws Exception;

    public List<FormArchiverosDTO> obtieneFormCeco(String idCeco) throws Exception;
}
