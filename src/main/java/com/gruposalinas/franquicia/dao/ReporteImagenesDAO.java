package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ConteoxPreguntaDTO;
import com.gruposalinas.franquicia.domain.HorasRespuestaDTO;
import com.gruposalinas.franquicia.domain.NumeroTiendasDTO;
import com.gruposalinas.franquicia.domain.ReporteImagenesDTO;
import com.gruposalinas.franquicia.domain.ReportesConteoDTO;
import java.util.List;
import java.util.Map;

public interface ReporteImagenesDAO {

    public Map<String, Object> obtieneModulosTiendas(String ceco, String checklist) throws Exception;

    public List<ReporteImagenesDTO> obtieneRespuestas(int idCeco, int idChecklist, String fecha) throws Exception;

    public String[] primerRespuesta(int idCeco, int idChecklist, String fecha) throws Exception;

    public List<NumeroTiendasDTO> obtieneNumTiendas(String ceco, int tipo) throws Exception;

    public Map<String, Object> obtieneListasDatos(int checklist, String ceco, String fecha) throws Exception;

    public List<ConteoxPreguntaDTO> obtieneConteoxModulo(int checklist, String ceco, String fecha) throws Exception;

    public List<NumeroTiendasDTO> obtieneTotalTiendasRegion(int checklist, String ceco, String fecha) throws Exception;

    public List<HorasRespuestaDTO> obtieneConteoxHora(int checklist, String ceco, String fecha) throws Exception;

    public List<ReportesConteoDTO> obtieneReportesConteo(int checklist, String ceco, String fecha) throws Exception;

}
