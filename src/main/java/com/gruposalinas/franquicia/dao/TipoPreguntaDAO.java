package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.TipoPreguntaDTO;
import java.util.List;

public interface TipoPreguntaDAO {

    public List<TipoPreguntaDTO> obtieneTipoPregunta() throws Exception;

    public List<TipoPreguntaDTO> obtieneTipoPregunta(int idTipoPreg) throws Exception;

    public int insertaTipoPregunta(TipoPreguntaDTO bean) throws Exception;

    public boolean actualizaTipoPregunta(TipoPreguntaDTO bean) throws Exception;

    public boolean eliminaTipoPregunta(int idTipoPreg) throws Exception;
}
