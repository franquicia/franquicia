package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PreguntaDTO;
import java.util.List;

public interface PreguntaDAO {

    public List<PreguntaDTO> obtienePregunta() throws Exception;

    public List<PreguntaDTO> obtienePregunta(int idPregunta) throws Exception;

    public int insertaPregunta(PreguntaDTO bean) throws Exception;

    public boolean actualizaPregunta(PreguntaDTO bean) throws Exception;

    public boolean eliminaPregunta(int idPregunta) throws Exception;

}
