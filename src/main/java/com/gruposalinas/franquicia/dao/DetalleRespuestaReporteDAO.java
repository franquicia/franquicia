package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.AlertasResumenDTO;
import com.gruposalinas.franquicia.domain.DatosVisitaResumenDTO;
import com.gruposalinas.franquicia.domain.DetalleRespuestaReporteDTO;
import com.gruposalinas.franquicia.domain.EvidenciasResumenDTO;
import com.gruposalinas.franquicia.domain.FoliosResumenDTO;
import java.util.List;
import java.util.Map;

public interface DetalleRespuestaReporteDAO {

    public List<DetalleRespuestaReporteDTO> obtieneRespuesta(int checklist, String ceco, String fecha) throws Exception;

    public Map<String, Object> obtieneResumen(int checklist, String ceco, String fecha) throws Exception;

    public List<DatosVisitaResumenDTO> obtieneDatos(int checklist, String ceco, String fecha) throws Exception;

    public List<FoliosResumenDTO> obtieneFolios(int checklist, String ceco, String fecha) throws Exception;

    public List<EvidenciasResumenDTO> obtieneEvidencias(int checklist, String ceco, String fecha) throws Exception;

    public List<AlertasResumenDTO> obtieneAlertas(int checklist, String ceco, String fecha) throws Exception;
}
