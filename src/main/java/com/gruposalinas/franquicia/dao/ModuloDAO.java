package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ModuloDTO;
import java.util.List;

public interface ModuloDAO {

    public List<ModuloDTO> obtieneModulo() throws Exception;

    public List<ModuloDTO> obtieneModulo(int idModulo) throws Exception;

    public int insertaModulo(ModuloDTO bean) throws Exception;

    public boolean actualizaModulo(ModuloDTO bean) throws Exception;

    public boolean eliminaModulo(int idModulo) throws Exception;

}
