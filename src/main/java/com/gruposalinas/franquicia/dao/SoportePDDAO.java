package com.gruposalinas.franquicia.dao;

public interface SoportePDDAO {

    String insertCecoInfo(int op, int idCeco, int numSucursal, String nombre, int cecoPadre, int pais, int estado, int municipio, String calle, int nivelC, int negocio, int canal, int responsable, String fccp, int geografia, String estado2,
            String municipio2, String colonia, String numExterior, String numInterior, int activo);

}
