package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.CecoPilaDTO;
import com.gruposalinas.franquicia.domain.ChecklistCompletoDTO;
import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.ChecklistPilaDTO;
import com.gruposalinas.franquicia.domain.CompromisoDTO;
import com.gruposalinas.franquicia.domain.Usuario_ADTO;
import com.gruposalinas.franquicia.mappers.CecoPilaRowMapper;
import com.gruposalinas.franquicia.mappers.ChecklistCompletoMapper;
import com.gruposalinas.franquicia.mappers.ChecklistGeneralRowMapper;
import com.gruposalinas.franquicia.mappers.ChecklistPilaRowMapper;
import com.gruposalinas.franquicia.mappers.ChecklistResumenRowMapper;
import com.gruposalinas.franquicia.mappers.ChecklistRowMapper;
import com.gruposalinas.franquicia.mappers.CompromisoSucursalRowMapper;
import com.gruposalinas.franquicia.mappers.CompromisoSucursalRowMapper2;
import com.gruposalinas.franquicia.mappers.DatosTiendaEstatusRowMapper;
import com.gruposalinas.franquicia.mappers.UsuarioSucursalRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecklistDAOImpl extends DefaultDAO implements ChecklistDAO {

    private static final Logger logger = LogManager.getLogger(ChecklistDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneChecklist;
    private DefaultJdbcCall jdbcBuscaChecklistUsuario;
    private DefaultJdbcCall jdbcCompromisoChecklist;
    private DefaultJdbcCall jdbcCompromisoCheckWeb;
    private DefaultJdbcCall jdbcBuscaChecklistUsuarioPrueba;
    private DefaultJdbcCall jdbcBuscaResumenCheck;
    private DefaultJdbcCall jdbcInsertaChecklist;
    private DefaultJdbcCall jdbcActualizaChecklist;
    private DefaultJdbcCall jdbcActualizaVigente;
    private DefaultJdbcCall jdbcActualizaFechaTermino;
    private DefaultJdbcCall jdbcEliminaChecklist;
    private DefaultJdbcCall jdbcBuscaChecklist;
    private DefaultJdbcCall jdbcBuscaTChecklist;
    private DefaultJdbcCall jdbcBuscaChecklists;
    private DefaultJdbcCall jdbcAsignaChecklist;
    private DefaultJdbcCall jdbcGeneraChecklist;
    private DefaultJdbcCall jdbcValidaCheckusua;
    private DefaultJdbcCall jdbcReportePila;

    public void init() {

        jdbcObtieneChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKLIST").withProcedureName("SP_BUSCA_CHECKLIST")
                .returningResultSet("RCL_CHECK", new ChecklistRowMapper());

        jdbcBuscaChecklistUsuarioPrueba = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate()).withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECKLIST2").withProcedureName("SP_BUSCA_CHECKLIST_N")
                .returningResultSet("RCL_CHECK", new ChecklistRowMapper());

        jdbcBuscaChecklistUsuario = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKLIST")
                .withProcedureName("SP_CHECK_COMPLETO")
                .returningResultSet("RCL_CHECKLIST", new ChecklistCompletoMapper())
                .returningResultSet("RCL_USUARIO_SUC", new UsuarioSucursalRowMapper());

        jdbcCompromisoChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKLIST")
                .withProcedureName("SP_COMP_CHECK")
                .returningResultSet("RCL_COMPROMISOS", new CompromisoSucursalRowMapper());

        jdbcCompromisoCheckWeb = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECKLIST2")
                .withProcedureName("SP_COMP_CHECK")
                .returningResultSet("RCL_COMP", new CompromisoSucursalRowMapper2());

        jdbcBuscaResumenCheck = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK")
                .withProcedureName("SP_RESUMEN_CK")
                .returningResultSet("RCL_CHECK", new ChecklistResumenRowMapper());

        jdbcInsertaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK")
                .withProcedureName("SP_INS_CHECK");

        jdbcActualizaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK")
                .withProcedureName("SP_ACT_CHECK");

        jdbcActualizaVigente = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK")
                .withProcedureName("SP_ACT_VIGENTE");

        jdbcEliminaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK")
                .withProcedureName("SP_DEL_CHECK");

        jdbcBuscaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK")
                .withProcedureName("SP_SEL_CHECK")
                .returningResultSet("RCL_CHECK", new ChecklistGeneralRowMapper());

        jdbcBuscaTChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK")
                .withProcedureName("SP_SEL_TCHECK")
                .returningResultSet("RCL_CHECK", new ChecklistGeneralRowMapper());

        jdbcBuscaChecklists = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_CHECK")
                .withProcedureName("SP_SEL_G_CK")
                .returningResultSet("RCL_CHECK", new ChecklistGeneralRowMapper());

        jdbcAsignaChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKLIST")
                .withProcedureName("SP_ASIGNA_CHECKS");

        jdbcGeneraChecklist = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAREGISTRACHECK")
                .withProcedureName("SP_REGISTRA_CHECK");

        jdbcActualizaFechaTermino = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKLIST")
                .withProcedureName("SP_FECHA_TERMINO");

        jdbcValidaCheckusua = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECKLIST2")
                .withProcedureName("SP_ESTATUS_CU")
                .returningResultSet("RCL_DATOS", new DatosTiendaEstatusRowMapper());

        jdbcReportePila = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_CHECKLIST")
                .withProcedureName("SP_REPORTE_PILA")
                .returningResultSet("RCL_CECOS", new CecoPilaRowMapper())
                .returningResultSet("RCL_CHECKLIST", new ChecklistPilaRowMapper());
    }

    @SuppressWarnings("unchecked")
    //public List<ChecklistDTO> buscaChecklistCompletoPrueba(int noUsuario, int idCheck, double latitud, double longitud) throws Exception {
    public List<ChecklistDTO> buscaChecklistCompletoPrueba(int noUsuario, int idCheck, String latitud, String longitud) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistDTO> listaChecklist = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", noUsuario).addValue("PA_IDCHECK", idCheck)
                .addValue("PA_LATITUD", latitud).addValue("PA_LONGITUD", longitud);

        out = jdbcBuscaChecklistUsuarioPrueba.execute(in);
        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_CHECKLIST.SP_BUSCA_CHECKLIST}");

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el Store Procedure");
        }

        return listaChecklist;
    }

    @SuppressWarnings("unchecked")
    public Map<String, Object> buscaChecklistCompleto(int idCheckUsua) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<ChecklistCompletoDTO> datosCheckUsuario = null;
        List<Usuario_ADTO> listaUsuarios = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUA_CHECK", idCheckUsua);
        out = jdbcBuscaChecklistUsuario.execute(in);

        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_CHECKLIST.SP_CHECK_COMPLETO}");

        datosCheckUsuario = (List<ChecklistCompletoDTO>) out.get("RCL_CHECKLIST");
        listaUsuarios = (List<Usuario_ADTO>) out.get("RCL_USUARIO_SUC");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el Store Procedure");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaUsuarios", listaUsuarios);
        lista.put("checklistCompleto", datosCheckUsuario);

        return lista;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistDTO> buscaChecklistActivos(int noUsuario, double latitud, double longitud) throws Exception {
        Map<String, Object> out = null;
        List<ChecklistDTO> listaChecklist = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", noUsuario)
                .addValue("PA_LATITUD", latitud).addValue("PA_LONGITUD", longitud);
        out = jdbcObtieneChecklist.execute(in);
        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_CHECKLIST.SP_BUSCA_CHECKLIST}");

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el Store Procedure");
        }

        return listaChecklist;
    }

    public int insertaChecklist(ChecklistDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        int idChecklist = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_TIPO_CHECK", bean.getIdTipoChecklist().getIdTipoCheck())
                .addValue("PA_FCNOMBRE", bean.getNombreCheck())
                .addValue("PA_IDHORARIO", bean.getIdHorario())
                .addValue("PA_VIGENTE", bean.getVigente())
                .addValue("PA_FECHA_I", bean.getFecha_inicio())
                .addValue("PA_FECHA_FIN", bean.getFecha_fin())
                .addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_DIA", bean.getDia())
                .addValue("PA_PERIODO", bean.getPeriodicidad())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_ORDEN_G", bean.getOrdeGrupo())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaChecklist.execute(in);

        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_ADM_CHECK.SP_INS_CHECK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        BigDecimal idreturn = (BigDecimal) out.get("PA_IDCHECKLIST");
        idChecklist = idreturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar Checklist");
        } else {
            return idChecklist;
        }

        return idChecklist;
    }

    public boolean actualizaChecklist(ChecklistDTO bean) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECK", bean.getIdChecklist())
                .addValue("PA_FIID_TIPO_CHECK", bean.getIdTipoChecklist().getIdTipoCheck())
                .addValue("PA_FCNOMBRE", bean.getNombreCheck())
                .addValue("PA_IDHORARIO", bean.getIdHorario())
                .addValue("PA_VIGENTE", bean.getVigente())
                .addValue("PA_FECHA_I", bean.getFecha_inicio())
                .addValue("PA_FECHA_FIN", bean.getFecha_fin())
                .addValue("PA_FIID_ESTADO", bean.getIdEstado())
                .addValue("PA_IDUSUARIO", bean.getIdUsuario())
                .addValue("PA_DIA", bean.getDia())
                .addValue("PA_PERIODO", bean.getPeriodicidad())
                .addValue("PA_VERSION", bean.getVersion())
                .addValue("PA_ORDEN_G", bean.getOrdeGrupo());

        out = jdbcActualizaChecklist.execute(in);

        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_ADM_CHECK.SP_ACT_CHECK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar Checklist");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaChecklist(int idCheckList) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECK", idCheckList);

        out = jdbcEliminaChecklist.execute(in);

        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_ADM_CHECK.SP_DEL_CHECK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar Checklist el id: " + idCheckList);
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistDTO> buscaChecklist() throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistDTO> listaChecklist = null;

        out = jdbcBuscaChecklists.execute();

        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_ADM_CHECK.SP_SEL_G_CK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");

        if (error == 1) {
            logger.info("Algo ocurrió al consultar los Checklist");
        } else {
            return listaChecklist;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistDTO> buscaChecklist(int idChecklist) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistDTO> listaChecklist = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECK", idChecklist);

        out = jdbcBuscaChecklist.execute(in);

        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_ADM_CHECK.SP_SEL_CHECK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");

        if (error == 1) {
            logger.info("Algo ocurrió al consultar el Checklist id: " + idChecklist);
        } else {
            return listaChecklist;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    public List<ChecklistDTO> buscaTChecklist(int idTipoCheck) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<ChecklistDTO> listaChecklist = null;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_TCHECK", idTipoCheck);

        out = jdbcBuscaTChecklist.execute(in);

        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_ADM_CHECK.SP_SEL_TCHECK}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");

        if (error == 1) {
            logger.info("Algo ocurrió al consultar el Checklist id: " + idTipoCheck);
        } else {
            return listaChecklist;
        }

        return null;
    }

    public boolean asignaChecklist(String ceco, int puesto, int idChecklist) {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CECO", ceco)
                .addValue("PA_PUESTO", puesto)
                .addValue("PA_IDCHECK", idChecklist);

        out = jdbcAsignaChecklist.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_CHECK.SP_ASIGNA_CHECKS}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al asignar los checklist");
        } else {
            return true;
        }

        return false;
    }

    public boolean registraChecklist(String checklist, String preguntas, String arbolDesiciones) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_CHECKLIST", checklist)
                .addValue("PA_PREGUNTAS", preguntas)
                .addValue("PA_ARBOL_DES", arbolDesiciones);

        out = jdbcGeneraChecklist.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PA_CHECKLIST.SP_REGISTRA_CHECK}");

        BigDecimal resultado = (BigDecimal) out.get("PA_EJECUCION");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al crear el Checklist Completo");
        } else {
            return true;
        }

        return false;
    }

    /*
	@Override
	public List<ChecklistDTO> buscaResumenCheck(int idUsuario) throws Exception {
		Map<String, Object> out = null;
		List<ChecklistDTO> listaChecklist = null;
		int respuesta = 0;

		  SqlParameterSource in = new MapSqlParameterSource().addValue("PA_USUARIO", idUsuario) ;
			out = jdbcBuscaResumenCheck.execute(in);

			logger.info("Funciï¿½n ejecutada:{FRANQUICIA.PA_CHECKLIST.SP_RESUMEN_CK}");

			listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");

			BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
			respuesta = respuestaEjec.intValue();

			if (respuesta == 1) {
				throw new Exception("El Store Procedure retorno ERRROR");
			}

		return listaChecklist;
	}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<ChecklistDTO> buscaResumenCheck() throws Exception {
        Map<String, Object> out = null;
        List<ChecklistDTO> listaChecklist = null;
        int respuesta = 0;

        out = jdbcBuscaResumenCheck.execute();

        logger.info("Funciï¿½n ejecutada:{FRANQUICIA.PA_CHECKLIST.SP_RESUMEN_CK}");

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_ERROR");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el SP");
        }

        return listaChecklist;
    }

    @Override
    public boolean actualizaCheckVigente(int idCheck, int estatus) throws Exception {

        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FIID_CHECK", idCheck)
                .addValue("PA_VIGENTE", estatus);

        out = jdbcActualizaVigente.execute(in);

        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_ADM_CHECK.SP_ACT_VIGENTE}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar el estado del Checklist");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaFechatermino(String fechaTermino, int idBitacora) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FDTERMINO", fechaTermino)
                .addValue("PA_IDBITACORA", idBitacora);

        out = jdbcActualizaFechaTermino.execute(in);

        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_CHECKLIST.SP_FECHA_TERMINO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_ERROR");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar la fecha termino de la bitacora " + idBitacora);
        } else {
            return true;
        }

        return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<CompromisoDTO> compromisosChecklist(int idChecklist, int idSucursal) throws Exception {
        Map<String, Object> out = null;
        List<CompromisoDTO> listaCompromiso = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDCHECK", idChecklist).addValue("PA_IDCECO", idSucursal);

        out = jdbcCompromisoChecklist.execute(in);

        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_CHECKLIST.SP_COMP_CHECK}");

        listaCompromiso = (List<CompromisoDTO>) out.get("RCL_COMPROMISOS");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el SP");
        }

        return listaCompromiso;
    }

    @SuppressWarnings("unchecked")
    public List<CompromisoDTO> compromisosChecklistWeb(int idBitacora) throws Exception {
        Map<String, Object> out = null;
        int respuesta = 0;
        List<CompromisoDTO> listaCompromiso = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_BITACORA", idBitacora);
        out = jdbcCompromisoCheckWeb.execute(in);

        logger.info("Funciï¿½n ejecutada:{FRANQUICIA.PACHECKLIST2.SP_COMP_CHECK}");

        listaCompromiso = (List<CompromisoDTO>) out.get("RCL_COMP");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");
        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            logger.info("Algo ocurrió al consultar los compromisos");
        } else {
            return listaCompromiso;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    //public List<ChecklistDTO> validaCheckUsua (int checkUsua,  double latitud, double longitud) throws Exception{
    public List<ChecklistDTO> validaCheckUsua(int checkUsua, String latitud, String longitud) throws Exception {
        Map<String, Object> out = null;
        int ejecucion = 0;
        List<ChecklistDTO> datos = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_CHECKUSUA", checkUsua).addValue("PA_LATITUD", latitud).addValue("PA_LONGITUD", longitud);

        out = jdbcValidaCheckusua.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PACHECKLIST2.SP_ESTATUS_CU}");

        datos = (List<ChecklistDTO>) out.get("RCL_DATOS");

        BigDecimal returnEjecucion = (BigDecimal) out.get("PA_EJECUCION");
        ejecucion = returnEjecucion.intValue();

        if (ejecucion == 0) {
            logger.info("Algo ocurrió al validar el checkusua");
            return null;
        }

        return datos;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> ReportePila(int idUsuario) throws Exception {
        Map<String, Object> out = null;
        Map<String, Object> lista = null;
        List<CecoPilaDTO> listaCecos = null;
        List<ChecklistPilaDTO> listaChecklist = null;
        int respuesta = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario);
        out = jdbcReportePila.execute(in);

        logger.info("Funci�n ejecutada:{FRANQUICIA.PA_CHECKLIST.SP_REPORTE_PILA}");

        listaCecos = (List<CecoPilaDTO>) out.get("RCL_CECOS");
        listaChecklist = (List<ChecklistPilaDTO>) out.get("RCL_CHECKLIST");

        BigDecimal respuestaEjec = (BigDecimal) out.get("PA_EJECUCION");

        respuesta = respuestaEjec.intValue();

        if (respuesta == 1) {
            throw new Exception("Algo ocurrió en el Store Procedure");
        }

        lista = new HashMap<String, Object>();
        lista.put("listaCecos", listaCecos);
        lista.put("listaChecklist", listaChecklist);

        return lista;
    }

}
