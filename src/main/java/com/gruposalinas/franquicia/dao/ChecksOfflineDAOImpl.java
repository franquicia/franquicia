package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.ArbolDecisionDTO;
import com.gruposalinas.franquicia.domain.BitacoraDTO;
import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.ChecklistPreguntaDTO;
import com.gruposalinas.franquicia.domain.ChecklistUsuarioDTO;
import com.gruposalinas.franquicia.domain.HorarioDTO;
import com.gruposalinas.franquicia.domain.ModuloDTO;
import com.gruposalinas.franquicia.domain.TipoArchivoDTO;
import com.gruposalinas.franquicia.domain.TipoChecklistDTO;
import com.gruposalinas.franquicia.domain.TipoPreguntaDTO;
import com.gruposalinas.franquicia.domain.Usuario_ADTO;
import com.gruposalinas.franquicia.mappers.ArbolDecisionRowMapper;
import com.gruposalinas.franquicia.mappers.BitacoraOfflineRowMapper;
import com.gruposalinas.franquicia.mappers.CheckPregOfflineRowMapper;
import com.gruposalinas.franquicia.mappers.CheckUOfflineRowMapper;
import com.gruposalinas.franquicia.mappers.CheckUsuaOffRowMapper;
import com.gruposalinas.franquicia.mappers.ChecklistOfflineRowMapper;
import com.gruposalinas.franquicia.mappers.ChecksOfflineCompletosRowMapper;
import com.gruposalinas.franquicia.mappers.ChecksOfflineRowMapper;
import com.gruposalinas.franquicia.mappers.HorarioRowMapper;
import com.gruposalinas.franquicia.mappers.ModuloOfflineRowMapper;
import com.gruposalinas.franquicia.mappers.TipoArchivoRowMapper;
import com.gruposalinas.franquicia.mappers.TipoChecklistRowMapper;
import com.gruposalinas.franquicia.mappers.TipoPreguntaRowMapper;
import com.gruposalinas.franquicia.mappers.UsuarioSucOfflineRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class ChecksOfflineDAOImpl extends DefaultDAO implements ChecksOfflineDAO {

    private static Logger logger = LogManager.getLogger(ChecklistDAOImpl.class);

    private DefaultJdbcCall jdbcObtieneChecks;
    private DefaultJdbcCall jdbcObtieneChecksNuevo;

    public void init() {

        jdbcObtieneChecks = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PACHECKOFFLINE")
                .withProcedureName("SPOBTIENECHECKS")
                .returningResultSet("RCL_CHECKS", new ChecksOfflineRowMapper()).
                returningResultSet("RCL_COMPLETOS", new ChecksOfflineCompletosRowMapper())
                .returningResultSet("RCL_USUARIO_SUC", new UsuarioSucOfflineRowMapper());

        jdbcObtieneChecksNuevo = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PAOFFLINE_NV")
                .withProcedureName("SPOBTIENECHECKS_N")
                .returningResultSet("RCL_CHECK", new ChecklistOfflineRowMapper())
                .returningResultSet("RCL_TIPOCHECK", new TipoChecklistRowMapper())
                .returningResultSet("RCL_HORARIOS", new HorarioRowMapper())
                .returningResultSet("RCL_CHECK_USUA", new CheckUsuaOffRowMapper())
                .returningResultSet("RCL_MODULOS", new ModuloOfflineRowMapper())
                .returningResultSet("RCL_TIPO_PREG", new TipoPreguntaRowMapper())
                .returningResultSet("RCL_CHECK_PREG", new CheckPregOfflineRowMapper())
                .returningResultSet("RCL_ARBOL", new ArbolDecisionRowMapper())
                .returningResultSet("RCL_BITACORA", new BitacoraOfflineRowMapper())
                .returningResultSet("RCL_USUARIO_SUC", new UsuarioSucOfflineRowMapper())
                .returningResultSet("RCL_TIPO_AR", new TipoArchivoRowMapper())
                .returningResultSet("RCL_CHECK_IN", new CheckUOfflineRowMapper());
    }

    @Override
    public Map<String, Object> obtieneChecks(int idUsuario) throws Exception {

        Map<String, Object> out = null;

        Map<String, Object> listas = null;

        int ejecuccion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario);

        out = jdbcObtieneChecks.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PACHECKOFFLINE.SPOBTIENECHECKS}");

        BigDecimal valueReturn = (BigDecimal) out.get("PA_EJECUCCION");
        ejecuccion = valueReturn.intValue();

        if (ejecuccion == 0) {
            logger.info("Algo ocurrió al obtener los checklist para modo Offline ");
        } else {
            listas = new HashMap<String, Object>();

            listas.put("checklists", out.get("RCL_CHECKS"));
            listas.put("checklistsCompletos", out.get("RCL_COMPLETOS"));
            listas.put("listaUsuarios", out.get("RCL_USUARIO_SUC"));
        }

        return listas;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> obtieneChecksNuevo(int idUsuario) throws Exception {

        Map<String, Object> out = null;

        Map<String, Object> listas = null;
        List<ChecklistDTO> listaChecklist = null;
        List<TipoChecklistDTO> ListaTipoCheck = null;
        List<HorarioDTO> ListaHorario = null;
        List<ChecklistUsuarioDTO> ListaCheckUsuarios = null;
        List<ModuloDTO> listaModulos = null;
        List<TipoPreguntaDTO> listaTipoPregunta = null;
        List<ChecklistPreguntaDTO> listaCheckPregunta = null;
        List<ArbolDecisionDTO> listaArbol = null;
        List<BitacoraDTO> listaBitacora = null;
        List<Usuario_ADTO> listaUsuarios = null;
        List<TipoArchivoDTO> listaTipoArchivo = null;
        List<ChecklistUsuarioDTO> listaCheckUsuaInactivos = null;

        int ejecuccion = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDUSUARIO", idUsuario);

        logger.info("");
        out = jdbcObtieneChecksNuevo.execute(in);

        logger.info("Funcion ejecutada: {FRANQUICIA.PAOFFLINE_NV.SPOBTIENECHECKS_N}");

        listaChecklist = (List<ChecklistDTO>) out.get("RCL_CHECK");
        ListaTipoCheck = (List<TipoChecklistDTO>) out.get("RCL_TIPOCHECK");
        ListaHorario = (List<HorarioDTO>) out.get("RCL_HORARIOS");
        ListaCheckUsuarios = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_USUA");
        listaModulos = (List<ModuloDTO>) out.get("RCL_MODULOS");
        listaTipoPregunta = (List<TipoPreguntaDTO>) out.get("RCL_TIPO_PREG");
        listaCheckPregunta = (List<ChecklistPreguntaDTO>) out.get("RCL_CHECK_PREG");
        listaArbol = (List<ArbolDecisionDTO>) out.get("RCL_ARBOL");
        listaBitacora = (List<BitacoraDTO>) out.get("RCL_BITACORA");
        listaUsuarios = (List<Usuario_ADTO>) out.get("RCL_USUARIO_SUC");
        listaTipoArchivo = (List<TipoArchivoDTO>) out.get("RCL_TIPO_AR");
        listaCheckUsuaInactivos = (List<ChecklistUsuarioDTO>) out.get("RCL_CHECK_IN");

        BigDecimal valueReturn = (BigDecimal) out.get("PA_EJECUCCION");
        ejecuccion = valueReturn.intValue();

        if (ejecuccion == 0) {
            logger.info("Algo ocurrió al obtener los checklist para modo Offline ");
        } else {
            listas = new HashMap<String, Object>();

            listas.put("listaChecklist", listaChecklist);
            listas.put("ListaTipoCheck", ListaTipoCheck);
            listas.put("ListaHorario", ListaHorario);
            listas.put("ListaCheckUsuarios", ListaCheckUsuarios);
            listas.put("listaModulos", listaModulos);
            listas.put("listaTipoPregunta", listaTipoPregunta);
            listas.put("listaCheckPregunta", listaCheckPregunta);
            listas.put("listaArbol", listaArbol);
            listas.put("listaBitacora", listaBitacora);
            listas.put("listaUsuarios", listaUsuarios);
            listas.put("listaTipoArchivo", listaTipoArchivo);
            listas.put("listaCheckUsuaInactivos", listaCheckUsuaInactivos);
        }

        return listas;
    }

}
