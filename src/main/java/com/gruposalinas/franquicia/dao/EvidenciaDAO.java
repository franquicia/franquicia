package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.EvidenciaDTO;
import java.util.List;

public interface EvidenciaDAO {

    public List<EvidenciaDTO> obtieneEvidencia() throws Exception;

    public List<EvidenciaDTO> obtieneEvidencia(int idEvidencia) throws Exception;

    public int insertaEvidencia(EvidenciaDTO bean) throws Exception;

    public boolean actualizaEvidencia(EvidenciaDTO bean) throws Exception;

    public boolean eliminaEvidencia(int idEvidencia) throws Exception;

}
