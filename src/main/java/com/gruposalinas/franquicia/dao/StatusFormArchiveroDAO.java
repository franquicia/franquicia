package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.StatusFormArchiveroDTO;
import java.util.List;

public interface StatusFormArchiveroDAO {

    public List<StatusFormArchiveroDTO> obtieneStatusFormArchiveros(int idStatus) throws Exception;

    public boolean insertaStatusFormarchivero(StatusFormArchiveroDTO bean) throws Exception;

    public boolean modificaStatusFormarchivero(StatusFormArchiveroDTO bean) throws Exception;

    public boolean eliminaStatusFormarchivero(int idStatus) throws Exception;
}
