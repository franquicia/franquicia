package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PreguntaDTO;
import com.gruposalinas.franquicia.mappers.PreguntaRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

public class PreguntaDAOImpl extends DefaultDAO implements PreguntaDAO {

    private static Logger logger = LogManager.getLogger(PreguntaDTO.class);

    DefaultJdbcCall jdbcObtieneTodos;
    DefaultJdbcCall jdbcObtienePregunta;
    DefaultJdbcCall jdbcInsertaPregunta;
    DefaultJdbcCall jdbcActualizaPregunta;
    DefaultJdbcCall jdbcEliminaPregunta;

    public void init() {

        jdbcObtieneTodos = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_SEL_G_PREG")
                .returningResultSet("RCL_PREG", new PreguntaRowMapper());

        jdbcObtienePregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_SEL_PREG")
                .returningResultSet("RCL_PREG", new PreguntaRowMapper());

        jdbcInsertaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_INS_PREG");

        jdbcActualizaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_ACT_PREG");

        jdbcEliminaPregunta = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PREG")
                .withProcedureName("SP_DEL_PREG");
    }

    @SuppressWarnings("unchecked")
    public List<PreguntaDTO> obtienePregunta() throws Exception {
        Map<String, Object> out = null;
        List<PreguntaDTO> listaPregunta = null;
        int error = 0;

        out = jdbcObtieneTodos.execute();

        logger.info("Funci�n ejecutada: {FRANQUICIA.PA_ADM_PREG.SP_SEL_G_PREG}");

        listaPregunta = (List<PreguntaDTO>) out.get("RCL_PREG");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener las Preguntas");
        } else {
            return listaPregunta;
        }

        return listaPregunta;
    }

    @SuppressWarnings("unchecked")
    public List<PreguntaDTO> obtienePregunta(int idPreg) throws Exception {

        Map<String, Object> out = null;
        int error = 0;
        List<PreguntaDTO> listaPregunta = null;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", idPreg);

        out = jdbcObtienePregunta.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PA_ADM_PREG.SP_SEL_PREG}");

        listaPregunta = (List<PreguntaDTO>) out.get("RCL_PREG");
        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al obtener las Preguntas");
        } else {
            return listaPregunta;
        }

        return listaPregunta;

    }

    public int insertaPregunta(PreguntaDTO bean) throws Exception {
        Map<String, Object> out = null;

        int error = 0;
        int idPreg = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_DESCRIPCION", bean.getPregunta())
                .addValue("PA_COMMIT", bean.getCommit());

        out = jdbcInsertaPregunta.execute(in);

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        BigDecimal idPregReturn = (BigDecimal) out.get("PA_IDPREGUNTA");
        idPreg = idPregReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al insertar la Pregunta");
        } else {
            return idPreg;
        }

        return idPreg;

    }

    public boolean actualizaPregunta(PreguntaDTO bean) throws Exception {

        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", bean.getIdPregunta())
                .addValue("PA_IDMODULO", bean.getIdModulo())
                .addValue("PA_IDT_PREG", bean.getIdTipo())
                .addValue("PA_ESTATUS", bean.getEstatus())
                .addValue("PA_DESCRIPCION", bean.getPregunta());

        out = jdbcActualizaPregunta.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PA_ADM_PREG.SP_ACT_PREG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al actualizar la Pregunta id( " + bean.getIdPregunta() + ")");
        } else {
            return true;
        }

        return false;
    }

    public boolean eliminaPregunta(int idPreg) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FIID_PREG", idPreg);

        out = jdbcEliminaPregunta.execute(in);

        logger.info("Funci�n ejecutada: {FRANQUICIA.PA_ADM_PREG.SP_DEL_PREG}");

        BigDecimal resultado = (BigDecimal) out.get("PA_ERROR");
        error = resultado.intValue();

        if (error == 1) {
            logger.info("Algo ocurrió al borrar la pregunta id(" + idPreg + ")");
        } else {
            return true;
        }

        return false;

    }

}
