/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.CecoPasoDTO;
import com.gruposalinas.franquicia.domain.ListaTablaEKT;
import com.gruposalinas.franquicia.mappers.CecoWSRowMapper;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
 *
 * @author kramireza
 */
public class CecoWSDAOImpl extends DefaultDAO implements CecoWSDAO {

    private static Logger logger = LogManager.getLogger(CecoDAOImpl.class);

    DefaultJdbcCall jdbcConsultaCeco;
    DefaultJdbcCall jdbcInsertaCeco;
    DefaultJdbcCall jdbcActualizaCeco;
    DefaultJdbcCall jdbcDepuraTabla;

    public void init() {

        jdbcConsultaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PASO_CECO")
                .withProcedureName("SP_SEL_CCPASO")
                .returningResultSet("RCL_INFO", new CecoWSRowMapper());

        jdbcInsertaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PASO_CECO")
                .withProcedureName("SP_INS_CCPASO");

        jdbcActualizaCeco = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PASO_CECO")
                .withProcedureName("SP_ACT_CCPASO");

        jdbcDepuraTabla = (DefaultJdbcCall) new DefaultJdbcCall(getFrqJdbcTemplate())
                .withSchemaName("FRANQUICIA")
                .withCatalogName("PA_ADM_PASO_CECO")
                .withProcedureName("SP_DEP_TABLA");

    }

    @Override
    public List<CecoPasoDTO> buscaCeco(String idCeco) throws Exception {
        Map<String, Object> out = null;
        int error = 0;
        List<CecoPasoDTO> listaCeco = null;
        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_FCCCID", idCeco);
        out = jdbcConsultaCeco.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_PASO_CECO.SP_SEL_CCPASO}");

        listaCeco = (List<CecoPasoDTO>) out.get("RCL_INFO");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrio al consultar los CECO PASO");
        } else {
            return listaCeco;
        }

        return null;
    }

    @Override
    public boolean insertaCeco(ListaTablaEKT bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCCCID", bean.getIdCC())
                .addValue("PA_FIENTIDADID", bean.getIdEntidad())
                .addValue("PA_FINUM_ECONOMICO", bean.getNumEconomico())
                .addValue("PA_FCNOMBRECC", bean.getNomCC())
                .addValue("PA_FCNOMBRE_ENT", bean.getNomEntidad())
                .addValue("PA_FCCCID_PADRE", bean.getIdCCPa())
                .addValue("PA_FIENTDID_PADRE", bean.getIdEntidadPa())
                .addValue("PA_FCNOMBRECC_PAD", bean.getNomCCPa())
                .addValue("PA_FCTIPOCANAL", bean.getIdCanal())
                .addValue("PA_FCNOMBTIPCANAL", bean.getTipoCanal())
                .addValue("PA_FCCANAL", bean.getNomCanal())
                .addValue("PA_FISTATUSCCID", bean.getIdEstatus())
                .addValue("PA_FCSTATUSCC", bean.getNomEstatus())
                .addValue("PA_FITIPOCC", bean.getIdTipoCentro())
                .addValue("PA_FIESTADOID", bean.getIdEstado())
                .addValue("PA_FCMUNICIPIO", bean.getNomMunicipio())
                .addValue("PA_FCTIPOOPERACION", bean.getIdTipoOperacion())
                .addValue("PA_FCTIPOSUCURSAL", bean.getIdTipoSucursal())
                .addValue("PA_FCNOMBRETIPOSUC", bean.getNomTipoSucursal())
                .addValue("PA_FCCALLE", bean.getNomCalle())
                .addValue("PA_FIRESPONSABLEID", bean.getIdResponsable())
                .addValue("PA_FCRESPONSABLE", bean.getNomResponsable())
                .addValue("PA_FCCP", bean.getCodigoPostal())
                .addValue("PA_FCTELEFONOS", bean.getTelefono())
                .addValue("PA_FICLASIF_SIEID", bean.getIdEstructuracve())
                .addValue("PA_FCCLASIF_SIE", bean.getDesEstructuracve())
                .addValue("PA_FDAPERTURA", bean.getFechaApertura())
                .addValue("PA_FDCIERR", bean.getFechaCierre())
                .addValue("PA_FIPAISID", bean.getIdPais())
                .addValue("PA_FCPAIS", bean.getNomPais())
                .addValue("PA_FDCARGA", bean.getFechaCreacion())
                .addValue("PA_FIIDUNNEGO", bean.getIdUnidadNegocio())
                .addValue("PA_FCDESCUNBNEGO", bean.getNomunidadnegocio())
                .addValue("PA_FIIDSUBNEGO", bean.getIdSubnegocio())
                .addValue("PA_FCDESCSUBNEGO", bean.getDesSubnegocio());

        out = jdbcInsertaCeco.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_PASO_CECO.SP_INS_CECO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 0) {
            logger.info("Algo ocurri� al insertar CECO");
        } else if (error == 3) {
            logger.info("No se pudo borrar la tabla de PASO CECO");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean actualizaCeco(ListaTablaEKT bean) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource()
                .addValue("PA_FCCCID", bean.getIdCC())
                .addValue("PA_FIENTIDADID", bean.getIdEntidad())
                .addValue("PA_FINUM_ECONOMICO", bean.getNumEconomico())
                .addValue("PA_FCNOMBRECC", bean.getNomCC())
                .addValue("PA_FCNOMBRE_ENT", bean.getNomEntidad())
                .addValue("PA_FCCCID_PADRE", bean.getIdCCPa())
                .addValue("PA_FIENTDID_PADRE", bean.getIdEntidadPa())
                .addValue("PA_FCNOMBRECC_PAD", bean.getNomCCPa())
                .addValue("PA_FCTIPOCANAL", bean.getIdCanal())
                .addValue("PA_FCNOMBTIPCANAL", bean.getTipoCanal())
                .addValue("PA_FCCANAL", bean.getNomCanal())
                .addValue("PA_FISTATUSCCID", bean.getIdEstatus())
                .addValue("PA_FCSTATUSCC", bean.getNomEstatus())
                .addValue("PA_FITIPOCC", bean.getIdTipoCentro())
                .addValue("PA_FIESTADOID", bean.getIdEstado())
                .addValue("PA_FCMUNICIPIO", bean.getNomMunicipio())
                .addValue("PA_FCTIPOOPERACION", bean.getIdTipoOperacion())
                .addValue("PA_FCTIPOSUCURSAL", bean.getIdTipoSucursal())
                .addValue("PA_FCNOMBRETIPOSUC", bean.getNomTipoSucursal())
                .addValue("PA_FCCALLE", bean.getNomCalle())
                .addValue("PA_FIRESPONSABLEID", bean.getIdResponsable())
                .addValue("PA_FCRESPONSABLE", bean.getNomResponsable())
                .addValue("PA_FCCP", bean.getCodigoPostal())
                .addValue("PA_FCTELEFONOS", bean.getTelefono())
                .addValue("PA_FICLASIF_SIEID", bean.getIdEstructuracve())
                .addValue("PA_FCCLASIF_SIE", bean.getDesEstructuracve())
                .addValue("PA_FDAPERTURA", bean.getFechaApertura())
                .addValue("PA_FDCIERR", bean.getFechaCierre())
                .addValue("PA_FIPAISID", bean.getIdPais())
                .addValue("PA_FCPAIS", bean.getNomPais())
                .addValue("PA_FDCARGA", bean.getFechaCreacion())
                .addValue("PA_FIIDUNNEGO", bean.getIdUnidadNegocio())
                .addValue("PA_FCDESCUNBNEGO", bean.getNomunidadnegocio())
                .addValue("PA_FIIDSUBNEGO", bean.getIdSubnegocio())
                .addValue("PA_FCDESCSUBNEGO", bean.getDesSubnegocio());

        out = jdbcActualizaCeco.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_PASO_CECO.SP_ACT_CCPASO}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            logger.info("Algo ocurrio al actualizar el CECO PASO");
        } else {
            return true;
        }

        return false;
    }

    @Override
    public boolean depuraTabla(String nombreTabla) throws Exception {
        Map<String, Object> out = null;

        int error = 0;

        SqlParameterSource in = new MapSqlParameterSource().addValue("PA_NOMBTABLA", nombreTabla);

        out = jdbcDepuraTabla.execute(in);

        logger.info("Funcion ejecutada:{FRANQUICIA.PA_ADM_PASO_CECO.SP_DEP_TABLA}");

        BigDecimal errorReturn = (BigDecimal) out.get("PA_EJECUCION");
        error = errorReturn.intValue();

        if (error == 1) {
            return true;

        } else {
            logger.info("Algo ocurrio al depurar la tabla de CECO PASO");
            return false;
        }
    }

}
