package com.gruposalinas.franquicia.dao;

import com.gruposalinas.franquicia.domain.PaisNegocioDTO;
import java.util.List;

public interface PaisNegocioDAO {

    public List<PaisNegocioDTO> obtienePaisNegocio(String negocio, String pais) throws Exception;

    public boolean insertaPaisNegocio(int negocio, int pais) throws Exception;

    public boolean eliminaPaisNegocio(int negocio, int pais) throws Exception;

}
