package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.IndicadoresDataEGDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class IndicadoresDataEGRowMapperPD implements RowMapper<IndicadoresDataEGDTO> {

    @Override
    public IndicadoresDataEGDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        IndicadoresDataEGDTO data = new IndicadoresDataEGDTO();

        try {
            data.setTotalInstalaciones(rs.getInt("TOTAL_INSTAL"));
        } catch (Exception e) {
            data.setTotalInstalaciones(0);
        }

        try {
            data.setTotalTabletas(rs.getInt("TOTAL_TABLET"));
        } catch (Exception e) {
            data.setTotalTabletas(0);
        }

        try {
            data.setUltimaInstalacion(rs.getString("ULT_INSTAL"));
        } catch (Exception e) {
            data.setUltimaInstalacion(null);
        }

        try {
            data.setPendientes(rs.getInt("PENDIENTES"));
        } catch (Exception e) {
            data.setPendientes(0);
        }

        return data;
    }
}
