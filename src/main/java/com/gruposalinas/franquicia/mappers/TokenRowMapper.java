package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TokenDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TokenRowMapper implements RowMapper<TokenDTO> {

    public TokenDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TokenDTO tokenDTO = new TokenDTO();

        tokenDTO.setIdToken(rs.getInt("FIID_TOKEN"));
        tokenDTO.setUsuario(rs.getInt("FIID_USUARIO"));
        tokenDTO.setToken(rs.getString("FCTOKEN"));
        tokenDTO.setEstatus(rs.getInt("FIESTATUS"));

        return tokenDTO;
    }

}
