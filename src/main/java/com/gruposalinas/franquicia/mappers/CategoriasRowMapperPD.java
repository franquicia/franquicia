package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CategoriasPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CategoriasRowMapperPD implements RowMapper<CategoriasPDDTO> {

    @Override
    public CategoriasPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CategoriasPDDTO lista = new CategoriasPDDTO();
        lista.setIdCategoria(rs.getString("FIID_CATEGORIA"));
        lista.setDescripcion(rs.getString("FCDESCRIPCION"));
        lista.setRutaIcono(rs.getString("FCRUTA_ICONO"));
        lista.setNombreIcono(rs.getString("FCNOMBRE_ICONO"));
        lista.setUsuarioModifico(rs.getString("FCUSUARIO_MODIF"));
        lista.setFechaModifico(rs.getString("FDFECHA_MODIF"));
        lista.setOrden(rs.getInt("FIORDEN"));

        return lista;
    }
}
