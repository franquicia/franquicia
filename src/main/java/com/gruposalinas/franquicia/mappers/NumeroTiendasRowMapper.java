package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.NumeroTiendasDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class NumeroTiendasRowMapper implements RowMapper<NumeroTiendasDTO> {

    @Override
    public NumeroTiendasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        NumeroTiendasDTO numeroTiendasDTO = new NumeroTiendasDTO();

        numeroTiendasDTO.setCeco(rs.getString("FCID_CECO"));
        numeroTiendasDTO.setNombreCeco(rs.getString("FCNOMBRE"));
        numeroTiendasDTO.setNumTiendas(rs.getInt("TIENDAS"));

        return numeroTiendasDTO;
    }

}
