package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.PosiblesTipoPreguntaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PosibleTipoPreguntaRowMapper implements RowMapper<PosiblesTipoPreguntaDTO> {

    @Override
    public PosiblesTipoPreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PosiblesTipoPreguntaDTO posibleTipoPregunta = new PosiblesTipoPreguntaDTO();

        posibleTipoPregunta.setIdPosibleTipoPregunta(rs.getInt("FIIDPOSIPREG"));
        posibleTipoPregunta.setIdTipoPregunta(rs.getInt("FIID_TIPO_PREG"));
        posibleTipoPregunta.setIdPosibleRespuesta(rs.getInt("FIIDPOSIBLE"));
        posibleTipoPregunta.setDescripcionPosible(rs.getString("FCDESCRIPCION"));

        return posibleTipoPregunta;
    }

}
