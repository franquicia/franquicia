package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.AdmonTabletPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AdmonTabletRowMapperPD implements RowMapper<AdmonTabletPDDTO> {

    @Override
    public AdmonTabletPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        // TODO Auto-generated method stub
        AdmonTabletPDDTO admonTablet = new AdmonTabletPDDTO();

        admonTablet.setIdAdmonTable(rs.getInt("FIID_ADMON_TAB"));
        admonTablet.setIdArchivo(rs.getInt("FIID_ARCH_CARGA"));
        admonTablet.setEtapa(rs.getInt("FIETAPA"));
        admonTablet.setNumEconomico(rs.getInt("FINUM_ECONOMICO"));
        admonTablet.setDescribeSucursal(rs.getString("FCDESCRIBE_SUC"));
        admonTablet.setNumSerieTablet(rs.getString("FCNUM_SERIE_TAB"));
        admonTablet.setFechaInstalacion(rs.getString("FDFECHA_INSTALA"));
        admonTablet.setDetectaFalla(rs.getString("FCDETECTA_FALLA"));
        admonTablet.setCursor(rs.getString("FCUSR"));
        admonTablet.setFechaCambio(rs.getString("FDFECHA_CAMBIO"));
        admonTablet.setActivo(rs.getInt("FIACTIVO"));
        admonTablet.setFechaFalla(rs.getString("FDFECHA_FALLA"));
        admonTablet.setFolioFalla(rs.getInt("FIFOLIO_FALLA"));
        admonTablet.setFechaCorrige(rs.getString("FDFECHA_CORRIGE"));
        admonTablet.setResponsable(rs.getString("FCRESPONSABLE_SP"));

        return admonTablet;
    }
}
