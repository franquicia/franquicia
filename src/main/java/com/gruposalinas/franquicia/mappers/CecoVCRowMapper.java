package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CompromisoCecoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CecoVCRowMapper implements RowMapper<CompromisoCecoDTO> {

    public CompromisoCecoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CompromisoCecoDTO cecoDTO = new CompromisoCecoDTO();

        cecoDTO.setIdCeco(rs.getString("FCID_CECO"));
        cecoDTO.setDescCeco(rs.getString("FCNOMBRE"));
        cecoDTO.setTotal(rs.getInt("CONTEO"));
        cecoDTO.setIdChecklist(rs.getString("FIID_CHECKLIST"));
        cecoDTO.setNombreChecklist(rs.getString("NOSUCURSAL"));
        cecoDTO.setAsignados(rs.getInt("ASIGNADOS"));
        cecoDTO.setTerminados(rs.getInt("TERMINADOS"));

        return cecoDTO;
    }

}
