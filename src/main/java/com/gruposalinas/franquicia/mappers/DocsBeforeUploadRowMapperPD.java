package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.DocsBeforeUploadPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class DocsBeforeUploadRowMapperPD implements RowMapper<DocsBeforeUploadPDDTO> {

    @Override
    public DocsBeforeUploadPDDTO mapRow(ResultSet rs, int arg1) throws SQLException {
        DocsBeforeUploadPDDTO docs = new DocsBeforeUploadPDDTO();
        docs.setFiid_documento(rs.getInt("FIID_DOCUMENTO"));
        docs.setFifolio(rs.getInt("FIFOLIO"));
        docs.setFcnombre_arc(rs.getString("FCNOMBRE_ARC"));
        docs.setFiid_tipo_docto(rs.getInt("FIID_TIPO_DOCTO"));
        docs.setFcnombre_docto(rs.getString("FCNOMBRE_DOCTO"));
        docs.setId_cagtegoria(rs.getInt("ID_CAGTEGORIA"));
        docs.setDesc_categoria(rs.getString("DESC_CATEGORIA"));
        docs.setFiid_usuario(rs.getInt("FIID_USUARIO"));
        docs.setFcorigen(rs.getString("FCORIGEN"));
        docs.setFdvigencia_ini(rs.getString("FDVIGENCIA_INI"));
        docs.setFdvigencia_fin(rs.getString("FDVIGENCIA_FIN"));
        docs.setFcvisible_suc(rs.getString("FCVISIBLE_SUC"));
        docs.setFiid_estatus(rs.getInt("FIID_ESTATUS"));
        docs.setFctipo_envio(rs.getString("FCTIPO_ENVIO"));
        docs.setFdfecha_envio(rs.getString("FDFECHA_ENVIO"));

        return docs;
    }
}
