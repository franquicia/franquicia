package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.PaisDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PaisRowMapper implements RowMapper<PaisDTO> {

    public PaisDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PaisDTO paisDTO = new PaisDTO();

        paisDTO.setIdPais(rs.getInt("FIID_PAIS"));
        paisDTO.setNombre(rs.getString("FCNOMBRE"));
        paisDTO.setActivo(rs.getInt("FIACTIVO"));

        return paisDTO;
    }
}
