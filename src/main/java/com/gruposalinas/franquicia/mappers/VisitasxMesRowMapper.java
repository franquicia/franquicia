package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.VisitasxMesDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class VisitasxMesRowMapper implements RowMapper<VisitasxMesDTO> {

    @Override
    public VisitasxMesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        VisitasxMesDTO visitasxMesDTO = new VisitasxMesDTO();

        visitasxMesDTO.setVisitas(rs.getInt("VISITAS"));
        visitasxMesDTO.setUsuario(rs.getInt("FIID_USUARIO"));
        visitasxMesDTO.setCeco(rs.getString("FCID_CECO"));
        visitasxMesDTO.setNombreCeco(rs.getString("FCNOMBRE"));
        visitasxMesDTO.setMes(rs.getString("MES"));

        return visitasxMesDTO;
    }

}
