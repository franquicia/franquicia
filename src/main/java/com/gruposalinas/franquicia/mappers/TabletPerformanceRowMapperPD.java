package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TabletPerformancePDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TabletPerformanceRowMapperPD implements RowMapper<TabletPerformancePDDTO> {

    @Override
    public TabletPerformancePDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TabletPerformancePDDTO info = new TabletPerformancePDDTO();

        info.setIdTableta(rs.getInt("FIID_TABLETA"));
        info.setDescPadreWeb(rs.getString("DESC_PADRE_WEB"));
        info.setDescWeb(rs.getString("FCDESC_WEB"));
        info.setIdEstatus(rs.getInt("FIID_ESTATUS"));
        info.setVisible(rs.getString("FCVISIBLE"));
        info.setUltimaActualizacion(rs.getString("ULTIMA_ACTUALIZA"));

        return info;
    }
}
