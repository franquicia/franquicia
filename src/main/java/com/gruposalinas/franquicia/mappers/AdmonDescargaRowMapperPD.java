package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.BachDescargasPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AdmonDescargaRowMapperPD implements RowMapper<BachDescargasPDDTO> {

    @Override
    public BachDescargasPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        BachDescargasPDDTO descarga = new BachDescargasPDDTO();

        descarga.setIdControlDesc(rs.getInt("FIIDCTROL_DECAR"));
        descarga.setTipoDescarga(rs.getString("FCTIPO_DESCARGA"));
        descarga.setIdTablet(rs.getInt("FIID_TABLETA"));
        descarga.setIdEstatus(rs.getInt("FIID_ESTATUS"));
        descarga.setFcUser(rs.getString("FCUSER"));
        descarga.setFechaActu(rs.getString("FDFECHA_CAMBIO"));
        descarga.setActivo(rs.getInt("FIACTIVO"));

        return descarga;
    }

}
