package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.AdminTipoDocPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AdmonTipoDocRowMapperPD implements RowMapper<AdminTipoDocPDDTO> {

    @Override
    public AdminTipoDocPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        AdminTipoDocPDDTO adminTipoDoc = new AdminTipoDocPDDTO();

        adminTipoDoc.setIdTipoDoc(rs.getInt("FIID_TIPO_DOCTO"));
        adminTipoDoc.setNombre(rs.getString("FCNOMBRE_DOCTO"));
        adminTipoDoc.setNivel(rs.getInt("FINIVEL"));
        adminTipoDoc.setDependeDe(rs.getInt("FIDEPENDE_DE"));
        adminTipoDoc.setDescDependeDe(rs.getString("DESD_DEPENDE"));
        adminTipoDoc.setUser(rs.getString("FCUSER"));
        adminTipoDoc.setFecha(rs.getString("FDFECHA_CAMBIO"));
        adminTipoDoc.setActivo(rs.getInt("FIACTIVO"));

        return adminTipoDoc;
    }

}
