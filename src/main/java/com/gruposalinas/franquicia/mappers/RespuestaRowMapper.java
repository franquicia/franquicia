package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.RespuestaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class RespuestaRowMapper implements RowMapper<RespuestaDTO> {

    public RespuestaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        RespuestaDTO respuestaDTO = new RespuestaDTO();

        respuestaDTO.setIdRespuesta(rs.getInt("FIID_RESPUESTA"));
        respuestaDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
        respuestaDTO.setIdArboldecision(rs.getInt("FIID_ARBOL_DES"));
        respuestaDTO.setObservacion(rs.getString("FCOBSERVACIONES"));
        return respuestaDTO;
    }
}
