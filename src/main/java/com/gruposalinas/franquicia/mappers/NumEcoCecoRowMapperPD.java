package com.gruposalinas.franquicia.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class NumEcoCecoRowMapperPD implements RowMapper<String> {

    @Override
    public String mapRow(ResultSet rs, int rowNum) throws SQLException {
        String fcid_ceco = null;

        try {
            fcid_ceco = rs.getString("FCID_CECO");
        } catch (Exception e) {
            fcid_ceco = "";
        }

        return fcid_ceco;
    }
}
