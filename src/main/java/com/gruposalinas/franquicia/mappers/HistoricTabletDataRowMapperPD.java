package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.HistoricTabletDataPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class HistoricTabletDataRowMapperPD implements RowMapper<HistoricTabletDataPDDTO> {

    @Override
    public HistoricTabletDataPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        HistoricTabletDataPDDTO data = new HistoricTabletDataPDDTO();

        data.setIdTableta(rs.getInt("FIID_TABLETA"));
        data.setNumSerie(rs.getString("FCNUM_SERIE"));
        data.setIdTabletaEdo(rs.getInt("FIID_TABLETA_EDO"));
        data.setIdTabletaEdoPadre(rs.getInt("ID_EDO_TAB_PADRE"));
        data.setDescTabletaEdoPadreWeb(rs.getString("DESC_PADRE_WEB"));
        data.setDescTabletaEdoPadreJson(rs.getString("DESC_PADRE_JSON"));
        data.setIdEstadoTab(rs.getInt("FIID_ESTADO_TAB"));
        data.setDescEstadoTabWeb(rs.getString("FCDESC_WEB"));
        data.setDescEstadoTabJson(rs.getString("FCDESC_JSON"));
        data.setVisible(rs.getString("FCVISIBLE"));
        data.setIdEstatus(rs.getInt("FIID_ESTATUS"));
        data.setDependeDe(rs.getInt("FIDEPENDE_DE"));
        data.setFecha(rs.getString("FDFECHA_CAMBIO"));
        data.setActivo(rs.getInt("FIACTIVO"));

        return data;
    }
}
