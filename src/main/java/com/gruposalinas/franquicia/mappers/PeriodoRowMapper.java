package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.PeriodoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PeriodoRowMapper implements RowMapper<PeriodoDTO> {

    public PeriodoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PeriodoDTO periodo = new PeriodoDTO();

        periodo.setIdPeriodo(rs.getInt("FIID_PERIODO"));
        periodo.setDescripcion(rs.getString("FCDESCRIPCION"));

        return periodo;
    }

}
