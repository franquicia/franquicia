package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.SucursalDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class SucursalGCCRowMapper implements RowMapper<SucursalDTO> {

    @Override
    public SucursalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        SucursalDTO sucursalDTO = new SucursalDTO();

        sucursalDTO.setIdSucursal(rs.getInt("FIID_SUCURSAL"));
        sucursalDTO.setIdPais(rs.getInt("FIID_PAIS"));
        sucursalDTO.setIdCanal(rs.getInt("FIID_CANAL"));
        sucursalDTO.setNuSucursal(rs.getString("FCNU_SUCURSAL"));
        sucursalDTO.setNombresuc(rs.getString("FCNOMBRECC"));
        sucursalDTO.setLongitud(rs.getDouble("FCLONGITUD"));
        sucursalDTO.setLatitud(rs.getDouble("FCLATITUD"));

        return sucursalDTO;
    }

}
