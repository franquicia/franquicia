package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.SucEstTabPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class SucEstTabRowMapperPD implements RowMapper<SucEstTabPDDTO> {

    @Override
    public SucEstTabPDDTO mapRow(ResultSet rs, int arg1) throws SQLException {

        SucEstTabPDDTO obj = new SucEstTabPDDTO();
        obj.setFiid_tableta(rs.getInt("FIID_TABLETA"));
        obj.setFcnum_serie(rs.getString("FCNUM_SERIE"));
        obj.setFcid_ceco(rs.getString("FCID_CECO"));
        obj.setSucursal(rs.getInt("SUCURSAL"));
        obj.setNom_ceco(rs.getString("NOM_CECO"));
        obj.setUsuario_tablet(rs.getInt("USUARIO_TABLET"));
        obj.setFiid_estatus(rs.getInt("FIID_ESTATUS"));
        obj.setDesc_id_estatus(rs.getString("DESC_ID_ESTATUS"));
        obj.setFdfecha_instala(rs.getString("FDFECHA_INSTALA"));
        obj.setFcactivo(rs.getString("FCACTIVO"));
        obj.setFcfabricante(rs.getString("FCFABRICANTE"));
        obj.setFcmodelo(rs.getString("FCMODELO"));
        obj.setFcsistema_o(rs.getString("FCSISITEMA_O"));
        obj.setFcversion_app(rs.getString("FCVERSION_APP"));
        obj.setUltima_actualizacion(rs.getString("ULTIMA_ACTUALIZACION"));
        obj.setDescactivo_ceco(rs.getString("DESCACTIVO_CECO"));

        return obj;
    }
}
