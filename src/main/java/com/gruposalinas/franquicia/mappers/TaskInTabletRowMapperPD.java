package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TaskInTabletPDDAO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;

public class TaskInTabletRowMapperPD implements RowMapper<TaskInTabletPDDAO> {

    private static final Logger logger = LogManager.getLogger(TaskInTabletRowMapperPD.class);

    @Override
    public TaskInTabletPDDAO mapRow(ResultSet rs, int rowNum) throws SQLException {
        TaskInTabletPDDAO task = new TaskInTabletPDDAO();

        task.setIdFolio(rs.getInt("FIFOLIO"));
        task.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        task.setNombreArchivo(rs.getString("FCNOMBRE_ARC"));
        task.setTipoEnvio(rs.getString("FCTIPO_ENVIO"));
        task.setVisibleSucursal(rs.getString("FCVISIBLE_SUC"));
        task.setIdCeco(rs.getString("FCID_CECO"));
        task.setIdCategoria(rs.getInt("ID_CATEGORIA"));
        task.setDescCategoria(rs.getString("DESC_CATEGOR"));
        task.setIdTipoDocumento(rs.getInt("FIID_TIPO_DOCTO"));
        task.setNombreDocumento(rs.getString("FCNOMBRE_DOCTO"));
        task.setIdDocumentoPedestal(rs.getInt("FIID_DOCTOPEDESTAL"));
        task.setVigenciaInicial(rs.getString("VIGENCIA_INI"));
        task.setVigenciaFinal(rs.getString("VIGENCIA_FIN"));
        task.setPesoBytes(rs.getLong("FIPESO_BYTES"));
        task.setEstatusDocumento(rs.getInt("ESTATUS_DOC"));
        task.setEstatusDocumentoSucursal(rs.getInt("ESTA_DOC_SUC"));

        try {
            task.setHash(rs.getString("FCHASH"));
        } catch (Exception e) {
            logger.info("TaskInTabletPDDAO: No se pudo asignar el campo FCHASH");
        }

        task.setActivo(rs.getInt("FIACTIVO"));

        return task;
    }
}
