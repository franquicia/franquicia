package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.TipoChecklistDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ChecklistGeneralRowMapper implements RowMapper<ChecklistDTO> {

    public ChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ChecklistDTO checklistgeneral = new ChecklistDTO();
        TipoChecklistDTO tipochecklist = new TipoChecklistDTO();

        checklistgeneral.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        tipochecklist.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
        checklistgeneral.setIdTipoChecklist(tipochecklist);
        checklistgeneral.setNombreCheck(rs.getString("FCNOMBRE"));
        checklistgeneral.setIdHorario(rs.getInt("FIID_HORARIO"));
        checklistgeneral.setVigente(rs.getInt("FIVIGENTE"));
        checklistgeneral.setFecha_inicio(rs.getString("FDFECHA_INICIO"));
        checklistgeneral.setFecha_fin(rs.getString("FDFECHA_FIN"));
        checklistgeneral.setIdEstado(rs.getInt("FIID_ESTADO"));
        //checklistgeneral.setIdUsuario(rs.getInt("FIID_USUARIO"));
        //checklistgeneral.setDia(rs.getString("FCDIA"));
        //checklistgeneral.setPeriodicidad(rs.getString("FCPERIODO"));

        return checklistgeneral;
    }
}
