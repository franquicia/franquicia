package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.FileWithoutChecksumPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FileWithoutChecksumRowMapperPD implements RowMapper<FileWithoutChecksumPDDTO> {

    @Override
    public FileWithoutChecksumPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        FileWithoutChecksumPDDTO file = new FileWithoutChecksumPDDTO();
        file.setFiid_documento(rs.getInt("FIID_DOCUMENTO"));
        file.setCategoria(rs.getInt("CATEGORIA"));
        file.setFcnombre_arch(rs.getString("FCNOMBRE_ARC"));

        return file;
    }
}
