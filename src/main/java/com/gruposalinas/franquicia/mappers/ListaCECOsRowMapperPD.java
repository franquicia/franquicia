package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ListaCECOsPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ListaCECOsRowMapperPD implements RowMapper<ListaCECOsPDDTO> {

    @Override
    public ListaCECOsPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        // TODO Auto-generated method stub

        ListaCECOsPDDTO listacecos = new ListaCECOsPDDTO();

        listacecos.setId_listaCeco(rs.getInt("FIIDLISTA_CECO"));
        listacecos.setId_listaDistribucion(rs.getInt("FIID_LISTA_DISTR"));
        listacecos.setId_ceco(rs.getString("FCID_CECO"));
        listacecos.setActivo(rs.getInt("FIACTIVO"));
        listacecos.setNombreCeco(rs.getString("FCNOMBRE"));

        return listacecos;
    }
}
