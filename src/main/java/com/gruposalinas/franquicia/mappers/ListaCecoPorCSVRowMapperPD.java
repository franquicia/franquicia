package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ListaCecoPorCSVPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ListaCecoPorCSVRowMapperPD implements RowMapper<ListaCecoPorCSVPDDTO> {

    @Override
    public ListaCecoPorCSVPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ListaCecoPorCSVPDDTO lista = new ListaCecoPorCSVPDDTO();

        lista.setIdCeco(rs.getInt("FCID_CECO"));
        lista.setNombre(rs.getString("FCNOMBRE"));
        lista.setActivo(rs.getInt("FIACTIVO"));
        lista.setNegocio(rs.getInt("FIID_NEGOCIO"));

        return lista;
    }
}
