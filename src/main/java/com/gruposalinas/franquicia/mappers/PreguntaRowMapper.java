package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.PreguntaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PreguntaRowMapper implements RowMapper<PreguntaDTO> {

    public PreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PreguntaDTO preguntaDTO = new PreguntaDTO();

        preguntaDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
        preguntaDTO.setEstatus(rs.getInt("FIESTATUS"));
        preguntaDTO.setPregunta(rs.getString("FCDESCRIPCION"));
        preguntaDTO.setIdModulo(rs.getInt("FIID_MODULO"));
        preguntaDTO.setIdTipo(rs.getInt("FIID_TIPO_PREG"));
        return preguntaDTO;
    }
}
