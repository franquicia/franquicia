package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.PaisNegocioDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PaisNegocioRowMapper implements RowMapper<PaisNegocioDTO> {

    @Override
    public PaisNegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PaisNegocioDTO paisNegocio = new PaisNegocioDTO();

        paisNegocio.setIdPais(rs.getInt("FIID_NEGOCIO"));
        paisNegocio.setIdNegocio(rs.getInt("FIID_PAIS"));

        return paisNegocio;
    }

}
