package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.FiltrosCecoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FiltrosCecosRowMapper implements RowMapper<FiltrosCecoDTO> {

    @Override
    public FiltrosCecoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        FiltrosCecoDTO cecoDTO = new FiltrosCecoDTO();

        cecoDTO.setNombreCeco(rs.getString("FCNOMBRE"));
        cecoDTO.setIdCeco(rs.getString("FCID_CECO"));

        return cecoDTO;
    }

}
