package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ExpedientesDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ExpedienteRowMapper implements RowMapper<ExpedientesDTO> {

    public ExpedientesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ExpedientesDTO expedienteDTO = new ExpedientesDTO();

        expedienteDTO.setIdExpediente(rs.getInt("FIID_EXPPASIVO"));
        expedienteDTO.setIdGerente(rs.getInt("FIID_USUARIO"));
        expedienteDTO.setIdCeco(rs.getString("FIID_CECO"));
        expedienteDTO.setFecha(rs.getString("FECHA"));
        expedienteDTO.setEstatusExpediente(rs.getInt("FIESTATUS"));
        expedienteDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        expedienteDTO.setCiudad(rs.getString("FCCIUDAD"));

        return expedienteDTO;
    }

}
