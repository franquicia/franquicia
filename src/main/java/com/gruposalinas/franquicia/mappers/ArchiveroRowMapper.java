package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ArchiveroDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ArchiveroRowMapper implements RowMapper<ArchiveroDTO> {

    public ArchiveroDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ArchiveroDTO archDTO = new ArchiveroDTO();

        archDTO.setIdArchivero(rs.getInt("FIDETARCHIVE_ID"));
        archDTO.setPlaca(rs.getString("FCPLACA"));
        archDTO.setMarca(rs.getString("FCMARCA"));
        archDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        archDTO.setObservaciones(rs.getString("FCOBSERVACIONES"));
        archDTO.setIdFormArchivero(rs.getInt("FIARCHIVERO_ID"));

        return archDTO;
    }

}
