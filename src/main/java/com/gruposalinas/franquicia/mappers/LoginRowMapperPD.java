package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.LoginPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class LoginRowMapperPD implements RowMapper<LoginPDDTO> {

    public LoginPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        LoginPDDTO loginDTO = new LoginPDDTO();

        loginDTO.setUsuario(rs.getInt("FIID_USUARIO"));
        loginDTO.setPerfil(rs.getInt("FIID_PERFIL"));
        loginDTO.setDescripcion(rs.getString("FCDESCRIPCION"));

        return loginDTO;
    }
}
