package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.FolioDataPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FolioDataRowMapperPD implements RowMapper<FolioDataPDDTO> {

    @Override
    public FolioDataPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        FolioDataPDDTO lista = new FolioDataPDDTO();
        lista.setIdFolio(rs.getInt("FIFOLIO"));
        lista.setTipoEnvio(rs.getString("FCTIPO_ENVIO"));
        lista.setFechaEnvio(rs.getString("FDFECHA_ENVIO"));
        lista.setUsuarioModifica(rs.getString("FCUSER"));
        lista.setActivo(rs.getInt("FIACTIVO"));

        return lista;
    }
}
