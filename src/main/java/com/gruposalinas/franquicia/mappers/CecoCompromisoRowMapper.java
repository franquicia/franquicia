package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CompromisoCecoDTO;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CecoCompromisoRowMapper implements RowMapper<CompromisoCecoDTO> {

    public CompromisoCecoDTO mapRow(java.sql.ResultSet rs, int rowNum) throws SQLException {

        CompromisoCecoDTO cecoDTO = new CompromisoCecoDTO();

        cecoDTO.setIdCeco(rs.getString("FCID_CECO"));
        cecoDTO.setDescCeco(rs.getString("FCNOMBRE"));
        cecoDTO.setTotal(rs.getInt("CONTEO"));

        return cecoDTO;
    }

}
