package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TipoQuejaPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TipoQuejaRowMapperPD implements RowMapper<TipoQuejaPDDTO> {

    @Override
    public TipoQuejaPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        TipoQuejaPDDTO data = new TipoQuejaPDDTO();

        data.setIdCatComent(rs.getInt("FIIDCAT_COMENT"));
        data.setDesc(rs.getString("FCDESCRIPCION"));
        data.setUser(rs.getString("FCUSER"));
        data.setFechaCambio(rs.getString("FDFECHA_CAMBIO"));
        data.setActivo(rs.getInt("FIACTIVO"));

        return data;
    }
}
