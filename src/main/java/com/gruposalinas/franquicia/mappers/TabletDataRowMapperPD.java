package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TabletDataPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TabletDataRowMapperPD implements RowMapper<TabletDataPDDTO> {

    @Override
    public TabletDataPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TabletDataPDDTO lista = new TabletDataPDDTO();
        lista.setIdTableta(rs.getInt("FIID_TABLETA"));
        lista.setNumSerie(rs.getString("FCNUM_SERIE"));
        lista.setIdCeco(rs.getString("FCID_CECO"));
        lista.setIdUsuario(rs.getInt("FIID_USUARIO"));
        lista.setIdEstatus(rs.getInt("FIID_ESTATUS"));
        lista.setFechaInstalacion(rs.getString("FDFECHA_INSTALA"));
        lista.setUsuarioModifica(rs.getString("FCUSER"));
        lista.setFechaModifica(rs.getString("FDFECHA_CAMBIO"));
        lista.setActivo(rs.getInt("FCACTIVO"));

        return lista;
    }
}
