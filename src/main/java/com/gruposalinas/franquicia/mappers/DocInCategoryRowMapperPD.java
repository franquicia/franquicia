package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.DocInCategoriaPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class DocInCategoryRowMapperPD implements RowMapper<DocInCategoriaPDDTO> {

    @Override
    public DocInCategoriaPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        DocInCategoriaPDDTO lista = new DocInCategoriaPDDTO();
        lista.setIdItem(rs.getInt("FIID_ITEM"));
        lista.setDescripcion(rs.getString("FCDESCRIPCION"));
        lista.setIdCategoria(rs.getInt("FIID_CATEGORIA"));
        lista.setTipo(rs.getString("FCTIPO"));
        lista.setUsuarioModif(rs.getString("FCUSUARIO_MODIF"));
        lista.setFechaModif(rs.getString("FDFECHA_MODIF"));
        lista.setActivo(rs.getInt("FIACTIVO"));
        lista.setOrden(rs.getInt("FIORDEN"));

        return lista;
    }
}
