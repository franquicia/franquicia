package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TabletDataForExcelPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TabletDataForExcelRowMapperPD implements RowMapper<TabletDataForExcelPDDTO> {

    @Override
    public TabletDataForExcelPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        TabletDataForExcelPDDTO data = new TabletDataForExcelPDDTO();

        data.setIdTableta(rs.getInt("FIID_TABLETA"));
        data.setNumSerie(rs.getString("FCNUM_SERIE"));
        data.setIdCeco(rs.getString("FCID_CECO"));
        data.setDescCeco(rs.getString("NOM_CENTRO_COS"));
        data.setIdUsuario(rs.getInt("FIID_USUARIO"));
        data.setFabricante(rs.getString("FCFABRICANTE"));
        data.setModelo(rs.getString("FCMODELO"));
        data.setSistemaOp(rs.getString("FCSISTEMA_O"));
        data.setVersion(rs.getString("FCVERSION_APP"));
        data.setIdEstatus(rs.getInt("FIID_ESTATUS"));
        data.setActivo(rs.getString("FCACTIVO"));
        data.setFechaInstalacion(rs.getString("FDFECHA_INSTALA"));
        data.setFechaCambio(rs.getString("FDFECHA_CAMBIO"));
        data.setUsuario(rs.getString("FCUSER"));

        return data;
    }
}
