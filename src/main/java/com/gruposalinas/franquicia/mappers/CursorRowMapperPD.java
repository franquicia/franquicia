package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.MenuPedestalDigital;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CursorRowMapperPD implements RowMapper<MenuPedestalDigital> {

    public MenuPedestalDigital mapRow(ResultSet rs, int rowNum) throws SQLException {
        MenuPedestalDigital menu = new MenuPedestalDigital();

        menu.setIdMenu(rs.getInt("FIID_MENU"));
        menu.setDescripcion(rs.getString("FCDESCRIPCION"));

        return menu;
    }
}
