package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.AdminCanalPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AdmonCanalRowMapperPD implements RowMapper<AdminCanalPDDTO> {

    @Override
    public AdminCanalPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        AdminCanalPDDTO canal = new AdminCanalPDDTO();

        canal.setIdCanal(rs.getInt("FIID_CANAL"));
        canal.setCanal(rs.getString("FCCANAL"));
        canal.setUsuario(rs.getString("FCUSER"));
        canal.setFechaCambio(rs.getString("FDFECHA_CAMBIO"));
        canal.setActivo(rs.getInt("FIACTIVO"));

        return canal;
    }

}
