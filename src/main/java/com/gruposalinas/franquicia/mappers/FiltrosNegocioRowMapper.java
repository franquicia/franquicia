package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.NegocioDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FiltrosNegocioRowMapper implements RowMapper<NegocioDTO> {

    @Override
    public NegocioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        NegocioDTO negocioDTO = new NegocioDTO();
        negocioDTO.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        negocioDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        return negocioDTO;
    }

}
