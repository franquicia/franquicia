package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CecoPilaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CecoPilaRowMapper implements RowMapper<CecoPilaDTO> {

    public CecoPilaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CecoPilaDTO ceco = new CecoPilaDTO();

        ceco.setIdCeco(rs.getInt("FCID_CECO"));
        ceco.setNombreCeco(rs.getString("FCNOMBRE"));
        ceco.setAsignados(rs.getInt("ASIGNADOS"));
        ceco.setTotal(rs.getInt("TERMINADOS"));

        return ceco;
    }
}
