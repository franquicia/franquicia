package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CecoDTO;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CecoGeografiaRowMapper implements RowMapper<CecoDTO> {

    public CecoDTO mapRow(java.sql.ResultSet rs, int rowNum) throws SQLException {

        CecoDTO cecoDTO = new CecoDTO();

        cecoDTO.setIdCeco(rs.getString("FCID_CECO"));
        cecoDTO.setDescCeco(rs.getString("FCNOMBRE"));
        cecoDTO.setIdCCSuperior(rs.getString("FICECO_SUPERIOR"));

        return cecoDTO;
    }

}
