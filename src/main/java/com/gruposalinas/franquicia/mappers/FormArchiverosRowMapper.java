package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.FormArchiverosDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FormArchiverosRowMapper implements RowMapper<FormArchiverosDTO> {

    public FormArchiverosDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        FormArchiverosDTO formDTO = new FormArchiverosDTO();

        formDTO.setIdFromArchivero(rs.getInt("FIARCHIVERO_ID"));
        formDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        formDTO.setNombre(rs.getString("FCNOMBRE"));
        formDTO.setIdCeco(rs.getString("FCID_CECO"));
        formDTO.setIdRecibe(rs.getInt("FIID_USRECIBE"));
        formDTO.setNomRecibe(rs.getString("FCNOMRECIBE"));
        formDTO.setIdStatus(rs.getInt("FISTATUS_ID"));
        formDTO.setFecha(rs.getString("FECHA_CREACION"));
        return formDTO;
    }

}
