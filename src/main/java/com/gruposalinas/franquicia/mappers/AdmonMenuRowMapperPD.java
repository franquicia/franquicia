package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.AdminMenuPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AdmonMenuRowMapperPD implements RowMapper<AdminMenuPDDTO> {

    @Override
    public AdminMenuPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        AdminMenuPDDTO menu = new AdminMenuPDDTO();

        menu.setIdMenu(rs.getInt("FIID_MENU"));
        menu.setDescripcion(rs.getString("FCDESCRIPCION"));
        menu.setNivel(rs.getInt("FINIVEL"));
        menu.setCursor(rs.getString("FCUSR"));
        menu.setFechaCambio(rs.getString("FDFECHA_CAMBIO"));
        menu.setActivo(rs.getInt("FIACTIVO"));
        menu.setDependeDe(rs.getInt("FIDEPENDE_DE"));

        return menu;
    }

}
