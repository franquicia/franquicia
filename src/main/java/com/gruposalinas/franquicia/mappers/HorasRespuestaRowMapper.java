package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.HorasRespuestaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class HorasRespuestaRowMapper implements RowMapper<HorasRespuestaDTO> {

    @Override
    public HorasRespuestaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        HorasRespuestaDTO horasRespuestaDTO = new HorasRespuestaDTO();

        horasRespuestaDTO.setCeco(rs.getString("FCID_CECO"));
        horasRespuestaDTO.setHora(rs.getString("HORA"));

        return horasRespuestaDTO;
    }

}
