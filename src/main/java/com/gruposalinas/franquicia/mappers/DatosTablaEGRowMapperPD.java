package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.DatosTablaEGDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class DatosTablaEGRowMapperPD implements RowMapper<DatosTablaEGDTO> {

    @Override
    public DatosTablaEGDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        DatosTablaEGDTO datos = new DatosTablaEGDTO();

        datos.setMes(rs.getInt("MES"));
        datos.setTotal(rs.getInt("TOTAL_INSTALA"));

        return datos;
    }
}
