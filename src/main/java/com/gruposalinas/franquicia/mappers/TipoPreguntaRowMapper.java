package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TipoPreguntaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TipoPreguntaRowMapper implements RowMapper<TipoPreguntaDTO> {

    public TipoPreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TipoPreguntaDTO tipoPreguntaDTO = new TipoPreguntaDTO();

        tipoPreguntaDTO.setIdTipoPregunta(rs.getInt("FIID_TIPO_PREG"));
        tipoPreguntaDTO.setCvePregunta(rs.getString("FCCLAVE_TIPO"));
        tipoPreguntaDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        tipoPreguntaDTO.setActivo(rs.getInt("FIACTIVO"));

        return tipoPreguntaDTO;
    }
}
