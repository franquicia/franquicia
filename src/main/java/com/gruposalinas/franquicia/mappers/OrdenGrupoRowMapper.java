package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.OrdenGrupoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class OrdenGrupoRowMapper implements RowMapper<OrdenGrupoDTO> {

    public OrdenGrupoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        OrdenGrupoDTO ordenGrupo = new OrdenGrupoDTO();

        ordenGrupo.setIdOrdenGrupo(rs.getInt("FIID_ORDEN_G"));
        ordenGrupo.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        ordenGrupo.setOrden(rs.getInt("FIORDEN"));
        ordenGrupo.setIdGrupo(rs.getInt("FIID_GRUPO"));

        return ordenGrupo;
    }
}
