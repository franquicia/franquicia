package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.DocInCategoryPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class DocInCategory2RowMapperPD implements RowMapper<DocInCategoryPDDTO> {

    @Override
    public DocInCategoryPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        DocInCategoryPDDTO foo = new DocInCategoryPDDTO();

        foo.setIdTipoDoc(rs.getInt("FIID_TIPO_DOCTO"));
        foo.setNombreDoc(rs.getString("FCNOMBRE_DOCTO"));
        foo.setNivel(rs.getInt("FINIVEL"));
        foo.setDependeDe(rs.getInt("FIDEPENDE_DE"));
        foo.setDescDependeDe(rs.getString("DESD_DEPENDE"));
        foo.setUsuario(rs.getString("FCUSER"));
        foo.setFechaCambio(rs.getString("FDFECHA_CAMBIO"));
        foo.setActivo(rs.getInt("FIACTIVO"));

        return foo;
    }
}
