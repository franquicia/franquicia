package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ParamNegocioPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ParamNegocioRowMapperPD implements RowMapper<ParamNegocioPDDTO> {

    @Override
    public ParamNegocioPDDTO mapRow(ResultSet arg0, int arg1) throws SQLException {

        ParamNegocioPDDTO obj = new ParamNegocioPDDTO();
        obj.setFcdesc_parametro(arg0.getString("FCDESC_PARAMETRO"));
        obj.setFcvalor(arg0.getString("FCVALOR"));
        obj.setFcreferencia(arg0.getString("FCREFERENCIA"));
        obj.setFiactivo(arg0.getInt("FIACTIVO"));

        return obj;
    }
}
