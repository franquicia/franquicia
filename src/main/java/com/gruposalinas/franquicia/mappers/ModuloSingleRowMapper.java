package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ModuloDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ModuloSingleRowMapper implements RowMapper<ModuloDTO> {

    @Override
    public ModuloDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ModuloDTO moduloDTO = new ModuloDTO();

        moduloDTO.setIdModulo(rs.getInt("FIID_MODULO"));
        moduloDTO.setNombre(rs.getString("FCNOMBRE"));
        return moduloDTO;
    }

}
