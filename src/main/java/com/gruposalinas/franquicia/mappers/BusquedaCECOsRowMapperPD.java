package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.BusquedaCECOsDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class BusquedaCECOsRowMapperPD implements RowMapper<BusquedaCECOsDTO> {

    @Override
    public BusquedaCECOsDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        BusquedaCECOsDTO cecos = new BusquedaCECOsDTO();

        cecos.setIdCeco(rs.getInt("FCID_CECO"));
        cecos.setTipoCeco(rs.getInt("FITIPO_CECO"));
        cecos.setNombre(rs.getString("FCNOMBRE"));
        cecos.setCecoSuperior(rs.getInt("FICECO_SUPERIOR"));
        cecos.setCecoSuperiorDesc(rs.getString("DESC_DEPENDE_DE"));

        return cecos;
    }
}
