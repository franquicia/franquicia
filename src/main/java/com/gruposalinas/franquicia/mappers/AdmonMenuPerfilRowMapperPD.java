package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.AdminMenuPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AdmonMenuPerfilRowMapperPD implements RowMapper<AdminMenuPDDTO> {

    @Override
    public AdminMenuPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        AdminMenuPDDTO menu = new AdminMenuPDDTO();

        menu.setIdMenu(rs.getInt("FIID_MENU"));
        menu.setDescripcion(rs.getString("FCDESCRIPCION"));
        menu.setNivel(rs.getInt("FINIVEL"));
        menu.setActivo(rs.getInt("FIACTIVO"));
        menu.setDependeDe(rs.getInt("FIDEPENDE_DE"));

        menu.setDescDependeDe(rs.getString("DESC_DEPENDE_DE"));
        menu.setIdPerfil(rs.getInt("FIID_PERFIL"));
        menu.setDescPerfil(rs.getString("DESC_PERFIL"));
        menu.setActivoPerfil(rs.getInt("FIACTIVO"));
        return menu;
    }

}
