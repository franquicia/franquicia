package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ChecklistPilaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class NotificacionPorcentajeRowMapper implements RowMapper<ChecklistPilaDTO> {

    public ChecklistPilaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ChecklistPilaDTO checklist = new ChecklistPilaDTO();

        checklist.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        checklist.setNombreCheck(rs.getString("FCNOMBRE"));
        checklist.setEstatus(rs.getInt("CONTEO"));

        return checklist;
    }

}
