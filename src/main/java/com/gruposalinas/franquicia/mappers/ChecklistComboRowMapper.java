package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ChecklistDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ChecklistComboRowMapper implements RowMapper<ChecklistDTO> {

    public ChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ChecklistDTO checklistgeneral = new ChecklistDTO();

        checklistgeneral.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        checklistgeneral.setNombreCheck(rs.getString("FCNOMBRE"));

        return checklistgeneral;
    }
}
