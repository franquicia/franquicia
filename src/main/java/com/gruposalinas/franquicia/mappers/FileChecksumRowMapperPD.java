package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.FileChecksumPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FileChecksumRowMapperPD implements RowMapper<FileChecksumPDDTO> {

    @Override
    public FileChecksumPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        FileChecksumPDDTO file = new FileChecksumPDDTO();
        file.setFiid_enc_arch(rs.getInt("FIID_ENCR_ARCH"));
        file.setFireferencia(rs.getInt("FIREFERENCIA"));
        file.setFcorigen(rs.getString("FCORIGEN"));
        file.setFchash(rs.getString("FCHASH"));
        file.setFcuser(rs.getString("FCUSER"));
        file.setFdfecha_cambio(rs.getString("FDFECHA_CAMBIO"));
        file.setFiactivo(rs.getInt("FIACTIVO"));

        return file;
    }
}
