package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.PerfilDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PerfilRowMapper implements RowMapper<PerfilDTO> {

    public PerfilDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PerfilDTO perfilDTO = new PerfilDTO();

        perfilDTO.setIdPerfil(rs.getInt("FIID_PERFIL"));
        perfilDTO.setDescripcion(rs.getString("FCDESCRIPCION"));

        return perfilDTO;
    }
}
