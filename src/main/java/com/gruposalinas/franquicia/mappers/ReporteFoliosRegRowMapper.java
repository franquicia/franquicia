package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.PreguntaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ReporteFoliosRegRowMapper implements RowMapper<PreguntaDTO> {

    public PreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PreguntaDTO preguntaDTO = new PreguntaDTO();
        preguntaDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
        preguntaDTO.setPregunta(rs.getString("PREGUNTA"));
        return preguntaDTO;
    }

}
