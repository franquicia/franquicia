package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ParamCecoPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ParamCecoRowMapperPD implements RowMapper<ParamCecoPDDTO> {

    @Override
    public ParamCecoPDDTO mapRow(ResultSet rs, int arg1) throws SQLException {
        ParamCecoPDDTO parametro = new ParamCecoPDDTO();

        try {
            parametro.setFiid_parametro(rs.getInt("FIID_PARAMETRO"));
        } catch (Exception e) {
            parametro.setFiid_parametro(null);
        }

        try {
            parametro.setFiparametro(rs.getInt("FIPARAMETRO"));
        } catch (Exception e) {
            parametro.setFiparametro(rs.getInt(null));
        }

        try {
            parametro.setFcdesc_parametro(rs.getString("FCDESC_PARAMETRO"));
        } catch (Exception e) {
            parametro.setFcdesc_parametro(null);
        }

        try {
            parametro.setFcvalor(rs.getString("FCVALOR"));
        } catch (Exception e) {
            parametro.setFcvalor(rs.getString(null));
        }

        try {
            parametro.setFcreferencia(rs.getString("FCREFERENCIA"));
        } catch (Exception e) {
            parametro.setFcreferencia(null);
        }

        try {
            parametro.setFiactivo(rs.getInt("FIACTIVO"));
        } catch (Exception e) {
            parametro.setFiactivo(null);
        }

        return parametro;
    }
}
