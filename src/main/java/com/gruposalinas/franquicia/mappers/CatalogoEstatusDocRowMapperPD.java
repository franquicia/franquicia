package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CatalogoEstatusDocPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CatalogoEstatusDocRowMapperPD implements RowMapper<CatalogoEstatusDocPDDTO> {

    @Override
    public CatalogoEstatusDocPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        CatalogoEstatusDocPDDTO cat = new CatalogoEstatusDocPDDTO();

        cat.setIdEstatusDoc(rs.getInt("FIID_ESTATUS_DOC"));
        cat.setDescEstatusDoc(rs.getString("FCDESC_ESTATUS"));
        cat.setUser(rs.getString("FCUSER"));
        cat.setFechaModifica(rs.getString("FDFECHA_CAMBIO"));
        cat.setActivo(rs.getInt("FIACTIVO"));

        return cat;
    }
}
