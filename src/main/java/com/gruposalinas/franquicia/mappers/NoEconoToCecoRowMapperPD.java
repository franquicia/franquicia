package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.BusquedaCECOsDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class NoEconoToCecoRowMapperPD implements RowMapper<BusquedaCECOsDTO> {

    @Override
    public BusquedaCECOsDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        BusquedaCECOsDTO cecos = new BusquedaCECOsDTO();
        cecos.setIdCeco(rs.findColumn("FCID_CECO"));
        cecos.setNombre(rs.getString("FCNOMBRE"));
        cecos.setNegocio(rs.getInt("FIID_NEGOCIO"));
        cecos.setActivo(rs.getInt("FIACTIVO"));

        return cecos;
    }
}
