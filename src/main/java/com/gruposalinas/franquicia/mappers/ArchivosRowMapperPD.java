package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CargaArchivoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ArchivosRowMapperPD implements RowMapper<CargaArchivoDTO> {

    @Override
    public CargaArchivoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        // TODO Auto-generated method stub
        CargaArchivoDTO archivos = new CargaArchivoDTO();

        archivos.setId_cargaArchivo(rs.getInt("FIID_ARCH_CARGA"));
        archivos.setId_usuario(rs.getInt("FIID_USUARIO"));
        archivos.setNombre(rs.getString("FCNOMBRE"));
        archivos.setDescribe(rs.getString("FCDESCRIBE"));
        archivos.setVisibleSucursal(rs.getInt("FIVISIBLE_SUC"));
        archivos.setTipoArchivo(rs.getString("FCTIPO_ARCHIVO"));
        archivos.setCursor(rs.getString("FCUSR"));
        archivos.setFecha(rs.getString("FDFECHA_CAMBIO"));
        archivos.setActivo(rs.getInt("FIACTIVO"));

        return archivos;
    }

}
