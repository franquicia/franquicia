package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ListaCategoriasPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ListaCategoriasRowMapperPD implements RowMapper<ListaCategoriasPDDTO> {

    @Override
    public ListaCategoriasPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ListaCategoriasPDDTO lista = new ListaCategoriasPDDTO();

        lista.setIdTipoDoc(rs.getInt("FIID_TIPO_DOCTO"));
        lista.setNombreDoc(rs.getString("FCNOMBRE_DOCTO"));
        lista.setNivel(rs.getInt("FINIVEL"));
        lista.setDependeDe(rs.getInt("FIDEPENDE_DE"));
        lista.setDependeDeDesc(rs.getString("DESD_DEPENDE"));
        lista.setActivo(rs.getInt("FIACTIVO"));

        return lista;
    }
}
