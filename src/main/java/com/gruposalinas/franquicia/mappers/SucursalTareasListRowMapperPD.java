package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.SucursalTareasListPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class SucursalTareasListRowMapperPD implements RowMapper<SucursalTareasListPDDTO> {

    @Override
    public SucursalTareasListPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        SucursalTareasListPDDTO sucursal = new SucursalTareasListPDDTO();

        sucursal.setFolio(rs.getInt("FIFOLIO"));
        sucursal.setNombreDocumento(rs.getString("FCNOMBRE_DOCTO"));
        sucursal.setTipoEnvio(rs.getString("FCTIPO_ENVIO"));
        sucursal.setFechaCreacionDocumento(rs.getString("FECHA_CREA_DOCTO"));
        sucursal.setFechaVisualizacionDocumento(rs.getString("FECHA_VISUAL_DOC"));
        sucursal.setFechaRecepcion(rs.getString("FECHA_RECEPCION"));
        sucursal.setEstatusDocumentoSucursal(rs.getString("ESTATUS_DOC_SUC"));

        return sucursal;
    }
}
