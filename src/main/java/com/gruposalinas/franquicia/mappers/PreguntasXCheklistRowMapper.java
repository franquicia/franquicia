package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ChecklistPreguntaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PreguntasXCheklistRowMapper implements RowMapper<ChecklistPreguntaDTO> {

    @Override
    public ChecklistPreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ChecklistPreguntaDTO checklistPreguntaDTO = new ChecklistPreguntaDTO();
        checklistPreguntaDTO.setIdPregunta(rs.getInt("ID_PREGUNTA"));
        checklistPreguntaDTO.setPregunta(rs.getString("PREGUNTA"));
        checklistPreguntaDTO.setIdTipoPregunta(rs.getInt("ID_TIPO_PREGUNTA"));
        //checklistPreguntaDTO.setDescTipo(rs.getString("TIPO_PREGUNTA"));
        checklistPreguntaDTO.setIdModulo(rs.getInt("ID_MODULO"));
        checklistPreguntaDTO.setNombreModulo(rs.getString("NOMBRE_MODULO"));
        checklistPreguntaDTO.setTipoCambio(rs.getString("FCTIPOMODIF"));
        return checklistPreguntaDTO;
    }

}
