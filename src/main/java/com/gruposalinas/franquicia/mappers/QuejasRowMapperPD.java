package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.QuejasPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class QuejasRowMapperPD implements RowMapper<QuejasPDDTO> {

    @Override
    public QuejasPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        QuejasPDDTO quejas = new QuejasPDDTO();

        quejas.setIdComentario(rs.getInt("FIIDCOMENTARIO"));
        quejas.setIdCeco(rs.getString("FCID_CECO"));
        quejas.setDescCeco(rs.getString("DESC_SUC"));
        quejas.setNombreCliente(rs.getString("FCNOM_CLIENTE"));
        quejas.setTelefono(rs.getString("FCTELEFONO"));
        quejas.setEmail(rs.getString("FCMAIL"));
        quejas.setIdCatComent(rs.getInt("FIIDCAT_COMENT"));
        quejas.setDesc(rs.getString("FCDESCRIPCION"));
        quejas.setComentario(rs.getString("FCCOMENTARIO"));
        quejas.setIdContacto(rs.getInt("FICONTACTO"));
        quejas.setIdEstatus(rs.getInt("FIESTATUS"));
        quejas.setFecha(rs.getString("FD_ACTUALIZACION"));
        quejas.setActivo(rs.getInt("FIACTIVO"));

        return quejas;
    }
}
