package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.AsignacionDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AsignacionRowMapper implements RowMapper<AsignacionDTO> {

    @Override
    public AsignacionDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        AsignacionDTO asignacionDTO = new AsignacionDTO();

        asignacionDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        asignacionDTO.setCeco(rs.getString("FCID_CECO"));
        asignacionDTO.setIdPuesto(rs.getInt("FIID_PUESTO"));
        asignacionDTO.setActivo(rs.getInt("FIACTIVO"));

        return asignacionDTO;
    }

}
