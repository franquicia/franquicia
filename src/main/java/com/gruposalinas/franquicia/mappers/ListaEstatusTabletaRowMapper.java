package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ListaEstatusTabletaPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ListaEstatusTabletaRowMapper implements RowMapper<ListaEstatusTabletaPDDTO> {

    @Override
    public ListaEstatusTabletaPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ListaEstatusTabletaPDDTO lista = new ListaEstatusTabletaPDDTO();

        lista.setIdEstatusPadre(rs.getInt("ID_EDO_TAB_PADRE"));
        lista.setDescPadreWeb(rs.getString("DES_PADRE_WEB"));
        lista.setDescPadreJson(rs.getString("DESC_PADRE_JSON"));
        lista.setIdEstatusTableta(rs.getInt("FIID_ESTADO_TAB"));
        lista.setDescWeb(rs.getString("FCDESC_WEB"));
        lista.setDescJson(rs.getString("FCDESC_JSON"));
        lista.setVisible(rs.getString("FCVISIBLE"));
        lista.setIdEstatus(rs.getInt("FIID_ESTATUS"));
        lista.setIdDependeDe(rs.getInt("FIDEPENDE_DE"));
        lista.setActivo(rs.getInt("FIACTIVO"));

        return lista;
    }
}
