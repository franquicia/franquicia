package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TipoChecklistDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TipoChecklistRowMapper implements RowMapper<TipoChecklistDTO> {

    public TipoChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        TipoChecklistDTO tipoChecklistDTO = new TipoChecklistDTO();

        tipoChecklistDTO.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
        tipoChecklistDTO.setDescTipo(rs.getString("FCDESCRIPCION"));

        return tipoChecklistDTO;
    }

}
