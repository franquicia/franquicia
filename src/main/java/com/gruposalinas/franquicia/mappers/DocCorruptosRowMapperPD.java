package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.DocCorruptosPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class DocCorruptosRowMapperPD implements RowMapper<DocCorruptosPDDTO> {

    @Override
    public DocCorruptosPDDTO mapRow(ResultSet rs, int arg1) throws SQLException {
        DocCorruptosPDDTO list = new DocCorruptosPDDTO();
        list.setFifolio(rs.getInt("FIFOLIO"));
        list.setFiid_documento(rs.getInt("FIID_DOCUMENTO"));
        list.setFcnombre_arc(rs.getString("FCNOMBRE_ARC"));
        list.setTipo_envio(rs.getString("TIPO_ENVIO"));
        list.setSituacion_docto(rs.getString("SITUACION_DOCTO"));
        list.setFcid_ceco(rs.getString("FCID_CECO"));
        list.setNom_ceco(rs.getString("NOM_CECO"));
        list.setId_categoria(rs.getInt("ID_CATEGORIA"));
        list.setDesc_categor(rs.getString("DESC_CATEGOR"));
        list.setFiid_tipo_docto(rs.getInt("FIID_TIPO_DOCTO"));
        list.setFcnombre_docto(rs.getString("FCNOMBRE_DOCTO"));
        list.setFiid_doctopedestal(rs.getInt("FIID_DOCTOPEDESTAL"));
        list.setVigencia_ini(rs.getString("VIGENCIA_INI"));
        list.setVigencia_fin(rs.getString("VIGENCIA_FIN"));
        list.setFipeso_bytes(rs.getInt("FIPESO_BYTES"));
        list.setEstatus_doc(rs.getInt("ESTATUS_DOC"));
        list.setDesc_estatus_doc(rs.getString("DESC_ESTATUS_DOC"));
        list.setEsta_doc_suc(rs.getInt("ESTA_DOC_SUC"));
        list.setDesc_esta_doc_suc(rs.getString("DESC_ESTA_DOC_SUC"));
        list.setFchash(rs.getString("FCHASH"));
        list.setUlt_fecha_doc_p(rs.getString("ULT_FECHA_DOC_P"));
        list.setActivo_docto(rs.getInt("ACTIVO_DOCTO"));

        return list;
    }
}
