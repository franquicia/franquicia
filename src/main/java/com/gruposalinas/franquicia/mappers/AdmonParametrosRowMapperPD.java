package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.AdminParametroPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AdmonParametrosRowMapperPD implements RowMapper<AdminParametroPDDTO> {

    @Override
    public AdminParametroPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        AdminParametroPDDTO parametros = new AdminParametroPDDTO();

        parametros.setIdParametro(rs.getInt("FIID_PARAMETRO"));
        parametros.setParametro(rs.getString("FIPARAMETRO"));
        parametros.setDescParametro(rs.getString("FCDESC_PARAMETRO"));
        parametros.setReferencia(rs.getString("FCREFERENCIA"));
        parametros.setValor(rs.getString("FCVALOR"));
        parametros.setUsuario(rs.getString("FCUSER"));
        parametros.setFecha(rs.getString("FDFECHA_CAMBIO"));
        parametros.setActivo(rs.getInt("FIACTIVO"));

        return parametros;
    }

}
