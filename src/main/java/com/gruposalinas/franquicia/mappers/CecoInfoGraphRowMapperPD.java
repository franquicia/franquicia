package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CecoInfoGraphPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CecoInfoGraphRowMapperPD implements RowMapper<CecoInfoGraphPDDTO> {

    @Override
    public CecoInfoGraphPDDTO mapRow(ResultSet rs, int arg1) throws SQLException {

        CecoInfoGraphPDDTO obj = new CecoInfoGraphPDDTO();
        obj.setN_suc_ok(rs.getInt("N_SUC_OK"));
        obj.setN_suc_probl(rs.getInt("N_SUC_PROBL"));
        obj.setN_suc_mante(rs.getInt("N_SUC_MANTE"));
        obj.setN_suc_total(rs.getInt("N_SUC_TOTAL"));
        obj.setN_ceco_nivel_infe(rs.getInt("N_CECO_NIVEL_INFE"));
        obj.setNivel_ceco(rs.getInt("NIVEL_CECO"));

        return obj;
    }
}
