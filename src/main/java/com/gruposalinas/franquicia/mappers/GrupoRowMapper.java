package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.GrupoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class GrupoRowMapper implements RowMapper<GrupoDTO> {

    public GrupoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        GrupoDTO grupo = new GrupoDTO();

        grupo.setIdGrupo(rs.getInt("FIID_GRUPO"));
        grupo.setDescripcion(rs.getString("FCDESCRIPCION"));

        return grupo;
    }

}
