package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.RepGralCecoPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class RepGralCecoRowMapperPD implements RowMapper<RepGralCecoPDDTO> {

    @Override
    public RepGralCecoPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        RepGralCecoPDDTO reporte = new RepGralCecoPDDTO();

        reporte.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        reporte.setIdCategoria(rs.getInt("ID_CATEGORIA"));
        reporte.setDescCategoria(rs.getString("DESC_CATEGORIA"));
        reporte.setIdTipoDoc(rs.getInt("FIID_TIPO_DOCTO"));
        reporte.setFechaEnv(rs.getString("FDFECHA_ENVIO"));
        reporte.setNombreDoc(rs.getString("FCNOMBRE_DOCTO"));
        reporte.setIdUsuario(rs.getInt("FIID_USUARIO"));
        reporte.setVisibleSuc(rs.getString("FCVISIBLE_SUC"));
        reporte.setDescVisibleSuc(rs.getString("DESC_VISIBLE_SUC"));
        reporte.setIdEstatus(rs.getInt("FIID_ESTATUS"));
        reporte.setDescEstatus(rs.getString("DESC_ESTATUS"));
        reporte.setNombreArch(rs.getString("FCNOMBRE_ARC"));
        reporte.setVigenciaIni(rs.getString("FDVIGENCIA_INI"));
        reporte.setVigenciaFin(rs.getString("FDVIGENCIA_FIN"));

        return reporte;
    }
}
