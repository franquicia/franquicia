package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.LlaveDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class LlaveRowMapper implements RowMapper<LlaveDTO> {

    public LlaveDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        LlaveDTO llaveDTO = new LlaveDTO();

        llaveDTO.setIdLlave(rs.getInt("FIID_LLAVE"));
        llaveDTO.setLlave(rs.getString("FCLLAVE"));
        llaveDTO.setDescripcion(rs.getString("FCDESCRIPCION"));

        return llaveDTO;
    }

}
