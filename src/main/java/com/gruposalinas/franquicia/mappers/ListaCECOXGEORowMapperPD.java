package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ListaCECOXGEOPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ListaCECOXGEORowMapperPD implements RowMapper<ListaCECOXGEOPDDTO> {

    @Override
    public ListaCECOXGEOPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ListaCECOXGEOPDDTO list = new ListaCECOXGEOPDDTO();

        list.setIdCeco(rs.getInt("FCID_CECO"));
        list.setDescCeco(rs.getString("FCNOMBRE"));
        list.setIdGeo(rs.getInt("FIID_GEOGRAFIA"));
        list.setDescGeo(rs.getString("FCDESCRIPCION"));

        return list;
    }
}
