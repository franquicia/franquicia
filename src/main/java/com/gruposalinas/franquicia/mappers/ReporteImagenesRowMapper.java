package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ReporteImagenesDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ReporteImagenesRowMapper implements RowMapper<ReporteImagenesDTO> {

    @Override
    public ReporteImagenesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ReporteImagenesDTO reporteImagenesDTO = new ReporteImagenesDTO();

        reporteImagenesDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
        reporteImagenesDTO.setIdModulo(rs.getInt("FIID_MODULO"));
        reporteImagenesDTO.setIdPreguntaPadre(rs.getInt("FIID_PREG_PADRE"));
        reporteImagenesDTO.setTextoAbiertaImagen(rs.getString("TX_TIMG"));
        reporteImagenesDTO.setImagenPrincipal(rs.getString("FCURL_IMG"));
        reporteImagenesDTO.setImagenReporte(rs.getString("IMG_REPORTE"));
        reporteImagenesDTO.setObservaciones(rs.getString("FCOBSERVACIONES"));
        return reporteImagenesDTO;

    }

}
