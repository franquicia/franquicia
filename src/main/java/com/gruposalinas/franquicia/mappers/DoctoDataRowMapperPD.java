package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.DoctoDataPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class DoctoDataRowMapperPD implements RowMapper<DoctoDataPDDTO> {

    @Override
    public DoctoDataPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        DoctoDataPDDTO lista = new DoctoDataPDDTO();
        lista.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        lista.setIdFolio(rs.getInt("FIFOLIO"));
        lista.setNombreDocto(rs.getString("FCNOMBRE_ARC"));
        lista.setTipoDocto(rs.getInt("FIID_TIPO_DOCTO"));
        lista.setDependeDe(rs.getInt("FIDEPENDE_DE"));
        lista.setCategoria(rs.getString("CATEGORIA"));
        lista.setIdUsuario(rs.getInt("FIID_USUARIO"));
        lista.setPeso(rs.getLong("FIPESO_BYTES"));
        lista.setOrigen(rs.getString("FCORIGEN"));
        lista.setVigenciaIni(rs.getString("VIGENCIA_INI"));
        lista.setVigenciaFin("VIGENCIA_FIN");
        lista.setIdEstatus(rs.getInt("ID_ESTATUS"));
        lista.setDescEstatus(rs.getString("DESC_ESTATUS"));
        lista.setActivo(rs.getInt("FIACTIVO"));

        return lista;
    }
}
