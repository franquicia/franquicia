package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.PerfilUsuarioDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PerfilUsuarioRowMapper implements RowMapper<PerfilUsuarioDTO> {

    @Override
    public PerfilUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();

        perfilUsuarioDTO.setIdPerfil(rs.getInt("FIIDPERFIL"));
        perfilUsuarioDTO.setIdUsuario(rs.getInt("FIIDUSUARIO"));

        return perfilUsuarioDTO;
    }

}
