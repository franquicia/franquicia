package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.RecursoPerfilDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class RecursoPerfilRowMapper implements RowMapper<RecursoPerfilDTO> {

    public RecursoPerfilDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        RecursoPerfilDTO recursoPerfilDTO = new RecursoPerfilDTO();

        recursoPerfilDTO.setIdRecurso(rs.getString("FIIDRECURSO"));
        recursoPerfilDTO.setIdPerfil(rs.getString("FIIDPERFIL"));
        recursoPerfilDTO.setConsulta(rs.getInt("FICONSULTA"));
        recursoPerfilDTO.setInserta(rs.getInt("FIINSERTA"));
        recursoPerfilDTO.setModifica(rs.getInt("FIMODIFICA"));
        recursoPerfilDTO.setElimina(rs.getInt("FIELIMINA"));

        return recursoPerfilDTO;
    }
}
