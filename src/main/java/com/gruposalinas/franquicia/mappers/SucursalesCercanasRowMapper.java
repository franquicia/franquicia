package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.SucursalesCercanasDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class SucursalesCercanasRowMapper implements RowMapper<SucursalesCercanasDTO> {

    @Override
    public SucursalesCercanasDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        SucursalesCercanasDTO sucursalesCercanasDTO = new SucursalesCercanasDTO();

        sucursalesCercanasDTO.setCeco(rs.getString("FCID_CECO"));
        sucursalesCercanasDTO.setNombreCeco(rs.getString("FCNOMBRE"));
        sucursalesCercanasDTO.setNuSucursal(rs.getString("FIIDNU_SUCURSAL"));
        sucursalesCercanasDTO.setLatitud(rs.getString("FCLATITUD"));
        sucursalesCercanasDTO.setLongitud(rs.getString("FCLONGITUD"));

        return sucursalesCercanasDTO;
    }

}
