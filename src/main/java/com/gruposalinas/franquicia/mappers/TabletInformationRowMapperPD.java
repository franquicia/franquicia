package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TabletInformationPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TabletInformationRowMapperPD implements RowMapper<TabletInformationPDDTO> {

    @Override
    public TabletInformationPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        TabletInformationPDDTO info = new TabletInformationPDDTO();

        info.setIdTableta(rs.getInt("FIID_TABLETA"));
        info.setNumSerie(rs.getString("FCNUM_SERIE"));
        info.setIdCeco(rs.getInt("FCID_CECO"));
        info.setNombreCeco(rs.getString("FCNOMBRE"));
        info.setIdEstatus(rs.getInt("FIID_ESTATUS"));
        info.setFechaInstalacion(rs.getString("FDFECHA_INSTALA"));
        info.setFabricante(rs.getString("FCFABRICANTE"));
        info.setModelo(rs.getString("FCMODELO"));
        info.setSistemaOperativo(rs.getString("FCSISTEMA_O"));
        info.setVersion(rs.getString("FCVERSION_APP"));
        info.setEspacioTotal(rs.getLong("FIESPACIO_TOT"));
        info.setEspacioDisp(rs.getLong("FIESPACIO_DISP"));
        info.setRamTotal(rs.getLong("FIRAM_TOTAL"));
        info.setRamDisp(rs.getLong("FIRAM_DISP"));
        info.setProblemaMant(rs.getString("PROBLEMA_MANT"));

        return info;
    }
}
