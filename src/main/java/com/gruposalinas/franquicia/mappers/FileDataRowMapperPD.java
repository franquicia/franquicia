package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.FileDataPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FileDataRowMapperPD implements RowMapper<FileDataPDDTO> {

    @Override
    public FileDataPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        FileDataPDDTO lista = new FileDataPDDTO();
        lista.setIdItem(rs.getInt("FIID_ITEM"));
        lista.setRuta(rs.getString("FCRUTA_ARCHIVO"));
        lista.setNombre(rs.getString("FCNOMBRE"));
        lista.setActivo(rs.getInt("FIACTIVO"));
        lista.setUsuarioModif(rs.getString("FCUSUARIO_MODIF"));
        lista.setFechaModif(rs.getString("FDFECHA_MODIF"));

        return lista;
    }
}
