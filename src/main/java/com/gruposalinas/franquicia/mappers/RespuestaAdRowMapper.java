package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.RespuestaAdDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class RespuestaAdRowMapper implements RowMapper<RespuestaAdDTO> {

    @Override
    public RespuestaAdDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        RespuestaAdDTO respuestaADDTO = new RespuestaAdDTO();

        respuestaADDTO.setIdRespuestaAd(rs.getInt("FIID_RESPUESTA_AD"));
        respuestaADDTO.setIdRespuesta(rs.getInt("FIID_RESPUESTA"));
        respuestaADDTO.setDescripcion(rs.getString("FCDESCRIPCION"));

        return respuestaADDTO;
    }

}
