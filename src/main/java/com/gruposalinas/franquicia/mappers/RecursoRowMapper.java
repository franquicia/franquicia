package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.RecursoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class RecursoRowMapper implements RowMapper<RecursoDTO> {

    public RecursoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        RecursoDTO recursoDTO = new RecursoDTO();

        recursoDTO.setIdRecurso(rs.getInt("FIIDRECURSO"));
        recursoDTO.setNombreRecurso(rs.getString("FCNOMBRE"));

        return recursoDTO;
    }
}
