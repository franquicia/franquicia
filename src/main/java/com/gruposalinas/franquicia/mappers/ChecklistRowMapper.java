package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.TipoChecklistDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ChecklistRowMapper implements RowMapper<ChecklistDTO> {

    public ChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ChecklistDTO checklistDTO = new ChecklistDTO();
        TipoChecklistDTO tipochecklist = new TipoChecklistDTO();

        tipochecklist.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
        tipochecklist.setDescTipo(rs.getString("FCDESCRIPCION"));

        checklistDTO.setIdTipoChecklist(tipochecklist);
        checklistDTO.setIdCheckUsua(rs.getInt("FIID_CHECK_USUA"));
        checklistDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        checklistDTO.setNombreCheck(rs.getString("FCNOMBRE"));
        checklistDTO.setZona(rs.getString("FCZONA"));
        checklistDTO.setFecha_inicio(rs.getString("FDFECHA_INICIO"));
        checklistDTO.setFecha_fin(rs.getString("FDFECHA_FIN"));
        checklistDTO.setIdCeco(rs.getString("FCID_CECO"));
        checklistDTO.setNombresuc(rs.getString("FCNOMBRECC"));
        checklistDTO.setLongitud(rs.getDouble("FCLONGITUD"));
        checklistDTO.setLatitud(rs.getDouble("FCLATITUD"));
        checklistDTO.setUltimaVisita(rs.getString("ULTVISITA"));

        return checklistDTO;
    }

}
