package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.RepDetaCecoPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class RepDetaCecoRowMapperPD implements RowMapper<RepDetaCecoPDDTO> {

    @Override
    public RepDetaCecoPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        RepDetaCecoPDDTO reporte = new RepDetaCecoPDDTO();
        reporte.setTerritorio(rs.getString("TERRITORIO"));
        reporte.setZona(rs.getString("ZONA"));
        reporte.setRegion(rs.getString("REGION"));
        reporte.setPais(rs.getString("PAIS"));
        reporte.setEstado(rs.getString("FCESTADO"));
        reporte.setMunicipio(rs.getString("FCMUNICIPIO"));
        reporte.setNombre(rs.getString("FCNOMBRE"));
        reporte.setIdSucursal(rs.getInt("FISUCURSAL"));
        reporte.setDescCategoria(rs.getString("DESC_CATEGORIA"));
        reporte.setFechaEnv(rs.getString("FDFECHA_ENVIO"));
        reporte.setNombreDoc(rs.getString("FCNOMBRE_DOCTO"));
        reporte.setDescVisibleSuc(rs.getString("DESC_VISIBLE_SUC"));
        reporte.setNombreArch(rs.getString("FCNOMBRE_ARC"));
        reporte.setVigenciaIni(rs.getString("FDVIGENCIA_INI"));
        reporte.setVigenciaFin(rs.getString("FDVIGENCIA_FIN"));
        return reporte;
    }
}
