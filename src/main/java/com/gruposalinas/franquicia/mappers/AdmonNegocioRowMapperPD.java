package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.AdminNegocioPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AdmonNegocioRowMapperPD implements RowMapper<AdminNegocioPDDTO> {

    @Override
    public AdminNegocioPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        AdminNegocioPDDTO negocio = new AdminNegocioPDDTO();

        negocio.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        negocio.setNegocio(rs.getString("FCDESC_NEGOCIO"));
        negocio.setUsuario(rs.getString("FCUSER"));
        negocio.setFechaCambio(rs.getString("FCFECHA_CAMBIO"));
        negocio.setActivo(rs.getInt("FIACTIVO"));

        return negocio;
    }

}
