package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ImagenesRespuestaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ImagenesRespuestaRowMapper implements RowMapper<ImagenesRespuestaDTO> {

    @Override
    public ImagenesRespuestaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ImagenesRespuestaDTO imagenesRespuestaDTO = new ImagenesRespuestaDTO();

        imagenesRespuestaDTO.setIdImagen(rs.getInt("FIID_IMG"));
        imagenesRespuestaDTO.setIdArbol(rs.getInt("FIID_ARBOL_DES"));
        imagenesRespuestaDTO.setUrlImg(rs.getString("FCURL_IMG"));
        imagenesRespuestaDTO.setDescripcion(rs.getString("FCDESCRIPCION"));

        return imagenesRespuestaDTO;
    }

}
