package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.Usuario_ADTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class UsuarioSucOfflineRowMapper implements RowMapper<Usuario_ADTO> {

    @Override
    public Usuario_ADTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        Usuario_ADTO usuarioADTO = new Usuario_ADTO();

        usuarioADTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        usuarioADTO.setNombre(rs.getString("FCNOMBRE"));
        usuarioADTO.setIdCeco(rs.getString("FCID_CECO"));
        usuarioADTO.setDesPuestof(rs.getString("FCDESCRIPCION"));
        usuarioADTO.setNombreCeco(rs.getString("NOMBRECECO"));

        return usuarioADTO;
    }

}
