package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.DatosUsuarioDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class DatosUsuarioRowMapper implements RowMapper<DatosUsuarioDTO> {

    @Override
    public DatosUsuarioDTO mapRow(ResultSet rs, int cont) throws SQLException {

        DatosUsuarioDTO datosUsuarioDTO = new DatosUsuarioDTO();

        datosUsuarioDTO.setNombre(rs.getString("FCNOMBRE"));
        datosUsuarioDTO.setUsuario(rs.getString("FIID_USUARIO"));
        datosUsuarioDTO.setCeco(rs.getString("FCID_CECO"));
        datosUsuarioDTO.setDescCeco(rs.getString("FCNOM_CECO"));
        datosUsuarioDTO.setPuesto(rs.getInt("FIID_PUESTO"));
        datosUsuarioDTO.setDescPuesto(rs.getString("FCDESCRIPCION"));
        datosUsuarioDTO.setPais(rs.getInt("FIID_PAIS"));
        datosUsuarioDTO.setTerritorio(rs.getString("FCID_TERRITORIO"));
        datosUsuarioDTO.setZona(rs.getString("FCID_ZONA"));
        datosUsuarioDTO.setRegion(rs.getString("FCID_REGION"));
        datosUsuarioDTO.setIdNivel(rs.getInt("NIVEL"));

        return datosUsuarioDTO;
    }

}
