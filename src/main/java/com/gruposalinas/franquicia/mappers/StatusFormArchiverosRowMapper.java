package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.StatusFormArchiveroDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class StatusFormArchiverosRowMapper implements RowMapper<StatusFormArchiveroDTO> {

    public StatusFormArchiveroDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        StatusFormArchiveroDTO formDTO = new StatusFormArchiveroDTO();

        formDTO.setIdStatus(rs.getInt("FISTATUS_ID"));
        formDTO.setDescripcion(rs.getString("FCDESCRIPCION"));

        return formDTO;
    }

}
