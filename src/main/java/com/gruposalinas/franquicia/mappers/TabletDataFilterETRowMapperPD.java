package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TabletDataFilterETDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TabletDataFilterETRowMapperPD implements RowMapper<TabletDataFilterETDTO> {

    @Override
    public TabletDataFilterETDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        TabletDataFilterETDTO data = new TabletDataFilterETDTO();

        data.setSucursal(rs.getInt("FISUCURSAL"));
        data.setNombre(rs.getString("FCNOMBRE"));
        data.setFechaInstalacion(rs.getString("FDFECHA_INSTALA"));
        data.setIdEstatus(rs.getInt("FIID_ESTATUS"));
        data.setDescIdEstatus(rs.getString("DESC_ID_ESTATUS"));
        data.setUltimaActualizacion(rs.getString("ULTIMA_ACTUAL"));
        data.setIdTableta(rs.getInt("FIID_TABLETA"));

        return data;
    }
}
