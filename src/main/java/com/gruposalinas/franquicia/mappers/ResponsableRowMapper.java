package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ExpedientesDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ResponsableRowMapper implements RowMapper<ExpedientesDTO> {

    public ExpedientesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ExpedientesDTO expedienteDTO = new ExpedientesDTO();

        expedienteDTO.setIdResponsable(rs.getInt("FIID_RESPONSABLE"));
        expedienteDTO.setIdentificador(rs.getInt("FIID_USUARIO"));
        expedienteDTO.setNombreResp(rs.getString("FCNOMBRE"));
        expedienteDTO.setEstatusResp(rs.getInt("FIESTATUSRES"));
        expedienteDTO.setIdExpediente(rs.getInt("FIID_EXPPASIVO"));

        return expedienteDTO;
    }

}
