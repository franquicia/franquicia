package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.PosPregDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PosPregRowMapper implements RowMapper<PosPregDTO> {

    public PosPregDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        PosPregDTO posPregDTO = new PosPregDTO();

        posPregDTO.setIdposPreg(rs.getInt("FIIDPOSIPREG"));
        posPregDTO.setIdPosible(rs.getInt("FIIDPOSIBLE"));
        posPregDTO.setTipoPreg(rs.getInt("FIID_TIPO_PREG"));

        return posPregDTO;
    }
}
