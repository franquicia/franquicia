package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CompromisoCecoDTO;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class SucursalChecklistRowMapper implements RowMapper<CompromisoCecoDTO> {

    public CompromisoCecoDTO mapRow(java.sql.ResultSet rs, int rowNum) throws SQLException {

        CompromisoCecoDTO cecoDTO = new CompromisoCecoDTO();

        cecoDTO.setIdCeco(rs.getString("FCID_CECO"));
        cecoDTO.setDescCeco(rs.getString("FCNOMBRE"));
        cecoDTO.setIdChecklist(rs.getString("FIID_CHECKLIST"));
        cecoDTO.setNombreChecklist(rs.getString("NOMBRECHECK"));

        return cecoDTO;
    }
}
