package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TipoArchivoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TipoArchivoRowMapper implements RowMapper<TipoArchivoDTO> {

    public TipoArchivoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        TipoArchivoDTO tipoArchivoDTO = new TipoArchivoDTO();

        tipoArchivoDTO.setIdTipoArchivo(rs.getInt("FIID_TIPO"));
        tipoArchivoDTO.setNombreTipo(rs.getString("FCNOMBRE"));

        return tipoArchivoDTO;
    }

}
