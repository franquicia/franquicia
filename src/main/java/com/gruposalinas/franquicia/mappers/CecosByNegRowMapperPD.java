package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CecosByNegPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CecosByNegRowMapperPD implements RowMapper<CecosByNegPDDTO> {

    @Override
    public CecosByNegPDDTO mapRow(ResultSet rs, int arg1) throws SQLException {

        CecosByNegPDDTO obj = new CecosByNegPDDTO();
        obj.setFcid_ceco(rs.getString("FCID_CECO"));
        obj.setFitipo_ceco(rs.getInt("FITIPO_CECO"));
        obj.setFcnombre(rs.getString("FCNOMBRE"));
        obj.setFiceco_superior(rs.getInt("FICECO_SUPERIOR"));
        obj.setDesc_depende_de(rs.getString("DESC_DEPENDE_DE"));

        return obj;
    }
}
