package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.FolioFilterDataPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FolioFilterDataRowMapperPD implements RowMapper<FolioFilterDataPDDTO> {

    @Override
    public FolioFilterDataPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        FolioFilterDataPDDTO folio = new FolioFilterDataPDDTO();

        folio.setIdFolio(rs.getInt("FIFOLIO"));
        folio.setTipoEnvio(rs.getString("TIPO_DE_ENVIO"));
        folio.setFechaEnvio(rs.getString("FDFECHA_ENVIO"));
        folio.setSucursalesAfectadas(rs.getInt("SUC_AFECTADAS"));
        folio.setEstatus(rs.getString("ESTATUS"));

        return folio;
    }
}
