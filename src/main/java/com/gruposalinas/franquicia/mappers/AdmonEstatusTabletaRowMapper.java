package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.AdmonEstatusTabletaPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AdmonEstatusTabletaRowMapper implements RowMapper<AdmonEstatusTabletaPDDTO> {

    @Override
    public AdmonEstatusTabletaPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        // TODO Auto-generated method stub
        AdmonEstatusTabletaPDDTO admonStatusTableta = new AdmonEstatusTabletaPDDTO();

        admonStatusTableta.setIdEstadoTableta(rs.getInt("FIID_ESTADO_TAB"));
        admonStatusTableta.setDescWEB(rs.getString("FCDESC_WEB"));
        admonStatusTableta.setDescJSON(rs.getString("FCDESC_JSON"));
        admonStatusTableta.setVisible(rs.getString("FCVISIBLE"));
        admonStatusTableta.setIdStatus(rs.getInt("FIID_ESTATUS"));
        admonStatusTableta.setUsuario(rs.getString("FCUSER"));
        admonStatusTableta.setFechaCambio(rs.getString("FDFECHA_CAMBIO"));
        admonStatusTableta.setDependeDe(rs.getInt("FIDEPENDE_DE"));

        return admonStatusTableta;
    }

}
