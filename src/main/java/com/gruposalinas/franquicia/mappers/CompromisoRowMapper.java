package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CompromisoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CompromisoRowMapper implements RowMapper<CompromisoDTO> {

    public CompromisoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        CompromisoDTO compromisoDTO = new CompromisoDTO();

        compromisoDTO.setIdCompromiso(rs.getInt("FIID_COMPROMISO"));
        compromisoDTO.setIdRespuesta(rs.getInt("FIID_RESPUESTA"));
        compromisoDTO.setDescripcion(rs.getString("FCDESCRIPCION"));
        compromisoDTO.setEstatus(rs.getInt("FIESTATUS"));
        compromisoDTO.setFechaCompromiso(rs.getString("FDFECHA_COMP"));
        compromisoDTO.setIdResponsable(rs.getInt("FIID_RESPONSABLE"));
        return compromisoDTO;
    }
}
