package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.IndicadoresDataETDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class IndicadoresDataETRowMapperPD implements RowMapper<IndicadoresDataETDTO> {

    @Override
    public IndicadoresDataETDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        IndicadoresDataETDTO data = new IndicadoresDataETDTO();

        try {
            data.setTabletasCorrectas(rs.getInt("VL_TAB_CORREC"));
        } catch (Exception e) {
            data.setTabletasCorrectas(0);
        }

        try {
            data.setTabletasIncorrectas(rs.getInt("VL_TAB_INCORREC"));
        } catch (Exception e) {
            data.setTabletasIncorrectas(0);
        }

        try {
            data.setTabletasMantenimiento(rs.getInt("VL_TAB_MANTEN"));
        } catch (Exception e) {
            data.setTabletasMantenimiento(0);
        }

        return data;
    }
}
