package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.FoliosEstatusPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FoliosEstatusRowMapperPD implements RowMapper<FoliosEstatusPDDTO> {

    @Override
    public FoliosEstatusPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        FoliosEstatusPDDTO data = new FoliosEstatusPDDTO();

        data.setIdTipoDocto(rs.getInt("FIID_TIPO_DOCTO"));
        data.setNombreDocto(rs.getString("FCNOMBRE_DOCTO"));
        data.setDependeDe(rs.getInt("FIDEPENDE_DE"));
        data.setActivo(rs.getInt("FIACTIVO"));

        return data;
    }
}
