package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.TipoChecklistDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ChecklistOfflineRowMapper implements RowMapper<ChecklistDTO> {

    public ChecklistDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ChecklistDTO checklistDTO = new ChecklistDTO();
        TipoChecklistDTO tipochecklist = new TipoChecklistDTO();

        checklistDTO.setIdTipoChecklist(tipochecklist);
        tipochecklist.setIdTipoCheck(rs.getInt("FIID_TIPO_CHECK"));
        checklistDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        checklistDTO.setNombreCheck(rs.getString("FCNOMBRE"));
        checklistDTO.setIdHorario(rs.getInt("FIID_HORARIO"));
        checklistDTO.setVigente(rs.getInt("FIVIGENTE"));
        checklistDTO.setFecha_inicio(rs.getString("FDFECHA_INICIO"));

        checklistDTO.setFecha_fin(rs.getString("FDFECHA_FIN"));
        checklistDTO.setIdEstado(rs.getInt("FIID_ESTADO"));
        checklistDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        checklistDTO.setDia(rs.getString("FCDIA"));
        checklistDTO.setPeriodo(rs.getString("FCPERIODO"));
        checklistDTO.setVersion(rs.getString("FIVERSION"));
        checklistDTO.setOrdeGrupo(rs.getString("FIORDEN_GRUPO"));

        return checklistDTO;
    }
}
