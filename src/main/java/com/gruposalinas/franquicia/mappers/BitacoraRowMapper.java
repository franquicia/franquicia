package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.BitacoraDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class BitacoraRowMapper implements RowMapper<BitacoraDTO> {

    public BitacoraDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        BitacoraDTO bitacoraDTO = new BitacoraDTO();

        bitacoraDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
        bitacoraDTO.setIdCheckUsua(rs.getInt("FIID_CHECK_USUA"));
        bitacoraDTO.setFechaInicio(rs.getString("FDINICIO"));
        bitacoraDTO.setFechaFin(rs.getString("FDTERMINO"));
        bitacoraDTO.setLatitud(rs.getString("FCLATITUD"));
        bitacoraDTO.setLongitud(rs.getString("FCLONGITUD"));

        return bitacoraDTO;
    }

}
