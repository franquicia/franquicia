package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ChecklistUsuarioDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CheckUOfflineRowMapper implements RowMapper<ChecklistUsuarioDTO> {

    public ChecklistUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ChecklistUsuarioDTO checklistUsuarioDTO = new ChecklistUsuarioDTO();

        checklistUsuarioDTO.setIdCheckUsuario(rs.getInt("FIID_CHECK_USUA"));
        checklistUsuarioDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        checklistUsuarioDTO.setIdUsuario(rs.getInt("FIID_USUARIO"));
        checklistUsuarioDTO.setIdCeco(rs.getInt("FCID_CECO"));

        return checklistUsuarioDTO;
    }

}
