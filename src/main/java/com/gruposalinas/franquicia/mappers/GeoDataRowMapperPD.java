package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.GeoDataPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class GeoDataRowMapperPD implements RowMapper<GeoDataPDDTO> {

    @Override
    public GeoDataPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        GeoDataPDDTO data = new GeoDataPDDTO();

        data.setIdGeografia(rs.getInt("FIID_GEOGRAFIA"));
        data.setTipo(rs.getInt("FITIPO"));
        data.setIdcc(rs.getInt("FIIDCC"));
        data.setDescripcion(rs.getString("FCDESCRIPCION"));
        data.setDependeDe(rs.getInt("FIDEPENDE_DE"));
        data.setUser(rs.getString("FCUSER"));
        data.setFechaCambio(rs.getString("FDFECHA_CAMBIO"));
        data.setActivo(rs.getInt("FIACTIVO"));

        return data;
    }
}
