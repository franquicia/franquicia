package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.FolioDetailPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class FolioDetailRowMapperPD implements RowMapper<FolioDetailPDDTO> {

    @Override
    public FolioDetailPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        FolioDetailPDDTO folio = new FolioDetailPDDTO();

        folio.setIdFolio(rs.getInt("FIFOLIO"));
        folio.setFechaVisualizacion(rs.getString("FECHA_VISUALIZACION"));
        folio.setFechaFin(rs.getString("FECHA_FIN"));
        folio.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        folio.setNombreArchivo(rs.getString("FCNOMBRE_ARC"));
        folio.setIdTipoDocumento(rs.getInt("FIID_TIPO_DOCTO"));
        folio.setNombreDocumento(rs.getString("FCNOMBRE_DOCTO"));
        folio.setVisibleSucursal(rs.getString("FCVISIBLE_SUC"));
        folio.setVisibleSucursalDesc(rs.getString("FCVISIBLE_SUC_D"));
        folio.setTotalSucursal(rs.getInt("TOTAL_SUC"));
        folio.setDocVisSuc(rs.getInt("SUC_VIS_DOC"));
        folio.setDocNVisSuc(rs.getInt("SUC_N_VIS_DOC"));
        folio.setEstatus(rs.getString("ESTATUS"));
        folio.setIdCategoria(rs.getInt("ID_CATEGORIA"));
        folio.setCategoriaDesc(rs.getString("DESC_CATEGORIA"));

        return folio;
    }
}
