package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ChecklistPreguntaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CheckPregOfflineRowMapper implements RowMapper<ChecklistPreguntaDTO> {

    public ChecklistPreguntaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ChecklistPreguntaDTO checklistPreguntaDTO = new ChecklistPreguntaDTO();

        checklistPreguntaDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        checklistPreguntaDTO.setIdPregunta(rs.getInt("FIID_PREGUNTA"));
        checklistPreguntaDTO.setPregunta(rs.getString("FCDESCRIPCION"));
        checklistPreguntaDTO.setIdModulo(rs.getInt("FIID_MODULO"));
        checklistPreguntaDTO.setIdTipoPregunta(rs.getInt("FIID_TIPO_PREG"));
        checklistPreguntaDTO.setOrdenPregunta(rs.getInt("FIORDEN_CHECK"));
        checklistPreguntaDTO.setPregPadre(rs.getInt("FIID_PREG_PADRE"));
        checklistPreguntaDTO.setActivo(rs.getInt("FIESTATUS"));

        return checklistPreguntaDTO;
    }

}
