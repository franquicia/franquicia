package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.VisitaTiendaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class VisitaTiendaRowMapper implements RowMapper<VisitaTiendaDTO> {

    @Override
    public VisitaTiendaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        VisitaTiendaDTO visitaTienda = new VisitaTiendaDTO();

        visitaTienda.setIdBitacora(rs.getInt("FIID_BITACORA"));
        visitaTienda.setIdCheckusa(rs.getInt("FIID_CHECK_USUA"));
        visitaTienda.setIdCeco(rs.getInt("FCID_CECO"));
        visitaTienda.setNombreCeco(rs.getString("FCNOMBRE"));
        visitaTienda.setFechaInicio(rs.getString("FECHA_INICIO"));
        visitaTienda.setFechaFin(rs.getString("FECHA_FIN"));
        visitaTienda.setFechaEnvio(rs.getString("FECHA_ENVIO"));
        visitaTienda.setUltimaVisita(rs.getString("ULTIMA_VISITA"));

        return visitaTienda;
    }

}
