package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.TablaEstatusGeneralDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class TablaEstatusGeneralRowMapperPD implements RowMapper<TablaEstatusGeneralDTO> {

    @Override
    public TablaEstatusGeneralDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        TablaEstatusGeneralDTO tabla = new TablaEstatusGeneralDTO();

        tabla.setNumEconomico(rs.getInt("FISUCURSAL"));
        tabla.setNombre(rs.getString("FCNOMBRE"));
        tabla.setNumSerie(rs.getString("FCNUM_SERIE"));
        tabla.setFechaInstalacion(rs.getString("FDFECHA_INSTALA"));
        tabla.setEstatus(rs.getString("FIID_ESTATUS"));
        tabla.setActivo(rs.getString("FIACTIVO"));
        tabla.setUltimaActual(rs.getString("ULTIMA_ACTUAL"));

        return tabla;
    }
}
