package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ListaDistribucionPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ListaDistribucionRowMapperPD implements RowMapper<ListaDistribucionPDDTO> {

    @Override
    public ListaDistribucionPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        ListaDistribucionPDDTO listaDistribucion = new ListaDistribucionPDDTO();

        listaDistribucion.setId_usuario(rs.getInt("FIID_USUARIO"));
        listaDistribucion.setNegocio(rs.getInt("FIID_NEGOCIO"));
        listaDistribucion.setId_listaDistribucion(rs.getInt("FIID_LISTA_DISTR"));
        listaDistribucion.setActivo(rs.getInt("FIACTIVO"));
        listaDistribucion.setNombre(rs.getString("FCNOMBRE"));

        return listaDistribucion;
    }
}
