package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.SucursalInfoPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class SucursalInfoRowMapperPD implements RowMapper<SucursalInfoPDDTO> {

    @Override
    public SucursalInfoPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        SucursalInfoPDDTO sucursal = new SucursalInfoPDDTO();

        sucursal.setIdTableta(rs.getInt("FIID_TABLETA"));
        sucursal.setSucursal(rs.getString("SUCURSAL"));
        sucursal.setIdCeco(rs.getString("FCID_CECO"));
        sucursal.setDireccion(rs.getString("DIRECCION"));
        sucursal.setFechaInstalacion(rs.getString("FDFECHA_INSTALA"));
        sucursal.setUltimaActualizacion(rs.getString("ULTIMA_ACTUALIZA"));

        return sucursal;
    }
}
