package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.BusquedaGeoDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class BusquedaGeoRowMapperPD implements RowMapper<BusquedaGeoDTO> {

    @Override
    public BusquedaGeoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        BusquedaGeoDTO geo = new BusquedaGeoDTO();

        geo.setId_geolocalizacion(rs.getInt("FIID_GEOGRAFIA"));
        geo.setTipo(rs.getInt("FITIPO"));
        geo.setDescripcion(rs.getString("FCDESCRIPCION"));
        geo.setIdDependeDe(rs.getInt("FIDEPENDE_DE"));
        geo.setDependeDeDesc(rs.getString("DESC_DEPENDE_DE"));

        return geo;
    }
}
