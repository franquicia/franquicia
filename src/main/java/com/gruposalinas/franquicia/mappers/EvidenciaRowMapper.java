package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.EvidenciaDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class EvidenciaRowMapper implements RowMapper<EvidenciaDTO> {

    public EvidenciaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        EvidenciaDTO evidenciaDTO = new EvidenciaDTO();

        evidenciaDTO.setIdEvidencia(rs.getInt("FIIDEVIDENCIA"));
        evidenciaDTO.setIdRespuesta(rs.getInt("FIID_RESPUESTA"));
        evidenciaDTO.setIdTipo(rs.getInt("FIID_TIPO"));
        evidenciaDTO.setRuta(rs.getString("FCRUTA"));

        return evidenciaDTO;

    }
}
