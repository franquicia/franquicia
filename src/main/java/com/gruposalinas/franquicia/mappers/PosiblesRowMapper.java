package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.PosiblesDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PosiblesRowMapper implements RowMapper<PosiblesDTO> {

    public PosiblesDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        PosiblesDTO posiblesDTO = new PosiblesDTO();

        posiblesDTO.setIdPosible(rs.getInt("FIIDPOSIBLE"));
        posiblesDTO.setDescripcion(rs.getString("FCDESCRIPCION"));

        return posiblesDTO;
    }

}
