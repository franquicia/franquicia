package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ChecklistUsuarioDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CheckUsuaOfflineRowMapper implements RowMapper<ChecklistUsuarioDTO> {

    public ChecklistUsuarioDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ChecklistUsuarioDTO checklistUsuarioDTO = new ChecklistUsuarioDTO();

        checklistUsuarioDTO.setIdCheckUsuario(rs.getInt("FIID_CHECK_USUA"));
        checklistUsuarioDTO.setIdChecklist(rs.getInt("FIID_CHECKLIST"));
        checklistUsuarioDTO.setIdBitacora(rs.getInt("FIID_BITACORA"));
        checklistUsuarioDTO.setUltimaVisita(rs.getString("ULTVISITA"));

        return checklistUsuarioDTO;
    }

}
