package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.BatchDoctoPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class BatchDoctoRowMapperPD implements RowMapper<BatchDoctoPDDTO> {

    @Override
    public BatchDoctoPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        BatchDoctoPDDTO doc = new BatchDoctoPDDTO();

        doc.setIdDocumento(rs.getInt("FIID_DOCUMENTO"));
        doc.setIdTipoDocto(rs.getInt("FIID_TIPO_DOCTO"));
        doc.setOrigen(rs.getString("FCORIGEN"));
        doc.setVigenciaIni(rs.getString("FDVIGENCIA_INI"));
        doc.setVigenciaFin(rs.getString("FDVIGENCIA_FIN"));
        doc.setVisibleSuc(rs.getString("FCVISIBLE_SUC"));
        doc.setTipoEnvio(rs.getString("FCTIPO_ENVIO"));

        return doc;
    }
}
