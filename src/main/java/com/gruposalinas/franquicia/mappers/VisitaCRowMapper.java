package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.VistaCDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class VisitaCRowMapper implements RowMapper<VistaCDTO> {

    public VistaCDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        VistaCDTO vista = new VistaCDTO();

        vista.setIdCeco(rs.getString("FCID_CECO"));
        vista.setDescCeco(rs.getString("FCNOMBRE"));
        vista.setTotal(rs.getInt("CONTEO"));
        vista.setIdChecklist(rs.getString("FIID_CHECKLIST"));
        vista.setNumSuc(rs.getString("NOSUCURSAL"));
        vista.setAsignados(rs.getInt("ASIGNADOS"));
        vista.setTerminados(rs.getInt("TERMINADOS"));

        return vista;
    }
}
