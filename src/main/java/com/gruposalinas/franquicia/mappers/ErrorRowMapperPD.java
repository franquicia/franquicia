package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.ErrorPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ErrorRowMapperPD implements RowMapper<ErrorPDDTO> {

    @Override
    public ErrorPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {

        ErrorPDDTO error = new ErrorPDDTO();
        error.setMsj(rs.getString("IDTABLA"));

        return error;
    }
}
