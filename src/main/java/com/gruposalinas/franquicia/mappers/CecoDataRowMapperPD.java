package com.gruposalinas.franquicia.mappers;

import com.gruposalinas.franquicia.domain.CecoDataPDDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class CecoDataRowMapperPD implements RowMapper<CecoDataPDDTO> {

    @Override
    public CecoDataPDDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        CecoDataPDDTO ceco = new CecoDataPDDTO();

        ceco.setIdCeco(rs.getString("FCID_CECO"));
        ceco.setIdSucursal(rs.getInt("FISUCURSAL"));
        ceco.setNombre(rs.getString("FCNOMBRE"));
        ceco.setIdCecoPadre(rs.getInt("FICECO_PADRE"));
        ceco.setIdPais(rs.getInt("FIID_PAIS"));
        ceco.setIdEstado(rs.getInt("FIID_ESTADO"));
        ceco.setEstado(rs.getString("FCESTADO"));
        ceco.setIdMunicipio(rs.getInt("FIID_MUNICIPIO"));
        ceco.setMunicipio(rs.getString("FCMUNICIPIO"));
        ceco.setCalle(rs.getString("FCCALLE"));
        ceco.setNumExt(rs.getString("FCNUMERO_EXTERIOR"));
        ceco.setNumInt(rs.getString("FCNUMERO_INTERIOR"));
        ceco.setColonia(rs.getString("FCCOLONIA"));
        ceco.setNivelCeco(rs.getInt("FINIVEL_CECO"));
        ceco.setIdNegocio(rs.getInt("FIID_NEGOCIO"));
        ceco.setIdCanal(rs.getInt("FIID_CANAL"));
        ceco.setIdResponsable(rs.getInt("FIRESPONSABLE"));
        ceco.setCp(rs.getString("FCCP"));
        ceco.setIdGeografia(rs.getInt("FIID_GEOGRAFIA"));
        ceco.setUser(rs.getString("FUSER"));
        ceco.setFechaCambio(rs.getString("FDCAMBIO_FECHA"));
        ceco.setActivo(rs.getInt("FIACTIVO"));

        return ceco;
    }
}
