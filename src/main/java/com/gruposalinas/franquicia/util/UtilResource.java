package com.gruposalinas.franquicia.util;

import com.gruposalinas.franquicia.resources.FRQAppContextProvider;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import com.gruposalinas.franquicia.resources.FRQResourceLoader;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

public class UtilResource {

    public static Resource getResourceFromInternet(String paginaEstatica) {
        ApplicationContext appContext = FRQAppContextProvider.getApplicationContext();
        FRQResourceLoader resourceLoader = (FRQResourceLoader) appContext.getBean("frqResourceLoader");
        //Resource resource = resourceLoader.getResourceLoader().getResource("http://ipdelserver/static/prueba.html");
        Resource resource = resourceLoader.getResourceLoader().getResource(FRQConstantes.getURLServer() + "/franquicia/static/" + paginaEstatica);
        return resource;
    }
}
