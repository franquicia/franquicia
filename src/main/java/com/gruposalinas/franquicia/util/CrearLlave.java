package com.gruposalinas.franquicia.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

public class CrearLlave {

    public static void main(String[] args) throws NoSuchAlgorithmException {

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);
        String fecha2 = (fecha.substring(6, 10) + fecha.substring(3, 5) + fecha.substring(0, 2));

        String validator = fecha2 + "appmovil" + "sistemaios";
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(validator.getBytes());
        byte byteData[] = md.digest();
        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        System.out.println("Digest(in hex format): " + sb.toString());
    }

}
