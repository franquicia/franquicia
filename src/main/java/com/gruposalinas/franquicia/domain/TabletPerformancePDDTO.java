package com.gruposalinas.franquicia.domain;

public class TabletPerformancePDDTO {

    private int idTableta;
    private String descPadreWeb;
    private String descWeb;
    private int idEstatus;
    private String visible;
    private String ultimaActualizacion;

    public int getIdTableta() {
        return idTableta;
    }

    public void setIdTableta(int idTableta) {
        this.idTableta = idTableta;
    }

    public String getDescPadreWeb() {
        return descPadreWeb;
    }

    public void setDescPadreWeb(String descPadreWeb) {
        this.descPadreWeb = descPadreWeb;
    }

    public String getDescWeb() {
        return descWeb;
    }

    public void setDescWeb(String descWeb) {
        this.descWeb = descWeb;
    }

    public int getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
        this.idEstatus = idEstatus;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public String getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(String ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }
}
