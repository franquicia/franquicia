package com.gruposalinas.franquicia.domain;

public class CartaAsignacionAFDTO {

    private int anioActual;
    private int anioPasado;
    private String trimestre;
    private int idCeco;
    private String fecha;
    private int idCarta;
    private String nombreArc;
    private String ruta;

    public int getAnioActual() {
        return anioActual;
    }

    public void setAnioActual(int anioActual) {
        this.anioActual = anioActual;
    }

    public int getAnioPasado() {
        return anioPasado;
    }

    public void setAnioPasado(int anioPasado) {
        this.anioPasado = anioPasado;
    }

    public String getTrimestre() {
        return trimestre;
    }

    public void setTrimestre(String trimestre) {
        this.trimestre = trimestre;
    }

    public int getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(int idCeco) {
        this.idCeco = idCeco;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdCarta() {
        return idCarta;
    }

    public void setIdCarta(int idCarta) {
        this.idCarta = idCarta;
    }

    public String getNombreArc() {
        return nombreArc;
    }

    public void setNombreArc(String nombreArc) {
        this.nombreArc = nombreArc;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

}
