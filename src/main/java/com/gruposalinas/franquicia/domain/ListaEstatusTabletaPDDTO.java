package com.gruposalinas.franquicia.domain;

public class ListaEstatusTabletaPDDTO {

    private int idEstatusPadre;
    private String descPadreWeb;
    private String descPadreJson;
    private int idEstatusTableta;
    private String descWeb;
    private String descJson;
    private String visible;
    private int idEstatus;
    private int idDependeDe;
    private int activo;

    public int getIdEstatusPadre() {
        return idEstatusPadre;
    }

    public void setIdEstatusPadre(int idEstatusPadre) {
        this.idEstatusPadre = idEstatusPadre;
    }

    public String getDescPadreWeb() {
        return descPadreWeb;
    }

    public void setDescPadreWeb(String descPadreWeb) {
        this.descPadreWeb = descPadreWeb;
    }

    public String getDescPadreJson() {
        return descPadreJson;
    }

    public void setDescPadreJson(String descPadreJson) {
        this.descPadreJson = descPadreJson;
    }

    public int getIdEstatusTableta() {
        return idEstatusTableta;
    }

    public void setIdEstatusTableta(int idEstatusTableta) {
        this.idEstatusTableta = idEstatusTableta;
    }

    public String getDescWeb() {
        return descWeb;
    }

    public void setDescWeb(String descWeb) {
        this.descWeb = descWeb;
    }

    public String getDescJson() {
        return descJson;
    }

    public void setDescJson(String descJson) {
        this.descJson = descJson;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public int getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
        this.idEstatus = idEstatus;
    }

    public int getIdDependeDe() {
        return idDependeDe;
    }

    public void setIdDependeDe(int idDependeDe) {
        this.idDependeDe = idDependeDe;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
