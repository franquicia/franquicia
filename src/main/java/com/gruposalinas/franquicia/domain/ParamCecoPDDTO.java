package com.gruposalinas.franquicia.domain;

public class ParamCecoPDDTO {

    private Integer fiid_parametro;
    private Integer fiparametro;
    private String fcdesc_parametro;
    private String fcvalor;
    private String fcreferencia;
    private Integer fiactivo;

    public Integer getFiid_parametro() {
        return fiid_parametro;
    }

    public void setFiid_parametro(Integer fiid_parametro) {
        this.fiid_parametro = fiid_parametro;
    }

    public Integer getFiparametro() {
        return fiparametro;
    }

    public void setFiparametro(Integer fiparametro) {
        this.fiparametro = fiparametro;
    }

    public String getFcdesc_parametro() {
        return fcdesc_parametro;
    }

    public void setFcdesc_parametro(String fcdesc_parametro) {
        this.fcdesc_parametro = fcdesc_parametro;
    }

    public String getFcvalor() {
        return fcvalor;
    }

    public void setFcvalor(String fcvalor) {
        this.fcvalor = fcvalor;
    }

    public String getFcreferencia() {
        return fcreferencia;
    }

    public void setFcreferencia(String fcreferencia) {
        this.fcreferencia = fcreferencia;
    }

    public Integer getFiactivo() {
        return fiactivo;
    }

    public void setFiactivo(Integer fiactivo) {
        this.fiactivo = fiactivo;
    }
}
