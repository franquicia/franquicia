package com.gruposalinas.franquicia.domain;

public class ReporteAreaApoyoDTO {

    private int idformato;
    private String ceco;
    private String areainv;
    private String folio;
    private String prioridad;
    private String fecha;

    public int getIdformato() {
        return idformato;
    }

    public void setIdformato(int idformato) {
        this.idformato = idformato;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public String getAreainv() {
        return areainv;
    }

    public void setAreainv(String areainv) {
        this.areainv = areainv;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
