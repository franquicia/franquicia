package com.gruposalinas.franquicia.domain;

public class BatchDoctoPDDTO {

    private int idDocumento;
    private int idTipoDocto;
    private String origen;
    private String vigenciaIni;
    private String vigenciaFin;
    private String visibleSuc;
    private String tipoEnvio;

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public int getIdTipoDocto() {
        return idTipoDocto;
    }

    public void setIdTipoDocto(int idTipoDocto) {
        this.idTipoDocto = idTipoDocto;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getVigenciaIni() {
        return vigenciaIni;
    }

    public void setVigenciaIni(String vigenciaIni) {
        this.vigenciaIni = vigenciaIni;
    }

    public String getVigenciaFin() {
        return vigenciaFin;
    }

    public void setVigenciaFin(String vigenciaFin) {
        this.vigenciaFin = vigenciaFin;
    }

    public String getVisibleSuc() {
        return visibleSuc;
    }

    public void setVisibleSuc(String visibleSuc) {
        this.visibleSuc = visibleSuc;
    }

    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }
}
