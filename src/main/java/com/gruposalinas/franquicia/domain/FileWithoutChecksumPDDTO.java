package com.gruposalinas.franquicia.domain;

public class FileWithoutChecksumPDDTO {

    private Integer fiid_documento;
    private Integer categoria;
    private String fcnombre_arch;

    public Integer getFiid_documento() {
        return fiid_documento;
    }

    public void setFiid_documento(Integer fiid_documento) {
        this.fiid_documento = fiid_documento;
    }

    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }

    public String getFcnombre_arch() {
        return fcnombre_arch;
    }

    public void setFcnombre_arch(String fcnombre_arch) {
        this.fcnombre_arch = fcnombre_arch;
    }
}
