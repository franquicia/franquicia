package com.gruposalinas.franquicia.domain;

public class HistoricTabletDataPDDTO {

    private int idTableta;
    private String numSerie;
    private int idTabletaEdo;
    private int idTabletaEdoPadre;
    private String descTabletaEdoPadreWeb;
    private String descTabletaEdoPadreJson;
    private int idEstadoTab;
    private String descEstadoTabWeb;
    private String descEstadoTabJson;
    private String visible;
    private int idEstatus;
    private int dependeDe;
    private String fecha;
    private int activo;

    public int getIdTableta() {
        return idTableta;
    }

    public void setIdTableta(int idTableta) {
        this.idTableta = idTableta;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }

    public int getIdTabletaEdo() {
        return idTabletaEdo;
    }

    public void setIdTabletaEdo(int idTabletaEdo) {
        this.idTabletaEdo = idTabletaEdo;
    }

    public int getIdTabletaEdoPadre() {
        return idTabletaEdoPadre;
    }

    public void setIdTabletaEdoPadre(int idTabletaEdoPadre) {
        this.idTabletaEdoPadre = idTabletaEdoPadre;
    }

    public String getDescTabletaEdoPadreWeb() {
        return descTabletaEdoPadreWeb;
    }

    public void setDescTabletaEdoPadreWeb(String descTabletaEdoPadreWeb) {
        this.descTabletaEdoPadreWeb = descTabletaEdoPadreWeb;
    }

    public String getDescTabletaEdoPadreJson() {
        return descTabletaEdoPadreJson;
    }

    public void setDescTabletaEdoPadreJson(String descTabletaEdoPadreJson) {
        this.descTabletaEdoPadreJson = descTabletaEdoPadreJson;
    }

    public int getIdEstadoTab() {
        return idEstadoTab;
    }

    public void setIdEstadoTab(int idEstadoTab) {
        this.idEstadoTab = idEstadoTab;
    }

    public String getDescEstadoTabWeb() {
        return descEstadoTabWeb;
    }

    public void setDescEstadoTabWeb(String descEstadoTabWeb) {
        this.descEstadoTabWeb = descEstadoTabWeb;
    }

    public String getDescEstadoTabJson() {
        return descEstadoTabJson;
    }

    public void setDescEstadoTabJson(String descEstadoTabJson) {
        this.descEstadoTabJson = descEstadoTabJson;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public int getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
        this.idEstatus = idEstatus;
    }

    public int getDependeDe() {
        return dependeDe;
    }

    public void setDependeDe(int dependeDe) {
        this.dependeDe = dependeDe;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
