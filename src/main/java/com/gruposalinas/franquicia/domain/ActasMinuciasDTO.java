package com.gruposalinas.franquicia.domain;

public class ActasMinuciasDTO {

    private int idActa;
    private String fecha;
    private String direccion;
    private String lugarHechos;
    private String testigo1;
    private String testigo2;
    private String destruye;
    private String vobo;

    public int getIdActa() {
        return idActa;
    }

    public void setIdActa(int idActa) {
        this.idActa = idActa;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLugarHechos() {
        return lugarHechos;
    }

    public void setLugarHechos(String lugarHechos) {
        this.lugarHechos = lugarHechos;
    }

    public String getTestigo1() {
        return testigo1;
    }

    public void setTestigo1(String testigo1) {
        this.testigo1 = testigo1;
    }

    public String getTestigo2() {
        return testigo2;
    }

    public void setTestigo2(String testigo2) {
        this.testigo2 = testigo2;
    }

    public String getDestruye() {
        return destruye;
    }

    public void setDestruye(String destruye) {
        this.destruye = destruye;
    }

    public String getVobo() {
        return vobo;
    }

    public void setVobo(String vobo) {
        this.vobo = vobo;
    }
}
