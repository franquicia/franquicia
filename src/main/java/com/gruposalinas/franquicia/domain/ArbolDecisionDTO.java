package com.gruposalinas.franquicia.domain;

public class ArbolDecisionDTO {

    private int idArbolDesicion;
    private int idCheckList;
    private int idPregunta;
    private String respuesta;
    private int estatusEvidencia;
    private int ordenCheckRespuesta;
    private int commit;
    private int reqAccion;
    private int reqObservacion;
    private int reqOblig;
    private String descEvidencia;
    private String numVersion;
    private String tipoCambio;

    public int getIdArbolDesicion() {
        return idArbolDesicion;
    }

    public void setIdArbolDesicion(int idArbolDesicion) {
        this.idArbolDesicion = idArbolDesicion;
    }

    public int getIdCheckList() {
        return idCheckList;
    }

    public void setIdCheckList(int idCheckList) {
        this.idCheckList = idCheckList;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public int getEstatusEvidencia() {
        return estatusEvidencia;
    }

    public void setEstatusEvidencia(int estatusEvidencia) {
        this.estatusEvidencia = estatusEvidencia;
    }

    public int getOrdenCheckRespuesta() {
        return ordenCheckRespuesta;
    }

    public void setOrdenCheckRespuesta(int ordenCheckRespuesta) {
        this.ordenCheckRespuesta = ordenCheckRespuesta;
    }

    public int getCommit() {
        return commit;
    }

    public void setCommit(int commit) {
        this.commit = commit;
    }

    public int getReqAccion() {
        return reqAccion;
    }

    public void setReqAccion(int reqAccion) {
        this.reqAccion = reqAccion;
    }

    public int getReqObservacion() {
        return reqObservacion;
    }

    public void setReqObservacion(int reqObservacion) {
        this.reqObservacion = reqObservacion;
    }

    public int getReqOblig() {
        return reqOblig;
    }

    public void setReqOblig(int reqOblig) {
        this.reqOblig = reqOblig;
    }

    public String getDescEvidencia() {
        return descEvidencia;
    }

    public void setDescEvidencia(String descEvidencia) {
        this.descEvidencia = descEvidencia;
    }

    public String getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(String numVersion) {
        this.numVersion = numVersion;
    }

    public String getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

}
