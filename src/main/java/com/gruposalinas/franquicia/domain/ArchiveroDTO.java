package com.gruposalinas.franquicia.domain;

public class ArchiveroDTO {

    private int idArchivero;
    private String placa;
    private String marca;
    private String descripcion;
    private String observaciones;
    private int idFormArchivero;

    public int getIdArchivero() {
        return idArchivero;
    }

    public void setIdArchivero(int idArchivero) {
        this.idArchivero = idArchivero;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public String toString() {
        return "ArchiveroDTO [idArchivero=" + idArchivero + ", placa=" + placa + ", marca=" + marca + ", descripcion="
                + descripcion + ", observaciones=" + observaciones + "]";
    }

    public int getIdFormArchivero() {
        return idFormArchivero;
    }

    public void setIdFormArchivero(int idFormArchivero) {
        this.idFormArchivero = idFormArchivero;
    }

}
