package com.gruposalinas.franquicia.domain;

public class RespuestasPDDTO {

    private int ID_ARCHIVO;

    public int getID_ARCHIVO() {
        return ID_ARCHIVO;
    }

    public void setID_ARCHIVO(int iD_ARCHIVO) {
        ID_ARCHIVO = iD_ARCHIVO;
    }
}
