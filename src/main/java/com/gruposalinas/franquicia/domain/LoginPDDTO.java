package com.gruposalinas.franquicia.domain;

public class LoginPDDTO {

    private int usuario;
    private int perfil;
    private String descripcion;

    public int getUsuario() {
        return usuario;
    }

    public void setUsuario(int usuario) {
        this.usuario = usuario;
    }

    public int getPerfil() {
        return perfil;
    }

    public void setPerfil(int perfil) {
        this.perfil = perfil;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "LoginPDDTO [usuario=" + usuario + ", perfil=" + perfil + "]";
    }
}
