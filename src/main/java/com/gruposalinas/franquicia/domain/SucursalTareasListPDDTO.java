package com.gruposalinas.franquicia.domain;

public class SucursalTareasListPDDTO {

    private int folio;
    private String nombreDocumento;
    private String tipoEnvio;
    private String fechaCreacionDocumento;
    private String fechaVisualizacionDocumento;
    private String fechaRecepcion;
    private String estatusDocumentoSucursal;

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    public String getFechaCreacionDocumento() {
        return fechaCreacionDocumento;
    }

    public void setFechaCreacionDocumento(String fechaCreacionDocumento) {
        this.fechaCreacionDocumento = fechaCreacionDocumento;
    }

    public String getFechaVisualizacionDocumento() {
        return fechaVisualizacionDocumento;
    }

    public void setFechaVisualizacionDocumento(String fechaVisualizacionDocumento) {
        this.fechaVisualizacionDocumento = fechaVisualizacionDocumento;
    }

    public String getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(String fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }

    public String getEstatusDocumentoSucursal() {
        return estatusDocumentoSucursal;
    }

    public void setEstatusDocumentoSucursal(String estatusDocumentoSucursal) {
        this.estatusDocumentoSucursal = estatusDocumentoSucursal;
    }
}
