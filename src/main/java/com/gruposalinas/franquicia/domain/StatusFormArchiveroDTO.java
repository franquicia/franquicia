package com.gruposalinas.franquicia.domain;

public class StatusFormArchiveroDTO {

    private int idStatus;
    private String descripcion;

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "StatusFormArchiveroDTO [idStatus=" + idStatus + ", descripcion=" + descripcion + "]";
    }

}
