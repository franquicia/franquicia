package com.gruposalinas.franquicia.domain;

public class TabletInformationPDDTO {

    private int idTableta;
    private String numSerie;
    private int idCeco;
    private String nombreCeco;
    private int idEstatus;
    private String fechaInstalacion;
    private String fabricante;
    private String modelo;
    private String sistemaOperativo;
    private String version;
    private long espacioTotal;
    private long espacioDisp;
    private long ramTotal;
    private long ramDisp;
    private String problemaMant;

    public int getIdTableta() {
        return idTableta;
    }

    public void setIdTableta(int idTableta) {
        this.idTableta = idTableta;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }

    public int getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(int idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public int getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
        this.idEstatus = idEstatus;
    }

    public String getFechaInstalacion() {
        return fechaInstalacion;
    }

    public void setFechaInstalacion(String fechaInstalacion) {
        this.fechaInstalacion = fechaInstalacion;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getSistemaOperativo() {
        return sistemaOperativo;
    }

    public void setSistemaOperativo(String sistemaOperativo) {
        this.sistemaOperativo = sistemaOperativo;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public long getEspacioTotal() {
        return espacioTotal;
    }

    public void setEspacioTotal(long espacioTotal) {
        this.espacioTotal = espacioTotal;
    }

    public long getEspacioDisp() {
        return espacioDisp;
    }

    public void setEspacioDisp(long espacioDisp) {
        this.espacioDisp = espacioDisp;
    }

    public long getRamTotal() {
        return ramTotal;
    }

    public void setRamTotal(long ramTotal) {
        this.ramTotal = ramTotal;
    }

    public long getRamDisp() {
        return ramDisp;
    }

    public void setRamDisp(long ramDisp) {
        this.ramDisp = ramDisp;
    }

    public String getProblemaMant() {
        return problemaMant;
    }

    public void setProblemaMant(String problemaMant) {
        this.problemaMant = problemaMant;
    }
}
