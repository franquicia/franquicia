package com.gruposalinas.franquicia.domain;

public class CecosByNegPDDTO {

    private String fcid_ceco;
    private Integer fitipo_ceco;
    private String fcnombre;
    private Integer ficeco_superior;
    private String desc_depende_de;

    public String getFcid_ceco() {
        return fcid_ceco;
    }

    public void setFcid_ceco(String fcid_ceco) {
        this.fcid_ceco = fcid_ceco;
    }

    public Integer getFitipo_ceco() {
        return fitipo_ceco;
    }

    public void setFitipo_ceco(Integer fitipo_ceco) {
        this.fitipo_ceco = fitipo_ceco;
    }

    public String getFcnombre() {
        return fcnombre;
    }

    public void setFcnombre(String fcnombre) {
        this.fcnombre = fcnombre;
    }

    public Integer getFiceco_superior() {
        return ficeco_superior;
    }

    public void setFiceco_superior(Integer ficeco_superior) {
        this.ficeco_superior = ficeco_superior;
    }

    public String getDesc_depende_de() {
        return desc_depende_de;
    }

    public void setDesc_depende_de(String desc_depende_de) {
        this.desc_depende_de = desc_depende_de;
    }
}
