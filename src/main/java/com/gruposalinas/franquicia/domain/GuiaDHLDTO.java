package com.gruposalinas.franquicia.domain;

public class GuiaDHLDTO {

    private int idGuia;
    private String fecha;
    private String ceco;
    private int idEvid;
    private String ruta;
    private String nombreArch;
    private String coment;

    public int getIdGuia() {
        return idGuia;
    }

    public void setIdGuia(int idGuia) {
        this.idGuia = idGuia;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

    public int getIdEvid() {
        return idEvid;
    }

    public void setIdEvid(int idEvid) {
        this.idEvid = idEvid;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getNombreArch() {
        return nombreArch;
    }

    public void setNombreArch(String nombreArch) {
        this.nombreArch = nombreArch;
    }

    public String getComent() {
        return coment;
    }

    public void setComent(String coment) {
        this.coment = coment;
    }

}
