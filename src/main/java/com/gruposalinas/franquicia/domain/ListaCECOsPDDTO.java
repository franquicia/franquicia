package com.gruposalinas.franquicia.domain;

public class ListaCECOsPDDTO {

    private int id_listaCeco;
    private int id_listaDistribucion;
    private String id_ceco;
    private String user;
    private String fechaCambio;
    private int activo;
    private String nombreCeco;

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public int getId_listaCeco() {
        return id_listaCeco;
    }

    public void setId_listaCeco(int id_listaCeco) {
        this.id_listaCeco = id_listaCeco;
    }

    public int getId_listaDistribucion() {
        return id_listaDistribucion;
    }

    public void setId_listaDistribucion(int id_listaDistribucion) {
        this.id_listaDistribucion = id_listaDistribucion;
    }

    public String getId_ceco() {
        return id_ceco;
    }

    public void setId_ceco(String id_ceco) {
        this.id_ceco = id_ceco;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(String fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "ListaCECOsPDDTO [id_listaCeco=" + id_listaCeco + ", id_listaDistribucion=" + id_listaDistribucion + ", id_ceco=" + id_ceco + ", user=" + user + ", fechaCambio=" + fechaCambio + ", activo=" + activo + "]";
    }

}
