package com.gruposalinas.franquicia.domain;

public class IndicadoresDataEGDTO {

    private int totalInstalaciones;
    private int totalTabletas;
    private String ultimaInstalacion;
    private int pendientes;

    public int getTotalInstalaciones() {
        return totalInstalaciones;
    }

    public void setTotalInstalaciones(int totalInstalaciones) {
        this.totalInstalaciones = totalInstalaciones;
    }

    public int getTotalTabletas() {
        return totalTabletas;
    }

    public void setTotalTabletas(int totalTabletas) {
        this.totalTabletas = totalTabletas;
    }

    public String getUltimaInstalacion() {
        return ultimaInstalacion;
    }

    public void setUltimaInstalacion(String ultimaInstalacion) {
        this.ultimaInstalacion = ultimaInstalacion;
    }

    public int getPendientes() {
        return pendientes;
    }

    public void setPendientes(int pendientes) {
        this.pendientes = pendientes;
    }
}
