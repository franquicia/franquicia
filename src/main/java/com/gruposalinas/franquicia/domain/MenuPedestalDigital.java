package com.gruposalinas.franquicia.domain;

public class MenuPedestalDigital {

    private int idMenu;
    private String descripcion;
    private int nivel;
    private String cursor;
    private String fechaCambio;
    private int activo;
    private int dependeDe;

    public int getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public String getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(String fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public int getDependeDe() {
        return dependeDe;
    }

    public void setDependeDe(int dependeDe) {
        this.dependeDe = dependeDe;
    }

    @Override
    public String toString() {
        return "MenuPedestalDigital [idMenu=" + idMenu + ", descripcion=" + descripcion + "]";
    }
}
