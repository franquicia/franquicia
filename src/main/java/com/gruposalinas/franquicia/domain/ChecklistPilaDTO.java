package com.gruposalinas.franquicia.domain;

public class ChecklistPilaDTO {

    private int idCheckUsua;
    private int idChecklist;
    private String nombreCheck;
    private int idCeco;
    private String nombreCeco;
    private int estatus;

    public int getIdCheckUsua() {
        return idCheckUsua;
    }

    public void setIdCheckUsua(int idCheckUsua) {
        this.idCheckUsua = idCheckUsua;
    }

    public int getIdChecklist() {
        return idChecklist;
    }

    public void setIdChecklist(int idChecklist) {
        this.idChecklist = idChecklist;
    }

    public int getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(int idCeco) {
        this.idCeco = idCeco;
    }

    public String getNombreCeco() {
        return nombreCeco;
    }

    public void setNombreCeco(String nombreCeco) {
        this.nombreCeco = nombreCeco;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public String getNombreCheck() {
        return nombreCheck;
    }

    public void setNombreCheck(String nombreCheck) {
        this.nombreCheck = nombreCheck;
    }

}
