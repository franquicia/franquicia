package com.gruposalinas.franquicia.domain;

public class AdminParametroPDDTO {

    private int idParametro;
    private String parametro;
    private String descParametro;
    private String valor;
    private String referencia;
    private int activo;
    private String usuario;
    private String fecha;

    public int getIdParametro() {
        return idParametro;
    }

    public void setIdParametro(int idParametro) {
        this.idParametro = idParametro;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getDescParametro() {
        return descParametro;
    }

    public void setDescParametro(String descParametro) {
        this.descParametro = descParametro;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "AdminParametroPDDTO [idParametro=" + idParametro + ", parametro=" + parametro + ", descParametro="
                + descParametro + ", valor=" + valor + ", referencia=" + referencia + ", activo=" + activo + "]";
    }
}
