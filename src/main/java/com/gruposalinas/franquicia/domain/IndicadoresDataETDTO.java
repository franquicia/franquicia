package com.gruposalinas.franquicia.domain;

public class IndicadoresDataETDTO {

    private int tabletasCorrectas;
    private int tabletasIncorrectas;
    private int tabletasMantenimiento;

    public int getTabletasCorrectas() {
        return tabletasCorrectas;
    }

    public void setTabletasCorrectas(int tabletasCorrectas) {
        this.tabletasCorrectas = tabletasCorrectas;
    }

    public int getTabletasIncorrectas() {
        return tabletasIncorrectas;
    }

    public void setTabletasIncorrectas(int tabletasIncorrectas) {
        this.tabletasIncorrectas = tabletasIncorrectas;
    }

    public int getTabletasMantenimiento() {
        return tabletasMantenimiento;
    }

    public void setTabletasMantenimiento(int tabletasMantenimiento) {
        this.tabletasMantenimiento = tabletasMantenimiento;
    }
}
