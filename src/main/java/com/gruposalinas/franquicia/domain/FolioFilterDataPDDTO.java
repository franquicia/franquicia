package com.gruposalinas.franquicia.domain;

public class FolioFilterDataPDDTO {

    private int idFolio;
    private String tipoEnvio;
    private String fechaEnvio;
    private int sucursalesAfectadas;
    private String estatus;

    public int getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(int idFolio) {
        this.idFolio = idFolio;
    }

    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    public String getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(String fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public int getSucursalesAfectadas() {
        return sucursalesAfectadas;
    }

    public void setSucursalesAfectadas(int sucursalesAfectadas) {
        this.sucursalesAfectadas = sucursalesAfectadas;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }
}
