package com.gruposalinas.franquicia.domain;

public class ArchiverosDTO {

    private int idArchivero;
    private int idGerente;
    private String idCeco;
    private String fecha;
    private int estatusArchivero;
    private String descripcion;
    private int idResponsable;
    private int identificador;
    private String nombreResp;
    private int estatusResp;
    private String Ciudad;

    public int getIdArchivero() {
        return idArchivero;
    }

    public void setIdArchivero(int idArchivero) {
        this.idArchivero = idArchivero;
    }

    public int getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(int idGerente) {
        this.idGerente = idGerente;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getEstatusArchivero() {
        return estatusArchivero;
    }

    public void setEstatusArchivero(int estatusArchivero) {
        this.estatusArchivero = estatusArchivero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdResponsable() {
        return idResponsable;
    }

    public void setIdResponsable(int idResponsable) {
        this.idResponsable = idResponsable;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getNombreResp() {
        return nombreResp;
    }

    public void setNombreResp(String nombreResp) {
        this.nombreResp = nombreResp;
    }

    public int getEstatusResp() {
        return estatusResp;
    }

    public void setEstatusResp(int estatusResp) {
        this.estatusResp = estatusResp;
    }

    public String getCiudad() {
        return Ciudad;
    }

    public void setCiudad(String ciudad) {
        Ciudad = ciudad;
    }

}
