package com.gruposalinas.franquicia.domain;

public class FolioDetailPDDTO {

    private int idFolio;
    private String fechaVisualizacion;
    private String fechaFin;
    private int idDocumento;
    private String nombreArchivo;
    private int idTipoDocumento;
    private String nombreDocumento;
    private String visibleSucursal;
    private String visibleSucursalDesc;
    private int totalSucursal;
    private int docVisSuc;
    private int docNVisSuc;
    private String estatus;
    private int idCategoria;
    private String categoriaDesc;

    public int getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(int idFolio) {
        this.idFolio = idFolio;
    }

    public String getFechaVisualizacion() {
        return fechaVisualizacion;
    }

    public void setFechaVisualizacion(String fechaVisualizacion) {
        this.fechaVisualizacion = fechaVisualizacion;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public String getVisibleSucursal() {
        return visibleSucursal;
    }

    public void setVisibleSucursal(String visibleSucursal) {
        this.visibleSucursal = visibleSucursal;
    }

    public String getVisibleSucursalDesc() {
        return visibleSucursalDesc;
    }

    public void setVisibleSucursalDesc(String visibleSucursalDesc) {
        this.visibleSucursalDesc = visibleSucursalDesc;
    }

    public int getTotalSucursal() {
        return totalSucursal;
    }

    public void setTotalSucursal(int totalSucursal) {
        this.totalSucursal = totalSucursal;
    }

    public int getDocVisSuc() {
        return docVisSuc;
    }

    public void setDocVisSuc(int docVisSuc) {
        this.docVisSuc = docVisSuc;
    }

    public int getDocNVisSuc() {
        return docNVisSuc;
    }

    public void setDocNVisSuc(int docNVisSuc) {
        this.docNVisSuc = docNVisSuc;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCategoriaDesc() {
        return categoriaDesc;
    }

    public void setCategoriaDesc(String categoriaDesc) {
        this.categoriaDesc = categoriaDesc;
    }
}
