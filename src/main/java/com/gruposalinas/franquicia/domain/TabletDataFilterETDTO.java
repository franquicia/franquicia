package com.gruposalinas.franquicia.domain;

public class TabletDataFilterETDTO {

    private int idTableta;
    private int sucursal;
    private String nombre;
    private String fechaInstalacion;
    private int idEstatus;
    private String descIdEstatus;
    private String ultimaActualizacion;

    public int getIdTableta() {
        return idTableta;
    }

    public void setIdTableta(int idTableta) {
        this.idTableta = idTableta;
    }

    public int getSucursal() {
        return sucursal;
    }

    public void setSucursal(int sucursal) {
        this.sucursal = sucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaInstalacion() {
        return fechaInstalacion;
    }

    public void setFechaInstalacion(String fechaInstalacion) {
        this.fechaInstalacion = fechaInstalacion;
    }

    public int getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
        this.idEstatus = idEstatus;
    }

    public String getDescIdEstatus() {
        return descIdEstatus;
    }

    public void setDescIdEstatus(String descIdEstatus) {
        this.descIdEstatus = descIdEstatus;
    }

    public String getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(String ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }
}
