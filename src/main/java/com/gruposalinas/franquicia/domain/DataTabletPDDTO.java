package com.gruposalinas.franquicia.domain;

public class DataTabletPDDTO {

    private String numSerie;
    private String fabricante;
    private String modelo;
    private String sistemaOperativo;
    private String versionApp;
    private String totalEspacioAlmacenamiento;
    private String ramTotal;
    private String espacioDisp;
    private String ramDisp;
    private String activo;
    private String alimentacion;
    private String intensidadSenial;
    private String nivelBateria;
    private String saludBateria;
    private String temperaturaBateria;

    public DataTabletPDDTO() {

    }

    // Constructor para objeto con datos internos de la tableta
    public DataTabletPDDTO(String a, String b, String c, String d, String e, String f, String g, String h, String i) {
        this.numSerie = a;
        this.fabricante = b;
        this.modelo = c;
        this.sistemaOperativo = d;
        this.versionApp = e;
        this.totalEspacioAlmacenamiento = f;
        this.ramTotal = g;
        this.espacioDisp = h;
        this.ramDisp = i;

        this.activo = "";
        this.alimentacion = "";
        this.intensidadSenial = "";
        this.nivelBateria = "";
        this.saludBateria = "";
        this.temperaturaBateria = "";
    }

    // Constructor para objeto con datos historicos de la tableta
    public DataTabletPDDTO(String a, String b, String c, String d, String e, String f, String g) {
        this.numSerie = a;
        this.alimentacion = b;
        this.intensidadSenial = c;
        this.nivelBateria = d;
        this.saludBateria = e;
        this.temperaturaBateria = f;
        this.activo = g;

        this.fabricante = "";
        this.modelo = "";
        this.sistemaOperativo = "";
        this.versionApp = "";
        this.totalEspacioAlmacenamiento = "";
        this.ramTotal = "";
        this.espacioDisp = "";
        this.ramDisp = "";
    }

    public String getEspacioDisp() {
        return espacioDisp;
    }

    public void setEspacioDisp(String espacioDisp) {
        this.espacioDisp = espacioDisp;
    }

    public String getRamDisp() {
        return ramDisp;
    }

    public void setRamDisp(String ramDisp) {
        this.ramDisp = ramDisp;
    }

    public String getAlimentacion() {
        return alimentacion;
    }

    public void setAlimentacion(String alimentacion) {
        this.alimentacion = alimentacion;
    }

    public String getIntensidadSenial() {
        return intensidadSenial;
    }

    public void setIntensidadSenial(String intensidadSenial) {
        this.intensidadSenial = intensidadSenial;
    }

    public String getNivelBateria() {
        return nivelBateria;
    }

    public void setNivelBateria(String nivelBateria) {
        this.nivelBateria = nivelBateria;
    }

    public String getSaludBateria() {
        return saludBateria;
    }

    public void setSaludBateria(String saludBateria) {
        this.saludBateria = saludBateria;
    }

    public String getTemperaturaBateria() {
        return temperaturaBateria;
    }

    public void setTemperaturaBateria(String temperaturaBateria) {
        this.temperaturaBateria = temperaturaBateria;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getSistemaOperativo() {
        return sistemaOperativo;
    }

    public void setSistemaOperativo(String sistemaOperativo) {
        this.sistemaOperativo = sistemaOperativo;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public String getTotalEspacioAlmacenamiento() {
        return totalEspacioAlmacenamiento;
    }

    public void setTotalEspacioAlmacenamiento(String totalEspacioAlmacenamiento) {
        this.totalEspacioAlmacenamiento = totalEspacioAlmacenamiento;
    }

    public String getRamTotal() {
        return ramTotal;
    }

    public void setRamTotal(String ramTotal) {
        this.ramTotal = ramTotal;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }
}
