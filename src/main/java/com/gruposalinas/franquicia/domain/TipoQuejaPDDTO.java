package com.gruposalinas.franquicia.domain;

public class TipoQuejaPDDTO {

    private int idCatComent;
    private String desc;
    private String user;
    private String fechaCambio;
    private int activo;

    public int getIdCatComent() {
        return idCatComent;
    }

    public void setIdCatComent(int idCatComent) {
        this.idCatComent = idCatComent;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(String fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
