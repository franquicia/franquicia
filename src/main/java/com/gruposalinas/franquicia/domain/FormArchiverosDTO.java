package com.gruposalinas.franquicia.domain;

public class FormArchiverosDTO {

    private int idFromArchivero;
    private int idUsuario;
    private String nombre;
    private String idCeco;
    private int idRecibe;
    private String nomRecibe;
    private int idStatus;
    private String fecha;

    public int getIdFromArchivero() {
        return idFromArchivero;
    }

    public void setIdFromArchivero(int idFromArchivero) {
        this.idFromArchivero = idFromArchivero;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public int getIdRecibe() {
        return idRecibe;
    }

    public void setIdRecibe(int idRecibe) {
        this.idRecibe = idRecibe;
    }

    public String getNomRecibe() {
        return nomRecibe;
    }

    public void setNomRecibe(String nomRecibe) {
        this.nomRecibe = nomRecibe;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "FormArchiverosDTO [idFromArchivero=" + idFromArchivero + ", idUsuario=" + idUsuario + ", nombre="
                + nombre + ", idCeco=" + idCeco + ", idRecibe=" + idRecibe + ", nomRecibe=" + nomRecibe + ", idStatus="
                + idStatus + ", fecha=" + fecha + "]";
    }

}
