package com.gruposalinas.franquicia.domain;

public class ErrorPDDTO {

    private String msj;

    public String getMsj() {
        return msj;
    }

    public void setMsj(String msj) {
        this.msj = msj;
    }
}
