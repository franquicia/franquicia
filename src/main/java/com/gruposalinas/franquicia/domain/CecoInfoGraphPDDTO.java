package com.gruposalinas.franquicia.domain;

public class CecoInfoGraphPDDTO {

    private Integer n_suc_ok;
    private Integer n_suc_probl;
    private Integer n_suc_mante;
    private Integer n_suc_total;
    private Integer n_ceco_nivel_infe;
    private Integer nivel_ceco;

    public Integer getN_suc_ok() {
        return n_suc_ok;
    }

    public void setN_suc_ok(Integer n_suc_ok) {
        this.n_suc_ok = n_suc_ok;
    }

    public Integer getN_suc_probl() {
        return n_suc_probl;
    }

    public void setN_suc_probl(Integer n_suc_probl) {
        this.n_suc_probl = n_suc_probl;
    }

    public Integer getN_suc_mante() {
        return n_suc_mante;
    }

    public void setN_suc_mante(Integer n_suc_mante) {
        this.n_suc_mante = n_suc_mante;
    }

    public Integer getN_suc_total() {
        return n_suc_total;
    }

    public void setN_suc_total(Integer n_suc_total) {
        this.n_suc_total = n_suc_total;
    }

    public Integer getN_ceco_nivel_infe() {
        return n_ceco_nivel_infe;
    }

    public void setN_ceco_nivel_infe(Integer n_ceco_nivel_infe) {
        this.n_ceco_nivel_infe = n_ceco_nivel_infe;
    }

    public Integer getNivel_ceco() {
        return nivel_ceco;
    }

    public void setNivel_ceco(Integer nivel_ceco) {
        this.nivel_ceco = nivel_ceco;
    }
}
