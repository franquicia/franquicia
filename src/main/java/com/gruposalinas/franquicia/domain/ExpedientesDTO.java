package com.gruposalinas.franquicia.domain;

public class ExpedientesDTO {

    private int idExpediente;
    private int idGerente;
    private String idCeco;
    private String fecha;
    private int estatusExpediente;
    private String descripcion;

    private int idResponsable;
    private int identificador;
    private String nombreResp;
    private int estatusResp;
    private String Ciudad;

    public int getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(int idExpediente) {
        this.idExpediente = idExpediente;
    }

    public int getIdGerente() {
        return idGerente;
    }

    public void setIdGerente(int idGerente) {
        this.idGerente = idGerente;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getEstatusExpediente() {
        return estatusExpediente;
    }

    public void setEstatusExpediente(int estatusExpediente) {
        this.estatusExpediente = estatusExpediente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdResponsable() {
        return idResponsable;
    }

    public void setIdResponsable(int idResponsable) {
        this.idResponsable = idResponsable;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getNombreResp() {
        return nombreResp;
    }

    public void setNombreResp(String nombreResp) {
        this.nombreResp = nombreResp;
    }

    public int getEstatusResp() {
        return estatusResp;
    }

    public void setEstatusResp(int estatusResp) {
        this.estatusResp = estatusResp;
    }

    public String getCiudad() {
        return Ciudad;
    }

    public void setCiudad(String ciudad) {
        Ciudad = ciudad;
    }

}
