package com.gruposalinas.franquicia.domain;

public class ListaDistribucionPDDTO {

    private int id_usuario;
    private int negocio;
    private int id_listaDistribucion;
    private int activo;
    private String fechaCambio;
    private String user;
    private String nombre;

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getNegocio() {
        return negocio;
    }

    public void setNegocio(int negocio) {
        this.negocio = negocio;
    }

    public int getId_listaDistribucion() {
        return id_listaDistribucion;
    }

    public void setId_listaDistribucion(int id_listaDistribucion) {
        this.id_listaDistribucion = id_listaDistribucion;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(String fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "ListaDistribucionPDDTO [id_usuario=" + id_usuario + ", negocio=" + negocio + ", id_listaDistribucion=" + id_listaDistribucion + ", activo=" + activo + ", fechaCambio=" + fechaCambio + ", user=" + user + ", nombre=" + nombre
                + "]";
    }
}
