package com.gruposalinas.franquicia.domain;

public class TaskInTabletPDDAO {

    private int idFolio;
    private int idDocumento;
    private String nombreArchivo;
    private String tipoEnvio;
    private String visibleSucursal;
    private String idCeco;
    private int idCategoria;
    private String descCategoria;
    private int idTipoDocumento;
    private String nombreDocumento;
    private int idDocumentoPedestal;
    private String vigenciaInicial;
    private String vigenciaFinal;
    private long pesoBytes;
    private int estatusDocumento;
    private int estatusDocumentoSucursal;
    private String hash;
    private int activo;

    public int getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(int idFolio) {
        this.idFolio = idFolio;
    }

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getTipoEnvio() {
        return tipoEnvio;
    }

    public void setTipoEnvio(String tipoEnvio) {
        this.tipoEnvio = tipoEnvio;
    }

    public String getVisibleSucursal() {
        return visibleSucursal;
    }

    public void setVisibleSucursal(String visibleSucursal) {
        this.visibleSucursal = visibleSucursal;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getDescCategoria() {
        return descCategoria;
    }

    public void setDescCategoria(String descCategoria) {
        this.descCategoria = descCategoria;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getNombreDocumento() {
        return nombreDocumento;
    }

    public void setNombreDocumento(String nombreDocumento) {
        this.nombreDocumento = nombreDocumento;
    }

    public int getIdDocumentoPedestal() {
        return idDocumentoPedestal;
    }

    public void setIdDocumentoPedestal(int idDocumentoPedestal) {
        this.idDocumentoPedestal = idDocumentoPedestal;
    }

    public String getVigenciaInicial() {
        return vigenciaInicial;
    }

    public void setVigenciaInicial(String vigenciaInicial) {
        this.vigenciaInicial = vigenciaInicial;
    }

    public String getVigenciaFinal() {
        return vigenciaFinal;
    }

    public void setVigenciaFinal(String vigenciaFinal) {
        this.vigenciaFinal = vigenciaFinal;
    }

    public long getPesoBytes() {
        return pesoBytes;
    }

    public void setPesoBytes(long pesoBytes) {
        this.pesoBytes = pesoBytes;
    }

    public int getEstatusDocumento() {
        return estatusDocumento;
    }

    public void setEstatusDocumento(int estatusDocumento) {
        this.estatusDocumento = estatusDocumento;
    }

    public int getEstatusDocumentoSucursal() {
        return estatusDocumentoSucursal;
    }

    public void setEstatusDocumentoSucursal(int estatusDocumentoSucursal) {
        this.estatusDocumentoSucursal = estatusDocumentoSucursal;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
