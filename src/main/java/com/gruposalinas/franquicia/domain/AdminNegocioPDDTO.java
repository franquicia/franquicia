package com.gruposalinas.franquicia.domain;

public class AdminNegocioPDDTO {

    private int idNegocio;
    private String negocio;
    private String usuario;
    private String fechaCambio;
    private int activo;

    public int getIdNegocio() {
        return idNegocio;
    }

    public void setIdNegocio(int idNegocio) {
        this.idNegocio = idNegocio;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(String fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "AdminNegocioPDDTO [idNegocio=" + idNegocio + ", negocio=" + negocio + ", usuario=" + usuario
                + ", fechaCambio=" + fechaCambio + ", activo=" + activo + "]";
    }
}
