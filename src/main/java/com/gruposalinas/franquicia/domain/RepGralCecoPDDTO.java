package com.gruposalinas.franquicia.domain;

public class RepGralCecoPDDTO {

    private int idDocumento;
    private int idCategoria;
    private String descCategoria;
    private int idTipoDoc;
    private String fechaEnv;
    private String nombreDoc;
    private int idUsuario;
    private String visibleSuc;
    private String descVisibleSuc;
    private int idEstatus;
    private String descEstatus;
    private String nombreArch;
    private String vigenciaIni;
    private String vigenciaFin;

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getDescCategoria() {
        return descCategoria;
    }

    public void setDescCategoria(String descCategoria) {
        this.descCategoria = descCategoria;
    }

    public int getIdTipoDoc() {
        return idTipoDoc;
    }

    public void setIdTipoDoc(int idTipoDoc) {
        this.idTipoDoc = idTipoDoc;
    }

    public String getFechaEnv() {
        return fechaEnv;
    }

    public void setFechaEnv(String fechaEnv) {
        this.fechaEnv = fechaEnv;
    }

    public String getNombreDoc() {
        return nombreDoc;
    }

    public void setNombreDoc(String nombreDoc) {
        this.nombreDoc = nombreDoc;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getVisibleSuc() {
        return visibleSuc;
    }

    public void setVisibleSuc(String visibleSuc) {
        this.visibleSuc = visibleSuc;
    }

    public String getDescVisibleSuc() {
        return descVisibleSuc;
    }

    public void setDescVisibleSuc(String descVisibleSuc) {
        this.descVisibleSuc = descVisibleSuc;
    }

    public int getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
        this.idEstatus = idEstatus;
    }

    public String getDescEstatus() {
        return descEstatus;
    }

    public void setDescEstatus(String descEstatus) {
        this.descEstatus = descEstatus;
    }

    public String getNombreArch() {
        return nombreArch;
    }

    public void setNombreArch(String nombreArch) {
        this.nombreArch = nombreArch;
    }

    public String getVigenciaIni() {
        return vigenciaIni;
    }

    public void setVigenciaIni(String vigenciaIni) {
        this.vigenciaIni = vigenciaIni;
    }

    public String getVigenciaFin() {
        return vigenciaFin;
    }

    public void setVigenciaFin(String vigenciaFin) {
        this.vigenciaFin = vigenciaFin;
    }
}
