package com.gruposalinas.franquicia.domain;

public class EvidenciasFotograficasDTO {

    private String rutafotoantes;
    private String fechadespues;
    private String horadespues;
    private String lugarantes;
    private String lugardespues;
    private String rutafotodespues;
    private String periodo;
    private String fechaantes;
    private String horaantes;

    public String getHoradespues() {
        return horadespues;
    }

    public void setHoradespues(String horadespues) {
        this.horadespues = horadespues;
    }

    public String getHoraantes() {
        return horaantes;
    }

    public void setHoraantes(String horaantes) {
        this.horaantes = horaantes;
    }

    public String getRutafotoantes() {
        return rutafotoantes;
    }

    public void setRutafotoantes(String rutafotoantes) {
        this.rutafotoantes = rutafotoantes;
    }

    public String getFechadespues() {
        return fechadespues;
    }

    public void setFechadespues(String fechadespues) {
        this.fechadespues = fechadespues;
    }

    public String getLugarantes() {
        return lugarantes;
    }

    public void setLugarantes(String lugarantes) {
        this.lugarantes = lugarantes;
    }

    public String getLugardespues() {
        return lugardespues;
    }

    public void setLugardespues(String lugardespues) {
        this.lugardespues = lugardespues;
    }

    public String getRutafotodespues() {
        return rutafotodespues;
    }

    public void setRutafotodespues(String rutafotodespues) {
        this.rutafotodespues = rutafotodespues;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getFechaantes() {
        return fechaantes;
    }

    public void setFechaantes(String fechaantes) {
        this.fechaantes = fechaantes;
    }

}
