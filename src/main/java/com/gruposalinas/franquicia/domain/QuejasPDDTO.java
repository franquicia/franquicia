package com.gruposalinas.franquicia.domain;

public class QuejasPDDTO {

    private int idComentario;
    private String idCeco;
    private String descCeco;
    private String nombreCliente;
    private String telefono;
    private String email;
    private int idCatComent;
    private String desc;
    private String comentario;
    private int idContacto;
    private int idEstatus;
    private String fecha;
    private int activo;

    public int getIdComentario() {
        return idComentario;
    }

    public void setIdComentario(int idComentario) {
        this.idComentario = idComentario;
    }

    public String getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(String idCeco) {
        this.idCeco = idCeco;
    }

    public String getDescCeco() {
        return descCeco;
    }

    public void setDescCeco(String descCeco) {
        this.descCeco = descCeco;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdCatComent() {
        return idCatComent;
    }

    public void setIdCatComent(int idCatComent) {
        this.idCatComent = idCatComent;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

    public int getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
        this.idEstatus = idEstatus;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
