package com.gruposalinas.franquicia.domain;

import java.util.ArrayList;

public class TipoDocumentoUrlsDTO {

    private int economico;
    private int idTipoDocumento;
    private String documento;
    private ArrayList<String> urls;
    private String ultimaActualizacion;
    private String mensaje;
    private String negocio;

    public int getEconomico() {
        return economico;
    }

    public void setEconomico(int economico) {
        this.economico = economico;
    }

    public int getIdTipoDocumento() {
        return idTipoDocumento;
    }

    public void setIdTipoDocumento(int idTipoDocumento) {
        this.idTipoDocumento = idTipoDocumento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public void setUrls(ArrayList<String> urls) {
        this.urls = urls;
    }

    public String getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(String ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

}
