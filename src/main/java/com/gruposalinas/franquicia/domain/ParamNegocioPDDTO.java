package com.gruposalinas.franquicia.domain;

public class ParamNegocioPDDTO {

    private String fcdesc_parametro;
    private String fcvalor;
    private String fcreferencia;
    private Integer fiactivo;

    public String getFcdesc_parametro() {
        return fcdesc_parametro;
    }

    public void setFcdesc_parametro(String fddesc_parametro) {
        this.fcdesc_parametro = fddesc_parametro;
    }

    public String getFcvalor() {
        return fcvalor;
    }

    public void setFcvalor(String fcvalor) {
        this.fcvalor = fcvalor;
    }

    public String getFcreferencia() {
        return fcreferencia;
    }

    public void setFcreferencia(String fcreferencia) {
        this.fcreferencia = fcreferencia;
    }

    public Integer getFiactivo() {
        return fiactivo;
    }

    public void setFiactivo(Integer fiactivo) {
        this.fiactivo = fiactivo;
    }
}
