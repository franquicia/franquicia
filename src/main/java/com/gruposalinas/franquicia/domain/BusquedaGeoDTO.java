package com.gruposalinas.franquicia.domain;

import java.util.List;

public class BusquedaGeoDTO {

    private int id_geolocalizacion;
    private int tipo;
    private String descripcion;
    private int idDependeDe;
    private String dependeDeDesc;

    public String getDependeDeDesc() {
        return dependeDeDesc;
    }

    public void setDependeDeDesc(String dependeDeDesc) {
        this.dependeDeDesc = dependeDeDesc;
    }

    private List<BusquedaGeoDTO> listBusqueda;

    public int getId_geolocalizacion() {
        return id_geolocalizacion;
    }

    public void setId_geolocalizacion(int id_geolocalizacion) {
        this.id_geolocalizacion = id_geolocalizacion;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdDependeDe() {
        return idDependeDe;
    }

    public void setIdDependeDe(int idDependeDe) {
        this.idDependeDe = idDependeDe;
    }

    public List<BusquedaGeoDTO> getListBusqueda() {
        return listBusqueda;
    }

    public void setListBusqueda(List<BusquedaGeoDTO> listBusqueda) {
        this.listBusqueda = listBusqueda;
    }

    @Override
    public String toString() {
        return "BusquedaGeoDTO [id_geolocalizacion=" + id_geolocalizacion + ", tipo=" + tipo + ", descripcion=" + descripcion + ", idDependeDe=" + idDependeDe + ", listBusqueda=" + listBusqueda + "]";
    }
}
