package com.gruposalinas.franquicia.domain;

public class AdminCanalPDDTO {

    private int idCanal;
    private String canal;
    private String usuario;
    private String fechaCambio;
    private int activo;

    public int getIdCanal() {
        return idCanal;
    }

    public void setIdCanal(int idCanal) {
        this.idCanal = idCanal;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(String fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "AdminCanalPDDTO [idCanal=" + idCanal + ", canal=" + canal + ", usuario=" + usuario + ", fechaCambio="
                + fechaCambio + ", activo=" + activo + "]";
    }
}
