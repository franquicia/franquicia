package com.gruposalinas.franquicia.domain;

public class CatalogoEstatusDocPDDTO {

    private int idEstatusDoc;
    private String descEstatusDoc;
    private String user;
    private String fechaModifica;
    private int activo;

    public int getIdEstatusDoc() {
        return idEstatusDoc;
    }

    public void setIdEstatusDoc(int idEstatusDoc) {
        this.idEstatusDoc = idEstatusDoc;
    }

    public String getDescEstatusDoc() {
        return descEstatusDoc;
    }

    public void setDescEstatusDoc(String descEstatusDoc) {
        this.descEstatusDoc = descEstatusDoc;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFechaModifica() {
        return fechaModifica;
    }

    public void setFechaModifica(String fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
