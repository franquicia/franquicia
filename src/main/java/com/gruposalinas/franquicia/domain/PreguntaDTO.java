package com.gruposalinas.franquicia.domain;

import java.util.List;

public class PreguntaDTO {

    private int idPregunta;
    private int idModulo;
    private int idTipo;
    private int estatus;
    private String pregunta;
    private List<ArbolDecisionDTO> desiciones;
    private int commit;
    private String numVersion;
    private String tipoCambio;
    private int pregPadre;

    private String numRespuestas;

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public int getIdModulo() {
        return idModulo;
    }

    public void setIdModulo(int idModulo) {
        this.idModulo = idModulo;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public List<ArbolDecisionDTO> getDesiciones() {
        return desiciones;
    }

    public void setDesiciones(List<ArbolDecisionDTO> desiciones) {
        this.desiciones = desiciones;
    }

    public String getNumRespuestas() {
        return numRespuestas;
    }

    public void setNumRespuestas(String numRespuestas) {
        this.numRespuestas = numRespuestas;
    }

    public int getCommit() {
        return commit;
    }

    public void setCommit(int commit) {
        this.commit = commit;
    }

    public String getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(String numVersion) {
        this.numVersion = numVersion;
    }

    public String getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(String tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public int getPregPadre() {
        return pregPadre;
    }

    public void setPregPadre(int pregPadre) {
        this.pregPadre = pregPadre;
    }

}
