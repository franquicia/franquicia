package com.gruposalinas.franquicia.domain;

public class AdminMenuPDDTO {

    private int idMenu;
    private String descripcion;
    private int nivel;
    private String cursor;
    private String fechaCambio;
    private int activo;
    private int dependeDe;
    private int idPerfil;

    private String descDependeDe;
    private String descPerfil;
    private int activoPerfil;

    public int getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public String getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(String fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public int getDependeDe() {
        return dependeDe;
    }

    public void setDependeDe(int dependeDe) {
        this.dependeDe = dependeDe;
    }

    public int getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getDescDependeDe() {
        return descDependeDe;
    }

    public void setDescDependeDe(String descDependeDe) {
        this.descDependeDe = descDependeDe;
    }

    public String getDescPerfil() {
        return descPerfil;
    }

    public void setDescPerfil(String descPerfil) {
        this.descPerfil = descPerfil;
    }

    public int getActivoPerfil() {
        return activoPerfil;
    }

    public void setActivoPerfil(int activoPerfil) {
        this.activoPerfil = activoPerfil;
    }

    @Override
    public String toString() {
        return "AdminMenuPDDTO [idMenu=" + idMenu + ", descripcion=" + descripcion + ", nivel=" + nivel + ", cursor="
                + cursor + ", fechaCambio=" + fechaCambio + ", activo=" + activo + ", dependeDe=" + dependeDe
                + ", idPerfil=" + idPerfil + "]";
    }
}
