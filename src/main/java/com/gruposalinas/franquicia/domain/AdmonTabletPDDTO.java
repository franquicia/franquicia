package com.gruposalinas.franquicia.domain;

public class AdmonTabletPDDTO {

    private int numEconomico;
    private int idArchivo;
    private int idAdmonTable;
    private int etapa;
    private int activo;
    private String fechaInstalacion;
    private String fechaCambio;
    private String fechaFalla;
    private String cursor;
    private String numSerieTablet;
    private String detectaFalla;
    private String describeSucursal;
    private int folioFalla;
    private String fechaCorrige;
    private String responsable;

    public int getNumEconomico() {
        return numEconomico;
    }

    public void setNumEconomico(int numEconomico) {
        this.numEconomico = numEconomico;
    }

    public int getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(int idArchivo) {
        this.idArchivo = idArchivo;
    }

    public int getIdAdmonTable() {
        return idAdmonTable;
    }

    public void setIdAdmonTable(int idAdmonTable) {
        this.idAdmonTable = idAdmonTable;
    }

    public int getEtapa() {
        return etapa;
    }

    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getFechaInstalacion() {
        return fechaInstalacion;
    }

    public void setFechaInstalacion(String fechaInstalacion) {
        this.fechaInstalacion = fechaInstalacion;
    }

    public String getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(String fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public String getFechaFalla() {
        return fechaFalla;
    }

    public void setFechaFalla(String fechaFalla) {
        this.fechaFalla = fechaFalla;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public String getNumSerieTablet() {
        return numSerieTablet;
    }

    public void setNumSerieTablet(String numSerieTablet) {
        this.numSerieTablet = numSerieTablet;
    }

    public String getDetectaFalla() {
        return detectaFalla;
    }

    public void setDetectaFalla(String detectaFalla) {
        this.detectaFalla = detectaFalla;
    }

    public String getDescribeSucursal() {
        return describeSucursal;
    }

    public void setDescribeSucursal(String describeSucursal) {
        this.describeSucursal = describeSucursal;
    }

    public int getFolioFalla() {
        return folioFalla;
    }

    public void setFolioFalla(int folioFalla) {
        this.folioFalla = folioFalla;
    }

    public String getFechaCorrige() {
        return fechaCorrige;
    }

    public void setFechaCorrige(String fechaCorrige) {
        this.fechaCorrige = fechaCorrige;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    @Override
    public String toString() {
        return "AdmonTabletPDDTO [numEconomico=" + numEconomico + ", idArchivo=" + idArchivo + ", idAdmonTable="
                + idAdmonTable + ", etapa=" + etapa + ", activo=" + activo + ", fechaInstalacion=" + fechaInstalacion
                + ", fechaCambio=" + fechaCambio + ", fechaFalla=" + fechaFalla + ", cursor=" + cursor
                + ", numSerieTablet=" + numSerieTablet + ", detectaFalla=" + detectaFalla + ", describeSucursal="
                + describeSucursal + ", folioFalla=" + folioFalla + ", fechaCorrige=" + fechaCorrige + ", responsable="
                + responsable + "]";
    }
}
