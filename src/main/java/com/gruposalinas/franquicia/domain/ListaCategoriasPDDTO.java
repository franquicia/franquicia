package com.gruposalinas.franquicia.domain;

public class ListaCategoriasPDDTO {

    int idTipoDoc;
    String nombreDoc;
    int nivel;
    int dependeDe;
    String dependeDeDesc;
    int activo;

    public int getIdTipoDoc() {
        return idTipoDoc;
    }

    public void setIdTipoDoc(int idTipoDoc) {
        this.idTipoDoc = idTipoDoc;
    }

    public String getNombreDoc() {
        return nombreDoc;
    }

    public void setNombreDoc(String nombreDoc) {
        this.nombreDoc = nombreDoc;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDependeDe() {
        return dependeDe;
    }

    public void setDependeDe(int dependeDe) {
        this.dependeDe = dependeDe;
    }

    public String getDependeDeDesc() {
        return dependeDeDesc;
    }

    public void setDependeDeDesc(String dependeDeDesc) {
        this.dependeDeDesc = dependeDeDesc;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
