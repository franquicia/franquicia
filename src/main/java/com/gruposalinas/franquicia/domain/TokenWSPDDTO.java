package com.gruposalinas.franquicia.domain;

public class TokenWSPDDTO {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "RespuestaTokenDto{" + "token=" + token + '}';
    }
}
