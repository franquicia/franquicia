package com.gruposalinas.franquicia.domain;

public class FormatoCumplimientoUsuarioDTO {

    private String atenderMejor;
    private String venderMejor;
    private int idUsuario;
    private int ordenador;
    private String nomUsuario;
    private String fecha;
    private String puesto;
    private int bandera;
    private int idFormato;

    public int getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(int idFormato) {
        this.idFormato = idFormato;
    }

    public String getAtenderMejor() {
        return atenderMejor;
    }

    public void setAtenderMejor(String atenderMejor) {
        this.atenderMejor = atenderMejor;
    }

    public String getVenderMejor() {
        return venderMejor;
    }

    public void setVenderMejor(String venderMejor) {
        this.venderMejor = venderMejor;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getOrdenador() {
        return ordenador;
    }

    public void setOrdenador(int ordenador) {
        this.ordenador = ordenador;
    }

    public String getNomUsuario() {
        return nomUsuario;
    }

    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public int getBandera() {
        return bandera;
    }

    public void setBandera(int bandera) {
        this.bandera = bandera;
    }

}
