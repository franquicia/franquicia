package com.gruposalinas.franquicia.domain;

public class ListaCECOXGEOPDDTO {

    private int idCeco;
    private String descCeco;
    private int idGeo;
    private String descGeo;

    public int getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(int idCeco) {
        this.idCeco = idCeco;
    }

    public String getDescCeco() {
        return descCeco;
    }

    public void setDescCeco(String descCeco) {
        this.descCeco = descCeco;
    }

    public int getIdGeo() {
        return idGeo;
    }

    public void setIdGeo(int idGeo) {
        this.idGeo = idGeo;
    }

    public String getDescGeo() {
        return descGeo;
    }

    public void setDescGeo(String descGeo) {
        this.descGeo = descGeo;
    }
}
