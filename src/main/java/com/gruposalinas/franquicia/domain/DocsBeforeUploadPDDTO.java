package com.gruposalinas.franquicia.domain;

public class DocsBeforeUploadPDDTO {

    private Integer fiid_documento;
    private Integer fifolio;
    private String fcnombre_arc;
    private Integer fiid_tipo_docto;
    private String fcnombre_docto;
    private Integer id_cagtegoria;
    private String desc_categoria;
    private Integer fiid_usuario;
    private String fcorigen;
    private String fdvigencia_ini;
    private String fdvigencia_fin;
    private String fcvisible_suc;
    private Integer fiid_estatus;
    private String fctipo_envio;
    private String fdfecha_envio;

    public Integer getFiid_documento() {
        return fiid_documento;
    }

    public void setFiid_documento(Integer fiid_documento) {
        this.fiid_documento = fiid_documento;
    }

    public Integer getFifolio() {
        return fifolio;
    }

    public void setFifolio(Integer fifolio) {
        this.fifolio = fifolio;
    }

    public String getFcnombre_arc() {
        return fcnombre_arc;
    }

    public void setFcnombre_arc(String fcnombre_arc) {
        this.fcnombre_arc = fcnombre_arc;
    }

    public Integer getFiid_tipo_docto() {
        return fiid_tipo_docto;
    }

    public void setFiid_tipo_docto(Integer fiid_tipo_docto) {
        this.fiid_tipo_docto = fiid_tipo_docto;
    }

    public String getFcnombre_docto() {
        return fcnombre_docto;
    }

    public void setFcnombre_docto(String fcnombre_docto) {
        this.fcnombre_docto = fcnombre_docto;
    }

    public Integer getId_cagtegoria() {
        return id_cagtegoria;
    }

    public void setId_cagtegoria(Integer id_cagtegoria) {
        this.id_cagtegoria = id_cagtegoria;
    }

    public String getDesc_categoria() {
        return desc_categoria;
    }

    public void setDesc_categoria(String desc_categoria) {
        this.desc_categoria = desc_categoria;
    }

    public Integer getFiid_usuario() {
        return fiid_usuario;
    }

    public void setFiid_usuario(Integer fiid_usuario) {
        this.fiid_usuario = fiid_usuario;
    }

    public String getFcorigen() {
        return fcorigen;
    }

    public void setFcorigen(String fcorigen) {
        this.fcorigen = fcorigen;
    }

    public String getFdvigencia_ini() {
        return fdvigencia_ini;
    }

    public void setFdvigencia_ini(String fdvigencia_ini) {
        this.fdvigencia_ini = fdvigencia_ini;
    }

    public String getFdvigencia_fin() {
        return fdvigencia_fin;
    }

    public void setFdvigencia_fin(String fdvigencia_fin) {
        this.fdvigencia_fin = fdvigencia_fin;
    }

    public String getFcvisible_suc() {
        return fcvisible_suc;
    }

    public void setFcvisible_suc(String fcvisible_suc) {
        this.fcvisible_suc = fcvisible_suc;
    }

    public Integer getFiid_estatus() {
        return fiid_estatus;
    }

    public void setFiid_estatus(Integer fiid_estatus) {
        this.fiid_estatus = fiid_estatus;
    }

    public String getFctipo_envio() {
        return fctipo_envio;
    }

    public void setFctipo_envio(String fctipo_envio) {
        this.fctipo_envio = fctipo_envio;
    }

    public String getFdfecha_envio() {
        return fdfecha_envio;
    }

    public void setFdfecha_envio(String fdfecha_envio) {
        this.fdfecha_envio = fdfecha_envio;
    }
}
