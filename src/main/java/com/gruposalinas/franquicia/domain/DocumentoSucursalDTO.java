package com.gruposalinas.franquicia.domain;

public class DocumentoSucursalDTO {

    private int idTipo;
    private String tipo;
    private String fileBase64;
    private String negocio;
    private String mensaje;
    private String ultimaActulizacion;
    private String imagenUrl;

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFileBase64() {
        return fileBase64;
    }

    public void setFileBase64(String fileBase64) {
        this.fileBase64 = fileBase64;
    }

    public String getNegocio() {
        return negocio;
    }

    public void setNegocio(String negocio) {
        this.negocio = negocio;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getUltimaActulizacion() {
        return ultimaActulizacion;
    }

    public void setUltimaActulizacion(String ultimaActulizacion) {
        this.ultimaActulizacion = ultimaActulizacion;
    }

    public String getImagenUrl() {
        return imagenUrl;
    }

    public void setImagenUrl(String imagenUrl) {
        this.imagenUrl = imagenUrl;
    }

}
