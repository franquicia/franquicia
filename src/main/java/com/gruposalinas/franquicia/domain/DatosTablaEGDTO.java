package com.gruposalinas.franquicia.domain;

public class DatosTablaEGDTO {

    private int mes;
    private int total;

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
