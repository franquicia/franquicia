package com.gruposalinas.franquicia.domain;

public class TablaEstatusGeneralDTO {

    private int numEconomico;
    private String nombre;
    private String numSerie;
    private String fechaInstalacion;
    private String estatus;
    private String activo;
    private String ultimaActual;

    public int getNumEconomico() {
        return numEconomico;
    }

    public void setNumEconomico(int numEconomico) {
        this.numEconomico = numEconomico;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }

    public String getFechaInstalacion() {
        return fechaInstalacion;
    }

    public void setFechaInstalacion(String fechaInstalacion) {
        this.fechaInstalacion = fechaInstalacion;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public String getUltimaActual() {
        return ultimaActual;
    }

    public void setUltimaActual(String ultimaActual) {
        this.ultimaActual = ultimaActual;
    }
}
