package com.gruposalinas.franquicia.domain;

public class AdmonEstatusTabletaPDDTO {

    private int idEstadoTableta;
    private String descWEB;
    private String descJSON;
    private String visible;
    private int idStatus;
    private String usuario;
    private String fechaCambio;
    private int dependeDe;
    private int activo;

    public int getIdEstadoTableta() {
        return idEstadoTableta;
    }

    public void setIdEstadoTableta(int idEstadoTableta) {
        this.idEstadoTableta = idEstadoTableta;
    }

    public String getDescWEB() {
        return descWEB;
    }

    public void setDescWEB(String descWEB) {
        this.descWEB = descWEB;
    }

    public String getDescJSON() {
        return descJSON;
    }

    public void setDescJSON(String descJSON) {
        this.descJSON = descJSON;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public int getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(int idStatus) {
        this.idStatus = idStatus;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFechaCambio() {
        return fechaCambio;
    }

    public void setFechaCambio(String fechaCambio) {
        this.fechaCambio = fechaCambio;
    }

    public int getDependeDe() {
        return dependeDe;
    }

    public void setDependeDe(int dependeDe) {
        this.dependeDe = dependeDe;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "AdmonEstatusTabletaPDDTO [idEstadoTableta=" + idEstadoTableta + ", descWEB=" + descWEB + ", descJSON="
                + descJSON + ", visible=" + visible + ", idStatus=" + idStatus + ", usuario=" + usuario
                + ", fechaCambio=" + fechaCambio + ", dependeDe=" + dependeDe + "]";
    }

}
