package com.gruposalinas.franquicia.domain;

public class FileChecksumPDDTO {

    private Integer fiid_enc_arch;
    private Integer fireferencia;
    private String fcorigen;
    private String fchash;
    private String fcuser;
    private String fdfecha_cambio;
    private Integer fiactivo;

    public Integer getFiid_enc_arch() {
        return fiid_enc_arch;
    }

    public void setFiid_enc_arch(Integer fiid_enc_arch) {
        this.fiid_enc_arch = fiid_enc_arch;
    }

    public Integer getFireferencia() {
        return fireferencia;
    }

    public void setFireferencia(Integer fireferencia) {
        this.fireferencia = fireferencia;
    }

    public String getFcorigen() {
        return fcorigen;
    }

    public void setFcorigen(String fcorigen) {
        this.fcorigen = fcorigen;
    }

    public String getFchash() {
        return fchash;
    }

    public void setFchash(String fchash) {
        this.fchash = fchash;
    }

    public String getFcuser() {
        return fcuser;
    }

    public void setFcuser(String fcuser) {
        this.fcuser = fcuser;
    }

    public String getFdfecha_cambio() {
        return fdfecha_cambio;
    }

    public void setFdfecha_cambio(String fdfecha_cambio) {
        this.fdfecha_cambio = fdfecha_cambio;
    }

    public Integer getFiactivo() {
        return fiactivo;
    }

    public void setFiactivo(Integer fiactivo) {
        this.fiactivo = fiactivo;
    }
}
