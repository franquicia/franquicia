package com.gruposalinas.franquicia.domain;

import java.util.ArrayList;

public class RespuestaCecosWSPDDTO {

    private Integer codigo;
    private String mensaje;
    private ArrayList<CecosWSPDDTO> listaTablaEKT;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public ArrayList<CecosWSPDDTO> getListaTablaEKT() {
        return listaTablaEKT;
    }

    public void setListaTablaEKT(ArrayList<CecosWSPDDTO> listaTablaEKT) {
        this.listaTablaEKT = listaTablaEKT;
    }
}
