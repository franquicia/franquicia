package com.gruposalinas.franquicia.domain;

public class BitMantenimientoDTO {

    private String nomProveedor;
    private int tipoMant;
    private String folio;
    private String fechaSolicitud;
    private String horaEntra;
    private String horaSale;
    private int numPersonas;
    private String detalle;
    private int idUser;
    private String fechaVisita;

    public String getNomProveedor() {
        return nomProveedor;
    }

    public void setNomProveedor(String nomProveedor) {
        this.nomProveedor = nomProveedor;
    }

    public int getTipoMant() {
        return tipoMant;
    }

    public void setTipoMant(int tipoMant) {
        this.tipoMant = tipoMant;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(String fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public String getHoraEntra() {
        return horaEntra;
    }

    public void setHoraEntra(String horaEntra) {
        this.horaEntra = horaEntra;
    }

    public String getHoraSale() {
        return horaSale;
    }

    public void setHoraSale(String horaSale) {
        this.horaSale = horaSale;
    }

    public int getNumPersonas() {
        return numPersonas;
    }

    public void setNumPersonas(int numPersonas) {
        this.numPersonas = numPersonas;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(String fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

}
