package com.gruposalinas.franquicia.domain;

public class FoliosEstatusPDDTO {

    private int idTipoDocto;
    private String nombreDocto;
    private int dependeDe;
    private int activo;

    public int getIdTipoDocto() {
        return idTipoDocto;
    }

    public void setIdTipoDocto(int idTipoDocto) {
        this.idTipoDocto = idTipoDocto;
    }

    public String getNombreDocto() {
        return nombreDocto;
    }

    public void setNombreDocto(String nombreDocto) {
        this.nombreDocto = nombreDocto;
    }

    public int getDependeDe() {
        return dependeDe;
    }

    public void setDependeDe(int dependeDe) {
        this.dependeDe = dependeDe;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
