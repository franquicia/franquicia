package com.gruposalinas.franquicia.domain;

public class RepDetaCecoPDDTO {

    private String territorio;
    private String zona;
    private String region;
    private String pais;
    private String estado;
    private String municipio;
    private String nombre;
    private int idSucursal;
    private String descCategoria;
    private String fechaEnv;
    private String nombreDoc;
    private String descVisibleSuc;
    private String nombreArch;
    private String vigenciaIni;
    private String vigenciaFin;
    private String envio;
    private int idFolio;

    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(int idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getDescCategoria() {
        return descCategoria;
    }

    public void setDescCategoria(String descCategoria) {
        this.descCategoria = descCategoria;
    }

    public String getFechaEnv() {
        return fechaEnv;
    }

    public void setFechaEnv(String fechaEnv) {
        this.fechaEnv = fechaEnv;
    }

    public String getNombreDoc() {
        return nombreDoc;
    }

    public void setNombreDoc(String nombreDoc) {
        this.nombreDoc = nombreDoc;
    }

    public String getDescVisibleSuc() {
        return descVisibleSuc;
    }

    public void setDescVisibleSuc(String descVisibleSuc) {
        this.descVisibleSuc = descVisibleSuc;
    }

    public String getNombreArch() {
        return nombreArch;
    }

    public void setNombreArch(String nombreArch) {
        this.nombreArch = nombreArch;
    }

    public String getVigenciaIni() {
        return vigenciaIni;
    }

    public void setVigenciaIni(String vigenciaIni) {
        this.vigenciaIni = vigenciaIni;
    }

    public String getVigenciaFin() {
        return vigenciaFin;
    }

    public void setVigenciaFin(String vigenciaFin) {
        this.vigenciaFin = vigenciaFin;
    }

    public String getEnvio() {
        return envio;
    }

    public void setEnvio(String envio) {
        this.envio = envio;
    }

    public int getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(int idFolio) {
        this.idFolio = idFolio;
    }
}
