package com.gruposalinas.franquicia.domain;

public class SucEstTabPDDTO {

    private Integer fiid_tableta;
    private String fcnum_serie;
    private String fcid_ceco;
    private Integer sucursal;
    private String nom_ceco;
    private Integer usuario_tablet;
    private Integer fiid_estatus;
    private String desc_id_estatus;
    private String fdfecha_instala;
    private String fcactivo;
    private String fcfabricante;
    private String fcmodelo;
    private String fcsistema_o;
    private String fcversion_app;
    private String ultima_actualizacion;
    private String descactivo_ceco;

    public String getDescactivo_ceco() {
        return descactivo_ceco;
    }

    public void setDescactivo_ceco(String descactivo_ceco) {
        this.descactivo_ceco = descactivo_ceco;
    }

    public Integer getFiid_tableta() {
        return fiid_tableta;
    }

    public void setFiid_tableta(Integer fiid_tableta) {
        this.fiid_tableta = fiid_tableta;
    }

    public String getFcnum_serie() {
        return fcnum_serie;
    }

    public void setFcnum_serie(String fcnum_serie) {
        this.fcnum_serie = fcnum_serie;
    }

    public String getFcid_ceco() {
        return fcid_ceco;
    }

    public void setFcid_ceco(String fcid_ceco) {
        this.fcid_ceco = fcid_ceco;
    }

    public Integer getSucursal() {
        return sucursal;
    }

    public void setSucursal(Integer sucursal) {
        this.sucursal = sucursal;
    }

    public String getNom_ceco() {
        return nom_ceco;
    }

    public void setNom_ceco(String nom_ceco) {
        this.nom_ceco = nom_ceco;
    }

    public Integer getUsuario_tablet() {
        return usuario_tablet;
    }

    public void setUsuario_tablet(Integer usuario_tablet) {
        this.usuario_tablet = usuario_tablet;
    }

    public Integer getFiid_estatus() {
        return fiid_estatus;
    }

    public void setFiid_estatus(Integer fiid_estatus) {
        this.fiid_estatus = fiid_estatus;
    }

    public String getDesc_id_estatus() {
        return desc_id_estatus;
    }

    public void setDesc_id_estatus(String desc_id_estatus) {
        this.desc_id_estatus = desc_id_estatus;
    }

    public String getFdfecha_instala() {
        return fdfecha_instala;
    }

    public void setFdfecha_instala(String fdfecha_instala) {
        this.fdfecha_instala = fdfecha_instala;
    }

    public String getFcactivo() {
        return fcactivo;
    }

    public void setFcactivo(String fcactivo) {
        this.fcactivo = fcactivo;
    }

    public String getFcfabricante() {
        return fcfabricante;
    }

    public void setFcfabricante(String fcfabricante) {
        this.fcfabricante = fcfabricante;
    }

    public String getFcmodelo() {
        return fcmodelo;
    }

    public void setFcmodelo(String fcmodelo) {
        this.fcmodelo = fcmodelo;
    }

    public String getFcsistema_o() {
        return fcsistema_o;
    }

    public void setFcsistema_o(String fcsistema_o) {
        this.fcsistema_o = fcsistema_o;
    }

    public String getFcversion_app() {
        return fcversion_app;
    }

    public void setFcversion_app(String fcversion_app) {
        this.fcversion_app = fcversion_app;
    }

    public String getUltima_actualizacion() {
        return ultima_actualizacion;
    }

    public void setUltima_actualizacion(String ultima_actualizacion) {
        this.ultima_actualizacion = ultima_actualizacion;
    }
}
