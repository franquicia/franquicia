package com.gruposalinas.franquicia.domain;

public class DocCorruptosPDDTO {

    private Integer fifolio;
    private Integer fiid_documento;
    private String fcnombre_arc;
    private String tipo_envio;
    private String situacion_docto;
    private String fcid_ceco;
    private String nom_ceco;
    private Integer id_categoria;
    private String desc_categor;
    private Integer fiid_tipo_docto;
    private String fcnombre_docto;
    private Integer fiid_doctopedestal;
    private String vigencia_ini;
    private String vigencia_fin;
    private Integer fipeso_bytes;
    private Integer estatus_doc;
    private String desc_estatus_doc;
    private Integer esta_doc_suc;
    private String desc_esta_doc_suc;
    private String fchash;
    private String ult_fecha_doc_p;
    private Integer activo_docto;

    public Integer getFifolio() {
        return fifolio;
    }

    public void setFifolio(Integer fifolio) {
        this.fifolio = fifolio;
    }

    public Integer getFiid_documento() {
        return fiid_documento;
    }

    public void setFiid_documento(Integer fiid_documento) {
        this.fiid_documento = fiid_documento;
    }

    public String getFcnombre_arc() {
        return fcnombre_arc;
    }

    public void setFcnombre_arc(String fcnombre_arc) {
        this.fcnombre_arc = fcnombre_arc;
    }

    public String getTipo_envio() {
        return tipo_envio;
    }

    public void setTipo_envio(String tipo_envio) {
        this.tipo_envio = tipo_envio;
    }

    public String getSituacion_docto() {
        return situacion_docto;
    }

    public void setSituacion_docto(String situacion_docto) {
        this.situacion_docto = situacion_docto;
    }

    public String getFcid_ceco() {
        return fcid_ceco;
    }

    public void setFcid_ceco(String fcid_ceco) {
        this.fcid_ceco = fcid_ceco;
    }

    public String getNom_ceco() {
        return nom_ceco;
    }

    public void setNom_ceco(String nom_ceco) {
        this.nom_ceco = nom_ceco;
    }

    public Integer getId_categoria() {
        return id_categoria;
    }

    public void setId_categoria(Integer id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getDesc_categor() {
        return desc_categor;
    }

    public void setDesc_categor(String desc_categor) {
        this.desc_categor = desc_categor;
    }

    public Integer getFiid_tipo_docto() {
        return fiid_tipo_docto;
    }

    public void setFiid_tipo_docto(Integer fiid_tipo_docto) {
        this.fiid_tipo_docto = fiid_tipo_docto;
    }

    public String getFcnombre_docto() {
        return fcnombre_docto;
    }

    public void setFcnombre_docto(String fcnombre_docto) {
        this.fcnombre_docto = fcnombre_docto;
    }

    public Integer getFiid_doctopedestal() {
        return fiid_doctopedestal;
    }

    public void setFiid_doctopedestal(Integer fiid_doctopedestal) {
        this.fiid_doctopedestal = fiid_doctopedestal;
    }

    public String getVigencia_ini() {
        return vigencia_ini;
    }

    public void setVigencia_ini(String vigencia_ini) {
        this.vigencia_ini = vigencia_ini;
    }

    public String getVigencia_fin() {
        return vigencia_fin;
    }

    public void setVigencia_fin(String vigencia_fin) {
        this.vigencia_fin = vigencia_fin;
    }

    public Integer getFipeso_bytes() {
        return fipeso_bytes;
    }

    public void setFipeso_bytes(Integer fipeso_bytes) {
        this.fipeso_bytes = fipeso_bytes;
    }

    public Integer getEstatus_doc() {
        return estatus_doc;
    }

    public void setEstatus_doc(Integer estatus_doc) {
        this.estatus_doc = estatus_doc;
    }

    public String getDesc_estatus_doc() {
        return desc_estatus_doc;
    }

    public void setDesc_estatus_doc(String desc_estatus_doc) {
        this.desc_estatus_doc = desc_estatus_doc;
    }

    public Integer getEsta_doc_suc() {
        return esta_doc_suc;
    }

    public void setEsta_doc_suc(Integer esta_doc_suc) {
        this.esta_doc_suc = esta_doc_suc;
    }

    public String getDesc_esta_doc_suc() {
        return desc_esta_doc_suc;
    }

    public void setDesc_esta_doc_suc(String desc_esta_doc_suc) {
        this.desc_esta_doc_suc = desc_esta_doc_suc;
    }

    public String getFchash() {
        return fchash;
    }

    public void setFchash(String fchash) {
        this.fchash = fchash;
    }

    public String getUlt_fecha_doc_p() {
        return ult_fecha_doc_p;
    }

    public void setUlt_fecha_doc_p(String ult_fecha_doc_p) {
        this.ult_fecha_doc_p = ult_fecha_doc_p;
    }

    public Integer getActivo_docto() {
        return activo_docto;
    }

    public void setActivo_docto(Integer activo_docto) {
        this.activo_docto = activo_docto;
    }
}
