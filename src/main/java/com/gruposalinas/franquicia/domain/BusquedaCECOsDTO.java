package com.gruposalinas.franquicia.domain;

public class BusquedaCECOsDTO {

    private int idCeco;
    private int tipoCeco;
    private String nombre;
    private int cecoSuperior;
    private String cecoSuperiorDesc;
    private int negocio;
    private int activo;

    public int getIdCeco() {
        return idCeco;
    }

    public void setIdCeco(int idCeco) {
        this.idCeco = idCeco;
    }

    public int getTipoCeco() {
        return tipoCeco;
    }

    public void setTipoCeco(int tipoCeco) {
        this.tipoCeco = tipoCeco;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCecoSuperior() {
        return cecoSuperior;
    }

    public void setCecoSuperior(int cecoSuperior) {
        this.cecoSuperior = cecoSuperior;
    }

    public String getCecoSuperiorDesc() {
        return cecoSuperiorDesc;
    }

    public void setCecoSuperiorDesc(String cecoSuperiorDesc) {
        this.cecoSuperiorDesc = cecoSuperiorDesc;
    }

    public int getNegocio() {
        return negocio;
    }

    public void setNegocio(int negocio) {
        this.negocio = negocio;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
