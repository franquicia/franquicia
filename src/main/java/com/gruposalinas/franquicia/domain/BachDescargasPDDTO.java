package com.gruposalinas.franquicia.domain;

public class BachDescargasPDDTO {

    private String tipoDescarga;
    private String numSerie;
    private int idEstatus;
    private String fechaActu;
    private String respuesta;
    private int idControlDesc;
    private int idTablet;
    private String fcUser;
    private int activo;

    public String getTipoDescarga() {
        return tipoDescarga;
    }

    public void setTipoDescarga(String tipoDescarga) {
        this.tipoDescarga = tipoDescarga;
    }

    public String getNumSerie() {
        return numSerie;
    }

    public void setNumSerie(String numSerie) {
        this.numSerie = numSerie;
    }

    public int getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
        this.idEstatus = idEstatus;
    }

    public String getFechaActu() {
        return fechaActu;
    }

    public void setFechaActu(String fechaActu) {
        this.fechaActu = fechaActu;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public int getIdControlDesc() {
        return idControlDesc;
    }

    public void setIdControlDesc(int idControlDesc) {
        this.idControlDesc = idControlDesc;
    }

    public int getIdTablet() {
        return idTablet;
    }

    public void setIdTablet(int idTablet) {
        this.idTablet = idTablet;
    }

    public String getFcUser() {
        return fcUser;
    }

    public void setFcUser(String fcUser) {
        this.fcUser = fcUser;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "BachDescargasPDDTO [tipoDescarga=" + tipoDescarga + ", numSerie=" + numSerie + ", idEstatus=" + idEstatus + ", fechaActu=" + fechaActu + ", respuesta=" + respuesta + ", idControlDesc=" + idControlDesc + ", idTablet="
                + idTablet + ", fcUser=" + fcUser + ", activo=" + activo + "]";
    }
}
