package com.gruposalinas.franquicia.domain;

public class DoctoDataPDDTO {

    private int idDocumento;
    private int idFolio;
    private String nombreDocto;
    private int tipoDocto;
    private int dependeDe;
    private String categoria;
    private int idUsuario;
    private long peso;
    private String origen;
    private String vigenciaIni;
    private String vigenciaFin;
    private int idEstatus;
    private String descEstatus;
    private int activo;

    public int getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(int idDocumento) {
        this.idDocumento = idDocumento;
    }

    public int getIdFolio() {
        return idFolio;
    }

    public void setIdFolio(int idFolio) {
        this.idFolio = idFolio;
    }

    public String getNombreDocto() {
        return nombreDocto;
    }

    public void setNombreDocto(String nombreDocto) {
        this.nombreDocto = nombreDocto;
    }

    public int getTipoDocto() {
        return tipoDocto;
    }

    public void setTipoDocto(int tipoDocto) {
        this.tipoDocto = tipoDocto;
    }

    public int getDependeDe() {
        return dependeDe;
    }

    public void setDependeDe(int dependeDe) {
        this.dependeDe = dependeDe;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public long getPeso() {
        return peso;
    }

    public void setPeso(long peso) {
        this.peso = peso;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getVigenciaIni() {
        return vigenciaIni;
    }

    public void setVigenciaIni(String vigenciaIni) {
        this.vigenciaIni = vigenciaIni;
    }

    public String getVigenciaFin() {
        return vigenciaFin;
    }

    public void setVigenciaFin(String vigenciaFin) {
        this.vigenciaFin = vigenciaFin;
    }

    public int getIdEstatus() {
        return idEstatus;
    }

    public void setIdEstatus(int idEstatus) {
        this.idEstatus = idEstatus;
    }

    public String getDescEstatus() {
        return descEstatus;
    }

    public void setDescEstatus(String descEstatus) {
        this.descEstatus = descEstatus;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
}
