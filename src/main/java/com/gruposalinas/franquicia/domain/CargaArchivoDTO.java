package com.gruposalinas.franquicia.domain;

public class CargaArchivoDTO {

    private int id_cargaArchivo;
    private int id_usuario;
    private String nombre;
    private String describe;
    private int visibleSucursal;
    private String tipoArchivo;
    private String cursor;
    private String fecha;
    private int activo;

    public int getId_cargaArchivo() {
        return id_cargaArchivo;
    }

    public void setId_cargaArchivo(int id_cargaArchivo) {
        this.id_cargaArchivo = id_cargaArchivo;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public int getVisibleSucursal() {
        return visibleSucursal;
    }

    public void setVisibleSucursal(int visibleSucursal) {
        this.visibleSucursal = visibleSucursal;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "CargaArchivoDTO [id_cargaArchivo=" + id_cargaArchivo + ", id_usuario=" + id_usuario + ", nombre=" + nombre + ", describe=" + describe + ", visibleSucursal=" + visibleSucursal + ", tipoArchivo=" + tipoArchivo + ", cursor="
                + cursor + ", fecha=" + fecha + ", activo=" + activo + "]";
    }
}
