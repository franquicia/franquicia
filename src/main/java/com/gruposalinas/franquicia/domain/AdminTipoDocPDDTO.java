package com.gruposalinas.franquicia.domain;

public class AdminTipoDocPDDTO {

    private int idTipoDoc;
    private String nombre;
    private int nivel;
    private int dependeDe;
    private String descDependeDe;
    private String user;
    private String fecha;
    private int activo;

    public int getIdTipoDoc() {
        return idTipoDoc;
    }

    public void setIdTipoDoc(int idTipoDoc) {
        this.idTipoDoc = idTipoDoc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getDependeDe() {
        return dependeDe;
    }

    public void setDependeDe(int dependeDe) {
        this.dependeDe = dependeDe;
    }

    public String getDescDependeDe() {
        return descDependeDe;
    }

    public void setDescDependeDe(String descDependeDe) {
        this.descDependeDe = descDependeDe;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "AdminTipoDoc [idTipoDoc=" + idTipoDoc + ", nombre=" + nombre + ", nivel=" + nivel + ", dependeDe="
                + dependeDe + ", descDependeDe=" + descDependeDe + ", user=" + user + ", fecha=" + fecha + ", activo="
                + activo + "]";
    }

}
