package com.gruposalinas.franquicia.domain;

import java.util.List;

public class UsuarioPDDTO {

    private List<LoginPDDTO> usuario;
    private int respuesta;

    public List<LoginPDDTO> getUsuario() {
        return usuario;
    }

    public void setUsuario(List<LoginPDDTO> usuario) {
        this.usuario = usuario;
    }

    public int getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(int respuesta) {
        this.respuesta = respuesta;
    }

}
