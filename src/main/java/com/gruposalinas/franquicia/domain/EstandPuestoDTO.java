package com.gruposalinas.franquicia.domain;

public class EstandPuestoDTO {

    private int area;
    private String cajon1;
    private String cajon2;
    private String cajon3;
    private int tipoMueble;
    private String fecha;
    private String ceco;

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getCajon1() {
        return cajon1;
    }

    public void setCajon1(String cajon1) {
        this.cajon1 = cajon1;
    }

    public String getCajon2() {
        return cajon2;
    }

    public void setCajon2(String cajon2) {
        this.cajon2 = cajon2;
    }

    public String getCajon3() {
        return cajon3;
    }

    public void setCajon3(String cajon3) {
        this.cajon3 = cajon3;
    }

    public int getTipoMueble() {
        return tipoMueble;
    }

    public void setTipoMueble(int tipoMueble) {
        this.tipoMueble = tipoMueble;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCeco() {
        return ceco;
    }

    public void setCeco(String ceco) {
        this.ceco = ceco;
    }

}
