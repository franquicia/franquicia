package com.gruposalinas.franquicia.domain;

public class DetalleApoyoDTO {

    private int idEvidencia;
    private String ruta;
    private String nomarchivo;

    public int getIdEvidencia() {
        return idEvidencia;
    }

    public void setIdEvidencia(int idEvidencia) {
        this.idEvidencia = idEvidencia;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getNomarchivo() {
        return nomarchivo;
    }

    public void setNomarchivo(String nomarchivo) {
        this.nomarchivo = nomarchivo;
    }

}
