package com.gruposalinas.franquicia.controller;

import com.gruposalinas.franquicia.business.MenuBI;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MenuController {

    @Autowired
    private MenuBI menuBI;

    @RequestMapping(value = "/irBuscador.htm", method = RequestMethod.GET)
    public ModelAndView masBuscados(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        //return menuBI.masBuscados();
        return null;
    }

    @RequestMapping(value = {"tienda/queEs.htm", "central/queEs.htm"}, method = RequestMethod.GET)
    public ModelAndView queEs(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("queEs");
    }

    @RequestMapping(value = {"tienda/queVerificar.htm", "central/queVerificar.htm"}, method = RequestMethod.GET)
    public ModelAndView queVerificar(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("queVerificar");
    }

    @RequestMapping(value = {"tienda/queHacer.htm", "central/queHacer.htm"}, method = RequestMethod.GET)
    public ModelAndView queHacer(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("queHacer");
    }

    @RequestMapping(value = {"tienda/guiasOperativas.htm", "central/guiasOperativas.htm"}, method = RequestMethod.GET)
    public ModelAndView guiasOperativas(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("guiasOperativas");
    }

    @RequestMapping(value = {"tienda/miApertura.htm", "central/miApertura.htm"}, method = RequestMethod.GET)
    public ModelAndView miApertura(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("miApertura");
    }

    @RequestMapping(value = {"tienda/juntaInicioDia.htm", "central/juntaInicioDia.htm"}, method = RequestMethod.GET)
    public ModelAndView juntaInicioDia(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("juntaInicioDia");
    }

    @RequestMapping(value = {"tienda/preparandoCierre.htm", "central/preparandoCierre.htm"}, method = RequestMethod.GET)
    public ModelAndView preparandoCierre(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("preparandoCierre");
    }

    @RequestMapping(value = {"tienda/cobroResultados.htm", "central/cobroResultados.htm"}, method = RequestMethod.GET)
    public ModelAndView cobroResultados(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {
        return new ModelAndView("cobroResultados");
    }

    @RequestMapping(value = {"tienda/ayuda.htm", "central/ayuda.htm"}, method = RequestMethod.GET)
    public ModelAndView ayuda(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        return new ModelAndView("ayudaAndroid");
    }

    @RequestMapping(value = {"tienda/carrusel7s.htm", "central/carrusel7s.htm"}, method = RequestMethod.GET)
    public ModelAndView carrusel7s(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        return new ModelAndView("carrusel7s");
    }

    /*

	@RequestMapping(value={"tienda/checklistDesign.htm","central/checklistDesign.htm"},method=RequestMethod.GET)
	public ModelAndView checkListDesign(HttpServletRequest requqest,HttpServletResponse response,Model model)
			throws ServletException, IOException{


		return new ModelAndView("checklistDesign");

	}*/
    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest request, Exception ex) {
        UtilFRQ.printErrorLog(request, ex);
        ModelAndView mv = new ModelAndView("exception");
        return mv;
    }
}
