package com.gruposalinas.franquicia.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gruposalinas.franquicia.dao.CargaArchivosPedestalDAO;
import com.gruposalinas.franquicia.domain.CecosWSPDDTO;
import com.gruposalinas.franquicia.domain.ParamCecoPDDTO;
import com.gruposalinas.franquicia.domain.RespuestaCecosWSPDDTO;
import com.gruposalinas.franquicia.domain.TokenCecosDTO;
import com.gruposalinas.franquicia.domain.TokenWSPDDTO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/central/pedestalDigital/cecosws/")
public class CecosPDController {

    @Autowired
    CargaArchivosPedestalDAO cargaArchivo;

    private static final Logger logger = LogManager.getLogger(CecosPDController.class);

    @RequestMapping(value = "ekt.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ArrayList<CecosWSPDDTO> distribucion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ArrayList<CecosWSPDDTO> list = null;
        TokenWSPDDTO respuesta = new TokenWSPDDTO();

        HttpURLConnection conn = null;
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        URL url = new URL("http://10.50.109.69:8080/cecows/rest/token?usuario=USRREFLI&contrasena=UsrRLfx72"); // DESA
        // TODO: Obtener claves de PROD para este servicio
        // URL url = new URL("http://10.53.33.85:8643/cecows/rest/token?usuario=" + user + "&contrasena=" + pass); // PROD

        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("cache-control", "no-cache");

            if (conn.getResponseCode() != 200 && conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            String output = null;

            while ((output = br.readLine()) != null) {
                logger.info("Se obtiene token...");
                respuesta = gson.fromJson(output.toString(), TokenWSPDDTO.class);
                logger.info("Se inicia proceso para obtener estructura unificada de cecos ekt...");
                list = unificada(respuesta.getToken());
            }

            return list;
        } catch (Exception e) {
            logger.info("Algo ocurrio al obtener el token para el proceso de cecos...");
            e.printStackTrace();
            conn.disconnect();
            return new ArrayList<CecosWSPDDTO>();
        } finally {
            conn.disconnect();
        }
    }

    @RequestMapping(value = "updateCecosEkt.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateCecosEkt(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Ejecutar proceso para truncar tabla
        logger.info("Truncando tabla de cecos...");
        logger.info(cargaArchivo.truncateTPasoCeco());
        logger.info("Finaliza proceso para truncar tabla de cecos...");

        HttpURLConnection conn = null;
        ArrayList<CecosWSPDDTO> list = null;
        TokenCecosDTO respuesta = new TokenCecosDTO();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        URL url = new URL("http://10.50.109.69:8080/cecows/rest/token?usuario=USRREFLI&contrasena=UsrRLfx72"); // DESA
        // TODO: Obtener claves de PROD para este servicio
        // URL url = new URL("http://10.53.33.85:8643/cecows/rest/token?usuario=" + user + "&contrasena=" + pass); // PROD

        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("cache-control", "no-cache");

            if (conn.getResponseCode() != 200 && conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            String output = null;

            // Proceso que inserta informacion en tabla PDTP_PASO_CECO
            while ((output = br.readLine()) != null) {
                logger.info("Se obtiene token...");
                respuesta = gson.fromJson(output.toString(), TokenCecosDTO.class);
                logger.info("Se inicia proceso para obtener estructura unificada de cecos ekt...");
                list = unificada(respuesta.getToken() != null ? respuesta.getToken() : "null");
                logger.info("Se termina proceso para obtener estructura unificada de cecos ekt...");
            }

            // Obtiene listado de parametros para insertar cecos en PDTA_CECO
            List<ParamCecoPDDTO> paramList = cargaArchivo.getCecoParameters();
            for (ParamCecoPDDTO param : paramList) {
                String resp = cargaArchivo.updateCecosByParam(0, param.getFcvalor(), param.getFiparametro());
                logger.info("A -- " + param.getFcvalor() + " -- B -- " + param.getFiparametro());
            }

            return "OK";
        } catch (Exception e) {
            logger.info("Algo ocurrio al obtener el token para el proceso de cecos...");
            e.printStackTrace();
            conn.disconnect();
            return "ERROR";
        } finally {
            conn.disconnect();
        }
    }

    @SuppressWarnings("unused")
    private ArrayList<CecosWSPDDTO> unificada(String token) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        ArrayList<CecosWSPDDTO> respuesta = new ArrayList<CecosWSPDDTO>();

        RespuestaCecosWSPDDTO _resp = null;
        ArrayList<CecosWSPDDTO> list = null;

        URL urlConstant = new URL("http://10.50.109.69:8080/cecows/rest/ekt/unificada?token="); // DESA
        // URL urlConstant = new URL("http://10.53.33.85:8643/cecows/rest/ekt/unificada?token="); // PROD

        URL urlList = new URL(urlConstant + URLEncoder.encode(token, "UTF-8"));
        HttpURLConnection conn = null;

        logger.info(urlList);

        try {
            conn = (HttpURLConnection) urlList.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("cache-control", "no-cache");

            if (conn.getResponseCode() != 200 && conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            String output;
            StringBuilder stringBuilder = new StringBuilder();

            logger.info("Obteniendo cadena de cecos...");
            while ((output = br.readLine()) != null) {
                stringBuilder.append(output);
            }

            logger.info("Creando objeto de java con cadena de cecos...");
            _resp = gson.fromJson(stringBuilder.toString(), RespuestaCecosWSPDDTO.class);
            logger.info("Objeto creado...");

            if (_resp.getCodigo() == 0) {
                list = _resp.getListaTablaEKT();

                /**
                 * PROCESO QUE INSERTA CECOS EN TPASO_CECO *
                 */
                String resp = null;
                for (CecosWSPDDTO cecoWsDTO : list) {
                    resp = cargaArchivo.insertTPasoCeco(cecoWsDTO);
                    // logger.info(resp);
                }

                return list;
            } else {
                logger.info(_resp.getMensaje());
            }
        } catch (Exception e) {
            logger.info("Ocurrió una excepcion en la extracción de la información de los CECOS de Estructura.:" + e);
            return new ArrayList<CecosWSPDDTO>();
        } finally {
            conn.disconnect();
        }

        return new ArrayList<CecosWSPDDTO>();
    }
}
