package com.gruposalinas.franquicia.controller;

import com.gruposalinas.franquicia.business.LoginBI;
import com.gruposalinas.franquicia.domain.Login;
import com.gruposalinas.franquicia.domain.LoginDTO;
import com.gruposalinas.franquicia.util.UtilFRQ;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {

    @Autowired
    LoginBI loginBI;

    @RequestMapping(value = "/login.htm", method = RequestMethod.GET)
    public ModelAndView login() {
        Login login = new Login();
        return new ModelAndView("login", "command", login);
    }

    @InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setAllowedFields("user", "pass");
    }

    @RequestMapping(value = "/consultaLogin.htm", method = RequestMethod.POST)
    public ModelAndView consultaLogin(@ModelAttribute("login") Login login, BindingResult result, HttpServletRequest request) throws Exception {

        int user = login.getUser();
        String pass = login.getPass();
        String valida = request.getParameter("valida");
        ModelAndView salida = null;

        if (user > 0 && valida.equals("true")) {
            LoginDTO regEmpleado = null;
            try {
                regEmpleado = loginBI.consultaUsuario(user);
            } catch (Exception e) {
                UtilFRQ.printErrorLog(request, e);
            }
            if (regEmpleado != null) {
                if (pass.equals(regEmpleado.getNombreEmpelado())) {
                    salida = new ModelAndView("redirect:/central/schedulerForm.htm");
                    salida.addObject("valida", valida);

                } else {
                    request.setAttribute("mensaje", "Password incorrecto");
                    salida = new ModelAndView("login", "command", login);
                    salida.addObject("invalido", "Password incorrecto");
                }
            } else {
                request.setAttribute("mensaje", "Usuario no registrado como administrador");
                salida = new ModelAndView("login", "command", login);
                salida.addObject("invalido", "Usuario no registrado");
            }
        } else {
            request.setAttribute("mensaje", "Número de empleado o llave incorrecta, favor de verificar");
            salida = new ModelAndView("login", "command", login);
            salida.addObject("invalido", "Número de empleado incorrecto");
        }
        return salida;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest request, Exception ex) {
        UtilFRQ.printErrorLog(request, ex);
        ModelAndView mv = new ModelAndView("exception");
        return mv;
    }
}
