package com.gruposalinas.franquicia.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PruebaController {

    @RequestMapping(value = "/pruebaFranquicia0.htm", method = RequestMethod.GET)
    public ModelAndView pruebaFranquicia0(HttpServletRequest request, HttpServletResponse response, Model model) {

        return new ModelAndView("pruebaFranquicia");
    }

    @RequestMapping(value = "/pruebaFranquicia1.htm", method = RequestMethod.GET)
    public ModelAndView pruebaFranquicia1(HttpServletRequest request, HttpServletResponse response, Model model) {

        return new ModelAndView("mailBuzon");
    }
}
