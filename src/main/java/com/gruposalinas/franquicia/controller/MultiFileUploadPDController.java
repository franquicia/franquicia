package com.gruposalinas.franquicia.controller;

import com.google.gson.Gson;
import com.gruposalinas.franquicia.business.CargaArchivosPedestalDigitalBI;
import com.gruposalinas.franquicia.domain.ListaCategoriasPDDTO;
import com.gruposalinas.franquicia.domain.LoginPDDTO;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping("/central/pedestalDigital/upload/")
public class MultiFileUploadPDController {

    @Autowired
    CargaArchivosPedestalDigitalBI cargaArchivos;

    private static Logger logger = LogManager.getLogger(MultiFileUploadPDController.class);

    private String PATH_DESA = File.listRoots()[0].getAbsolutePath() + "/users/rodolfozenilcruz/documents/UPLOAD/franquicia/pedestal_digital/cargas/"; // "/root/franquicia/pedestal_digital/cargas/";
    private String PATH_PROD = File.listRoots()[0].getAbsolutePath() + "/franquicia/pedestal_digital/cargas/";

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "multiFileUploadPD.htm", method = RequestMethod.POST)
    public ResponseEntity multiFileUploadPD(MultipartHttpServletRequest request) throws NumberFormatException, IOException, Exception {

        LoginPDDTO usuario = (LoginPDDTO) request.getSession().getAttribute("userBean");

        if (usuario == null) {
            return new ResponseEntity<>("{\t\n\"idFolio\":\"" + "0" + "\"\n}", HttpStatus.OK);
        }

        String negocio = "41"; // (String) request.getSession().getAttribute("idNegocio");
        DocData[] docDataArr = (DocData[]) request.getSession().getAttribute("docData");
        DistribData[] distribDataArr = (DistribData[]) request.getSession().getAttribute("distribData");
        EnvData[] envDataArr = (EnvData[]) request.getSession().getAttribute("envData");

        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String strDate = dateFormat.format(new Date());
        String _insDate = dateFormat.format(new Date());

        /**
         * Si _strVF es una cadena vacia, significa que no hay vigencia final...
         */
        String _strVF = envDataArr[0].getFechaVF();
        if (_strVF.isEmpty()) {
            _strVF = null;
        }

        // 1.- Se obtiene el folio
        String respInsertFolio = cargaArchivos.insertFolio(0, 0, envDataArr[0].getId(), strDate, 1);
        String[] respArrInsertFolio = respInsertFolio.split("-.-.-");
        String idFolio = respArrInsertFolio[1];

        if (Integer.parseInt(idFolio) == 0) {
            return new ResponseEntity<>("{\t\n\"idFolio\":\"" + "0" + "\"\n}", HttpStatus.OK);
        }

        // 2.- Se realiza la carga de los archivos al servidor de archivos
        try {
            List<ListaCategoriasPDDTO> listaCategorias = cargaArchivos.obtieneCategorias(2, 1, 0);
            Iterator<String> itr = request.getFileNames();
            String categoria = "";
            String newFileName = "";
            DocData[] auxDocDataArr = null;

            int cont = 0;
            while (itr.hasNext()) {
                String uploadedFile = itr.next();
                MultipartFile file = request.getFile(uploadedFile);
                String filename = file.getOriginalFilename();
                auxDocDataArr = docDataArr;

                String visibleStr = null;
                if (envDataArr[0].getId().equals("I")) {
                    visibleStr = "V";
                } else if (envDataArr[0].getId().equals("C")) {
                    visibleStr = "N";
                } else if (envDataArr[0].getId().equals("B")) {
                    visibleStr = "N";
                }

                int docId = docDataArr[cont].getDocumentId(); // Significa que existe un archivo anterior; Se actualiza el documento viejo a no visible
                Integer _idFile = null;

                // Sustituye documento
                if (docId > 0) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(new Date());
                    dateFormat = new SimpleDateFormat("ddMMyyyy");
                    strDate = dateFormat.format(cal.getTime());
                    // strDate = _strVF;

                    if (envDataArr[0].getId().equals("I")) {
                        // Servicio que actualiza el documento anterior
                        String respLatestIdDoc = cargaArchivos.getLastestDoc(3, docId);
                        String[] respArrLatestDoc = respLatestIdDoc.split("-.-.-");

                        if (!respArrLatestDoc[1].equals("0") || respArrLatestDoc[1].equals("ERROR")) {
                            // Se actualiza la vigencia_fin del documento a sustituir
                            // TODO: Se quita strDate cuando se agreguen los cambios de vigencia final
                            String respUpdateOldDoc = cargaArchivos.updateOldDocument(Integer.parseInt(respArrLatestDoc[1]), strDate, "N");
                        }
                    }

                    newFileName = idFolio + "_" + docId + "_" + _insDate + "." + docDataArr[cont].getFileType();
                    String _resp = cargaArchivos.insertDocto(0, 0, Integer.parseInt(idFolio), newFileName, docId, usuario.getUsuario(), docDataArr[cont].getFileSize(), "S", envDataArr[0].getFecha(), _strVF, visibleStr, 1, 1);
                    String[] _array = _resp.split("-.-.-");
                    auxDocDataArr[cont].setDocumentId(Integer.parseInt(_array[1])); // sustituye por nuevoId
                    _idFile = new Integer(_array[1]);
                } else if (docId == 0) {
                    // Documento nuevo

                    String respInsertAdmonTipoDoc = cargaArchivos.insertAdmonTipoDoc(0, 0, docDataArr[cont].getDocumentName(), 2, Integer.parseInt(docDataArr[cont].getIdCategory()), 1);
                    String[] respArrInsertAdmonTipoDoc = respInsertAdmonTipoDoc.split("-.-.-");
                    newFileName = idFolio + "_" + respArrInsertAdmonTipoDoc[1] + "_" + _insDate + "." + docDataArr[cont].getFileType();

                    if (!respArrInsertAdmonTipoDoc[1].equals("ERROR") || !respArrInsertAdmonTipoDoc[1].equals("0")) {
                        String _resp = cargaArchivos.insertDocto(0, 0, Integer.parseInt(idFolio), newFileName, Integer.parseInt(respArrInsertAdmonTipoDoc[1]), usuario.getUsuario(), docDataArr[cont].getFileSize(), "S", envDataArr[0].getFecha(), _strVF, visibleStr, 1, 1);
                        String[] _array = _resp.split("-.-.-");
                        auxDocDataArr[cont].setDocumentId(Integer.parseInt(_array[1])); // sustituye por nuevoId
                        _idFile = new Integer(_array[1]);
                    } else {
                        String _resp = cargaArchivos.insertDocto(0, 0, Integer.parseInt(idFolio), newFileName, 0, usuario.getUsuario(), docDataArr[cont].getFileSize(), "S", envDataArr[0].getFecha(), _strVF, visibleStr, 1, 1);
                        String[] _array = _resp.split("-.-.-");
                        auxDocDataArr[cont].setDocumentId(Integer.parseInt(_array[1])); // sustituye por nuevoId
                        _idFile = new Integer(_array[1]);
                    }
                }

                if (filename.equalsIgnoreCase(docDataArr[cont].getOgFileName())) {
                    // newFileName = idFolio + "_" + docId + "_" + _insDate + "." + docDataArr[x].getFileType();

                    for (ListaCategoriasPDDTO listaCategoriasPDDTO : listaCategorias) {
                        if (docDataArr[cont].getIdCategory().equalsIgnoreCase(Integer.toString(listaCategoriasPDDTO.getIdTipoDoc()))) {
                            categoria = listaCategoriasPDDTO.getIdTipoDoc() + "/";
                            break;
                        }
                    }
                }

                String _filePath = null;

                if (FRQConstantes.PRODUCCION) {
                    _filePath = PATH_PROD;
                } else {
                    _filePath = PATH_DESA;
                }

                File _file = new File(_filePath + categoria);
                _file.mkdirs();

                if (_file.exists()) {
                    FileOutputStream fileOutput = null;
                    BufferedOutputStream bufferOutput = null;

                    try {
                        logger.info("RUTA DEL ARCHIVO -  " + _filePath + categoria + newFileName);

                        _file = new File(_filePath + categoria, newFileName);
                        fileOutput = new FileOutputStream(_file);
                        bufferOutput = new BufferedOutputStream(fileOutput);
                        bufferOutput.write((byte[]) file.getBytes());

                        // Obtiene MD5 Checksum del archivo
                        MessageDigest md5Digest = MessageDigest.getInstance("MD5");
                        String _checksum = getFileChecksum(md5Digest, _file);
                        String _resp = cargaArchivos.insertUpdateFileChecksum(0, null, _idFile, "ADMON", _checksum, 1);
                        logger.info(_resp);

                        bufferOutput.close();
                    } catch (Exception e) {
                        logger.info("ERROR: " + e.getMessage());
                    } finally {
                        try {
                            if (fileOutput != null) {
                                fileOutput.close();
                            }

                            if (bufferOutput != null) {
                                bufferOutput.close();
                            }
                        } catch (Exception e) {
                            logger.info("ERROR: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                } else {
                    logger.info(_file);
                    logger.info("NO SE PUDO SUBIR EL ARCHIVO EN LA RUTA " + _filePath + categoria);
                }

                cont++;
            }

            // Se realizan los lanzamientos de los documentos
            for (int x = 0; x < auxDocDataArr.length; x++) {
                if (distribDataArr.length > 0) {
                    String respInsertaLanzamientosPorCecos = null;

                    for (int y = 0; y < distribDataArr.length; y++) {
                        if (distribDataArr[y].getTipo().equalsIgnoreCase("1")) {
                            // CECOS - Sucursales
                            respInsertaLanzamientosPorCecos = cargaArchivos.insertaLanzamientosPorCecos(0, distribDataArr[y].getData(), Integer.parseInt(negocio), auxDocDataArr[x].getDocumentId() /**
                             * Integer.parseInt(respArrInsertDocto[1])
                             */
                            );
                        } else if (distribDataArr[y].getTipo().equalsIgnoreCase("2")) {
                            if (distribDataArr[y].getNivel().equalsIgnoreCase("1")) {
                                // Nivel 1 - Pais
                                respInsertaLanzamientosPorCecos = cargaArchivos.insertaLanzamientosPorGeo(1, distribDataArr[y].getData(), Integer.parseInt(negocio), Integer.parseInt(distribDataArr[y].getNivel()), auxDocDataArr[x].getDocumentId() /**
                                 * Integer.parseInt(respArrInsertDocto[1])
                                 */
                                );
                            } else if (distribDataArr[y].getNivel().equalsIgnoreCase("2")) {
                                // Nivel 2 - Estados
                                respInsertaLanzamientosPorCecos = cargaArchivos.insertaLanzamientosPorGeo(1, distribDataArr[y].getData(), Integer.parseInt(negocio), Integer.parseInt(distribDataArr[y].getNivel()), auxDocDataArr[x].getDocumentId() /**
                                 * Integer.parseInt(respArrInsertDocto[1])
                                 */
                                );
                            } else if (distribDataArr[y].getNivel().equalsIgnoreCase("3")) {
                                // Nivel 3 - Municipios
                                respInsertaLanzamientosPorCecos = cargaArchivos.insertaLanzamientosPorGeo(1, distribDataArr[y].getData(), Integer.parseInt(negocio), Integer.parseInt(distribDataArr[y].getNivel()), auxDocDataArr[x].getDocumentId() /**
                                 * Integer.parseInt(respArrInsertDocto[1])
                                 */
                                );
                            } else if (distribDataArr[y].getNivel().equalsIgnoreCase("4")) {
                                // Nivel 4 - Sucursales
                                respInsertaLanzamientosPorCecos = cargaArchivos.insertaLanzamientosPorCecos(0, distribDataArr[y].getData(), Integer.parseInt(negocio), auxDocDataArr[x].getDocumentId() /**
                                 * Integer.parseInt(respArrInsertDocto[1])
                                 */
                                );
                            }
                        } else if (distribDataArr[y].getTipo().equalsIgnoreCase("3")) {
                            // Listas de distribucion
                            respInsertaLanzamientosPorCecos = cargaArchivos.insertaLanzamientosPorListaDD(2, Integer.parseInt(distribDataArr[y].getData()), Integer.parseInt(negocio), auxDocDataArr[x].getDocumentId() /**
                             * Integer.parseInt(respArrInsertDocto[1])
                             */
                            );
                        } else if (distribDataArr[y].getTipo().equalsIgnoreCase("4")) {
                            // CSV - CECOS - Sucursales
                            respInsertaLanzamientosPorCecos = cargaArchivos.insertaLanzamientosPorCecos(0, distribDataArr[y].getData(), Integer.parseInt(negocio), auxDocDataArr[x].getDocumentId() /**
                             * Integer.parseInt(respArrInsertDocto[1])
                             */
                            );
                        }
                    }
                }
            }
        } catch (Exception e) {
            return new ResponseEntity<>("{\t\n\"idFolio\":\"" + "0" + "\"\n}", HttpStatus.OK);
        }

        return new ResponseEntity<>("{\t\n\"idFolio\":\"" + idFolio + "\"\n}", HttpStatus.OK);
    }

    @RequestMapping(value = "saveDocDataInSession.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String saveDocDataInSession(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("data") String data) throws NumberFormatException, Exception {

        String json = "{\n\t\"code\":\"0\",\n\t\"msj\":\"EXITO\"\n}";
        String datos = URLDecoder.decode(data, "UTF-8");

        Gson gson = new Gson();
        DocData[] docDataArr = gson.fromJson(datos, DocData[].class);
        request.getSession().setAttribute("docData", docDataArr);

        return json;
    }

    @RequestMapping(value = "saveDistribDataInSession.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String saveDistribDataInSession(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("data") String data) throws NumberFormatException, Exception {

        String json = "{\n\t\"code\":\"0\",\n\t\"msj\":\"EXITO\"\n}";
        String datos = URLDecoder.decode(data, "UTF-8");

        Gson gson = new Gson();
        DistribData[] distribDataArr = gson.fromJson(datos, DistribData[].class);
        request.getSession().setAttribute("distribData", distribDataArr);

        return json;
    }

    @RequestMapping(value = "saveEnvDataInSession.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String saveEnvDataInSession(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("data") String data) throws NumberFormatException, Exception {

        String json = "{\n\t\"code\":\"0\",\n\t\"msj\":\"EXITO\"\n}";
        String datos = URLDecoder.decode(data, "UTF-8");

        Gson gson = new Gson();
        EnvData[] envDataArr = gson.fromJson(datos, EnvData[].class);
        request.getSession().setAttribute("envData", envDataArr);

        return json;
    }

    /**
     * Metodo que devuelve File Checksum de un archivo (MD5)
     */
    private static String getFileChecksum(MessageDigest digest, File file) throws IOException {

        // Get file input stream for reading the file content
        FileInputStream fis = new FileInputStream(file);

        // Create byte array to read data in chunks
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        // Read file data and update in message digest
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        }

        // close the stream; We don't need it now.
        fis.close();

        // Get the hash's bytes
        byte[] bytes = digest.digest();

        // This bytes[] has bytes in decimal format;
        // Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        // return complete hash
        return sb.toString();
    }
}

class DocData {

    private String ogFileName;
    private String documentName;
    private int documentId;
    private String idCategory;
    private String fileType;
    private long fileSize;
    private int idCategoria;

    public String getOgFileName() {
        return ogFileName;
    }

    public void setOgFileName(String ogFileName) {
        this.ogFileName = ogFileName;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public int getDocumentId() {
        return documentId;
    }

    public void setDocumentId(int documentId) {
        this.documentId = documentId;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }
}

class DistribData {

    private String tipo;
    private String nivel;
    private String data;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}

class EnvData {

    private String id;
    private String desc;
    private String fecha;
    private String hora;
    private String fechaVF;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getFechaVF() {
        return fechaVF;
    }

    public void setFechaVF(String fechaVF) {
        this.fechaVF = fechaVF;
    }
}
