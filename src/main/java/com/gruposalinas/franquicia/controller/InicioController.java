package com.gruposalinas.franquicia.controller;

import com.gruposalinas.franquicia.business.ArchiveroBI;
import com.gruposalinas.franquicia.business.ExpedienteBI;
import com.gruposalinas.franquicia.business.FormArchiverosBI;
import com.gruposalinas.franquicia.business.Gestion7SBI;
import com.gruposalinas.franquicia.business.Usuario_ABI;
import com.gruposalinas.franquicia.domain.ActasActFijoDTO;
import com.gruposalinas.franquicia.domain.ActasMinuciasDTO;
import com.gruposalinas.franquicia.domain.ArchiveroDTO;
import com.gruposalinas.franquicia.domain.BitMantenimientoDTO;
import com.gruposalinas.franquicia.domain.CartaAsignacionAFDTO;
import com.gruposalinas.franquicia.domain.CedulaIdentificacionDTO;
import com.gruposalinas.franquicia.domain.DetalleApoyoDTO;
import com.gruposalinas.franquicia.domain.EstandPuestoDTO;
import com.gruposalinas.franquicia.domain.EvidenciaMinuciasDTO;
import com.gruposalinas.franquicia.domain.EvidenciasFotograficasDTO;
import com.gruposalinas.franquicia.domain.ExpedientesDTO;
import com.gruposalinas.franquicia.domain.FormArchiverosDTO;
import com.gruposalinas.franquicia.domain.FormatoCumplimientoUsuarioDTO;
import com.gruposalinas.franquicia.domain.FotoPlantillaActualDTO;
import com.gruposalinas.franquicia.domain.GuiaDHLDTO;
import com.gruposalinas.franquicia.domain.ProgLimpiezaDTO;
import com.gruposalinas.franquicia.domain.ReporteAreaApoyoDTO;
import com.gruposalinas.franquicia.domain.ReporteCheckDTO;
import com.gruposalinas.franquicia.domain.UsuarioADN;
import com.gruposalinas.franquicia.domain.Usuario_ADTO;
import com.gruposalinas.franquicia.resources.FRQAuthInterceptor;
import com.gruposalinas.franquicia.util.CleanParameter;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class InicioController {

    @Autowired
    Usuario_ABI usuarioabi;

    @Autowired
    ExpedienteBI InsertExped;

    @Autowired
    Usuario_ABI usuario_ABI;

    @Autowired
    Gestion7SBI gestion7SBI;

    @Autowired
    FormArchiverosBI formArchiverosBI;

    @Autowired
    ArchiveroBI archiveroBI;

    private static final Logger logger = LogManager.getLogger(InicioController.class);

    @Autowired
    private CleanParameter UtilString;

    @RequestMapping(value = "mensajes.htm", method = RequestMethod.GET)
    public String mensajes(HttpServletRequest request, Exception ex) {
        logger.info("ENTRO A MENSAJES.HTM");
        return "redirect:tienda/inicio.htm?" + request.getQueryString();
    }

    @RequestMapping(value = {"tienda/inicio.htm", "central/inicio.htm"}, method = RequestMethod.GET)
    public ModelAndView metodoRedirect(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "usuario", required = false, defaultValue = "vacio") String usuario,
            @RequestParam(value = "password", required = false, defaultValue = "") String password,
            @RequestParam(value = "numempleado", required = false, defaultValue = "") String numempleado,
            @RequestParam(value = "ws", required = false, defaultValue = "") String ws,
            @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
            @RequestParam(value = "apellidop", required = false, defaultValue = "") String apellidop,
            @RequestParam(value = "apellidom", required = false, defaultValue = "") String apellidom,
            @RequestParam(value = "sucursal", required = false, defaultValue = "") String sucursal,
            @RequestParam(value = "nomsucursal", required = false, defaultValue = "") String nomsucursal,
            @RequestParam(value = "puesto", required = false, defaultValue = "") String puesto,
            @RequestParam(value = "descpuesto", required = false, defaultValue = "") String descpuesto,
            @RequestParam(value = "SAP", required = false, defaultValue = "") String sap,
            @RequestParam(value = "pais", required = false, defaultValue = "") String pais,
            @RequestParam(value = "canal", required = false, defaultValue = "") String canal,
            @RequestParam(value = "servidor", required = false, defaultValue = "") String servidor,
            @RequestParam(value = "puestobase", required = false, defaultValue = "") String puestobase)
            throws ServletException, IOException {

        String salida = "mensajes";
        ModelAndView mv = new ModelAndView(salida);

        List<Usuario_ADTO> regChecklist = null;

        logger.info("" + request.getRequestURI() + "?" + request.getQueryString());

        if (usuario != "" && request.getRequestURI().contains("tienda/inicio.htm")) {
            // Creo un Usuario ADN para almacenar la informacion
            UsuarioADN userADN = new UsuarioADN();
            userADN.setIdUsuario(usuario);
            //logger.info("Id Usuario--> " + userADN.getIdUsuario());
            userADN.setPassword(UtilString.cleanParameter(password));
            //logger.info("Password--> " + userADN.getPassword());
            userADN.setNumempleado(UtilString.cleanParameter(numempleado));
            //logger.info("Numero Empleado--> " + userADN.getNumempleado());
            userADN.setWs(UtilString.cleanParameter(ws));
            //logger.info("WS--> " + userADN.getWs());
            userADN.setNombre(UtilString.cleanParameter(nombre));
            //logger.info("Nombre--> " + userADN.getNombre());
            userADN.setLlaveMaestra(UtilString.cleanParameter(null));
            //logger.info("Llave Maestra--> " + userADN.getLlaveMaestra());
            userADN.setApellidop(UtilString.cleanParameter(apellidop));
            //logger.info("Apellidop--> " + userADN.getApellidop());
            userADN.setApellidom(UtilString.cleanParameter(apellidom));
            //logger.info("Apellidom--> " + userADN.getApellidom());
            userADN.setSucursal(UtilString.cleanParameter(sucursal));
            //logger.info("Sucursal --> " + userADN.getSucursal());
            userADN.setNomsucursal(UtilString.cleanParameter(nomsucursal));
            //logger.info("NomSucursal--> " + userADN.getNomsucursal());
            userADN.setPuesto(UtilString.cleanParameter(puesto));
            //logger.info("Puesto--> " + userADN.getPuesto());
            userADN.setDescpuesto(UtilString.cleanParameter(descpuesto));
            //logger.info("DescPuesto--> " + userADN.getDescpuesto());
            userADN.setSap(UtilString.cleanParameter(sap));
            //logger.info("SAP--> " + userADN.getSap());
            userADN.setPais(UtilString.cleanParameter(pais));
            //logger.info("Pais--> " + userADN.getPais());
            userADN.setCanal(UtilString.cleanParameter(canal));
            //logger.info("Canal--> " + userADN.getCanal());
            userADN.setServidor(UtilString.cleanParameter(servidor));
            //logger.info("Servidor--> " + userADN.getServidor());
            userADN.setPuestobase(UtilString.cleanParameter(puestobase));
            //logger.info("Puesto Base--> " + userADN.getPuestobase());

            //HttpSession s = sesionADN(request, userADN);
            //s.setAttribute("nombre", nombre + " " + apellidop + " " + apellidom );
            //s.setAttribute("tienda", nomsucursal);
            String ceco = sucursal;
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }

            if (numempleado.contains("T")) {
                numempleado = numempleado.replaceAll("T", "");

            }

            if (numempleado.contains("C")) {
                numempleado = numempleado.replaceAll("C", "");

            }

            logger.info("Usuario limpio " + UtilString.cleanParameter(numempleado));
            //Se agrega busqueda de usuario, para obtener el CECO de eddicho usuario y meterlo en la sesion
            //DatosUsuarioDTO datosUsuarioDTO = usuarioabi.buscaUsuario(UtilString.cleanParameter(numempleado));
            List<Usuario_ADTO> lista = usuarioabi.obtieneUsuario(Integer.parseInt(numempleado));

            if (lista != null && lista.size() != 0) {
                Usuario_ADTO usuAux = new Usuario_ADTO();
                for (Usuario_ADTO x : lista) {
                    usuAux = x;
                }
                ceco = usuAux.getIdCeco();
            } else {
                ceco = ceco;
            }

            request.getSession().setAttribute("numempleado", numempleado);
            request.getSession().setAttribute("sucursal", ceco);
            //request.getSession().setAttribute("nombre", nombre);
            request.getSession().setAttribute("nombre", nombre + " " + apellidop + " " + apellidom);
            request.getSession().setAttribute("nomsucursal", nomsucursal);

            logger.info("PuestoUsuaro" + puestobase);
            logger.info("PuestoUsuaro" + puesto);
            logger.info("PuestoUsuaro" + sap);

            if (puestobase.contains("291") || puesto.contains("292") || puesto.contains("293")) {
                puestobase = "652";
            }
            if (puesto.contains("291") || puesto.contains("292") || puesto.contains("293")) {
                puesto = "652";
            }
            if (sap.contains("291") || sap.contains("292") || sap.contains("293")) {
                sap = "652";
            }
            if (puestobase.contains("653") || puestobase.contains("652") || puesto.contains("653") || puesto.contains("652")
                    || puestobase.contains("291") || puesto.contains("292") || puesto.contains("293")) {

                if (puestobase.contains("652") || puesto.contains("652")) {
                    if (ceco != null && ceco.length() == 6) {

                        if (ceco.substring(0, 2).equals("55")) {
                            request.getSession().setAttribute("puesto", 775);
                        } else {
                            request.getSession().setAttribute("puesto", "652");
                        }

                    } else {
                        request.getSession().setAttribute("puesto", "652");
                    }

                } else {
                    request.getSession().setAttribute("puesto", "652");
                }

            } else {
                request.getSession().setAttribute("puesto", puesto);
            }

            //System.out.println("Puesto: '"+request.getSession().getAttribute("puesto")+"'");
            //String pues=""+request.getSession().getAttribute("puesto");
            /*if(pues.equals("")){
			try {
					regChecklist = usuario_ABI.obtieneUsuario(Integer.parseInt(numempleado));
					for(Usuario_ADTO x:regChecklist){
						request.getSession().setAttribute("puesto", x.getIdPuesto());
					}

			} catch (Exception e) {
					UtilFRQ.printErrorLog(null, e);
					logger.info("Ocurrió algo  al retornar el usuario en login");
			}
             */
 /*System.out.println("USUARIO ADN INICIO SESION" + userADN);
			System.out.println("USUARIO ADN NOMBRE INICIO SESION" + userADN.getNombre());
			System.out.println("USUARIO ADN INICIO SESION" + userADN.getNumempleado());*/
            mv.addObject("objUserADN", sesionADN(request, userADN));
            //mv.addObject("nombre", s.getAttribute("nombre"));
            //mv.addObject("nomsucursal", s.getAttribute("tienda"));

        }
        return mv;
    }

    @RequestMapping(value = {"central/principal.htm"}, method = RequestMethod.GET)
    public ModelAndView postPrincipal(HttpServletRequest request, HttpServletResponse response, Model model) {

        ModelAndView mv = new ModelAndView("principal");

        return mv;
    }

    public HttpSession sesionADN(HttpServletRequest request, UsuarioADN userADN) {
        HttpSession session = request.getSession();
        /*System.out.println("USUARIO ADN INICIO SESION" + userADN);
		System.out.println("USUARIO ADN NOMBRE INICIO SESION" + userADN.getNombre());
		System.out.println("USUARIO ADN INICIO SESION" + userADN.getNumempleado());*/
        session.setAttribute("user", userADN);
        return session;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest request, Exception ex) {
        UtilFRQ.printErrorLog(request, ex);
        ModelAndView mv = new ModelAndView("exception");
        return mv;
    }

    @RequestMapping(value = {"tienda/redirecciona.htm", "central/redirecciona.htm"}, method = RequestMethod.GET)
    public ModelAndView redirecciona(HttpServletRequest request, Exception ex) {
        ModelAndView mv = new ModelAndView("reinicioSesion");
        return mv;
    }

    @RequestMapping(value = "central/expedienteInactivo.htm", method = RequestMethod.GET)
    public ModelAndView controllerInactivos(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "usuario", required = false, defaultValue = "vacio") String usuario,
            @RequestParam(value = "password", required = false, defaultValue = "") String password,
            @RequestParam(value = "numempleado", required = false, defaultValue = "") String numempleado,
            @RequestParam(value = "ws", required = false, defaultValue = "") String ws,
            @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
            @RequestParam(value = "apellidop", required = false, defaultValue = "") String apellidop,
            @RequestParam(value = "apellidom", required = false, defaultValue = "") String apellidom,
            @RequestParam(value = "sucursal", required = false, defaultValue = "") String sucursal,
            @RequestParam(value = "nomsucursal", required = false, defaultValue = "") String nomsucursal,
            @RequestParam(value = "puesto", required = false, defaultValue = "") String puesto,
            @RequestParam(value = "descpuesto", required = false, defaultValue = "") String descpuesto,
            @RequestParam(value = "SAP", required = false, defaultValue = "") String sap,
            @RequestParam(value = "pais", required = false, defaultValue = "") String pais,
            @RequestParam(value = "canal", required = false, defaultValue = "") String canal,
            @RequestParam(value = "servidor", required = false, defaultValue = "") String servidor,
            @RequestParam(value = "puestobase", required = false, defaultValue = "") String puestobase)
            throws ServletException, IOException {

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");//Que tenga id 0 inactivas

        boolean flag = true;

        for (ExpedientesDTO exp : lista) {
            if (exp.getEstatusExpediente() == 1 || exp.getEstatusExpediente() == 2) {
                flag = false;
                break;
            }
        }
        String salida = "";

        if (flag) {
            salida = "expedInactivos";
            //salida = "expedCaptacionCancelados";
        } else {
            salida = "consultaExpedientes";
        }
        ModelAndView mv = new ModelAndView(salida, "command", new ExpedientesDTO());

        if (usuario != "vacio" && request.getRequestURI().contains("tienda/archivoPasivo.htm")) {
            // Creo un Usuario ADN para almacenar la informacion
            UsuarioADN userADN = new UsuarioADN();
            userADN.setIdUsuario(usuario);
            //logger.info("Id Usuario--> " + userADN.getIdUsuario());
            userADN.setPassword(UtilString.cleanParameter(password));
            //logger.info("Password--> " + userADN.getPassword());
            userADN.setNumempleado(UtilString.cleanParameter(numempleado));
            //logger.info("Numero Empleado--> " + userADN.getNumempleado());
            userADN.setWs(UtilString.cleanParameter(ws));
            //logger.info("WS--> " + userADN.getWs());
            userADN.setNombre(UtilString.cleanParameter(nombre));
            //logger.info("Nombre--> " + userADN.getNombre());
            userADN.setLlaveMaestra(UtilString.cleanParameter(null));
            //logger.info("Llave Maestra--> " + userADN.getLlaveMaestra());
            userADN.setApellidop(UtilString.cleanParameter(apellidop));
            //logger.info("Apellidop--> " + userADN.getApellidop());
            userADN.setApellidom(UtilString.cleanParameter(apellidom));
            //logger.info("Apellidom--> " + userADN.getApellidom());
            userADN.setSucursal(UtilString.cleanParameter(sucursal));
            //logger.info("Sucursal --> " + userADN.getSucursal());
            userADN.setNomsucursal(UtilString.cleanParameter(nomsucursal));
            //logger.info("NomSucursal--> " + userADN.getNomsucursal());
            userADN.setPuesto(UtilString.cleanParameter(puesto));
            //logger.info("Puesto--> " + userADN.getPuesto());
            userADN.setDescpuesto(UtilString.cleanParameter(descpuesto));
            //logger.info("DescPuesto--> " + userADN.getDescpuesto());
            userADN.setSap(UtilString.cleanParameter(sap));
            //logger.info("SAP--> " + userADN.getSap());
            userADN.setPais(UtilString.cleanParameter(pais));
            //logger.info("Pais--> " + userADN.getPais());
            userADN.setCanal(UtilString.cleanParameter(canal));
            //logger.info("Canal--> " + userADN.getCanal());
            userADN.setServidor(UtilString.cleanParameter(servidor));
            //logger.info("Servidor--> " + userADN.getServidor());
            userADN.setPuestobase(UtilString.cleanParameter(puestobase));
            //logger.info("Puesto Base--> " + userADN.getPuestobase());

            //HttpSession s = sesionADN(request, userADN);
            //s.setAttribute("nombre", nombre + " " + apellidop + " " + apellidom );
            //s.setAttribute("tienda", nomsucursal);
            request.getSession().setAttribute("numempleado", numempleado);
            request.getSession().setAttribute("sucursal", ceco);
            request.getSession().setAttribute("nombre", nombre);
            request.getSession().setAttribute("nomsucursal", nomsucursal);

            if (puestobase.contains("653") || puestobase.contains("652") || puesto.contains("653") || puesto.contains("652")) {
                request.getSession().setAttribute("puesto", "652");
            } else {
                request.getSession().setAttribute("puesto", puesto);
            }

            /*System.out.println("USUARIO ADN INICIO SESION" + userADN);
			System.out.println("USUARIO ADN NOMBRE INICIO SESION" + userADN.getNombre());
			System.out.println("USUARIO ADN INICIO SESION" + userADN.getNumempleado());*/
            mv.addObject("objUserADN", sesionADN(request, userADN));
            //mv.addObject("nombre", s.getAttribute("nombre"));
            //mv.addObject("nomsucursal", s.getAttribute("tienda"));

        }

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("HH:mm");
        System.out.println("Hora: " + hourFormat.format(date));
        //Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: " + dateFormat.format(date));
        //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        System.out.println("Hora y fecha: " + hourdateFormat.format(date));

        mv.addObject("hora", "" + hourFormat.format(date));
        mv.addObject("fecha", "" + hourdateFormat.format(date));
        if (flag) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("list", lista);
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/expedienteInactivo.htm", method = RequestMethod.POST)
    public ModelAndView postcontrollerInactivos(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "ciudad", required = true, defaultValue = "") String ciudad,
            @RequestParam(value = "nomRegional", required = true, defaultValue = "") String nomRegional,
            @RequestParam(value = "numRegional", required = true, defaultValue = "") String numRegional,
            @RequestParam(value = "descripcion", required = true, defaultValue = "") String descripcion,
            @RequestParam(value = "idSucursal", required = true, defaultValue = "") String idSucursal,
            @RequestParam(value = "nomSucursal", required = true, defaultValue = "") String nomSucursal,
            @RequestParam(value = "fecha", required = true, defaultValue = "") String fecha,
            @RequestParam(value = "nomGerente", required = true, defaultValue = "") String nomGerente,
            @RequestParam(value = "numGerente", required = true, defaultValue = "") String numGerente,
            @RequestParam(value = "usuario", required = false, defaultValue = "") String usuario,
            @RequestParam(value = "usuario2", required = false, defaultValue = "") String usuario2,
            @RequestParam(value = "usuario3", required = false, defaultValue = "") String usuario3,
            @RequestParam(value = "usr", required = false, defaultValue = "") String usr,
            @RequestParam(value = "usr2", required = false, defaultValue = "") String usr2,
            @RequestParam(value = "usr3", required = false, defaultValue = "") String usr3,
            @RequestParam(value = "token1", required = false, defaultValue = "") String token1,
            @RequestParam(value = "token2", required = false, defaultValue = "") String token2,
            @RequestParam(value = "token3", required = false, defaultValue = "") String token3,
            @RequestParam(value = "tok", required = false, defaultValue = "") String tok,
            @RequestParam(value = "numExpediente", required = false, defaultValue = "") String numExpediente,
            @RequestParam(value = "impreso", required = false, defaultValue = "") String impreso
    )
            throws ServletException, IOException {
        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
        String salida = "expedInactivos";
        ModelAndView mv = new ModelAndView(salida, "command", new ExpedientesDTO());

        logger.info("Nombre--> " + nomGerente);
        logger.info("usuario2--> '" + usuario2 + "'");
        logger.info("usuario3--> '" + usuario3 + "'");
        logger.info("usr--> '" + usr + "'");
        logger.info("usr2--> '" + usr2 + "'");
        logger.info("usr3--> '" + usr3 + "'");
        logger.info("descripcion--> " + descripcion);
        logger.info("token1--> " + token1);
        logger.info("token2--> " + token2);
        logger.info("token3--> " + token3);
        int numExpediente1 = 0;

        if (numGerente.contains("T")) {
            numGerente = numGerente.replaceAll("T", "");

        }

        if (!usr.equals("") && !usr2.equals("") && !token1.equals("") && !token2.equals("")) {
            if (tok.equals("")) {
                try {
                    //logger.info("Paso");
                    List<Usuario_ADTO> lista = usuarioabi.obtieneUsuario(Integer.parseInt(usr2));

                    //logger.info("Paso");
                    //List<Usuario_ADTO> lista2 = usuarioabi.obtieneUsuario(Integer.parseInt(usr3));
                    //logger.info("Paso");
                    String nomusuario2 = "";
                    //String nomusuario3="";

                    for (Usuario_ADTO x : lista) {
                        nomusuario2 = x.getNombre();
                        mv.addObject("nomusuario2", x.getNombre());
                        break;
                    }
                    /*
				for(Usuario_ADTO x2:lista2){
					nomusuario3=x2.getNombre();
					mv.addObject("nomusuario3", x2.getNombre());
					break;
				}
                     */
                    //logger.info("Paso");

                    ExpedientesDTO exped = new ExpedientesDTO();

                    //logger.info("Paso");
                    exped.setIdGerente(Integer.parseInt(numGerente));
                    exped.setIdCeco(idSucursal);
                    exped.setFecha(fecha);
                    exped.setEstatusExpediente(2);
                    exped.setDescripcion(descripcion);
                    exped.setCiudad(ciudad);
                    //logger.info("Paso");
                    numExpediente1 = InsertExped.insertaExpediente(exped);
                    logger.info("Insertando expediente.... " + numExpediente1);

                    exped.setIdExpediente(numExpediente1);

                    exped.setIdentificador(Integer.parseInt(numRegional));
                    exped.setNombreResp(nomRegional);
                    exped.setEstatusResp(1);
                    //logger.info("Paso");
                    logger.info("Insertando Regional.... " + InsertExped.insertaResponsable(exped));

                    exped.setIdentificador(Integer.parseInt(usr2));
                    exped.setNombreResp(nomusuario2);
                    exped.setEstatusResp(3);
                    //logger.info("Paso");
                    logger.info("Insertando respondable1.... " + InsertExped.insertaResponsable(exped));

                    /*
				exped.setIdentificador(Integer.parseInt(usr3));
				exped.setNombreResp(nomusuario3);
				exped.setEstatusResp(3);

				logger.info("Insertando respondable2.... "+ InsertExped.insertaResponsable(exped));
                     */
                    exped.setIdentificador(Integer.parseInt(numGerente));
                    exped.setNombreResp(nomGerente);
                    exped.setEstatusResp(4);

                    logger.info("Insertando Gerente.... " + InsertExped.insertaResponsable(exped));

                    mv.addObject("ciudad", ciudad);
                    mv.addObject("nomRegional", nomRegional);
                    mv.addObject("numRegional", numRegional);
                    mv.addObject("descripcion", descripcion);
                    mv.addObject("idSucursal", idSucursal);
                    mv.addObject("nomSucursal", nomSucursal);
                    mv.addObject("fecha", fecha);
                    mv.addObject("nomGerente", nomGerente);
                    mv.addObject("numGerente", numGerente);
                    mv.addObject("usuario", usuario);
                    mv.addObject("usuario2", usuario2);

                    mv.addObject("numExpediente", numExpediente1);

                    mv.addObject("usuario3", usuario3);

                    mv.addObject("paso", "no2");
                    //logger.info("Paso");
                } catch (Exception e) {
                    logger.info("Algo paso: " + e);
                    if (numExpediente1 != 0) {
                        InsertExped.eliminaResponsables(numExpediente1);
                        InsertExped.eliminaExpediente(numExpediente1);
                    }

                    mv.addObject("paso", "error");
                }
            } else {
                try {
                    //logger.info("Paso");
                    List<Usuario_ADTO> lista = usuarioabi.obtieneUsuario(Integer.parseInt(usr2));

                    //logger.info("Paso");
                    //List<Usuario_ADTO> lista2 = usuarioabi.obtieneUsuario(Integer.parseInt(usr3));
                    //logger.info("Paso");
                    String nomusuario2 = "";
                    //String nomusuario3="";

                    for (Usuario_ADTO x : lista) {
                        nomusuario2 = x.getNombre();
                        mv.addObject("nomusuario2", x.getNombre());
                        break;
                    }
                    /*
						for(Usuario_ADTO x2:lista2){
							nomusuario3=x2.getNombre();
							mv.addObject("nomusuario3", x2.getNombre());
							break;
						}
                     */
                    //logger.info("Paso");

                    ExpedientesDTO exped = new ExpedientesDTO();

                    logger.info("Insertando audiror.... " + InsertExped.actualizaEstatus(2, Integer.parseInt(numExpediente)));

                    //logger.info("Paso");
                    exped.setIdExpediente(Integer.parseInt(numExpediente));

                    exped.setIdGerente(Integer.parseInt(numGerente));
                    exped.setIdCeco(idSucursal);
                    exped.setFecha(fecha);
                    exped.setEstatusExpediente(2);
                    exped.setDescripcion(descripcion);
                    exped.setCiudad(ciudad);

                    exped.setIdentificador(Integer.parseInt(usr2));
                    exped.setNombreResp(nomusuario2);
                    exped.setEstatusResp(3);
                    //logger.info("Paso");
                    logger.info("Insertando respondable1.... " + InsertExped.insertaResponsable(exped));

                    /*
						exped.setIdentificador(Integer.parseInt(usr3));
						exped.setNombreResp(nomusuario3);
						exped.setEstatusResp(3);

						logger.info("Insertando respondable2.... "+ InsertExped.insertaResponsable(exped));
                     */
                    mv.addObject("ciudad", ciudad);
                    mv.addObject("nomRegional", nomRegional);
                    mv.addObject("numRegional", numRegional);
                    mv.addObject("descripcion", descripcion);
                    mv.addObject("idSucursal", idSucursal);
                    mv.addObject("nomSucursal", nomSucursal);
                    mv.addObject("fecha", fecha);
                    mv.addObject("nomGerente", nomGerente);
                    mv.addObject("numGerente", numGerente);
                    mv.addObject("usuario", usuario);
                    mv.addObject("usuario2", usuario2);

                    mv.addObject("numExpediente", numExpediente);

                    mv.addObject("usuario3", usuario3);

                    mv.addObject("paso", "no2");
                    //logger.info("Paso");
                } catch (Exception e) {
                    logger.info("Algo paso: " + e);
                    if (Integer.parseInt(numExpediente) != 0) {
                        InsertExped.eliminaResponsables(Integer.parseInt(numExpediente));
                        InsertExped.eliminaExpediente(Integer.parseInt(numExpediente));
                    }

                    mv.addObject("paso", "error");
                }
            }

        } else {
            try {
                if (impreso.equals("")) {
                    //logger.info("Paso2");

                    ExpedientesDTO exped = new ExpedientesDTO();

                    //logger.info("Paso2");
                    exped.setIdGerente(Integer.parseInt(numGerente));
                    exped.setIdCeco(idSucursal);
                    exped.setFecha(fecha);
                    exped.setEstatusExpediente(1);
                    exped.setDescripcion(descripcion);
                    exped.setCiudad(ciudad);
                    numExpediente1 = InsertExped.insertaExpediente(exped);
                    logger.info("Insertando expediente.... " + numExpediente1);

                    exped.setIdExpediente(numExpediente1);

                    exped.setIdentificador(Integer.parseInt(numRegional));
                    exped.setNombreResp(nomRegional);
                    exped.setEstatusResp(1);
                    logger.info("Insertando Regional.... " + InsertExped.insertaResponsable(exped));

                    exped.setIdentificador(Integer.parseInt(numGerente));
                    exped.setNombreResp(nomGerente);
                    exped.setEstatusResp(4);

                    logger.info("Insertando Gerente.... " + InsertExped.insertaResponsable(exped));

                    mv.addObject("ciudad", ciudad);
                    mv.addObject("nomRegional", nomRegional);
                    mv.addObject("numRegional", numRegional);
                    mv.addObject("descripcion", descripcion);
                    mv.addObject("idSucursal", idSucursal);
                    mv.addObject("nomSucursal", nomSucursal);
                    mv.addObject("fecha", fecha);
                    mv.addObject("nomGerente", nomGerente);
                    mv.addObject("numGerente", numGerente);
                    mv.addObject("usuario", usuario);
                    mv.addObject("usuario2", usuario2);

                    mv.addObject("numExpediente", numExpediente1);

                    mv.addObject("usuario3", usuario3);

                    mv.addObject("tok", "OK");

                    mv.addObject("paso", "no3");
                    logger.info("PAso2");
                } else {
                    logger.info("Insertando audiror.... " + InsertExped.actualizaEstatus(3, Integer.parseInt(numExpediente)));
                    mv.addObject("paso", "no4");
                }
            } catch (Exception e) {
                logger.info("Algo paso: " + e);
                if (numExpediente1 != 0) {
                    InsertExped.eliminaResponsables(numExpediente1);
                    InsertExped.eliminaExpediente(numExpediente1);
                }

                mv.addObject("paso", "error");
            }
        }

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("HH:mm");
        System.out.println("Hora: " + hourFormat.format(date));
        //Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: " + dateFormat.format(date));
        //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        System.out.println("Hora y fecha: " + hourdateFormat.format(date));

        mv.addObject("hora", "" + hourFormat.format(date));
        mv.addObject("fecha", "" + hourdateFormat.format(date));

        return mv;
    }

    @RequestMapping(value = "central/consultaExpediente.htm", method = RequestMethod.GET)
    public ModelAndView getconsultaInactivos(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "consultaExpedientes";
        ModelAndView mv = new ModelAndView(salida, "command", new ExpedientesDTO());

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/recuperaExpediente.htm", method = RequestMethod.POST)
    public ModelAndView postconsultaInactivos(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idExpediente", required = true, defaultValue = "") String idExpediente)
            throws ServletException, IOException {

        String ciudad = "";
        String nomRegional = "";
        String numRegional = "";
        String descripcion = "";
        String idSucursal = "";
        String nomSucursal = "";
        String fecha = "";
        String nomGerente = "";
        String numGerente = "";
        String usuario = "";
        String usuario2 = "";
        String usuario3 = "";
        String nomusuario2 = "";
        String nomusuario3 = "";
        String usr = "";
        String usr2 = "";
        String usr3 = "";
        String token1 = "";
        String token2 = "";
        String token3 = "";
        String tok = "";
        String numExpediente = "";
        String impreso = "";
        int estatus = 0;

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
        nomGerente = "" + request.getSession().getAttribute("nombre");
        numGerente = "" + request.getSession().getAttribute("numempleado");

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(idExpediente, null, null, null, null, null, "1");

        logger.info("Numero de sucursal " + request.getSession().getAttribute("sucursal"));

        String salida = "";
        boolean imp = false;

        for (ExpedientesDTO exp : lista) {
            numExpediente = "" + exp.getIdExpediente();
            ciudad = exp.getCiudad();
            descripcion = exp.getDescripcion();
            idSucursal = exp.getIdCeco();
            fecha = exp.getFecha();
            numGerente = "" + exp.getIdGerente();
            estatus = exp.getEstatusExpediente();
        }

        List<ExpedientesDTO> listaU = InsertExped.buscaResponsable(idExpediente, null, null);

        for (ExpedientesDTO exp : listaU) {
            if (exp.getEstatusResp() == 1) {
                nomRegional = exp.getNombreResp();
                numRegional = "" + exp.getIdentificador();
            }
            if (exp.getEstatusResp() == 3) {
                nomusuario2 = exp.getNombreResp();
                usuario2 = "" + exp.getIdentificador();
            }
            if (exp.getEstatusResp() == 4) {
                nomGerente = exp.getNombreResp();
                numGerente = "" + exp.getIdentificador();
            }
        }

        if (estatus == 3) {
            salida = "consultaExpedientes";
        } else if (estatus == 2) {
            salida = "expedInactivos";
            imp = true;
        } else {
            salida = "expedInactivos";
        }
        //salida = "consultaExpedientes";
        ModelAndView mv = new ModelAndView(salida, "command", new ExpedientesDTO());

        String fechaSplit[] = fecha.split(" ");
        String hora = fechaSplit[1];
        String dia = fechaSplit[0].split("/")[0];
        String mes = fechaSplit[0].split("/")[1];
        String anio = fechaSplit[0].split("/")[2];

        switch (mes) {
            case "01":
                mes = "Enero";
                break;
            case "02":
                mes = "Febrero";
                break;
            case "03":
                mes = "Marzo";
                break;
            case "04":
                mes = "Abril";
                break;
            case "05":
                mes = "Mayo";
                break;
            case "06":
                mes = "Junio";
                break;
            case "07":
                mes = "Julio";
                break;
            case "08":
                mes = "Agosto";
                break;
            case "09":
                mes = "Septiembre";
                break;
            case "10":
                mes = "Octubre";
                break;
            case "11":
                mes = "Noviembre";
                break;
            case "12":
                mes = "Diciembre";
                break;

        }

        mv.addObject("ciudad", ciudad);
        mv.addObject("nomRegional", nomRegional);
        mv.addObject("numRegional", numRegional);
        mv.addObject("descripcion", descripcion);
        mv.addObject("idSucursal", idSucursal);
        mv.addObject("nomSucursal", nomSucursal);
        mv.addObject("fecha", fecha);
        mv.addObject("nomGerente", nomGerente);
        mv.addObject("numGerente", numGerente);
        mv.addObject("usuario", usuario);
        mv.addObject("usuario2", usuario2);
        mv.addObject("nomusuario2", nomusuario2);
        mv.addObject("nomusuario3", nomusuario3);

        mv.addObject("numExpediente", numExpediente);

        mv.addObject("usuario3", usuario3);

        if (estatus == 3) {
            mv.addObject("paso", "impreso");
            mv.addObject("hora", hora);
            mv.addObject("mes", mes);
            mv.addObject("dia", dia);
            mv.addObject("anio", anio);
            return mv;
        } else if (estatus == 2) {
            mv.addObject("paso", "no2");
            nomGerente = "" + request.getSession().getAttribute("nombre");
            numGerente = "" + request.getSession().getAttribute("numempleado");
            Date date = new Date();
            //Caso 1: obtener la hora y salida por pantalla con formato:
            DateFormat hourFormat = new SimpleDateFormat("HH:mm");
            System.out.println("Hora: " + hourFormat.format(date));
            //Caso 2: obtener la fecha y salida por pantalla con formato:
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println("Fecha: " + dateFormat.format(date));
            //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
            DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            System.out.println("Hora y fecha: " + hourdateFormat.format(date));

            mv.addObject("hora", "" + hourFormat.format(date));
            mv.addObject("fecha", "" + hourdateFormat.format(date));
            return mv;
        } else {
            mv.addObject("paso", "no3");
            mv.addObject("tok", "OK");
            nomGerente = "" + request.getSession().getAttribute("nombre");
            numGerente = "" + request.getSession().getAttribute("numempleado");
            Date date = new Date();
            //Caso 1: obtener la hora y salida por pantalla con formato:
            DateFormat hourFormat = new SimpleDateFormat("HH:mm");
            System.out.println("Hora: " + hourFormat.format(date));
            //Caso 2: obtener la fecha y salida por pantalla con formato:
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println("Fecha: " + dateFormat.format(date));
            //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
            DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            System.out.println("Hora y fecha: " + hourdateFormat.format(date));

            mv.addObject("hora", "" + hourFormat.format(date));
            mv.addObject("fecha", "" + hourdateFormat.format(date));
            return mv;
        }

    }

    @RequestMapping(value = "central/cierraSesion.htm", method = RequestMethod.GET)
    public String cierraSesionCentral(HttpServletRequest request, HttpServletResponse response) throws Exception {
        FRQAuthInterceptor o = new FRQAuthInterceptor();
        o.emptyMap();

        request.removeAttribute("user");
        request.removeAttribute("nombre");
        request.removeAttribute("nomSucursal");
        request.removeAttribute("urlServer");
        request.removeAttribute("perfilAdmin");
        request.removeAttribute("perfilReportes");
        request.removeAttribute("servidorApache");
        request.removeAttribute("startTime");
        request.getSession().invalidate();

        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }

        //request.getSession().setAttribute("user", null);
        //request.getSession().invalidate();
        return "redirect:http://portal.socio.gs/Logout_azteca/cerrar_sesion.html";
        //Para ocupara este cierre de sesion se tiene que crear un DNS para franquicia y pueda cerrar toda la sesion
        //return "redirect:http://portal.socio.gs/logoutportal";
    }

    @SuppressWarnings("unused")
    private class ExpedientePasivoDTO {

        private String nomAuditor;

        public String getNomAuditor() {
            return nomAuditor;
        }

        public void setNomAuditor(String nomAuditor) {
            this.nomAuditor = nomAuditor;
        }
    }

    /**
     * ****************************** ModelAndView
     * ******************************************************************
     */
    //http://10.51.218.72:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=480100&nomsucursal=DAZIGUALAPA&puesto=652
    //http://10.51.218.72:8080/franquicia/central/inicio7s.htm
    @RequestMapping(value = "central/inicio7s.htm", method = RequestMethod.GET)
    public ModelAndView getinicio7s(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "inicio7s";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }		//List<ExpedientesDTO> lista=InsertExped.buscaExpediente(null, null, ceco, null, null, null);

        return mv;
    }

    @RequestMapping(value = "central/evidencias-fotograficas.htm", method = RequestMethod.GET)
    public ModelAndView getevidenciasfotograficas(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "evidencias-fotograficas";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        try {

            String json = gestion7SBI.obtieneEvidenciasFotograficas1(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);

                List<EvidenciasFotograficasDTO> lista11 = null;
                JSONArray aux = new JSONArray();
                if (rec.has("DETALLE") && !rec.isNull("DETALLE")) {
                    aux = rec.getJSONArray("DETALLE");
                    lista11 = new ArrayList<EvidenciasFotograficasDTO>();
                    for (int i = 0; i < aux.length(); i++) {
                        JSONObject jObject = aux.getJSONObject(i);
                        EvidenciasFotograficasDTO AMAUX = new EvidenciasFotograficasDTO();
                        //AMAUX.setRutafotoantes(jObject.getString("RUTA_FOTO_ANTES"));
                        AMAUX.setRutafotoantes(jObject.getString("RUTA_FOTO_ANTES").substring(1, jObject.getString("RUTA_FOTO_ANTES").length()));
                        // AMAUX.setFechadespues(jObject.getString("FECHA_DESPUES"));
                        AMAUX.setLugarantes(jObject.getString("LUGAR_ANTES"));
                        AMAUX.setLugardespues(jObject.getString("LUGAR_DESPUES"));
                        //AMAUX.setRutafotodespues(jObject.getString("RUTA_FOTO_DESPUES"));
                        AMAUX.setRutafotodespues(jObject.getString("RUTA_FOTO_DESPUES").substring(1, jObject.getString("RUTA_FOTO_DESPUES").length()));
                        AMAUX.setPeriodo(jObject.getString("PERIODO"));
                        // AMAUX.setFechaantes(jObject.getString("FECHA_ANTES"));
                        lista11.add(AMAUX);

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        String dateInString = jObject.getString("FECHA_ANTES").trim();
                        String dateIntString = jObject.getString("FECHA_DESPUES").trim();
                        // String dateIndString = jObject.getString("HORA_DESPUES").trim();
                        SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                        SimpleDateFormat formatoEsMX2 = new SimpleDateFormat("HH:mm a", new Locale("ES", "MX"));
                        Date date1;
                        Date horad;
                        Date horaa;
                        Date date2;
                        date1 = formatter.parse(dateInString);
                        horaa = formatter.parse(dateInString);

                        date2 = formatter.parse(dateIntString);
                        horad = formatter.parse(dateIntString);

                        AMAUX.setFechaantes(formatoEsMX1.format(date1));
                        AMAUX.setHoraantes(formatoEsMX2.format(horaa));

                        AMAUX.setFechadespues(formatoEsMX1.format(date2));
                        AMAUX.setHoradespues(formatoEsMX2.format(horad));

                    }
                }
                if (aux.length() > 0) {
                    mv.addObject("datos", 1);
                } else {
                    mv.addObject("datos", 0);
                }
                //mv.addObject("datos", 0);
                mv.addObject("lista12", lista11);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/acta-de-hecho-minucias.htm", method = RequestMethod.GET)
    public ModelAndView getactadeechominucias(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "acta-de-hecho-minucias";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        try {
            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }

            String json = gestion7SBI.obtieneActasMinucias(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);

                List<ActasMinuciasDTO> lista = new ArrayList<ActasMinuciasDTO>();
                JSONArray aux = new JSONArray();
                if (rec.has("ACTAS_MINUCIAS") && !rec.isNull("ACTAS_MINUCIAS")) {
                    aux = rec.getJSONArray("ACTAS_MINUCIAS");

                    for (int i = 0; i < aux.length(); i++) {
                        JSONObject jObject = aux.getJSONObject(i);
                        ActasMinuciasDTO AMAUX = new ActasMinuciasDTO();
                        AMAUX.setIdActa(jObject.getInt("ID_ACTA"));

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateInString = jObject.getString("FECHA").trim();
                        SimpleDateFormat formatoEsMX = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                        Date date;
                        date = formatter.parse(dateInString);

                        AMAUX.setFecha(formatoEsMX.format(date));
                        AMAUX.setDireccion(jObject.getString("DIRECCION"));
                        lista.add(AMAUX);
                    }
                }
                if (aux.length() > 0) {
                    mv.addObject("datos", 1);
                } else {
                    mv.addObject("datos", 0);
                }
                //mv.addObject("datos", 0);
                mv.addObject("lista", lista);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return mv;
    }

    @RequestMapping(value = "central/acta-entrega-expediente.htm", method = RequestMethod.GET)
    public ModelAndView getactaentregaexpediente(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "acta-entrega-expediente";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        try {
            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }

            List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

            if (lista.size() > 0) {
                mv.addObject("datos", 1);
            } else {
                mv.addObject("datos", 0);
            }
            //mv.addObject("datos", 0);
            mv.addObject("lista", lista);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/acta-de-hechos-de-depuracion-de-activos-fijos.htm", method = RequestMethod.GET)
    public ModelAndView getactadehechosdedepuraciondeactivosfijos(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "acta-de-hechos-de-depuracion-de-activos-fijos";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        try {
            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }
            String json = gestion7SBI.obtieneActivoFijo(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                List<ActasActFijoDTO> lista = new ArrayList<ActasActFijoDTO>();
                JSONArray aux = new JSONArray();
                if (rec.has("ACTAS_ACTIVOS") && !rec.isNull("ACTAS_ACTIVOS")) {
                    aux = rec.getJSONArray("ACTAS_ACTIVOS");

                    for (int i = 0; i < aux.length(); i++) {
                        JSONObject jObject = aux.getJSONObject(i);
                        ActasActFijoDTO AAF = new ActasActFijoDTO();
                        AAF.setIdActa(Integer.parseInt(jObject.getString("ID_ACTA")));
                        AAF.setUbicacion(jObject.getString("UBICACION"));
                        AAF.setInvolucrados(jObject.getString("INVOLUCRADOS"));
                        AAF.setMonto(Double.parseDouble(jObject.getString("MONTO")));
                        AAF.setRazonsoc(jObject.getString("RAZON_SOCIAL"));
                        AAF.setDomicilio(jObject.getString("DOMICILIO"));
                        AAF.setRfc(jObject.getString("RFC"));
                        AAF.setResponsable(jObject.getString("RESPONSABLE"));
                        AAF.setTestigo(jObject.getString("TESTIGO"));

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateInString = jObject.getString("FECHA").trim();

                        if (dateInString != null && dateInString != "null") {
                            SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                            Date date1;
                            date1 = formatter.parse(dateInString);
                            AAF.setFecha(formatoEsMX1.format(date1));
                        }
                        lista.add(AAF);

                    }
                }

                if (aux.length() > 0) {
                    mv.addObject("datos", 1);
                } else {
                    mv.addObject("datos", 0);
                }
                //mv.addObject("datos", 0);

                mv.addObject("lista", lista);
            }

        } catch (JSONException json) {

            json.printStackTrace();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return mv;
    }

    @RequestMapping(value = "central/carta-de-asignacion-de-activo-fijo.htm", method = RequestMethod.GET)
    public ModelAndView getcartadeasignaciondeactivofijo(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {
        String salida = "carta-de-asignacion-de-activo-fijo";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        try {
            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }
            String json = gestion7SBI.obtieneCartaActFijo(ceco);

            if (json != null) {
                JSONObject rec = new JSONObject(json);
                JSONObject anios = rec.getJSONObject("ANIOS");

                Calendar cal = Calendar.getInstance();
                int anioActual = cal.get(Calendar.YEAR);
                int anioPasado = anioActual - 1;
                CartaAsignacionAFDTO carta = new CartaAsignacionAFDTO();
                carta.setAnioActual(anioActual);

                JSONObject year = anios.getJSONObject(String.valueOf(anioActual));
                String primerTrim = "1ER_TRIM";
                String segundoTrim = "2DO_TRIM";
                String tercerTrim = "3ER_TRIM";
                String cuartoTrim = "4TO_TRIM";
                JSONArray aux = year.getJSONArray(primerTrim);
                JSONArray aux2 = year.getJSONArray(segundoTrim);

                JSONArray aux3 = year.getJSONArray(tercerTrim);
                JSONArray aux4 = year.getJSONArray(cuartoTrim);
                List<CartaAsignacionAFDTO> lista = new ArrayList<CartaAsignacionAFDTO>();

                for (int i = 0; i < aux.length(); i++) {
                    JSONObject jObject = aux.getJSONObject(i);
                    CartaAsignacionAFDTO CAF = new CartaAsignacionAFDTO();
                    CAF.setIdCarta(Integer.parseInt(jObject.getString("ID_CARTA")));
                    CAF.setNombreArc(jObject.getString("NOM_ARCHIVO"));
                    CAF.setRuta(
                            jObject.getString("RUTA_ARCHIVO").substring(1, jObject.getString("RUTA_ARCHIVO").length()));
                    lista.add(CAF);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateInString = jObject.getString("FECHA").trim();
                    SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                    Date date1;
                    date1 = formatter.parse(dateInString);
                    CAF.setFecha(formatoEsMX1.format(date1));
                    mv.addObject("IMG",
                            jObject.getString("RUTA_ARCHIVO").substring(1, jObject.getString("RUTA_ARCHIVO").length()));
                }
                mv.addObject("lista", lista);

                List<CartaAsignacionAFDTO> lista2 = new ArrayList<CartaAsignacionAFDTO>();

                for (int i = 0; i < aux2.length(); i++) {
                    JSONObject jObject = aux2.getJSONObject(i);
                    CartaAsignacionAFDTO CAF = new CartaAsignacionAFDTO();
                    CAF.setIdCarta(Integer.parseInt(jObject.getString("ID_CARTA")));
                    CAF.setNombreArc(jObject.getString("NOM_ARCHIVO"));
                    CAF.setRuta(
                            jObject.getString("RUTA_ARCHIVO").substring(1, jObject.getString("RUTA_ARCHIVO").length()));
                    lista2.add(CAF);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateInString = jObject.getString("FECHA").trim();
                    SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                    Date date1;
                    date1 = formatter.parse(dateInString);
                    CAF.setFecha(formatoEsMX1.format(date1));
                    mv.addObject("IMG2",
                            jObject.getString("RUTA_ARCHIVO").substring(1, jObject.getString("RUTA_ARCHIVO").length()));
                }
                mv.addObject("lista2", lista2);

                List<CartaAsignacionAFDTO> lista3 = new ArrayList<CartaAsignacionAFDTO>();

                for (int i = 0; i < aux3.length(); i++) {
                    JSONObject jObject = aux3.getJSONObject(i);
                    CartaAsignacionAFDTO CAF = new CartaAsignacionAFDTO();
                    CAF.setIdCarta(Integer.parseInt(jObject.getString("ID_CARTA")));
                    CAF.setNombreArc(jObject.getString("NOM_ARCHIVO"));
                    CAF.setRuta(jObject.getString("RUTA_ARCHIVO").substring(1, jObject.getString("RUTA_ARCHIVO").length()));
                    lista3.add(CAF);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateInString = jObject.getString("FECHA").trim();
                    SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                    Date date1;
                    date1 = formatter.parse(dateInString);
                    CAF.setFecha(formatoEsMX1.format(date1));
                }
                mv.addObject("lista3", lista3);

                List<CartaAsignacionAFDTO> lista4 = new ArrayList<CartaAsignacionAFDTO>();

                for (int i = 0; i < aux4.length(); i++) {
                    JSONObject jObject = aux4.getJSONObject(i);
                    CartaAsignacionAFDTO CAF = new CartaAsignacionAFDTO();
                    CAF.setIdCarta(Integer.parseInt(jObject.getString("ID_CARTA")));
                    CAF.setNombreArc(jObject.getString("NOM_ARCHIVO"));
                    CAF.setRuta(jObject.getString("RUTA_ARCHIVO").substring(1, jObject.getString("RUTA_ARCHIVO").length()));
                    lista4.add(CAF);
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateInString = jObject.getString("FECHA").trim();
                    SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                    Date date1;
                    date1 = formatter.parse(dateInString);
                    CAF.setFecha(formatoEsMX1.format(date1));
                }
                mv.addObject("lista4", lista4);

                mv.addObject("TRIM", "1º Trimestre");
                mv.addObject("TRIM2", "2º Trimestre");
                mv.addObject("TRIM3", "3º Trimestre");
                mv.addObject("TRIM4", "4º Trimestre");
                mv.addObject("ANIO", anioActual);
            }
        } catch (JSONException e) {
            logger.info("Paso algo al obtener el json");
        } catch (ParseException e) {
            logger.info("Algo paso en el metodo de carta de asignacion de activo fijo");
        }
        return mv;
    }

    @RequestMapping(value = "central/estandarizacion-por-puesto.htm", method = RequestMethod.GET)
    public ModelAndView getestandarizacionporpuesto(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "estandarizacion-por-puesto";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        try {
            String json = gestion7SBI.estandarizacionPuesto(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);

                List<EstandPuestoDTO> listaG = new ArrayList<EstandPuestoDTO>();
                List<EstandPuestoDTO> listaC = new ArrayList<EstandPuestoDTO>();
                List<EstandPuestoDTO> listaF = new ArrayList<EstandPuestoDTO>();
                JSONArray auxG = new JSONArray();
                JSONArray auxC = new JSONArray();
                JSONArray auxF = new JSONArray();
                if ((rec.has("GERENTE") && !rec.isNull("GERENTE")) || (rec.has("CAJA") && !rec.isNull("CAJA")) || (rec.has("FINANCIEROS") && !rec.isNull("FINANCIEROS"))) {
                    auxG = rec.getJSONArray("GERENTE");
                    auxC = rec.getJSONArray("CAJA");
                    auxF = rec.getJSONArray("FINANCIEROS");

                    for (int i = 0; i < auxG.length(); i++) {
                        JSONObject jObject = auxG.getJSONObject(i);
                        EstandPuestoDTO AMAUX = new EstandPuestoDTO();

                        AMAUX.setArea(jObject.getInt("AREA"));
                        AMAUX.setCajon3(jObject.getString("MUEBLE1_CAJA3"));
                        AMAUX.setCajon2(jObject.getString("MUEBLE1_CAJA2"));
                        AMAUX.setCajon1(jObject.getString("MUEBLE1_CAJA1"));

                        AMAUX.setTipoMueble(jObject.getInt("TIPO_MUEBLE"));
                        AMAUX.setFecha(jObject.getString("FECHA"));
                        AMAUX.setCeco(jObject.getString("ID_CECO"));

                        listaG.add(AMAUX);
                    }
                    for (int i = 0; i < auxC.length(); i++) {
                        JSONObject jObject = auxC.getJSONObject(i);
                        EstandPuestoDTO AMAUX = new EstandPuestoDTO();

                        AMAUX.setArea(jObject.getInt("AREA"));
                        AMAUX.setCajon3(jObject.getString("MUEBLE1_CAJA3"));
                        AMAUX.setCajon2(jObject.getString("MUEBLE1_CAJA2"));
                        AMAUX.setCajon1(jObject.getString("MUEBLE1_CAJA1"));
                        AMAUX.setTipoMueble(jObject.getInt("TIPO_MUEBLE"));
                        AMAUX.setFecha(jObject.getString("FECHA"));
                        AMAUX.setCeco(jObject.getString("ID_CECO"));

                        listaC.add(AMAUX);
                    }
                    for (int i = 0; i < auxF.length(); i++) {
                        JSONObject jObject = auxF.getJSONObject(i);
                        EstandPuestoDTO AMAUX = new EstandPuestoDTO();

                        AMAUX.setArea(jObject.getInt("AREA"));
                        AMAUX.setCajon3(jObject.getString("MUEBLE1_CAJA3"));
                        AMAUX.setCajon2(jObject.getString("MUEBLE1_CAJA2"));
                        AMAUX.setCajon1(jObject.getString("MUEBLE1_CAJA1"));

                        AMAUX.setTipoMueble(jObject.getInt("TIPO_MUEBLE"));
                        AMAUX.setFecha(jObject.getString("FECHA"));
                        AMAUX.setCeco(jObject.getString("ID_CECO"));

                        listaF.add(AMAUX);
                    }
                }
                mv.addObject("listaG", listaG);
                mv.addObject("listaC", listaC);
                mv.addObject("listaF", listaF);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return mv;
    }

    @RequestMapping(value = "central/copia-de-la-guia-dhl.htm", method = RequestMethod.GET)
    public ModelAndView getcopiadelaguiadhl(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "copia-de-la-guia-dhl";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        try {
            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }
            String json = gestion7SBI.obtieneGuiaDHL(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                List<GuiaDHLDTO> lista = null;
                JSONArray aux = new JSONArray();
                if (rec.has("DETALLE") && !rec.isNull("DETALLE")) {
                    aux = rec.getJSONArray("DETALLE");

                    lista = new ArrayList<GuiaDHLDTO>();

                    for (int i = 0; i < aux.length(); i++) {
                        JSONObject jObject = aux.getJSONObject(i);
                        GuiaDHLDTO GuiaDHL = new GuiaDHLDTO();
                        GuiaDHL.setIdGuia(Integer.parseInt(jObject.getString("ID_GUIA")));

                        lista.add(GuiaDHL);

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateInString = jObject.getString("FECHA").trim();
                        SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                        Date date1;
                        date1 = formatter.parse(dateInString);
                        GuiaDHL.setFecha(formatoEsMX1.format(date1));
                    }
                }
                if (aux.length() > 0) {
                    mv.addObject("datos", 1);
                } else {
                    mv.addObject("datos", 0);
                }
                //mv.addObject("datos", 0);
                mv.addObject("lista", lista);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/programa-de-limpieza.htm", method = RequestMethod.GET)
    public ModelAndView getprogramadelimpieza(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "programa-de-limpieza";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        try {
            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }

            String json = gestion7SBI.obtieneProgramaLimpieza(ceco);

            int max = 0;
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                List<ProgLimpiezaDTO> lista = new ArrayList<ProgLimpiezaDTO>();
                JSONArray aux = new JSONArray();
                if (rec.has("DETALLE") && !rec.isNull("DETALLE")) {
                    aux = rec.getJSONArray("DETALLE");

                    ProgLimpiezaDTO progLimpieza = new ProgLimpiezaDTO();
                    for (int i = 0; i < aux.length(); i++) {
                        JSONObject jObject = aux.getJSONObject(i);
                        progLimpieza.setIdPrograma(Integer.parseInt(jObject.getString("ID_PROGRAMA")));
                        lista.add(progLimpieza);

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateInString = jObject.getString("FECHA").trim();
                        SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                        Date date1;
                        date1 = formatter.parse(dateInString);

                        //MAX ID_PROGRAMA
                        for (int j = 0; j < lista.size(); j++) {
                            if (lista.get(j).getIdPrograma() > max) {
                                max = lista.get(j).getIdPrograma();
                                progLimpieza.setIdPrograma(max);
                                progLimpieza.setFecha(formatoEsMX1.format(date1));

                            }
                        }
                    }
                    lista.clear();
                    lista.add(progLimpieza);
                }
                ////logger.error("MAX: "+max+", FECHA: "+progLimpieza.getFecha());
                mv.addObject("lista", lista);

            }

            String jsonDetalle = gestion7SBI.obtieneDetalleProgLimp(max);
            ////logger.error("JSON: "+jsonDetalle);
            if (jsonDetalle != null) {
                JSONObject rec = new JSONObject(jsonDetalle);

                List<ProgLimpiezaDTO> listaProg = new ArrayList<ProgLimpiezaDTO>();
                JSONArray array = new JSONArray();
                if (rec.has("DETALLE") && !rec.isNull("DETALLE")) {

                    array = rec.getJSONArray("DETALLE");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jObject = array.getJSONObject(i);
                        ProgLimpiezaDTO detalles = new ProgLimpiezaDTO();
                        detalles.setIdPrograma(Integer.parseInt(jObject.getString("ID_PROGRAMA")));
                        detalles.setArea(jObject.getString("AREA"));
                        detalles.setArticulos(jObject.getString("ARTICULOS"));
                        detalles.setFrecuencia(jObject.getString("FRECUENCIA"));
                        detalles.setPeriodicidad(jObject.getString("HORARIO"));
                        detalles.setSupervisor(jObject.getString("SUPERVISOR"));
                        detalles.setResponsable(jObject.getString("RESPONSABLE"));
                        mv.addObject("listaProg", listaProg);
                        listaProg.add(detalles);

                    }
                }
                ////logger.error("SIZE: "+listaProg.size());

            }

        } catch (JSONException e) {

            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/reporte-de-area-de-apoyo.htm", method = RequestMethod.GET)
    public ModelAndView getreportedeareadeapoyo(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "reporte-de-area-de-apoyo";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        try {
            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }

            String json = gestion7SBI.obtieneRepAreasApoyo(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                List<ReporteAreaApoyoDTO> lista = new ArrayList<ReporteAreaApoyoDTO>();
                JSONArray aux = new JSONArray();
                if (rec.has("DETALLE") && !rec.isNull("DETALLE")) {
                    aux = rec.getJSONArray("DETALLE");
                    for (int i = 0; i < aux.length(); i++) {

                        JSONObject jObject = aux.getJSONObject(i);
                        ReporteAreaApoyoDTO AMAUX = new ReporteAreaApoyoDTO();
                        AMAUX.setIdformato(jObject.getInt("ID_FORMATO"));
                        AMAUX.setFolio(jObject.getString("FOLIO"));

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateInString = jObject.getString("FECHA").trim();
                        SimpleDateFormat formatoEsMX = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                        Date date;
                        date = formatter.parse(dateInString);

                        AMAUX.setFecha(formatoEsMX.format(date));

                        //AMAUX.setFecha(jObject.getString("FECHA"));
                        lista.add(AMAUX);
                        if (lista.isEmpty()) {
                            mv.addObject("paso", "no");
                        } else {
                            mv.addObject("paso", "cons1");
                        }
                    }
                }
                mv.addObject("lista", lista);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return mv;
    }

    @RequestMapping(value = "central/bitacora-de-mantenimiento.htm", method = RequestMethod.GET)
    public ModelAndView getbitacorademantenimiento(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "bitacora-de-mantenimiento";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        try {
            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }

            String json = gestion7SBI.obtieneBitMantenimiento(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                List<BitMantenimientoDTO> lista = new ArrayList<BitMantenimientoDTO>();
                JSONArray aux = new JSONArray();
                if (rec.has("DETALLEBITA") && !rec.isNull("DETALLEBITA")) {
                    aux = rec.getJSONArray("DETALLEBITA");
                    for (int i = 0; i < aux.length(); i++) {
                        JSONObject jObject = aux.getJSONObject(i);
                        BitMantenimientoDTO bitacora = new BitMantenimientoDTO();
                        bitacora.setFolio(jObject.getString("FOLIO"));

                        lista.add(bitacora);

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateInString = jObject.getString("FECHA_SOLICITUD").trim();
                        SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                        Date date1;
                        date1 = formatter.parse(dateInString);
                        bitacora.setFechaSolicitud(formatoEsMX1.format(date1));
                    }
                }
                mv.addObject("lista", lista);
            }
        } catch (JSONException json) {

            json.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/camino-para-atender-mejor.htm", method = RequestMethod.GET)
    public ModelAndView getcamioparaatendermejor(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "camino-para-atender-mejor";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        return mv;
    }

    @RequestMapping(value = "central/camino-para-vender-mejor.htm", method = RequestMethod.GET)
    public ModelAndView getcaminoparavendermejor(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "camino-para-vender-mejor";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        return mv;
    }

    @RequestMapping(value = "central/contratacion-masiva.htm", method = RequestMethod.GET)
    public ModelAndView getcontratacionmasiva(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "contratacion-masiva";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/guia-de-atencion-falla.htm", method = RequestMethod.GET)
    public ModelAndView getguiadeatencionfalla(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "guia-de-atencion-falla";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/guia-de-atencion-a-contigencia.htm", method = RequestMethod.GET)
    public ModelAndView getguiadeatencionacontigencia(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "guia-de-atencion-a-contigencia";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/contigencia-fenomeno.htm", method = RequestMethod.GET)
    public ModelAndView getcontigenciafenomeno(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "contigencia-fenomeno";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/checklist-de-respuesta-para-contigencias.htm", method = RequestMethod.GET)
    public ModelAndView getchecklistderespuestaparacontigencias(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "checklist-de-respuesta-para-contigencias";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        return mv;
    }

    @RequestMapping(value = "central/pantalla-actual-punto-de-venta.htm", method = RequestMethod.GET)
    public ModelAndView getpantallaactualpuntodeventa(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "pantalla-actual-punto-de-venta";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        try {
            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }

            String json = gestion7SBI.obtieneFotoPlantilla(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                JSONArray aux = rec.getJSONArray("FOTOPLANTILLA");
                String ruta = rec.get("RUTA_FOTO").toString().substring(1, rec.get("RUTA_FOTO").toString().length());
                List<FotoPlantillaActualDTO> lista = new ArrayList<FotoPlantillaActualDTO>();

                for (int i = 0; i < aux.length(); i++) {
                    JSONObject jObject = aux.getJSONObject(i);
                    FotoPlantillaActualDTO plantillaAct = new FotoPlantillaActualDTO();
                    plantillaAct.setIdUser(Integer.parseInt(jObject.getString("ID_USUARIO")));
                    plantillaAct.setNomUser(jObject.getString("NOM_USUARIO"));
                    plantillaAct.setPuesto(jObject.getString("PUESTO"));
                    mv.addObject("RUTA_FOTO", ruta);
                    //logger.error(ruta);
                    lista.add(plantillaAct);
                }
                mv.addObject("lista", lista);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/cedula-de-identificacion-personal.htm", method = RequestMethod.GET)
    public ModelAndView getceduladeidentificaciopersonal(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "cedula-de-identificacion-personal";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        try {
            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }

            String json = gestion7SBI.obtienePlantillaCedula(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                JSONArray aux = rec.getJSONArray("PLANTILLA");

                List<CedulaIdentificacionDTO> lista = new ArrayList<CedulaIdentificacionDTO>();

                for (int i = 0; i < aux.length(); i++) {
                    JSONObject jObject = aux.getJSONObject(i);
                    CedulaIdentificacionDTO cedID = new CedulaIdentificacionDTO();
                    cedID.setIdUser(jObject.getString("ID_USUARIO"));
                    cedID.setNomUser(jObject.getString("NOM_USUARIO"));

                    lista.add(cedID);
                }
                mv.addObject("lista", lista);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/formato-cumplimiento.htm", method = RequestMethod.GET)
    public ModelAndView getformatocumplimiento(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "formato-cumplimiento";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        try {
            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }

            String json = gestion7SBI.obtienePlantillaRev7S(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                JSONArray aux = rec.getJSONArray("PLANTILLA");

                List<CedulaIdentificacionDTO> lista = new ArrayList<CedulaIdentificacionDTO>();

                for (int i = 0; i < aux.length(); i++) {
                    JSONObject jObject = aux.getJSONObject(i);
                    CedulaIdentificacionDTO cedID = new CedulaIdentificacionDTO();
                    cedID.setIdUser(jObject.getString("ID_USUARIO"));
                    cedID.setNomUser(jObject.getString("NOM_USUARIO"));
                    cedID.setPuesto(jObject.getString("PUESTO"));
                    cedID.setBandera(jObject.getInt("BANDERA"));
                    lista.add(cedID);
                }
                mv.addObject("lista", lista);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }

        return mv;
    }

    @RequestMapping(value = "central/manual-metodologia.htm", method = RequestMethod.GET)
    public ModelAndView getmanualmetodologia(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "manual-metodologia";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/protocolo-de-medicion.htm", method = RequestMethod.GET)
    public ModelAndView getprotocolodemedicion(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "protocolo-de-medicion";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        try {

            String json = gestion7SBI.obtieneReporteChecksSemana(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                JSONArray aux = rec.getJSONArray("REPORTE");

                List<ReporteCheckDTO> lista = new ArrayList<ReporteCheckDTO>();
                for (int i = 0; i < aux.length(); i++) {
                    JSONObject jObject = aux.getJSONObject(i);
                    ReporteCheckDTO auxRep = new ReporteCheckDTO();
                    auxRep.setIdChecklist(jObject.getInt("ID_CHECKLIST"));
                    auxRep.setBandera(jObject.getInt("BANDERA"));
                    auxRep.setCalificacion(jObject.getInt("CALIFICACION"));
                    auxRep.setCeco(jObject.getString("ID_CECO"));
                    auxRep.setFechaFin(jObject.getString("FECHA_FIN"));
                    auxRep.setFechaInicio(jObject.getString("FECHA_INICIO"));
                    auxRep.setIdBitacora(jObject.getInt("ID_BITACORA"));
                    auxRep.setNomChecklist(jObject.getString("NOMBRE_CHECKLIST"));
                    auxRep.setSemana(jObject.getInt("SEMANA"));

                    String fecha = jObject.getString("FECHA_FIN");
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("es", "ES"));
                    try {
                        if (fecha != null && fecha != "null") {

                            cal.setTime(sdf.parse(fecha));
                            int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
                            if (diaSemana == 1) {
                                cal.add(Calendar.DATE, -1);
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (fecha != null && fecha != "null") {
                        int semana = 0;
                        semana = cal.get(Calendar.WEEK_OF_YEAR);
                        auxRep.setSemana(semana);
                    }

                    lista.add(auxRep);
                }
                //ArrayList<List<ReporteCheckDTO>> listaListas = new ArrayList<>();
                HashMap<String, List<ReporteCheckDTO>> anios = new HashMap<String, List<ReporteCheckDTO>>();
                List<String> nomAnios = new ArrayList<String>();
                for (ReporteCheckDTO auxRep : lista) {
                    String anio = auxRep.getFechaInicio().substring(0, 4).trim();
                    if (anios.containsKey(anio)) {
                        anios.get(anio).add(auxRep);
                    } else {
                        anios.put(anio, new ArrayList<ReporteCheckDTO>());
                        anios.get(anio).add(auxRep);
                        nomAnios.add(anio);
                    }

                }

                for (String auxAnio : nomAnios) {
                    List<ReporteCheckDTO> listaAux = new ArrayList<ReporteCheckDTO>();
                    for (int i = 1; i < 53; i++) {
                        if (!anios.isEmpty()) {
                            ReporteCheckDTO RC = new ReporteCheckDTO();
                            boolean flag = false;
                            for (ReporteCheckDTO sa : anios.get(auxAnio)) {
                                if (i == sa.getSemana()) {
                                    RC = sa;
                                    flag = true;
                                }
                            }

                            if (flag == true) {
                                if (i == 0) {
                                    RC.setMes(0);
                                }
                                if (i >= 1 && i <= 5) {
                                    RC.setMes(1);
                                }
                                if (i >= 6 && i <= 9) {
                                    RC.setMes(2);
                                }
                                if (i >= 10 && i <= 13) {
                                    RC.setMes(3);
                                }
                                if (i >= 14 && i <= 18) {
                                    RC.setMes(4);
                                }
                                if (i >= 19 && i <= 22) {
                                    RC.setMes(5);
                                }
                                if (i >= 23 && i <= 26) {
                                    RC.setMes(6);
                                }
                                if (i >= 27 && i <= 31) {
                                    RC.setMes(7);
                                }
                                if (i >= 32 && i <= 35) {
                                    RC.setMes(8);
                                }
                                if (i >= 36 && i <= 40) {
                                    RC.setMes(9);
                                }
                                if (i >= 41 && i <= 44) {
                                    RC.setMes(10);
                                }
                                if (i >= 45 && i <= 48) {
                                    RC.setMes(11);
                                }
                                if (i >= 49 && i <= 52) {
                                    RC.setMes(12);
                                }

                                listaAux.add(RC);
                            } else {
                                RC.setSemana(i);
                                RC.setBandera(2);
                                RC.setCalificacion(0);

                                if (i == 0) {
                                    RC.setMes(0);
                                }
                                if (i >= 2 && i <= 5) {
                                    RC.setMes(1);
                                }
                                if (i >= 6 && i <= 9) {
                                    RC.setMes(2);
                                }
                                if (i >= 10 && i <= 13) {
                                    RC.setMes(3);
                                }
                                if (i >= 14 && i <= 18) {
                                    RC.setMes(4);
                                }
                                if (i >= 19 && i <= 22) {
                                    RC.setMes(5);
                                }
                                if (i >= 23 && i <= 26) {
                                    RC.setMes(6);
                                }
                                if (i >= 27 && i <= 31) {
                                    RC.setMes(7);
                                }
                                if (i >= 32 && i <= 35) {
                                    RC.setMes(8);
                                }
                                if (i >= 36 && i <= 40) {
                                    RC.setMes(9);
                                }
                                if (i >= 41 && i <= 44) {
                                    RC.setMes(10);
                                }
                                if (i >= 45 && i <= 48) {
                                    RC.setMes(11);
                                }
                                if (i >= 49 && i <= 52) {
                                    RC.setMes(12);
                                }

                                listaAux.add(RC);
                            }
                        } else {

                            ReporteCheckDTO RC = new ReporteCheckDTO();
                            RC.setSemana(i);
                            RC.setBandera(2);
                            RC.setCalificacion(0);

                            if (i == 0) {
                                RC.setMes(0);
                            }
                            if (i >= 2 && i <= 5) {
                                RC.setMes(1);
                            }
                            if (i >= 6 && i <= 9) {
                                RC.setMes(2);
                            }
                            if (i >= 10 && i <= 13) {
                                RC.setMes(3);
                            }
                            if (i >= 14 && i <= 18) {
                                RC.setMes(4);
                            }
                            if (i >= 19 && i <= 22) {
                                RC.setMes(5);
                            }
                            if (i >= 23 && i <= 26) {
                                RC.setMes(6);
                            }
                            if (i >= 27 && i <= 31) {
                                RC.setMes(7);
                            }
                            if (i >= 32 && i <= 35) {
                                RC.setMes(8);
                            }
                            if (i >= 36 && i <= 40) {
                                RC.setMes(9);
                            }
                            if (i >= 41 && i <= 44) {
                                RC.setMes(10);
                            }
                            if (i >= 45 && i <= 48) {
                                RC.setMes(11);
                            }
                            if (i >= 49 && i <= 52) {
                                RC.setMes(12);
                            }

                            listaAux.add(RC);

                        }

                    }
                    String[] strMonths = new String[]{
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"};
                    List<String> meses = new ArrayList<String>();
                    for (String mes : strMonths) {
                        meses.add(mes);
                    }
                    mv.addObject("A" + auxAnio, listaAux);
                    mv.addObject("meses", meses);
                }

                //mv.addObject("lista", lista);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }

        return mv;
    }

    @RequestMapping(value = "central/acta-de-hechos.htm", method = RequestMethod.GET)
    public ModelAndView getactadehechos(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idActa", required = false, defaultValue = "vacio") String idActa
    )
            throws ServletException, IOException {

        String salida = "acta-de-hechos";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        try {

            String json = gestion7SBI.obtieneActasMinucias(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                JSONArray aux = rec.getJSONArray("ACTAS_MINUCIAS");

                for (int i = 0; i < aux.length(); i++) {
                    JSONObject jObject = aux.getJSONObject(i);
                    if (jObject.getString("ID_ACTA").trim().equals(idActa)) {
                        mv.addObject("LUGAR_HECHOS", jObject.getString("LUGAR_HECHOS"));
                        mv.addObject("FECHA", jObject.getString("FECHA"));
                        mv.addObject("DIRECCION", jObject.getString("DIRECCION"));
                        mv.addObject("ID_ACTA", jObject.getString("ID_ACTA"));
                        mv.addObject("TESTIGO1", jObject.getString("TESTIGO1"));
                        mv.addObject("TESTIGO2", jObject.getString("TESTIGO2"));
                        mv.addObject("VOBO", jObject.getString("VOBO"));
                        mv.addObject("DESTRUYE", jObject.getString("DESTRUYE"));

                        JSONArray jArray = jObject.getJSONArray("ARCHIVOS");
                        List<EvidenciaMinuciasDTO> lista = new ArrayList<EvidenciaMinuciasDTO>();
                        for (int j = 0; j < jArray.length(); j++) {
                            JSONObject jObject2 = jArray.getJSONObject(j);
                            EvidenciaMinuciasDTO evid = new EvidenciaMinuciasDTO();
                            evid.setIdEvidencia(jObject2.getInt("ID_EVIDENCIA"));
                            evid.setNomArchivo(jObject2.getString("NOM_ARCHIVO"));
                            evid.setRuta(jObject2.getString("RUTA").substring(1, jObject2.getString("RUTA").length()));

                            lista.add(evid);
                        }
                        mv.addObject("listaEvid", lista);
                    }

                }

            }
        } catch (JSONException e) {

            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/acta-entrega-expediente2.htm", method = RequestMethod.GET)
    public ModelAndView getactaentregaexpediente2(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idExp", required = false, defaultValue = "") String idExpediente
    )
            throws ServletException, IOException {

        String salida = "acta-entrega-expediente2";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        String ciudad = "";
        String nomRegional = "";
        String numRegional = "";
        String descripcion = "";
        String idSucursal = "";
        String nomSucursal = "";
        String fecha = "";
        String nomGerente = "";
        String numGerente = "";
        String usuario = "";
        String usuario2 = "";
        String usuario3 = "";
        String nomusuario2 = "";
        String nomusuario3 = "";

        String numExpediente = "";

        int estatus = 0;

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
        nomGerente = "" + request.getSession().getAttribute("nombre");
        numGerente = "" + request.getSession().getAttribute("numempleado");

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(idExpediente, null, null, null, null, null, "1");

        logger.info("Numero de sucursal " + request.getSession().getAttribute("sucursal"));

        boolean imp = false;

        for (ExpedientesDTO exp : lista) {
            numExpediente = "" + exp.getIdExpediente();
            ciudad = exp.getCiudad();
            descripcion = exp.getDescripcion();
            idSucursal = exp.getIdCeco();
            fecha = exp.getFecha();
            numGerente = "" + exp.getIdGerente();
            estatus = exp.getEstatusExpediente();
        }

        List<ExpedientesDTO> listaU = InsertExped.buscaResponsable(idExpediente, null, null);

        for (ExpedientesDTO exp : listaU) {
            if (exp.getEstatusResp() == 1) {
                nomRegional = exp.getNombreResp();
                numRegional = "" + exp.getIdentificador();
            }
            if (exp.getEstatusResp() == 3) {
                nomusuario2 = exp.getNombreResp();
                usuario2 = "" + exp.getIdentificador();
            }
            if (exp.getEstatusResp() == 4) {
                nomGerente = exp.getNombreResp();
                numGerente = "" + exp.getIdentificador();
            }
        }

        String fechaSplit[] = fecha.split(" ");
        String hora = fechaSplit[1];
        String dia = fechaSplit[0].split("/")[0];
        String mes = fechaSplit[0].split("/")[1];
        String anio = fechaSplit[0].split("/")[2];

        switch (mes) {
            case "01":
                mes = "Enero";
                break;
            case "02":
                mes = "Febrero";
                break;
            case "03":
                mes = "Marzo";
                break;
            case "04":
                mes = "Abril";
                break;
            case "05":
                mes = "Mayo";
                break;
            case "06":
                mes = "Junio";
                break;
            case "07":
                mes = "Julio";
                break;
            case "08":
                mes = "Agosto";
                break;
            case "09":
                mes = "Septiembre";
                break;
            case "10":
                mes = "Octubre";
                break;
            case "11":
                mes = "Noviembre";
                break;
            case "12":
                mes = "Diciembre";
                break;

        }

        mv.addObject("ciudad", ciudad);
        mv.addObject("nomRegional", nomRegional);
        mv.addObject("numRegional", numRegional);
        mv.addObject("descripcion", descripcion);
        mv.addObject("idSucursal", idSucursal);
        mv.addObject("nomSucursal", nomSucursal);
        mv.addObject("fecha", fecha);
        mv.addObject("nomGerente", nomGerente);
        mv.addObject("numGerente", numGerente);
        mv.addObject("usuario", usuario);
        mv.addObject("usuario2", usuario2);
        mv.addObject("nomusuario2", nomusuario2);
        mv.addObject("nomusuario3", nomusuario3);

        mv.addObject("numExpediente", numExpediente);

        mv.addObject("usuario3", usuario3);

        if (estatus == 3) {
            mv.addObject("paso", "impreso");
            mv.addObject("hora", hora);
            mv.addObject("mes", mes);
            mv.addObject("dia", dia);
            mv.addObject("anio", anio);
            return mv;
        } else if (estatus == 2) {
            mv.addObject("paso", "no2");
            nomGerente = "" + request.getSession().getAttribute("nombre");
            numGerente = "" + request.getSession().getAttribute("numempleado");
            Date date = new Date();
            //Caso 1: obtener la hora y salida por pantalla con formato:
            DateFormat hourFormat = new SimpleDateFormat("HH:mm");
            System.out.println("Hora: " + hourFormat.format(date));
            //Caso 2: obtener la fecha y salida por pantalla con formato:
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println("Fecha: " + dateFormat.format(date));
            //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
            DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            System.out.println("Hora y fecha: " + hourdateFormat.format(date));

            mv.addObject("hora", "" + hourFormat.format(date));
            mv.addObject("fecha", "" + hourdateFormat.format(date));
            return mv;
        } else {
            mv.addObject("paso", "no3");
            mv.addObject("tok", "OK");
            nomGerente = "" + request.getSession().getAttribute("nombre");
            numGerente = "" + request.getSession().getAttribute("numempleado");
            Date date = new Date();
            //Caso 1: obtener la hora y salida por pantalla con formato:
            DateFormat hourFormat = new SimpleDateFormat("HH:mm");
            System.out.println("Hora: " + hourFormat.format(date));
            //Caso 2: obtener la fecha y salida por pantalla con formato:
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println("Fecha: " + dateFormat.format(date));
            //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
            DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            System.out.println("Hora y fecha: " + hourdateFormat.format(date));

            mv.addObject("hora", "" + hourFormat.format(date));
            mv.addObject("fecha", "" + hourdateFormat.format(date));
            return mv;
        }
    }

    @RequestMapping(value = "central/acta-de-entrega-para-concentracion.htm", method = RequestMethod.GET)
    public ModelAndView getactadeentregaparaconcentracion(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "acta-de-entrega-para-concentracion";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
//
        return mv;
    }

    @RequestMapping(value = "central/activos-fijos.htm", method = RequestMethod.GET)
    public ModelAndView getactivosfijos(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idActa", required = false, defaultValue = "vacio") String idActa
    )
            throws ServletException, IOException {

        String salida = "activos-fijos";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
        String nomSuc = "" + request.getSession().getAttribute("nomsucursal");
        try {
            String json = gestion7SBI.obtieneActivoFijo(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                JSONArray aux = rec.getJSONArray("ACTAS_ACTIVOS");

                for (int i = 0; i < aux.length(); i++) {
                    JSONObject jObject = aux.getJSONObject(i);
                    if (jObject.getString("ID_ACTA").trim().equals(idActa)) {
                        mv.addObject("ID_ACTA", jObject.getString("ID_ACTA"));
                        mv.addObject("CECO", ceco);
                        mv.addObject("DESC_CECO", nomSuc);
                        mv.addObject("UBICACION", jObject.getString("UBICACION"));
                        mv.addObject("INVOLUCRADOS", jObject.getString("INVOLUCRADOS"));
                        mv.addObject("MONTO", jObject.getString("MONTO"));
                        mv.addObject("RAZON_SOCIAL", jObject.getString("RAZON_SOCIAL"));
                        mv.addObject("DOMICILIO", jObject.getString("DOMICILIO"));
                        mv.addObject("RFC", jObject.getString("RFC"));
                        mv.addObject("RESPONSABLE", jObject.getString("RESPONSABLE"));
                        mv.addObject("TESTIGO", jObject.getString("TESTIGO"));

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateInString = jObject.getString("FECHA").trim();

                        if (dateInString != null && dateInString != "null") {
                            SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                            Date date1;
                            date1 = formatter.parse(dateInString);
                            mv.addObject("FECHA", formatoEsMX1.format(date1));

                            JSONArray jArray = jObject.getJSONArray("ACTIVOS");
                            List<ActasActFijoDTO> listaActivos = new ArrayList<ActasActFijoDTO>();
                            for (int j = 0; j < jArray.length(); j++) {
                                JSONObject jObject2 = jArray.getJSONObject(j);
                                ActasActFijoDTO activos = new ActasActFijoDTO();
                                activos.setIdActFijo(Integer.parseInt(jObject2.getString("ID_ACTIVO")));
                                activos.setPlaca(jObject2.getString("PLACA"));
                                activos.setDescripcion(jObject2.getString("DESCRIPCION"));
                                activos.setMarca(jObject2.getString("MARCA"));
                                activos.setSerie(jObject2.getString("SERIE"));

                                listaActivos.add(activos);
                            }
                            mv.addObject("listaActivos", listaActivos);

                            //logger.error("LISTA: "+listaActivos.size());
                        }
                    }

                }

            }
        } catch (JSONException e) {

            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/detalle-de-la-guia.htm", method = RequestMethod.GET)
    public ModelAndView getdetalledelaguia(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idGuia", required = false, defaultValue = "vacio") String idGuia
    )
            throws ServletException, IOException {

        String salida = "detalle-de-la-guia";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
        String nomSucursal = "" + request.getSession().getAttribute("nomsucursal");
        try {
            String json = gestion7SBI.obtieneGuiaDHL(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                JSONArray aux = rec.getJSONArray("DETALLE");

                for (int i = 0; i < aux.length(); i++) {
                    JSONObject jObject = aux.getJSONObject(i);
                    if (jObject.getString("ID_GUIA").trim().equals(idGuia)) {
                        mv.addObject("ID_GUIA", jObject.getString("ID_GUIA"));
                        mv.addObject("CECO", ceco);
                        mv.addObject("DESC_CECO", nomSucursal);

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateInString = jObject.getString("FECHA").trim();
                        SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                        Date date1;
                        date1 = formatter.parse(dateInString);
                        mv.addObject("FECHA", formatoEsMX1.format(date1));

                        JSONArray jArray = jObject.getJSONArray("ARCHIVOS");
                        List<GuiaDHLDTO> listaGuia = new ArrayList<GuiaDHLDTO>();
                        for (int j = 0; j < jArray.length(); j++) {
                            JSONObject jObject2 = jArray.getJSONObject(j);
                            GuiaDHLDTO guia = new GuiaDHLDTO();
                            if (jObject2.getString("ID_EVIDENCIA") != null && jObject2.getString("ID_EVIDENCIA") != "null") {
                                guia.setIdEvid(Integer.parseInt(jObject2.getString("ID_EVIDENCIA")));
                                guia.setRuta(jObject2.getString("RUTA").substring(1, jObject2.getString("RUTA").length()));
                                guia.setNombreArch(jObject2.getString("NOM_ARCHIVO"));
                                if (jObject2.getString("COMENTARIO") != null && jObject2.getString("COMENTARIO") != "null") {
                                    guia.setComent(jObject2.getString("COMENTARIO"));
                                } else {
                                    guia.setComent("Sin comentarios");
                                }
                            } else {
                                //logger.error("NO HAY EVIDENCIAS");
                            }
                            listaGuia.add(guia);
                        }
                        mv.addObject("listaGuia", listaGuia);

                    }

                }

            }
        } catch (JSONException e) {

            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/datalle-apoyo.htm", method = RequestMethod.GET)
    public ModelAndView getDetalleApoyo(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "folio", required = false, defaultValue = "") String Folio
    )
            throws ServletException, IOException {

        String salida = "detalle-apoyo";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        try {

            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }

            String json = gestion7SBI.obtieneDetalleApoyo(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                JSONArray aux = rec.getJSONArray("DETALLE");

                for (int i = 0; i < aux.length(); i++) {
                    JSONObject jObject = aux.getJSONObject(i);
                    if (jObject.getString("FOLIO").trim().equals(Folio)) {
                        mv.addObject("FOLIO", jObject.getString("FOLIO"));
                        mv.addObject("CECO", jObject.getString("CECO"));
                        mv.addObject("AREA_INV", jObject.getString("AREA_INV"));
                        mv.addObject("PRIORIDAD", jObject.getString("PRIORIDAD"));
                        mv.addObject("FECHA", jObject.getString("FECHA"));
                        mv.addObject("ID_FORMATO", jObject.getString("ID_FORMATO"));

                        JSONArray jArray = jObject.getJSONArray("ARCHIVOS");
                        List<DetalleApoyoDTO> lista = new ArrayList<DetalleApoyoDTO>();
                        for (int j = 0; j < jArray.length(); j++) {
                            JSONObject jObject2 = jArray.getJSONObject(j);
                            DetalleApoyoDTO evid = new DetalleApoyoDTO();
                            evid.setIdEvidencia(jObject2.getInt("ID_EVIDENCIA"));
                            // evid.setRuta(jObject2.getString("RUTA"));
                            evid.setRuta(jObject2.getString("RUTA").substring(1, jObject2.getString("RUTA").length()));
                            evid.setNomarchivo(jObject2.getString("NOM_ARCHIVO"));

                            lista.add(evid);
                        }
                        mv.addObject("listaevid", lista);
                    }

                }

            }
        } catch (JSONException e) {

            e.printStackTrace();
        }

        return mv;
    }

    @RequestMapping(value = "central/bitacora-de-mantenimiento2.htm", method = RequestMethod.GET)
    public ModelAndView getbitacorademantenimiento2(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "folio", required = false, defaultValue = "vacio") String folio
    )
            throws ServletException, IOException {

        String salida = "bitacora-de-mantenimiento2";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
        try {
            String json = gestion7SBI.obtieneBitMantenimiento(ceco);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                JSONArray aux = rec.getJSONArray("DETALLEBITA");

                for (int i = 0; i < aux.length(); i++) {
                    JSONObject jObject = aux.getJSONObject(i);
                    if (jObject.getString("FOLIO").trim().equals(folio)) {
                        mv.addObject("FOLIO", jObject.getString("FOLIO"));
                        mv.addObject("NOM_PROVEE", jObject.getString("NOMBRE_PROVEED"));
                        if (jObject.getString("TIPO_MTTO").equalsIgnoreCase("0")) {
                            mv.addObject("TIPO_MTTO", "Correctivo");
                        } else {

                            mv.addObject("TIPO_MTTO", "Preventivo");
                        }
                        mv.addObject("HORA_ENTRA", jObject.getString("HORA_ENTRADA"));
                        mv.addObject("HORA_SALE", jObject.getString("HORA_SALIDA"));
                        mv.addObject("NUM_PERSONAS", Integer.parseInt(jObject.getString("NUM_PERSONAS")));
                        mv.addObject("DETALLES", jObject.getString("DETALLE_TRABAJO"));

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateInString = jObject.getString("FECHA_SOLICITUD").trim();
                        SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                        Date date1;
                        date1 = formatter.parse(dateInString);
                        mv.addObject("FECHA_SOLICITUD", formatoEsMX1.format(date1));
                    }

                }

            }
        } catch (JSONException e) {

            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/detalle-cedula.htm", method = RequestMethod.GET)
    public ModelAndView getdetallecedula(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idUser", required = false, defaultValue = "vacio") String idUser
    )
            throws ServletException, IOException {
        String salida = "detalle-cedula";
        ModelAndView mv = new ModelAndView(salida, "command", null);
        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
        try {
            String json = gestion7SBI.obtieneDetalleCedula(idUser);
            if (json != null && !json.trim().equals("null")) {
                JSONObject rec = new JSONObject(json);
                if (!rec.isNull("DETALLE")) {
                    JSONArray aux = rec.getJSONArray("DETALLE");
                    mv.addObject("datos", 1);
                    for (int i = 0; i < aux.length(); i++) {
                        JSONObject jObject = aux.getJSONObject(i);
                        if (jObject.getString("ID_USUARIO").trim().equals(idUser)) {
                            mv.addObject("ID_USUARIO", jObject.getString("ID_USUARIO"));
                            mv.addObject("NOM_USUARIO", jObject.getString("NOM_USUARIO"));
                            mv.addObject("PUESTO", jObject.getString("PUESTO"));
                            mv.addObject("NSS", jObject.getString("NSS"));
                            mv.addObject("TIPO_SANGRE", jObject.getString("TIPO_SANGRE"));

                            if (jObject.getString("ALERGIA").trim().equals("0")) {
                                mv.addObject("ALERGIA", "No");
                            } else {
                                if (jObject.getString("DESC_ALERGIA") != null && !jObject.getString("DESC_ALERGIA").trim().equals("null")) {
                                    mv.addObject("ALERGIA", "Si, " + jObject.getString("DESC_ALERGIA"));
                                } else {
                                    mv.addObject("ALERGIA", "Si");
                                }
                            }

                            if (jObject.getString("ENFERMEDAD").trim().equals("0")) {
                                mv.addObject("ENFERMEDAD", "No");
                            } else {
                                if (jObject.getString("DESC_ENFERMEDAD") != null && !jObject.getString("DESC_ENFERMEDAD").trim().equals("null")) {
                                    mv.addObject("ENFERMEDAD", "Si, " + jObject.getString("DESC_ENFERMEDAD"));
                                } else {
                                    mv.addObject("ENFERMEDAD", "Si");
                                }

                            }

                            if (jObject.getString("TRATAMIENTO").trim().equals("0")) {
                                mv.addObject("TRATAMIENTO", "No");
                            } else {
                                if (jObject.getString("DESC_TRATAMIENTO") != null && !jObject.getString("DESC_TRATAMIENTO").trim().equals("null")) {
                                    mv.addObject("TRATAMIENTO", "Si, " + jObject.getString("DESC_TRATAMIENTO"));
                                } else {
                                    mv.addObject("TRATAMIENTO", "Si");
                                }

                            }
                            mv.addObject("CONTACTO", jObject.getString("CONTACTO"));
                            mv.addObject("TEL_CONTACTO", jObject.getString("TEL_CONTACTO"));
                            mv.addObject("RUTA_FOTO", jObject.getString("RUTA_IMAGEN").substring(1, jObject.getString("RUTA_IMAGEN").length()));

                            //logger.error("RUTA: "+jObject.getString("RUTA_IMAGEN").substring(1, jObject.getString("RUTA_IMAGEN").length()));
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String dateInString = jObject.getString("FECHA").trim();
                            SimpleDateFormat formatoEsMX1 = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                            Date date1;
                            date1 = formatter.parse(dateInString);
                            mv.addObject("FECHA", formatoEsMX1.format(date1));
                        }

                    }
                } else {
                    mv.addObject("datos", 0);
                }

            } else {
                mv.addObject("datos", 0);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return mv;
    }

    @RequestMapping(value = "central/formato-cumplimiento-usuario.htm", method = RequestMethod.GET)
    public ModelAndView getformatocumplimientousuario(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idUsu", required = false, defaultValue = "") String IdUsuario
    )
            throws ServletException, IOException {

        String salida = "formato-cumplimiento-usuario";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        try {

            String ceco = "" + request.getSession().getAttribute("sucursal");
            if (ceco.trim().length() == 4) {
                ceco = "48" + ceco.trim();
            }
            if (ceco.trim().length() == 3) {
                ceco = "480" + ceco.trim();
            }
            if (ceco.trim().length() == 2) {
                ceco = "4800" + ceco.trim();
            }
            if (ceco.trim().length() == 1) {
                ceco = "48000" + ceco.trim();
            }

            String json = gestion7SBI.obtieneDetalleFormatoRev7S(IdUsuario);
            if (json != null) {
                JSONObject rec = new JSONObject(json);
                if (!rec.isNull("DETALLE")) {
                    JSONArray aux = rec.getJSONArray("DETALLE");
                    List<FormatoCumplimientoUsuarioDTO> lista = new ArrayList<FormatoCumplimientoUsuarioDTO>();
                    if (rec.isNull("DETALLE")) {
                        mv.addObject("listaevid", lista);
                        return mv;
                    }

                    for (int i = 0; i < aux.length(); i++) {
                        JSONObject jObject = aux.getJSONObject(i);
                        FormatoCumplimientoUsuarioDTO mv1 = new FormatoCumplimientoUsuarioDTO();
                        mv1.setNomUsuario(jObject.getString("NOM_USUARIO"));
                        mv1.setAtenderMejor(jObject.getString("ATENDER_MEJOR"));
                        mv1.setVenderMejor(jObject.getString("VENDER_MEJOR"));
                        mv1.setIdUsuario(jObject.getInt("ID_USUARIO"));
                        mv1.setOrdenador(jObject.getInt("ORDENADO"));
                        mv1.setPuesto(jObject.getString("PUESTO"));
                        mv1.setBandera(jObject.getInt("BANDERA"));
                        mv1.setIdFormato(jObject.getInt("ID_FORMATO"));

                        mv.addObject("NOM_USUARIO", jObject.getString("NOM_USUARIO"));
                        mv.addObject("ATENDER_MEJOR", jObject.getString("ATENDER_MEJOR"));
                        mv.addObject("VENDER_MEJOR", jObject.getString("VENDER_MEJOR"));
                        mv.addObject("ORDENADO", jObject.getString("ORDENADO"));
                        mv.addObject("FECHA", jObject.getString("FECHA"));

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateInString = jObject.getString("FECHA").trim();
                        SimpleDateFormat formatoEsMX = new SimpleDateFormat("dd MMMM yyyy", new Locale("ES", "MX"));
                        Date date;
                        date = formatter.parse(dateInString);
                        mv1.setFecha(formatoEsMX.format(date));

                        lista.add(mv1);

                    }
                    if (aux.length() > 0) {
                        mv.addObject("datos", 1);
                    } else {
                        mv.addObject("datos", 0);
                    }
                    mv.addObject("lista", lista);
                } else {
                    mv.addObject("datos", 0);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return mv;
    }

    @RequestMapping(value = "central/protocolo-semana-uno.htm", method = RequestMethod.GET)
    public ModelAndView getprotocolosemanauno(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "protocolo-semana-uno";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/cuestionario-semana1.htm", method = RequestMethod.GET)
    public ModelAndView getcuestionariosemana1(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "cuestionario-semana1";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/activos-fijos-pdf.htm", method = RequestMethod.GET)
    public ModelAndView getactivosfijospdf(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "activos-fijos-pdf";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/catalogo-de-activos-fijos.htm", method = RequestMethod.GET)
    public ModelAndView getcatalogodeactivosfijos(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "catalogo-de-activos-fijos";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/checklist-de-respuesta-para-contigencias2.htm", method = RequestMethod.GET)
    public ModelAndView getchecklistderespuestaparacontigencias2(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "checklist-de-respuesta-para-contigencias2";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/27.htm", method = RequestMethod.GET)
    public ModelAndView get27(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "27";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, "1");

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/catalogo-de-minucias.htm", method = RequestMethod.GET)
    public ModelAndView getcatalogodeminucias(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "catalogo-de-minucias";
        ModelAndView mv = new ModelAndView(salida, "command", null);

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        return mv;
    }

    /**
     * ***********************************************************************************************
     */
//	http://10.51.218.72:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=480100&nomsucursal=DAZIGUALAPA&puesto=652
//	http://10.51.218.189:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=480100&nomsucursal=DAZIGUALAPA&puesto=652
    @RequestMapping(value = "central/altaArchiveros.htm", method = RequestMethod.GET)
    public ModelAndView controllerArchiveros(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "usuario", required = false, defaultValue = "vacio") String usuario,
            @RequestParam(value = "password", required = false, defaultValue = "") String password,
            @RequestParam(value = "numempleado", required = false, defaultValue = "") String numempleado,
            @RequestParam(value = "ws", required = false, defaultValue = "") String ws,
            @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
            @RequestParam(value = "apellidop", required = false, defaultValue = "") String apellidop,
            @RequestParam(value = "apellidom", required = false, defaultValue = "") String apellidom,
            @RequestParam(value = "sucursal", required = false, defaultValue = "") String sucursal,
            @RequestParam(value = "nomsucursal", required = false, defaultValue = "") String nomsucursal,
            @RequestParam(value = "puesto", required = false, defaultValue = "") String puesto,
            @RequestParam(value = "descpuesto", required = false, defaultValue = "") String descpuesto,
            @RequestParam(value = "SAP", required = false, defaultValue = "") String sap,
            @RequestParam(value = "pais", required = false, defaultValue = "") String pais,
            @RequestParam(value = "canal", required = false, defaultValue = "") String canal,
            @RequestParam(value = "servidor", required = false, defaultValue = "") String servidor,
            @RequestParam(value = "puestobase", required = false, defaultValue = "") String puestobase)
            throws ServletException, IOException {

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<FormArchiverosDTO> lista = formArchiverosBI.obtieneFormCeco(ceco);

        boolean flag = true;

        for (FormArchiverosDTO form : lista) {
            if (form.getIdStatus() == 1 || form.getIdStatus() == 2) {
                flag = false;
                break;
            }
        }

        String salida = "";
        if (flag) {
            salida = "altaArchiveros";
        } else {
            salida = "consultaArchiveros";
        }

        ModelAndView mv = new ModelAndView(salida, "command", new FormArchiverosDTO());

        mv.addObject("list", lista);

        if (usuario != "vacio" && request.getRequestURI().contains("tienda/altaArchiveros.htm")) {
            // Creo un Usuario ADN para almacenar la informacion
            UsuarioADN userADN = new UsuarioADN();
            userADN.setIdUsuario(usuario);
            //logger.info("Id Usuario--> " + userADN.getIdUsuario());
            userADN.setPassword(UtilString.cleanParameter(password));
            //logger.info("Password--> " + userADN.getPassword());
            userADN.setNumempleado(UtilString.cleanParameter(numempleado));
            //logger.info("Numero Empleado--> " + userADN.getNumempleado());
            userADN.setWs(UtilString.cleanParameter(ws));
            //logger.info("WS--> " + userADN.getWs());
            userADN.setNombre(UtilString.cleanParameter(nombre));
            //logger.info("Nombre--> " + userADN.getNombre());
            userADN.setLlaveMaestra(UtilString.cleanParameter(null));
            //logger.info("Llave Maestra--> " + userADN.getLlaveMaestra());
            userADN.setApellidop(UtilString.cleanParameter(apellidop));
            //logger.info("Apellidop--> " + userADN.getApellidop());
            userADN.setApellidom(UtilString.cleanParameter(apellidom));
            //logger.info("Apellidom--> " + userADN.getApellidom());
            userADN.setSucursal(UtilString.cleanParameter(sucursal));
            //logger.info("Sucursal --> " + userADN.getSucursal());
            userADN.setNomsucursal(UtilString.cleanParameter(nomsucursal));
            //logger.info("NomSucursal--> " + userADN.getNomsucursal());
            userADN.setPuesto(UtilString.cleanParameter(puesto));
            //logger.info("Puesto--> " + userADN.getPuesto());
            userADN.setDescpuesto(UtilString.cleanParameter(descpuesto));
            //logger.info("DescPuesto--> " + userADN.getDescpuesto());
            userADN.setSap(UtilString.cleanParameter(sap));
            //logger.info("SAP--> " + userADN.getSap());
            userADN.setPais(UtilString.cleanParameter(pais));
            //logger.info("Pais--> " + userADN.getPais());
            userADN.setCanal(UtilString.cleanParameter(canal));
            //logger.info("Canal--> " + userADN.getCanal());
            userADN.setServidor(UtilString.cleanParameter(servidor));
            //logger.info("Servidor--> " + userADN.getServidor());
            userADN.setPuestobase(UtilString.cleanParameter(puestobase));
            //logger.info("Puesto Base--> " + userADN.getPuestobase());

            if (numempleado.contains("T")) {
                numempleado = numempleado.replaceAll("T", "");

            }
            request.getSession().setAttribute("numempleado", numempleado);
            request.getSession().setAttribute("sucursal", sucursal);
            request.getSession().setAttribute("nombre", nombre);
            request.getSession().setAttribute("nomsucursal", nomsucursal);

            System.out.println("**************NO. Emp: " + numempleado);

            if (puestobase.contains("653") || puestobase.contains("652") || puesto.contains("653") || puesto.contains("652")) {
                request.getSession().setAttribute("puesto", "652");
            } else {
                request.getSession().setAttribute("puesto", puesto);
            }

            mv.addObject("objUserADN", sesionADN(request, userADN));

        }

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("HH:mm");
        System.out.println("Hora: " + hourFormat.format(date));
        //Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: " + dateFormat.format(date));
        //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        System.out.println("Hora y fecha: " + hourdateFormat.format(date));

        mv.addObject("hora", "" + hourFormat.format(date));
        mv.addObject("fecha", "" + hourdateFormat.format(date));

        if (flag) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("list", lista);
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/altaArchiveros.htm", method = RequestMethod.POST)
    public ModelAndView postcontrollerArchiveros(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "PlacaBD", required = true, defaultValue = "") String placa,
            @RequestParam(value = "MarcaBD", required = true, defaultValue = "") String marca,
            @RequestParam(value = "DescripcionBD", required = true, defaultValue = "") String descripcion,
            @RequestParam(value = "ObservacionesBD", required = true, defaultValue = "") String observaciones,
            @RequestParam(value = "numGerente", required = true, defaultValue = "") String idUsuario,
            @RequestParam(value = "nomGerente", required = true, defaultValue = "") String nomUsuario,
            //@RequestParam(value = "numeroUsr", required = true, defaultValue = "") String idRecibe,
            @RequestParam(value = "nombreUsr", required = true, defaultValue = "") String nomRecibe,
            @RequestParam(value = "idSucursal", required = true, defaultValue = "") String idSucursal,
            @RequestParam(value = "nomSucursal", required = true, defaultValue = "") String nomSucursal,
            @RequestParam(value = "fecha", required = true, defaultValue = "") String fecha,
            @RequestParam(value = "usuario", required = false, defaultValue = "") String usuario,
            //@RequestParam(value = "usuario2", required = false, defaultValue = "") String usuario2,
            @RequestParam(value = "usr", required = false, defaultValue = "") String usr,
            //@RequestParam(value = "usr2", required = false, defaultValue = "") String usr2,
            @RequestParam(value = "token1", required = false, defaultValue = "") String token,
            //@RequestParam(value = "token2", required = false, defaultValue = "") String token2,
            @RequestParam(value = "tok", required = false, defaultValue = "") String tok,
            @RequestParam(value = "idArchivero", required = false, defaultValue = "") String idArchivero,
            @RequestParam(value = "impreso", required = false, defaultValue = "") String impreso,
            @RequestParam(value = "idFormato", required = false, defaultValue = "") String idFormatoS)
            throws ServletException, IOException {

        String salida = "altaArchiveros";
        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
        List<FormArchiverosDTO> lista2 = formArchiverosBI.obtieneFormCeco(ceco);
        if (idFormatoS.equals("")) {
            idFormatoS = "0";
        }
        int idFormato = Integer.parseInt(idFormatoS);
        int estatus = 0;
        boolean bandera = false;
        for (FormArchiverosDTO aux : lista2) {
            if (aux.getIdStatus() != 3) {
                idFormato = aux.getIdFromArchivero();
                estatus = aux.getIdStatus();
                bandera = true;
                break;
            }
        }

        ModelAndView mv = new ModelAndView(salida, "command", new FormArchiverosDTO());
        System.out.println("usuario--> '" + usuario + "'");
        //System.out.println("usuario2--> '" + usuario2+"'");
        System.out.println("usr--> '" + usr + "'");
        System.out.println("token--> '" + token + "'");

        int numExpediente1 = 0;
        boolean archAux = false;
        int numForm = 0;
        int formAux = 0;

        if (!usr.equals("") && !token.equals("")) {
            if (tok.equals("")) {
                if (idFormato == 0) {
                    //logger.info("Paso2");
                    try {
                        //logger.info("Paso");
                        ExpedientesDTO exped = new ExpedientesDTO();
                        //ArchiverosDTO archiveros= new ArchiverosDTO();

                        FormArchiverosDTO form = new FormArchiverosDTO();
                        form.setIdFromArchivero(0);

                        form.setIdUsuario(Integer.parseInt(idUsuario));
                        form.setNombre(nomUsuario);
                        form.setIdCeco(idSucursal);
                        form.setIdRecibe(0);
                        form.setNomRecibe(nomRecibe);
                        form.setIdStatus(2);
                        form.setFecha(fecha);
                        formAux = formArchiverosBI.insertaFormArchiveros(form);
                        logger.info("Insertando formArchivero.... " + formAux);

                        String sPlaca[] = placa.split("&");
                        String sMarca[] = marca.split("&");
                        String sDescripcion[] = descripcion.split("&");
                        String sObservaciones[] = observaciones.split("&");

                        for (int i = 0; i < placa.split("&").length; i++) {
                            ArchiveroDTO archiveros = new ArchiveroDTO();
                            archiveros.setIdArchivero(0);
                            archiveros.setPlaca(sPlaca[i]);
                            archiveros.setMarca(sMarca[i]);
                            archiveros.setDescripcion(sDescripcion[i]);
                            archiveros.setObservaciones(sObservaciones[i]);
                            archiveros.setIdFormArchivero(formAux); //formAux en lugar de 1
                            archAux = archiveroBI.insertaArchivero(archiveros);
                            logger.info("Insertando archivero.... " + archAux);

                        }

                        List<ArchiveroDTO> list = archiveroBI.obtieneArchiveroFormato(formAux);
                        mv.addObject("paso", "no2");
                        mv.addObject("list", list);
                        mv.addObject("nomRecibe", nomRecibe);
                        mv.addObject("idFormato", formAux);

                        mv.addObject("tok", "OK");
                    } catch (Exception e) {
                        logger.info("Algo paso: " + e);

                        mv.addObject("paso", "error");
                    }
                } else {
                    try {
                        //logger.info("Paso");

                        List<FormArchiverosDTO> lista = formArchiverosBI.obtieneFormCeco(ceco);
                        idFormato = Integer.parseInt(idFormatoS);
                        estatus = 0;
                        nomRecibe = "";
                        for (FormArchiverosDTO aux : lista) {
                            if (aux.getIdFromArchivero() == idFormato) {
                                aux.setIdStatus(2);
                                logger.info("Formato modificado: " + formArchiverosBI.modificaFormArchiveros(aux));
                                nomRecibe = aux.getNomRecibe();
                                break;
                            }
                        }

                        List<ArchiveroDTO> list = archiveroBI.obtieneArchiveroFormato(idFormato);
                        mv.addObject("paso", "no2");
                        mv.addObject("list", list);
                        mv.addObject("nomRecibe", nomRecibe);
                        mv.addObject("idFormato", idFormato);

                        mv.addObject("tok", "OK");

                        //logger.info("Paso");
                    } catch (Exception e) {
                        logger.info("Algo paso: " + e);
                        mv.addObject("paso", "error");
                    }
                }
            } else {
                //logger.info("Paso2");
                try {
                    //logger.info("Paso");
                    ExpedientesDTO exped = new ExpedientesDTO();
                    //ArchiverosDTO archiveros= new ArchiverosDTO();

                    FormArchiverosDTO form = new FormArchiverosDTO();
                    form.setIdFromArchivero(0);

                    form.setIdUsuario(Integer.parseInt(idUsuario));
                    form.setNombre(nomUsuario);
                    form.setIdCeco(idSucursal);
                    form.setIdRecibe(0);
                    form.setNomRecibe(nomRecibe);
                    form.setIdStatus(2);
                    form.setFecha(fecha);
                    formAux = formArchiverosBI.insertaFormArchiveros(form);
                    logger.info("Insertando formArchivero.... " + formAux);

                    String sPlaca[] = placa.split("&");
                    String sMarca[] = marca.split("&");
                    String sDescripcion[] = descripcion.split("&");
                    String sObservaciones[] = observaciones.split("&");

                    for (int i = 0; i < placa.split("&").length; i++) {
                        ArchiveroDTO archiveros = new ArchiveroDTO();
                        archiveros.setIdArchivero(0);
                        archiveros.setPlaca(sPlaca[i]);
                        archiveros.setMarca(sMarca[i]);
                        archiveros.setDescripcion(sDescripcion[i]);
                        archiveros.setObservaciones(sObservaciones[i]);
                        archiveros.setIdFormArchivero(formAux); //formAux en lugar de 1
                        archAux = archiveroBI.insertaArchivero(archiveros);
                        logger.info("Insertando archivero.... " + archAux);

                    }

                    List<ArchiveroDTO> list = archiveroBI.obtieneArchiveroFormato(formAux);
                    mv.addObject("paso", "impreso");
                    mv.addObject("list", list);
                    mv.addObject("nomRecibe", nomRecibe);
                    mv.addObject("idFormato", formAux);

                    mv.addObject("tok", "OK");
                } catch (Exception e) {
                    logger.info("Algo paso: " + e);

                    mv.addObject("paso", "error");
                }
            }

        } else {
            if (tok.equals("")) {
                try {
                    if (impreso.equals("")) {
                        //logger.info("Paso2");

                        ExpedientesDTO exped = new ExpedientesDTO();
                        //ArchiverosDTO archiveros= new ArchiverosDTO();

                        FormArchiverosDTO form = new FormArchiverosDTO();
                        form.setIdFromArchivero(0);

                        form.setIdUsuario(Integer.parseInt(idUsuario));
                        form.setNombre(nomUsuario);
                        form.setIdCeco(idSucursal);
                        form.setIdRecibe(0);
                        form.setNomRecibe(nomRecibe);
                        form.setIdStatus(1);
                        form.setFecha(fecha);
                        formAux = formArchiverosBI.insertaFormArchiveros(form);
                        logger.info("Insertando formArchivero.... " + formAux);

                        String sPlaca[] = placa.split("&");
                        String sMarca[] = marca.split("&");
                        String sDescripcion[] = descripcion.split("&");
                        String sObservaciones[] = observaciones.split("&");

                        for (int i = 0; i < placa.split("&").length; i++) {
                            ArchiveroDTO archiveros = new ArchiveroDTO();
                            archiveros.setIdArchivero(0);
                            archiveros.setPlaca(sPlaca[i]);
                            archiveros.setMarca(sMarca[i]);
                            archiveros.setDescripcion(sDescripcion[i]);
                            archiveros.setObservaciones(sObservaciones[i]);
                            archiveros.setIdFormArchivero(formAux); //formAux en lugar de 1
                            archAux = archiveroBI.insertaArchivero(archiveros);
                            logger.info("Insertando archivero.... " + archAux);

                        }

                        List<ArchiveroDTO> list = archiveroBI.obtieneArchiveroFormato(formAux);
                        mv.addObject("paso", "no3");
                        mv.addObject("list", list);
                        mv.addObject("nomRecibe", nomRecibe);
                        mv.addObject("idFormato", formAux);

                        mv.addObject("tok", "");
                        //logger.info("PAso2");
                    } else {
                        mv.addObject("paso", "no4");
                    }
                } catch (Exception e) {
                    logger.info("Algo paso: " + e);

                    mv.addObject("paso", "error");
                }
            } else {
                ////logger.info("Paso2");
                try {
                    //logger.info("Paso");

                    List<FormArchiverosDTO> lista = formArchiverosBI.obtieneFormCeco(ceco);
                    idFormato = Integer.parseInt(idFormatoS);
                    estatus = 0;
                    nomRecibe = "";
                    for (FormArchiverosDTO aux : lista) {
                        if (aux.getIdFromArchivero() == idFormato) {
                            aux.setIdStatus(3);
                            logger.info("Formato modificado: " + formArchiverosBI.modificaFormArchiveros(aux));
                            nomRecibe = aux.getNomRecibe();
                            break;
                        }
                    }

                    List<ArchiveroDTO> list = archiveroBI.obtieneArchiveroFormato(idFormato);
                    mv.addObject("paso", "no4");
                    mv.addObject("list", list);
                    mv.addObject("nomRecibe", nomRecibe);
                    mv.addObject("idFormato", idFormato);

                    mv.addObject("tok", "OK");

                    //logger.info("Paso");
                } catch (Exception e) {
                    logger.info("Algo paso: " + e);
                    mv.addObject("paso", "error");
                }
            }
        }

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("HH:mm");
        System.out.println("Hora: " + hourFormat.format(date));
        //Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: " + dateFormat.format(date));
        //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        System.out.println("Hora y fecha: " + hourdateFormat.format(date));

        mv.addObject("hora", "" + hourFormat.format(date));
        mv.addObject("fecha", "" + hourdateFormat.format(date));

        return mv;
    }

    @RequestMapping(value = "central/consultaArchiveros.htm", method = RequestMethod.GET)
    public ModelAndView getconsultaArchiveros(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "consultaArchiveros";
        ModelAndView mv = new ModelAndView(salida, "command", new FormArchiverosDTO());

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<FormArchiverosDTO> lista = formArchiverosBI.obtieneFormCeco(ceco);
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = "central/recuperaArchivero.htm", method = RequestMethod.POST)
    public ModelAndView postconsultaArchiveros(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idExpediente", required = true, defaultValue = "") String idExpediente)
            throws ServletException, IOException {

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<FormArchiverosDTO> lista = formArchiverosBI.obtieneFormCeco(ceco);
        int idFormato = Integer.parseInt(idExpediente);
        int estatus = 0;
        String nomRecibe = "";
        for (FormArchiverosDTO aux : lista) {
            if (aux.getIdFromArchivero() == idFormato) {
                estatus = aux.getIdStatus();
                nomRecibe = aux.getNomRecibe();
                break;
            }
        }
        String salida = "";
        boolean imp = false;
        if (estatus == 3) {
            salida = "consultaArchiveros";
        } else if (estatus == 2) {
            salida = "altaArchiveros";
            imp = true;
        } else {
            salida = "altaArchiveros";

        }
        ModelAndView mv = new ModelAndView(salida, "command", new FormArchiverosDTO());

        if (estatus == 3) {//Impreso
            List<ArchiveroDTO> list = archiveroBI.obtieneArchiveroFormato(idFormato);
            mv.addObject("paso", "impreso");
            mv.addObject("list", list);
            mv.addObject("nomRecibe", nomRecibe);
            mv.addObject("idFormato", idFormato);
        } else if (estatus == 2) {//Firmado
            List<ArchiveroDTO> list = archiveroBI.obtieneArchiveroFormato(idFormato);
            mv.addObject("paso", "no2");
            mv.addObject("list", list);
            mv.addObject("nomRecibe", nomRecibe);
            mv.addObject("idFormato", idFormato);
            mv.addObject("tok", "OK");
        } else {//Lleno
            List<ArchiveroDTO> list = archiveroBI.obtieneArchiveroFormato(idFormato);
            mv.addObject("paso", "no3");
            mv.addObject("list", list);
            mv.addObject("nomRecibe", nomRecibe);
            mv.addObject("idFormato", idFormato);
        }

        return mv;

    }

    @RequestMapping(value = {"central/expedientesCreditoActivos.htm", "central/expedientesCreditoCancelados.htm",
        "central/expedientesCaptacionActivos.htm", "central/expedientesCaptacionCancelados.htm"}, method = RequestMethod.GET)
    public ModelAndView expedientesCredito(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "usuario", required = false, defaultValue = "vacio") String usuario,
            @RequestParam(value = "password", required = false, defaultValue = "") String password,
            @RequestParam(value = "numempleado", required = false, defaultValue = "") String numempleado,
            @RequestParam(value = "ws", required = false, defaultValue = "") String ws,
            @RequestParam(value = "nombre", required = false, defaultValue = "") String nombre,
            @RequestParam(value = "apellidop", required = false, defaultValue = "") String apellidop,
            @RequestParam(value = "apellidom", required = false, defaultValue = "") String apellidom,
            @RequestParam(value = "sucursal", required = false, defaultValue = "") String sucursal,
            @RequestParam(value = "nomsucursal", required = false, defaultValue = "") String nomsucursal,
            @RequestParam(value = "puesto", required = false, defaultValue = "") String puesto,
            @RequestParam(value = "descpuesto", required = false, defaultValue = "") String descpuesto,
            @RequestParam(value = "SAP", required = false, defaultValue = "") String sap,
            @RequestParam(value = "pais", required = false, defaultValue = "") String pais,
            @RequestParam(value = "canal", required = false, defaultValue = "") String canal,
            @RequestParam(value = "servidor", required = false, defaultValue = "") String servidor,
            @RequestParam(value = "puestobase", required = false, defaultValue = "") String puestobase)
            throws ServletException, IOException {

        String tipoActa = "";

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        System.out.println("LA URL CONTIENE " + request.getRequestURI());

        if (request.getRequestURI().contains("CreditoActivos")) {
            tipoActa = "2";
        } else if (request.getRequestURI().contains("CreditoCancelados")) {
            tipoActa = "3";
        } else if (request.getRequestURI().contains("CaptacionActivos")) {
            tipoActa = "4";
        } else if (request.getRequestURI().contains("CaptacionCancelados")) {
            tipoActa = "5";
        } else {
            tipoActa = "";
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, tipoActa);

        boolean flag = true;

        for (ExpedientesDTO exp : lista) {
            if (exp.getEstatusExpediente() == 1 || exp.getEstatusExpediente() == 2) {
                flag = false;
                break;
            }
        }
        String salida = "";

        String modelNameAlta = "";
        String modelNameConsulta = "";

        switch (tipoActa) {
            case "2":
                modelNameAlta = "expedCreditoActivos";
                modelNameConsulta = "consultaExpeCreditoActivas";
                break;
            case "3":
                modelNameAlta = "expedCreditoCancelados";
                modelNameConsulta = "consultaExpeCreditoCanceladas";
                break;
            case "4":
                modelNameAlta = "expedCaptacionActivos";
                modelNameConsulta = "consultaExpeCaptacionActivas";
                break;
            case "5":
                modelNameAlta = "expedCaptacionCancelados";
                modelNameConsulta = "consultaExpeCaptacionCanceladas";
                break;

            default:
                break;
        }

        if (flag) {
            //salida = "expedInactivos";
            salida = modelNameAlta;
        } else {
            salida = modelNameConsulta;
        }
        ModelAndView mv = new ModelAndView(salida, "command", new ExpedientesDTO());

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("HH:mm");
        System.out.println("Hora: " + hourFormat.format(date));
        //Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: " + dateFormat.format(date));
        //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        System.out.println("Hora y fecha: " + hourdateFormat.format(date));

        mv.addObject("hora", "" + hourFormat.format(date));
        mv.addObject("fecha", "" + hourdateFormat.format(date));
        if (flag) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("list", lista);
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = {"central/consultaExpeCreditoActivas.htm",
        "central/consultaExpeCreditoCanceladas.htm", "central/consultaExpeCaptacionActivas.htm",
        "central/consultaExpeCaptacionCanceladas.htm"}, method = RequestMethod.GET)
    public ModelAndView getconsultaActivosCancelados(HttpServletRequest request, HttpServletResponse response, Model model
    )
            throws ServletException, IOException {

        String salida = "";
        String tipoActa = "";

        if (request.getRequestURI().contains("CreditoActivas")) {
            salida = "consultaExpeCreditoActivas";
            tipoActa = "2";
        } else if (request.getRequestURI().contains("CreditoCanceladas")) {
            salida = "consultaExpeCreditoCanceladas";
            tipoActa = "3";
        } else if (request.getRequestURI().contains("CaptacionActivas")) {
            salida = "consultaExpeCaptacionActivas";
            tipoActa = "4";
        } else if (request.getRequestURI().contains("CaptacionCanceladas")) {
            salida = "consultaExpeCaptacionCanceladas";
            tipoActa = "5";
        } else {
            salida = "";
            tipoActa = "";
        }

        ModelAndView mv = new ModelAndView(salida, "command", new ExpedientesDTO());

        String ceco = "" + request.getSession().getAttribute("sucursal");

        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(null, null, ceco, null, null, null, tipoActa);

        //logger.info("Numero de sucursal "+request.getSession().getAttribute("sucursal"));
        mv.addObject("list", lista);
        if (lista.isEmpty()) {
            mv.addObject("paso", "no");
        } else {
            mv.addObject("paso", "cons1");
        }

        return mv;
    }

    @RequestMapping(value = {"central/expedientesCreditoActivos.htm", "central/expedientesCreditoCancelados.htm",
        "central/expedientesCaptacionActivos.htm", "central/expedientesCaptacionCancelados.htm"}, method = RequestMethod.POST)
    public ModelAndView expedientesCreditoPost(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "ciudad", required = true, defaultValue = "") String ciudad,
            @RequestParam(value = "nomRegional", required = true, defaultValue = "") String nomRegional,
            @RequestParam(value = "numRegional", required = true, defaultValue = "") String numRegional,
            @RequestParam(value = "descripcion", required = true, defaultValue = "") String descripcion,
            @RequestParam(value = "idSucursal", required = true, defaultValue = "") String idSucursal,
            @RequestParam(value = "nomSucursal", required = true, defaultValue = "") String nomSucursal,
            @RequestParam(value = "fecha", required = true, defaultValue = "") String fecha,
            @RequestParam(value = "nomGerente", required = true, defaultValue = "") String nomGerente,
            @RequestParam(value = "numGerente", required = true, defaultValue = "") String numGerente,
            @RequestParam(value = "usuario", required = false, defaultValue = "") String usuario,
            @RequestParam(value = "usuario2", required = false, defaultValue = "") String usuario2,
            @RequestParam(value = "usuario3", required = false, defaultValue = "") String usuario3,
            @RequestParam(value = "usr", required = false, defaultValue = "") String usr,
            @RequestParam(value = "usr2", required = false, defaultValue = "") String usr2,
            @RequestParam(value = "usr3", required = false, defaultValue = "") String usr3,
            @RequestParam(value = "token1", required = false, defaultValue = "") String token1,
            @RequestParam(value = "token2", required = false, defaultValue = "") String token2,
            @RequestParam(value = "token3", required = false, defaultValue = "") String token3,
            @RequestParam(value = "tok", required = false, defaultValue = "") String tok,
            @RequestParam(value = "numExpediente", required = false, defaultValue = "") String numExpediente,
            @RequestParam(value = "impreso", required = false, defaultValue = "") String impreso
    )
            throws ServletException, IOException {

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }

        System.out.println("LA URL CONTIENE " + request.getRequestURI());

        int tipoActa = 0;

        if (request.getRequestURI().contains("CreditoActivos")) {
            tipoActa = 2;
        } else if (request.getRequestURI().contains("CreditoCancelados")) {
            tipoActa = 3;
        } else if (request.getRequestURI().contains("CaptacionActivos")) {
            tipoActa = 4;
        } else if (request.getRequestURI().contains("CaptacionCancelados")) {
            tipoActa = 5;
        }

        String modelNameAlta = "";

        switch (tipoActa) {
            case 2:
                modelNameAlta = "expedCreditoActivos";

                break;
            case 3:
                modelNameAlta = "expedCreditoCancelados";

                break;
            case 4:
                modelNameAlta = "expedCaptacionActivos";

                break;
            case 5:
                modelNameAlta = "expedCaptacionCancelados";
                break;

            default:
                modelNameAlta = "";
                break;
        }

        String salida = modelNameAlta;
        ModelAndView mv = new ModelAndView(salida, "command", new ExpedientesDTO());

        logger.info("Nombre--> " + nomGerente);
        logger.info("usuario2--> '" + usuario2 + "'");
        logger.info("usuario3--> '" + usuario3 + "'");
        logger.info("usr--> '" + usr + "'");
        logger.info("usr2--> '" + usr2 + "'");
        logger.info("usr3--> '" + usr3 + "'");
        logger.info("descripcion--> " + descripcion);
        logger.info("token1--> " + token1);
        logger.info("token2--> " + token2);
        logger.info("token3--> " + token3);
        int numExpediente1 = 0;

        if (numGerente.contains("T")) {
            numGerente = numGerente.replaceAll("T", "");

        }

        if (!usr.equals("") && !usr2.equals("") && !token1.equals("") && !token2.equals("")) {
            if (tok.equals("")) {
                try {
                    //logger.info("Paso");
                    List<Usuario_ADTO> lista = usuarioabi.obtieneUsuario(Integer.parseInt(usr2));

                    //logger.info("Paso");
                    //List<Usuario_ADTO> lista2 = usuarioabi.obtieneUsuario(Integer.parseInt(usr3));
                    //logger.info("Paso");
                    String nomusuario2 = "";
                    //String nomusuario3="";

                    for (Usuario_ADTO x : lista) {
                        nomusuario2 = x.getNombre();
                        mv.addObject("nomusuario2", x.getNombre());
                        break;
                    }
                    /*
				for(Usuario_ADTO x2:lista2){
					nomusuario3=x2.getNombre();
					mv.addObject("nomusuario3", x2.getNombre());
					break;
				}
                     */
                    //logger.info("Paso");

                    ExpedientesDTO exped = new ExpedientesDTO();

                    //logger.info("Paso");
                    exped.setIdGerente(Integer.parseInt(numGerente));
                    exped.setIdCeco(idSucursal);
                    exped.setFecha(fecha);
                    exped.setEstatusExpediente(2);
                    exped.setDescripcion(descripcion);
                    exped.setCiudad(ciudad);
                    //logger.info("Paso");
                    numExpediente1 = InsertExped.insertaExpediente(exped, tipoActa);
                    logger.info("Insertando expediente.... " + numExpediente1);

                    exped.setIdExpediente(numExpediente1);

                    exped.setIdentificador(Integer.parseInt(numRegional));
                    exped.setNombreResp(nomRegional);
                    exped.setEstatusResp(1);
                    //logger.info("Paso");
                    logger.info("Insertando Regional.... " + InsertExped.insertaResponsable(exped));

                    exped.setIdentificador(Integer.parseInt(usr2));
                    exped.setNombreResp(nomusuario2);
                    exped.setEstatusResp(3);
                    //logger.info("Paso");
                    logger.info("Insertando respondable1.... " + InsertExped.insertaResponsable(exped));

                    /*
				exped.setIdentificador(Integer.parseInt(usr3));
				exped.setNombreResp(nomusuario3);
				exped.setEstatusResp(3);

				logger.info("Insertando respondable2.... "+ InsertExped.insertaResponsable(exped));
                     */
                    exped.setIdentificador(Integer.parseInt(numGerente));
                    exped.setNombreResp(nomGerente);
                    exped.setEstatusResp(4);

                    logger.info("Insertando Gerente.... " + InsertExped.insertaResponsable(exped));

                    mv.addObject("ciudad", ciudad);
                    mv.addObject("nomRegional", nomRegional);
                    mv.addObject("numRegional", numRegional);
                    mv.addObject("descripcion", descripcion);
                    mv.addObject("idSucursal", idSucursal);
                    mv.addObject("nomSucursal", nomSucursal);
                    mv.addObject("fecha", fecha);
                    mv.addObject("nomGerente", nomGerente);
                    mv.addObject("numGerente", numGerente);
                    mv.addObject("usuario", usuario);
                    mv.addObject("usuario2", usuario2);

                    mv.addObject("numExpediente", numExpediente1);

                    mv.addObject("usuario3", usuario3);

                    mv.addObject("paso", "no2");
                    //logger.info("Paso");
                } catch (Exception e) {
                    logger.info("Algo paso: " + e);
                    if (numExpediente1 != 0) {
                        InsertExped.eliminaResponsables(numExpediente1);
                        InsertExped.eliminaExpediente(numExpediente1);
                    }

                    mv.addObject("paso", "error");
                }
            } else {
                try {
                    //logger.info("Paso");
                    List<Usuario_ADTO> lista = usuarioabi.obtieneUsuario(Integer.parseInt(usr2));

                    //logger.info("Paso");
                    //List<Usuario_ADTO> lista2 = usuarioabi.obtieneUsuario(Integer.parseInt(usr3));
                    //logger.info("Paso");
                    String nomusuario2 = "";
                    //String nomusuario3="";

                    for (Usuario_ADTO x : lista) {
                        nomusuario2 = x.getNombre();
                        mv.addObject("nomusuario2", x.getNombre());
                        break;
                    }
                    /*
						for(Usuario_ADTO x2:lista2){
							nomusuario3=x2.getNombre();
							mv.addObject("nomusuario3", x2.getNombre());
							break;
						}
                     */
                    //logger.info("Paso");

                    ExpedientesDTO exped = new ExpedientesDTO();

                    logger.info("Insertando audiror.... " + InsertExped.actualizaEstatus(2, Integer.parseInt(numExpediente)));

                    //logger.info("Paso");
                    exped.setIdExpediente(Integer.parseInt(numExpediente));

                    exped.setIdGerente(Integer.parseInt(numGerente));
                    exped.setIdCeco(idSucursal);
                    exped.setFecha(fecha);
                    exped.setEstatusExpediente(2);
                    exped.setDescripcion(descripcion);
                    exped.setCiudad(ciudad);

                    exped.setIdentificador(Integer.parseInt(usr2));
                    exped.setNombreResp(nomusuario2);
                    exped.setEstatusResp(3);
                    //logger.info("Paso");
                    logger.info("Insertando respondable1.... " + InsertExped.insertaResponsable(exped));

                    /*
						exped.setIdentificador(Integer.parseInt(usr3));
						exped.setNombreResp(nomusuario3);
						exped.setEstatusResp(3);

						logger.info("Insertando respondable2.... "+ InsertExped.insertaResponsable(exped));
                     */
                    mv.addObject("ciudad", ciudad);
                    mv.addObject("nomRegional", nomRegional);
                    mv.addObject("numRegional", numRegional);
                    mv.addObject("descripcion", descripcion);
                    mv.addObject("idSucursal", idSucursal);
                    mv.addObject("nomSucursal", nomSucursal);
                    mv.addObject("fecha", fecha);
                    mv.addObject("nomGerente", nomGerente);
                    mv.addObject("numGerente", numGerente);
                    mv.addObject("usuario", usuario);
                    mv.addObject("usuario2", usuario2);

                    mv.addObject("numExpediente", numExpediente);

                    mv.addObject("usuario3", usuario3);

                    mv.addObject("paso", "no2");
                    //logger.info("Paso");
                } catch (Exception e) {
                    logger.info("Algo paso: " + e);
                    if (Integer.parseInt(numExpediente) != 0) {
                        InsertExped.eliminaResponsables(Integer.parseInt(numExpediente));
                        InsertExped.eliminaExpediente(Integer.parseInt(numExpediente));
                    }

                    mv.addObject("paso", "error");
                }
            }

        } else {
            try {
                if (impreso.equals("")) {
                    //logger.info("Paso2");

                    ExpedientesDTO exped = new ExpedientesDTO();

                    //logger.info("Paso2");
                    exped.setIdGerente(Integer.parseInt(numGerente));
                    exped.setIdCeco(idSucursal);
                    exped.setFecha(fecha);
                    exped.setEstatusExpediente(1);
                    exped.setDescripcion(descripcion);
                    exped.setCiudad(ciudad);
                    numExpediente1 = InsertExped.insertaExpediente(exped, tipoActa);
                    logger.info("Insertando expediente.... " + numExpediente1);

                    exped.setIdExpediente(numExpediente1);

                    exped.setIdentificador(Integer.parseInt(numRegional));
                    exped.setNombreResp(nomRegional);
                    exped.setEstatusResp(1);
                    logger.info("Insertando Regional.... " + InsertExped.insertaResponsable(exped));

                    exped.setIdentificador(Integer.parseInt(numGerente));
                    exped.setNombreResp(nomGerente);
                    exped.setEstatusResp(4);

                    logger.info("Insertando Gerente.... " + InsertExped.insertaResponsable(exped));

                    mv.addObject("ciudad", ciudad);
                    mv.addObject("nomRegional", nomRegional);
                    mv.addObject("numRegional", numRegional);
                    mv.addObject("descripcion", descripcion);
                    mv.addObject("idSucursal", idSucursal);
                    mv.addObject("nomSucursal", nomSucursal);
                    mv.addObject("fecha", fecha);
                    mv.addObject("nomGerente", nomGerente);
                    mv.addObject("numGerente", numGerente);
                    mv.addObject("usuario", usuario);
                    mv.addObject("usuario2", usuario2);

                    mv.addObject("numExpediente", numExpediente1);

                    mv.addObject("usuario3", usuario3);

                    mv.addObject("tok", "OK");

                    mv.addObject("paso", "no3");
                    logger.info("PAso2");
                } else {
                    logger.info("Insertando audiror.... " + InsertExped.actualizaEstatus(3, Integer.parseInt(numExpediente)));
                    mv.addObject("paso", "no4");
                }
            } catch (Exception e) {
                logger.info("Algo paso: " + e);
                if (numExpediente1 != 0) {
                    InsertExped.eliminaResponsables(numExpediente1);
                    InsertExped.eliminaExpediente(numExpediente1);
                }

                mv.addObject("paso", "error");
            }
        }

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("HH:mm");
        System.out.println("Hora: " + hourFormat.format(date));
        //Caso 2: obtener la fecha y salida por pantalla con formato:
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Fecha: " + dateFormat.format(date));
        //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
        DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        System.out.println("Hora y fecha: " + hourdateFormat.format(date));

        mv.addObject("hora", "" + hourFormat.format(date));
        mv.addObject("fecha", "" + hourdateFormat.format(date));

        return mv;
    }

    @RequestMapping(value = "central/recuperaExpedienteActivosCancelados.htm", method = RequestMethod.POST)
    public ModelAndView postconsultaActivosCancelados(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idExpediente", required = true, defaultValue = "") String idExpediente,
            @RequestParam(value = "idTipoActa", required = true, defaultValue = "") String idTipoActa)
            throws ServletException, IOException {

        String ciudad = "";
        String nomRegional = "";
        String numRegional = "";
        String descripcion = "";
        String idSucursal = "";
        String nomSucursal = "";
        String fecha = "";
        String nomGerente = "";
        String numGerente = "";
        String usuario = "";
        String usuario2 = "";
        String usuario3 = "";
        String nomusuario2 = "";
        String nomusuario3 = "";
        String usr = "";
        String usr2 = "";
        String usr3 = "";
        String token1 = "";
        String token2 = "";
        String token3 = "";
        String tok = "";
        String numExpediente = "";
        String impreso = "";
        int estatus = 0;

        String ceco = "" + request.getSession().getAttribute("sucursal");
        if (ceco.trim().length() == 4) {
            ceco = "48" + ceco.trim();
        }
        if (ceco.trim().length() == 3) {
            ceco = "480" + ceco.trim();
        }
        if (ceco.trim().length() == 2) {
            ceco = "4800" + ceco.trim();
        }
        if (ceco.trim().length() == 1) {
            ceco = "48000" + ceco.trim();
        }
        nomGerente = "" + request.getSession().getAttribute("nombre");
        numGerente = "" + request.getSession().getAttribute("numempleado");

        List<ExpedientesDTO> lista = InsertExped.buscaExpediente(idExpediente, null, null, null, null, null, idTipoActa);

        logger.info("Numero de sucursal " + request.getSession().getAttribute("sucursal"));

        String salida = "";
        boolean imp = false;

        for (ExpedientesDTO exp : lista) {
            numExpediente = "" + exp.getIdExpediente();
            ciudad = exp.getCiudad();
            descripcion = exp.getDescripcion();
            idSucursal = exp.getIdCeco();
            fecha = exp.getFecha();
            numGerente = "" + exp.getIdGerente();
            estatus = exp.getEstatusExpediente();
        }

        List<ExpedientesDTO> listaU = InsertExped.buscaResponsable(idExpediente, null, null);

        for (ExpedientesDTO exp : listaU) {
            if (exp.getEstatusResp() == 1) {
                nomRegional = exp.getNombreResp();
                numRegional = "" + exp.getIdentificador();
            }
            if (exp.getEstatusResp() == 3) {
                nomusuario2 = exp.getNombreResp();
                usuario2 = "" + exp.getIdentificador();
            }
            if (exp.getEstatusResp() == 4) {
                nomGerente = exp.getNombreResp();
                numGerente = "" + exp.getIdentificador();
            }
        }

        String nameModel = "";
        String nameModelConsulta = "";

        switch (idTipoActa) {

            case "2":
                nameModel = "expedCreditoActivos";
                nameModelConsulta = "consultaExpeCreditoActivas";
                break;
            case "3":
                nameModel = "expedCreditoCancelados";
                nameModelConsulta = "consultaExpeCreditoCanceladas";
                break;
            case "4":
                nameModel = "expedCaptacionActivos";
                nameModelConsulta = "consultaExpeCaptacionActivas";
                break;
            case "5":
                nameModel = "expedCaptacionCancelados";
                nameModelConsulta = "consultaExpeCaptacionCanceladas";
                break;

        }

        if (estatus == 3) {
            salida = nameModelConsulta;
        } else if (estatus == 2) {
            salida = nameModel;
            imp = true;
        } else {
            salida = nameModel;
        }
        //salida = "consultaExpedientes";
        ModelAndView mv = new ModelAndView(salida, "command", new ExpedientesDTO());

        String fechaSplit[] = fecha.split(" ");
        String hora = fechaSplit[1];
        String dia = fechaSplit[0].split("/")[0];
        String mes = fechaSplit[0].split("/")[1];
        String anio = fechaSplit[0].split("/")[2];

        switch (mes) {
            case "01":
                mes = "Enero";
                break;
            case "02":
                mes = "Febrero";
                break;
            case "03":
                mes = "Marzo";
                break;
            case "04":
                mes = "Abril";
                break;
            case "05":
                mes = "Mayo";
                break;
            case "06":
                mes = "Junio";
                break;
            case "07":
                mes = "Julio";
                break;
            case "08":
                mes = "Agosto";
                break;
            case "09":
                mes = "Septiembre";
                break;
            case "10":
                mes = "Octubre";
                break;
            case "11":
                mes = "Noviembre";
                break;
            case "12":
                mes = "Diciembre";
                break;

        }

        mv.addObject("ciudad", ciudad);
        mv.addObject("nomRegional", nomRegional);
        mv.addObject("numRegional", numRegional);
        mv.addObject("descripcion", descripcion);
        mv.addObject("idSucursal", idSucursal);
        mv.addObject("nomSucursal", nomSucursal);
        mv.addObject("fecha", fecha);
        mv.addObject("nomGerente", nomGerente);
        mv.addObject("numGerente", numGerente);
        mv.addObject("usuario", usuario);
        mv.addObject("usuario2", usuario2);
        mv.addObject("nomusuario2", nomusuario2);
        mv.addObject("nomusuario3", nomusuario3);

        mv.addObject("numExpediente", numExpediente);

        mv.addObject("usuario3", usuario3);

        if (estatus == 3) {
            mv.addObject("paso", "impreso");
            mv.addObject("hora", hora);
            mv.addObject("mes", mes);
            mv.addObject("dia", dia);
            mv.addObject("anio", anio);
            return mv;
        } else if (estatus == 2) {
            mv.addObject("paso", "no2");
            nomGerente = "" + request.getSession().getAttribute("nombre");
            numGerente = "" + request.getSession().getAttribute("numempleado");
            Date date = new Date();
            //Caso 1: obtener la hora y salida por pantalla con formato:
            DateFormat hourFormat = new SimpleDateFormat("HH:mm");
            System.out.println("Hora: " + hourFormat.format(date));
            //Caso 2: obtener la fecha y salida por pantalla con formato:
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println("Fecha: " + dateFormat.format(date));
            //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
            DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            System.out.println("Hora y fecha: " + hourdateFormat.format(date));

            mv.addObject("hora", "" + hourFormat.format(date));
            mv.addObject("fecha", "" + hourdateFormat.format(date));
            return mv;
        } else {
            mv.addObject("paso", "no3");
            mv.addObject("tok", "OK");
            nomGerente = "" + request.getSession().getAttribute("nombre");
            numGerente = "" + request.getSession().getAttribute("numempleado");
            Date date = new Date();
            //Caso 1: obtener la hora y salida por pantalla con formato:
            DateFormat hourFormat = new SimpleDateFormat("HH:mm");
            System.out.println("Hora: " + hourFormat.format(date));
            //Caso 2: obtener la fecha y salida por pantalla con formato:
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println("Fecha: " + dateFormat.format(date));
            //Caso 3: obtenerhora y fecha y salida por pantalla con formato:
            DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            System.out.println("Hora y fecha: " + hourdateFormat.format(date));

            mv.addObject("hora", "" + hourFormat.format(date));
            mv.addObject("fecha", "" + hourdateFormat.format(date));
            return mv;
        }

    }

    @RequestMapping(value = {"central/pedestal.html",}, method = RequestMethod.GET)
    public ModelAndView pedestal(HttpServletRequest request, Exception ex) {
        ModelAndView mv = new ModelAndView("pedestal_pwa/index_pedestal");
        return mv;
    }

    @RequestMapping(value = {"central/menu.html",}, method = RequestMethod.GET)
    public ModelAndView pedestal_menu(HttpServletRequest request, Exception ex) {
        ModelAndView mv = new ModelAndView("pedestal_pwa/menu_categoria");
        return mv;
    }

    @RequestMapping(value = {"central/algoPasoPedestal.html",}, method = RequestMethod.GET)
    public ModelAndView algoPaso(HttpServletRequest request, Exception ex) {
        ModelAndView mv = new ModelAndView("pedestal_pwa/algoPaso");
        return mv;
    }

    @RequestMapping(value = {"central/loginPedestal.html",}, method = RequestMethod.GET)
    public ModelAndView loginPedestal(HttpServletRequest request, Exception ex) {
        ModelAndView mv = new ModelAndView("pedestal_pwa/login");
        return mv;
    }

    @RequestMapping(value = {"central/datosSucursal.html",}, method = RequestMethod.GET)
    public ModelAndView datosSucursal(HttpServletRequest request, Exception ex) {
        ModelAndView mv = new ModelAndView("pedestal_pwa/datos_de_la_sucursal");
        return mv;
    }
}
