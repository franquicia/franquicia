package com.gruposalinas.franquicia.controller;

import com.google.gson.Gson;
import com.gruposalinas.franquicia.business.EstatusGeneralBI;
import com.gruposalinas.franquicia.business.LoginBI;
import com.gruposalinas.franquicia.business.PedestalDigitalBI;
import com.gruposalinas.franquicia.domain.AdminNegocioPDDTO;
import com.gruposalinas.franquicia.domain.IndicadoresDataEGDTO;
import com.gruposalinas.franquicia.domain.Login;
import com.gruposalinas.franquicia.domain.LoginDTO;
import com.gruposalinas.franquicia.domain.LoginPDDTO;
import com.gruposalinas.franquicia.domain.MenuPedestalDigital;
import com.gruposalinas.franquicia.domain.NegocioDTO;
import com.gruposalinas.franquicia.domain.ParamNegocioPDDTO;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import com.gruposalinas.franquicia.util.UtilFRQ;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/central/pedestalDigital/")
public class LoginPDController {

    private static Logger logger = LogManager.getLogger(LoginPDController.class);

    @Autowired
    PedestalDigitalBI pd;

    @Autowired
    LoginBI lb;

    @Autowired
    EstatusGeneralBI estatusGeneralBI;

    @RequestMapping(value = "loginPD.htm", method = RequestMethod.GET)
    public ModelAndView loginPD() throws Exception {
        logger.info("Pedestal Digital - LoginPDController - loginPD.htm");
        Login login = new Login();
        return new ModelAndView("loginPD", "command", login);
    }

    @RequestMapping(value = "validaUsuarioPD.htm", method = RequestMethod.POST)
    public ModelAndView validaUsuarioPD(@ModelAttribute("loginPD") Login login, BindingResult result, HttpServletRequest request) throws Exception {
        logger.info("Pedestal Digital - LoginPDController - validaUsuarioPD.htm");

        int user = login.getUser();
        String pass = Integer.toString(login.getUser()); // login.getPass();
        String valida = request.getParameter("valida");
        ModelAndView mv = null;

        try {
            if (user > 0 && valida.equals("true")) {
                LoginPDDTO userDataPD = null;
                List<MenuPedestalDigital> userMenuPD = null;
                LoginDTO frqUser = null;

                try {
                    userDataPD = pd.login1(user);
                    frqUser = lb.consultaUsuario(user);
                    userMenuPD = pd.login2(userDataPD.getUsuario(), userDataPD.getPerfil());

                    if (userDataPD != null) {
                        if (user == userDataPD.getUsuario()) {
                            DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy", new Locale("es", "MX"));
                            DateFormat hourFormat = new SimpleDateFormat("HH:mm", new Locale("es", "MX"));

                            mv = new ModelAndView("redirect:/central/pedestalDigital/estatusGeneral.htm");
                            request.getSession().setAttribute("userBean", userDataPD);
                            request.getSession().setAttribute("frqUser", frqUser);
                            request.getSession().setAttribute("userMenu", userMenuPD);
                            request.getSession().setAttribute("userPicId", userDataPD.getUsuario());
                            request.getSession().setAttribute("userNamePD", frqUser.getNombreEmpelado());
                            request.getSession().setAttribute("userPositionPD", userDataPD.getDescripcion());
                            request.getSession().setAttribute("fechaPD", dateFormat.format(new Date()));
                            request.getSession().setAttribute("horaPD", hourFormat.format(new Date()));
                        } else {
                            mv = new ModelAndView("loginPD", "command", login);
                            mv.addObject("invalido", "Usuario no válido.");
                        }
                    }
                } catch (Exception e) {
                    UtilFRQ.printErrorLog(request, e);
                }
            } else {
                mv = new ModelAndView("loginPD", "command", login);
                mv.addObject("invalido", "Número de empleado no válido.");
            }
        } catch (Exception e) {
            if (valida == null) {
                mv = new ModelAndView("loginPD", "command", login);
                mv.addObject("invalido", "Password incorrecto.");
            } else {
                // e.printStackTrace();
                mv = new ModelAndView("loginPD", "command", login);
                mv.addObject("invalido", "Password incorrecto.");
            }
        }

        return mv;
    }

    @RequestMapping(value = "validaLoginPD.htm", method = RequestMethod.GET)
    public ModelAndView validaLoginPD(HttpServletRequest request, String idUsuario) throws Exception {

        logger.info("Pedestal Digital - LoginPDController - validaLoginPD.htm");

        ModelAndView mv = null;

        try {
            int user = Integer.parseInt(idUsuario);

            if (user > 0) {
                LoginPDDTO userDataPD = null;
                List<MenuPedestalDigital> userMenuPD = null;
                LoginDTO frqUser = null;

                try {
                    userDataPD = pd.login1(user);
                    frqUser = lb.consultaUsuario(user);
                    userMenuPD = pd.login2(userDataPD.getUsuario(), userDataPD.getPerfil());

                    if (userDataPD != null) {
                        if (user == userDataPD.getUsuario()) {
                            DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy", new Locale("es", "MX"));
                            DateFormat hourFormat = new SimpleDateFormat("HH:mm", new Locale("es", "MX"));

                            mv = new ModelAndView("redirect:/central/pedestalDigital/estatusGeneral.htm");
                            request.getSession().setAttribute("userBean", userDataPD);
                            request.getSession().setAttribute("frqUser", frqUser);
                            request.getSession().setAttribute("userMenu", userMenuPD);
                            request.getSession().setAttribute("userPicId", userDataPD.getUsuario());
                            request.getSession().setAttribute("userNamePD", frqUser.getNombreEmpelado());
                            request.getSession().setAttribute("userPositionPD", userDataPD.getDescripcion());
                            request.getSession().setAttribute("fechaPD", dateFormat.format(new Date()));
                            request.getSession().setAttribute("horaPD", hourFormat.format(new Date()));
                        } else {
                            mv = new ModelAndView("redirect:/central/inicio.htm");
                        }
                    }
                } catch (Exception e) {
                    UtilFRQ.printErrorLog(request, e);
                }
            } else {
                mv = new ModelAndView("redirect:/central/inicio.htm");
            }
        } catch (Exception e) {
            mv = new ModelAndView("redirect:/central/inicio.htm");
        }

        return mv;
    }

    @RequestMapping(value = "logout.htm", method = RequestMethod.GET)
    public ModelAndView logOut(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("Pedestal Digital - LoginPDController - logout.htm");

        request.getSession().setAttribute("userBean", null);
        HttpSession session = request.getSession();
        session.invalidate();

        if (FRQConstantes.PRODUCCION) {
            return new ModelAndView("redirect:/central/inicio.htm");
        } else if (FRQConstantes.DESARROLLO) {
            return new ModelAndView("redirect:/central/pedestalDigital/loginPD.htm");
        }

        return null;
    }

    @RequestMapping(value = "estatusGeneral.htm", method = RequestMethod.GET)
    public ModelAndView estatusGeneral(HttpServletRequest request) throws Exception {
        logger.info("Pedestal Digital - LoginPDController - estatusGeneral.htm");

        LoginPDDTO user = (LoginPDDTO) request.getSession().getAttribute("userBean");
        ModelAndView mv = null;

        if (user != null) {
            mv = new ModelAndView("estatusGeneralPD");
            Gson gson = new Gson();

            // Obtiene todos los negocios
            NegocioDTO obj = new NegocioDTO();
            obj.setIdNegocio(0);
            List<AdminNegocioPDDTO> list = pd.admonNegocio(obj, 2);

            String _negocios = "";
            for (AdminNegocioPDDTO adminNegocioPDDTO : list) {
                _negocios += adminNegocioPDDTO.getIdNegocio() + ",";
            }

            _negocios = StringUtils.chop(_negocios);
            List<IndicadoresDataEGDTO> indicadores = estatusGeneralBI.getIndicadoresEG(0, _negocios);
            mv.addObject("indicadoresGeneral", gson.toJson(indicadores.get(0)));

            indicadores = estatusGeneralBI.getIndicadoresEG(0, "41");
            mv.addObject("indicadoresPropios", gson.toJson(indicadores.get(0)));

            indicadores = estatusGeneralBI.getIndicadoresEG(0, "96");
            mv.addObject("indicadoresTerceros", gson.toJson(indicadores.get(0)));
        } else {
            mv = new ModelAndView("redirect:/central/inicio.htm");
        }

        return mv;
    }

    @RequestMapping(value = "adminArchivos.htm", method = RequestMethod.GET)
    public ModelAndView adminArchivos() {
        logger.info("Pedestal Digital - LoginPDController - adminArchivos.htm");
        ModelAndView mv = new ModelAndView("adminArchivosPD");

        if (FRQConstantes.PRODUCCION) {
            mv.addObject("repoUrl", "http://10.53.33.82/");
        } else {
            mv.addObject("repoUrl", "http://localhost:8888/");
        }

        return mv;
    }

    @RequestMapping(value = "historicoFolios.htm", method = RequestMethod.GET)
    public ModelAndView historicoFolios() {
        logger.info("Pedestal Digital - LoginPDController - historicoFolios.htm");
        return new ModelAndView("historicoFoliosPD");
    }

    @RequestMapping(value = "cargarArchivos.htm", method = RequestMethod.GET)
    public ModelAndView cargarArchivos() {
        logger.info("Pedestal Digital - LoginPDController - cargarArchivos.htm");
        return new ModelAndView("cargarArchivosPD");
    }

    @RequestMapping(value = "cifrasControl.htm", method = RequestMethod.GET)
    public ModelAndView cifrasControl() {
        logger.info("Pedestal Digital - LoginPDController - cifrasControl.htm");
        return new ModelAndView("cifrasControlPD");
    }

    @RequestMapping(value = "estatusTableta.htm", method = RequestMethod.GET)
    public ModelAndView estatusTableta() {
        ModelAndView mv = new ModelAndView("estatusTabletaPD");
        logger.info("Pedestal Digital - LoginPDController - estatusTableta.htm");

        ArrayList<ParamNegocioPDDTO> list = estatusGeneralBI.getParamNegocios();
        for (ParamNegocioPDDTO paramNegocioPDDTO : list) {
            if (paramNegocioPDDTO.getFcreferencia().equals("General")) {
                mv.addObject("generalNegVal", paramNegocioPDDTO.getFcvalor());
            } else if (paramNegocioPDDTO.getFcreferencia().equals("Propios")) {
                mv.addObject("propiosNegVal", paramNegocioPDDTO.getFcvalor());
            } else if (paramNegocioPDDTO.getFcreferencia().equals("Terceros")) {
                mv.addObject("tercerosNegVal", paramNegocioPDDTO.getFcvalor());
            }
        }

        return mv;
    }

    @RequestMapping(value = "historialArchivos.htm", method = RequestMethod.GET)
    public ModelAndView historialArchivos() {
        logger.info("Pedestal Digital - LoginPDController - historialArchivos.htm");
        return new ModelAndView("historialArchivosPD");
    }

    @RequestMapping(value = "infoSucursal.htm", method = RequestMethod.GET)
    public ModelAndView infoSucursal() {
        logger.info("Pedestal Digital - LoginPDController - infoSucursal.htm");
        return new ModelAndView("infoSucursalPD");
    }

    @RequestMapping(value = "infoTableta.htm", method = RequestMethod.GET)
    public ModelAndView infoTableta() {
        logger.info("Pedestal Digital - LoginPDController - infoTableta.htm");
        return new ModelAndView("infoTabletaPD");
    }

    @RequestMapping(value = "reporteria.htm", method = RequestMethod.GET)
    public ModelAndView reporteria() {
        logger.info("Pedestal Digital - LoginPDController - reporteria.htm");
        return new ModelAndView("reporteriaPD");
    }

    @RequestMapping(value = "listaDistribucionPD.htm", method = RequestMethod.GET)
    public ModelAndView listaDistribucionPD() {
        logger.info("Pedestal Digital - LoginPDController - listaDistribucionPD.htm");
        return new ModelAndView("listaDistribucionPD");
    }

    @RequestMapping(value = "quejas.htm", method = RequestMethod.GET)
    public ModelAndView quejas() {
        logger.info("Pedestal Digital - LoginPDController - quejas.htm");
        return new ModelAndView("quejasPD");
    }
}
