package com.gruposalinas.franquicia.controller;

import com.gruposalinas.franquicia.business.CargaArchivosPedestalDigitalBI;
import com.gruposalinas.franquicia.business.PedestalDigitalBI;
import com.gruposalinas.franquicia.domain.AdminCanalPDDTO;
import com.gruposalinas.franquicia.domain.AdminMenuPDDTO;
import com.gruposalinas.franquicia.domain.AdminNegocioPDDTO;
import com.gruposalinas.franquicia.domain.AdminParametroPDDTO;
import com.gruposalinas.franquicia.domain.AdminTipoDocPDDTO;
import com.gruposalinas.franquicia.domain.AdmonEstatusTabletaPDDTO;
import com.gruposalinas.franquicia.domain.BachDescargasPDDTO;
import com.gruposalinas.franquicia.domain.CanalDTO;
import com.gruposalinas.franquicia.domain.Login;
import com.gruposalinas.franquicia.domain.NegocioDTO;
import com.gs.baz.frq.data.sources.pg.DefaultDAO;
import com.gs.baz.frq.data.sources.pg.DefaultJdbcCall;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/central/pedestalDigital/soporte/")
public class SoportePDController extends DefaultDAO {

    @Autowired
    CargaArchivosPedestalDigitalBI cargaArchivos;

    private static Logger logger = LogManager.getLogger(SoportePDController.class);

    private DefaultJdbcCall jdbcDMLPorLinea;
    @Autowired
    PedestalDigitalBI pd;
    //cabeceras
    String cabeceraParam[] = {"ID_PARAMETRO", "PARAMETRO", "DESCRIPCION_PARAM", "VALOR", "REFERENCIA", "FCUSER", "FECHA_MODIFICO", "ACTIVO"};
    String cabeceraNegocio[] = {"ID_NEGOCIO", "DESCRIPCION_NEGOCIO", "FCUSER", "FECHA_MODIFICO", "ACTIVO"};
    String cabeceraCanal[] = {"ID_CANAL", "CANAL", "FCUSER", "FECHA_MODIFICO", "ACTIVO"};
    String cabeceraMenu[] = {"ID_MENU", "DESCRIPCION_MENU", "NIVEL", "FCUSER", "FECHA_MODIFICO", "ACTIVO", "DEPENDE_DE"};
    String cabeceraPerfil[] = {"ID_MENU", "DESCRIPCION", "NIVEL", "ACTIVO_MENU", "DEPENDE_DE", "DESCRIPCION_DEPENDE_DE", "ID_PERFIL", "DESCIPCION_PERFIL", "ACTIVO"};
    String cabeceraEstatusTableta[] = {"ID_ESTADO_TABLETA", "DESCRIPCION_WEB", "DESCRIPCION_JSON", "VISIBLE", "ID_ESTATUS", "FCUSER", "FECHA_CAMBIO", "DEPENDE_DE", "ACTIVO"};
    String cabeceraTipoDocumento[] = {"ID_TIPO_DOC", "NOMBRE_DOCUMENTO", "NIVEL", "DEPENDE_DE", "DESCIPCION_DEPENDE_DE", "FCUSER", "FECHA_CAMBIO", "ACTIVO"};
    String cabeceraDescargas[] = {"ID_CONTROL_DESC", "TIPO_DESCARGA", "ID_TABLETA", "ID_ESTATUS", "FCUSER", "FECHA_CAMBIO", "ACTIVO"};

    public void init() {

        jdbcDMLPorLinea = (DefaultJdbcCall) new DefaultJdbcCall(this.getFrqJdbcTemplate())
                .withSchemaName("PEDESTALDIG")
                .withCatalogName("PA_FILTRO")
                .withFunctionName("FN2_FILTRO");

    }

    @RequestMapping(value = "consultasPD.htm", method = RequestMethod.GET)
    public ModelAndView inicioSoportePD() throws Exception {
        logger.info("Pedestal Digital - SoportePDController - consultasPD.htm");
        return new ModelAndView("consultasPD", "command", new Login());
    }

    @RequestMapping(value = "cecosPDS.htm", method = RequestMethod.GET)
    public ModelAndView cecosSoportePD() throws Exception {
        logger.info("Pedestal Digital - SoportePDController - cecosPDS.htm");
        return new ModelAndView("cecosPDS", "command", new Login());
    }

    @RequestMapping(value = "soporteParametrosPD.htm", method = RequestMethod.GET)
    public ModelAndView soporteParametrosPD() throws Exception {
        logger.info("Pedestal Digital - soporteParametrosPD - soporteParametrosPD.htm");
        return new ModelAndView("parametrosPD", "command", new Login());
    }

    @RequestMapping(value = "insertUpdateparametrosPD.htm", method = RequestMethod.GET)
    public ModelAndView soporteParametrosInsertUdatePD() throws Exception {
        logger.info("Pedestal Digital - soporteParametrosPD - soporteParametrosPD.htm");

        return new ModelAndView("parametrosIPPD", "command", new AdminParametroPDDTO());
    }

    @RequestMapping(value = "insertUpdateNegocioPD.htm", method = RequestMethod.GET)
    public ModelAndView soporteNegocioInsertUdatePD() throws Exception {
        logger.info("Pedestal Digital - soporteParametrosPD - insertUpdateNegocioPD.htm");

        return new ModelAndView("negocioIPPD", "command", new NegocioDTO());
    }

    @RequestMapping(value = "insertUpdateCanalPD.htm", method = RequestMethod.GET)
    public ModelAndView soporteCanalInsertUdatePD() throws Exception {
        logger.info("Pedestal Digital - soporteParametrosPD - insertUpdateCanalPD.htm");

        return new ModelAndView("canalIPPD", "command", new CanalDTO());
    }

    @RequestMapping(value = "insertUpdateMenuPerfilPD.htm", method = RequestMethod.GET)
    public ModelAndView soporteMenuPerfilInsertUdatePD() throws Exception {
        logger.info("Pedestal Digital - soporteParametrosPD - insertUpdateMenuPerfilPD.htm");

        return new ModelAndView("menuPerfilIPPD", "command", new AdminMenuPDDTO());
    }

    @RequestMapping(value = "insertUpdatePerfilPD.htm", method = RequestMethod.GET)
    public ModelAndView soportePerfilInsertUdatePD() throws Exception {
        logger.info("Pedestal Digital - soporteParametrosPD - insertUpdatePerfilPD.htm");

        return new ModelAndView("perfilIPPD", "command", new AdminMenuPDDTO());
    }

    @RequestMapping(value = "insertUpdateEstatusTabletaPD.htm", method = RequestMethod.GET)
    public ModelAndView soporteEstadoInsertUdatePD() throws Exception {
        logger.info("Pedestal Digital - soporteParametrosPD - insertUpdateEstadoPD.htm");

        return new ModelAndView("estatusIPPD", "command", new AdmonEstatusTabletaPDDTO());
    }

    @RequestMapping(value = "insertUpdateTipoDocumentoPD.htm", method = RequestMethod.GET)
    public ModelAndView soporteTipoDocInsertUdatePD() throws Exception {
        logger.info("Pedestal Digital - soporteParametrosPD - insertUpdateTipoDocPD.htm");

        return new ModelAndView("tipoDocIPPD", "command", new AdminTipoDocPDDTO());
    }

    @RequestMapping(value = "insertUpdateDescargalPD.htm", method = RequestMethod.GET)
    public ModelAndView soporteDescargaInsertUdatePD() throws Exception {
        logger.info("Pedestal Digital - soporteParametrosPD - insertUpdateDescargalPD.htm");

        return new ModelAndView("descargaIPPD", "command", new BachDescargasPDDTO());
    }

    @RequestMapping(value = "consultaDescargalPD.htm", method = RequestMethod.GET)
    public ModelAndView soporteConsultaDescargaInsertUdatePD() throws Exception {
        logger.info("Pedestal Digital - soporteParametrosPD - insertUpdateDescargalPD.htm");

        return new ModelAndView("consultaDescIPPD", "command", new BachDescargasPDDTO());
    }

    @RequestMapping(value = "insertUpdateparametrosPD.htm", method = RequestMethod.POST)
    public ModelAndView parametrosInsertUdatePD(HttpServletRequest request, HttpServletResponse response, Model model,
            @ModelAttribute("insertUpdateparametrosPD") AdminParametroPDDTO parametros, @RequestParam("tipo") String tipoConsulta) throws Exception {
        logger.info("Pedestal Digital - insertUpdateparametrosPD - insertUpdateparametrosPD.htm");
        List<AdminParametroPDDTO> result = new ArrayList<AdminParametroPDDTO>();
        ModelAndView mv = new ModelAndView("parametrosIPPD", "command", new AdminParametroPDDTO());
        String mensaje = "";

        switch (tipoConsulta) {
            case "insert":
                result = pd.admonParametros(parametros, 0);

                if (result.get(0).getParametro().equals("INSERT")) {
                    mensaje = "Se INSERTO correctamente. ";
                }
                break;
            case "update":
                result = pd.admonParametros(parametros, 1);

                if (result.get(0).getParametro().contains("UPDATE")) {
                    mensaje = "Se ACTUALIZO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }
                break;

        }

        mv.addObject("resultado", mensaje);
        return mv;
    }

    @RequestMapping(value = "insertUpdateNegocioPD.htm", method = RequestMethod.POST)
    public ModelAndView insertUpdateNegocioPD(HttpServletRequest request, HttpServletResponse response, Model model,
            @ModelAttribute("insertUpdateNegocioPD") NegocioDTO negocio, @RequestParam("tipo") String tipoConsulta) throws Exception {
        logger.info("Pedestal Digital - insertUpdateNegocioPD - insertUpdateNegocioPD.htm");

        ModelAndView mv = new ModelAndView("negocioIPPD", "command", new NegocioDTO());
        List<AdminNegocioPDDTO> result = new ArrayList<AdminNegocioPDDTO>();
        String mensaje = "";

        switch (tipoConsulta) {
            case "insert":
                result = pd.admonNegocio(negocio, 0);
                if (result.get(0).getNegocio().equals("INSERT")) {
                    mensaje = "Se INSERTO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }
                break;
            case "update":
                result = pd.admonNegocio(negocio, 1);

                if (result.get(0).getNegocio().equals("UPDATE")) {
                    mensaje = "Se ACTUALIZO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;

        }
        mv.addObject("resultado", mensaje);
        return mv;
    }

    @RequestMapping(value = "insertUpdateCanalPD.htm", method = RequestMethod.POST)
    public ModelAndView insertUpdateCanalPD(HttpServletRequest request, HttpServletResponse response, Model model,
            @ModelAttribute("insertUpdateCanalPD") CanalDTO canal, @RequestParam("tipo") String tipoConsulta) throws Exception {
        logger.info("Pedestal Digital - insertUpdateCanalPD - insertUpdateCanalPD.htm");

        ModelAndView mv = new ModelAndView("canalIPPD", "command", new CanalDTO());
        List<AdminCanalPDDTO> result = new ArrayList<AdminCanalPDDTO>();
        String mensaje = "";

        switch (tipoConsulta) {
            case "insert":
                result = pd.admonCanal(canal, 0);
                if (result.get(0).getCanal().contains("INSERT")) {
                    mensaje = "Se INSERTO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;
            case "update":
                result = pd.admonCanal(canal, 1);

                if (result.get(0).getCanal().contains("UPDATE")) {
                    mensaje = "Se ACTUALIZO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;

        }
        mv.addObject("resultado", mensaje);
        return mv;
    }

    @RequestMapping(value = "insertUpdateMenuPerfilPD.htm", method = RequestMethod.POST)
    public ModelAndView insertUpdateMenuPerfilPD(HttpServletRequest request, HttpServletResponse response, Model model,
            @ModelAttribute("insertUpdateMenuPerfilPD") AdminMenuPDDTO menuPerfil, @RequestParam("tipo") String tipoConsulta) throws Exception {
        logger.info("Pedestal Digital - insertUpdateMenuPerfilPD - insertUpdateMenuPerfilPD.htm");

        ModelAndView mv = new ModelAndView("menuPerfilIPPD", "command", new AdminMenuPDDTO());
        List<AdminMenuPDDTO> result = new ArrayList<AdminMenuPDDTO>();
        String mensaje = "";

        switch (tipoConsulta) {
            case "insert":
                result = pd.admonMenu(menuPerfil, 0);

                if (result.get(0).getDescripcion().contains("INSERT")) {
                    mensaje = "Se INSERTO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;
            case "update":
                result = pd.admonMenu(menuPerfil, 1);

                if (result.get(0).getDescripcion().contains("UPDATE")) {
                    mensaje = "Se ACTUALIZO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;

        }

        mv.addObject("resultado", mensaje);
        return mv;
    }

    @RequestMapping(value = "insertUpdatePerfilPD.htm", method = RequestMethod.POST)
    public ModelAndView insertUpdatePerfilPD(HttpServletRequest request, HttpServletResponse response, Model model,
            @ModelAttribute("insertUpdateMenuPerfilPD") AdminMenuPDDTO menuPerfil, @RequestParam("tipo") String tipoConsulta) throws Exception {
        logger.info("Pedestal Digital - insertUpdateMenuPerfilPD - insertUpdatePerfilPD.htm");

        ModelAndView mv = new ModelAndView("perfilIPPD", "command", new AdminMenuPDDTO());
        List<AdminMenuPDDTO> result = new ArrayList<AdminMenuPDDTO>();
        String mensaje = "";

        switch (tipoConsulta) {
            case "insert":
                result = pd.admonMenu(menuPerfil, 3);

                if (result.get(0).getDescripcion().contains("INSERT")) {
                    mensaje = "Se INSERTO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;
            case "update":
                result = pd.admonMenu(menuPerfil, 4);

                if (result.get(0).getDescripcion().contains("UPDATE")) {
                    mensaje = "Se ACTUALIZO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;

        }
        mv.addObject("resultado", mensaje);
        return mv;
    }

    @RequestMapping(value = "insertUpdateEstatuslPD.htm", method = RequestMethod.POST)
    public ModelAndView insertUpdateEstatusPD(HttpServletRequest request, HttpServletResponse response, Model model,
            @ModelAttribute("insertUpdateEstatuslPD") AdmonEstatusTabletaPDDTO estatus, @RequestParam("tipo") String tipoConsulta) throws Exception {
        logger.info("Pedestal Digital - insertUpdateMenuPerfilPD - insertUpdateEstatuslPD.htm");

        ModelAndView mv = new ModelAndView("estatusIPPD", "command", new AdmonEstatusTabletaPDDTO());
        List<AdmonEstatusTabletaPDDTO> result = new ArrayList<AdmonEstatusTabletaPDDTO>();
        String mensaje = "";

        switch (tipoConsulta) {
            case "insert":
                result = pd.admonEstatusTablet(estatus, 0);

                if (result.get(0).getDescWEB().contains("INSERT")) {
                    mensaje = "Se INSERTO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;
            case "update":
                result = pd.admonEstatusTablet(estatus, 1);

                if (result.get(0).getDescWEB().contains("UPDATE")) {
                    mensaje = "Se ACTUALIZO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;

        }
        mv.addObject("resultado", mensaje);
        return mv;
    }

    @RequestMapping(value = "insertUpdateTipoDocPD.htm", method = RequestMethod.POST)
    public ModelAndView insertUpdatetipoDocPD(HttpServletRequest request, HttpServletResponse response, Model model,
            @ModelAttribute("insertUpdateTipoDocPD") AdminTipoDocPDDTO tipoDoc, @RequestParam("tipo") String tipoConsulta) throws Exception {
        logger.info("Pedestal Digital - insertUpdateMenuPerfilPD - insertUpdateTipoDocPD.htm");

        ModelAndView mv = new ModelAndView("tipoDocIPPD", "command", new AdminTipoDocPDDTO());
        List<AdminTipoDocPDDTO> result = new ArrayList<AdminTipoDocPDDTO>();
        String mensaje = "";

        switch (tipoConsulta) {
            case "insert":
                result = pd.admonTipoDoc(tipoDoc, 3);

                if (result.get(0).getDescDependeDe().contains("INSERT")) {
                    mensaje = "Se INSERTO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;
            case "update":
                result = pd.admonTipoDoc(tipoDoc, 1);

                if (result.get(0).getDescDependeDe().contains("UPDATE")) {
                    mensaje = "Se ACTUALIZO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;

        }
        mv.addObject("resultado", mensaje);
        return mv;
    }

    @RequestMapping(value = "insertUpdateDescPD.htm", method = RequestMethod.POST)
    public ModelAndView insertUpdateDescargaPD(HttpServletRequest request, HttpServletResponse response, Model model,
            @ModelAttribute("insertUpdateTipoDocPD") BachDescargasPDDTO desc,
            @RequestParam("tipo") String tipoConsulta) throws Exception {

        logger.info("Pedestal Digital - insertUpdateMenuPerfilPD - insertUpdateDescargaPD.htm");
        ModelAndView mv = new ModelAndView("descargaIPPD", "command", new BachDescargasPDDTO());

        List<BachDescargasPDDTO> result = new ArrayList<BachDescargasPDDTO>();
        String mensaje = "";

        switch (tipoConsulta) {
            case "insert":
                result = pd.admonDesc(desc, 0);

                if (result.get(0).getRespuesta().contains("INSERT")) {
                    mensaje = "Se INSERTO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;
            case "update":
                result = pd.admonDesc(desc, 1);

                if (result.get(0).getRespuesta().contains("UPDATE")) {
                    mensaje = "Se ACTUALIZO correctamente. ";
                } else {
                    mensaje = "ALGO NO SALIO BIEN";
                }

                break;
        }

        mv.addObject("resultado", mensaje);
        return mv;
    }

    @RequestMapping(value = "consultaParam.htm", method = RequestMethod.POST)
    public ModelAndView consultaParam(HttpServletRequest request, HttpServletResponse response, Model model, @RequestParam("idParametro") String idParam, @RequestParam("tipo") String tipoConsulta) throws Exception {
        logger.info("Pedestal Digital - consultaParam - consultaParam.htm");
        ModelAndView mv = new ModelAndView("parametrosPD");

        switch (tipoConsulta) {
            case "param":
                mv.addObject("listHeader", cabeceraParam);
                mv.addObject("tipoConsulta", "Parametros");
                List<AdminParametroPDDTO> listParametros = new ArrayList<AdminParametroPDDTO>();
                listParametros = parametros(idParam);
                mv.addObject("listDatos", listParametros);
                break;

            case "negocio":
                mv.addObject("listHeader", cabeceraNegocio);
                mv.addObject("tipoConsulta", "Negocio");
                List<AdminNegocioPDDTO> listNegocio = new ArrayList<AdminNegocioPDDTO>();
                listNegocio = negocio(idParam);
                mv.addObject("listDatos", listNegocio);
                break;

            case "canal":
                mv.addObject("listHeader", cabeceraCanal);
                mv.addObject("tipoConsulta", "Canal");
                List<AdminCanalPDDTO> listCanal = new ArrayList<AdminCanalPDDTO>();
                listCanal = canal(idParam);
                mv.addObject("listDatos", listCanal);
                break;

            case "menu":
                mv.addObject("listHeader", cabeceraMenu);
                mv.addObject("tipoConsulta", "Menu");
                List<AdminMenuPDDTO> listmenu = new ArrayList<AdminMenuPDDTO>();
                listmenu = menu(idParam, 2);
                mv.addObject("listDatos", listmenu);
                break;

            case "perfil":
                mv.addObject("listHeader", cabeceraPerfil);
                mv.addObject("tipoConsulta", "Perfil");
                List<AdminMenuPDDTO> listperfil = new ArrayList<AdminMenuPDDTO>();
                listperfil = menu(idParam, 5);
                mv.addObject("listDatos", listperfil);
                break;
            case "estatus":
                mv.addObject("listHeader", cabeceraEstatusTableta);
                mv.addObject("tipoConsulta", "Estatus Tableta");
                List<AdmonEstatusTabletaPDDTO> listEstatus = new ArrayList<AdmonEstatusTabletaPDDTO>();
                listEstatus = estatus(idParam, 2);
                mv.addObject("listDatos", listEstatus);
                break;
            case "tipoDoc":
                mv.addObject("listHeader", cabeceraTipoDocumento);
                mv.addObject("tipoConsulta", "Tipo de documento");
                List<AdminTipoDocPDDTO> listTipoDoc = new ArrayList<AdminTipoDocPDDTO>();
                listTipoDoc = tipoDoc(idParam, 2);
                mv.addObject("listDatos", listTipoDoc);
                break;
            case "desc":
                mv.addObject("listHeader", cabeceraDescargas);
                mv.addObject("tipoConsulta", "Descargas");
                List<BachDescargasPDDTO> desc = new ArrayList<BachDescargasPDDTO>();
                desc = desc(idParam, tipoConsulta, 2);
                mv.addObject("listDatos", desc);
                break;

        }
        return mv;
    }

    @RequestMapping(value = "consultaDesc.htm", method = RequestMethod.POST)
    public ModelAndView consultaDesc(HttpServletRequest request, HttpServletResponse response, Model model, @RequestParam("tipoDesc") String tipoDesc, @RequestParam("fecha") String fecha) throws Exception {
        logger.info("Pedestal Digital - consultaParam - consultaParam.htm");
        ModelAndView mv = new ModelAndView("consultaDescIPPD");

        mv.addObject("listHeader", cabeceraDescargas);
        mv.addObject("tipoConsulta", "Descargas");
        List<BachDescargasPDDTO> desc = new ArrayList<BachDescargasPDDTO>();
        desc = desc(tipoDesc, fecha, 2);
        mv.addObject("listDatos", desc);

        return mv;
    }

    public List<AdminParametroPDDTO> parametros(String idParam) {
        List<AdminParametroPDDTO> listParametros = new ArrayList<AdminParametroPDDTO>();

        AdminParametroPDDTO parametros = new AdminParametroPDDTO();
        try {
            if (idParam != "") {
                parametros.setIdParametro(Integer.parseInt(idParam));
                listParametros = pd.admonParametros(parametros, 2);
            } else {
                listParametros = pd.admonParametros(parametros, 2);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return listParametros;
    }

    public List<AdminNegocioPDDTO> negocio(String idParam) {
        List<AdminNegocioPDDTO> listNegocio = new ArrayList<AdminNegocioPDDTO>();

        NegocioDTO negocio = new NegocioDTO();
        try {
            if (idParam != "") {
                negocio.setIdNegocio(Integer.parseInt(idParam));
                listNegocio = pd.admonNegocio(negocio, 2);
            } else {
                listNegocio = pd.admonNegocio(negocio, 2);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return listNegocio;
    }

    public List<AdminCanalPDDTO> canal(String idParam) {
        List<AdminCanalPDDTO> listCanal = new ArrayList<AdminCanalPDDTO>();

        CanalDTO canal = new CanalDTO();
        try {
            if (idParam != "") {
                canal.setIdCanal(Integer.parseInt(idParam));
                listCanal = pd.admonCanal(canal, 2);
            } else {
                listCanal = pd.admonCanal(canal, 2);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return listCanal;
    }

    public List<AdminMenuPDDTO> menu(String idParam, int menuPerfil) {
        List<AdminMenuPDDTO> listCanal = new ArrayList<AdminMenuPDDTO>();

        AdminMenuPDDTO perfil = new AdminMenuPDDTO();
        try {
            if (idParam != "") {
                perfil.setIdPerfil(Integer.parseInt(idParam));
                listCanal = pd.admonMenu(perfil, menuPerfil);
            } else {
                listCanal = pd.admonMenu(perfil, menuPerfil);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return listCanal;
    }

    public List<AdmonEstatusTabletaPDDTO> estatus(String idParam, int menuPerfil) {
        List<AdmonEstatusTabletaPDDTO> listEstatus = new ArrayList<AdmonEstatusTabletaPDDTO>();

        AdmonEstatusTabletaPDDTO estatus = new AdmonEstatusTabletaPDDTO();
        try {
            if (idParam != "") {
                estatus.setIdEstadoTableta(Integer.parseInt(idParam));
                listEstatus = pd.admonEstatusTablet(estatus, menuPerfil);
            } else {
                listEstatus = pd.admonEstatusTablet(estatus, menuPerfil);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return listEstatus;
    }

    public List<AdminTipoDocPDDTO> tipoDoc(String idParam, int menuPerfil) {
        List<AdminTipoDocPDDTO> listTipoDco = new ArrayList<AdminTipoDocPDDTO>();

        AdminTipoDocPDDTO tipoDoc = new AdminTipoDocPDDTO();
        try {
            if (idParam != "") {
                tipoDoc.setIdTipoDoc(Integer.parseInt(idParam));
                listTipoDco = pd.admonTipoDoc(tipoDoc, menuPerfil);
            } else {
                tipoDoc.setActivo(2);
                listTipoDco = pd.admonTipoDoc(tipoDoc, menuPerfil);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return listTipoDco;
    }

    public List<BachDescargasPDDTO> desc(String tipoDesc, String fecha, int menuPerfil) {
        List<BachDescargasPDDTO> listDesc = new ArrayList<BachDescargasPDDTO>();

        BachDescargasPDDTO desc = new BachDescargasPDDTO();
        try {
            if (tipoDesc != "" && fecha != "") {
                desc.setFechaActu(fecha);
                desc.setTipoDescarga(tipoDesc);
                listDesc = pd.admonDesc(desc, menuPerfil);
            } else {
                listDesc.get(0).setRespuesta("Faltan datos");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return listDesc;
    }

    @RequestMapping(value = "cargaCecosPDS.htm", method = RequestMethod.GET)
    public ModelAndView cargaCecosPDS() throws Exception {
        logger.info("Pedestal Digital - SoportePDController - cargaCecosPDS.htm");
        return new ModelAndView("cargaCecosPDS", "command", new Login());
    }

    @RequestMapping(value = "cargaGeoPDS.htm", method = RequestMethod.GET)
    public ModelAndView cargaGeoPDS() throws Exception {
        logger.info("Pedestal Digital - SoportePDController - cargaGeoPDS.htm");
        return new ModelAndView("cargaGeoPDS", "command", new Login());
    }

    @RequestMapping(value = "readExcelForGeoData.json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String readExcelForGeoData(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("geoData") String geoData) throws Exception {

        String resp = cargaArchivos.readExcelForGeoData(ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(geoData)));
        return resp;
    }

    /**
     * *************
     */
    // TODO: Serivicio para consulta DML por linea
    String consultaDMLPorLinea(String query) throws Exception {
        Map<String, Object> out = null;
        int error = 0;

        try {
            /**
             * SqlParameterSource in = new MapSqlParameterSource()
             * .addValue("VL_FILTRO", query);
             *
             * out = jdbcDMLPorLinea.execute(in);
             */
            logger.info("Función ejecutada:{PEDESTALDIG.PA_FILTRO.FN2_FILTRO}");

            BigDecimal errorReturn = (BigDecimal) out.get("PA_RESPUESTASP");
            error = errorReturn.intValue();

            if (error == 1) {
                logger.info("Algo ocurrió al ejecutar la consulta");
                return "1";
            } else {
                return "0";
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
        }

        return "1";
    }

    /**
     * ********************
     */
    /**
     * METODOS GENERICOS *
     */
    /**
     * ********************
     */
    // Se comenta metodo
    /**
     * public String executeSql(String sql) { String resultado = " "; sql =
     * sql.replaceAll("\n", " "); Connection con = null; Statement st = null;
     *
     * try { con = getPedesJdbcTemplate().getDataSource().getConnection();
     * enable_dbms_output(con, 100); st = con.createStatement();
     * st.execute(sql); List<String> mensajes = get_dbms_output(con);
     * st.close(); con.close();
     *
     * for (String msg : mensajes) { resultado += msg; } } catch (Exception e) {
     * e.printStackTrace(); } finally { try { if (st != null) { st.close();
     * logger.info("cerro st"); }
     *
     * if (con != null) { con.close(); logger.info("cerro con"); } } catch
     * (SQLException e) { e.printStackTrace(); } }
     *
     * return resultado; }
     */
    public static void enable_dbms_output(Connection conn, int buffer_size) {
        System.out.print("Enabling DBMS_OUTPUT - ");

        try {
            CallableStatement enableStatement = conn.prepareCall("begin dbms_output.enable(:1); end;");
            enableStatement.setLong(1, buffer_size);
            enableStatement.executeUpdate();
            enableStatement.close();
            System.out.println("Enabled!");
        } catch (Exception e) {
            System.out.println("Problem occurred while trying to enable dbms_output! " + e.toString());
        }
    }

    public static List<String> get_dbms_output(Connection conn) {
        List<String> res = new ArrayList<String>();
        System.out.println("Dumping_OUTPUT to System.out : ");

        try {
            CallableStatement stmt = conn.prepareCall("{call sys.dbms_output.get_line(?,?)}");
            stmt.registerOutParameter(1, java.sql.Types.VARCHAR);
            stmt.registerOutParameter(2, java.sql.Types.NUMERIC);
            int status = 0;

            do {
                stmt.execute();
                System.out.println(" " + stmt.getString(1));

                if (stmt.getString(1) != null) {
                    res.add(stmt.getString(1) + "\n");
                }

                status = stmt.getInt(2);
            } while (status == 0);

            System.out.println("Endbms_output!");
        } catch (Exception e) {
            System.out.println("Problemrred during dump of dbms_output! " + e.toString());
        }

        return res;
    }
}

/**
 * ********************************************************************
 */
/**
 * ************************* CLASES FINALES ***************************
 */
/**
 * ********************************************************************
 */
final class DatosConsultasPD {

    private List<String> cabezero;
    private List<List<String>> datos;
    private String mensaje;

    public List<String> getCabezero() {
        return cabezero;
    }

    public void setCabezero(List<String> cabezero) {
        this.cabezero = cabezero;
    }

    public List<List<String>> getDatos() {
        return datos;
    }

    public void setDatos(List<List<String>> datos) {
        this.datos = datos;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}

final class DatosConsultasRowMapperPD implements RowMapper<DatosConsultasPD> {

    @Override
    public DatosConsultasPD mapRow(ResultSet rs, int rowNum) throws SQLException {
        DatosConsultasPD datos = new DatosConsultasPD();

        datos.setMensaje(rs.getString(""));

        return datos;
    }
}
