package com.gruposalinas.franquicia.controller;

import com.gruposalinas.franquicia.business.CecoBI;
import com.gruposalinas.franquicia.business.ChecklistBI;
import com.gruposalinas.franquicia.business.ChecklistUsuarioBI;
import com.gruposalinas.franquicia.business.SucursalBI;
import com.gruposalinas.franquicia.business.VersionBI;
import com.gruposalinas.franquicia.domain.CecoDTO;
import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.ChecklistUsuarioDTO;
import com.gruposalinas.franquicia.domain.SucursalDTO;
import com.gruposalinas.franquicia.domain.UsuarioDTO;
import com.gruposalinas.franquicia.domain.VersionDTO;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/soporte")
public class Soporte {

    @Autowired
    SucursalBI sucursalbi;

    @Autowired
    ChecklistUsuarioBI checklistUsuariobi;

    @Autowired
    CecoBI cecobi;

    @Autowired
    ChecklistBI checklistbi;

    @Autowired
    VersionBI versionbi;

    private static final Logger logger = LogManager.getLogger(Soporte.class);

    //http://localhost:8080/franquicia/soporte/inicio.htm
    @RequestMapping(value = "/inicio", method = RequestMethod.GET)
    public ModelAndView getIndexSoporte(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        } else if (FRQConstantes.DESARROLLO) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(99999999))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexSoporte", "command", new TransportSoporteDTO());

        logger.info(request.getSession().getAttributeNames());

        mv.addObject("paso", new String("0"));

        return mv;
    }

    //http://localhost:8080/franquicia/soporte/validaSucursal.htm?idUsuario=<?>
    @RequestMapping(value = "/validaSucursal", method = RequestMethod.GET)
    public ModelAndView getValidaCoord(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexValidaCoord", "command", new TransportSoporteDTO());

        mv.addObject("paso", new String("0"));

        return mv;
    }

    //http://localhost:8080/franquicia/soporte/validaSucursal.htm?idUsuario=<?>
    @RequestMapping(value = "/validaSucursal", method = RequestMethod.POST)
    public ModelAndView postValidaCoord(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idSucursal", required = true, defaultValue = "") String idSucursal,
            @RequestParam(value = "checkGCC", required = false) boolean checkGCC)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexValidaCoord", "command", new TransportSoporteDTO());

        List<SucursalDTO> lista = new ArrayList<SucursalDTO>();

        if (checkGCC) {
            try {
                lista = sucursalbi.obtieneSucursalGCC(idSucursal);
                mv.addObject("auxGCC", "checked");
            } catch (Exception e) {
                logger.info("ERROR " + e.getMessage());
            }
        } else {
            try {
                lista = sucursalbi.obtieneSucursal(idSucursal);
            } catch (Exception e) {
                logger.info("ERROR " + e.getMessage());
            }
        }

        if (lista.isEmpty()) {
            mv.addObject("paso", new String("no"));
            return mv;
        } else {
            SucursalDTO sucursal = lista.get(0);

            mv.addObject("paso", new String("1"));
            mv.addObject("sucursal", sucursal);

            return mv;
        }
    }

    //http://localhost:8080/franquicia/soporte/validaCoordenadas.htm?idUsuario=<?>
    @RequestMapping(value = "/validaCoordenadas", method = RequestMethod.POST)
    public ModelAndView postConfirmaCoord(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idSucursal", required = true, defaultValue = "") String idSucursal,
            @RequestParam(value = "latitud", required = true, defaultValue = "") String latitud,
            @RequestParam(value = "auxGCC", required = false, defaultValue = "") String auxGCC)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexValidaCoord", "command", new TransportSoporteDTO());

        List<SucursalDTO> lista = new ArrayList<SucursalDTO>();

        if (auxGCC.equals("")) {
            try {
                lista = sucursalbi.obtieneSucursal(idSucursal);
            } catch (Exception e) {
                logger.info("ERROR " + e.getMessage());
            }
        } else if (auxGCC.equals("checked")) {
            try {
                lista = sucursalbi.obtieneSucursalGCC(idSucursal);
                mv.addObject("auxGCC", auxGCC);
            } catch (Exception e) {
                logger.info("ERROR " + e.getMessage());
            }
        }

        SucursalDTO sucursal = lista.get(0);

        List<String> coord = Arrays.asList(latitud.split(","));

        mv.addObject("paso", new String("2"));
        mv.addObject("sucursal", sucursal);
        mv.addObject("nuevaLatitud", coord.get(0));
        mv.addObject("nuevaLongitud", coord.get(1));

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/confirmaCoordenadas.htm?idUsuario=<?>
    @RequestMapping(value = "/confirmaCoordenadas", method = RequestMethod.POST)
    public ModelAndView postShowSucursal(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idSucursal", required = true, defaultValue = "") String idSucursal,
            @RequestParam(value = "nuevaLatitud", required = true, defaultValue = "") String nuevaLatitud,
            @RequestParam(value = "nuevaLongitud", required = true, defaultValue = "") String nuevaLongitud,
            @RequestParam(value = "auxGCC", required = false, defaultValue = "") String auxGCC)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexValidaCoord", "command", new TransportSoporteDTO());

        List<SucursalDTO> lista = new ArrayList<SucursalDTO>();

        if (auxGCC.equals("")) {
            lista = sucursalbi.obtieneSucursal(idSucursal.trim());
            SucursalDTO sucursal = lista.get(0);

            SucursalDTO sucursal2 = new SucursalDTO();

            Double lat = new Double(nuevaLatitud.trim());
            Double lng = new Double(nuevaLongitud.trim());

            sucursal2.setIdCanal(sucursal.getIdCanal());
            sucursal2.setIdPais(sucursal.getIdPais());
            sucursal2.setLatitud(lat.doubleValue());
            sucursal2.setLongitud(lng.doubleValue());
            sucursal2.setNombresuc(sucursal.getNombresuc());
            sucursal2.setNuSucursal(sucursal.getNuSucursal());
            sucursal2.setIdSucursal(sucursal.getIdSucursal());

            sucursalbi.actualizaSucursal(sucursal2);

            mv.addObject("paso", new String("3"));
            mv.addObject("sucursal", sucursal2);
            mv.addObject("latitud1", sucursal2.getLatitud());
            mv.addObject("longitud1", sucursal2.getLongitud());
            mv.addObject("latitud2", sucursal.getLatitud());
            mv.addObject("longitud2", sucursal.getLongitud());
        } else if (auxGCC.equals("checked")) {
            lista = sucursalbi.obtieneSucursalGCC(idSucursal.trim());
            SucursalDTO sucursal = lista.get(0);

            SucursalDTO sucursal2 = new SucursalDTO();

            Double lat = new Double(nuevaLatitud.trim());
            Double lng = new Double(nuevaLongitud.trim());

            sucursal2.setIdCanal(sucursal.getIdCanal());
            sucursal2.setIdPais(sucursal.getIdPais());
            sucursal2.setLatitud(lat.doubleValue());
            sucursal2.setLongitud(lng.doubleValue());
            sucursal2.setNombresuc(sucursal.getNombresuc());
            sucursal2.setNuSucursal(sucursal.getNuSucursal());
            sucursal2.setIdSucursal(sucursal.getIdSucursal());

            sucursalbi.actualizaSucursalGCC(sucursal2);

            mv.addObject("paso", new String("3"));
            mv.addObject("sucursal", sucursal2);
            mv.addObject("latitud1", sucursal2.getLatitud());
            mv.addObject("longitud1", sucursal2.getLongitud());
            mv.addObject("latitud2", sucursal.getLatitud());
            mv.addObject("longitud2", sucursal.getLongitud());
            mv.addObject("auxGCC", auxGCC.trim());
        }

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/altaUserChecklist.htm?idUsuario=<?>
    @RequestMapping(value = "/altaUserChecklist", method = RequestMethod.GET)
    public ModelAndView getAltaUserChecklist(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexChecklist", "command", new ChecklistUsuarioDTO());

        List<ChecklistDTO> lista = checklistbi.buscaChecklist();

        List<Integer> listaFiltrada = new ArrayList<Integer>();

        for (ChecklistDTO val : lista) {
            if (val.getVigente() == 1) {
                try {
                    listaFiltrada.add(val.getIdChecklist());
                } catch (NumberFormatException nfe) {
                    logger.info("ERROR: " + nfe.getMessage());
                }
            }
        }

        mv.addObject("paso", new String("1"));
        mv.addObject("alta", new String("si"));
        mv.addObject("listaChecklist", listaFiltrada);

        return mv;
    }

    //http://localhost:8080/franquicia/soporte/altaUserChecklist.htm?idUsuario=<?>
    @RequestMapping(value = "/altaUserChecklist", method = RequestMethod.POST)
    public ModelAndView postAltaUserChecklist(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "activo", required = true, defaultValue = "") String activo,
            @RequestParam(value = "fechaIni", required = true, defaultValue = "") String fechaIni,
            @RequestParam(value = "fechaResp", required = true, defaultValue = "") String fechaResp,
            @RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
            @RequestParam(value = "idChecklist", required = true, defaultValue = "") String idChecklist,
            @RequestParam(value = "idUsuario", required = true, defaultValue = "") String idUsuario)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        try {
            ChecklistUsuarioDTO checkUsu = new ChecklistUsuarioDTO();

            checkUsu.setActivo(Integer.parseInt(activo));
            checkUsu.setFechaIni(fechaIni.trim());
            checkUsu.setFechaResp(fechaResp.trim());
            checkUsu.setIdCeco(Integer.parseInt(idCeco.trim()));
            checkUsu.setIdChecklist(Integer.parseInt(idChecklist));
            checkUsu.setIdUsuario(Integer.parseInt(idUsuario.trim()));

            int idChecklistUsu = checklistUsuariobi.insertaCheckUsuario(checkUsu);

            checkUsu.setIdChecklist(idChecklistUsu);

            ModelAndView mv = new ModelAndView("indexChecklist", "command", new ChecklistUsuarioDTO());

            mv.addObject("tipo", "ID CHECKLIST USUARIO CREADO--->"); // tipo: 1 //Creado // 0 // No creado
            mv.addObject("paso", new String("2"));
            mv.addObject("alta", new String("si"));
            mv.addObject("res", idChecklistUsu);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/soporte/bajaUserChecklist.htm?idUsuario=<?>
    @RequestMapping(value = "/bajaUserChecklist", method = RequestMethod.GET)
    public ModelAndView getBajaUserChecklist(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexChecklist", "command", new ChecklistUsuarioDTO());

        mv.addObject("paso", new String("1"));
        mv.addObject("alta", new String("no"));

        return mv;
    }

    //http://localhost:8080/franquicia/soporte/bajaUserChecklist.htm?idUsuario=<?>
    @RequestMapping(value = "/bajaUserChecklist", method = RequestMethod.POST)
    public ModelAndView postBajaUserChecklist(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idCheckUsuario", required = true, defaultValue = "") String idCheckUsuario)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexChecklist", "command", new ChecklistUsuarioDTO());

        boolean res = checklistUsuariobi.eliminaChecklistUsuario(Integer.parseInt(idCheckUsuario.trim()));

        mv.addObject("paso", new String("2"));
        mv.addObject("alta", new String("no"));
        mv.addObject("res", res);

        return mv;
    }

    //http://localhost:8080/franquicia/soporte/altaCeco.htm?idUsuario=<?>
    @RequestMapping(value = "/altaCeco", method = RequestMethod.GET)
    public ModelAndView getAltaCeco(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexCeco", "command", new TransportCecoDTO());

        mv.addObject("paso", new String("1"));
        mv.addObject("alta", new String("si"));

        return mv;
    }

    //http://localhost:8080/franquicia/soporte/altaCeco.htm?idUsuario=<?>
    @RequestMapping(value = "/altaCeco", method = RequestMethod.POST)
    public ModelAndView postAltaCeco(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "activo", required = true, defaultValue = "") String activo,
            @RequestParam(value = "calle", required = true, defaultValue = "") String calle,
            @RequestParam(value = "ciudad", required = true, defaultValue = "") String ciudad,
            @RequestParam(value = "cp", required = true, defaultValue = "") String cp,
            @RequestParam(value = "idCeco", required = true, defaultValue = "") String idCeco,
            @RequestParam(value = "descCeco", required = true, defaultValue = "") String descCeco,
            @RequestParam(value = "faxContacto", required = true, defaultValue = "") String faxContacto,
            @RequestParam(value = "idCanal", required = true, defaultValue = "") String idCanal,
            @RequestParam(value = "idCecoSuperior", required = true, defaultValue = "") String idCecoSuperior,
            @RequestParam(value = "idEstado", required = true, defaultValue = "") String idEstado,
            @RequestParam(value = "idNegocio", required = true, defaultValue = "") String idNegocio,
            @RequestParam(value = "idNivel", required = true, defaultValue = "") String idNivel,
            @RequestParam(value = "idPais", required = true, defaultValue = "") String idPais,
            @RequestParam(value = "nombreContacto", required = true, defaultValue = "") String nombreContacto,
            @RequestParam(value = "puestoContacto", required = true, defaultValue = "") String puestoContacto,
            @RequestParam(value = "telefonoContacto", required = true, defaultValue = "") String telefonoContacto,
            @RequestParam(value = "fechaModifico", required = true, defaultValue = "") String fechaModifico,
            @RequestParam(value = "usuarioModifico", required = true, defaultValue = "") String usuarioModifico)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        try {
            CecoDTO ceco = new CecoDTO();
            ceco.setActivo(Integer.parseInt(activo));
            ceco.setCalle(calle);
            ceco.setCiudad(ciudad);
            ceco.setCp(cp);
            ceco.setDescCeco(descCeco);
            ceco.setFaxContacto(faxContacto);
            ceco.setIdCanal(Integer.parseInt(idCanal));
            ceco.setIdCeco(idCeco);
            ceco.setIdCecoSuperior(Integer.parseInt(idCecoSuperior));
            ceco.setIdEstado(Integer.parseInt(idEstado));
            ceco.setIdNegocio(Integer.parseInt(idNegocio));
            ceco.setIdNivel(Integer.parseInt(idNivel));
            ceco.setIdPais(Integer.parseInt(idPais));
            ceco.setNombreContacto(nombreContacto);
            ceco.setPuestoContacto(puestoContacto);
            ceco.setTelefonoContacto(telefonoContacto);
            ceco.setFechaModifico(fechaModifico);
            ceco.setUsuarioModifico(usuarioModifico);
            boolean cecoId = cecobi.insertaCeco(ceco);

            ModelAndView mv = new ModelAndView("indexCeco", "command", new TransportCecoDTO());

            mv.addObject("paso", new String("2"));
            mv.addObject("alta", new String("si"));
            mv.addObject("res", cecoId);

            return mv;
        } catch (Exception e) {
            logger.info("ERROR: " + e.getMessage());
            return null;
        }
    }

    //http://localhost:8080/franquicia/soporte/listSucursal.htm?idUsuario=<?>
    @RequestMapping(value = "/listSucursal", method = RequestMethod.GET)
    public ModelAndView getGetSucursalTables(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexTables", "command", new SucursalDTO());

        List<SucursalDTO> lista = new ArrayList<SucursalDTO>();

        try {
            lista = sucursalbi.obtieneSucursal("");
            mv.addObject("typeList", "sucursal");
            mv.addObject("list", lista);
        } catch (Exception e) {
            logger.info("ERROR " + e.getMessage());
        }

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/listSucursalGCC.htm?idUsuario=<?>
    @RequestMapping(value = "/listSucursalGCC", method = RequestMethod.GET)
    public ModelAndView getGetSucursalGCCTables(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexTables", "command", new SucursalDTO());

        List<SucursalDTO> lista = new ArrayList<SucursalDTO>();

        try {
            lista = sucursalbi.obtieneSucursalGCC("");
            mv.addObject("typeList", "sucursalGCC");
            mv.addObject("list", lista);
        } catch (Exception e) {
            logger.info("ERROR " + e.getMessage());
        }

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/listCheckUser.htm?idUsuario=<?>
    @RequestMapping(value = "/listCheckUser", method = RequestMethod.GET)
    public ModelAndView getCheckUsuTables(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexTables", "command", new ChecklistUsuarioDTO());

        List<ChecklistUsuarioDTO> lista = new ArrayList<ChecklistUsuarioDTO>();

        try {
            lista = checklistUsuariobi.obtieneCheckU(null);
            mv.addObject("typeList", "checkUsu");
            mv.addObject("list", lista);
        } catch (Exception e) {
            logger.info("ERROR " + e.getMessage());
        }

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/listURL.htm?idUsuario=<?>
    @RequestMapping(value = "/listURL", method = RequestMethod.GET)
    public ModelAndView getListURL(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexListURL", "command", new Object());

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/listVersions.htm?idUsuario=<?>
    @RequestMapping(value = "/listVersions", method = RequestMethod.GET)
    public ModelAndView getListVersions(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexVersiones", "command", new VersionDTO());

        List<VersionDTO> lista = new ArrayList<VersionDTO>();

        try {
            lista = versionbi.obtieneVersion("", "", "");
            mv.addObject("list", lista);
        } catch (Exception e) {
            logger.info("ERROR " + e.getMessage());
        }

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/insertVersion.htm?idUsuario=<?>
    @RequestMapping(value = "/insertVersion", method = RequestMethod.GET)
    public ModelAndView getInsertVersion(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexVersiones", "command", new VersionDTO());

        mv.addObject("action", "insert");

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/insertVersion.htm?idUsuario=<?>
    @RequestMapping(value = "/insertVersion", method = RequestMethod.POST)
    public ModelAndView postInsertVersion(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "descripcion", required = true, defaultValue = "") String descripcion,
            @RequestParam(value = "version", required = true, defaultValue = "") String version,
            @RequestParam(value = "sistema", required = true, defaultValue = "") String sistema,
            @RequestParam(value = "so", required = true, defaultValue = "") String so)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexVersiones", "command", new VersionDTO());

        VersionDTO insertVersion = new VersionDTO();

        try {
            insertVersion.setDescripcion(descripcion.trim());
            insertVersion.setVersion(version.trim());
            insertVersion.setSistema(sistema.trim());
            insertVersion.setSo(so.trim());
            insertVersion.setCommit(1);

            int val = versionbi.insertaVersion(insertVersion);
            logger.info("Se insertó el registro " + val);
            mv.addObject("inserted", "si");
        } catch (Exception e) {
            mv.addObject("inserted", "no");
            logger.info("ERROR " + e.getMessage());
        }

        mv.addObject("action", "insert");

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/updateVersion.htm?idUsuario=<?>
    @RequestMapping(value = "/updateVersion", method = RequestMethod.GET)
    public ModelAndView getUpdateVersion(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexVersiones", "command", new VersionDTO());

        List<VersionDTO> lista = new ArrayList<VersionDTO>();

        try {
            lista = versionbi.obtieneVersion("", "", "");
            List<Integer> list = new ArrayList<Integer>();

            for (VersionDTO versionDTO : lista) {
                list.add(versionDTO.getIdVersion());
            }

            mv.addObject("list", list);
            mv.addObject("action", "update");
        } catch (Exception e) {
            logger.info("ERROR " + e.getMessage());
        }

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/updateVersion.htm?idUsuario=<?>
    @RequestMapping(value = "/updateVersion", method = RequestMethod.POST)
    public ModelAndView postUpdateVersion(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idVersion", required = true, defaultValue = "") int idVersion,
            @RequestParam(value = "descripcion", required = true, defaultValue = "") String descripcion,
            @RequestParam(value = "version", required = true, defaultValue = "") String version,
            @RequestParam(value = "sistema", required = true, defaultValue = "") String sistema,
            @RequestParam(value = "so", required = true, defaultValue = "") String so)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexVersiones", "command", new VersionDTO());

        VersionDTO updateVersion = new VersionDTO();

        try {
            updateVersion.setIdVersion(idVersion);
            updateVersion.setDescripcion(descripcion.trim());
            updateVersion.setVersion(version.trim());
            updateVersion.setSistema(sistema.trim());
            updateVersion.setSo(so.trim());
            updateVersion.setCommit(1);

            boolean val = versionbi.actualizaVersion(updateVersion);

            if (val) {
                mv.addObject("updated", "si");
            } else {
                mv.addObject("updated", "no");
            }
        } catch (Exception e) {
            mv.addObject("updated", "no");
            logger.info("ERROR " + e.getMessage());
        }

        mv.addObject("action", "update");

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/deleteVersion.htm?idUsuario=<?>
    @RequestMapping(value = "/deleteVersion", method = RequestMethod.GET)
    public ModelAndView getDeleteVersion(HttpServletRequest request, HttpServletResponse response, Model model)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexVersiones", "command", new VersionDTO());

        List<VersionDTO> lista = new ArrayList<VersionDTO>();

        try {
            lista = versionbi.obtieneVersion("", "", "");
            List<Integer> list = new ArrayList<Integer>();

            for (VersionDTO versionDTO : lista) {
                list.add(versionDTO.getIdVersion());
            }

            mv.addObject("list", list);
            mv.addObject("action", "delete");
        } catch (Exception e) {
            logger.info("ERROR " + e.getMessage());
        }

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/deleteVersion.htm?idUsuario=<?>
    @RequestMapping(value = "/deleteVersion", method = RequestMethod.POST)
    public ModelAndView postDeleteVersion(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "idVersion", required = true, defaultValue = "") int idVersion)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexVersiones", "command", new VersionDTO());

        try {
            boolean val = versionbi.eliminaVersion(idVersion);

            if (val) {
                mv.addObject("deleted", "si");
            } else {
                mv.addObject("deleted", "no");
            }
        } catch (Exception e) {
            mv.addObject("deleted", "no");
            logger.info("ERROR " + e.getMessage());
        }

        mv.addObject("action", "delete");

        return mv;

    }

    @SuppressWarnings("unused")
    private class TransportSoporteDTO {

        private String idSucursal;
        private String latitud;
        private String longitud;
        private String nuevaLatitud;
        private String nuevaLongitud;
        private boolean checkGCC;
        private String auxGCC;

        public String getIdSucursal() {
            return idSucursal;
        }

        public void setIdSucursal(String idSucursal) {
            this.idSucursal = idSucursal;
        }

        public String getLatitud() {
            return latitud;
        }

        public void setLatitud(String latitud) {
            this.latitud = latitud;
        }

        public String getLongitud() {
            return longitud;
        }

        public void setLongitud(String longitud) {
            this.longitud = longitud;
        }

        public String getNuevaLatitud() {
            return nuevaLatitud;
        }

        public void setNuevaLatitud(String nuevaLatitud) {
            this.nuevaLatitud = nuevaLatitud;
        }

        public String getNuevaLongitud() {
            return nuevaLongitud;
        }

        public void setNuevaLongitud(String nuevaLongitud) {
            this.nuevaLongitud = nuevaLongitud;
        }

        public boolean isCheckGCC() {
            return checkGCC;
        }

        public void setCheckGCC(boolean checkGCC) {
            this.checkGCC = checkGCC;
        }

        public String getAuxGCC() {
            return auxGCC;
        }

        public void setAuxGCC(String auxGCC) {
            this.auxGCC = auxGCC;
        }
    }

    @SuppressWarnings("unused")
    private class TransportCecoDTO {

        private String activo;
        private String calle;
        private String ciudad;
        private String cp;
        private String idCeco;
        private String descCeco;
        private String faxContacto;
        private String idCanal;
        private String idCecoSuperior;
        private String idEstado;
        private String idNegocio;
        private String idNivel;
        private String idPais;
        private String nombreContacto;
        private String puestoContacto;
        private String telefonoContacto;
        private String fechaModifico;
        private String usuarioModifico;

        public String getActivo() {
            return activo;
        }

        public void setActivo(String activo) {
            this.activo = activo;
        }

        public String getCalle() {
            return calle;
        }

        public void setCalle(String calle) {
            this.calle = calle;
        }

        public String getCiudad() {
            return ciudad;
        }

        public void setCiudad(String ciudad) {
            this.ciudad = ciudad;
        }

        public String getCp() {
            return cp;
        }

        public void setCp(String cp) {
            this.cp = cp;
        }

        public String getIdCeco() {
            return idCeco;
        }

        public void setIdCeco(String idCeco) {
            this.idCeco = idCeco;
        }

        public String getDescCeco() {
            return descCeco;
        }

        public void setDescCeco(String descCeco) {
            this.descCeco = descCeco;
        }

        public String getFaxContacto() {
            return faxContacto;
        }

        public void setFaxContacto(String faxContacto) {
            this.faxContacto = faxContacto;
        }

        public String getIdCanal() {
            return idCanal;
        }

        public void setIdCanal(String idCanal) {
            this.idCanal = idCanal;
        }

        public String getIdCecoSuperior() {
            return idCecoSuperior;
        }

        public void setIdCecoSuperior(String idCecoSuperior) {
            this.idCecoSuperior = idCecoSuperior;
        }

        public String getIdEstado() {
            return idEstado;
        }

        public void setIdEstado(String idEstado) {
            this.idEstado = idEstado;
        }

        public String getIdNegocio() {
            return idNegocio;
        }

        public void setIdNegocio(String idNegocio) {
            this.idNegocio = idNegocio;
        }

        public String getIdNivel() {
            return idNivel;
        }

        public void setIdNivel(String idNivel) {
            this.idNivel = idNivel;
        }

        public String getIdPais() {
            return idPais;
        }

        public void setIdPais(String idPais) {
            this.idPais = idPais;
        }

        public String getNombreContacto() {
            return nombreContacto;
        }

        public void setNombreContacto(String nombreContacto) {
            this.nombreContacto = nombreContacto;
        }

        public String getPuestoContacto() {
            return puestoContacto;
        }

        public void setPuestoContacto(String puestoContacto) {
            this.puestoContacto = puestoContacto;
        }

        public String getTelefonoContacto() {
            return telefonoContacto;
        }

        public void setTelefonoContacto(String telefonoContacto) {
            this.telefonoContacto = telefonoContacto;
        }

        public String getFechaModifico() {
            return fechaModifico;
        }

        public void setFechaModifico(String fechaModifico) {
            this.fechaModifico = fechaModifico;
        }

        public String getUsuarioModifico() {
            return usuarioModifico;
        }

        public void setUsuarioModifico(String usuarioModifico) {
            this.usuarioModifico = usuarioModifico;
        }
    }
}
