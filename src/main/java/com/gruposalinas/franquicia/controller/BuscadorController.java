package com.gruposalinas.franquicia.controller;

public class BuscadorController {
//
//	@Autowired
//	private BuscadorBI buscadorBI;
//	@Autowired
//	private GridFSDAO fileStorage;
//	@Autowired
//	private CleanParameter UtilString;
//
//	private String recurso;
//	private String tagName;
//
//	/* +++++++++Metodos de controlador para el autocompletado+++++++++ */
//	public BuscadorController() {
//		this.recurso = null;
//	}
//
//	@RequestMapping(value = "/busqueda.htm", method = RequestMethod.GET)
//	public ModelAndView metodoCompleta() {
//		return new ModelAndView("buscador");
//	}
//
//	@RequestMapping(value = "/getTags", method = RequestMethod.GET)
//	public @ResponseBody List<Tag> getTags(@RequestParam String tagName, HttpServletRequest request) throws IOException, EncodingException {
//		try {
//			this.tagName = UtilString.cleanParameter(tagName);
//			tagName = new String(tagName.getBytes("ISO-8859-1"), "UTF-8");
//			if (tagName.length() > 3) {
//				return buscadorBI.getTags(ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(tagName)), request);
//			} else {
//				return null;
//			}
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			UtilFRQ.printErrorLog(request, e);
//			return null;
//		}
//	}
//
//	// AJAX CONTROLLER METHODS
//	@RequestMapping(value = "/ajaxtest", method = RequestMethod.GET)
//	public @ResponseBody List<Tag> getData(
//			@RequestParam(value = "termino", required = true, defaultValue = "termino") String termino, HttpServletRequest request) throws EncodingException {
//			try {
//				termino = UtilString.cleanParameter(termino);
//				termino = new String(termino.getBytes("ISO-8859-1"), "UTF-8");
//				return buscadorBI.getResults(ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(termino)));
//			} catch (UnsupportedEncodingException e) {
//				// TODO Auto-generated catch block
//				UtilFRQ.printErrorLog(request, e);
//				return null;
//			}
//	}
//
//	@RequestMapping(value = "/muestraDoc.htm", method = RequestMethod.GET)
//	public @ResponseBody ModelAndView muestraDoc(HttpServletResponse response, HttpServletRequest request,
//			@RequestParam(value = "recurso", required = true, defaultValue = "") String recurso) throws IOException, EncodingException {
//		//No aplico el limpiar por que quita el punto
//		this.recurso = recurso;
//		List<String> np = buscadorBI.muestraDoc(ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(recurso)), this.tagName, request);
//		ModelAndView mv = new ModelAndView("muestraDoc");
//		mv.addObject("numPags", np);
//		mv.addObject("recurso", ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(recurso)));
//		return mv;
//	}
//
//	/* METODO PARA EMBEBER EL DOCUMENTO PDF AL DIV-EMBED */
//	@RequestMapping(value = "getPdfDocument.htm", method = RequestMethod.GET)
//	public String reporte(HttpServletRequest request, HttpServletResponse response) throws IOException {
//		File f = fileStorage.getFile(this.recurso);
//
//	    try {
//
//	    	File nf = new PDFMarksBI().getMarks(f.getAbsolutePath(),this.tagName, request);
//			InputStream fileInputStream = FileUtils.openInputStream(nf);
//
//			response.setHeader("Content-Type", "application/pdf");
//			response.setHeader("Content-Disposition", "inline; filename=Doc.pdf");
//			response.setHeader("Content-Transfer-Encoding: binary", "Accept-Ranges: bytes");
//			response.setHeader("Cache-Control:private", "Pragma: private");
//			response.setHeader("Expires:0", "attachment;filename=documento.pdf");
//			try {
//				FileCopyUtils.copy(fileInputStream, response.getOutputStream());
//			} catch ( Exception e ) {
//				UtilFRQ.printErrorLog(request, e);
//			} finally {
//				fileInputStream.close();
//			}
//
//		} catch (Exception e1) {
//			UtilFRQ.printErrorLog(request, e1);
//		}
//		response.getOutputStream();
//		response.flushBuffer();
//
//		return null;
//	}
//
//	@ExceptionHandler(Exception.class)
//	public ModelAndView handleError(HttpServletRequest request, Exception ex) {
//		UtilFRQ.printErrorLog(request, ex);
//		ModelAndView mv = new ModelAndView("exception");
//	    return mv;
//	}
}

//CODIGO DOCUMENTO QUE ME DIO CESAR
/*
@RequestMapping(value="getDocument.htm", method = RequestMethod.GET)
public String reporteExcel(HttpServletRequest request, HttpServletResponse response) throws IOException{
	ServletOutputStream sos = null;
	String edo = "ESTADO";
	String tipo = "TIPO";
	String since = "SINCE";
	String to = "TO";
	String archivo = "Folio,Fecha,Nombre de Socio,Categoría,Título,Estado\n";
	archivo += "SI ME CREO EL DOCUMENTO!!!!!!!!"+edo+tipo+since+to+archivo;
	response.setContentType("application/pdf");
	response.addHeader("Content-Disposition", "attachment; filename=ReporteAdministracion.pdf");
	sos = response.getOutputStream();
	sos.write(archivo.getBytes());
	return null;
}
 */
 /*FUNCIONA EN LOS NAVEGADORES
 * response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		response.setHeader("Content-Type", "application/pdf");
		response.setHeader("Content-Disposition", null);
 */
