package com.gruposalinas.franquicia.controller;

import com.gruposalinas.franquicia.business.ChecklistBI;
import com.gruposalinas.franquicia.business.ChecklistModificacionesBI;
import com.gruposalinas.franquicia.business.CorreoBI;
import com.gruposalinas.franquicia.domain.UsuarioDTO;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Correo {

    private static final Logger logger = LogManager.getLogger(Correo.class);

    @Autowired
    CorreoBI correoBI;
    @Autowired
    ChecklistBI checklistBI;
    @Autowired
    ChecklistModificacionesBI checklistModificacionesBI;

    @RequestMapping(value = "pruebaCorreo.htm", method = RequestMethod.GET)
    public @ResponseBody
    String setEmpCausa(HttpServletRequest request) {
        try {
            //String basePath = FRQConstantes.getURLApacheServer()+request.getContextPath()+"/";
            //request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");

            //int idOwner = 2;
            //int causa = 1;
            //if (causaBI.insertaColaboradorCausa(causa, idEmpleado)){
            if (true) {
                //Se mandan los correos a creador y apoyador
                correoBI.sendMailAutorizacion(userSession.getIdUsuario(), userSession, "");
            }
        } catch (Exception e) {
            logger.info("Algo ocurrió..... " + e);
        }
        return null;
    }

    @RequestMapping(value = "/central/autorizaCambiosCheck.htm", method = RequestMethod.POST)
    public ModelAndView autorizaCambiosCheck(HttpServletRequest request,
            @RequestParam(value = "tipo", required = true, defaultValue = "") String tipo,
            @RequestParam(value = "idCheck", required = true, defaultValue = "") String idCheck,
            @RequestParam(value = "nombreCheck", required = true, defaultValue = "") String nombreCheck) {
        ModelAndView mv = new ModelAndView("resumenChecklist");
        try {

            //System.out.println("NOMBRE CHECK ARMANDO EL CORREO" + nombreCheck);
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            //System.out.println("*********DATOS USER********* \n"+ userSession.getNombre() +"\n"+userSession.getIdUsuario());
            Boolean mail = false;
            Boolean autoriza = false;
            //Map<String, Object> listas = checklistBI.buscaResumenCheck(Integer.parseInt(userSession.getIdUsuario()));

            Map<String, Object> listas = checklistBI.buscaResumenCheck();

            if (tipo.equals("true")) {
                mail = correoBI.sendMailAprobacion(Boolean.parseBoolean(tipo), userSession, nombreCheck);
                autoriza = checklistModificacionesBI.autorizaChecklist(1, Integer.parseInt(idCheck));

                mv.addObject("listaResumen", listas.get("activos"));
                mv.addObject("listaPendientes", listas.get("pendientes"));

                mv.addObject("show", tipo);
                //mv.addObject("respuesta", res);
            } else if (tipo.equals("false")) {
                mail = correoBI.sendMailAprobacion(Boolean.parseBoolean(tipo), userSession, nombreCheck);
                autoriza = checklistModificacionesBI.autorizaChecklist(0, Integer.parseInt(idCheck));

                mv.addObject("listaResumen", listas.get("activos"));
                mv.addObject("listaPendientes", listas.get("pendientes"));

                mv.addObject("show", tipo);
                //mv.addObject("respuesta", res);
            }

            logger.info("MAIL--->" + mail + "AUTORIZA" + autoriza);
        } catch (Exception e) {
            logger.info("Algo ocurrió..... " + e);
        }
        return mv;
    }

}
