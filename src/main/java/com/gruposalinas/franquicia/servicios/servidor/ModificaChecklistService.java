package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.ArbolDecisionBI;
import com.gruposalinas.franquicia.business.BitacoraBI;
import com.gruposalinas.franquicia.business.ChecklistBI;
import com.gruposalinas.franquicia.business.ChecklistPreguntaBI;
import com.gruposalinas.franquicia.business.ChecklistTBI;
import com.gruposalinas.franquicia.business.ChecklistUsuarioBI;
import com.gruposalinas.franquicia.business.CompromisoBI;
import com.gruposalinas.franquicia.business.EdoChecklistBI;
import com.gruposalinas.franquicia.business.EvidenciaBI;
import com.gruposalinas.franquicia.business.ModuloBI;
import com.gruposalinas.franquicia.business.PosiblesBI;
import com.gruposalinas.franquicia.business.PreguntaBI;
import com.gruposalinas.franquicia.business.RespuestaBI;
import com.gruposalinas.franquicia.business.TipoChecklistBI;
import com.gruposalinas.franquicia.business.TipoPreguntaBI;
import com.gruposalinas.franquicia.domain.ArbolDecisionDTO;
import com.gruposalinas.franquicia.domain.BitacoraDTO;
import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.ChecklistPreguntaDTO;
import com.gruposalinas.franquicia.domain.ChecklistUsuarioDTO;
import com.gruposalinas.franquicia.domain.CompromisoDTO;
import com.gruposalinas.franquicia.domain.EdoChecklistDTO;
import com.gruposalinas.franquicia.domain.EvidenciaDTO;
import com.gruposalinas.franquicia.domain.ModuloDTO;
import com.gruposalinas.franquicia.domain.PosiblesDTO;
import com.gruposalinas.franquicia.domain.PreguntaDTO;
import com.gruposalinas.franquicia.domain.RespuestaDTO;
import com.gruposalinas.franquicia.domain.TipoChecklistDTO;
import com.gruposalinas.franquicia.domain.TipoPreguntaDTO;
import com.gruposalinas.franquicia.domain.UsuarioDTO;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/checklistServices")
public class ModificaChecklistService {

    @Autowired
    ModuloBI modulobi;
    @Autowired
    TipoPreguntaBI tipPreguntabi;
    @Autowired
    PreguntaBI preguntabi;
    @Autowired
    TipoChecklistBI tipoChecklistbi;
    @Autowired
    ChecklistBI checklistbi;
    @Autowired
    ChecklistPreguntaBI checklistPreguntabi;
    @Autowired
    ChecklistUsuarioBI checklistUsuariobi;
    @Autowired
    EdoChecklistBI edoChecklistbi;
    @Autowired
    ArbolDecisionBI arbolDecisionbi;
    @Autowired
    BitacoraBI bitacorabi;
    @Autowired
    RespuestaBI respuestabi;
    @Autowired
    CompromisoBI compromisobi;
    @Autowired
    EvidenciaBI evidenciabi;
    //TEMPORALES
    @Autowired
    ChecklistTBI checkTbi;
    @Autowired
    PosiblesBI posiblesbi;

    // http://localhost:8080/franquicia/checklistServices/updateModulo.json?idModulo=<?>&idModuloPadre=<?>&nombreMod=<?>
    @RequestMapping(value = "/updateModulo", method = RequestMethod.GET)
    public ModelAndView updateModulo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String idModulo = request.getParameter("idModulo");
        String idModuloPadre = request.getParameter("idModuloPadre");
        String nombreModulo = new String(request.getParameter("nombreMod").getBytes("ISO-8859-1"), "UTF-8");

        ModuloDTO modulo = new ModuloDTO();
        modulo.setIdModuloPadre(Integer.parseInt(idModuloPadre));
        modulo.setNombre(nombreModulo);
        modulo.setIdModulo(Integer.parseInt(idModulo));
        boolean res = modulobi.actualizaModulo(modulo);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "MODULO MODIFICADO");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/checklistServices/updateTipoPregunta.json?idTipoPreg=<?>&cveTipoPreg=<?>&desTipoPreg=<?>&setActivo=<?>
    @RequestMapping(value = "/updateTipoPregunta", method = RequestMethod.GET)
    public ModelAndView updateTipoPregunta(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String cveTipoPreg = new String(request.getParameter("cveTipoPreg").getBytes("ISO-8859-1"), "UTF-8");
        String desTipoPreg = new String(request.getParameter("desTipoPreg").getBytes("ISO-8859-1"), "UTF-8");
        String setActivo = request.getParameter("setActivo");
        String idTipoPregunta = request.getParameter("idTipoPreg");

        TipoPreguntaDTO tipoPregunta = new TipoPreguntaDTO();
        tipoPregunta.setCvePregunta(cveTipoPreg);
        tipoPregunta.setDescripcion(desTipoPreg);
        tipoPregunta.setActivo(Integer.parseInt(setActivo));
        tipoPregunta.setIdTipoPregunta(Integer.parseInt(idTipoPregunta));
        boolean res = tipPreguntabi.actualizaTipoPregunta(tipoPregunta);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "TIPO_PREGUNTA MODIFICADO");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/checklistServices/updatePregunta.json?idPregunta=<?>&idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>
    @RequestMapping(value = "/updatePregunta", method = RequestMethod.GET)
    public ModelAndView updatePregunta(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String idMod = request.getParameter("idMod");
        String idTipo = request.getParameter("idTipo");
        String estatus = request.getParameter("estatus");
        String setPregunta = new String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
        String idPregunta = request.getParameter("idPregunta");

        PreguntaDTO pregunta = new PreguntaDTO();
        pregunta.setIdModulo(Integer.parseInt(idMod));
        pregunta.setIdTipo(Integer.parseInt(idTipo));
        pregunta.setEstatus(Integer.parseInt(estatus));
        pregunta.setPregunta(setPregunta);
        pregunta.setIdPregunta(Integer.parseInt(idPregunta));
        boolean res = this.preguntabi.actualizaPregunta(pregunta);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "ID DE PREGUNTA ACTUALIZADO");
        mv.addObject("res", res);
        return mv;

    }
    // http://localhost:8080/franquicia/checklistServices/updatePregunta.json?idPregunta=<?>&idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>

    @RequestMapping(value = "/updatePreguntaService", method = RequestMethod.GET)
    public @ResponseBody
    boolean updatePreguntaService(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String idMod = request.getParameter("idMod");
        String idTipo = request.getParameter("idTipo");
        String estatus = request.getParameter("estatus");
        String setPregunta = new String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
        String idPregunta = request.getParameter("idPregunta");

        PreguntaDTO pregunta = new PreguntaDTO();
        pregunta.setIdModulo(Integer.parseInt(idMod));
        pregunta.setIdTipo(Integer.parseInt(idTipo));
        pregunta.setEstatus(Integer.parseInt(estatus));
        pregunta.setPregunta(setPregunta);
        pregunta.setIdPregunta(Integer.parseInt(idPregunta));
        boolean res = this.preguntabi.actualizaPregunta(pregunta);

        return res;

    }
    // http://localhost:8080/franquicia/checklistServices/updateTipoCheck.json?idTipoCheck=<?>&descTipoCheck=<?>

    @RequestMapping(value = "/updateTipoCheck", method = RequestMethod.GET)
    public ModelAndView updateTipoChecklist(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String descTipoCheck = new String(request.getParameter("descTipoCheck").getBytes("ISO-8859-1"), "UTF-8");
        String idTipoChck = request.getParameter("idTipoCheck");

        TipoChecklistDTO tipoChecklist = new TipoChecklistDTO();
        tipoChecklist.setDescTipo(descTipoCheck);
        tipoChecklist.setIdTipoCheck(Integer.parseInt(idTipoChck));
        boolean res = tipoChecklistbi.actualizaTipoChecklist(tipoChecklist);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "TIPO CHECKLIST MODIFICADO");
        mv.addObject("res", res);
        return mv;

    }

    // http://localhost:8080/franquicia/checklistServices/updateEstadoChecklist.json?idEdoCheck=<?>&descripcion=<?>
    @RequestMapping(value = "/updateEstadoChecklist", method = RequestMethod.GET)
    public ModelAndView updateEstadoChecklist(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
        String idEdoChck = request.getParameter("idEdoCheck");

        EdoChecklistDTO edoCheck = new EdoChecklistDTO();
        edoCheck.setDescripcion(descripcion);
        edoCheck.setIdEdochecklist(Integer.parseInt(idEdoChck));
        boolean res = edoChecklistbi.actualizaEdoChecklist(edoCheck);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "ESTADO CHECKLIST ACTUALIZADO");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/checklistServices/updateCheckService.json?idChecklist=<?>&fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&idUsuario=<?>&periodicidad=<?>&dia=<?>&version=<?>&ordenGrupo=<?>
    @RequestMapping(value = "/updateCheckService", method = RequestMethod.GET)
    public @ResponseBody
    boolean updateCheckService(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String fechaIni = request.getParameter("fechaIni");
        String fechaFin = request.getParameter("fechaFin");
        String idEstado = request.getParameter("idEstado");
        String idHorario = request.getParameter("idHorario");
        String idTipoCheck = request.getParameter("idTipoCheck");
        String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
        String setVigente = request.getParameter("setVigente");
        String idChck = request.getParameter("idChecklist");
        String idUsuario = request.getParameter("idUsuario");
        String periodicidad = request.getParameter("periodicidad");
        String version = request.getParameter("version");
        String ordenGrupo = request.getParameter("ordenGrupo");
        String dia = request.getParameter("dia");

        UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
        idUsuario = userSession.getIdUsuario();

        ChecklistDTO checklist = new ChecklistDTO();
        checklist.setFecha_inicio(fechaIni);
        checklist.setFecha_fin(fechaFin);
        checklist.setIdEstado(Integer.parseInt(idEstado));
        checklist.setIdHorario(Integer.parseInt(idHorario));
        TipoChecklistDTO chk = new TipoChecklistDTO();
        chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
        checklist.setIdTipoChecklist(chk);
        checklist.setNombreCheck(nombreCheck);
        checklist.setVigente(Integer.parseInt(setVigente));
        // Obtengo el ID del checklist
        checklist.setIdChecklist(Integer.parseInt(idChck));
        //SE AGREGA USUARIO RESPONSABLE, DIA Y PERIDO
        checklist.setIdUsuario(Integer.parseInt(idUsuario));
        checklist.setPeriodicidad(periodicidad);
        checklist.setVersion(version);
        checklist.setOrdeGrupo(ordenGrupo);
        checklist.setDia(dia);
        boolean res = checklistbi.actualizaChecklist(checklist);

        return res;

    }

    // http://localhost:8080/franquicia/checklistServices/updateCheck.json?idChecklist=<?>&fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>
    // FORMATO FECHA INI Y FECHA FIN (AAAAMMDD)
    @RequestMapping(value = "/updateCheck", method = RequestMethod.GET)
    public ModelAndView updateCheck(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String fechaIni = request.getParameter("fechaIni");
        String fechaFin = request.getParameter("fechaFin");
        String idEstado = request.getParameter("idEstado");
        String idHorario = request.getParameter("idHorario");
        String idTipoCheck = request.getParameter("idTipoCheck");
        String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
        String setVigente = request.getParameter("setVigente");
        String idChck = request.getParameter("idChecklist");

        ChecklistDTO checklist = new ChecklistDTO();
        checklist.setFecha_inicio(fechaIni);
        checklist.setFecha_fin(fechaFin);
        checklist.setIdEstado(Integer.parseInt(idEstado));
        checklist.setIdHorario(Integer.parseInt(idHorario));
        TipoChecklistDTO chk = new TipoChecklistDTO();
        chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
        checklist.setIdTipoChecklist(chk);
        checklist.setNombreCheck(nombreCheck);
        checklist.setVigente(Integer.parseInt(setVigente));
        // Obtengo el ID del checklist
        checklist.setIdChecklist(Integer.parseInt(idChck));
        boolean res = checklistbi.actualizaChecklist(checklist);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "CHECKLIST MODIFICADO");
        mv.addObject("res", res);
        return mv;

    }

    // http://localhost:8080/franquicia/checklistServices/updatePregCheck.json?idPreg=<?>&idCheck=<?>&ordenPreg=<?>&pregPadre=<?>
    @RequestMapping(value = "/updatePregCheck", method = RequestMethod.GET)
    public ModelAndView updatePreguntaCheck(HttpServletRequest request, HttpServletResponse response) {
        String idCheck = request.getParameter("idCheck");
        String ordenPreg = request.getParameter("ordenPreg");
        String idPadre = request.getParameter("pregPadre");
        String idPreg = request.getParameter("idPreg");

        ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
        pregDTO.setIdChecklist(Integer.parseInt(idCheck));
        pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
        pregDTO.setPregPadre(Integer.parseInt(idPadre));
        pregDTO.setIdPregunta(Integer.parseInt(idPreg));
        boolean res = checklistPreguntabi.actualizaChecklistPregunta(pregDTO);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "PREGUNTA CHECKLIST MODIFICADA");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/checklistServices/updatePregCheckService.json?idPreg=<?>&idCheck=<?>&ordenPreg=<?>&pregPadre=<?>
    @RequestMapping(value = "/updatePregCheckService", method = RequestMethod.GET)
    public @ResponseBody
    boolean updatePreguntaCheckService(HttpServletRequest request, HttpServletResponse response) {
        String idCheck = request.getParameter("idCheck");
        String ordenPreg = request.getParameter("ordenPreg");
        String idPadre = request.getParameter("pregPadre");
        String idPreg = request.getParameter("idPreg");

        ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
        pregDTO.setIdChecklist(Integer.parseInt(idCheck));
        pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
        pregDTO.setPregPadre(Integer.parseInt(idPadre));
        pregDTO.setIdPregunta(Integer.parseInt(idPreg));
        boolean res = checklistPreguntabi.actualizaChecklistPregunta(pregDTO);

        return res;
    }

    /*AGREGUE IDCHECKUSUARIO (ID)*/
    // http://localhost:8080/franquicia/checklistServices/updateUsuCheck.json?idCheckUsuario=<?>&setActivo=<?>&fechaIni=<?>&fechaResp=<?>&idCeco=<?>&idChecklist=<?>&idUsu=<?>
    @RequestMapping(value = "/updateUsuCheck", method = RequestMethod.GET)
    public ModelAndView updateUsuarioCheck(HttpServletRequest request, HttpServletResponse response) {
        String setActivo = request.getParameter("setActivo");
        String fechaIni = request.getParameter("fechaIni");
        String fechaResp = request.getParameter("fechaResp");
        String idCeco = request.getParameter("idCeco");
        String idChecklist = request.getParameter("idChecklist");
        String idUsu = request.getParameter("idUsu");
        String idCheckUsuario = request.getParameter("idCheckUsuario");

        ChecklistUsuarioDTO checkUsu = new ChecklistUsuarioDTO();
        checkUsu.setActivo(Integer.parseInt(setActivo));
        checkUsu.setFechaIni(fechaIni);
        checkUsu.setFechaResp(fechaResp);
        checkUsu.setIdCeco(Integer.parseInt(idCeco));
        checkUsu.setIdChecklist(Integer.parseInt(idChecklist));
        checkUsu.setIdUsuario(Integer.parseInt(idUsu));
        checkUsu.setIdCheckUsuario(Integer.parseInt(idCheckUsuario));
        boolean res = checklistUsuariobi.actualizaChecklistUsuario(checkUsu);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "ID CHECKLIST MODIFICADO");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/checklistServices/descativaCheckUsua.json?idCeco=<?>&idChecklist=<?>
    @RequestMapping(value = "/descativaCheckUsua", method = RequestMethod.GET)
    public ModelAndView descativaCheckUsua(HttpServletRequest request, HttpServletResponse response) {

        String idCeco = request.getParameter("idCeco");
        String idChecklist = request.getParameter("idChecklist");

        boolean res = checklistUsuariobi.desactivaCheckUsua(Integer.parseInt(idCeco), Integer.parseInt(idChecklist));

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "ID CHECKLIST USUARIO DESACTIVADO");
        mv.addObject("res", res);
        return mv;
    }

    /*AGREGUE IDSETARBOLDESICION (ID)*/
    // http://localhost:8080/franquicia/checklistServices/updateArbolDesicion.json?idArbolDesicion=<?>&estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>
    @RequestMapping(value = "/updateArbolDesicion", method = RequestMethod.GET)
    public ModelAndView updateArbolDecision(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String estatusEvidencia = request.getParameter("estatusEvidencia");
        String idCheck = request.getParameter("idCheck");
        String idPreg = request.getParameter("idPreg");
        String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
        String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
        String idArbolDesicion = request.getParameter("idArbolDesicion");
        String reqAccion = request.getParameter("reqAccion");
        String reqObs = request.getParameter("reqObs");
        String reqOblig = request.getParameter("reqOblig");
        String etiquetaEvi = request.getParameter("etiquetaEvidencia");

        ArbolDecisionDTO arbol = new ArbolDecisionDTO();
        arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
        arbol.setIdCheckList(Integer.parseInt(idCheck));
        arbol.setIdPregunta(Integer.parseInt(idPreg));
        arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
        arbol.setRespuesta(setRes);
        arbol.setIdArbolDesicion(Integer.parseInt(idArbolDesicion));
        arbol.setReqAccion(Integer.parseInt(reqAccion));
        arbol.setReqObservacion(Integer.parseInt(reqObs));
        arbol.setReqOblig(Integer.parseInt(reqOblig));
        arbol.setDescEvidencia(etiquetaEvi);
        boolean res = arbolDecisionbi.actualizaArbolDecision(arbol);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "ARBOLR CREADO CON EXITO--->");
        mv.addObject("res", res);
        return mv;
    }

    /*AGREGUE IDSETARBOLDESICION (ID)*/
    // http://localhost:8080/franquicia/checklistServices/updateArbolDesicion.json?idArbolDesicion=<?>&estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>
    @RequestMapping(value = "/updateArbolDesicionService", method = RequestMethod.GET)
    public @ResponseBody
    boolean updateArbolDecisionService(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String estatusEvidencia = request.getParameter("estatusEvidencia");
        String idCheck = request.getParameter("idCheck");
        String idPreg = request.getParameter("idPreg");
        String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
        String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
        String idArbolDesicion = request.getParameter("idArbolDesicion");
        String reqAccion = request.getParameter("reqAccion");
        String reqObs = request.getParameter("reqObs");
        String reqOblig = request.getParameter("reqOblig");
        String etiquetaEvi = request.getParameter("etiquetaEvidencia");

        ArbolDecisionDTO arbol = new ArbolDecisionDTO();
        arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
        arbol.setIdCheckList(Integer.parseInt(idCheck));
        arbol.setIdPregunta(Integer.parseInt(idPreg));
        arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
        arbol.setRespuesta(setRes);
        arbol.setIdArbolDesicion(Integer.parseInt(idArbolDesicion));
        arbol.setReqAccion(Integer.parseInt(reqAccion));
        arbol.setReqObservacion(Integer.parseInt(reqObs));
        arbol.setReqOblig(Integer.parseInt(reqOblig));
        arbol.setDescEvidencia(etiquetaEvi);
        boolean res = arbolDecisionbi.actualizaArbolDecision(arbol);

        return res;
    }
    // http://localhost:8080/franquicia/checklistServices/updateBitacora.json?idBitacora=<?>&fechaIni=<?>&fechaFin=<?>&idCheckUsua=<?>&latitud=<?>&longitud=<?>

    @RequestMapping(value = "/updateBitacora", method = RequestMethod.GET)
    public ModelAndView updateBitacora(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String fechaInicio = request.getParameter("fechaIni");
        String fechaFin = request.getParameter("fechaFin");
        String idCheckUsua = request.getParameter("idCheckUsua");
        String latitud = request.getParameter("latitud");
        String longitud = request.getParameter("longitud");
        String idBit = request.getParameter("idBitacora");

        BitacoraDTO bitacora = new BitacoraDTO();
        bitacora.setFechaInicio(fechaInicio);
        bitacora.setFechaFin(fechaFin);
        bitacora.setIdCheckUsua(Integer.parseInt(idCheckUsua));
        bitacora.setLatitud(latitud);
        bitacora.setLongitud(longitud);
        bitacora.setIdBitacora(Integer.parseInt(idBit));
        boolean res = bitacorabi.actualizaBitacora(bitacora);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "BITACORA MODIFICADA");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/checklistServices/updateTBitacora.json
    @RequestMapping(value = "/updateTBitacora", method = RequestMethod.GET)
    public ModelAndView updateTBitacora(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        boolean res = bitacorabi.cierraBitacora();

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "BITACORAS TERMINADAS");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/checklistServices/updateRespuesta.json?idRespuesta=<?>&idArbolDesicion=<?>&idBitacora=<?>&commit=<?>&observaciones=<?>
    @RequestMapping(value = "/updateRespuesta", method = RequestMethod.GET)
    public ModelAndView updateRespuesta(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String idArbolDesicion = request.getParameter("idArbolDesicion");
        String idBitacora = request.getParameter("idBitacora");
        String commit = request.getParameter("commit");
        String idRes = request.getParameter("idRespuesta");
        String observaciones = new String(request.getParameter("observaciones").getBytes("ISO-8859-1"), "UTF-8");

        RespuestaDTO respuesta = new RespuestaDTO();
        respuesta.setIdArboldecision(Integer.parseInt(idArbolDesicion));
        respuesta.setIdBitacora(Integer.parseInt(idBitacora));
        respuesta.setCommit(Integer.parseInt(commit));
        respuesta.setIdRespuesta(Integer.parseInt(idRes));
        respuesta.setObservacion(observaciones);
        boolean res = respuestabi.actualizaRespuesta(respuesta);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "RESPUESTA MODIFICADA");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/checklistServices/updateFechaResp.json?idRespuesta=<?>&fecha=<?>&commit=<?>
    @RequestMapping(value = "/updateFechaResp", method = RequestMethod.GET)
    public ModelAndView updateFechaResp(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String idRespuesta = request.getParameter("idRespuesta");
        String fechaTermino = request.getParameter("fecha");
        String commit = request.getParameter("commit");

        RespuestaDTO respuesta = new RespuestaDTO();
        respuesta.setIdRespuesta(Integer.parseInt(idRespuesta));
        respuesta.setFechaModificacion(fechaTermino);
        respuesta.setCommit(Integer.parseInt(commit));

        boolean res = respuestabi.actualizaFechaResp(idRespuesta, fechaTermino, commit);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "RESPUESTA MODIFICADA");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/checklistServices/updateCompromiso.json?idCompromiso=<?>&commit=<?>&descripcion=<?>&estatus=<?>&fechaCompromiso=<?>&idRespuesta=<?>&idResponsable=<?>
    @RequestMapping(value = "/updateCompromiso", method = RequestMethod.GET)
    public ModelAndView updateCompromiso(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String commit = request.getParameter("commit");
        String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
        String estatus = request.getParameter("estatus");
        String fechaCompromiso = request.getParameter("fechaCompromiso");
        String idRespuesta = request.getParameter("idRespuesta");
        String idCompromiso = request.getParameter("idCompromiso");
        String idResponsable = request.getParameter("idResponsable");

        CompromisoDTO compromiso = new CompromisoDTO();
        compromiso.setCommit(Integer.parseInt(commit));
        compromiso.setDescripcion(descripcion);
        compromiso.setEstatus(Integer.parseInt(estatus));
        compromiso.setFechaCompromiso(fechaCompromiso);
        compromiso.setIdRespuesta(Integer.parseInt(idRespuesta));
        compromiso.setIdCompromiso(Integer.parseInt(idCompromiso));
        compromiso.setIdResponsable(Integer.parseInt(idResponsable));
        boolean res = compromisobi.actualizaCompromiso(compromiso);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "COMPROMISO MODIFICADO");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/checklistServices/updateEvidencia.json?idEvidencia=<?>&commit=<?>&idRespuesta=<?>&idTipo=<?>&ruta=<?>
    @RequestMapping(value = "/updateEvidencia", method = RequestMethod.GET)
    public ModelAndView updateEvidencia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String commit = request.getParameter("commit");
        String idRespuesta = request.getParameter("idRespuesta");
        String idTipo = request.getParameter("idTipo");
        String ruta = request.getParameter("ruta");
        String idEvidencia = request.getParameter("idEvidencia");

        EvidenciaDTO evidencia = new EvidenciaDTO();
        evidencia.setCommit(Integer.parseInt(commit));
        evidencia.setIdRespuesta(Integer.parseInt(idRespuesta));
        evidencia.setIdTipo(Integer.parseInt(idTipo));
        evidencia.setRuta(ruta);
        evidencia.setIdEvidencia(Integer.parseInt(idEvidencia));
        boolean idEvi = evidenciabi.actualizaEvidencia(evidencia);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "EVIDENCIA MODIFICADA");
        mv.addObject("res", idEvi);
        return mv;
    }

    //Servicio para modificar el estado del checklist
    // http://localhost:8080/franquicia/checklistServices/updateEstadoCheck.json?idCheck=<?>&estado=<?>
    @RequestMapping(value = "/updateEstadoCheck", method = RequestMethod.GET)
    public @ResponseBody
    boolean updateEstadoCheck(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String idCheck = request.getParameter("idCheck");
        String estado = request.getParameter("estado");

        //Llamo al bi para hacer la actualizacion
        boolean res = checkTbi.actualizaEdo(Integer.parseInt(idCheck), Integer.parseInt(estado));

        return res;
    }

    //Servicio para modificar posible respuesta
    // http://localhost:8080/franquicia/checklistServices/updatePosible.json?idPosible=<?>&respuesta=<?>
    @RequestMapping(value = "/updatePosible", method = RequestMethod.GET)
    public @ResponseBody
    boolean updatePosible(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String idPosible = request.getParameter("idPosible");
        String respuesta = request.getParameter("respuesta");

        PosiblesDTO posible = new PosiblesDTO();
        posible.setIdPosible(Integer.parseInt(idPosible));
        posible.setDescripcion(respuesta);

        //Llamo al bi para hacer la actualizacion
        boolean res = posiblesbi.actualizaPosible(posible);

        return res;
    }
}
