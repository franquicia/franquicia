package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.ArbolDecisionBI;
import com.gruposalinas.franquicia.business.ArbolDecisionTBI;
import com.gruposalinas.franquicia.business.AsignacionBI;
import com.gruposalinas.franquicia.business.AsignacionNewBI;
import com.gruposalinas.franquicia.business.BitacoraBI;
import com.gruposalinas.franquicia.business.ChecklistBI;
import com.gruposalinas.franquicia.business.ChecklistPreguntaBI;
import com.gruposalinas.franquicia.business.ChecklistPreguntaTBI;
import com.gruposalinas.franquicia.business.ChecklistTBI;
import com.gruposalinas.franquicia.business.ChecklistUsuarioBI;
import com.gruposalinas.franquicia.business.CompromisoBI;
import com.gruposalinas.franquicia.business.EdoChecklistBI;
import com.gruposalinas.franquicia.business.EvidenciaBI;
import com.gruposalinas.franquicia.business.ImagenesRespuestaBI;
import com.gruposalinas.franquicia.business.ModuloBI;
import com.gruposalinas.franquicia.business.ModuloTBI;
import com.gruposalinas.franquicia.business.PosibleTipoPreguntaTBI;
import com.gruposalinas.franquicia.business.PosiblesTBI;
import com.gruposalinas.franquicia.business.PreguntaBI;
import com.gruposalinas.franquicia.business.PreguntaTBI;
import com.gruposalinas.franquicia.business.RespuestaBI;
import com.gruposalinas.franquicia.business.TipoChecklistBI;
import com.gruposalinas.franquicia.business.TipoPreguntaBI;
import com.gruposalinas.franquicia.domain.ArbolDecisionDTO;
import com.gruposalinas.franquicia.domain.AsignacionDTO;
import com.gruposalinas.franquicia.domain.BitacoraDTO;
import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.ChecklistPreguntaDTO;
import com.gruposalinas.franquicia.domain.ChecklistUsuarioDTO;
import com.gruposalinas.franquicia.domain.CompromisoDTO;
import com.gruposalinas.franquicia.domain.EdoChecklistDTO;
import com.gruposalinas.franquicia.domain.EvidenciaDTO;
import com.gruposalinas.franquicia.domain.ImagenesRespuestaDTO;
import com.gruposalinas.franquicia.domain.ModuloDTO;
import com.gruposalinas.franquicia.domain.PreguntaDTO;
import com.gruposalinas.franquicia.domain.RespuestaDTO;
import com.gruposalinas.franquicia.domain.TipoChecklistDTO;
import com.gruposalinas.franquicia.domain.TipoPreguntaDTO;
import com.gruposalinas.franquicia.domain.UsuarioDTO;
import com.gruposalinas.franquicia.resources.FRQAuthInterceptor;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("/checklistServices")
public class AltaChecklistService {

    @Autowired
    ModuloBI modulobi;
    @Autowired
    TipoPreguntaBI tipPreguntabi;
    @Autowired
    PreguntaBI preguntabi;
    @Autowired
    TipoChecklistBI tipoChecklistbi;
    @Autowired
    ChecklistBI checklistbi;
    @Autowired
    ChecklistPreguntaBI checklistPreguntabi;
    @Autowired
    ChecklistUsuarioBI checklistUsuariobi;
    @Autowired
    EdoChecklistBI edoChecklistbi;
    @Autowired
    ArbolDecisionBI arbolDecisionbi;
    @Autowired
    BitacoraBI bitacorabi;
    @Autowired
    RespuestaBI respuestabi;
    @Autowired
    CompromisoBI compromisobi;
    @Autowired
    EvidenciaBI evidenciabi;
    // Temporales
    @Autowired
    ArbolDecisionTBI arbolDecisionTbi;
    @Autowired
    ChecklistTBI checkTbi;
    @Autowired
    ModuloTBI moduloTbi;
    @Autowired
    PosibleTipoPreguntaTBI posiblePreguntaTbi;
    @Autowired
    PreguntaTBI preguntaTbi;
    @Autowired
    ChecklistPreguntaTBI checklistPreguntaTbi;
    @Autowired
    PosiblesTBI posiblesTbi;
    @Autowired
    ImagenesRespuestaBI imagenesRespuestaBI;
    @Autowired
    AsignacionBI asignacionBI;

    @Autowired
    AsignacionNewBI asignacionNewBI;

    private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

    // http://localhost:8080/franquicia/checklistServices/altaModulo.json?idModuloPadre=<?>&nombreMod=<?>
    @RequestMapping(value = "/altaModulo", method = RequestMethod.GET)
    public ModelAndView altaModulo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idModuloPadre = request.getParameter("idModuloPadre");
            String nombreModulo = new String(request.getParameter("nombreMod").getBytes("ISO-8859-1"), "UTF-8");

            ModuloDTO modulo = new ModuloDTO();
            modulo.setIdModuloPadre(Integer.parseInt(idModuloPadre));
            modulo.setNombre(nombreModulo);
            int idMod = modulobi.insertaModulo(modulo);
            modulo.setIdModulo(idMod);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID DE MODULO CREADO");
            mv.addObject("res", idMod);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaModuloTemp.json?idModuloPadre=<?>&nombreMod=<?>&numVersion=<?>&tipoCambio=<?>
    @RequestMapping(value = "/altaModuloTemp", method = RequestMethod.GET)
    public ModelAndView altaModuloTemp(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            System.out.println("ALTA MODULO TEMP");
            String idModuloPadre = request.getParameter("idModuloPadre");
            String nombreModulo = new String(request.getParameter("nombreMod").getBytes("ISO-8859-1"), "UTF-8");
            String numVersion = request.getParameter("numVersion");
            String tipoCambio = request.getParameter("tipoModif");

            ModuloDTO modulo = new ModuloDTO();
            modulo.setIdModuloPadre(Integer.parseInt(idModuloPadre));
            modulo.setNombre(nombreModulo);
            modulo.setNumVersion(numVersion);
            modulo.setTipoCambio(tipoCambio);
            int idMod = moduloTbi.insertaModulo(modulo);
            modulo.setIdModulo(idMod);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID DE MODULO CREADO");
            //mv.addObject("res", idMod);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaTipoPregunta.json?cveTipoPreg=<?>&desTipoPreg=<?>&setActivo=<?>
    @RequestMapping(value = "/altaTipoPregunta", method = RequestMethod.GET)
    public ModelAndView altaTipoPregunta(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String cveTipoPreg = new String(request.getParameter("cveTipoPreg").getBytes("ISO-8859-1"), "UTF-8");
            String desTipoPreg = new String(request.getParameter("desTipoPreg").getBytes("ISO-8859-1"), "UTF-8");
            String setActivo = request.getParameter("setActivo");

            TipoPreguntaDTO tipoPregunta = new TipoPreguntaDTO();
            tipoPregunta.setCvePregunta(cveTipoPreg);
            tipoPregunta.setDescripcion(desTipoPreg);
            tipoPregunta.setActivo(Integer.parseInt(setActivo));
            int idTP = tipPreguntabi.insertaTipoPregunta(tipoPregunta);
            tipoPregunta.setIdTipoPregunta(idTP);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID DE TIPO_PREGUNTA CREADA");
            mv.addObject("res", idTP);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaPregunta.json?idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>&commit=<?>
    @RequestMapping(value = "/altaPregunta", method = RequestMethod.GET)
    public ModelAndView altaPregunta(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idMod = request.getParameter("idMod");
            String idTipo = request.getParameter("idTipo");
            String estatus = request.getParameter("estatus");
            String setPregunta = new String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
            String commit = request.getParameter("commit");

            PreguntaDTO pregunta = new PreguntaDTO();
            pregunta.setIdModulo(Integer.parseInt(idMod));
            pregunta.setIdTipo(Integer.parseInt(idTipo));
            pregunta.setEstatus(Integer.parseInt(estatus));
            pregunta.setPregunta(setPregunta);
            pregunta.setCommit(Integer.parseInt(commit));
            int idPreg = this.preguntabi.insertaPreguntas(pregunta);
            pregunta.setIdPregunta(idPreg);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID DE PREGUNTA CREADA");
            mv.addObject("res", idPreg);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaPregunta.json?idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>&commit=<?>
    @RequestMapping(value = "/altaPreguntaService", method = RequestMethod.GET)
    public @ResponseBody
    int altaPreguntaService(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idMod = request.getParameter("idMod");
            String idTipo = request.getParameter("idTipo");
            String estatus = request.getParameter("estatus");
            String setPregunta = new String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
            String commit = request.getParameter("commit");

            PreguntaDTO pregunta = new PreguntaDTO();
            pregunta.setIdModulo(Integer.parseInt(idMod));
            pregunta.setIdTipo(Integer.parseInt(idTipo));
            pregunta.setEstatus(Integer.parseInt(estatus));
            pregunta.setPregunta(setPregunta);
            pregunta.setCommit(Integer.parseInt(commit));
            int idPreg = this.preguntabi.insertaPreguntas(pregunta);
            pregunta.setIdPregunta(idPreg);
            logger.info("PREGUNTA: " + setPregunta);
            logger.info("PREGUNTA REQUEST: " + request.getParameter("setPregunta"));

            return idPreg;
        } catch (Exception e) {
            logger.info(e);
            return 0;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaPreguntaServiceTemp.json?idMod=<?>&idTipo=<?>&estatus=<?>&setPregunta=<?>&commit=<?>&numVersion=<?>&tipoCambio<?>
    @RequestMapping(value = "/altaPreguntaServiceTemp", method = RequestMethod.GET)
    public @ResponseBody
    int altaPreguntaServiceTemp(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idMod = request.getParameter("idMod");
            String idTipo = request.getParameter("idTipo");
            String estatus = request.getParameter("estatus");
            String setPregunta = new String(request.getParameter("setPregunta").getBytes("ISO-8859-1"), "UTF-8");
            String commit = request.getParameter("commit");
            String numVersion = request.getParameter("numRevision");
            String tipoCambio = request.getParameter("tipoCambio");
            String idPregunta = request.getParameter("idPregunta");

            PreguntaDTO pregunta = new PreguntaDTO();
            pregunta.setIdModulo(Integer.parseInt(idMod));
            pregunta.setIdTipo(Integer.parseInt(idTipo));
            pregunta.setEstatus(Integer.parseInt(estatus));
            pregunta.setPregunta(setPregunta);
            pregunta.setCommit(Integer.parseInt(commit));
            pregunta.setNumVersion(numVersion);
            pregunta.setTipoCambio(tipoCambio);
            pregunta.setIdPregunta(Integer.parseInt(idPregunta));
            int idPreg = this.preguntaTbi.insertaPreguntaTemp(pregunta);
            pregunta.setIdPregunta(idPreg);
            logger.info("PREGUNTA: " + setPregunta);
            logger.info("PREGUNTA REQUEST: " + request.getParameter("setPregunta"));

            return idPreg;
        } catch (Exception e) {
            logger.info(e);
            return 0;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaTipoCheck.json?descTipoCheck=<?>
    @RequestMapping(value = "/altaTipoCheck", method = RequestMethod.GET)
    public ModelAndView altaTipoChecklist(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String descTipoCheck = new String(request.getParameter("descTipoCheck").getBytes("ISO-8859-1"), "UTF-8");

            TipoChecklistDTO tipoChecklist = new TipoChecklistDTO();
            tipoChecklist.setDescTipo(descTipoCheck);
            int tChecklist = tipoChecklistbi.insertaTipoChecklist(tipoChecklist);
            tipoChecklist.setIdTipoCheck(tChecklist);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID DE DE TIPO CHECKLIST CREADO");
            mv.addObject("res", tChecklist);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/checklistServices/estadoChecklist.json?descripcion=<?>
    @RequestMapping(value = "/estadoChecklist", method = RequestMethod.GET)
    public ModelAndView estadoChecklist(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");

            EdoChecklistDTO edoCheck = new EdoChecklistDTO();
            edoCheck.setDescripcion(descripcion);
            int idEdoCheck = edoChecklistbi.insertaEdoChecklist(edoCheck);
            edoCheck.setIdEdochecklist(idEdoCheck);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID ESTADO CHECKLIST CREADO--->");
            mv.addObject("res", idEdoCheck);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaCheck.json?fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&commit=<?>
    // FORMATO FECHA INI Y FECHA FIN (AAAAMMDD)
    @RequestMapping(value = "/altaCheck", method = RequestMethod.GET)
    public ModelAndView altaCheck(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String fechaIni = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");
            String idEstado = request.getParameter("idEstado");
            String idHorario = request.getParameter("idHorario");
            String idTipoCheck = request.getParameter("idTipoCheck");
            String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
            String setVigente = request.getParameter("setVigente");
            String commit = request.getParameter("commit");

            ChecklistDTO checklist = new ChecklistDTO();
            checklist.setFecha_inicio(fechaIni);
            checklist.setFecha_fin(fechaFin);
            checklist.setIdEstado(Integer.parseInt(idEstado));
            checklist.setIdHorario(Integer.parseInt(idHorario));
            checklist.setCommit(Integer.parseInt(commit));
            TipoChecklistDTO chk = new TipoChecklistDTO();
            chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
            checklist.setIdTipoChecklist(chk);
            checklist.setNombreCheck(nombreCheck);
            checklist.setVigente(Integer.parseInt(setVigente));
            // Obtengo el ID del checklist
            int idChecklist = checklistbi.insertaChecklist(checklist);
            checklist.setIdChecklist(idChecklist);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID DE CHECKLIST");
            mv.addObject("res", idChecklist);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/checklistServices/altaCheckService.json?fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&commit=<?>&idUsuario=<?>&periodicidad=<?>&dia=<?>&version=<?>&ordenGrupo=<?>
    // FORMATO FECHA INI Y FECHA FIN (AAAAMMDD)
    @RequestMapping(value = "/altaCheckService", method = RequestMethod.GET)
    public @ResponseBody
    ChecklistDTO altaCheckService(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        try {
            String fechaIni = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");
            String idEstado = request.getParameter("idEstado");
            String idHorario = request.getParameter("idHorario");
            String idTipoCheck = request.getParameter("idTipoCheck");
            String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
            String setVigente = request.getParameter("setVigente");
            String commit = request.getParameter("commit");
            String idUsuario = request.getParameter("idUsuario");
            String periodicidad = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("periodicidad")));
            String version = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("version")));
            String ordenGrupo = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("ordenGrupo")));
            String dia = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("dia")));

            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            idUsuario = userSession.getIdUsuario();

            ChecklistDTO checklist = new ChecklistDTO();
            checklist.setFecha_inicio(fechaIni);
            checklist.setFecha_fin(fechaFin);
            checklist.setIdEstado(Integer.parseInt(idEstado));
            checklist.setIdHorario(Integer.parseInt(idHorario));
            checklist.setCommit(Integer.parseInt(commit));
            TipoChecklistDTO chk = new TipoChecklistDTO();
            chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
            checklist.setIdTipoChecklist(chk);
            checklist.setNombreCheck(nombreCheck);
            checklist.setVigente(Integer.parseInt(setVigente));
            // SE AGREGA USUARIO RESPONSABLE, DIA Y PERIDO
            checklist.setIdUsuario(Integer.parseInt(idUsuario));
            checklist.setPeriodicidad(periodicidad);
            checklist.setVersion(version);
            checklist.setOrdeGrupo(ordenGrupo);
            checklist.setDia(dia);
            // Obtengo el ID del checklist
            int idChecklist = checklistbi.insertaChecklist(checklist);
            checklist.setIdChecklist(idChecklist);

            return checklist;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/checklistServices/altaCheckTemp.json?fechaIni=<?>&fechaFin=<?>&idEstado=<?>&idHorario=<?>&idTipoCheck=<?>&nombreCheck=<?>&setVigente=<?>&commit=<?>&idCheck=<?>&numVersion=<?>&tipoCambio=<?>
    // FORMATO FECHA INI Y FECHA FIN (AAAAMMDD)
    @RequestMapping(value = "/altaCheckServiceTemp", method = RequestMethod.GET)
    public @ResponseBody
    ChecklistDTO altaCheckServiceTemp(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        try {
            String fechaIni = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");
            String idEstado = request.getParameter("idEstado");
            String idHorario = request.getParameter("idHorario");
            String idTipoCheck = request.getParameter("idTipoCheck");
            String nombreCheck = new String(request.getParameter("nombreCheck").getBytes("ISO-8859-1"), "UTF-8");
            String setVigente = request.getParameter("setVigente");
            String commit = request.getParameter("commit");
            String idCheck = request.getParameter("idCheck");
            String numVersion = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("numVersion")));
            String tipoCambio = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("tipoCambio")));

            ChecklistDTO checklist = new ChecklistDTO();
            checklist.setFecha_inicio(fechaIni);
            checklist.setFecha_fin(fechaFin);
            checklist.setIdEstado(Integer.parseInt(idEstado));
            checklist.setIdHorario(Integer.parseInt(idHorario));
            checklist.setCommit(Integer.parseInt(commit));
            TipoChecklistDTO chk = new TipoChecklistDTO();
            chk.setIdTipoCheck(Integer.parseInt(idTipoCheck));
            checklist.setIdTipoChecklist(chk);
            checklist.setNombreCheck(nombreCheck);
            checklist.setVigente(Integer.parseInt(setVigente));
            // Obtengo el ID del checklist
            checklist.setIdChecklist(Integer.parseInt(idCheck));
            checklist.setNumVersion(numVersion);
            checklist.setTipoCambio(tipoCambio);
            int idChecklist = checkTbi.insertaChecklist(checklist);
            checklist.setIdChecklist(idChecklist);
            System.out.println("NUM VERSION" + checklist.getNumVersion());
            return checklist;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/checklistServices/altaPregCheck.json?idCheck=<?>&ordenPreg=<?>&idPreg=<?>&commit=<?>&pregPadre=<?>
    @RequestMapping(value = "/altaPregCheck", method = RequestMethod.GET)
    public ModelAndView altaPreguntaCheck(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = request.getParameter("idCheck");
            String ordenPreg = request.getParameter("ordenPreg");
            String idPreg = request.getParameter("idPreg");
            String idPadre = request.getParameter("pregPadre");
            String commit = request.getParameter("commit");

            ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
            pregDTO.setIdChecklist(Integer.parseInt(idCheck));
            pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
            pregDTO.setIdPregunta(Integer.parseInt(idPreg));
            pregDTO.setCommit(Integer.parseInt(commit));
            pregDTO.setPregPadre(Integer.parseInt(idPadre));
            boolean checklistPreg = checklistPreguntabi.insertaChecklistPregunta(pregDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PREGUNTA CREADA CON EXITO--->");
            mv.addObject("res", checklistPreg);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaPregCheck.json?idCheck=<?>&ordenPreg=<?>&idPreg=<?>&commit=<?>&pregPadre=<?>
    @RequestMapping(value = "/altaPregCheckService", method = RequestMethod.GET)
    public @ResponseBody
    boolean altaPreguntaCheckService(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = request.getParameter("idCheck");
            String ordenPreg = request.getParameter("ordenPreg");
            String idPreg = request.getParameter("idPreg");
            String idPadre = request.getParameter("pregPadre");
            String commit = request.getParameter("commit");

            ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
            pregDTO.setIdChecklist(Integer.parseInt(idCheck));
            pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
            pregDTO.setIdPregunta(Integer.parseInt(idPreg));
            pregDTO.setCommit(Integer.parseInt(commit));
            pregDTO.setPregPadre(Integer.parseInt(idPadre));
            boolean checklistPreg = checklistPreguntabi.insertaChecklistPregunta(pregDTO);

            return checklistPreg;
        } catch (Exception e) {
            logger.info(e);
            return false;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaPregCheckTempService.json?idCheck=<?>&ordenPreg=<?>&idPreg=<?>&commit=<?>&pregPadre=<?>&numRevision=<?>&tipoCambio=<?>
    @RequestMapping(value = "/altaPregCheckTempService", method = RequestMethod.GET)
    public @ResponseBody
    boolean altaPreguntaCheckTempService(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = request.getParameter("idCheck");
            String ordenPreg = request.getParameter("ordenPreg");
            String idPreg = request.getParameter("idPreg");
            String idPadre = request.getParameter("pregPadre");
            String commit = request.getParameter("commit");
            String numRevision = request.getParameter("numRevision");
            String tipoCambio = request.getParameter("tipoCambio");

            ChecklistPreguntaDTO pregDTO = new ChecklistPreguntaDTO();
            pregDTO.setIdChecklist(Integer.parseInt(idCheck));
            pregDTO.setOrdenPregunta(Integer.parseInt(ordenPreg));
            pregDTO.setIdPregunta(Integer.parseInt(idPreg));
            pregDTO.setCommit(Integer.parseInt(commit));
            pregDTO.setPregPadre(Integer.parseInt(idPadre));
            pregDTO.setNumVersion(numRevision);
            pregDTO.setTipoCambio(tipoCambio);

            boolean checklistPreg = checklistPreguntaTbi.insertaChecklistPreguntaTemp(pregDTO);
            System.out.println("retorno true -->" + checklistPreg);

            return checklistPreg;
        } catch (Exception e) {
            logger.info(e);
            System.out.println("IMPRIME LOG -->");
            return false;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaUsuCheck.json?setActivo=<?>&fechaIni=<?>&fechaResp=<?>&idCeco=<?>&idChecklist=<?>&idUsu=<?>
    @RequestMapping(value = "/altaUsuCheck", method = RequestMethod.GET)
    public ModelAndView altaUsuarioCheck(HttpServletRequest request, HttpServletResponse response) {
        try {
            String setActivo = request.getParameter("setActivo");
            String fechaIni = request.getParameter("fechaIni");
            String fechaResp = request.getParameter("fechaResp");
            String idCeco = request.getParameter("idCeco");
            String idChecklist = request.getParameter("idChecklist");
            String idUsu = request.getParameter("idUsu");

            ChecklistUsuarioDTO checkUsu = new ChecklistUsuarioDTO();
            checkUsu.setActivo(Integer.parseInt(setActivo));
            checkUsu.setFechaIni(fechaIni);
            checkUsu.setFechaResp(fechaResp);
            checkUsu.setIdCeco(Integer.parseInt(idCeco));
            checkUsu.setIdChecklist(Integer.parseInt(idChecklist));
            checkUsu.setIdUsuario(Integer.parseInt(idUsu));
            int idChecklistUsu = checklistUsuariobi.insertaCheckUsuario(checkUsu);
            checkUsu.setIdChecklist(idChecklistUsu);

            //ModelAndView mv = new ModelAndView("altaModuloRes");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
            mv.addObject("tipo", "ID CHECKLIST USUARIO CREADO--->");
            mv.addObject("res", idChecklistUsu);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaUsuCheckEsp.json?idChecklist=<?>&idCeco=<?>&idUsu=<?>
    @RequestMapping(value = "/altaUsuCheckEsp", method = RequestMethod.GET)
    public ModelAndView altaUsuCheckEsp(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idCeco = request.getParameter("idCeco");
            String idChecklist = request.getParameter("idChecklist");
            String idUsu = request.getParameter("idUsu");

            boolean repuesta = checklistUsuariobi.insertaCheckusuaEspecial(Integer.parseInt(idUsu), Integer.parseInt(idChecklist), idCeco);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ID CHECKLIST-USUARIO INSERTADO -->  ");
            mv.addObject("res", repuesta);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaUsuCheckEsp2.json?idChecklist=<?>&idCeco=<?>&idUsu=<?>
    @RequestMapping(value = "/altaUsuCheckEsp2", method = RequestMethod.GET)
    public @ResponseBody
    boolean altaUsuCheckEsp2(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idCeco = request.getParameter("idCeco");
            String idChecklist = request.getParameter("idChecklist");
            String idUsu = request.getParameter("idUsu");

            boolean repuesta = checklistUsuariobi.insertaCheckusuaEspecial(Integer.parseInt(idUsu), Integer.parseInt(idChecklist), idCeco);

            return repuesta;

        } catch (Exception e) {
            logger.info(e);
            return false;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/arbolDesicion.json?estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&commit=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>
    @RequestMapping(value = "/arbolDesicion", method = RequestMethod.GET)
    public ModelAndView arbolDecision(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String estatusEvidencia = request.getParameter("estatusEvidencia");
            String idCheck = request.getParameter("idCheck");
            String idPreg = request.getParameter("idPreg");
            String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
            String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
            String reqAccion = request.getParameter("reqAccion");
            String commit = request.getParameter("commit");
            String reqObs = request.getParameter("reqObs");
            String reqOblig = request.getParameter("reqOblig");
            String etiquetaEvi = request.getParameter("etiquetaEvidencia");

            ArbolDecisionDTO arbol = new ArbolDecisionDTO();
            arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
            arbol.setIdCheckList(Integer.parseInt(idCheck));
            arbol.setIdPregunta(Integer.parseInt(idPreg));
            arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
            arbol.setRespuesta(setRes);
            arbol.setReqAccion(Integer.parseInt(reqAccion));
            arbol.setCommit(Integer.parseInt(commit));
            arbol.setReqObservacion(Integer.parseInt(reqObs));
            arbol.setReqOblig(Integer.parseInt(reqOblig));
            arbol.setDescEvidencia(etiquetaEvi);

            int resArbol = arbolDecisionbi.insertaArbolDecision(arbol);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID ARBOL  CREADO --->");
            mv.addObject("res", resArbol);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/arbolDesicion.json?estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&commit=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>
    @RequestMapping(value = "/arbolDesicionService", method = RequestMethod.GET)
    public @ResponseBody
    int arbolDecisionService(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String estatusEvidencia = request.getParameter("estatusEvidencia");
            String idCheck = request.getParameter("idCheck");
            String idPreg = request.getParameter("idPreg");
            String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
            String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
            String reqAccion = request.getParameter("reqAccion");
            String commit = request.getParameter("commit");
            String reqObs = request.getParameter("reqObs");
            String reqOblig = request.getParameter("reqOblig");
            String etiquetaEvi = request.getParameter("etiquetaEvidencia");

            ArbolDecisionDTO arbol = new ArbolDecisionDTO();
            arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
            arbol.setIdCheckList(Integer.parseInt(idCheck));
            arbol.setIdPregunta(Integer.parseInt(idPreg));
            arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
            arbol.setRespuesta(setRes);
            arbol.setReqAccion(Integer.parseInt(reqAccion));
            arbol.setCommit(Integer.parseInt(commit));
            arbol.setReqObservacion(Integer.parseInt(reqObs));
            arbol.setReqOblig(Integer.parseInt(reqOblig));
            arbol.setDescEvidencia(etiquetaEvi);

            int resArbol = arbolDecisionbi.insertaArbolDecision(arbol);

            return resArbol;
        } catch (Exception e) {
            logger.info(e);
            return 0;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/arbolDesicion.json?idArbol=<?>&estatusEvidencia=<?>&idCheck=<?>&idPreg=<?>&idOrdenCheckRes=<?>&setRes=<?>&reqAccion=<?>&commit=<?>&reqObs=<?>&reqOblig=<?>&etiquetaEvidencia=<?>&numRevision=<?>&tipoCambio=<?>
    @RequestMapping(value = "/arbolDesicionTempService", method = RequestMethod.GET)
    public @ResponseBody
    int arbolDecisionTempService(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String estatusEvidencia = request.getParameter("estatusEvidencia");
            String idCheck = request.getParameter("idCheck");
            String idPreg = request.getParameter("idPreg");
            String idOrdenCheckRes = request.getParameter("idOrdenCheckRes");
            String setRes = new String(request.getParameter("setRes").getBytes("ISO-8859-1"), "UTF-8");
            String reqAccion = request.getParameter("reqAccion");
            String commit = request.getParameter("commit");
            String reqObs = request.getParameter("reqObs");
            String reqOblig = request.getParameter("reqOblig");
            String etiquetaEvi = request.getParameter("etiquetaEvidencia");
            String numRevision = request.getParameter("numRevision");
            String tipoCambio = request.getParameter("tipoCambio");
            String idArbol = request.getParameter("idArbol");

            ArbolDecisionDTO arbol = new ArbolDecisionDTO();
            arbol.setEstatusEvidencia(Integer.parseInt(estatusEvidencia));
            arbol.setIdCheckList(Integer.parseInt(idCheck));
            arbol.setIdPregunta(Integer.parseInt(idPreg));
            arbol.setOrdenCheckRespuesta(Integer.parseInt(idOrdenCheckRes));
            arbol.setRespuesta(setRes);
            arbol.setReqAccion(Integer.parseInt(reqAccion));
            arbol.setCommit(Integer.parseInt(commit));
            arbol.setReqObservacion(Integer.parseInt(reqObs));
            arbol.setReqOblig(Integer.parseInt(reqOblig));
            arbol.setDescEvidencia(etiquetaEvi);
            arbol.setNumVersion(numRevision);
            arbol.setTipoCambio(tipoCambio);
            arbol.setIdArbolDesicion(Integer.parseInt(idArbol));
            int resArbol = arbolDecisionTbi.insertaArbolDecisionTemp(arbol);
            arbol.setIdArbolDesicion(resArbol);
            return resArbol;
            //return resArbol;
        } catch (Exception e) {
            logger.info(e);
            return 0;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaBitacora.json?fechaIni=<?>&fechaFin=<?>&idCheckUsua=<?>&latitud=<?>&longitud=<?>&modo=<?>
    @RequestMapping(value = "/altaBitacora", method = RequestMethod.GET)
    public ModelAndView altaBitacora(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String fechaInicio = request.getParameter("fechaIni");
            String fechaFin = request.getParameter("fechaFin");
            String idCheckUsua = request.getParameter("idCheckUsua");
            String latitud = request.getParameter("latitud");
            String longitud = request.getParameter("longitud");
            String modo = request.getParameter("modo");

            BitacoraDTO bitacora = new BitacoraDTO();
            bitacora.setFechaInicio(fechaInicio);
            bitacora.setFechaFin(fechaFin);
            bitacora.setIdCheckUsua(Integer.parseInt(idCheckUsua));
            bitacora.setLatitud(latitud);
            bitacora.setLongitud(longitud);
            bitacora.setModo(Integer.parseInt(modo));
            int idBitacora = bitacorabi.insertaBitacora(bitacora);
            bitacora.setIdBitacora(idBitacora);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID BITACORA CREADA-->");
            mv.addObject("res", idBitacora);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaRespuesta.json?idArbolDesicion=<?>&idBitacora=<?>&commit=<?>&observaciones=<?>
    @RequestMapping(value = "/altaRespuesta", method = RequestMethod.GET)
    public ModelAndView altaRespuesta(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idArbolDesicion = request.getParameter("idArbolDesicion");
            String idBitacora = request.getParameter("idBitacora");
            String commit = request.getParameter("commit");
            String observaciones = new String(request.getParameter("observaciones").getBytes("ISO-8859-1"), "UTF-8");

            RespuestaDTO res = new RespuestaDTO();

            res.setIdArboldecision(Integer.parseInt(idArbolDesicion));
            res.setIdBitacora(Integer.parseInt(idBitacora));
            res.setCommit(Integer.parseInt(commit));
            res.setObservacion(observaciones);
            int idRes = respuestabi.insertaRespuesta(res);
            res.setIdRespuesta(idRes);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID RESPUESTA CREADA-->");
            mv.addObject("res", idRes);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaCompromiso.json?commit=<?>&descripcion=<?>&estatus=<?>&fechaCompromiso=<?>&idRespuesta=<?>&idResponsable=<?>
    @RequestMapping(value = "/altaCompromiso", method = RequestMethod.GET)
    public ModelAndView altaCompromiso(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String commit = request.getParameter("commit");
            String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
            String estatus = request.getParameter("estatus");
            String fechaCompromiso = request.getParameter("fechaCompromiso");
            String idRespuesta = request.getParameter("idRespuesta");
            String idResponsable = request.getParameter("idResponsable");

            CompromisoDTO compr = new CompromisoDTO();
            compr.setCommit(Integer.parseInt(commit));
            compr.setDescripcion(descripcion);
            compr.setEstatus(Integer.parseInt(estatus));
            compr.setFechaCompromiso(fechaCompromiso);
            compr.setIdRespuesta(Integer.parseInt(idRespuesta));
            compr.setIdResponsable(Integer.parseInt(idResponsable));
            int comp = compromisobi.insertaCompromiso(compr);
            compr.setIdCompromiso(comp);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID COMPROMISO CREADO-->");
            mv.addObject("res", comp);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaEvidencia.json?commit=<?>&idRespuesta=<?>&idTipo=<?>&ruta=<?>
    @RequestMapping(value = "/altaEvidencia", method = RequestMethod.GET)
    public ModelAndView altaEvidencia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String commit = request.getParameter("commit");
            String idRespuesta = request.getParameter("idRespuesta");
            String idTipo = request.getParameter("idTipo");
            String ruta = request.getParameter("ruta");

            EvidenciaDTO evidencia = new EvidenciaDTO();
            evidencia.setCommit(Integer.parseInt(commit));
            evidencia.setIdRespuesta(Integer.parseInt(idRespuesta));
            evidencia.setIdTipo(Integer.parseInt(idTipo));
            evidencia.setRuta(ruta);
            int idEvi = evidenciabi.insertaEvidencia(evidencia);
            evidencia.setIdEvidencia(idEvi);
            ;

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID EVIDENCIA CREADA-->");
            mv.addObject("res", idEvi);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/asignaChecklist.json?ceco=<?>&puesto=<?>&idCheck=<?>
    @RequestMapping(value = "/asignaChecklist", method = RequestMethod.GET)
    public ModelAndView asignaCheclist(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String ceco = request.getParameter("ceco");
            String puesto = request.getParameter("puesto");
            String idCheck = request.getParameter("idCheck");

            boolean res = checklistbi.asignaChecklist(ceco, Integer.parseInt(puesto), Integer.parseInt(idCheck));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CHECKLIST ASIGNADO CORRECTAMENTE-->");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/asignaChecklist.json?ceco=<?>&puesto=<?>&idCheck=<?>
    @RequestMapping(value = "/asignaChecklistService", method = RequestMethod.GET)
    public @ResponseBody
    boolean asignaCheclistService(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String ceco = request.getParameter("ceco");
            String puesto = request.getParameter("puesto");
            String idCheck = request.getParameter("idCheck");

            boolean res = checklistbi.asignaChecklist(ceco, Integer.parseInt(puesto), Integer.parseInt(idCheck));

            return res;
        } catch (Exception e) {
            logger.info(e);
            return false;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaUsuCheck.json?setActivo=<?>&fechaIni=<?>&fechaResp=<?>&idCeco=<?>&idChecklist=<?>&idUsu=<?>
    @RequestMapping(value = "/altaUsuCheckService", method = RequestMethod.GET)
    public @ResponseBody
    int altaUsuarioCheckService(HttpServletRequest request, HttpServletResponse response) {
        try {
            String setActivo = request.getParameter("setActivo");
            String fechaIni = request.getParameter("fechaIni");
            String fechaResp = request.getParameter("fechaResp");
            String idCeco = request.getParameter("idCeco");
            String idChecklist = request.getParameter("idChecklist");
            String idUsu = request.getParameter("idUsu");

            ChecklistUsuarioDTO checkUsu = new ChecklistUsuarioDTO();
            checkUsu.setActivo(Integer.parseInt(setActivo));
            checkUsu.setFechaIni(fechaIni);
            checkUsu.setFechaResp(fechaResp);
            checkUsu.setIdCeco(Integer.parseInt(idCeco));
            checkUsu.setIdChecklist(Integer.parseInt(idChecklist));
            checkUsu.setIdUsuario(Integer.parseInt(idUsu));
            int idChecklistUsu = checklistUsuariobi.insertaCheckUsuario(checkUsu);
            checkUsu.setIdChecklist(idChecklistUsu);

            return idChecklistUsu;
        } catch (Exception e) {
            logger.info(e);
            return 0;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaImagenesArbol.json?idArbol=<?>&urlImg=<?>&descripcion=<?>
    @RequestMapping(value = "/altaImagenesArbol", method = RequestMethod.GET)
    public ModelAndView altaImagenesArbol(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idArbol = request.getParameter("idArbol");
            String urlImg = request.getParameter("urlImg");
            String descripcion = request.getParameter("descripcion");

            ImagenesRespuestaDTO imagen = new ImagenesRespuestaDTO();

            imagen.setIdArbol(Integer.parseInt(idArbol));
            imagen.setUrlImg(urlImg);
            imagen.setDescripcion(descripcion);

            int idImagen = imagenesRespuestaBI.insertaImagen(imagen);

            imagen.setIdImagen(idImagen);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID IMAGEN CREADA-->");
            mv.addObject("res", idImagen);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/altaAsignaciones.json?idChecklist=<?>&idCeco=<?>&idPuesto=<?>&activo=<?>
    @RequestMapping(value = "/altaAsignaciones", method = RequestMethod.GET)
    public ModelAndView altaAsignaciones(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idChecklist = request.getParameter("idChecklist");
            String idCeco = request.getParameter("idCeco");
            String idPuesto = request.getParameter("idPuesto");
            String activo = request.getParameter("activo");

            AsignacionDTO asignacionDTO = new AsignacionDTO();

            asignacionDTO.setIdChecklist(Integer.parseInt(idChecklist));
            asignacionDTO.setCeco(idCeco);
            asignacionDTO.setIdPuesto(Integer.parseInt(idPuesto));
            asignacionDTO.setActivo(Integer.parseInt(activo));

            boolean res = asignacionBI.insertaAsignacion(asignacionDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "RESULTADO DE INSERTAR ASIGNACION ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/franquicia/checklistServices/asignaChecklistNew.json?ceco=<?>&puesto=<?>&idCheck=<?>

    @RequestMapping(value = "/asignaChecklistNew", method = RequestMethod.GET)
    public ModelAndView asignaChecklistNew(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String ceco = request.getParameter("ceco");
            String puesto = request.getParameter("puesto");
            String idCheck = request.getParameter("idCheck");

            Map<String, Object> res = asignacionNewBI.ejecutaAsignacion(ceco, Integer.parseInt(puesto), Integer.parseInt(idCheck));

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "EJECUTE ASIGNACIONES_NEW");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/asignaChecklistEspecialNew.json?usrNew=<?>&usrOld=<?>
    @RequestMapping(value = "/asignaChecklistEspecialNew", method = RequestMethod.GET)
    public ModelAndView asignaChecklistEspecialNew(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String usrNew = request.getParameter("usrNew");
            String usrOld = request.getParameter("usrOld");

            Map<String, Object> res = asignacionNewBI.asignacionEspecial(usrNew, usrOld);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "EJECUTE ASIGNACIONES_NEW");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
}
