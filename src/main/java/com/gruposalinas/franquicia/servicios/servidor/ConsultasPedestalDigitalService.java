package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.PedestalDigitalBI;
import com.gruposalinas.franquicia.domain.LoginPDDTO;
import com.gruposalinas.franquicia.domain.MenuPedestalDigital;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("central/pedestalDigital")
public class ConsultasPedestalDigitalService {

    private static Logger logger = LogManager.getLogger(ConsultasPedestalDigitalService.class);

    @Autowired
    PedestalDigitalBI pd;

    @RequestMapping(value = "/loginRestPD", method = RequestMethod.GET)
    @ResponseBody
    public LoginPDDTO loginPedestalDigital(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {

        String usuario = (String) request.getParameter("usuario");
        LoginPDDTO login = pd.login1(Integer.parseInt(usuario));

        if (login != null) {
            return login;
        } else {
            logger.info("No esta devolviendo datos el servicio1");
        }

        return null;
    }

    @RequestMapping(value = "/loginMenuRestPD", method = RequestMethod.GET)
    public @ResponseBody
    List<MenuPedestalDigital> loginMenuPedestalDigital(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {

        List<MenuPedestalDigital> menu = null;
        String usuario = (String) request.getParameter("usuario");
        String perfil = (String) request.getParameter("perfil");
        menu = pd.login2(Integer.parseInt(usuario), Integer.parseInt(perfil));

        if (menu != null) {
            return menu;
        } else {
            logger.info("No esta devolviendo datos el servicio2");
        }

        return null;
    }
}
