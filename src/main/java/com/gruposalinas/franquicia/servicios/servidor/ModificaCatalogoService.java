package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.ArchiveroBI;
import com.gruposalinas.franquicia.business.AsignacionBI;
import com.gruposalinas.franquicia.business.CanalBI;
import com.gruposalinas.franquicia.business.CecoBI;
import com.gruposalinas.franquicia.business.EmpFijoBI;
import com.gruposalinas.franquicia.business.ExpedienteBI;
import com.gruposalinas.franquicia.business.FormArchiverosBI;
import com.gruposalinas.franquicia.business.HorarioBI;
import com.gruposalinas.franquicia.business.NegocioBI;
import com.gruposalinas.franquicia.business.NivelBI;
import com.gruposalinas.franquicia.business.PaisBI;
import com.gruposalinas.franquicia.business.ParametroBI;
import com.gruposalinas.franquicia.business.PerfilBI;
import com.gruposalinas.franquicia.business.PuestoBI;
import com.gruposalinas.franquicia.business.RecursoBI;
import com.gruposalinas.franquicia.business.RecursoPerfilBI;
import com.gruposalinas.franquicia.business.StatusFormArchiverosBI;
import com.gruposalinas.franquicia.business.SucursalBI;
import com.gruposalinas.franquicia.business.TareasBI;
import com.gruposalinas.franquicia.business.TipoArchivoBI;
import com.gruposalinas.franquicia.business.Usuario_ABI;
import com.gruposalinas.franquicia.domain.ArchiveroDTO;
import com.gruposalinas.franquicia.domain.AsignacionDTO;
import com.gruposalinas.franquicia.domain.CanalDTO;
import com.gruposalinas.franquicia.domain.CecoDTO;
import com.gruposalinas.franquicia.domain.EmpFijoDTO;
import com.gruposalinas.franquicia.domain.FormArchiverosDTO;
import com.gruposalinas.franquicia.domain.HorarioDTO;
import com.gruposalinas.franquicia.domain.NegocioDTO;
import com.gruposalinas.franquicia.domain.NivelDTO;
import com.gruposalinas.franquicia.domain.PaisDTO;
import com.gruposalinas.franquicia.domain.ParametroDTO;
import com.gruposalinas.franquicia.domain.PerfilDTO;
import com.gruposalinas.franquicia.domain.PuestoDTO;
import com.gruposalinas.franquicia.domain.RecursoDTO;
import com.gruposalinas.franquicia.domain.RecursoPerfilDTO;
import com.gruposalinas.franquicia.domain.StatusFormArchiveroDTO;
import com.gruposalinas.franquicia.domain.SucursalDTO;
import com.gruposalinas.franquicia.domain.TareaDTO;
import com.gruposalinas.franquicia.domain.TipoArchivoDTO;
import com.gruposalinas.franquicia.domain.Usuario_ADTO;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/catalogosService")
public class ModificaCatalogoService {

    private static final Logger logger = LogManager.getLogger(ModificaCatalogoService.class);

    @Autowired
    CanalBI canalbi;
    @Autowired
    PerfilBI perfilbi;
    @Autowired
    PuestoBI puestobi;
    @Autowired
    TipoArchivoBI tipoarchivobi;
    @Autowired
    CecoBI cecobi;
    @Autowired
    SucursalBI sucursalbi;
    @Autowired
    Usuario_ABI usuarioabi;
    @Autowired
    PaisBI paisbi;
    @Autowired
    HorarioBI horariobi;
    @Autowired
    ParametroBI parametrobi;
    @Autowired
    NegocioBI negociobi;
    @Autowired
    NivelBI nivelbi;
    @Autowired
    RecursoBI recursobi;
    @Autowired
    RecursoPerfilBI recursoperfilbi;
    @Autowired
    AsignacionBI asignacionBI;
    @Autowired
    TareasBI tareasBI;

    @Autowired
    EmpFijoBI empFijoBI;

    @Autowired
    ExpedienteBI expedienteBi;

    @Autowired
    StatusFormArchiverosBI statusFormArchiverosBI;
    @Autowired
    FormArchiverosBI formArchiverosBI;
    @Autowired
    ArchiveroBI archiveroBI;

    // http://localhost:8080/franquicia/catalogosService/updateCanal.json?idCanal=<?>&setActivo=<?>&descripcion=<?>
    @RequestMapping(value = "/updateCanal", method = RequestMethod.GET)
    public ModelAndView altaCanal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String activo = request.getParameter("setActivo");
        String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
        String idCanal = request.getParameter("idCanal");

        CanalDTO canal = new CanalDTO();
        canal.setActivo(Integer.parseInt(activo));
        canal.setDescrpicion(descripcion);
        canal.setIdCanal(Integer.parseInt(idCanal));
        boolean res = canalbi.actualizaCanal(canal);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "CANAL MODIFICADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updatePerfil.json?idPerfil=<?>&setDescripcion=<?>
    @RequestMapping(value = "/updatePerfil", method = RequestMethod.GET)
    public ModelAndView updatePerfil(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String idP = request.getParameter("idPerfil");
        String descripcion = new String(request.getParameter("setDescripcion").getBytes("ISO-8859-1"), "UTF-8");

        PerfilDTO perfil = new PerfilDTO();
        perfil.setDescripcion(descripcion);
        perfil.setIdPerfil(Integer.parseInt(idP));
        boolean res = perfilbi.actualizaPerfil(perfil);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "ID DE PERFIL ACTUALIZADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updatePuesto.json?idPuesto=<?>&setActivo=<?>&setCodigo=<?>&descripcion=<?>&idCanal=<?>&idNegocio=<?>&idNivel=<?>&idSubnegocio=<?>&idTipoPuesto=<?>
    @RequestMapping(value = "/updatePuesto", method = RequestMethod.GET)
    public ModelAndView updatePuesto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String activo = request.getParameter("setActivo");
        String codigo = request.getParameter("setCodigo");
        String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
        String idCanal = request.getParameter("idCanal");
        String idNegocio = request.getParameter("idNegocio");
        String idNivel = request.getParameter("idNivel");
        String idSubnegocio = request.getParameter("idSubnegocio");
        String idTipoPuesto = request.getParameter("idTipoPuesto");
        String idPuesto = request.getParameter("idPuesto");

        PuestoDTO puesto = new PuestoDTO();
        puesto.setActivo(Integer.parseInt(activo));
        puesto.setCodigo(codigo);
        puesto.setDescripcion(descripcion);
        puesto.setIdCanal(Integer.parseInt(idCanal));
        puesto.setIdNegocio(Integer.parseInt(idNegocio));
        puesto.setIdNivel(Integer.parseInt(idNivel));
        puesto.setIdSubnegocio(Integer.parseInt(idSubnegocio));
        puesto.setIdTipoPuesto(Integer.parseInt(idTipoPuesto));
        puesto.setIdPuesto(Integer.parseInt(idPuesto));
        boolean res = puestobi.actualizaPuesto(puesto);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "ID DE PUESTO ACTUALIZADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updateTipoArchivo.json?idArchivo=<?>&setNombreTipo=<?>
    @RequestMapping(value = "/updateTipoArchivo", method = RequestMethod.GET)
    public ModelAndView updateTipoArchivo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String nombre = new String(request.getParameter("setNombreTipo").getBytes("ISO-8859-1"), "UTF-8");
        String idA = request.getParameter("idArchivo");

        TipoArchivoDTO tipoArchivo = new TipoArchivoDTO();
        tipoArchivo.setNombreTipo(nombre);
        tipoArchivo.setIdTipoArchivo(Integer.parseInt(idA));
        boolean res = tipoarchivobi.actualizaTipoArchivo(tipoArchivo);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "ID TIPO ARCHIVO ACTUALIZADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updateCeco.json?idCeco=<?>&activo=<?>&calle=<?>&ciudad=<?>&cp=<?>&descCeco=<?>&faxContacto=<?>&idCanal=<?>&idCecoSuperior=<?>&idEstado=<?>&idNegocio=<?>&idNivel=<?>&idPais=<?>&nombreContacto=<?>&puestoContacto=<?>&telefonoContacto=<?>&fecha=<?>&usuarioMod=<?>
    @RequestMapping(value = "/updateCeco", method = RequestMethod.GET)
    public ModelAndView updateCeco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String activo = request.getParameter("activo");
        String calle = new String(request.getParameter("calle").getBytes("ISO-8859-1"), "UTF-8");
        String ciudad = new String(request.getParameter("ciudad").getBytes("ISO-8859-1"), "UTF-8");
        String cp = new String(request.getParameter("cp").getBytes("ISO-8859-1"), "UTF-8");
        String idCeco = request.getParameter("idCeco");
        String descCeco = new String(request.getParameter("descCeco").getBytes("ISO-8859-1"), "UTF-8");
        String faxContacto = new String(request.getParameter("faxContacto").getBytes("ISO-8859-1"), "UTF-8");
        String idCanal = request.getParameter("idCanal");
        String idCecoSuperior = request.getParameter("idCecoSuperior");
        String idEstado = request.getParameter("idEstado");
        String idNegocio = request.getParameter("idNegocio");
        String idNivel = request.getParameter("idNivel");
        String idPais = request.getParameter("idPais");
        String nombreContacto = new String(request.getParameter("nombreContacto").getBytes("ISO-8859-1"), "UTF-8");
        String puestoContacto = new String(request.getParameter("puestoContacto").getBytes("ISO-8859-1"), "UTF-8");
        String telefonoContacto = new String(request.getParameter("telefonoContacto").getBytes("ISO-8859-1"), "UTF-8");
        String fecha = request.getParameter("fecha");
        String usuarioMod = request.getParameter("usuarioMod");

        CecoDTO ceco = new CecoDTO();
        ceco.setActivo(Integer.parseInt(activo));
        ceco.setCalle(calle);
        ceco.setCiudad(ciudad);
        ceco.setCp(cp);
        ceco.setDescCeco(descCeco);
        ceco.setFaxContacto(faxContacto);
        ceco.setIdCanal(Integer.parseInt(idCanal));
        ceco.setIdCeco(idCeco);
        ceco.setIdCecoSuperior(Integer.parseInt(idCecoSuperior));
        ceco.setIdEstado(Integer.parseInt(idEstado));
        ceco.setIdNegocio(Integer.parseInt(idNegocio));
        ceco.setIdNivel(Integer.parseInt(idNivel));
        ceco.setIdPais(Integer.parseInt(idPais));
        ceco.setNombreContacto(nombreContacto);
        ceco.setPuestoContacto(puestoContacto);
        ceco.setTelefonoContacto(telefonoContacto);
        ceco.setFechaModifico(fecha);
        ceco.setUsuarioModifico(usuarioMod);
        boolean res = cecobi.actualizaCeco(ceco);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "CECO ACTUALIZADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updateSucursal.json?idSuc=<?>&idCanal=<?>&idPais=<?>&latitud=<?>&longitud=<?>&nombreSuc=<?>&nuSuc=<?>
    @RequestMapping(value = "/updateSucursal", method = RequestMethod.GET)
    public ModelAndView updateSucursal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String idCanal = request.getParameter("idCanal");
        String idPais = request.getParameter("idPais");
        String latitud = request.getParameter("latitud");
        String longitud = request.getParameter("longitud");
        String nombreSuc = new String(request.getParameter("nombreSuc").getBytes("ISO-8859-1"), "UTF-8");
        String nuSuc = request.getParameter("nuSuc");
        String idSuc = request.getParameter("idSuc");

        SucursalDTO sucursal = new SucursalDTO();
        sucursal.setIdCanal(Integer.parseInt(idCanal));
        sucursal.setIdPais(Integer.parseInt(idPais));
        sucursal.setLatitud(Double.parseDouble(latitud));
        sucursal.setLongitud(Double.parseDouble(longitud));
        sucursal.setNombresuc(nombreSuc);
        sucursal.setNuSucursal(nuSuc);
        sucursal.setIdSucursal(Integer.parseInt(idSuc));
        boolean res = sucursalbi.actualizaSucursal(sucursal);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "SUCURSAL ACTUALIZADA CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updateSucursalGCC.json?idSuc=<?>&idCanal=<?>&idPais=<?>&latitud=<?>&longitud=<?>&nombreSuc=<?>&nuSuc=<?>
    @RequestMapping(value = "/updateSucursalGCC", method = RequestMethod.GET)
    public ModelAndView updateSucursalGCC(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String idCanal = request.getParameter("idCanal");
        String idPais = request.getParameter("idPais");
        String latitud = request.getParameter("latitud");
        String longitud = request.getParameter("longitud");
        String nombreSuc = new String(request.getParameter("nombreSuc").getBytes("ISO-8859-1"), "UTF-8");
        String nuSuc = request.getParameter("nuSuc");
        String idSuc = request.getParameter("idSuc");

        SucursalDTO sucursal = new SucursalDTO();
        sucursal.setIdCanal(Integer.parseInt(idCanal));
        sucursal.setIdPais(Integer.parseInt(idPais));
        sucursal.setLatitud(Double.parseDouble(latitud));
        sucursal.setLongitud(Double.parseDouble(longitud));
        sucursal.setNombresuc(nombreSuc);
        sucursal.setNuSucursal(nuSuc);
        sucursal.setIdSucursal(Integer.parseInt(idSuc));
        boolean res = sucursalbi.actualizaSucursalGCC(sucursal);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "SUCURSAL GCC ACTUALIZADA CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updateUsuarioA.json?idUsuario=<?>&activo=<?>&fecha=<?>&idCeco=<?>&idPuesto=<?>&nombre=<?>
    @RequestMapping(value = "/updateUsuarioA", method = RequestMethod.GET)
    public ModelAndView updateUsuarioA(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String activo = request.getParameter("activo");
        String fecha = request.getParameter("fecha");
        String idCeco = request.getParameter("idCeco");
        String idPuesto = request.getParameter("idPuesto");
        String idUsuario = request.getParameter("idUsuario");
        String nombre = new String(request.getParameter("nombre").getBytes("ISO-8859-1"), "UTF-8");

        Usuario_ADTO usuarioA = new Usuario_ADTO();
        usuarioA.setActivo(Integer.parseInt(activo));
        usuarioA.setFecha(fecha);
        usuarioA.setIdCeco(idCeco);
        usuarioA.setIdPuesto(Integer.parseInt(idPuesto));
        usuarioA.setIdUsuario(Integer.parseInt(idUsuario));
        usuarioA.setNombre(nombre);
        boolean res = usuarioabi.actualizaUsuario(usuarioA);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "FRTA Usuario MODIFICADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updatePais.json?idPais=<?>&activo=<?>&nombre=<?>
    @RequestMapping(value = "/updatePais", method = RequestMethod.GET)
    public ModelAndView updatePais(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String activo = request.getParameter("activo");
        String nombre = new String(request.getParameter("nombre").getBytes("ISO-8859-1"), "UTF-8");
        String idPais = request.getParameter("idPais");

        PaisDTO pais = new PaisDTO();
        pais.setActivo(Integer.parseInt(activo));
        pais.setNombre(nombre);
        pais.setIdPais(Integer.parseInt(idPais));
        boolean res = paisbi.actualizaPais(pais);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "PAIS MODIFICADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updateHorario.json?idHorario=<?>&cveHorario=<?>&valorIni=<?>&valorFin=<?>
    @RequestMapping(value = "/updateHorario", method = RequestMethod.GET)
    public ModelAndView updateHorario(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String cveHorario = request.getParameter("cveHorario");
        String valorIni = request.getParameter("valorIni");
        String valorFin = request.getParameter("valorFin");
        String idHorario = request.getParameter("idHorario");

        HorarioDTO horario = new HorarioDTO();
        horario.setCveHorario(cveHorario);
        horario.setValorIni(valorIni);
        horario.setValorFin(valorFin);
        horario.setIdHorario(Integer.parseInt(idHorario));
        boolean res = horariobi.actualizaHorario(horario);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "HORARIO MODIFICADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updateParametro.json?setActivo=<?>&setClave=<?>&setValor=<?>
    @RequestMapping(value = "/updateParametro", method = RequestMethod.GET)
    public ModelAndView updateParametro(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String activo = request.getParameter("setActivo");
        String clave = request.getParameter("setClave");
        String valor = request.getParameter("setValor");

        ParametroDTO parametro = new ParametroDTO();
        parametro.setActivo(Integer.parseInt(activo));
        parametro.setClave(clave);
        parametro.setValor(valor);
        boolean res = parametrobi.actualizaParametro(parametro);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "PARAMETRO MODIFICADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updateNegocio.json?idNegocio<?>&setActivo=<?>&descripcion=<?>
    @RequestMapping(value = "/updateNegocio", method = RequestMethod.GET)
    public ModelAndView updateNegocio(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String activo = request.getParameter("setActivo");
        String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
        String idNegocio = request.getParameter("idNegocio");

        NegocioDTO negocio = new NegocioDTO();
        negocio.setActivo(Integer.parseInt(activo));
        negocio.setDescripcion(descripcion);
        negocio.setIdNegocio(Integer.parseInt(idNegocio));
        boolean res = negociobi.actualizaNegocio(negocio);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "NEGOCIO MODIFICADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updateNivel.json?idNivel=<?>&setActivo=<?>&codigo=<?>&descripcion=<?>&idNegocio=<?>
    @RequestMapping(value = "/updateNivel", method = RequestMethod.GET)
    public ModelAndView updateNivel(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String activo = request.getParameter("setActivo");
        String codigo = request.getParameter("codigo");
        String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
        String idNegocio = request.getParameter("idNegocio");
        String idNivel = request.getParameter("idNivel");

        NivelDTO nivel = new NivelDTO();
        nivel.setActivo(Integer.parseInt(activo));
        nivel.setCodigo(codigo);
        nivel.setDescripcion(descripcion);
        nivel.setIdNegocio(Integer.parseInt(idNegocio));
        nivel.setIdNivel(Integer.parseInt(idNivel));
        boolean res = nivelbi.actualizaNivel(nivel);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "NIVEL MODIFICADO CORRECTAMENTE");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updateRecurso.json?idRecurso=<?>&descRecurso=<?>
    @RequestMapping(value = "/updateRecurso", method = RequestMethod.GET)
    public ModelAndView updateRecurso(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String idRecurso = request.getParameter("idRecurso");
        String descRecurso = new String(request.getParameter("descRecurso").getBytes("ISO-8859-1"), "UTF-8");

        RecursoDTO recursoDTO = new RecursoDTO();
        recursoDTO.setIdRecurso(Integer.parseInt(idRecurso));
        recursoDTO.setNombreRecurso(descRecurso);

        boolean res = recursobi.actualizaRecurso(recursoDTO);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "RECURSO MODIFICADO ");
        mv.addObject("res", res);
        return mv;

    }

    // http://localhost:8080/franquicia/catalogosService/updateRecursoPerfil.json?idPerfil=<?>&idRecurso=<?>&inserta=<?>&consulta=<?>&elimina=<?>&modifca=<?>
    @RequestMapping(value = "/updateRecursoPerfil", method = RequestMethod.GET)
    public ModelAndView updateRecursoPerfil(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String idPerfil = request.getParameter("idPerfil");
        String idRecurso = request.getParameter("idRecurso");
        String consulta = request.getParameter("consulta");
        String elimina = request.getParameter("elimina");
        String inserta = request.getParameter("inserta");
        String modifica = request.getParameter("modifca");

        RecursoPerfilDTO recursoPerfilDTO = new RecursoPerfilDTO();

        recursoPerfilDTO.setIdPerfil(idPerfil);
        recursoPerfilDTO.setIdRecurso(idRecurso);
        recursoPerfilDTO.setInserta(Integer.parseInt(inserta));
        recursoPerfilDTO.setConsulta(Integer.parseInt(consulta));
        recursoPerfilDTO.setElimina(Integer.parseInt(elimina));
        recursoPerfilDTO.setModifica(Integer.parseInt(modifica));

        boolean res = recursoperfilbi.actualizaRecursoP(recursoPerfilDTO);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "RECURSO PERFIL MODIFICADO ");
        mv.addObject("res", res);
        return mv;

    }

    // http://localhost:8080/franquicia/catalogosService/updateAsignaciones.json?idChecklist=<?>&idCeco=<?>&idPuesto=<?>&activo=<?>
    @RequestMapping(value = "/updateAsignaciones", method = RequestMethod.GET)
    public ModelAndView updateAsignaciones(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String idChecklist = request.getParameter("idChecklist");
        String idCeco = request.getParameter("idCeco");
        String idPuesto = request.getParameter("idPuesto");
        String activo = request.getParameter("activo");

        AsignacionDTO asignacionDTO = new AsignacionDTO();

        asignacionDTO.setIdChecklist(Integer.parseInt(idChecklist));
        asignacionDTO.setCeco(idCeco);
        asignacionDTO.setIdPuesto(Integer.parseInt(idPuesto));
        asignacionDTO.setActivo(Integer.parseInt(activo));

        boolean res = asignacionBI.actualizaAsignacion(asignacionDTO);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "ASIGNACION MODIFICADO ");
        mv.addObject("res", res);
        return mv;

    }

    // http://localhost:8080/franquicia/catalogosService/updateObtieneTiendas.json?idTarea=<?>&claveTarea=<?>&fechaTarea=<?>&activo=<?>
    @RequestMapping(value = "/updateObtieneTiendas", method = RequestMethod.GET)
    public ModelAndView updateObtieneTiendas(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String idTarea = request.getParameter("idTarea");
        String claveTarea = request.getParameter("claveTarea");
        String fechaTarea = request.getParameter("fechaTarea");
        String activo = request.getParameter("activo");

        TareaDTO tareaDTO = new TareaDTO();
        tareaDTO.setIdTarea(Integer.parseInt(idTarea));
        tareaDTO.setCveTarea(claveTarea);
        tareaDTO.setStrFechaTarea(fechaTarea);
        tareaDTO.setActivo(Integer.parseInt(activo));

        boolean res = tareasBI.actualizaTarea(tareaDTO);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "LISTA TAREAS MODIFICADO ");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/updateEmpFijo.json?idEmpFijo=<?>&idUsuario=<?>&idActivo=<?>
    @RequestMapping(value = "/updateEmpFijo", method = RequestMethod.GET)
    public ModelAndView updateEmpFijo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String idEmpFijo = request.getParameter("idEmpFijo");
        String idUsuario = request.getParameter("idUsuario");
        String idActivo = request.getParameter("idActivo");

        EmpFijoDTO empFijoDTO = new EmpFijoDTO();
        empFijoDTO.setIdEmpFijo(Integer.parseInt(idEmpFijo));
        empFijoDTO.setIdUsuario(Integer.parseInt(idUsuario));
        empFijoDTO.setIdActivo(Integer.parseInt(idActivo));

        boolean res = empFijoBI.actualiza(empFijoDTO);

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "Se Modifico empleado" + idUsuario);
        mv.addObject("res", res);

        return mv;
    }

    // http://localhost:8080/franquicia/catalogosService/actualizaEstatusExpediente.json?idExpediente=<?>&estatus=<?>
    @RequestMapping(value = "/actualizaEstatusExpediente", method = RequestMethod.GET)
    public ModelAndView actualizaEstatusExpediente(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idExpediente = request.getParameter("idExpediente");
            String estatus = request.getParameter("estatus");

            boolean res = expedienteBi.actualizaEstatus(Integer.parseInt(estatus), Integer.parseInt(idExpediente));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Actualiza Estatus Expediente");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/modificaStatusFormArchiveros.json?idStatus=<?>&descripcion=<?>
    @RequestMapping(value = "/modificaStatusFormArchiveros", method = RequestMethod.GET)
    public ModelAndView modificaStatusFormArchiveros(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idStatus = 0;
            String idStatusS = request.getParameter("idStatus");
            if (idStatusS != null) {
                idStatus = Integer.parseInt(idStatusS);
            }

            String descripcion = request.getParameter("descripcion");

            StatusFormArchiveroDTO bean = new StatusFormArchiveroDTO();
            bean.setIdStatus(idStatus);
            bean.setDescripcion(descripcion);

            boolean res = statusFormArchiverosBI.insertaFormArchiveros(bean);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Alta Status formato archiveros");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/modificaFormArchiveros.json?idFormArchiveros=<?>&idUsuario=<?>&nombre=<?>&idCeco=<?>&idUsrRecibe=<?>&nomRecibe=<?>&idStatus=<?>
    @RequestMapping(value = "/modificaFormArchiveros", method = RequestMethod.GET)
    public ModelAndView modificaFormArchiveros(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idFormArchiveros = 0;
            String idFormArchiverosS = request.getParameter("idFormArchiveros");
            if (idFormArchiverosS != null) {
                idFormArchiveros = Integer.parseInt(idFormArchiverosS);
            }

            int idUsuario = 0;
            String idUsuarioS = request.getParameter("idUsuario");
            if (idUsuarioS != null) {
                idUsuario = Integer.parseInt(idUsuarioS);
            }

            String nombre = request.getParameter("nombre");

            String idCeco = request.getParameter("idCeco");

            int idUsrRecibe = 0;
            String idUsrRecibeS = request.getParameter("idUsrRecibe");
            if (idUsrRecibeS != null) {
                idUsrRecibe = Integer.parseInt(idUsrRecibeS);
            }

            String nomRecibe = request.getParameter("nomRecibe");

            int idStatus = 0;
            String idStatusS = request.getParameter("idStatus");
            if (idStatusS != null) {
                idStatus = Integer.parseInt(idStatusS);
            }

            String fecha = request.getParameter("fecha");

            FormArchiverosDTO bean = new FormArchiverosDTO();
            bean.setIdFromArchivero(idFormArchiveros);
            bean.setIdUsuario(idUsuario);
            bean.setNombre(nombre);
            bean.setIdCeco(idCeco);
            bean.setIdRecibe(idUsrRecibe);
            bean.setNomRecibe(nomRecibe);
            bean.setIdStatus(idStatus);
            bean.setFecha(fecha);

            boolean res = formArchiverosBI.modificaFormArchiveros(bean);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Modifica formato archiveros");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/modificaArchivero.json?idArchivero=<?>&placa=<?>&marca=<?>&descripcion=<?>&observaciones=<?>&idFormato=<?>
    @RequestMapping(value = "/modificaArchivero", method = RequestMethod.GET)
    public ModelAndView modificaArchivero(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idArchivero = 0;
            String idArchiveroS = request.getParameter("idArchivero");
            if (idArchiveroS != null) {
                idArchivero = Integer.parseInt(idArchiveroS);
            }

            String placa = request.getParameter("placa");

            String marca = request.getParameter("marca");

            String descripcion = request.getParameter("descripcion");

            String observaciones = request.getParameter("observaciones");

            int idFormato = 0;
            String idFormatoS = request.getParameter("idFormato");
            if (idFormatoS != null) {
                idFormato = Integer.parseInt(idFormatoS);
            }

            ArchiveroDTO bean = new ArchiveroDTO();
            bean.setIdArchivero(idArchivero);
            bean.setPlaca(placa);
            bean.setMarca(marca);
            bean.setDescripcion(descripcion);
            bean.setObservaciones(observaciones);
            bean.setIdFormArchivero(idFormato);

            boolean res = archiveroBI.insertaArchivero(bean);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Alta archivero");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

}
