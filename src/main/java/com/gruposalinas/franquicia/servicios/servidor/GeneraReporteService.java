package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.ReporteBI;
import com.gruposalinas.franquicia.business.VisitasxMesBI;
import com.gruposalinas.franquicia.domain.PreguntaDTO;
import com.gruposalinas.franquicia.domain.ReporteDTO;
import com.gruposalinas.franquicia.domain.VisitasxMesDTO;
import com.gruposalinas.franquicia.resources.FRQAuthInterceptor;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/reporteService")
public class GeneraReporteService {

    @Autowired
    ReporteBI reportebi;

    @Autowired
    VisitasxMesBI visitasxMesBI;

    private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

    // http://localhost:8080/franquicia/reporteService/getReporte.json?idCheck=<?>&idUsuario=<?>&nombreUsuario=<?>&nombreSucursal=<?>&fechaRespuesta=<?>&nombreCheck=<?>&metodoCheck=<?>
    @RequestMapping(value = "/getReporte", method = RequestMethod.GET)
    public ModelAndView getReporte(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        try {
            String idCheck = request.getParameter("idCheck");
            String idUsuario = request.getParameter("idUsuario");
            String nombreUsuario = request.getParameter("nombreUsuario");
            String nombreSucursal = request.getParameter("nombreSucursal");
            String fechaRespuesta = request.getParameter("fechaRespuesta");
            String nombreCheck = request.getParameter("nombreCheck");
            String metodoCheck = request.getParameter("metodoCheck");

            ServletOutputStream sos = null;
            String archivo = "";
            ReporteDTO rep = new ReporteDTO();
            rep.setIdChecklist(idCheck);
            rep.setIdUsuario(idUsuario);
            rep.setNombreUsuario(nombreUsuario);
            rep.setNombreSucursal(nombreSucursal);
            rep.setFechaRespuesta(fechaRespuesta);
            rep.setNombreChecklist(nombreCheck);
            rep.setMetodoChecklist(metodoCheck);

            List<ReporteDTO> lista = reportebi.buscaReporte(idCheck, idUsuario, nombreUsuario, nombreSucursal, fechaRespuesta, nombreCheck, metodoCheck);

            Iterator<ReporteDTO> it = lista.iterator();
            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'><tr>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center'>ID CHECKLIST</th>"
                    + "<th align='center'>ID USUARIO</th>"
                    + "<th align='center'>NOMBRE USUARIO</th>"
                    + "<th align='center'>NUMERO SUCURSAL</th>"
                    + "<th align='center'>NOMBRE CECO</th>"
                    + "<th align='center'>NUMERO REGIONAL</th>"
                    + "<th align='center'>NOMBRE REGIONAL</th>"
                    + "<th align='center'>NUMERO ZONA</th>"
                    + "<th align='center'>NOMBRE ZONA</th>"
                    + "<th align='center'>NUMERO TERRITORIO</th>"
                    + "<th align='center'>NOMBRE TERRITORIO</th>"
                    + "<th align='center'>FECHA RESPUESTA</th>"
                    + "<th align='center'>SEMANA</th>"
                    + "<th align='center'>NOMBRE MODULO PADRE</th>"
                    + "<th align='center'>NOMBRE MODULO</th>"
                    + "<th align='center'>ID PREGUNTA</th>"
                    + "<th align='center'>DESCRIPCION PREGUNTA</th>"
                    + "<th align='center'>RESPUESTA</th>"
                    + "<th align='center'>NOMBRE CHECKLIST</th>"
                    + "<th align='center'>METODO CHECKLIST</th>"
                    + "<th align='center'>OBSERVACIONES</th>"
                    + "<th align='center'>COMPROMISO</th>"
                    + "<th align='center'>FECHA COMPROMISO</th>"
                    + "<th align='center'>RESPONSABLE</th>"
                    + "<tr></thead>";
            archivo += "<tbody>";
            while (it.hasNext()) {
                ReporteDTO o = it.next();
                archivo += "<tr>"
                        + "<td align='center'>" + o.getIdChecklist() + "</td>"
                        + "<td align='center'>" + o.getIdUsuario() + "</td>"
                        + "<td align='center'>" + o.getNombreUsuario() + "</td>"
                        + "<td align='center'>" + o.getNombreSucursal() + "</td>"
                        + "<td align='center'>" + o.getNombreCeco() + "</td>"
                        + "<td align='center'>" + o.getIdRegional() + "</td>"
                        + "<td align='center'>" + o.getRegional() + "</td>"
                        + "<td align='center'>" + o.getIdZona() + "</td>"
                        + "<td align='center'>" + o.getZona() + "</td>"
                        + "<td align='center'>" + o.getIdTerritorio() + "</td>"
                        + "<td align='center'>" + o.getTerritorio() + "</td>"
                        + "<td align='center'>" + o.getFechaRespuesta() + "</td>"
                        + "<td align='center'>" + o.getSemana() + "</td>"
                        + "<td align='center'>" + o.getNombreModuloP() + "</td>"
                        + "<td align='center'>" + o.getNombreMod() + "</td>"
                        + "<td align='center'>" + o.getIdPregunta() + "</td>"
                        + "<td align='center'>" + o.getDesPregunta() + "</td>"
                        + "<td align='center'>" + o.getRespuesta() + "</td>"
                        + "<td align='center'>" + o.getNombreChecklist() + "</td>"
                        + "<td align='center'>" + o.getMetodoChecklist() + "</td>"
                        + "<td align='center'>" + o.getObservaciones() + "</td>"
                        + "<td align='center'>" + o.getCompromisos() + "</td>"
                        + "<td align='center'>" + o.getFechaCompromiso() + "</td>"
                        + "<td align='center'>" + o.getIdResposable() + "</td>"
                        + "</tr>";
            }
            archivo += "</tbody><table>";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type: application/xls; name='excel'");
            response.setHeader("Content-Disposition", "attachment; filename=reporte" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            //byte[] encode = archivo.getBytes("UTF-8");
            sos.write(archivo.getBytes());
        } catch (Exception e) {
            logger.info(e);
        }
        //ModelAndView mv = new ModelAndView("muestraServicios");
        //mv.addObject("res",lista);
        //mv.addObject("tipo", "REPORTE");

        return null;
    }

    //http://localhost:8080/franquicia/reporteService/reporteRegional.json
    //http://localhost:8080/franquicia/reporteService/reporteRegional.json?fechaIni=24/11/2016&fechaFin=28/11/2016
    @RequestMapping(value = "/reporteRegional", method = RequestMethod.GET)
    public ModelAndView reporteRegional(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String fechaIni = null;
        String fechaFin = null;

        if (request.getQueryString() != null) {
            fechaIni = request.getParameter("fechaIni");
            fechaFin = request.getParameter("fechaFin");
        }

        try {

            ServletOutputStream sos = null;
            String archivo = "";

            List<ReporteDTO> lista = reportebi.ReporteRegional(fechaIni, fechaFin);

            Iterator<ReporteDTO> it = lista.iterator();
            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                    + "<thead><tr>"
                    + "<th align='center'>Territorio Acción</th>"
                    + "<th align='center'>Zona Acci&oacute;n</th>"
                    + "<th align='center'>Regional</th>"
                    + "<th align='center'>Sucursal</th>"
                    + "<th align='center'>Fecha que se realiza</th>"
                    + "<th align='center'>Arqueo en caja principal-Diferencia</th>"
                    + "<th align='center'>Monto</th>"
                    + "<th align='center'>Motivo</th>"
                    + "<th align='center'>Fecha de cuadre</th>"
                    + "<th align='center'>Arqueo en Caja Anclada - Diferencia</th>"
                    + "<th align='center'>Monto</th>"
                    + "<th align='center'>Motivo</th>"
                    + "<th align='center'>Fecha de cuadre</th>"
                    + "</tr></thead>";
            archivo += "<tbody>";

            if (lista != null && lista.size() > 0) {
                while (it.hasNext()) {
                    ReporteDTO o = it.next();
                    archivo += "<tr><td align='center'>" + o.getTerritorio() + "</td>"
                            + "<td align='center'>" + o.getZona() + "</td>"
                            + "<td align='center'>" + o.getRegional() + "</td>"
                            + "<td align='center'>" + o.getSucursal() + "</td>"
                            + "<td align='center'>" + o.getFecha_r() + "</td>"
                            + "<td align='center'>" + o.getPregunta_cp() + "</td>"
                            + "<td align='center'>" + o.getMonto() + "</td>"
                            + "<td align='center'>" + o.getMotivo() + "</td>"
                            + "<td align='center'>" + o.getFecha_c() + "</td>"
                            + "<td align='center'>" + o.getPregunta_ca() + "</td>"
                            + "<td align='center'>" + o.getMonto_ca() + "</td>"
                            + "<td align='center'>" + o.getMotivo_ca() + "</td>"
                            + "<td align='center'>" + o.getFecha_ca() + "</td>"
                            + "</tr>";
                }
            } else {
                archivo += "<tr><td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "</tr>";
            }

            archivo += "</tbody><table>";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=reporteRegional" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            //byte[] encode = archivo.getBytes("UTF-8");
            sos.write(archivo.getBytes());
        } catch (Exception e) {
            logger.info(e);
        }
        return null;
    }

    //http://localhost:8080/franquicia/reporteService/reporteGeneral.json?idChecklist=30&fechaInicio=12/12/2016&fechaFin=12/12/2016
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/reporteGeneral", method = RequestMethod.GET)
    public @ResponseBody
    Boolean reporteGeneral(HttpServletRequest request, HttpServletResponse response) throws Exception {

        //System.out.println("Entre al controller");
        Map<String, Object> mapReporteGeneral = null;
        List<PreguntaDTO> listaPreguntas = null;
        List<ReporteDTO> listaRespuestas = null;
        List<ReporteDTO> listaRespuestasFinal = new ArrayList<ReporteDTO>();
        int idChecklist = 0;
        int conteo = 0;

        if (request.getQueryString() != null) {
            String uri = request.getQueryString();
            if (uri.contains("idChecklist")) {
                idChecklist = Integer.parseInt(request.getParameter("idChecklist"));
                String fechaInicio = null;
                String fechaFin = null;

                if (uri.contains("fechaInicio")) {
                    fechaInicio = request.getParameter("fechaInicio");
                }

                if (uri.contains("fechaFin")) {
                    fechaFin = request.getParameter("fechaFin");
                }

                try {
                    mapReporteGeneral = reportebi.ReporteGeneral(idChecklist, fechaInicio, fechaFin);
                    if (mapReporteGeneral != null) {
                        Iterator<Map.Entry<String, Object>> enMap = mapReporteGeneral.entrySet().iterator();
                        while (enMap.hasNext()) {
                            Map.Entry<String, Object> enMaps = enMap.next();
                            String nomH = enMaps.getKey();
                            if (nomH == "listaPregunta") {
                                listaPreguntas = (List<PreguntaDTO>) enMaps.getValue();
                            }
                            if (nomH == "listaRespuesta") {
                                listaRespuestas = (List<ReporteDTO>) enMaps.getValue();
                            }
                            if (nomH == "conteo") {
                                conteo = (Integer) enMaps.getValue();
                            }
                        }

                        ServletOutputStream sos = null;
                        String archivo = "";
                        String columnaTitle = "";
                        if (listaPreguntas != null && listaRespuestas != null) {
                            Iterator<PreguntaDTO> itPreguntaDTO = listaPreguntas.iterator();
                            while (itPreguntaDTO.hasNext()) {
                                PreguntaDTO preguntaDTO = itPreguntaDTO.next();
                                columnaTitle += "<th align='center'>" + preguntaDTO.getPregunta() + "</th>";
                            }

                            if (listaRespuestas.size() > 0) {

                                int numBitacoras = 1;
                                for (int a = 0; a < listaRespuestas.size() - 1; a++) {
                                    if (listaRespuestas.get(a + 1) != null) {
                                        if (listaRespuestas.get(a).getIdBitacora() != listaRespuestas.get(a + 1).getIdBitacora()) {
                                            numBitacoras++;
                                        }
                                    }
                                }

                                int k = 0;
                                for (int i = 0; i < numBitacoras; i++) {
                                    for (int j = 0; j < conteo; j++) {
                                        //logger.info("...   No. Bita "+(i+1)+"   ...   bitacora "+listaRespuestas.get(k).getIdBitacora()+"   ....   variable Preguntas "+ listaPreguntas.get(j).getIdPregunta()+"   ...   variable Respuestas "+listaRespuestas.get(k).getIdPregunta()  +"   ...");

                                        if (listaPreguntas.get(j).getIdPregunta() == Integer.parseInt(listaRespuestas.get(k).getIdPregunta())) {
                                            listaRespuestasFinal.add(listaRespuestas.get(k));
                                            if (k < listaRespuestas.size() - 1) {
                                                k++;
                                            }
                                        } else {
                                            ReporteDTO repo = new ReporteDTO();
                                            repo.setDesPregunta("vacio");
                                            repo.setRespuesta("");
                                            listaRespuestasFinal.add(repo);
                                        }
                                    }
                                }

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                int inc = 0;
                                for (int i = 0; i < numBitacoras; i++) {
                                    ReporteDTO reporteDTO = listaRespuestasFinal.get(inc);
                                    archivo += "<tr>"
                                            + "<td align='center'>" + reporteDTO.getFecha_r() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getNombreUsuario() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getIdUsuario() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getPuesto() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getPais() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getTerritorio() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getZona() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getRegional() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getSucursal() + "</td>"
                                            + "<td align='center'>" + reporteDTO.getCanal() + "</td>";

                                    for (int j = 0; j < conteo; j++) {
                                        ReporteDTO reporteDTO2 = listaRespuestasFinal.get(inc);
                                        archivo += "<td align='center'>" + reporteDTO2.getRespuesta() + "</td>";
                                        inc++;
                                    }
                                    archivo += "</tr>";
                                }

                            } else {
                                logger.info("NO EXISTEN RESPUESTAS");

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                archivo += "<tr>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>";

                                for (int j = 0; j < conteo; j++) {
                                    archivo += "<td align='center'>Sin Respuesta</td>";
                                }

                                archivo += "</tr>";
                            }
                        } else {
                            logger.info("NO EXISTEN RESPUESTAS");

                            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                    + "<thead>"
                                    + "<tr>"
                                    + "<th align='center'>Fecha que se Realizo</th>"
                                    + "<th align='center'>Quien lo realizo</th>"
                                    + "<th align='center'>Número de Usuario</th>"
                                    + "<th align='center'>Puesto</th>"
                                    + "<th align='center'>País</th>"
                                    + "<th align='center'>Territorio</th>"
                                    + "<th align='center'>Zona</th>"
                                    + "<th align='center'>Regional</th>"
                                    + "<th align='center'>Sucursal</th>"
                                    + "<th align='center'>Canal</th>"
                                    + columnaTitle
                                    + "</tr>"
                                    + "</thead>";
                            archivo += "<tbody>";

                            archivo += "<tr>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>";

                            for (int j = 0; j < conteo; j++) {
                                archivo += "<td align='center'>Sin Respuesta</td>";
                            }

                            archivo += "</tr>";
                        }
                        archivo += "</tbody><table>";
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                        response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                        response.setHeader("Content-Disposition", "attachment; filename=reporteGeneral" + fecha + ".xls");
                        response.setHeader("Pragma: no-cache", "Expires: 0");
                        sos = response.getOutputStream();
                        //byte[] encode = archivo.getBytes("UTF-8");
                        sos.write(archivo.getBytes());
                    } else {
                        logger.info("LA CONSULTA DEL CHECKLIST ESTA VACIA");
                    }
                } catch (Exception e) {
                    logger.info(e);
                }
            } else {
                logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
            }

        } else {
            logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
        }
        return null;
    }

    //-----------------------------------------------------
    //http://localhost:8080/franquicia/reporteService/reporteGeneralComp.json?idChecklist=54&fechaInicio=01/04/2017&fechaFin=07/04/2017
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/reporteGeneralComp", method = RequestMethod.GET)
    public @ResponseBody
    Boolean reporteGeneralComp(HttpServletRequest request, HttpServletResponse response) throws Exception {

        Map<String, Object> mapReporteGeneral = null;
        List<PreguntaDTO> listaPreguntas = null;
        List<ReporteDTO> listaRespuestas = null;
        List<ReporteDTO> listaRespuestasFinal = new ArrayList<ReporteDTO>();
        int idChecklist = 0;
        int conteo = 0;

        if (request.getQueryString() != null) {
            String uri = request.getQueryString();
            if (uri.contains("idChecklist")) {
                idChecklist = Integer.parseInt(request.getParameter("idChecklist"));
                String fechaInicio = null;
                String fechaFin = null;

                if (uri.contains("fechaInicio")) {
                    fechaInicio = request.getParameter("fechaInicio");
                }

                if (uri.contains("fechaFin")) {
                    fechaFin = request.getParameter("fechaFin");
                }

                try {
                    mapReporteGeneral = reportebi.ReporteGeneralCompromisos(idChecklist, fechaInicio, fechaFin);
                    if (mapReporteGeneral != null) {
                        Iterator<Map.Entry<String, Object>> enMap = mapReporteGeneral.entrySet().iterator();
                        while (enMap.hasNext()) {
                            Map.Entry<String, Object> enMaps = enMap.next();
                            String nomH = enMaps.getKey();
                            if (nomH == "listaPregunta") {
                                listaPreguntas = (List<PreguntaDTO>) enMaps.getValue();
                            }
                            if (nomH == "listaRespuesta") {
                                listaRespuestas = (List<ReporteDTO>) enMaps.getValue();
                            }
                            if (nomH == "conteo") {
                                conteo = (Integer) enMaps.getValue();
                            }
                        }

                        ServletOutputStream sos = null;
                        String archivo = "";
                        String columnaTitle = "";

                        if (listaRespuestas != null && listaPreguntas != null) {

                            Iterator<PreguntaDTO> itPreguntaDTO = listaPreguntas.iterator();
                            while (itPreguntaDTO.hasNext()) {
                                PreguntaDTO preguntaDTO = itPreguntaDTO.next();
                                columnaTitle += "<th align='center'>" + preguntaDTO.getPregunta() + "</th>";
                            }

                            if (listaRespuestas.size() > 0) {

                                int numBitacoras = 1;
                                for (int a = 0; a < listaRespuestas.size() - 1; a++) {
                                    if (listaRespuestas.get(a + 1) != null) {
                                        if (listaRespuestas.get(a).getIdBitacora() != listaRespuestas.get(a + 1).getIdBitacora()) {
                                            numBitacoras++;
                                        }
                                    }
                                }

                                int k = 0;
                                for (int i = 0; i < numBitacoras; i++) {
                                    for (int j = 0; j < conteo; j++) {
                                        //logger.info("...   No. Bita "+(i+1)+"   ...   bitacora "+listaRespuestas.get(k).getIdBitacora()+"   ....   variable Preguntas "+ listaPreguntas.get(j).getIdPregunta()+"   ...   variable Respuestas "+listaRespuestas.get(k).getIdPregunta()  +"   ...");

                                        if (listaPreguntas.get(j).getIdPregunta() == Integer.parseInt(listaRespuestas.get(k).getIdPregunta())) {
                                            listaRespuestasFinal.add(listaRespuestas.get(k));
                                            if (k < listaRespuestas.size() - 1) {
                                                k++;
                                            }
                                        } else {
                                            ReporteDTO repo = new ReporteDTO();
                                            repo.setDesPregunta("vacio");
                                            repo.setRespuesta("");
                                            listaRespuestasFinal.add(repo);
                                        }
                                    }
                                }

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                int inc = 0;
                                int auxInc = 0;
                                for (int i = 0; i < numBitacoras; i++) {
                                    auxInc = inc;
                                    for (int r = 0; r < conteo; r++) {
                                        ReporteDTO reporteDTO = listaRespuestasFinal.get(auxInc);
                                        if (reporteDTO.getFecha_r() != null) {
                                            archivo += "<tr>"
                                                    + "<td align='center'>" + reporteDTO.getFecha_r() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getNombreUsuario() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getIdUsuario() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getPuesto() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getPais() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getTerritorio() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getZona() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getRegional() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getSucursal() + "</td>"
                                                    + "<td align='center'>" + reporteDTO.getCanal() + "</td>";
                                            break;
                                        }
                                        auxInc++;
                                    }

                                    for (int j = 0; j < conteo; j++) {
                                        ReporteDTO reporteDTO2 = listaRespuestasFinal.get(inc);
                                        archivo += "<td align='center'>" + reporteDTO2.getRespuesta() + "</td>";
                                        inc++;
                                    }
                                    archivo += "</tr>";
                                }

                            } else {
                                logger.info("NO EXISTEN RESPUESTAS");

                                archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                        + "<thead>"
                                        + "<tr>"
                                        + "<th align='center'>Fecha que se Realizo</th>"
                                        + "<th align='center'>Quien lo realizo</th>"
                                        + "<th align='center'>Número de Usuario</th>"
                                        + "<th align='center'>Puesto</th>"
                                        + "<th align='center'>País</th>"
                                        + "<th align='center'>Territorio</th>"
                                        + "<th align='center'>Zona</th>"
                                        + "<th align='center'>Regional</th>"
                                        + "<th align='center'>Sucursal</th>"
                                        + "<th align='center'>Canal</th>"
                                        + columnaTitle
                                        + "</tr>"
                                        + "</thead>";
                                archivo += "<tbody>";

                                archivo += "<tr>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>"
                                        + "<td align='center'>Sin Respuesta</td>";

                                for (int j = 0; j < conteo; j++) {
                                    archivo += "<td align='center'>Sin Respuesta</td>";
                                }

                                archivo += "</tr>";
                            }
                        } else {
                            logger.info("NO EXISTEN RESPUESTAS");

                            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                                    + "<thead>"
                                    + "<tr>"
                                    + "<th align='center'>Fecha que se Realizo</th>"
                                    + "<th align='center'>Quien lo realizo</th>"
                                    + "<th align='center'>Número de Usuario</th>"
                                    + "<th align='center'>Puesto</th>"
                                    + "<th align='center'>País</th>"
                                    + "<th align='center'>Territorio</th>"
                                    + "<th align='center'>Zona</th>"
                                    + "<th align='center'>Regional</th>"
                                    + "<th align='center'>Sucursal</th>"
                                    + "<th align='center'>Canal</th>"
                                    + columnaTitle
                                    + "</tr>"
                                    + "</thead>";
                            archivo += "<tbody>";

                            archivo += "<tr>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>"
                                    + "<td align='center'>Sin Respuesta</td>";

                            for (int j = 0; j < conteo; j++) {
                                archivo += "<td align='center'>Sin Respuesta</td>";
                            }

                            archivo += "</tr>";
                        }
                        archivo += "</tbody><table>";
                        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        Date date = new Date();
                        String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
                        response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
                        response.setHeader("Content-Disposition", "attachment; filename=reporteGeneralComp" + fecha + ".xls");
                        response.setHeader("Pragma: no-cache", "Expires: 0");
                        sos = response.getOutputStream();
                        //byte[] encode = archivo.getBytes("UTF-8");
                        sos.write(archivo.getBytes());
                    } else {
                        logger.info("LA CONSULTA DEL CHECKLIST ESTA VACIA");
                    }
                } catch (Exception e) {
                    logger.info(e);
                }
            } else {
                logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
            }

        } else {
            logger.info("NO SE INGRESO EL IDCKECKLIST EN LA URL");
        }
        return null;
    }
    //-----------------------------------------------------

    //http://localhost:8080/franquicia/reporteService/reporteCheckUsua.json
    //http://localhost:8080/franquicia/reporteService/reporteCheckUsua.json?idChecklist=30&idPuesto=25
    @RequestMapping(value = "/reporteCheckUsua", method = RequestMethod.GET)
    public ModelAndView reporteCheckUsua(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String idChecklist = null;
        String idPuesto = null;

        if (request.getQueryString() != null) {
            idChecklist = request.getParameter("idChecklist");
            idPuesto = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("idPuesto")));
        }

        try {

            ServletOutputStream sos = null;
            String archivo = "";

            List<ReporteDTO> lista = reportebi.ReporteCheckUsua(idChecklist, idPuesto);

            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center'>Territorio</th>"
                    + "<th align='center'>Zona</th>"
                    + "<th align='center'>Regional</th>"
                    + "<th align='center'>Id Sucursal</th>"
                    + "<th align='center'>Nombre Sucursal</th>"
                    + "<th align='center'>Id Usuario</th>"
                    + "<th align='center'>Nombre</th>"
                    + "<th align='center'>Id Puesto</th>"
                    + "<th align='center'>Descripcion Puesto</th>"
                    + "<th align='center'>Id Checklist</th>"
                    + "<th align='center'>Nombre Checklist</th>"
                    + "</tr>"
                    + "</thead>";
            archivo += "<tbody>";

            if (lista != null && lista.size() > 0) {
                for (int i = 0; i < lista.size(); i++) {
                    ReporteDTO reporteDTO = lista.get(i);
                    archivo += "<tr>"
                            + "<td align='center'>" + reporteDTO.getTerritorio() + "</td>"
                            + "<td align='center'>" + reporteDTO.getZona() + "</td>"
                            + "<td align='center'>" + reporteDTO.getRegional() + "</td>"
                            + "<td align='center'>" + reporteDTO.getSucursal() + "</td>"
                            + "<td align='center'>" + reporteDTO.getNombreSucursal() + "</td>"
                            + "<td align='center'>" + reporteDTO.getIdUsuario() + "</td>"
                            + "<td align='center'>" + reporteDTO.getNombreUsuario() + "</td>"
                            + "<td align='center'>" + reporteDTO.getIdPuesto() + "</td>"
                            + "<td align='center'>" + reporteDTO.getPuesto() + "</td>"
                            + "<td align='center'>" + reporteDTO.getIdChecklist() + "</td>"
                            + "<td align='center'>" + reporteDTO.getNombreChecklist() + "</td>";
                    archivo += "</tr>";
                }
            } else {
                archivo += "<tr>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>";
                archivo += "</tr>";
            }

            archivo += "</tbody><table>";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=reporteCheckUsua" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            //byte[] encode = archivo.getBytes("UTF-8");
            sos.write(archivo.getBytes());
        } catch (Exception e) {
            logger.info(e);
        }
        return null;
    }

    //http://localhost:8080/franquicia/reporteService/reporteRH.json?idChecklist=40&inicio=<?>&fin=<?>
    @RequestMapping(value = "/reporteRH", method = RequestMethod.GET)
    public ModelAndView reporteRH(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String inicio = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("inicio")));
        String fin = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("fin")));
        String checklist = request.getParameter("idChecklist");

        ServletOutputStream sos = null;
        String archivo = "";
        try {

            List<VisitasxMesDTO> lista = visitasxMesBI.obtieneVisitas(Integer.parseInt(checklist), inicio, fin);

            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center'>Visitas</th>"
                    + "<th align='center'>Usuario</th>"
                    + "<th align='center'>Ceco</th>"
                    + "<th align='center'>Nombre Ceco</th>"
                    + "<th align='center'>Mes</th>"
                    + "</tr>"
                    + "</thead>";
            archivo += "<tbody>";

            if (lista != null && lista.size() > 0) {
                for (int i = 0; i < lista.size(); i++) {
                    VisitasxMesDTO reporteDTO = (VisitasxMesDTO) lista.get(i);
                    archivo += "<tr>"
                            + "<td align='center'>" + reporteDTO.getVisitas() + "</td>"
                            + "<td align='center'>" + reporteDTO.getUsuario() + "</td>"
                            + "<td align='center'>" + reporteDTO.getCeco() + "</td>"
                            + "<td align='center'>" + reporteDTO.getNombreCeco() + "</td>"
                            + "<td align='center'>" + reporteDTO.getMes() + "</td>";
                    archivo += "</tr>";
                }

            } else {
                archivo += "<tr>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>"
                        + "<td align='center'>Sin Datos</td>";
                archivo += "</tr>";
            }

            archivo += "</tbody><table>";
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");
            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=reporteVisitasRH" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            //byte[] encode = archivo.getBytes("UTF-8");
            sos.write(archivo.getBytes());
        } catch (Exception e) {
            logger.info(e);

        }

        return null;
    }

}
