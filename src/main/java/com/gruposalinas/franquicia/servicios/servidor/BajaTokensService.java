package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.TokenBI;
import com.gruposalinas.franquicia.resources.FRQAuthInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/bajaTokensService")
public class BajaTokensService {

    @Autowired
    TokenBI tokenbi;

    private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

    // http://localhost:8080/franquicia/bajaTokensService/bajaTokens.json
    @RequestMapping(value = "/bajaTokens", method = RequestMethod.GET)
    public ModelAndView bajaTokens(HttpServletRequest request, HttpServletResponse response) {
        try {
            boolean res = tokenbi.eliminaTokenTodos();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "TOKENS ELIMINADOS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
}
