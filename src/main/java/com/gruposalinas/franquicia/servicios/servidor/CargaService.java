package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.ArbolDecisionBI;
import com.gruposalinas.franquicia.business.AsignacionBI;
import com.gruposalinas.franquicia.business.AsignacionNewBI;
import com.gruposalinas.franquicia.business.CecoBI;
import com.gruposalinas.franquicia.business.ChecklistPreguntaBI;
import com.gruposalinas.franquicia.business.ChecklistTBI;
import com.gruposalinas.franquicia.business.PerfilBI;
import com.gruposalinas.franquicia.business.SucursalBI;
import com.gruposalinas.franquicia.business.Usuario_ABI;
import com.gruposalinas.franquicia.resources.FRQAuthInterceptor;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/cargaServices")
public class CargaService {

    @Autowired
    Usuario_ABI usuarioabi;
    @Autowired
    SucursalBI sucursalbi;
    @Autowired
    CecoBI cecobi;
    @Autowired
    PerfilBI perfilbi;
    @Autowired
    ArbolDecisionBI arboldecisionbi;
    @Autowired
    ChecklistPreguntaBI checklistPreguntaBI;
    @Autowired
    ChecklistTBI checkTBI;
    @Autowired
    AsignacionBI asignacionBI;
    @Autowired
    AsignacionNewBI asignacionNewBI;

    private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

    // http://localhost:8080/franquicia/cargaServices/cargaUsuarios.json
    @RequestMapping(value = "/cargaUsuarios", method = RequestMethod.GET)
    public ModelAndView cargaUsuarios(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean[] res = usuarioabi.cargaUsuarios();
            System.out.println(res.length);
            System.out.println(res[0] + "---" + res[1]);
            //boolean res = usuarioabi.cargaUsuarios();
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA USUARIOS CORRECTA");
            mv.addObject("res", res[0] + "  --- BAJA USUARIOS  " + res[1] + "  --- CARGA PERFILES  " + res[2]);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/cargaServices/cargaGeografia.json
    @RequestMapping(value = "/cargaGeografia", method = RequestMethod.GET)
    public ModelAndView cargaGeografia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cecobi.cargaGeografia();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA GEOGRAFIA CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/franquicia/cargaServices/cargaSucursales.json

    @RequestMapping(value = "/cargaSucursales", method = RequestMethod.GET)
    public ModelAndView cargaSucursales(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = sucursalbi.cargaSucursales();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA SUCURSALES CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/cargaServices/cargaCecos.json
    @RequestMapping(value = "/cargaCecos", method = RequestMethod.GET)
    public ModelAndView cargaCecos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cecobi.cargaCecos();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA CECOS CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/cargaServices/updateCecos.json
    @RequestMapping(value = "/updateCecos", method = RequestMethod.GET)
    public ModelAndView updateCecos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cecobi.updateCecos();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "UPDATE CECOS CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/cargaServices/cargaCecos2.json
    @RequestMapping(value = "/cargaCecos2", method = RequestMethod.GET)
    public ModelAndView cargaCecos2(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            boolean res = cecobi.cargaCecos2();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA CECOS2 CORRECTA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/cargaServices/cargaCecosGuatemala.json
    @RequestMapping(value = "/cargaCecosGuatemala.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosGuatemala(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = cecobi.cargaCecosGuatemala();
            System.out.println(res.length);
            System.out.println(res[0] + "---" + res[1] + "---" + res[2] + "---" + res[3]);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS GUATEMALA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/franquicia/cargaServices/cargaCecosPeru.json

    @RequestMapping(value = "/cargaCecosPeru.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosPeru(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = cecobi.cargaCecosPeru();
            System.out.println(res.length);
            System.out.println(res[0] + "---" + res[1] + "---" + res[2] + "---" + res[3]);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS PERU");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/cargaServices/cargaCecosHonduras.json
    @RequestMapping(value = "/cargaCecosHonduras.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosHonduras(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = cecobi.cargaCecosHonduras();
            System.out.println(res.length);
            System.out.println(res[0] + "---" + res[1] + "---" + res[2] + "---" + res[3]);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS HONDURAS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/cargaServices/cargaCecosPanama.json
    @RequestMapping(value = "/cargaCecosPanama.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosPanama(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = cecobi.cargaCecosPanama();
            System.out.println(res.length);
            //System.out.println(res[0] + "---" + res[1] + "---" + res[2] + "---" + res[3]);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS PANAMA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/franquicia/cargaServices/cargaCecosSalvador.json

    @RequestMapping(value = "/cargaCecosSalvador.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosSalvador(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = cecobi.cargaCecosSalvador();
            System.out.println(res.length);
            //System.out.println(res[0] + "---" + res[1] + "---" + res[2] + "---" + res[3]);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS SALVADOR");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/cargaServices/cargaCecosLAM.json
    @RequestMapping(value = "/cargaCecosLAM.json", method = RequestMethod.GET)
    public ModelAndView cargaCecosLAM(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            Map<String, Integer> res = cecobi.cargaCecosLAM();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA CECOS LAM");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/cargaServices/cargaPerfiles.json
    @RequestMapping(value = "/cargaPerfiles.json", method = RequestMethod.GET)
    public ModelAndView cargaPerfiles(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            int[] res = perfilbi.agregaNuevoPerfil();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CARGA PERFILES");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/cargaServices/cargaIdPosibles.json
    @RequestMapping(value = "/cargaIdPosibles.json", method = RequestMethod.GET)
    public ModelAndView cargaIdPosibles(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            boolean res = arboldecisionbi.cambiaRespuestasPorId();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CARGA ID POSIBLES ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/cargaServices/cargaEstatusCheckPreg.json
    @RequestMapping(value = "/cargaEstatusCheckPreg.json", method = RequestMethod.GET)
    public ModelAndView cargaEstatusCheckPreg(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            boolean res = checklistPreguntaBI.cambiaEstatus();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Cambia estatus ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/cargaServices/ejecutaAutorizados.json
    @RequestMapping(value = "/ejecutaAutorizados.json", method = RequestMethod.GET)
    public ModelAndView ejecutaAutorizados(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            boolean res = checkTBI.ejecutaAutorizados();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Ejecute Autorizados ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/cargaServices/ejecutaAsignaciones.json
    @RequestMapping(value = "/ejecutaAsignaciones.json", method = RequestMethod.GET)
    public ModelAndView ejecutaAsignaciones(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            Map<String, Object> res = asignacionBI.ejecutaAsignacion();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "EJECUTE ASIGNACIONES");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/cargaServices/ejecutaAsignacionesNew.json
    @RequestMapping(value = "/ejecutaAsignacionesNew.json", method = RequestMethod.GET)
    public ModelAndView ejecutaAsignacionesNew(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            Map<String, Object> res = asignacionNewBI.ejecutaAsignaciones();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "EJECUTE ASIGNACIONES");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            System.out.println("DENTRO DEL CATCH");
            logger.info(e);
            return null;
        }

    }

}
