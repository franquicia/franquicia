package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.CargaArchivosPedestalDigitalBI;
import com.gruposalinas.franquicia.domain.BusquedaCECOsDTO;
import com.gruposalinas.franquicia.domain.BusquedaGeoDTO;
import com.gruposalinas.franquicia.domain.CategoriasPDDTO;
import com.gruposalinas.franquicia.domain.CecosWSPDDTO;
import com.gruposalinas.franquicia.domain.DocCorruptosPDDTO;
import com.gruposalinas.franquicia.domain.DocInCategoriaPDDTO;
import com.gruposalinas.franquicia.domain.DocInCategoryPDDTO;
import com.gruposalinas.franquicia.domain.DocsBeforeUploadPDDTO;
import com.gruposalinas.franquicia.domain.DoctoDataPDDTO;
import com.gruposalinas.franquicia.domain.FileChecksumPDDTO;
import com.gruposalinas.franquicia.domain.FileDataPDDTO;
import com.gruposalinas.franquicia.domain.FileWithoutChecksumPDDTO;
import com.gruposalinas.franquicia.domain.FolioDataPDDTO;
import com.gruposalinas.franquicia.domain.FolioFilterDataPDDTO;
import com.gruposalinas.franquicia.domain.ListaCECOXGEOPDDTO;
import com.gruposalinas.franquicia.domain.ListaCECOsPDDTO;
import com.gruposalinas.franquicia.domain.ListaCategoriasPDDTO;
import com.gruposalinas.franquicia.domain.ListaCecoPorCSVPDDTO;
import com.gruposalinas.franquicia.domain.ListaDistribucionPDDTO;
import com.gruposalinas.franquicia.domain.ParamCecoPDDTO;
import com.gruposalinas.franquicia.domain.TabletDataForExcelPDDTO;
import com.gruposalinas.franquicia.domain.TabletDataPDDTO;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("central/pedestalDigital/")
public class CargaArchivosService {

    private static Logger logger = LogManager.getLogger(CargaArchivosService.class);

    @Autowired
    CargaArchivosPedestalDigitalBI cargaArchivos;

    @RequestMapping(value = "consultaPais.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<BusquedaGeoDTO> busqedaGEO(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("tipo") int tipo,
            @RequestParam("cecos") String cecos,
            @RequestParam("nivel") int nivel,
            @RequestParam("negocio") int negocio) throws Exception {

        List<BusquedaGeoDTO> listaPais = null;
        listaPais = cargaArchivos.geolocaclizacion(
                tipo,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(cecos)),
                nivel,
                negocio);

        return listaPais;
    }

    @RequestMapping(value = "consultaCecos.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<BusquedaCECOsDTO> busquedaCECOs(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("tipo") int tipo,
            @RequestParam("cecos") String cecos,
            @RequestParam("nivel") int nivel,
            @RequestParam("negocio") int negocio) throws Exception {

        List<BusquedaCECOsDTO> listacecos = null;
        listacecos = cargaArchivos.cecos(
                tipo,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(cecos)),
                nivel,
                negocio);

        return listacecos;
    }

    @RequestMapping(value = "consultaListaDistribucion.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaDistribucionPDDTO> consultaListaDisctribucion(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idLista") int idLista,
            @RequestParam("idUsuario") int idUsuario,
            @RequestParam("negocio") int negocio) throws Exception {

        List<ListaDistribucionPDDTO> lista = null;
        lista = cargaArchivos.consultaListaDistribucion(idLista, idUsuario, negocio);

        return lista;
    }

    @RequestMapping(value = "obtieneCecosPorGeo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaCECOXGEOPDDTO> obtieneCecoConGeo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("geo") String geo,
            @RequestParam("tipo") int tipo,
            @RequestParam("negocio") int negocio) throws Exception {

        List<ListaCECOXGEOPDDTO> list = null;
        list = cargaArchivos.obtieneCecoConGeo(2,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(geo)),
                tipo,
                negocio);

        return list;
    }

    @RequestMapping(value = "insertListaDistribucion.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaDistribucionPDDTO> insertaListaDistribucion(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("nombre") String nombre,
            @RequestParam("idUsuario") int idUsuario,
            @RequestParam("negocio") int negocio) throws Exception {

        List<ListaDistribucionPDDTO> lista = null;
        lista = cargaArchivos.insertDistribucion(
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(nombre)),
                idUsuario,
                negocio);

        return lista;
    }

    @RequestMapping(value = "actualizaListaDistribucion.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String actualizaListaDistribucion(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idLista") int idLista,
            @RequestParam("nombre") String nombre,
            @RequestParam("idUsuario") int idUsuario,
            @RequestParam("negocio") int negocio,
            @RequestParam("activo") int activo) throws Exception {

        String lista = null;
        // lista = cargaArchivos.actualizaDistribucion(7, "nuevolista", 320442, 41, 0);
        lista = cargaArchivos.actualizaDistribucion(idLista, nombre, idUsuario, negocio, activo);

        return lista;
    }

    @RequestMapping(value = "consultaListaCecos.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaCECOsPDDTO> consultaListaCecos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idLista") int idLista,
            @RequestParam("ceco") String ceco) throws Exception {

        List<ListaCECOsPDDTO> lista = null;
        // lista = cargaArchivos.consultaListaCECOS(7, "230001");
        lista = cargaArchivos.consultaListaCECOS(idLista,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(ceco)));

        return lista;
    }

    @RequestMapping(value = "insertaListaCecos.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaCECOsPDDTO> insertaListaCecos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idLista") int idLista,
            @RequestParam("ceco") String ceco) throws Exception {

        List<ListaCECOsPDDTO> lista = null;
        // lista = cargaArchivos.insertaListaCECOS(7, "230001");
        lista = cargaArchivos.insertaListaCECOS(idLista,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(ceco)));

        return lista;
    }

    @RequestMapping(value = "actualizaListaCecos.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaCECOsPDDTO> actualizaListaCecos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idListaCecos") int idListaCecos,
            @RequestParam("idLista") int idLista,
            @RequestParam("ceco") String ceco,
            @RequestParam("activo") int activo) throws Exception {

        List<ListaCECOsPDDTO> lista = null;
        // lista = cargaArchivos.actualizaListaCECOS(22, 7, "230001", 0);
        lista = cargaArchivos.actualizaListaCECOS(idListaCecos,
                idLista,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(ceco)),
                activo);

        return lista;
    }

    @RequestMapping(value = "obtieneCecosPorCSV.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaCecoPorCSVPDDTO> obtieneCecosPorCSV(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("csvList") String csvList,
            @RequestParam("negocio") String negocio) throws Exception {

        List<ListaCecoPorCSVPDDTO> lista = null;
        lista = cargaArchivos.obtieneCecosPorCSV(
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(csvList)),
                negocio);

        return lista;
    }

    @RequestMapping(value = "getListaCecosFD.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaCecoPorCSVPDDTO> getListaCecosFD(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("csvList") String csvList,
            @RequestParam("negocio") String negocio,
            @RequestParam("idFolio") int idFolio) throws Exception {

        List<ListaCecoPorCSVPDDTO> lista = null;
        lista = cargaArchivos.getListaCecosFD(csvList, negocio, idFolio);

        return lista;
    }

    // TODO: Servicio que se encarga de insertar lanzamientos
    @RequestMapping(value = "insertLanzCecoFD.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertLanzCecoFD(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("cecos") String cecos,
            @RequestParam("negocio") int negocio,
            @RequestParam("idDocs") String idDocs) throws Exception {

        String resp = null;
        String[] array = idDocs.split(",");

        if (array.length > 0) {
            for (String string : array) {
                resp = cargaArchivos.insertaLanzamientosPorCecos(0, cecos, negocio, Integer.parseInt(string));
            }
        }

        return resp;
    }

    @RequestMapping(value = "obtieneCategorias.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaCategoriasPDDTO> obtieneCategorias(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("nivel") int nivel,
            @RequestParam("dependeDe") int dependeDe) throws Exception {

        List<ListaCategoriasPDDTO> lista = null;
        lista = cargaArchivos.obtieneCategorias(2, nivel, dependeDe);

        return lista;
    }

    @RequestMapping(value = "obtieneDocumentos.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaCategoriasPDDTO> obtieneDocumentos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("nivel") int nivel,
            @RequestParam("dependeDe") String dependeDe) throws Exception {

        List<ListaCategoriasPDDTO> lista = null;
        lista = cargaArchivos.obtieneDocumentos(0, nivel,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(dependeDe)));

        return lista;
    }

    @RequestMapping(value = "getDocsInCategory.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaCategoriasPDDTO> getDocsInCategory(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("nivel") int nivel,
            @RequestParam("dependeDe") int dependeDe) throws Exception {

        List<ListaCategoriasPDDTO> lista = null;
        lista = cargaArchivos.obtieneCategorias(0, nivel, dependeDe);

        return lista;
    }

    /**
     * **************************************************************************
     */
    /**
     * ************** SERVICIOS PARA LA INSERCIÓN DE LANZAMIENTOS
     * ***************
     */
    /**
     * **************************************************************************
     */
    @RequestMapping(value = "insertaLanzamientosPorCecos.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertaLanzamientosPorCecos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("cecos") String cecos,
            @RequestParam("negocio") int negocio,
            @RequestParam("idDocumento") int idDocumento) throws Exception {

        String resp = null;
        resp = cargaArchivos.insertaLanzamientosPorCecos(0, cecos, negocio, idDocumento);

        return resp;
    }

    @RequestMapping(value = "insertaLanzamientosPorGeo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertaLanzamientosPorGeo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("geo") String geo,
            @RequestParam("negocio") int negocio,
            @RequestParam("tipo") int tipo,
            @RequestParam("idDocumento") int idDocumento) throws NumberFormatException, Exception {

        String resp = null;
        resp = cargaArchivos.insertaLanzamientosPorGeo(1, geo, negocio, tipo, idDocumento);

        return resp;
    }

    @RequestMapping(value = "insertaLanzamientosPorListaDD.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertaLanzamientosPorListaDD(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idLista") int idLista,
            @RequestParam("negocio") int negocio,
            @RequestParam("idDocumento") int idDocumento) throws NumberFormatException, Exception {

        String resp = null;
        resp = cargaArchivos.insertaLanzamientosPorListaDD(2, idLista, negocio, idDocumento);

        return resp;
    }

    /**
     * *************************************************************************
     */
    /**
     * *********************** SERVICIOS PARA LA TABLET
     * ************************
     */
    /**
     * *************************************************************************
     */
    @RequestMapping(value = "insertCategory.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertCategory(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idCategoria") int idCategoria,
            @RequestParam("descripcion") String descripcion,
            @RequestParam("rutaIcono") String rutaIcono,
            @RequestParam("nombreIcono") String nombreIcono,
            @RequestParam("orden") int orden) throws NumberFormatException, Exception {

        String resp = null;
        resp = cargaArchivos.insertCategory(0, idCategoria, descripcion, rutaIcono, nombreIcono, orden);

        return resp;
    }

    @RequestMapping(value = "updateCategory.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateCategory(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idCategoria") int idCategoria,
            @RequestParam("descripcion") String descripcion,
            @RequestParam("rutaIcono") String rutaIcono,
            @RequestParam("nombreIcono") String nombreIcono,
            @RequestParam("orden") int orden) throws NumberFormatException, Exception {

        String resp = null;
        resp = cargaArchivos.updateCategory(1, idCategoria, descripcion, rutaIcono, nombreIcono, orden);

        return resp;
    }

    @RequestMapping(value = "getCategory.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<CategoriasPDDTO> getCategory(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idCategoria") int idCategoria) throws NumberFormatException, Exception {

        List<CategoriasPDDTO> lista = null;
        lista = cargaArchivos.getCategory(2, idCategoria);

        return lista;
    }

    @RequestMapping(value = "insertDocInCategory.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertDocInCategory(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idDoc") int idDoc,
            @RequestParam("descripcion") String descripcion,
            @RequestParam("idCategoria") int idCategoria,
            @RequestParam("tipo") String tipo,
            @RequestParam("activo") int activo,
            @RequestParam("orden") int orden) throws NumberFormatException, Exception {

        String resp = null;
        String _desc = new String(descripcion.getBytes("ISO-8859-1"), "utf-8");
        resp = cargaArchivos.insertDocInCategory(0, idDoc, _desc, idCategoria, tipo, activo, orden);

        return resp;
    }

    @RequestMapping(value = "updateDocInCategory.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateDocInCategory(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idDoc") int idDoc,
            @RequestParam("descripcion") String descripcion,
            @RequestParam("idCategoria") int idCategoria,
            @RequestParam("tipo") String tipo,
            @RequestParam("activo") int activo,
            @RequestParam("orden") int orden) throws NumberFormatException, Exception {

        String resp = null;
        String _desc = new String(descripcion.getBytes("ISO-8859-1"), "utf-8");
        resp = cargaArchivos.updateDocInCategory(1, idDoc, _desc, idCategoria, tipo, activo, orden);

        return resp;
    }

    @RequestMapping(value = "getDocInCategory.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DocInCategoriaPDDTO> getDocInCategory(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idDoc") int idDoc,
            @RequestParam("idCategoria") int idCategoria) throws NumberFormatException, Exception {

        List<DocInCategoriaPDDTO> resp = null;
        resp = cargaArchivos.getDocInCategory(2, idDoc, idCategoria);

        return resp;
    }

    @RequestMapping(value = "insertFileInfo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertFileInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idArchivo") int idArchivo,
            @RequestParam("idItem") int idItem,
            @RequestParam("ruta") String ruta,
            @RequestParam("nombre") String nombre,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {

        String resp = null;
        String _nom = new String(nombre.getBytes("ISO-8859-1"), "utf-8");
        resp = cargaArchivos.insertFileInfo(0, idArchivo, idItem, ruta, _nom, activo);

        return resp;
    }

    @RequestMapping(value = "updateFileInfo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateFileInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idArchivo") int idArchivo,
            @RequestParam("idItem") int idItem,
            @RequestParam("ruta") String ruta,
            @RequestParam("nombre") String nombre,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {

        String resp = null;
        String _nom = new String(nombre.getBytes("ISO-8859-1"), "utf-8");
        resp = cargaArchivos.updateFileInfo(1, idArchivo, idItem, ruta, _nom, activo);

        return resp;
    }

    @RequestMapping(value = "getFileInfo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<FileDataPDDTO> getFileInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idArchivo") int idArchivo) throws NumberFormatException, Exception {

        List<FileDataPDDTO> resp = null;
        resp = cargaArchivos.getFileInfo(2, idArchivo);

        return resp;
    }

    /**
     * ************************************************************************
     */
    /**
     * ************************ SERVICIOS PARA FOLIOS *************************
     */
    /**
     * ************************************************************************
     */
    @RequestMapping(value = "insertFolio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertFolio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idFolio") int idFolio,
            @RequestParam("tipo") String tipo,
            @RequestParam("fecha") String fecha,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {

        int op = 0;
        String resp = null;

        try {
            resp = cargaArchivos.insertFolio(op, idFolio,
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(tipo)),
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fecha)),
                    activo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "updateFolio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateFolio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idFolio") int idFolio,
            @RequestParam("tipo") String tipo,
            @RequestParam("fecha") String fecha,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {

        int op = 1;
        String resp = null;

        try {
            resp = cargaArchivos.updateFolio(op, idFolio, tipo, fecha, activo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "getFolio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<FolioDataPDDTO> getFolio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idFolio") int idFolio) throws NumberFormatException, Exception {

        int op = 2;
        List<FolioDataPDDTO> lista = null;

        try {
            lista = cargaArchivos.getFolio(op, idFolio);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lista;
    }

    /**
     * ********************************************************
     */
    /**
     * ******* SERVICIOS PARA ADMINISTRAR DOCUMENTOS *********
     */
    /**
     * ********************************************************
     */
    @RequestMapping(value = "insertDocto.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertDocto(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idDocumento") int idDocumento,
            @RequestParam("idFolio") int idFolio,
            @RequestParam("nombreDocto") String nombreArchivo,
            @RequestParam("idTipoDocto") int idTipoDocto,
            @RequestParam("idUsuario") int idUsuario,
            @RequestParam("pesoBytes") long pesoBytes,
            @RequestParam("origen") String origen,
            @RequestParam("vigenciaIni") String vigenciaIni,
            @RequestParam("vigenciaFin") String vigenciaFin,
            @RequestParam("visibleSuc") String visibleSuc,
            @RequestParam("idEstatus") int idEstatus,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {

        int op = 0;
        String resp = null;

        try {
            String _nom = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(new String(nombreArchivo.getBytes("ISO-8859-1"), "utf-8")));
            resp = cargaArchivos.insertDocto(op, idDocumento, idFolio, _nom, idTipoDocto, idUsuario, pesoBytes,
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(origen)),
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(vigenciaIni)),
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(vigenciaFin)),
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(visibleSuc)), idEstatus, activo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "updateDocto.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateDocto(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idDocumento") int idDocumento,
            @RequestParam("idFolio") int idFolio,
            @RequestParam("nombreDocto") String nombreArchivo,
            @RequestParam("idTipoDocto") int idTipoDocto,
            @RequestParam("idUsuario") int idUsuario,
            @RequestParam("pesoBytes") long pesoBytes,
            @RequestParam("origen") String origen,
            @RequestParam("vigenciaIni") String vigenciaIni,
            @RequestParam("vigenciaFin") String vigenciaFin,
            @RequestParam("visibleSuc") String visibleSuc,
            @RequestParam("idEstatus") int idEstatus,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {

        int op = 1;
        String resp = null;

        try {
            String _nom = new String(nombreArchivo.getBytes("ISO-8859-1"), "utf-8");
            resp = cargaArchivos.updateDocto(op, idDocumento, idFolio, _nom, idTipoDocto, idUsuario, pesoBytes, origen, vigenciaIni, vigenciaFin, visibleSuc, idEstatus, activo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "getDocto.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DoctoDataPDDTO> getDocto(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idDocumento") int idDocumento) throws NumberFormatException, Exception {

        int op = 2;
        List<DoctoDataPDDTO> lista = null;

        try {
            lista = cargaArchivos.getDocto(op, idDocumento);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lista;
    }

    /**
     * **************************************************************************
     */
    /**
     * ************************ SERVICIOS PARA TABLETAS
     * *************************
     */
    /**
     * **************************************************************************
     */
    @RequestMapping(value = "insertTabletData.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertTablet(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTableta") int idTableta,
            @RequestParam("numSerie") String numSerie,
            @RequestParam("idCeco") String idCeco,
            @RequestParam("idUsuario") int idUsuario,
            @RequestParam("idEstatus") int idEstatus,
            @RequestParam("fechaInstalacion") String fechaInstalacion,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {

        int op = 0;
        String resp = null;

        try {
            resp = cargaArchivos.insertTablet(op, idTableta, numSerie, idCeco, idUsuario, idEstatus, fechaInstalacion, activo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "updateTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateTablet(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTableta") int idTableta,
            @RequestParam("numSerie") String numSerie,
            @RequestParam("idCeco") String idCeco,
            @RequestParam("idUsuario") int idUsuario,
            @RequestParam("idEstatus") int idEstatus,
            @RequestParam("fechaInstalacion") String fechaInstalacion,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {

        int op = 1;
        String resp = null;

        try {
            resp = cargaArchivos.updateTablet(op, idTableta, numSerie, idCeco, idUsuario, idEstatus, fechaInstalacion, activo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "getTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TabletDataPDDTO> getTablet(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTableta") int idTableta,
            @RequestParam("numSerie") String numSerie) throws NumberFormatException, Exception {

        int op = 2;
        List<TabletDataPDDTO> resp = null;

        try {
            if (idTableta == 0) {
                resp = cargaArchivos.getTablet(op, idTableta,
                        ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(numSerie)));
            } else if (idTableta > 0) {
                resp = cargaArchivos.getTablet(op, idTableta, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    /**
     * *****************************************************************************
     */
    /**
     * *************** SERVICIOS MODULO DE ADMINISTRACION DE ENVIOS
     * ****************
     */
    /**
     * *****************************************************************************
     */
    @RequestMapping(value = "getFolioData.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<FolioFilterDataPDDTO> getFolioDataPorFolio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idFolio") int idFolio) throws NumberFormatException, Exception {

        int op = 0;
        List<FolioFilterDataPDDTO> resp = null;

        try {
            resp = cargaArchivos.getFolioDataPorFolio(op, idFolio);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "getFolioDataPorTerritorio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<FolioFilterDataPDDTO> getFolioDataPorTerritorio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("ceco") String ceco,
            @RequestParam("categoria") String categoria,
            @RequestParam("fecha") String fecha,
            @RequestParam("estatus") String estatus,
            @RequestParam("tipo") String tipo) throws NumberFormatException, Exception {

        int op = 0;
        List<FolioFilterDataPDDTO> resp = null;

        try {
            resp = cargaArchivos.getFolioDataPorTerritorio(op,
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(ceco)),
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(categoria)),
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fecha)),
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(estatus)),
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(tipo)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "convertEconNumToCeco.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<String> convertEconNumToCeco(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("numEco") String numEco) throws NumberFormatException, Exception {

        int op = 1;
        List<String> resp = null;

        try {
            resp = cargaArchivos.convertEconNumToCeco(op, ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(numEco)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "getFolioDataPorGeografia.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<FolioFilterDataPDDTO> getFolioDataPorGeografia(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("geo") String geo,
            @RequestParam("nivel") int nivel,
            @RequestParam("categoria") String categoria,
            @RequestParam("fecha") String fecha,
            @RequestParam("estatus") String estatus,
            @RequestParam("tipo") String tipo) throws NumberFormatException, Exception {

        int op = 0;
        List<FolioFilterDataPDDTO> resp = null;

        try {
            resp = cargaArchivos.getFolioDataPorGeografia(op,
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(geo)),
                    nivel,
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(categoria)),
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fecha)),
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(estatus)),
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(tipo)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    // Servicio para obtener el ultimo documento (inmediato superior) a partir del tipo de documento que se le envie
    @RequestMapping(value = "getLastestDoc.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getLastestDoc(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idDocto") int idDocto) throws NumberFormatException, Exception {

        int op = 3;
        String resp = null;

        try {
            resp = cargaArchivos.getLastestDoc(op, idDocto);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    /**
     * *******************************************************************
     */
    /**
     * ** SERVICIO PARA EXPORTAR DATOS DE TABLETAS EN ESTATUS GENERAL ****
     */
    /**
     * *******************************************************************
     */
    @RequestMapping(value = "getTablaEstatusGeneralExcel.json", method = RequestMethod.GET)
    public @ResponseBody
    Boolean getTablaEstatusGeneralExcel(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {

        int op = 2;
        ServletOutputStream sos = null;
        List<TabletDataPDDTO> resp = null;
        StringBuilder archivo = new StringBuilder();

        try {
            resp = cargaArchivos.getTablet(op, 0, "0");

            archivo.append("<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse: collapse;'>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center'>Número de Serie</th>"
                    + "<th align='center'>CECO</th>"
                    + "<th align='center'>idUsuario</th>"
                    + "<th align='center'>idEstatusTableta</th>"
                    + "<th align='center'>Fecha Instalación</th>"
                    + "<th align='center'>Activo</th>"
                    + "<th align='center'>idTableta</th>"
                    + "<th align='center'>Usuario_Modifica</th>"
                    + "<th align='center'>Fecha_Modifica</th>"
                    + "</tr>"
                    + "</thead>"
                    + "<tbody>");

            for (TabletDataPDDTO tabletDataPDDTO : resp) {
                archivo.append("<tr>");
                archivo.append("<td>" + tabletDataPDDTO.getNumSerie() + "</td>");
                archivo.append("<td>" + tabletDataPDDTO.getIdCeco() + "</td>");
                archivo.append("<td>" + tabletDataPDDTO.getIdUsuario() + "</td>");
                archivo.append("<td>" + tabletDataPDDTO.getIdEstatus() + "</td>");
                archivo.append("<td>" + tabletDataPDDTO.getFechaInstalacion() + "</td>");
                archivo.append("<td>" + tabletDataPDDTO.getActivo() + "</td>");
                archivo.append("<td>" + tabletDataPDDTO.getIdTableta() + "</td>");
                archivo.append("<td>" + tabletDataPDDTO.getUsuarioModifica() + "</td>");
                archivo.append("<td>" + tabletDataPDDTO.getFechaModifica() + "</td>");
                archivo.append("</tr>");
            }

            archivo.append("</tbody>");
            archivo.append("</table>");

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=reporte_tabletas_" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            sos.write(archivo.toString().getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * ******************************************************************************************************
     */
    /**
     * ** SERVICIO QUE OBTIENE DATOS DE TABLETAS ALMACENADAS EN BD Y LAS
     * DEVUELVE EN UN ARCHIVO DE EXCEL ****
     */
    /**
     * ******************************************************************************************************
     */
    @RequestMapping(value = "getTabletExcel.json", method = RequestMethod.GET)
    public @ResponseBody
    Boolean getTabletExcel(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {

        ServletOutputStream sos = null;
        List<TabletDataForExcelPDDTO> data = null;
        String archivo = "";

        try {
            data = cargaArchivos.getAllTabletsForExcel(4);

            archivo = "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse: collapse;'>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center'>idTableta</th>"
                    + "<th align='center'>Número de Serie</th>"
                    + "<th align='center'>CECO</th>"
                    + "<th align='center'>Desc. CECO</th>"
                    + "<th align='center'>idUsuario</th>"
                    + "<th align='center'>Fabricante</th>"
                    + "<th align='center'>Modelo</th>"
                    + "<th align='center'>Sistema Operativo</th>"
                    + "<th align='center'>Version</th>"
                    + "<th align='center'>Estatus</th>"
                    + "<th align='center'>Activo</th>"
                    + "<th align='center'>Fecha Instalación</th>"
                    + "<th align='center'>Fecha Cambio</th>"
                    + "<th align='center'>Usuario_Modifica</th>"
                    + "</tr>"
                    + "</thead>"
                    + "<tbody>";

            for (TabletDataForExcelPDDTO tabletDataPDDTO : data) {
                archivo += "<tr>";
                archivo += "<td>" + tabletDataPDDTO.getIdTableta() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getNumSerie() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getIdCeco() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getDescCeco() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getIdUsuario() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getFabricante() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getModelo() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getSistemaOp() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getVersion() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getIdEstatus() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getActivo() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getFechaInstalacion() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getFechaCambio() + "</td>";
                archivo += "<td>" + tabletDataPDDTO.getUsuario() + "</td>";
                archivo += "</tr>";
            }

            archivo += "</tbody>";
            archivo += "</table>";

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=reporte_tabletas_" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            sos.write(archivo.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * *********************************************************************
     */
    /**
     * *********** SERVICIOS PARA EL FLUJO DE CARGA DE ARCHIVOS ************
     */
    /**
     * *********************************************************************
     */
    @RequestMapping(value = "insertAdmonTipoDoc.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertAdmonTipoDoc(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTipoDoc") int idTipoDoc,
            @RequestParam("nombre") String nombre,
            @RequestParam("nivel") int nivel,
            @RequestParam("dependeDe") int dependeDe,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {

        int op = 0;
        String resp = null;

        try {
            resp = cargaArchivos.insertAdmonTipoDoc(op, idTipoDoc,
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(nombre)), nivel, dependeDe, activo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "updateAdmonTipoDoc.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateAdmonTipoDoc(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTipoDoc") int idTipoDoc,
            @RequestParam("nombre") String nombre,
            @RequestParam("nivel") int nivel,
            @RequestParam("dependeDe") int dependeDe,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {

        int op = 1;
        String resp = null;

        try {
            resp = cargaArchivos.updateAdmonTipoDoc(op, idTipoDoc,
                    ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(nombre)), nivel, dependeDe, activo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "getAdmonTipoDoc.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DocInCategoryPDDTO> getAdmonTipoDoc(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTipoDoc") int idTipoDoc) throws NumberFormatException, Exception {

        int op = 2;
        List<DocInCategoryPDDTO> resp = null;

        try {
            resp = cargaArchivos.getAdmonTipoDoc(op, idTipoDoc);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    /**
     * Servicio que inserta / actualiza el checksum de un archivo
     *
     */
    @RequestMapping(value = "insertUpdateFileChecksum.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertUpdateFileChecksum(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "op", required = true) Integer op,
            @RequestParam(value = "idEncArch", required = false) Integer idEncArch,
            @RequestParam(value = "idRef", required = false) Integer idRef,
            @RequestParam(value = "origen", required = false) String origen,
            @RequestParam(value = "checksum", required = true) String checksum,
            @RequestParam(value = "activo", required = false) Integer activo) throws Exception {

        String resp = cargaArchivos.insertUpdateFileChecksum(op, idEncArch, idRef, origen, checksum, activo);
        return resp;
    }

    /**
     * Servicio que consulta el checksum de un archivo
     */
    @RequestMapping(value = "getFileChecksum.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<FileChecksumPDDTO> getFileChecksum(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "op", required = false) Integer op,
            @RequestParam(value = "idEncArch", required = false) Integer idEncArch,
            @RequestParam(value = "idRef", required = false) Integer idRef,
            @RequestParam(value = "origen", required = false) String origen,
            @RequestParam(value = "checksum", required = false) String checksum,
            @RequestParam(value = "activo", required = false) Integer activo) throws Exception {

        List<FileChecksumPDDTO> resp = cargaArchivos.getFileChecksum(op, idEncArch, idRef, origen, checksum, activo);
        return resp;
    }

    /**
     * Servicio que devuelve informacion de archivos que se encuentran
     * almacenados en ISILON pero no tienen checksum en BD
     */
    @RequestMapping(value = "getFilesWithoutChecksum.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<FileWithoutChecksumPDDTO> getFilesWithoutChecksum(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "op", required = false) Integer op) throws Exception {

        List<FileWithoutChecksumPDDTO> resp = cargaArchivos.getFilesWithoutChecksum(op);
        return resp;
    }

    /**
     * Servicio que genera checksum de un archivo y lo almacena en BD
     */
    @RequestMapping(value = "createChecksumOfFiles.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String createChecksumOfFiles(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "op", required = false) Integer op) throws Exception {

        String resp = cargaArchivos.createChecksumOfFiles(op);
        return resp;
    }

    /**
     * Servicio que trunca tabla PDTP_PASO_CECO
     */
    @RequestMapping(value = "truncateTPasoCeco.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String truncateTPasoCeco(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String resp = cargaArchivos.truncateTPasoCeco();
        return resp;
    }

    /**
     * Servicio que inserta ceco en PDTP_PASO_CECO
     */
    @RequestMapping(value = "insertTPasoCeco.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertTPasoCeco(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "ceco", required = false) CecosWSPDDTO ceco) throws Exception {

        String resp = cargaArchivos.insertTPasoCeco(ceco);
        return resp;
    }

    /**
     * Servicio que obtiene parametros para insertar información de cecos en
     * PDTP_PASO_CECO
     */
    @RequestMapping(value = "getCecoParameters.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ParamCecoPDDTO> getCecoParameters(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<ParamCecoPDDTO> resp = cargaArchivos.getCecoParameters();
        return resp;
    }

    /**
     * Servicio que recibe parametro para insertar cecos en PDTA_CECO
     */
    @RequestMapping(value = "updateCecosByParam.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateCecosByParam(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "op", required = false) Integer op,
            @RequestParam(value = "idCeco", required = false) String negocio,
            @RequestParam(value = "negocio", required = false) Integer idCeco) throws Exception {
        String resp = cargaArchivos.updateCecosByParam(op, negocio, idCeco);
        return resp;
    }

    /**
     * Servicio que devuelve documentos y su estatus actual
     */
    @RequestMapping(value = "getDocCorruptos.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DocCorruptosPDDTO> getDocCorruptos(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "op", required = true) Integer op,
            @RequestParam(value = "idCeco", required = false) String idCeco,
            @RequestParam(value = "folio", required = false) Integer folio,
            @RequestParam(value = "idDocto", required = false) Integer idDocto,
            @RequestParam(value = "fecha", required = false) Integer fecha) throws Exception {
        List<DocCorruptosPDDTO> resp = cargaArchivos.getDocCorruptos(op, idCeco, folio, idDocto, fecha);
        return resp;
    }

    /**
     * Servicio que devuelve archivo Excel de documentos y su estatus actual
     */
    @RequestMapping(value = "getDocCorruptosExcel.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Boolean getDocCorruptosExcel(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "op", required = true) Integer op,
            @RequestParam(value = "idCeco", required = false) String idCeco,
            @RequestParam(value = "folio", required = false) Integer folio,
            @RequestParam(value = "idDocto", required = false) Integer idDocto,
            @RequestParam(value = "fecha", required = false) Integer fecha) throws Exception {

        ServletOutputStream sos = null;
        StringBuilder archivo = new StringBuilder();
        List<DocCorruptosPDDTO> resp = cargaArchivos.getDocCorruptos(op, idCeco, folio, idDocto, fecha);

        try {
            archivo.append("<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='excel-table' style='border-collapse: collapse;'>");
            archivo.append("<thead>");
            archivo.append("<tr>");

            archivo.append("<th align='center'>FIFOLIO</th>");
            archivo.append("<th align='center'>FIID_DOCUMENTO</th>");
            archivo.append("<th align='center'>FCNOMBRE_ARC</th>");
            archivo.append("<th align='center'>TIPO_ENVIO</th>");
            archivo.append("<th align='center'>SITUACION_DOCTO</th>");
            archivo.append("<th align='center'>FCID_CECO</th>");
            archivo.append("<th align='center'>NOM_CECO</th>");
            archivo.append("<th align='center'>ID_CATEGORIA</th>");
            archivo.append("<th align='center'>DESC_CATEGOR</th>");
            archivo.append("<th align='center'>FIID_TIPO_DOCTO</th>");
            archivo.append("<th align='center'>FCNOMBRE_DOCTO</th>");
            archivo.append("<th align='center'>FIID_DOCTOPEDESTAL</th>");
            archivo.append("<th align='center'>VIGENCIA_INI</th>");
            archivo.append("<th align='center'>VIGENCIA_FIN</th>");
            archivo.append("<th align='center'>FIPESO_BYTES</th>");
            archivo.append("<th align='center'>ESTATUS_DOC</th>");
            archivo.append("<th align='center'>DESC_ESTATUS_DOC</th>");
            archivo.append("<th align='center'>ESTA_DOC_SUC</th>");
            archivo.append("<th align='center'>DESC_ESTA_DOC_SUC</th>");
            archivo.append("<th align='center'>FCHASH</th>");
            archivo.append("<th align='center'>ULT_FECHA_DOC_P</th>");
            archivo.append("<th align='center'>ACTIVO_DOCTO</th>");

            archivo.append("</tr>");
            archivo.append("</thead>");
            archivo.append("<tbody>");

            for (DocCorruptosPDDTO doc : resp) {
                archivo.append("<tr>");
                archivo.append("<td>" + doc.getFifolio() + "</td>");
                archivo.append("<td>" + doc.getFiid_documento() + "</td>");
                archivo.append("<td>" + doc.getFcnombre_arc() + "</td>");
                archivo.append("<td>" + doc.getTipo_envio() + "</td>");
                archivo.append("<td>" + doc.getSituacion_docto() + "</td>");
                archivo.append("<td>" + doc.getFcid_ceco() + "</td>");
                archivo.append("<td>" + doc.getNom_ceco() + "</td>");
                archivo.append("<td>" + doc.getId_categoria() + "</td>");
                archivo.append("<td>" + doc.getDesc_categor() + "</td>");
                archivo.append("<td>" + doc.getFiid_tipo_docto() + "</td>");
                archivo.append("<td>" + doc.getFcnombre_docto() + "</td>");
                archivo.append("<td>" + doc.getFiid_doctopedestal() + "</td>");
                archivo.append("<td>" + doc.getVigencia_ini() + "</td>");
                archivo.append("<td>" + doc.getVigencia_fin() + "</td>");
                archivo.append("<td>" + doc.getFipeso_bytes() + "</td>");
                archivo.append("<td>" + doc.getEstatus_doc() + "</td>");
                archivo.append("<td>" + doc.getDesc_estatus_doc() + "</td>");
                archivo.append("<td>" + doc.getEsta_doc_suc() + "</td>");
                archivo.append("<td>" + doc.getDesc_esta_doc_suc() + "</td>");
                archivo.append("<td>" + doc.getFchash() + "</td>");
                archivo.append("<td>" + doc.getUlt_fecha_doc_p() + "</td>");
                archivo.append("<td>" + doc.getActivo_docto() + "</td>");
                archivo.append("</tr>");
            }

            archivo.append("</tbody>");
            archivo.append("</table>");

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String _fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            response.setContentType("Content-type:   application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=estatus_archivos_" + _fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            sos.write(String.valueOf(archivo).getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Servicio que valida si existe un documento (folio) NO VISIBLE antes de la
     * fecha actual (hoy)
     */
    @RequestMapping(value = "getDocsBeforeUpload.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DocsBeforeUploadPDDTO> getUsersNotif(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "op", required = true) Integer op,
            @RequestParam(value = "tipoDoc", required = true) Integer tipoDoc) throws Exception {
        List<DocsBeforeUploadPDDTO> resp = cargaArchivos.getDocsBeforeUpload(op, tipoDoc);
        return resp;
    }

    /**
     * Servicio que devuelve los documentos que se encuentran a punto de expirar
     * (7 dias o menos)
     */
    @RequestMapping(value = "getExpiringDocs.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DocsBeforeUploadPDDTO> getExpiringDocs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<DocsBeforeUploadPDDTO> resp = cargaArchivos.getExpiringDocs(1);
        return resp;
    }

    /**
     * Servicio que devuelve el historico de los folios
     */
    @RequestMapping(value = "getHistoryOfDocs.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DocsBeforeUploadPDDTO> getHistoryOfDocs(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<DocsBeforeUploadPDDTO> resp = cargaArchivos.getHistoryOfDocs(2);
        return resp;
    }

    /**
     * Servicio que devuelve todos los folios que han sido subidos ordenados por
     * fecha de modificacion
     */
    @RequestMapping(value = "getAllFoliosData.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DocsBeforeUploadPDDTO> getAllFoliosData(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<DocsBeforeUploadPDDTO> resp = cargaArchivos.getAllFoliosData(3);
        return resp;
    }
}
