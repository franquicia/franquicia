package com.gruposalinas.franquicia.servicios.servidor;

import com.google.common.reflect.TypeToken;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gruposalinas.franquicia.business.PedestalDigitalBI;
import com.gruposalinas.franquicia.domain.AdmonTabletPDDTO;
import com.gruposalinas.franquicia.domain.CargaArchivoDTO;
import java.io.IOException;
import java.lang.reflect.Type;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("central/pedestalDigital")
public class InsercionesPedestalDigital {

    private static Logger logger = LogManager.getLogger(InsercionesPedestalDigital.class);

    @Autowired
    PedestalDigitalBI pd;

    @RequestMapping(value = "/cargaArchivoGeneral", method = RequestMethod.GET)
    public @ResponseBody
    void loginPedestalDigital(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {
        CargaArchivoDTO archivo = new CargaArchivoDTO();
        int idArchivo = 0;
        String opcion = request.getParameter("opcion");

        switch (opcion) {
            case "0":
                pd.cargaArchivoGeneral(archivo);
                break;
            case "1":
                pd.actualizaArchivoGeneral(archivo);
                break;
            case "2":
                pd.consultaArchivo(idArchivo);
                break;
        }
    }

    @RequestMapping(value = "/admonTablet", method = RequestMethod.GET)
    public @ResponseBody
    void AdmonTablet(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {
        AdmonTabletPDDTO datosTablet = new AdmonTabletPDDTO();
        int id = 0;
        String opcion = request.getParameter("opcion");

        switch (opcion) {
            case "0":
                pd.cargaDatosAdmonTablet(datosTablet);
                break;
            case "1":
                pd.actualizaDatosAdmonTablet(datosTablet);
                break;
            case "2":
                pd.consultaDatosAdmonTablet(id);
                break;
        }
    }

    @SuppressWarnings({"serial"})
    @RequestMapping(value = "/cargaGeneralFile.htm", method = RequestMethod.GET)
    public @ResponseBody
    void cargaGeneralFile(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, IOException, Exception {
        Type listType = new TypeToken<CargaArchivoDTO>() {
        }.getType();
        Gson gson = new GsonBuilder().setPrettyPrinting().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
        String json = request.getParameter("datos");
        CargaArchivoDTO archivo = gson.fromJson(json, listType);
        int idArchivo = pd.cargaArchivoGeneral(archivo);

        if (idArchivo == 0) {
            logger.info("InsercionesPedestalDigitales - cargaGeneralFile - Exito");
        } else {
            logger.info("InsercionesPedestalDigitales - cargaGeneralFile - No se insertó el registro");
        }
    }
}
