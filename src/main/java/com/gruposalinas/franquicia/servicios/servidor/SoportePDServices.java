package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.SoportePDBI;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("central/pedestalDigital/soporte/")
public class SoportePDServices {

    private static Logger logger = LogManager.getLogger(SoportePDServices.class);

    @Autowired
    SoportePDBI soportePD;

    @RequestMapping(value = "insertCecoInfo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertCecoInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idCeco") int idCeco,
            @RequestParam("numSucursal") int numSucursal,
            @RequestParam("nombre") String nombre,
            @RequestParam("cecoPadre") int cecoPadre,
            @RequestParam("pais") int pais,
            @RequestParam("estado") int estado,
            @RequestParam("municipio") int municipio,
            @RequestParam("calle") String calle,
            @RequestParam("nivelC") int nivelC,
            @RequestParam("negocio") int negocio,
            @RequestParam("canal") int canal,
            @RequestParam("responsable") int responsable,
            @RequestParam("fccp") String fccp,
            @RequestParam("geografia") int geografia,
            @RequestParam("estado2") String estado2,
            @RequestParam("municipio2") String municipio2,
            @RequestParam("colonia") String colonia,
            @RequestParam("numExterior") String numExterior,
            @RequestParam("numInterior") String numInterior,
            @RequestParam("activo") int activo) throws Exception {

        int op = 1;
        String resp = null;
        try {
            soportePD.insertCecoInfo(op, idCeco, numSucursal, nombre, cecoPadre, pais, estado, municipio, calle, nivelC, negocio, canal, responsable, fccp, geografia, estado2, municipio2, colonia, numExterior, numInterior, activo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }
}
