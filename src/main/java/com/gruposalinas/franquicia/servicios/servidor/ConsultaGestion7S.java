package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.Gestion7SBI;
import java.io.IOException;
import java.security.KeyException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/consultaGestion7S")
public class ConsultaGestion7S {

    @Autowired
    Gestion7SBI gestion7SBI;

    private static final Logger logger = LogManager.getLogger(ConsultaGestion7S.class);

    // http://10.51.218.144:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=480100&nomsucursal=DAZIGUALAPA&puesto=652
    // http://10.51.218.72:8080/franquicia/consultaGestion7S/getCompruebaVersion.json?ceco=480100
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getCompruebaVersion", method = RequestMethod.GET)
    public @ResponseBody
    String getCompruebaVersion(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        String json = "";

        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("ceco")));
            json = gestion7SBI.obtienePlantillaCedula(ceco);

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        logger.info("json:" + json);
        return json;
    }

    // PROGRAMA DE LIMPIEZA
    // http://10.51.218.72:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=486263&nomsucursal=DAZIGUALAPA&puesto=652
    // http://10.51.218.72:8080/franquicia/consultaGestion7S/getDetalleProgLimpieza.json?ceco=480100
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDetalleProgLimpieza", method = RequestMethod.GET)
    public @ResponseBody
    String getProgramaLimpieza(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        String json = "";

        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("ceco")));
            json = gestion7SBI.obtieneProgramaLimpieza(ceco);
        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        logger.info("json:" + json);
        return json;
    }

    // ACTA DE HECHOS DE DEPURACIÓN DE ACTIVOS FIJOS (CATÁLOGO DE ACTIVOS FIJOS)
    // http://10.51.218.72:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=486263&nomsucursal=DAZIGUALAPA&puesto=652
    // http://10.51.218.72:8080/franquicia/consultaGestion7S/getDepuraActivos.json?ceco=480100
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDepuraActivos", method = RequestMethod.GET)
    public @ResponseBody
    String getActivosFijos(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        String json = "";

        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("ceco")));
            json = gestion7SBI.obtieneActivoFijo(ceco);

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        logger.info("json:" + json);
        return json;
    }

    // CARTA DE ASIGNACIÓN DE ACTIVO FIJO
    // http://10.51.218.72:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=486263&nomsucursal=DAZIGUALAPA&puesto=652
    // http://10.51.218.72:8080/franquicia/consultaGestion7S/getCartasActivos.json?ceco=480100
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getCartasActivos", method = RequestMethod.GET)
    public @ResponseBody
    String getCartaActFijos(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        String json = "";

        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("ceco")));
            json = gestion7SBI.obtieneCartaActFijo(ceco);

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        logger.info("json:" + json);
        return json;
    }

    // COPIA DE GUÍA DE DHL DE LA ENTREGA DEL PAQUETE OPERATIVO
    // http://10.51.218.72:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=486263&nomsucursal=DAZIGUALAPA&puesto=652
    // http://10.51.218.72:8080/franquicia/consultaGestion7S/getGuiasDHL.json?ceco=480100
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getGuiasDHL", method = RequestMethod.GET)
    public @ResponseBody
    String getGuiaDHL(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        String json = "";

        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("ceco")));
            json = gestion7SBI.obtieneGuiaDHL(ceco);

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        logger.info("json:" + json);
        return json;
    }

    // BITÁCORA DE MANTENIMIENTO
    // http://10.51.218.72:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=486263&nomsucursal=DAZIGUALAPA&puesto=652
    // http://10.51.218.72:8080/franquicia/consultaGestion7S/getDetalleBitaMtto.json?ceco=480100
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDetalleBitaMtto", method = RequestMethod.GET)
    public @ResponseBody
    String getBitMantenimiento(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        String json = "";

        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("ceco")));
            json = gestion7SBI.obtieneBitMantenimiento(ceco);

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        logger.info("json:" + json);
        return json;
    }

    // FOTO DE LA PLANTILLA ACTUAL DEL PUNTO DE VENTA
    // http://10.51.218.72:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=486263&nomsucursal=DAZIGUALAPA&puesto=652
    // http://10.51.218.72:8080/franquicia/consultaGestion7S/getFotoPlantilla.json?ceco=480100
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getFotoPlantilla", method = RequestMethod.GET)
    public @ResponseBody
    String getFotoPlantilla(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        String json = "";

        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("ceco")));
            json = gestion7SBI.obtieneFotoPlantilla(ceco);

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        logger.info("json:" + json);
        return json;
    }

    // CÉDULA DE IDENTIFICACIÓN PERSONAL
    // http://10.51.218.72:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=486263&nomsucursal=DAZIGUALAPA&puesto=652
    // http://10.51.218.72:8080/franquicia/consultaGestion7S/getPlantillaCedula.json?ceco=480100
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getPlantillaCedula", method = RequestMethod.GET)
    public @ResponseBody
    String getPlantillaCedula(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        String json = "";

        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("ceco")));
            json = gestion7SBI.obtienePlantillaCedula(ceco);

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        logger.info("json:" + json);
        return json;
    }

    // DETALLE CEDULA DE IDENTIFICACION
    // http://10.51.218.72:8080/franquicia/tienda/inicio.htm?numempleado=T808689&nombre=VERONICA&apellidop=MALDONADO&apellidom=DOMINGUEZ&sucursal=486263&nomsucursal=DAZIGUALAPA&puesto=652
    // http://10.51.218.72:8080/franquicia/consultaGestion7S/getDetalleCedula.json?ceco=480100
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getDetalleCedula", method = RequestMethod.GET)
    public @ResponseBody
    String getDetalleCedula(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        String json = "";

        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("ceco")));
            json = gestion7SBI.obtieneDetalleCedula(ceco);

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        logger.info("json:" + json);
        return json;
    }

} //FIN ConsultaGestion7S
