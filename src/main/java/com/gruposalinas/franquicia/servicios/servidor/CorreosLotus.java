package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.CorreoBI;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CorreosLotus {

    private String https_url_pro = "http://10.50.183.58:80/apps/envionotificacionesgs.nsf/ObtieneInformacionEmpleado?WSDL";
    private final Logger logger = LogManager.getLogger(CorreoBI.class);

    public String validaUsuarioLotusCorreos(String idUsuario) {
        if (idUsuario == null) {
            return "";
        }
        String correo = "";
        HttpURLConnection rc = null;
        BufferedReader read = null;
        OutputStream outStr = null;
        //logger.info("El numero de empleado que pasamos" +idUsuario);

        try {

            //crear la llamada al servidor
            //HttpsClient secureClient = new HttpsClient() ;
            logger.info("CORREOS LOTUS");
            URL url = new URL(https_url_pro);
            rc = (HttpURLConnection) url.openConnection();

            //rc = secureClient.testIt(https_url_pro);
            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Accept-Charset", "UTF-8");
            rc.setRequestProperty("Content-Type", "text/html");  //application/soap+xml
            //logger.info("CORREOS LOTUS 2");

            String reqStr = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:webservice.gruposalinas.com\">"
                    + "<soapenv:Header/>"
                    + "<soapenv:Body>"
                    + "<urn:usuario>UsrR3fl3x1s</urn:usuario>"
                    + "<urn:xml>"
                    + "&lt;EMPLEADOS&gt;&lt;EMPLEADO&gt;&lt;NUMERO&gt;" + idUsuario + "&lt;/NUMERO&gt;&lt;EMPRESA&gt;EKT&lt;/EMPRESA&gt;&lt;/EMPLEADO&gt;&lt;/EMPLEADOS&gt;"
                    + "</urn:xml>"
                    + "</soapenv:Body>"
                    + "</soapenv:Envelope>";

            //several more definitions to request
            int len = reqStr.length();
            rc.setRequestProperty("Content-Length", Integer.toString(len));
            rc.setRequestProperty("Connection", "Keep-Alive");
            rc.setConnectTimeout(5000);
            //logger.info("CORREOS LOTUS 3");
            rc.connect();
            //logger.info("CORREOS LOTUS 4");

            outStr = rc.getOutputStream();
            outStr.write(reqStr.getBytes("UTF-8"));
            outStr.flush();
            //logger.info("CORREOS LOTUS 5");

            read = null;
            try {
                read = new BufferedReader(new InputStreamReader(rc.getInputStream()));

            } catch (Exception exception) {

                //if something wrong instead of the output, read the error
                read = new BufferedReader(new InputStreamReader(rc.getErrorStream()));
                //error.setMsj(read.toString());
                // logger.info("error1"+exception.getMessage());
                return "Algo paso: " + exception.getMessage();
                //  objJson = format(error);
            } finally {
                try {
                    if (read != null) {
                        read.close();
                    }
                } catch (Exception e) {

                    logger.info("error2" + e.getMessage());

                }
            }

            //read server response
            String line;

            while ((line = read.readLine()) != null) {
                //logger.info(line);

                if (line.contains("MAIL")) {
                    String[] tags = line.split("MAIL");
                    correo = tags[1].replace("&lt;/", "").replace("&gt;", "");

                }

            }
        } catch (Exception e) {
            //e.printStackTrace();
            logger.info("error2" + e.getMessage());
            return "error2" + e.getMessage() + e.getStackTrace() + e.getCause() + e.hashCode() + e.toString() + e.getLocalizedMessage();
            //return validaLlave;
        } finally {
            try {
                if (outStr != null) {
                    outStr.close();
                }
                if (rc != null) {
                    rc.disconnect();
                }

                rc = null;
                read = null;
                outStr = null;
            } catch (Exception e) {

                logger.info("error2" + e.getMessage());
            }

        }
        return correo;
    }
}
