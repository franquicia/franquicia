package com.gruposalinas.franquicia.servicios.servidor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.gruposalinas.franquicia.business.DatosUsuarioBI;
import com.gruposalinas.franquicia.business.Usuario_ABI;
import com.gruposalinas.franquicia.domain.DatosPerfilDTO;
import com.gruposalinas.franquicia.domain.DatosUsuarioDTO;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("central/pedestalDigital/")
public class ValidaFirma {

    private static Logger logger = LogManager.getLogger(ValidaFirma.class);

    @Autowired
    DatosUsuarioBI datosUsuarioBI;

    @Autowired
    Usuario_ABI usuarioBI;

    // http://localhost:8080/migestion/servicios/validaFirmaAzteca.json?id=<?>&usuarioAdmin=1&firma=<?>&key=1
    @RequestMapping(value = "/validaFirmaAzteca", method = RequestMethod.GET)
    public @ResponseBody
    String consumeWebServiceGetData(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "idUsuario", required = true) String usuario,
            @RequestParam(value = "token", required = true) String tokenAzteca)
            throws IOException {

        //logger.info("VALIDA NUEVA FIRMA");
        String ticketSession = "";
        String json = "";
        DatosUsuarioDTO datosUsuarioDTO = null;

        List<DatosPerfilDTO> datosPerfil = null;

        Gson g = new GsonBuilder().serializeNulls().create();

        try {

            String idUsuario = usuario;
            //String usuarioAdmin = urides.split("&")[1].split("=")[1];
            // String ip = GTNConstantes.getIpOrigen();
            String token = tokenAzteca;

            String ntc = "1";

            String[] usuariosP = {"515725", "722878", "203977", "196228", "189140", "191841", "189870", "189871",
                "191312", "331952", "643965", "664899", "666462",
                "176361", "73429", "165943", "73437", "73433", "73610", "73601", "73606", "370566", "202622",
                "82144", "177870", "40525", "10018465"};

            logger.info("Usuario: " + idUsuario);
            //logger.info("UsuarioAdmin: " + usuarioAdmin);
            // logger.info("Ip: " + ip);
            logger.info("Firma: " + token);
            // logger.info("ntc: " + ntc);

            // Usuarios
            boolean bandera = false;

            //logger.info("usuarioAdmin.compareTo(\"0\"): " + usuarioAdmin.compareTo("0"));

            /*if (usuarioAdmin.compareTo("0") != 0) {
                for (int i = 0; i < usuariosP.length; i++) {
                    if (usuarioAdmin.compareTo(usuariosP[i]) == 0) {
                        bandera = true;
                        break;
                    }
                }
            }*/
            JsonObject jsonResult;

            // logger.info("bandera: " +bandera);
            if (bandera) {
                logger.info("si");
                jsonResult = new JsonObject();
                jsonResult.addProperty("resultado", 0);
            } else {
                logger.info("no: ");

                try {

                    jsonResult = usuarioBI.validaUsuario(usuario, token);
                } catch (Exception e) {
                    logger.info("Ocurrio algo en el servicio validaUsuario():" + e.getMessage());
                    jsonResult = null;
                }
            }

            if (jsonResult != null) {

                if (jsonResult.get("resultado").toString().contains("0")) {
                    logger.info("firma valida");
                    ticketSession = "TEMP";
                } else {
                    logger.info("Token_Info (firma no valida para usuario: " + idUsuario + ")");
                    ticketSession = "ERROR";
                }

                if (!ticketSession.equals("ERROR")) {

                    logger.info("carga");

                    datosUsuarioDTO = new DatosUsuarioDTO();
                    Map<String, Object> lista = datosUsuarioBI.buscaUsuario(idUsuario, 0);
                    datosUsuarioDTO = (DatosUsuarioDTO) lista.get("datosUsuario");
                    datosUsuarioDTO.setTicket(ticketSession);

                    datosPerfil = (List<DatosPerfilDTO>) lista.get("datosPerfil");

                }
            }
        } catch (Exception e) {
            //logger.info("Ocurrio algo ");
        }

        json = "{\"datosUsuario\":" + g.toJson(datosUsuarioDTO) + ",\"datosPerfil\":" + g.toJson(datosPerfil) + "}";

        return json;
    }

}
