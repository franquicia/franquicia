package com.gruposalinas.franquicia.servicios.servidor;

import com.google.gson.JsonArray;
import com.gruposalinas.franquicia.business.ArbolDecisionBI;
import com.gruposalinas.franquicia.business.AsignacionBI;
import com.gruposalinas.franquicia.business.BitacoraBI;
import com.gruposalinas.franquicia.business.ChecklistBI;
import com.gruposalinas.franquicia.business.ChecklistModificacionesBI;
import com.gruposalinas.franquicia.business.ChecklistPreguntaBI;
import com.gruposalinas.franquicia.business.ChecklistUsuarioBI;
import com.gruposalinas.franquicia.business.CompromisoBI;
import com.gruposalinas.franquicia.business.DetalleRespuestaReporteBI;
import com.gruposalinas.franquicia.business.EdoChecklistBI;
import com.gruposalinas.franquicia.business.EvidenciaBI;
import com.gruposalinas.franquicia.business.GeografiaBI;
import com.gruposalinas.franquicia.business.ImagenesRespuestaBI;
import com.gruposalinas.franquicia.business.ModuloBI;
import com.gruposalinas.franquicia.business.ModuloTBI;
import com.gruposalinas.franquicia.business.PreguntaBI;
import com.gruposalinas.franquicia.business.ReporteImagenesBI;
import com.gruposalinas.franquicia.business.RespuestaAdBI;
import com.gruposalinas.franquicia.business.RespuestaBI;
import com.gruposalinas.franquicia.business.RespuestaPdfBI;
import com.gruposalinas.franquicia.business.TipoChecklistBI;
import com.gruposalinas.franquicia.business.TipoPreguntaBI;
import com.gruposalinas.franquicia.business.VisitaTiendaBI;
import com.gruposalinas.franquicia.domain.ArbolDecisionDTO;
import com.gruposalinas.franquicia.domain.AsignacionDTO;
import com.gruposalinas.franquicia.domain.BitacoraDTO;
import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.ChecklistPreguntaDTO;
import com.gruposalinas.franquicia.domain.ChecklistUsuarioDTO;
import com.gruposalinas.franquicia.domain.CompromisoDTO;
import com.gruposalinas.franquicia.domain.ConteoxPreguntaDTO;
import com.gruposalinas.franquicia.domain.EdoChecklistDTO;
import com.gruposalinas.franquicia.domain.EvidenciaDTO;
import com.gruposalinas.franquicia.domain.GeografiaDTO;
import com.gruposalinas.franquicia.domain.HorasRespuestaDTO;
import com.gruposalinas.franquicia.domain.ImagenesRespuestaDTO;
import com.gruposalinas.franquicia.domain.ModuloDTO;
import com.gruposalinas.franquicia.domain.NumeroTiendasDTO;
import com.gruposalinas.franquicia.domain.PreguntaDTO;
import com.gruposalinas.franquicia.domain.ReportesConteoDTO;
import com.gruposalinas.franquicia.domain.RespuestaAdDTO;
import com.gruposalinas.franquicia.domain.RespuestaDTO;
import com.gruposalinas.franquicia.domain.TipoChecklistDTO;
import com.gruposalinas.franquicia.domain.TipoPreguntaDTO;
import com.gruposalinas.franquicia.resources.FRQAuthInterceptor;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("/consultaChecklistService")
public class ConsultaChecklistService {

    @Autowired
    ModuloBI modulobi;
    @Autowired
    ModuloTBI modulotbi;
    @Autowired
    TipoPreguntaBI tipPreguntabi;
    @Autowired
    TipoChecklistBI tipoChecklistbi;
    @Autowired
    EdoChecklistBI edoChecklistbi;
    @Autowired
    BitacoraBI bitacorabi;
    @Autowired
    RespuestaBI respuestabi;
    @Autowired
    CompromisoBI compromisobi;
    @Autowired
    EvidenciaBI evidenciabi;
    @Autowired
    ArbolDecisionBI arbolDecisionbi;
    @Autowired
    ChecklistPreguntaBI checklistPreguntabi;
    @Autowired
    ChecklistUsuarioBI checklistUsuariobi;
    @Autowired
    ChecklistBI checklistbi;
    @Autowired
    PreguntaBI preguntabi;
    @Autowired
    RespuestaAdBI respuestaAdbi;
    @Autowired
    ChecklistModificacionesBI checklistModificacionesbi;
    @Autowired
    ReporteImagenesBI reporteImagenesBI;
    @Autowired
    ImagenesRespuestaBI imagenesRespuestaBI;
    @Autowired
    AsignacionBI asignacionBI;
    @Autowired
    VisitaTiendaBI visitaTiendaBI;
    @Autowired
    DetalleRespuestaReporteBI reporteBI;

    @Autowired
    GeografiaBI geografiaBI;

    @Autowired
    RespuestaPdfBI respuestaPdfBI;

    private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

    // http://localhost:8080/franquicia/consultaChecklistService/getEstadoCheck.json
    @RequestMapping(value = "/getEstadoCheck", method = RequestMethod.GET)
    public ModelAndView getEstadoCheck(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<EdoChecklistDTO> lista = edoChecklistbi.obtieneEdoChecklist();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA ESTADO CHECKLIST");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getTiposCheck.json
    @RequestMapping(value = "/getTiposCheck", method = RequestMethod.GET)
    public ModelAndView getTiposCheck(HttpServletRequest request, HttpServletResponse response) {

        List<TipoChecklistDTO> lista = tipoChecklistbi.obtieneTipoChecklist();

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "LISTA TIPOS CHECKLIST");
        mv.addObject("res", lista);

        return mv;
    }

    @RequestMapping(value = "/getTiposCheckService", method = RequestMethod.GET)
    public @ResponseBody
    List<TipoChecklistDTO> getTiposCheckService(HttpServletRequest request, HttpServletResponse response) {

        List<TipoChecklistDTO> lista = tipoChecklistbi.obtieneTipoChecklist();

        return lista;
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getTiposPregunta.json
    @RequestMapping(value = "/getTiposPregunta", method = RequestMethod.GET)
    public ModelAndView getTiposPregunta(HttpServletRequest request, HttpServletResponse response) {

        List<TipoPreguntaDTO> lista = tipPreguntabi.obtieneTipoPregunta();

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "LISTA TIPOS PREGUNTA");
        mv.addObject("res", lista);

        return mv;
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getBitacora.json
    @RequestMapping(value = "/getBitacora", method = RequestMethod.GET)
    public ModelAndView getBitacora(HttpServletRequest request, HttpServletResponse response) {

        List<BitacoraDTO> lista = bitacorabi.buscaBitacora();

        System.out.println(lista);

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "LISTA BITACORA");
        mv.addObject("res", lista);

        return mv;
    }

    //http://localhost:8080/franquicia/consultaChecklistService/getBitacoraG.json?idCheckU=<?>&idBitacora=<?>&fechaI=<?>&fechaF=<?>
    @RequestMapping(value = "/getBitacoraG", method = RequestMethod.GET)
    public ModelAndView getBitacoraG(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheckU = request.getParameter("idCheckU");
            String idBitacora = request.getParameter("idBitacora");
            String fechaI = request.getParameter("fechaI");
            String fechaF = request.getParameter("fechaF");

            List<BitacoraDTO> lista = bitacorabi.buscaBitacora(idCheckU, idBitacora, fechaI, fechaF);

            System.out.println(lista);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA BITACORA");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/consultaChecklistService/getBitacoraCerradas.json?idChecklist=<?>
    @RequestMapping(value = "/getBitacoraCerradas", method = RequestMethod.GET)
    public ModelAndView getBitacoraCerradas(HttpServletRequest request, HttpServletResponse response) {

        String idChecklist = request.getParameter("idChecklist");

        List<BitacoraDTO> lista = bitacorabi.buscaBitacoraCerradas(idChecklist);

        System.out.println(lista);

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "BITACORA CERRADA");
        mv.addObject("res", lista);

        return mv;
    }

    //http://localhost:8080/franquicia/consultaChecklistService/getBitacoraCerradasR.json?idChecklist=<?>
    @RequestMapping(value = "/getBitacoraCerradasR", method = RequestMethod.GET)
    public ModelAndView getBitacoraCerradasR(HttpServletRequest request, HttpServletResponse response) {

        String idChecklist = request.getParameter("idChecklist");

        List<BitacoraDTO> lista = bitacorabi.buscaBitacoraCerradasR(idChecklist);

        System.out.println(lista);

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "BITACORA CERRADA");
        mv.addObject("res", lista);

        return mv;
    }

    //http://localhost:8080/franquicia/consultaChecklistService/getGeografia.json?idCeco=<?>&idRegion=<?>&IdZona=<?>&idTerritorio=<?>
    @RequestMapping(value = "/getGeografia", method = RequestMethod.GET)
    public ModelAndView getGeografia(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCeco = request.getParameter("idCeco");
            String idRegion = request.getParameter("idRegion");
            String idZona = request.getParameter("idZona");
            String idTerritorio = request.getParameter("idTerritorio");

            List<GeografiaDTO> lista = geografiaBI.obtieneGeo(idCeco, idRegion, idZona, idTerritorio);

            System.out.println(lista);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA GEO");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getRespuestas.json
    @RequestMapping(value = "/getRespuestas", method = RequestMethod.GET)
    public ModelAndView getRespuestas(HttpServletRequest request, HttpServletResponse response) {

        List<RespuestaDTO> lista = respuestabi.obtieneRespuesta();

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "LISTA RESPUESTAS");
        mv.addObject("res", lista);

        return mv;
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getRespuestasG.json?idRespuesta=<?>&idBitacora=<?>&idArbol=<?>
    @RequestMapping(value = "/getRespuestasG", method = RequestMethod.GET)
    public ModelAndView getRespuestasG(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idBitacora = request.getParameter("idBitacora");
            String idRespuesta = request.getParameter("idRespuesta");
            String idArbol = request.getParameter("idArbol");

            logger.info(idArbol + " " + idRespuesta + " " + idBitacora + "  dd ");

            List<RespuestaDTO> lista = respuestabi.obtieneRespuesta(idArbol, idRespuesta, idBitacora);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA RESPUESTAS");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/franquicia/consultaChecklistService/getCompromisos.json

    @RequestMapping(value = "/getCompromisos", method = RequestMethod.GET)
    public ModelAndView getCompromisos(HttpServletRequest request, HttpServletResponse response) {

        List<CompromisoDTO> lista = compromisobi.obtieneCompromiso();

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "LISTA COMPROMISOS");
        mv.addObject("res", lista);

        return mv;
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getEvidencias.json
    @RequestMapping(value = "/getEvidencias", method = RequestMethod.GET)
    public ModelAndView getEvidencias(HttpServletRequest request, HttpServletResponse response) {

        List<EvidenciaDTO> lista = evidenciabi.obtieneEvidencia();

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "LISTA EVIDENCIAS");
        mv.addObject("res", lista);

        return mv;
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getArbolDesicion.json?idCheck=<?>
    @RequestMapping(value = "/getArbolDesicion", method = RequestMethod.GET)
    public ModelAndView getArbolDesicion(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = request.getParameter("idCheck");

            List<ArbolDecisionDTO> lista = arbolDecisionbi.buscaArbolDecision(Integer.parseInt(idCheck));

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "ARBOL DESICION");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getArbolDesicionService.json?idCheck=<?>
    @RequestMapping(value = "/getArbolDesicionService", method = RequestMethod.GET)
    public @ResponseBody
    List<ArbolDecisionDTO> getArbolDesicionService(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("idCheck")));
            List<ArbolDecisionDTO> lista = arbolDecisionbi.buscaArbolModificaciones(Integer.parseInt(ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(idCheck))));
            return lista;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/franquicia/consultaChecklistService/getCheckPregunta.json?idCheck=<?>

    @RequestMapping(value = "/getCheckPregunta", method = RequestMethod.GET)
    public ModelAndView getCheckPregunta(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = request.getParameter("idCheck");

            List<ChecklistPreguntaDTO> lista = checklistPreguntabi.buscaPreguntas(Integer.parseInt(idCheck));

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "GET CHECKLIST PREGUNTA");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getCheckPreguntaService.json?idCheck=<?>
    @RequestMapping(value = "/getCheckPreguntaService", method = RequestMethod.GET)
    public @ResponseBody
    List<ChecklistPreguntaDTO> getCheckPreguntaService(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = request.getParameter("idCheck");

            List<ChecklistPreguntaDTO> lista = checklistPreguntabi.obtienePregXcheck(Integer.parseInt(idCheck));

            return lista;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getCheckUsu.json?idCheckUsu=<?>
    @RequestMapping(value = "/getCheckUsu", method = RequestMethod.GET)
    public ModelAndView getCheckUsua(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheckU = request.getParameter("idCheckUsu");

            List<ChecklistUsuarioDTO> lista = checklistUsuariobi.obtieneCheckUsuario(Integer.parseInt(idCheckU));

            //ModelAndView mv = new ModelAndView("muestraServicios");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "GET CHECKLIST USUARIO");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getChecklist.json
    @RequestMapping(value = "/getChecklist", method = RequestMethod.GET)
    public ModelAndView getChecklist(HttpServletRequest request, HttpServletResponse response) {

        List<ChecklistDTO> lista = checklistbi.buscaChecklist();

        ModelAndView mv = new ModelAndView("muestraServicios");
        //ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

        mv.addObject("tipo", "GET CHECKLIST");
        mv.addObject("res", lista);

        return mv;
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getModulos.json
    @RequestMapping(value = "/getModulos", method = RequestMethod.GET)
    public ModelAndView getModulos(HttpServletRequest request, HttpServletResponse response) {

        List<ModuloDTO> lista = modulobi.obtieneModulo();

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "GET MODULOS");
        mv.addObject("res", lista);

        return mv;
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getModulos.json?idModulo=<?>
    @RequestMapping(value = "/getModulosService", method = RequestMethod.GET)
    public @ResponseBody
    List<ModuloDTO> getModuloService(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idModulo = request.getParameter("idModulo");

            List<ModuloDTO> lista = modulobi.obtieneModulo();

            List<ModuloDTO> listaTemp = modulotbi.obtieneModuloTemp(idModulo);

            int cont = 0;
            while (cont < listaTemp.size()) {
                lista.add(listaTemp.get(cont));
                cont++;
            }

            return lista;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getPreguntas.json
    @RequestMapping(value = "/getPreguntas", method = RequestMethod.GET)
    public ModelAndView getPreguntas(HttpServletRequest request, HttpServletResponse response) {

        List<PreguntaDTO> lista = preguntabi.obtienePregunta();

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "GET PREGUNTAS");
        mv.addObject("res", lista);

        return mv;
    }

    //http://localhost:8080/franquicia/consultaChecklistService/getCheckUsuByIdUsu.json?idUsu=<?>
    @RequestMapping(value = "/getCheckUsuByIdUsu", method = RequestMethod.GET)
    public ModelAndView getCheckUsuByIdUsu(HttpServletRequest request, HttpServletResponse response) {
        List<ChecklistUsuarioDTO> lista = null;
        try {
            String idUsu = request.getParameter("idUsu");
            lista = checklistUsuariobi.obtieneCheckU(idUsu);
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
        }

        ModelAndView mv = new ModelAndView("muestraServicios");
        //ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
        mv.addObject("tipo", "GET CHECKLIST-USUARIO POR ID USUARIO");
        mv.addObject("res", lista);

        return mv;
    }

    //http://localhost:8080/franquicia/consultaChecklistService/getCheckUsuCompleto.json?idUsu=<?>&ceco=<?>&fechaInicio=<?>
    @RequestMapping(value = "/getCheckUsuCompleto", method = RequestMethod.GET)
    public ModelAndView getCheckUsuCompleto(HttpServletRequest request, HttpServletResponse response) {
        String idUsu = request.getParameter("idUsu");
        String ceco = request.getParameter("ceco");
        String fechaInicio = request.getParameter("fechaInicio");

        List<ChecklistUsuarioDTO> lista = checklistUsuariobi.obtieneCheckUsua(idUsu, ceco, fechaInicio);

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "GET CHECK COMPLETO");
        mv.addObject("res", lista);

        return mv;
    }

    //http://localhost:8080/franquicia/consultaChecklistService/getRespuestasAd.json?idRespuesta=<?>&idRespuestaAd=<?>
    @RequestMapping(value = "/getRespuestasAd", method = RequestMethod.GET)
    public ModelAndView getRespuestasAd(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idRespuesta = request.getParameter("idRespuesta");
            String idRespuestaAd = request.getParameter("idRespuestaAd");

            List<RespuestaAdDTO> lista = respuestaAdbi.buscaRespADP(idRespuestaAd, idRespuesta);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "GET RESPUESTAS ADICIONALES");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getChecklistDetails.json
    @RequestMapping(value = "/getChecklistDetails", method = RequestMethod.GET)
    public @ResponseBody
    String getChecklistDetailsService(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = request.getParameter("idCheck");
            String json = checklistModificacionesbi.obtieneChecklistActual(Integer.parseInt(idCheck));
            return json;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getChecklistCompareDetails.json
    @RequestMapping(value = "/getChecklistCompareDetails", method = RequestMethod.GET)
    public @ResponseBody
    String getChecklistCompareDetailsService(HttpServletRequest request,
            HttpServletResponse response) throws NumberFormatException, Exception {
        try {
            String idCheck = request.getParameter("idCheck");
            Object json = checklistModificacionesbi.obtieneChecklist(Integer.parseInt(idCheck));
            return json.toString();
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getModulosListas.json?idChecklist=<?>&idCeco=<?>
    @RequestMapping(value = "/getModulosListas", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getModulosListas(HttpServletRequest request,
            HttpServletResponse response) throws NumberFormatException, Exception {
        try {
            String idChecklist = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("idChecklist")));
            String idCeco = request.getParameter("idCeco");

            Map<String, Object> res = reporteImagenesBI.obtieneModulosTiendas(idCeco, idChecklist);

            return res;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getRespuestasReporte.json?idChecklist=<?>&idCeco=<?>&fecha=<?>
    @RequestMapping(value = "/getRespuestasReporte", method = RequestMethod.GET)
    public @ResponseBody
    List<Object> getRespuestasReporte(HttpServletRequest request,
            HttpServletResponse response) throws NumberFormatException, Exception {
        try {
            String idChecklist = request.getParameter("idChecklist");
            String idCeco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("idCeco")));
            String fecha = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("fecha")));

            List<Object> res = reporteImagenesBI.obtieneRespuestas(Integer.parseInt(idCeco), Integer.parseInt(idChecklist), fecha);

            return res;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }

    }

    //http://localhost:8080/franquicia/consultaChecklistService/getImagenesArbol.json?idArbol=<?>&idImagen=<?>
    @RequestMapping(value = "/getImagenesArbol", method = RequestMethod.GET)
    public ModelAndView getImagenesArbol(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idArbol = request.getParameter("idArbol");
            String idImagen = request.getParameter("idImagen");

            List<ImagenesRespuestaDTO> lista = imagenesRespuestaBI.obtieneImagenes(idArbol, idImagen);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "GET IMAGENES ARBOL");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/consultaChecklistService/getAsignaciones.json?idChecklist=<?>&idCeco=<?>&idPuesto=<?>&activo=<?>
    @RequestMapping(value = "/getAsignaciones", method = RequestMethod.GET)
    public ModelAndView getAsignaciones(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idChecklist = request.getParameter("idChecklist");
            String idCeco = request.getParameter("idCeco");
            String idPuesto = request.getParameter("idPuesto");
            String activo = request.getParameter("activo");

            List<AsignacionDTO> lista = asignacionBI.obtieneAsignaciones(idChecklist, idCeco, idPuesto, activo);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "GET ASIGNACIONES");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/consultaChecklistService/getVisitas.json?idChecklist=<?>&idUsuario=<?>&nuMes=<?>&anio=<?>
    @RequestMapping(value = "/getVisitas", method = RequestMethod.GET)
    public @ResponseBody
    String getVisitas(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idChecklist = request.getParameter("idChecklist");
            String idUsuario = request.getParameter("idUsuario");
            String nuMes = request.getParameter("nuMes");
            String anio = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("anio")));

            String visitas = visitaTiendaBI.obtieneVisitas(idChecklist, idUsuario, nuMes, anio);
            return visitas;

        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/consultaChecklistService/getDetallesRespuestasReporte.json?idChecklist=<?>&idCeco=<?>&fecha=<?>
    @RequestMapping(value = "/getDetallesRespuestasReporte", method = RequestMethod.GET)
    public @ResponseBody
    String getRepuestasReporte(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("idCeco")));
            String idCheck = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("idChecklist")));
            String fecha = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("fecha")));

            //System.out.println(reporteBI.obtieneRespuesta(54, "481339" , "12/10/2016"));
            String respuesta = reporteBI.obtieneRespuesta(Integer.parseInt(idCheck), ceco, fecha);

            return respuesta;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }

    }

    //http://localhost:8080/franquicia/consultaChecklistService/getResumenReporte.json?idChecklist=<?>&idCeco=<?>&fecha=<?>
    @RequestMapping(value = "/getResumenReporte", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getResumenReporte(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("idCeco")));
            String idCheck = request.getParameter("idChecklist");
            String fecha = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("fecha")));

            //System.out.println(reporteBI.obtieneRespuesta(54, "481339" , "12/10/2016"));
            Map<String, Object> resumen = reporteBI.obtieneResumen(Integer.parseInt(idCheck), ceco, fecha);

            return resumen;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }

    }

    //http://localhost:8080/franquicia/consultaChecklistService/getResumenReporte2.json?idChecklist=<?>&idCeco=<?>&fecha=<?>
    @RequestMapping(value = "/getResumenReporte2", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getResumenReporte2(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {
            String ceco = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("idCeco")));
            String idCheck = request.getParameter("idChecklist");
            String fecha = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("fecha")));

            //System.out.println(reporteBI.obtieneRespuesta(54, "481339" , "12/10/2016"));
            Map<String, Object> resumen = reporteBI.obtieneResumen2(Integer.parseInt(idCheck), ceco, fecha);

            return resumen;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }

    }

    //http://localhost:8080/franquicia/consultaChecklistService/getConteoxModulo.json?idChecklist=<?>&idCeco=<?>&fecha=<?>
    @RequestMapping(value = "/getConteoxModulo", method = RequestMethod.GET)
    public @ResponseBody
    List<ConteoxPreguntaDTO> getConteoxModulo(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {
            String ceco = request.getParameter("idCeco");
            String idCheck = request.getParameter("idChecklist");
            String fecha = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("fecha")));

            //System.out.println(reporteBI.obtieneRespuesta(54, "481339" , "12/10/2016"));
            List<ConteoxPreguntaDTO> lista = reporteImagenesBI.obtieneConteoxModulo(Integer.parseInt(idCheck), ceco, fecha);

            return lista;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }

    }

    //http://10.53.29.67:9991/franquicia/consultaChecklistService/getTotalTiendasRegion.json?idChecklist=54&idCeco=236108&fecha=17/04/2017
    @RequestMapping(value = "/getTotalTiendasRegion", method = RequestMethod.GET)
    public @ResponseBody
    List<NumeroTiendasDTO> getTotalTiendasRegion(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {
            String ceco = request.getParameter("idCeco");
            String idCheck = request.getParameter("idChecklist");
            String fecha = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("fecha")));

            //System.out.println(reporteBI.obtieneRespuesta(54, "481339" , "12/10/2016"));
            List<NumeroTiendasDTO> lista = reporteImagenesBI.obtieneTotalTiendasRegion(Integer.parseInt(idCheck), ceco, fecha);

            return lista;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }

    }

    //http://10.53.29.67:9991/franquicia/consultaChecklistService/getConteoxHora.json?idChecklist=54&idCeco=999995&fecha=17/04/2017
    @RequestMapping(value = "/getConteoxHora", method = RequestMethod.GET)
    public @ResponseBody
    List<HorasRespuestaDTO> getConteoxHora(HttpServletRequest request, HttpServletResponse response, Model model) {
        List<HorasRespuestaDTO> lista = null;
        try {
            String ceco = request.getParameter("idCeco");
            String idCheck = request.getParameter("idChecklist");
            String fecha = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("fecha")));

            //System.out.println(reporteBI.obtieneRespuesta(54, "481339" , "12/10/2016"));
            lista = reporteImagenesBI.obtieneConteoxHora(Integer.parseInt(idCheck), ceco, fecha);

            return lista;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://10.53.29.67:9991/franquicia/consultaChecklistService/getReportesConteo.json?idChecklist=54&idCeco=999995&fecha=17/04/2017
    @RequestMapping(value = "/getReportesConteo", method = RequestMethod.GET)
    public @ResponseBody
    List<ReportesConteoDTO> getReportesConteo(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {
            String ceco = request.getParameter("idCeco");
            String idCheck = request.getParameter("idChecklist");
            String fecha = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("fecha")));

            //System.out.println(reporteBI.obtieneRespuesta(54, "481339" , "12/10/2016"));
            List<ReportesConteoDTO> lista = reporteImagenesBI.obtieneReportesConteo(Integer.parseInt(idCheck), ceco, fecha);

            return lista;
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
            return null;
        }

    }

    //http://localhost:8080/franquicia/consultaChecklistService/getRespuestasPdf.json?idUsuario=<?>&idCeco=<?>
    @RequestMapping(value = "/getRespuestasPdf", method = RequestMethod.GET)
    public @ResponseBody
    String getRespuestasPdf(HttpServletRequest request, HttpServletResponse response, Model model) {

        String idUsuario = request.getParameter("idUsuario");
        String ceco = request.getParameter("idCeco");

        JsonArray res = respuestaPdfBI.obtieneRespuestas(Integer.parseInt(idUsuario), ceco);

        return res.toString();

    }

    /*::::::::::::::::                                   :::::::::::::::::::::*/
 /*::::::::::::::: METODOS DE CONSULTA DE LOS SERVICIOS :::::::::::::::::::*/
 /*::::::::::::::::                                   :::::::::::::::::::::*/
 /*::::::::::::::::                                   :::::::::::::::::::::*/
    //VERIFICAR EL ENVIO (PA_VERIFICAENVIO)
    //http://localhost:8080/franquicia/consultaChecklistService/getVerificaCheck.json?idUsuario=<?>&idBitacora=<?>
    @RequestMapping(value = "/getVerificaCheck", method = RequestMethod.GET)
    public @ResponseBody
    String getVerificaCheck(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idBitacora = request.getParameter("idBitacora");

            int res = respuestaPdfBI.obtieneEnvioCompromiso(Integer.parseInt(idUsuario), Integer.parseInt(idBitacora));

            return res + "";

        } catch (Exception e) {
            logger.info("ALGO OCURRIO AL VERIFICAR EL CHECKLIST!!!!");
            logger.info(e.getMessage());
            logger.info(e.getStackTrace());
            return "";
        }

    }

    //VERIFICAR CECO POR BITACORA (PA_RESPUESTAS)
    //http://localhost:8080/franquicia/consultaChecklistService/getConsultaCecoBit.json?idUsuario=<?>&idBitacora=<?>
    @RequestMapping(value = "/getConsultaCecoBit", method = RequestMethod.GET)
    public @ResponseBody
    String getConsultaCheckBit(HttpServletRequest request, HttpServletResponse response, Model model) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idBitacora = request.getParameter("idBitacora");

            Map<String, String> res = respuestaPdfBI.obtieneCecoPorBitacora(Integer.parseInt(idUsuario), Integer.parseInt(idBitacora));

            String ceco = res.get("idCeco");
            String nNegocio = res.get("numNegocio");

            logger.info("CECO POR BITACORA CECO !!!! --- " + ceco);
            logger.info("CECO POR BITACORA NEGOCIO !!!! --- " + nNegocio);

            return res.toString();

        } catch (Exception e) {
            logger.info("ALGO OCURRIO AL VERIFICAR EL CECO POR BITACORA!!!!");
            logger.info(e.getMessage());
            logger.info(e.getStackTrace());
            return "";
        }

    }

    /*::::::::::::::::                                   :::::::::::::::::::::*/
 /*::::::::::::::: METODOS DE CONSULTA DE LOS SERVICIOS :::::::::::::::::::*/
 /*::::::::::::::::                                   :::::::::::::::::::::*/
 /*::::::::::::::::                                   :::::::::::::::::::::*/
}
