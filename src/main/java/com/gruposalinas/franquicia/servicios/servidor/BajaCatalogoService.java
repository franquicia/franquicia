package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.ArchiveroBI;
import com.gruposalinas.franquicia.business.CanalBI;
import com.gruposalinas.franquicia.business.CecoBI;
import com.gruposalinas.franquicia.business.ChecklistNegocioBI;
import com.gruposalinas.franquicia.business.EmpFijoBI;
import com.gruposalinas.franquicia.business.ExpedienteBI;
import com.gruposalinas.franquicia.business.FormArchiverosBI;
import com.gruposalinas.franquicia.business.HorarioBI;
import com.gruposalinas.franquicia.business.NegocioAdicionalBI;
import com.gruposalinas.franquicia.business.NegocioBI;
import com.gruposalinas.franquicia.business.NivelBI;
import com.gruposalinas.franquicia.business.PaisBI;
import com.gruposalinas.franquicia.business.PaisNegocioBI;
import com.gruposalinas.franquicia.business.ParametroBI;
import com.gruposalinas.franquicia.business.PerfilBI;
import com.gruposalinas.franquicia.business.PerfilUsuarioBI;
import com.gruposalinas.franquicia.business.PosibleTipoPreguntaBI;
import com.gruposalinas.franquicia.business.PosiblesBI;
import com.gruposalinas.franquicia.business.PuestoBI;
import com.gruposalinas.franquicia.business.RecursoBI;
import com.gruposalinas.franquicia.business.RecursoPerfilBI;
import com.gruposalinas.franquicia.business.StatusFormArchiverosBI;
import com.gruposalinas.franquicia.business.SucursalBI;
import com.gruposalinas.franquicia.business.TipoArchivoBI;
import com.gruposalinas.franquicia.business.Usuario_ABI;
import com.gruposalinas.franquicia.domain.ArchiveroDTO;
import com.gruposalinas.franquicia.domain.PerfilUsuarioDTO;
import com.gruposalinas.franquicia.resources.FRQAuthInterceptor;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/catalogosService")
public class BajaCatalogoService {

    @Autowired
    CanalBI canalbi;
    @Autowired
    PerfilBI perfilbi;
    @Autowired
    PuestoBI puestobi;
    @Autowired
    TipoArchivoBI tipoarchivobi;
    @Autowired
    CecoBI cecobi;
    @Autowired
    SucursalBI sucursalbi;
    @Autowired
    Usuario_ABI usuarioabi;
    @Autowired
    PaisBI paisbi;
    @Autowired
    HorarioBI horariobi;
    @Autowired
    ParametroBI parametrobi;
    @Autowired
    NegocioBI negociobi;
    @Autowired
    NivelBI nivelbi;
    @Autowired
    PerfilUsuarioBI perfilusuariobi;
    @Autowired
    RecursoPerfilBI recursoperfilbi;
    @Autowired
    RecursoBI recursobi;
    @Autowired
    PosiblesBI posiblesbi;
    @Autowired
    PosibleTipoPreguntaBI posibletipopreguntabi;
    @Autowired
    ChecklistNegocioBI checklistNegocioBI;
    @Autowired
    NegocioAdicionalBI adicionalBI;
    @Autowired
    PaisNegocioBI paisNegocioBI;
    @Autowired
    EmpFijoBI empFijoBI;

    @Autowired
    ExpedienteBI expedienteBi;
    @Autowired
    StatusFormArchiverosBI statusFormArchiverosBI;
    @Autowired
    FormArchiverosBI formArchiverosBI;
    @Autowired
    ArchiveroBI archiveroBI;

    private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

    // http://localhost:8080/franquicia/catalogosService/eliminaCanal.json?idCanal=<?>
    @RequestMapping(value = "/eliminaCanal", method = RequestMethod.GET)
    public ModelAndView eliminaCanal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCanal = request.getParameter("idCanal");

            boolean res = canalbi.eliminaCanal(Integer.parseInt(idCanal));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CANAL ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaPerfil.json?idPerfil=<?>
    @RequestMapping(value = "/eliminaPerfil", method = RequestMethod.GET)
    public ModelAndView eliminaPerfil(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idP = request.getParameter("idPerfil");

            boolean res = perfilbi.eliminaPerfil(Integer.parseInt(idP));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ID DE PERFIL BORRADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaPuesto.json?idPuesto=<?>
    @RequestMapping(value = "/eliminaPuesto", method = RequestMethod.GET)
    public ModelAndView eliminaPuesto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPuesto = request.getParameter("idPuesto");

            boolean res = puestobi.eliminaPuesto(Integer.parseInt(idPuesto));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ID DE PUESTO BORRADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaTipoArchivo.json?idArchivo=<?>
    @RequestMapping(value = "/eliminaTipoArchivo", method = RequestMethod.GET)
    public ModelAndView eliminaTipoArchivo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idA = request.getParameter("idArchivo");

            boolean res = tipoarchivobi.eliminaTipoArchivo(Integer.parseInt(idA));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "TIPO ARCHIVO BORRADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaCeco.json?idCeco=<?>
    @RequestMapping(value = "/eliminaCeco", method = RequestMethod.GET)
    public ModelAndView eliminaCeco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCeco = request.getParameter("idCeco");

            boolean res = cecobi.eliminaCeco(Integer.parseInt(idCeco));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CECO BORRADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaSucursal.json?idSuc=<?>
    @RequestMapping(value = "/eliminaSucursal", method = RequestMethod.GET)
    public ModelAndView eliminaSucursal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idSuc = request.getParameter("idSuc");

            boolean res = sucursalbi.eliminaSucursal(Integer.parseInt(idSuc));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "SUCURSAL ACTUALIZADA CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaSucursalGCC.json?idSuc=<?>
    @RequestMapping(value = "/eliminaSucursalGCC", method = RequestMethod.GET)
    public ModelAndView eliminaSucursalGCC(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idSuc = request.getParameter("idSuc");

            boolean res = sucursalbi.eliminaSucursalGCC(Integer.parseInt(idSuc));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "SUCURSAL GCC ELIMINADA CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaUsuarioA.json?idUsuario=<?>
    @RequestMapping(value = "/eliminaUsuarioA", method = RequestMethod.GET)
    public ModelAndView eliminaUsuarioA(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idUsuario = request.getParameter("idUsuario");

            boolean res = usuarioabi.eliminaUsuario(Integer.parseInt(idUsuario));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "FRTA Usuario ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaPais.json?idPais=<?>
    @RequestMapping(value = "/eliminaPais", method = RequestMethod.GET)
    public ModelAndView eliminaPais(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPais = request.getParameter("idPais");

            boolean res = paisbi.eliminaPais(Integer.parseInt(idPais));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PAIS ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaHorario.json?idHorario=<?>
    @RequestMapping(value = "/eliminaHorario", method = RequestMethod.GET)
    public ModelAndView eliminaHorario(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idHorario = request.getParameter("idHorario");

            boolean res = horariobi.eliminaHorario(Integer.parseInt(idHorario));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "HORARIO ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaParametro.json?&setClave=<?>
    @RequestMapping(value = "/eliminaParametro", method = RequestMethod.GET)
    public ModelAndView eliminaParametro(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String clave = request.getParameter("setClave");

            boolean res = parametrobi.eliminaParametro(clave);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PARAMETRO ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaNegocio.json?idNegocio=<?>
    @RequestMapping(value = "/eliminaNegocio", method = RequestMethod.GET)
    public ModelAndView eliminaNegocio(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idNegocio = request.getParameter("idNegocio");

            boolean res = negociobi.eliminaNegocio(Integer.parseInt(idNegocio));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "NEGOCIO ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaNivel.json?idNivel=<?>
    @RequestMapping(value = "/eliminaNivel", method = RequestMethod.GET)
    public ModelAndView eliminaNivel(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idNivel = request.getParameter("idNivel");

            boolean res = nivelbi.eliminaNivel(Integer.parseInt(idNivel));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "NIVEL ELIMINADO CORRECTAMENTE");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaCecos.json
    @RequestMapping(value = "/eliminaCecos", method = RequestMethod.GET)
    public ModelAndView eliminaCecos(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            int res = cecobi.eliminaCecosTrabajo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CECOS TRABAJO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaPerfilUsuario.json?idUsuario=<?>&idPerfil=<?>
    @RequestMapping(value = "/eliminaPerfilUsuario", method = RequestMethod.GET)
    public ModelAndView eliminaPerfilUsuario(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");

            PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();
            perfilUsuarioDTO.setIdUsuario(Integer.parseInt(idUsuario));
            perfilUsuarioDTO.setIdPerfil(Integer.parseInt(idPerfil));

            boolean res = perfilusuariobi.eliminaPerfilUsaurio(perfilUsuarioDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Perfil eliminado ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaRecursoPerfil.json?idRecurso=<?>&idPerfil=<?>
    @RequestMapping(value = "/eliminaRecursoPerfil", method = RequestMethod.GET)
    public ModelAndView eliminaRecursoPerfil(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idRecurso = request.getParameter("idRecurso");
            String idPerfil = request.getParameter("idPerfil");

            boolean res = recursoperfilbi.eliminaRecursoP(Integer.parseInt(idRecurso), Integer.parseInt(idPerfil));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Recurso Perfil eliminado ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaRecurso.json?idRecurso=<?>
    @RequestMapping(value = "/eliminaRecurso", method = RequestMethod.GET)
    public ModelAndView eliminaRecurso(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idRecurso = request.getParameter("idRecurso");

            boolean res = recursobi.eliminaRecurso(Integer.parseInt(idRecurso));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Recurso Eliminado eliminado ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaPosible.json?idPosible=<?>
    @RequestMapping(value = "/eliminaPosible", method = RequestMethod.GET)
    public ModelAndView eliminaPosible(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idPosible = request.getParameter("idPosible");

            boolean res = posiblesbi.eliminaPosible(Integer.parseInt(idPosible));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Posible Respuesta  eliminado ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaPosibleTipoPregunta.json?idPosibleTipoPreg=<?>
    @RequestMapping(value = "/eliminaPosibleTipoPregunta", method = RequestMethod.GET)
    public ModelAndView eliminaPosibleTipoPregunta(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idPosibleTipoPreg = request.getParameter("idPosibleTipoPreg");

            boolean res = posibletipopreguntabi.eliminaPosibleTipoPregunta(Integer.parseInt(idPosibleTipoPreg));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Posible Tipo Pregunta Respuesta  eliminado ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaDuplicadosCeco.json
    @RequestMapping(value = "/eliminaDuplicadosCeco", method = RequestMethod.GET)
    public ModelAndView eliminaDuplicadosCeco(HttpServletRequest request, HttpServletResponse response) {
        try {

            boolean res = cecobi.eliminaDuplicados();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Elimina CECOS duplicados ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaChecklistNegocio.json?idChecklist=<?>&idNegocio=<?>
    @RequestMapping(value = "/eliminaChecklistNegocio", method = RequestMethod.GET)
    public ModelAndView eliminaChecklistNegocio(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idChecklist = request.getParameter("idChecklist");
            String idNegocio = request.getParameter("idNegocio");

            boolean res = checklistNegocioBI.eliminaChecklistNegocio(Integer.parseInt(idChecklist), Integer.parseInt(idNegocio));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Checklist-Negocio eliminado ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaNegocioAdicional.json?idUsuario=<?>&idNegocio=<?>
    @RequestMapping(value = "/eliminaNegocioAdicional", method = RequestMethod.GET)
    public ModelAndView eliminaNegocioAdicional(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String idNegocio = request.getParameter("idNegocio");

            boolean res = adicionalBI.eliminaNegocioAdicional(Integer.parseInt(idUsuario), Integer.parseInt(idNegocio));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Negocio Adicional eliminado ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaNegocioPais.json?idNegocio=<?>&idPais=<?>
    @RequestMapping(value = "/eliminaNegocioPais", method = RequestMethod.GET)
    public ModelAndView eliminaNegocioPais(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idPais = request.getParameter("idPais");
            String idNegocio = request.getParameter("idNegocio");

            boolean res = paisNegocioBI.eliminaPaisNegocio(Integer.parseInt(idNegocio), Integer.parseInt(idPais));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Pais Neogocio eliminado ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaEmpFijo.json?idEmpFijo=<?>
    @RequestMapping(value = "/eliminaEmpFijo", method = RequestMethod.GET)
    public ModelAndView eliminaEmpFijo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idEmpFijo = request.getParameter("idEmpFijo");

            boolean res = empFijoBI.elimina(idEmpFijo);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Empleado Fijo Eliminado " + idEmpFijo);
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaResponsables.json?idExpediente=<?>
    @RequestMapping(value = "/eliminaResponsables", method = RequestMethod.GET)
    public ModelAndView eliminaResponsables(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idExpediente = request.getParameter("idExpediente");

            boolean res = expedienteBi.eliminaResponsables(Integer.parseInt(idExpediente));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Elimina Responsables");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaExpediente.json?idExpediente=<?>
    @RequestMapping(value = "/eliminaExpediente", method = RequestMethod.GET)
    public ModelAndView eliminaExpediente(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idExpediente = request.getParameter("idExpediente");

            boolean res = expedienteBi.eliminaExpediente(Integer.parseInt(idExpediente));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Elimina Expediente");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaStatusFormArchiveros.json?idStatus=<?>
    @RequestMapping(value = "/eliminaStatusFormArchiveros", method = RequestMethod.GET)
    public ModelAndView eliminaStatusFormArchiveros(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idStatus = 0;
            String idStatusS = request.getParameter("idStatus");
            if (idStatusS != null) {
                idStatus = Integer.parseInt(idStatusS);
            }

            boolean res = statusFormArchiverosBI.eliminaFormArchiveros(idStatus);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Elimina Status");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaFormArchiveros.json?idFormArchiveros=<?>
    @RequestMapping(value = "/eliminaFormArchiveros", method = RequestMethod.GET)
    public ModelAndView eliminaFormArchiveros(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idFormArchiveros = 0;
            String idFormArchiverosS = request.getParameter("idFormArchiveros");
            if (idFormArchiverosS != null) {
                idFormArchiveros = Integer.parseInt(idFormArchiverosS);
            }

            boolean res = formArchiverosBI.eliminaFormArchiveros(idFormArchiveros);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Elimina Formato");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaArchivero.json?idArchivero=<?>&idForm=<?>
    @RequestMapping(value = "/eliminaArchivero", method = RequestMethod.GET)
    public ModelAndView eliminaArchivero(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idArchivero = 0;
            String idArchiveroS = request.getParameter("idArchivero");
            if (idArchiveroS != null) {
                idArchivero = Integer.parseInt(idArchiveroS);
            }
            int idForm = 0;
            String idFormS = request.getParameter("idForm");
            if (idArchiveroS != null) {
                idForm = Integer.parseInt(idFormS);
            }

            boolean res = archiveroBI.eliminaArchivero(idArchivero, idForm);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Elimina Archivero");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/eliminaArchiveroForm.json?idFormArchivero=<?>
    @RequestMapping(value = "/eliminaArchiveroForm", method = RequestMethod.GET)
    public ModelAndView eliminaArchiveroForm(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idArchivero = 0;
            String idArchiveroS = request.getParameter("idFormArchivero");
            if (idArchiveroS != null) {
                idArchivero = Integer.parseInt(idArchiveroS);
            }

            List<ArchiveroDTO> lista = archiveroBI.obtieneArchiveroFormato(idArchivero);
            boolean res = true;
            for (ArchiveroDTO arch : lista) {
                boolean resp = archiveroBI.eliminaArchivero(arch.getIdArchivero(), idArchivero);
                if (resp == false && res == true) {
                    res = false;
                }

            }
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Elimina Archiveros por Formato");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

}
