package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.ArbolDecisionBI;
import com.gruposalinas.franquicia.business.AsignacionBI;
import com.gruposalinas.franquicia.business.BitacoraBI;
import com.gruposalinas.franquicia.business.ChecklistBI;
import com.gruposalinas.franquicia.business.ChecklistPreguntaBI;
import com.gruposalinas.franquicia.business.ChecklistUsuarioBI;
import com.gruposalinas.franquicia.business.CompromisoBI;
import com.gruposalinas.franquicia.business.EdoChecklistBI;
import com.gruposalinas.franquicia.business.EvidenciaBI;
import com.gruposalinas.franquicia.business.ImagenesRespuestaBI;
import com.gruposalinas.franquicia.business.ModuloBI;
import com.gruposalinas.franquicia.business.PreguntaBI;
import com.gruposalinas.franquicia.business.RespuestaAdBI;
import com.gruposalinas.franquicia.business.RespuestaBI;
import com.gruposalinas.franquicia.business.TipoChecklistBI;
import com.gruposalinas.franquicia.business.TipoPreguntaBI;
import com.gruposalinas.franquicia.domain.AsignacionDTO;
import com.gruposalinas.franquicia.resources.FRQAuthInterceptor;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("/checklistServices")
public class BajaChecklistService {

    @Autowired
    ModuloBI modulobi;
    @Autowired
    TipoPreguntaBI tipPreguntabi;
    @Autowired
    PreguntaBI preguntabi;
    @Autowired
    TipoChecklistBI tipoChecklistbi;
    @Autowired
    ChecklistBI checklistbi;
    @Autowired
    ChecklistPreguntaBI checklistPreguntabi;
    @Autowired
    ChecklistUsuarioBI checklistUsuariobi;
    @Autowired
    EdoChecklistBI edoChecklistbi;
    @Autowired
    ArbolDecisionBI arbolDecisionbi;
    @Autowired
    BitacoraBI bitacorabi;
    @Autowired
    RespuestaBI respuestabi;
    @Autowired
    CompromisoBI compromisobi;
    @Autowired
    EvidenciaBI evidenciabi;
    @Autowired
    RespuestaAdBI respuestaAdbi;
    @Autowired
    ImagenesRespuestaBI imagenesRespuestaBI;
    @Autowired
    AsignacionBI asignacionBI;

    private static final Logger logger = LogManager.getLogger(FRQAuthInterceptor.class);

    // http://localhost:8080/franquicia/checklistServices/eliminaModulo.json?idModulo=<?>
    @RequestMapping(value = "/eliminaModulo", method = RequestMethod.GET)
    public ModelAndView eliminaModulo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        String idModulo = request.getParameter("idModulo");

        boolean res = modulobi.eliminaModulo(Integer.parseInt(idModulo));

        ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
        mv.addObject("tipo", "MODULO ELIMINADO");
        mv.addObject("res", res);
        return mv;
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaTipoPregunta.json?idTipoPreg=<?>
    @RequestMapping(value = "/eliminaTipoPregunta", method = RequestMethod.GET)
    public ModelAndView eliminaTipoPregunta(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idTipoPregunta = request.getParameter("idTipoPreg");

            boolean res = tipPreguntabi.eliminaTipoPregunta(Integer.parseInt(idTipoPregunta));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "TIPO_PREGUNTA ELIMINADO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaPregunta.json?idPregunta=<?>
    @RequestMapping(value = "/eliminaPregunta", method = RequestMethod.GET)
    public ModelAndView eliminaPregunta(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPregunta = request.getParameter("idPregunta");

            boolean res = this.preguntabi.eliminaPregunta(Integer.parseInt(idPregunta));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PREGUNTA ELIMINADA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaPregunta.json?idPregunta=<?>
    @RequestMapping(value = "/eliminaPreguntaService", method = RequestMethod.GET)
    public @ResponseBody
    boolean eliminaPreguntaService(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPregunta = request.getParameter("idPregunta");

            boolean res = this.preguntabi.eliminaPregunta(Integer.parseInt(idPregunta));

            return res;
        } catch (Exception e) {
            logger.info(e);
            return false;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaTipoCheck.json?idTipoCheck=<?>
    @RequestMapping(value = "/eliminaTipoCheck", method = RequestMethod.GET)
    public ModelAndView eliminaTipoChecklist(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idTipoChck = request.getParameter("idTipoCheck");

            boolean res = tipoChecklistbi.eliminaTipoChecklist(Integer.parseInt(idTipoChck));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "TIPO CHECKLIST ELIMINADO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/checklistServices/eliminaEstadoChecklist.json?idEdoCheck=<?>
    @RequestMapping(value = "/eliminaEstadoChecklist", method = RequestMethod.GET)
    public ModelAndView eliminaEstadoChecklist(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idEdoChck = request.getParameter("idEdoCheck");

            boolean res = edoChecklistbi.eliminaEdoChecklist(Integer.parseInt(idEdoChck));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ESTADO CHECKLIST ELIMINADO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaCheck.json?idChecklist=<?>
    @RequestMapping(value = "/eliminaCheck", method = RequestMethod.GET)
    public ModelAndView eliminaCheck(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idChck = request.getParameter("idChecklist");

            boolean res = checklistbi.eliminaChecklist(Integer.parseInt(idChck));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "CHECKLIST ELIMINADO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/checklistServices/eliminaPregCheck.json?idChecklist=<?>&idPreg=<?>
    @RequestMapping(value = "/eliminaPregCheck", method = RequestMethod.GET)
    public ModelAndView eliminaPreguntaCheck(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = request.getParameter("idChecklist");
            String idPreg = request.getParameter("idPreg");

            boolean res = checklistPreguntabi.eliminaChecklistPregunta(Integer.parseInt(idCheck),
                    Integer.parseInt(idPreg));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PREGUNTA CHECKLIST ELIMINADA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
    // http://localhost:8080/franquicia/checklistServices/eliminaPregCheck.json?idChecklist=<?>&idPreg=<?>

    @RequestMapping(value = "/eliminaPregCheckService", method = RequestMethod.GET)
    public @ResponseBody
    boolean eliminaPreguntaCheckService(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheck = request.getParameter("idChecklist");
            String idPreg = request.getParameter("idPreg");

            boolean res = checklistPreguntabi.eliminaChecklistPregunta(Integer.parseInt(idCheck),
                    Integer.parseInt(idPreg));

            return res;
        } catch (Exception e) {
            logger.info(e);
            return false;
        }
    }

    /* AGREGUE IDCHECKUSUARIO (ID) */
    // http://localhost:8080/franquicia/checklistServices/eliminaUsuCheck.json?idCheckUsuario=<?>
    @RequestMapping(value = "/eliminaUsuCheck", method = RequestMethod.GET)
    public ModelAndView eliminaUsuarioCheck(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCheckUsuario = request.getParameter("idCheckUsuario");

            boolean res = checklistUsuariobi.eliminaChecklistUsuario(Integer.parseInt(idCheckUsuario));

            //ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "ID CHECKLIST ELIMINADO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/desactivaCheckusua.json?idChecklist=<?>
    @RequestMapping(value = "/desactivaCheckusua", method = RequestMethod.GET)
    public ModelAndView desactivaCheckusua(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idChecklist = request.getParameter("idChecklist");

            boolean res = checklistUsuariobi.desactivaChecklist(Integer.parseInt(idChecklist));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ID CHECKLIST DESACTIVADO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/desactivaCheckusuaBoolean.json?idChecklist=<?>
    @RequestMapping(value = "/desactivaCheckusuaBoolean", method = RequestMethod.GET)
    public @ResponseBody
    boolean desactivaCheckusuaBoolean(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idChecklist = request.getParameter("idChecklist");

            boolean res = checklistUsuariobi.desactivaChecklist(Integer.parseInt(idChecklist));

            return res;
        } catch (Exception e) {

            return false;
        }
    }

    /* AGREGUE IDSETARBOLDESICION (ID) */
    // http://localhost:8080/franquicia/checklistServices/eliminaArbolDecision.json?idArbolDecision=<?>
    @RequestMapping(value = "/eliminaArbolDecision", method = RequestMethod.GET)
    public ModelAndView eliminaArbolDecision(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idArbolDecision = request.getParameter("idArbolDecision");

            boolean res = arbolDecisionbi.eliminaArbolDecision(Integer.parseInt(idArbolDecision));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ARBOL ELIMINADO CON EXITO--->");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaBitacora.json?idBitacora=<?>
    @RequestMapping(value = "/eliminaBitacora", method = RequestMethod.GET)
    public ModelAndView eliminaBitacora(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idBitacora = request.getParameter("idBitacora");

            boolean res = bitacorabi.eliminaBitacora(Integer.parseInt(idBitacora));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "BITACORA ELIMINADA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaRespuesta.json?idRespuesta=<?>
    @RequestMapping(value = "/eliminaRespuesta", method = RequestMethod.GET)
    public ModelAndView eliminaRespuesta(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idRespuesta = request.getParameter("idRespuesta");

            boolean res = respuestabi.eliminaRespuesta(Integer.parseInt(idRespuesta));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "RESPUESTA ELIMINADA");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaRespuestasDuplicadas.json
    @RequestMapping(value = "/eliminaRespuestasDuplicadas", method = RequestMethod.GET)
    public ModelAndView eliminaRespuestasDuplicadas(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {

            boolean res = respuestabi.eliminaRespuestasDuplicadas();

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "RESPUESTAS DUPLICADAS ELIMINADAS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaCompromiso.json?idCompromiso=<?>
    @RequestMapping(value = "/eliminaCompromiso", method = RequestMethod.GET)
    public ModelAndView eliminaCompromiso(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCompromiso = request.getParameter("idCompromiso");

            boolean res = compromisobi.eliminaCompromiso(Integer.parseInt(idCompromiso));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "COMPROMISO ELIMINADO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaEvidencia.json?idEvidencia=<?>
    @RequestMapping(value = "/eliminaEvidencia", method = RequestMethod.GET)
    public ModelAndView eliminaEvidencia(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idEvidencia = request.getParameter("idEvidencia");

            boolean idEvi = evidenciabi.eliminaEvidencia(Integer.parseInt(idEvidencia));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "EVIDENCIA ELIMNADA");
            mv.addObject("res", idEvi);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaRespAd.json?idRespAd=<?>
    @RequestMapping(value = "/eliminaRespAd", method = RequestMethod.GET)
    public ModelAndView eliminaRespA(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idRespAd = request.getParameter("idRespAd");

            boolean idRespA = respuestaAdbi.eliminaRespAD(Integer.parseInt(idRespAd));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "RESPUESTA ADICIONAL ELIMINADA");
            mv.addObject("res", idRespA);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaImagenArbol.json?idImagen=<?>
    @RequestMapping(value = "/eliminaImagenArbol", method = RequestMethod.GET)
    public ModelAndView eliminaImagenArbol(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idImagen = request.getParameter("idImagen");

            boolean res = imagenesRespuestaBI.eliminaImagen(Integer.parseInt(idImagen));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "IMAGEN ARBOL ELIMINADA ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/checklistServices/eliminaAsignaciones.json?idChecklist=<?>&idCeco=<?>&idPuesto=<?>
    @RequestMapping(value = "/eliminaAsignaciones", method = RequestMethod.GET)
    public ModelAndView eliminaAsignaciones(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idChecklist = request.getParameter("idChecklist");
            String idCeco = request.getParameter("idCeco");
            String idPuesto = request.getParameter("idPuesto");

            AsignacionDTO asignacionDTO = new AsignacionDTO();

            asignacionDTO.setIdChecklist(Integer.parseInt(idChecklist));
            asignacionDTO.setCeco(idCeco);
            asignacionDTO.setIdPuesto(Integer.parseInt(idPuesto));

            boolean res = asignacionBI.eliminarAsignacion(asignacionDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "ASIGNACION ELIMINADA ");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
}
