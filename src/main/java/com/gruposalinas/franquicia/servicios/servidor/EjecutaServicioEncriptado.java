package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.TokenBI;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import com.gruposalinas.franquicia.util.UtilCryptoGS;
import com.gruposalinas.franquicia.util.UtilDate;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/ejecutaServiciosEncriptados")
public class EjecutaServicioEncriptado {

    @Autowired
    TokenBI tokenbi;

    // http://localhost:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/loginRest.json?id=196228
    // http://localhost:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/loginRest2.json?id=196228
    // http://localhost:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/checklist.json?id=196228&lat=19.30941887886347&lon=-99.18721647408643
    // http://localhost:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/checklist2.json?id=196228&lat=19.30941887886347&lon=-99.18721647408643&idCheck=2
    // http://localhost:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/checklistUsuario.json?id=191841&idCheck=1
    // http://localhost:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/checklistOffline.json?id=191312
    // http://10.51.210.239:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/checklistOffline.json?id=191312
    // http://10.51.210.239:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/tipoCifrado.json?id=191312
    // http://10.51.210.239:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCorreosAcuerdos.json?id=189870&idtienda=G2244&idBitacora=3611
    // http://10.51.210.239:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCorreosAcuerdos.json?id=189870&idtienda=191841&idBitacora=3875
    @RequestMapping(value = "/ejecutaServicio", method = RequestMethod.GET)
    public String ejecutaServicio(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException, GeneralSecurityException {
        System.out.println("Entro a ejecutar servicio");

        UtilDate fec = new UtilDate();
        @SuppressWarnings("static-access")
        String fecha = fec.getSysDate("ddMMyyyyHHmmss");

        String service = new String(request.getQueryString().getBytes("ISO-8859-1"), "UTF-8");
        String rutaService = service.split("\\?")[0].replace("service=", "");
        String params = service.replaceAll(rutaService, "").replace("service=", "").replace("?", "");
        String enc = new UtilCryptoGS().encryptParams(params);
        String token = new UtilCryptoGS().encryptParams("ip=" + FRQConstantes.getIpOrigen() + "&fecha=" + fecha + "&idapl=666&uri=http://" + FRQConstantes.getIpOrigen() + "/franquicia/" + rutaService).replace("\n", "");
        String tokenCifrado = tokenbi.getToken(token);
        String exec = (rutaService + "?" + enc + "&token=" + tokenCifrado).replace("\n", "");

        return "redirect:/" + exec;
    }

}
