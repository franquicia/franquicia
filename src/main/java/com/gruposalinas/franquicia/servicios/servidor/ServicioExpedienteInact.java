package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.domain.UsuarioDTO;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/central")
public class ServicioExpedienteInact {

    private static Logger logger = LogManager.getLogger(ServicioExpedienteInact.class);

    private static final int MAX_FILE_SIZE = 1024 * 1024 * 100; // 100MB

    /**
     * ********************************************* EXPEDIENTES
     * ****************************************************
     */
    //http://localhost:8080/franquicia/central/documentoExpediente.htm
    //@RequestMapping(value = "/documentoExpediente", method = RequestMethod.GET)
    @RequestMapping(value = {"/documentoExpediente", "/documentoCreditoActivo", "/documentoCreditoCancelados",
        "/documentoCaptacionActivo", "/documentoCaptacionCancelados"}, method = RequestMethod.GET)
    public ModelAndView getdocumentoExpediente(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String modelName = "";
        String fileNamePlus = "";
        String titulo = "";

        if (request.getRequestURI().contains("documentoExpediente")) {

            modelName = "documentoExpediente";
            fileNamePlus = "";

        } else if (request.getRequestURI().contains("documentoCreditoActivo")) {

            modelName = "documentoCreditoActivo";
            fileNamePlus = "_CreditoActivo";

        } else if (request.getRequestURI().contains("documentoCreditoCancelados")) {

            modelName = "documentoCreditoCancelados";
            fileNamePlus = "_CreditoCancelados";

        } else if (request.getRequestURI().contains("documentoCaptacionActivo")) {

            modelName = "documentoCaptacionActivo";
            fileNamePlus = "_CaptacionActivo";

        } else if (request.getRequestURI().contains("documentoCaptacionCancelados")) {

            modelName = "documentoCaptacionCancelados";
            fileNamePlus = "_CaptacionCancelados";

        }

        ModelAndView mv = new ModelAndView("documentoExpediente", "command", new FileParameters());
        //ModelAndView mv = new ModelAndView(modelName, "command", new FileParameters());

        Date fechaActual = new Date();
        System.out.println(fechaActual);
        System.out.println("---------------------------------------------");

        //Formateando la fecha:
        DateFormat formatoHora = new SimpleDateFormat("HHmmss");
        DateFormat formatoFecha = new SimpleDateFormat("ddMMyyyy");
        DateFormat formatoAnio = new SimpleDateFormat("yyyy");
        System.out.println("Son las: " + formatoHora.format(fechaActual) + " de fecha: " + formatoFecha.format(fechaActual) + " del anio: " + formatoAnio.format(fechaActual));

        String anio = formatoAnio.format(fechaActual);

        String fileRoute = "/franquicia/imgExpedientes/" + anio + "/";
        String ceco = "" + request.getSession().getAttribute("sucursal");

        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + fileRoute;

        File route = new File(ruta);

        //crear ruta
        File r = null;
        r = new File(ruta);

        logger.info("RUTA: " + ruta);
        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }

        String[] lista = route.list();

        mv.addObject("fileRoute", fileRoute);

        String nombreArchivo = ceco + fileNamePlus;

        if (new File(fileRoute).isDirectory()) {
            logger.info("El directorio " + fileRoute + " si existe");
            mv.addObject("listaArchivos", new File(fileRoute).list());
            for (String arch : lista) {
                //if(arch.contains(ceco)){
                if (arch.contains(nombreArchivo + ".pdf")) {
                    logger.info("Si se encontro");
                    mv.addObject("fileAction", "subirArchivo");
                    mv.addObject("OKARCH", "OK");
                    mv.addObject("NomArchivo", arch);
                    break;
                } else {
                    mv.addObject("OKARCH", "NO");
                    mv.addObject("NomArchivo", nombreArchivo);
                    logger.info("No se encontro");
                }
            }

        } else {
            logger.info("El directorio no existe");
            mv.addObject("directorio", "noExiste");
        }
        mv.addObject("fileAction", "subirArchivo");

        return mv;
    }

    //http://localhost:8080/franquicia/central/documentoExpediente.htm
    @RequestMapping(value = "/documentoExpediente", method = RequestMethod.POST)
    public ModelAndView postDocumentoExpediente(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "fileRoute", required = false) String fileRoute,
            @RequestParam(value = "fileAction", required = false) String fileAction)
            throws IOException {

        ModelAndView mv = new ModelAndView("documentoExpediente", "command", new FileParameters());

        Date fechaActual = new Date();
        System.out.println(fechaActual);
        System.out.println("---------------------------------------------");

        //Formateando la fecha:
        DateFormat formatoHora = new SimpleDateFormat("HHmmss");
        DateFormat formatoFecha = new SimpleDateFormat("ddMMyyyy");
        DateFormat formatoAnio = new SimpleDateFormat("yyyy");
        System.out.println("Son las: " + formatoHora.format(fechaActual) + " de fecha: " + formatoFecha.format(fechaActual) + " del anio: " + formatoAnio.format(fechaActual));

        String anio = formatoAnio.format(fechaActual);

        String fileRouteX = "/franquicia/imgExpedientes/" + anio + "/";
        String ceco = "" + request.getSession().getAttribute("sucursal");

        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + fileRouteX;

        File route = new File(ruta);

        //crear ruta
        File r = null;
        r = new File(ruta);

        logger.info("RUTA: " + ruta);
        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }
        String[] lista = route.list();

        if (new File(fileRouteX).isDirectory()) {
            logger.info("El directorio " + fileRouteX + " si existe");
            mv.addObject("listaArchivos", new File(fileRouteX).list());
            for (String arch : lista) {
                if (arch.contains(ceco)) {
                    logger.info("Si se encontro");
                    mv.addObject("fileAction", "subirArchivo");
                    mv.addObject("OKARCH", "OK");
                    mv.addObject("NomArchivo", arch);
                    break;
                } else {
                    mv.addObject("OKARCH", "NO");
                    logger.info("No se encontro");
                }
            }

        } else {
            logger.info("El directorio no existe");
            mv.addObject("directorio", "noExiste");
        }
        mv.addObject("fileAction", "subirArchivo");

        return mv;

    }

    /**
     * ********************************************* Archiveros
     * ****************************************************
     */
    //http://localhost:8080/franquicia/central/documentoArchiveros.htm
    @RequestMapping(value = "/documentoArchiveros", method = RequestMethod.GET)
    public ModelAndView getDocumentoArchiveros(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ModelAndView mv = new ModelAndView("documentoArchivero", "command", new FileParameters());

        //String fileRoute="http://10.53.33.82/franquicia/imagenes/";
        Date fechaActual = new Date();
        System.out.println(fechaActual);
        System.out.println("---------------------------------------------");

        //Formateando la fecha:
        DateFormat formatoHora = new SimpleDateFormat("HHmmss");
        DateFormat formatoFecha = new SimpleDateFormat("ddMMyyyy");
        DateFormat formatoAnio = new SimpleDateFormat("yyyy");
        System.out.println("Son las: " + formatoHora.format(fechaActual) + " de fecha: " + formatoFecha.format(fechaActual) + " del anio: " + formatoAnio.format(fechaActual));

        String anio = formatoAnio.format(fechaActual);

        String fileRoute = "/franquicia/imgArchiveros/" + anio + "/";
        String ceco = "" + request.getSession().getAttribute("sucursal");

        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + fileRoute;

        File route = new File(ruta);

        //crear ruta
        File r = null;
        r = new File(ruta);

        mv.addObject("fileRoute", fileRoute);

        logger.info("RUTA: " + ruta);
        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }

        String[] lista = route.list();

        if (new File(fileRoute).isDirectory()) {
            logger.info("El directorio " + fileRoute + " si existe");
            mv.addObject("listaArchivos", new File(fileRoute).list());
            for (String arch : lista) {
                if (arch.contains(ceco)) {
                    logger.info("Si se encontro");
                    mv.addObject("fileAction", "subirArchivo");
                    mv.addObject("OKARCH", "OK");
                    mv.addObject("NomArchivo", arch);
                    break;
                } else {
                    mv.addObject("OKARCH", "NO");
                    logger.info("No se encontro");
                }
            }

        } else {
            logger.info("El directorio no existe");
            mv.addObject("directorio", "noExiste");
        }
        mv.addObject("fileAction", "subirArchivo");

        return mv;
    }

    //http://localhost:8080/franquicia/central/documentoArchiveros.htm
    @RequestMapping(value = "/documentoArchiveros", method = RequestMethod.POST)
    public ModelAndView postDocumentoArchiveros(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "fileRoute", required = false) String fileRoute,
            @RequestParam(value = "fileAction", required = false) String fileAction)
            throws IOException {

        ModelAndView mv = new ModelAndView("documentoArchivero", "command", new FileParameters());

        Date fechaActual = new Date();
        System.out.println(fechaActual);
        System.out.println("---------------------------------------------");

        //Formateando la fecha:
        DateFormat formatoHora = new SimpleDateFormat("HHmmss");
        DateFormat formatoFecha = new SimpleDateFormat("ddMMyyyy");
        DateFormat formatoAnio = new SimpleDateFormat("yyyy");
        System.out.println("Son las: " + formatoHora.format(fechaActual) + " de fecha: " + formatoFecha.format(fechaActual) + " del anio: " + formatoAnio.format(fechaActual));

        String anio = formatoAnio.format(fechaActual);

        String fileRouteX = "/franquicia/imgArchiveros/" + anio + "/";
        String ceco = "" + request.getSession().getAttribute("sucursal");

        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + fileRouteX;

        File route = new File(ruta);

        //crear ruta
        File r = null;
        r = new File(ruta);

        logger.info("RUTA: " + ruta);
        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }
        String[] lista = route.list();

        if (new File(fileRouteX).isDirectory()) {
            logger.info("El directorio " + fileRouteX + " si existe");
            mv.addObject("listaArchivos", new File(fileRouteX).list());
            for (String arch : lista) {
                if (arch.contains(ceco)) {
                    logger.info("Si se encontro");
                    mv.addObject("fileAction", "subirArchivo");
                    mv.addObject("OKARCH", "OK");
                    mv.addObject("NomArchivo", arch);
                    break;
                } else {
                    mv.addObject("OKARCH", "NO");
                    logger.info("No se encontro");
                }
            }

        } else {
            logger.info("El directorio no existe");
            mv.addObject("directorio", "noExiste");
        }
        mv.addObject("fileAction", "subirArchivo");

        return mv;

    }

    //http://localhost:8080/franquicia/central/uploadExpedFiles.htm
    @RequestMapping(value = "/uploadExpedFiles", method = RequestMethod.POST)
    public ModelAndView postUploadExpedFiless(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "uploadedFile", required = false) MultipartFile uploadedFile,
            @RequestParam(value = "fileRoute", required = false) String fileRoute,
            @RequestParam(value = "fileAction", required = false) String fileAction,
            @RequestParam(value = "nombreArchivo", required = false) String nombreArchivo
    )
            throws IOException {

        nombreArchivo = nombreArchivo.replace(".pdf", "");

        ModelAndView mv = new ModelAndView("documentoExpediente", "command", new FileParameters());

        Date fechaActual = new Date();
        System.out.println(fechaActual);
        System.out.println("---------------------------------------------");

        //Formateando la fecha:
        DateFormat formatoHora = new SimpleDateFormat("HHmmss");
        DateFormat formatoFecha = new SimpleDateFormat("ddMMyyyy");
        DateFormat formatoAnio = new SimpleDateFormat("yyyy");
        System.out.println("Son las: " + formatoHora.format(fechaActual) + " de fecha: " + formatoFecha.format(fechaActual) + " del anio: " + formatoAnio.format(fechaActual));

        String anio = formatoAnio.format(fechaActual);

        String fileRouteX = "/franquicia/imgExpedientes/" + anio + "/";
        String ceco = "" + request.getSession().getAttribute("sucursal");

        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + fileRouteX;

        File route = new File(ruta);

        //crear ruta
        File r = null;
        r = new File(ruta);

        logger.info("RUTA: " + ruta);
        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }

        if (route.exists()) {
            if (uploadedFile.getSize() < MAX_FILE_SIZE) {
                if (checkContentType(uploadedFile.getContentType())) {
                    FileOutputStream fileOutput = null;
                    BufferedOutputStream bufferOutput = null;
                    try {
                        //fileOutput = new FileOutputStream(ruta + ceco+".pdf");
                        fileOutput = new FileOutputStream(ruta + nombreArchivo + ".pdf");
                        //fileOutput = new FileOutputStream(ruta + uploadedFile.getOriginalFilename());

                        bufferOutput = new BufferedOutputStream(fileOutput);

                        bufferOutput.write((byte[]) uploadedFile.getBytes());
                        bufferOutput.close();

                        File uploadedFile2 = new File(ruta + uploadedFile.getOriginalFilename());
                        String cad = ruta + ceco + uploadedFile.getOriginalFilename();
                        //uploadedFile2.renameTo(new File(ruta + ceco+uploadedFile.getOriginalFilename()));

                        logger.info("Archivo " + uploadedFile.getOriginalFilename() + " fue subido exitosamente!");
                        mv.addObject("archivoSubido", "si");
                        mv.addObject("OKARCH", "OK");
                        //mv.addObject("NomArchivo", ceco+".pdf");
                        mv.addObject("NomArchivo", nombreArchivo + ".pdf");
                    } catch (Exception e) {
                        mv.addObject("archivoSubido", "no");
                        e.printStackTrace();
                        logger.info("ERROR: " + e.getMessage());
                    } finally {
                        try {
                            if (fileOutput != null) {
                                fileOutput.close();
                            }
                            if (bufferOutput != null) {
                                bufferOutput.close();
                            }
                        } catch (Exception e) {
                            logger.info("ERROR: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                } else {
                    mv.addObject("archivoSubido", "no-compatible");
                    logger.info("El archivo " + uploadedFile.getOriginalFilename() + " tiene una extensión incompatible.");
                }
            } else {
                mv.addObject("archivoSubido", "size");
                logger.info("El archivo " + uploadedFile.getOriginalFilename() + " es mayor a 100MB");
            }
        } else {
            mv.addObject("archivoSubido", "no");
            logger.info("El directorio " + fileRouteX + " no existe");
        }

        mv.addObject("fileAction", fileAction);
        mv.addObject("fileRoute", fileRouteX);

        return mv;

    }

    //http://localhost:8080/franquicia/central/uploadArchiverosFiles.htm
    @RequestMapping(value = "/uploadArchiverosFiles", method = RequestMethod.POST)
    public ModelAndView postUploadArchiverosFiles(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "uploadedFile", required = false) MultipartFile uploadedFile,
            @RequestParam(value = "fileRoute", required = false) String fileRoute,
            @RequestParam(value = "fileAction", required = false) String fileAction)
            throws IOException {

        ModelAndView mv = new ModelAndView("documentoArchivero", "command", new FileParameters());

        Date fechaActual = new Date();
        System.out.println(fechaActual);
        System.out.println("---------------------------------------------");

        //Formateando la fecha:
        DateFormat formatoHora = new SimpleDateFormat("HHmmss");
        DateFormat formatoFecha = new SimpleDateFormat("ddMMyyyy");
        DateFormat formatoAnio = new SimpleDateFormat("yyyy");
        System.out.println("Son las: " + formatoHora.format(fechaActual) + " de fecha: " + formatoFecha.format(fechaActual) + " del anio: " + formatoAnio.format(fechaActual));

        String anio = formatoAnio.format(fechaActual);

        String fileRouteX = "/franquicia/imgArchiveros/" + anio + "/";
        String ceco = "" + request.getSession().getAttribute("sucursal");

        String rootPath = File.listRoots()[0].getAbsolutePath();
        String ruta = rootPath + fileRouteX;

        File route = new File(ruta);

        //crear ruta
        File r = null;
        r = new File(ruta);

        logger.info("RUTA: " + ruta);
        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }

        if (route.exists()) {
            if (uploadedFile.getSize() < MAX_FILE_SIZE) {
                if (checkContentType(uploadedFile.getContentType())) {
                    FileOutputStream fileOutput = null;
                    BufferedOutputStream bufferOutput = null;
                    try {
                        fileOutput = new FileOutputStream(ruta + ceco + ".pdf");
                        //fileOutput = new FileOutputStream(ruta + uploadedFile.getOriginalFilename());

                        bufferOutput = new BufferedOutputStream(fileOutput);

                        bufferOutput.write((byte[]) uploadedFile.getBytes());
                        bufferOutput.close();

                        File uploadedFile2 = new File(ruta + uploadedFile.getOriginalFilename());
                        String cad = ruta + ceco + uploadedFile.getOriginalFilename();
                        //uploadedFile2.renameTo(new File(ruta + ceco+uploadedFile.getOriginalFilename()));

                        logger.info("Archivo " + uploadedFile.getOriginalFilename() + " fue subido exitosamente!");
                        mv.addObject("archivoSubido", "si");
                        mv.addObject("OKARCH", "OK");
                        mv.addObject("NomArchivo", ceco + ".pdf");
                    } catch (Exception e) {
                        mv.addObject("archivoSubido", "no");
                        e.printStackTrace();
                        logger.info("ERROR: " + e.getMessage());
                    } finally {
                        try {
                            if (fileOutput != null) {
                                fileOutput.close();
                            }
                            if (bufferOutput != null) {
                                bufferOutput.close();
                            }
                        } catch (Exception e) {
                            logger.info("ERROR: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                } else {
                    mv.addObject("archivoSubido", "no-compatible");
                    logger.info("El archivo " + uploadedFile.getOriginalFilename() + " tiene una extensión incompatible.");
                }
            } else {
                mv.addObject("archivoSubido", "size");
                logger.info("El archivo " + uploadedFile.getOriginalFilename() + " es mayor a 100MB");
            }
        } else {
            mv.addObject("archivoSubido", "no");
            logger.info("El directorio " + fileRouteX + " no existe");
        }

        mv.addObject("fileAction", fileAction);
        mv.addObject("fileRoute", fileRouteX);

        return mv;

    }

    /**
     * ********************************************* ARCHIVEROS
     * ****************************************************
     */
//	//http://localhost:8080/franquicia/central/documentoExpediente.htm
//		@RequestMapping(value = "/documentoArchiveros", method = RequestMethod.GET)
//		public ModelAndView getFileCenterArch(HttpServletRequest request, HttpServletResponse response) 
//				throws ServletException, IOException {
//			
//			ModelAndView mv = new ModelAndView("documentoExpediente", "command", new FileParameters());
//			
//			//String fileRoute="http://10.53.33.82/franquicia/imagenes/";
//			String fileRoute="/franquicia/imagenes/";
//			String ceco=""+request.getSession().getAttribute("sucursal");
//			
//			
//			String rootPath = File.listRoots()[0].getAbsolutePath();
//			String ruta = rootPath + fileRoute;
//
//			File route = new File(ruta);
//			//File route = new File(fileRoute);
//			
//			//List<String> lista = new ArrayList<String>();
//			String[] lista =route.list();
//			
//			
//			if (new File(fileRoute).isDirectory()) {
//				logger.info("El directorio " + fileRoute + " si existe");
//				mv.addObject("listaArchivos", new File(fileRoute).list());
//				for(String arch: lista){
//					if(arch.contains(ceco)){
//						logger.info("Si se encontro");
//						mv.addObject("fileAction", "subirArchivo");
//						mv.addObject("OKARCH", "OK");
//						mv.addObject("NomArchivo", arch);
//						break;
//					}
//					else{
//						mv.addObject("OKARCH", "NO");
//						logger.info("No se encontro");
//					}
//				}
//				
//			}
//			else {
//				logger.info("El directorio no existe");
//				mv.addObject("directorio", "noExiste");
//			}
//			mv.addObject("fileAction", "subirArchivo");
//
//			return mv;
//		}
//
//		//http://localhost:8080/franquicia/central/documentoExpediente.htm
//		@RequestMapping(value = "/documentoArchiveros", method = RequestMethod.POST)
//		public ModelAndView postFileCenterArch(HttpServletRequest request, HttpServletResponse response, Model model,
//				@RequestParam(value = "fileRoute", required = false) String fileRoute,
//				@RequestParam(value = "fileAction", required = false) String fileAction) 
//						throws IOException {
//
//			ModelAndView mv = new ModelAndView("documentoExpediente", "command", new FileParameters());
//			
//			//String fileRoute="http://10.53.33.82/franquicia/imagenes/";
//			fileRoute="/franquicia/imagenes/";
//			String ceco=""+request.getSession().getAttribute("sucursal");
//			
//			
//			String rootPath = File.listRoots()[0].getAbsolutePath();
//			String ruta = rootPath + fileRoute;
//
//			File route = new File(ruta);
//			//File route = new File(fileRoute);
//			
//			//List<String> lista = new ArrayList<String>();
//			String[] lista =route.list();
//			
//			
//			if (new File(fileRoute).isDirectory()) {
//				logger.info("El directorio " + fileRoute + " si existe");
//				mv.addObject("listaArchivos", new File(fileRoute).list());
//				for(String arch: lista){
//					if(arch.contains(ceco)){
//						logger.info("Si se encontro");
//						mv.addObject("fileAction", "subirArchivo");
//						mv.addObject("OKARCH", "OK");
//						mv.addObject("NomArchivo", arch);
//						break;
//					}
//					else{
//						mv.addObject("OKARCH", "NO");
//						logger.info("No se encontro");
//					}
//				}
//				
//			}
//			else {
//				logger.info("El directorio no existe");
//				mv.addObject("directorio", "noExiste");
//			}
//			mv.addObject("fileAction", "subirArchivo");
//
//			return mv;
//
//		}	
//		
//		//http://localhost:8080/franquicia/central/uploadExpedFiles.htm
//		@RequestMapping(value = "/uploadExpedFiles", method = RequestMethod.POST)
//		public ModelAndView postUploadFilesArch(HttpServletRequest request, HttpServletResponse response, Model model,
//				@RequestParam(value = "uploadedFile", required = false) MultipartFile uploadedFile,
//				@RequestParam(value = "fileRoute", required = false) String fileRoute,
//				@RequestParam(value = "fileAction", required = false) String fileAction)
//						throws IOException {
//
//			ModelAndView mv = new ModelAndView("documentoExpediente", "command", new FileParameters());
//			
//			String ceco=""+request.getSession().getAttribute("sucursal");
//			fileRoute="/franquicia/imagenes/";
//			String rootPath = File.listRoots()[0].getAbsolutePath();
//			String ruta = rootPath + fileRoute;
//
//			File route = new File(ruta);
//
//			if (route.exists()) {
//				if (uploadedFile.getSize() < MAX_FILE_SIZE) {
//					if (checkContentType(uploadedFile.getContentType())) {
//						FileOutputStream fileOutput=null;
//						BufferedOutputStream bufferOutput=null;
//			   			try {
//			   				fileOutput = new FileOutputStream(ruta + ceco+".pdf");
//			   				//fileOutput = new FileOutputStream(ruta + uploadedFile.getOriginalFilename());
//			   				
//			   				
//			   				bufferOutput = new BufferedOutputStream(fileOutput);
//			   				
//			   				bufferOutput.write((byte []) uploadedFile.getBytes());
//			   				bufferOutput.close();
//			   				
//			   				File uploadedFile2 = new File(ruta + uploadedFile.getOriginalFilename());
//			   				String cad=ruta + ceco+uploadedFile.getOriginalFilename();
//			   				//uploadedFile2.renameTo(new File(ruta + ceco+uploadedFile.getOriginalFilename()));
//			   				
//			       			logger.info("Archivo " + uploadedFile.getOriginalFilename() + " fue subido exitosamente!");
//			       			mv.addObject("archivoSubido", "si");
//			       			mv.addObject("OKARCH", "OK");
//							mv.addObject("NomArchivo", ceco+".pdf");
//			   			}
//			   			catch (Exception e) {
//			       			mv.addObject("archivoSubido", "no");
//			       			e.printStackTrace();
//			   				logger.info("ERROR: " + e.getMessage());
//			   			}
//			   			finally{
//			   				try{
//			   					if(fileOutput!=null){
//			   						fileOutput.close();
//			   					}
//			   					if(bufferOutput!=null){
//			   						bufferOutput.close();
//			   					}
//			   				}
//			   				catch(Exception e) {
//				   				logger.info("ERROR: " + e.getMessage());
//				   				e.printStackTrace();
//				   			}
//			   			}
//					}
//					else {
//		       			mv.addObject("archivoSubido", "no-compatible");
//						logger.info("El archivo " + uploadedFile.getOriginalFilename() + " tiene una extensión incompatible.");
//					}
//		       	}
//		       	else {
//	       			mv.addObject("archivoSubido", "size");
//		   			logger.info("El archivo " + uploadedFile.getOriginalFilename() + " es mayor a 100MB");
//		       	}
//			}
//			else {
//	   			mv.addObject("archivoSubido", "no");
//				logger.info("El directorio " + fileRoute + " no existe");
//			}
//			
//			mv.addObject("fileAction", fileAction);
//			mv.addObject("fileRoute", fileRoute);
//
//			return mv;
//			
//		}
//	
    // http://localhost:8080/franquicia/central/conteoDocumentos.json
    @RequestMapping(value = "/conteoDocumentos", method = RequestMethod.GET)
    public ModelAndView eliminaCanal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Conteo de Archivos");

            String fileRoute = "/franquicia/imagenes/";
            String ceco = "" + request.getSession().getAttribute("sucursal");

            String rootPath = File.listRoots()[0].getAbsolutePath();
            String ruta = rootPath + fileRoute;

            File route = new File(ruta);
            //File route = new File(fileRoute);

            //List<String> lista = new ArrayList<String>();
            String[] lista = route.list();

            int conteo = 0;
            for (String arch : lista) {
                if (arch.contains(".pdf") || arch.contains(".PDF")) {
                    conteo++;
                }

            }

            mv.addObject("res", conteo);
            return mv;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/soporte/deleteFiles.htm
    @RequestMapping(value = "/deleteFiles2", method = RequestMethod.POST)
    public ModelAndView postListFiles(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "fileRoute", required = true) String fileRoute,
            @RequestParam(value = "fileAction", required = true) String fileAction)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexFileCenter", "command", new FileParameters());

        List<String> listaArchivosEliminar = new ArrayList<String>();
        List<String> auxList = new ArrayList<String>();
        List<String> fullList = new ArrayList<String>();

        String ruta = File.listRoots()[0].getAbsolutePath() + fileRoute;

        File directory = new File(ruta);

        for (String video : directory.list()) {
            fullList.add(video);
        }

        if (request.getParameterValues("listaArchivosEliminar") != null) {
            for (String video : request.getParameterValues("listaArchivosEliminar")) {
                listaArchivosEliminar.add(video);
            }

            if (listaArchivosEliminar.size() > 0) {
                if (directory.exists()) {
                    File deleteFile;

                    for (String file : listaArchivosEliminar) {
                        deleteFile = new File(ruta + file);

                        if (deleteFile.delete()) {
                            logger.info("El archivo " + file + " fue eliminado");
                            auxList.add(file);
                            mv.addObject("eliminado", "si");
                        } else {
                            logger.info("El archivo " + file + " no fue eliminado");
                            mv.addObject("eliminado", "no");
                        }
                    }

                    deleteFile = null;

                    if (auxList.isEmpty()) {
                        mv.addObject("listaArchivos", fullList);
                    } else {
                        fullList.removeAll(auxList);
                        mv.addObject("listaArchivos", fullList);
                    }
                } else {
                    logger.info("El directorio no existe, no se pueden eliminar los vídeos");
                    mv.addObject("eliminado", "no");
                    mv.addObject("listaArchivos", fullList);
                }
            } else {
                logger.info("La lista se encuentra vacía, no se puede eliminar ningún vídeo");
                mv.addObject("eliminado", "no");
            }
        } else {
            mv.addObject("eliminado", "no-vacio");
            mv.addObject("listaArchivos", fullList);
        }

        mv.addObject("fileAction", fileAction);
        mv.addObject("fileRoute", fileRoute);

        return mv;

    }

    //http://localhost:8080/franquicia/soporte/renameFiles.htm
    @RequestMapping(value = "/renameFiles2", method = RequestMethod.POST)
    public ModelAndView postRenameFiles(HttpServletRequest request, HttpServletResponse response, Model model,
            @RequestParam(value = "fileRoute", required = true) String fileRoute,
            @RequestParam(value = "fileAction", required = true) String fileAction,
            @RequestParam(value = "oldFileName", required = true) String oldFileName,
            @RequestParam(value = "newFileName", required = true) String newFileName)
            throws ServletException, IOException {

        if (FRQConstantes.PRODUCCION) {
            UsuarioDTO userSession = (UsuarioDTO) request.getSession().getAttribute("user");
            Integer user = Integer.parseInt(userSession.getIdUsuario());

            if (!(user.equals(189140) || user.equals(196228) || user.equals(191841) || user.equals(191312)
                    || user.equals(189870) || user.equals(189871) || user.equals(192201) || user.equals(664899)
                    || user.equals(643965) || user.equals(304513) || user.equals(331952))) {
                return new ModelAndView("redirect:/views/http404.jsp");
            }
        }

        ModelAndView mv = new ModelAndView("indexFileCenter", "command", new FileParameters());

        File directory = new File(fileRoute);

        if (oldFileName != null) {
            logger.info("NOT NULL");
        } else {
            logger.info("NULL");
        }

        if (directory.isDirectory()) {
            File uploadedFile = new File(fileRoute + oldFileName);

            if (uploadedFile.isFile()) {
                try {
                    if (uploadedFile.renameTo(new File(fileRoute + newFileName))) {
                        logger.info("El archivo " + uploadedFile.getName() + " fue renombrado exitosamente");
                        mv.addObject("fileAction", fileAction);
                        mv.addObject("fileRoute", fileRoute);
                        mv.addObject("renombrado", "si");
                    } else {
                        logger.info("El archivo " + uploadedFile.getName() + " no pudo ser renombrado");
                        mv.addObject("fileAction", fileAction);
                        mv.addObject("fileRoute", fileRoute);
                        mv.addObject("renombrado", "no");
                    }
                } catch (Exception e) {
                    logger.info("ERROR " + e.getMessage());
                }
            } else {
                uploadedFile = null;
                logger.info("El archivo no existe o su nombre no es correcto.");
                mv.addObject("renombrado", "no");
            }

            List<String> fullList = new ArrayList<String>();

            for (String video : directory.list()) {
                fullList.add(video);
            }

            mv.addObject("listaArchivos", fullList);
        } else {
            logger.info("El directorio no existe o no es correcto.");
            mv.addObject("renombrado", "no");
        }

        return mv;

    }

    /**
     * audio/aac	//AAC audio/mp4	//AAC (Firefox) video/x-msvideo //AVI
     * application/octet-stream	//BIN text/css	//CSS application/msword	//DOC
     * application/epub+zip	//EPUB image/gif	//GIF text/html	//HTM / HTML
     * image/x-icon	//ICO application/java-archive	//JAR image/jpeg	//JPG / JPEG
     * application/javascript	//JS application/json	//JSON video/mpeg	//MPEG
     * audio/mpeg	//MP3 video/mp4	//MP4 video/quicktime	//MOV image/png	//PNG
     * application/pdf	//PDF application/vnd.ms-powerpoint	//PPT
     * application/x-rar-compressed	//RAR application/rtf	//RTF audio/x-wav
     * //WAV video/x-ms-wmv	//WMV application/xhtml+xml	//XHMTL
     * application/vnd.ms-excel	//XLS application/xml	//XML application/zip
     * //ZIP
     *
     * application/vnd.openxmlformats-officedocument.wordprocessingml.document
     * //DOCX application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
     * //XLSX
     * application/vnd.openxmlformats-officedocument.presentationml.presentation
     * //PPTX
     *
     */
    boolean checkContentType(String contentType) {

        String[] contentTypeList = {"audio/aac", "video/x-msvideo", "application/octet-stream", "text/css", "application/msword", "application/epub+zip",
            "image/gif", "text/html", "image/x-icon", "application/java-archive", "image/jpeg", "application/javascript", "application/json", "video/mpeg",
            "audio/mpeg", "video/mp4", "image/png", "application/pdf", "application/vnd.ms-powerpoint", "application/x-rar-compressed", "application/rtf",
            "audio/x-wav", "application/xhtml+xml", "application/vnd.ms-excel", "application/xml", "application/zip",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "audio/mp4", "video/quicktime",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "video/x-ms-wmv",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            null};

        for (String string : contentTypeList) {
            if (string.equals(contentType)) {
                return true;
            }
        }

        return false;
    }
}

class FileParametersExped {

    private MultipartFile video;
    private String fileAction;
    private String fileRoute;
    private String newFileName;
    private String oldFileName;

    public MultipartFile getVideo() {
        return video;
    }

    public void setVideo(MultipartFile video) {
        this.video = video;
    }

    public String getFileAction() {
        return fileAction;
    }

    public void setFileAction(String fileAction) {
        this.fileAction = fileAction;
    }

    public String getFileRoute() {
        return fileRoute;
    }

    public void setFileRoute(String fileRoute) {
        this.fileRoute = fileRoute;
    }

    public String getNewFileName() {
        return newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName;
    }

    public String getOldFileName() {
        return oldFileName;
    }

    public void setOldFileName(String oldFileName) {
        this.oldFileName = oldFileName;
    }
}
