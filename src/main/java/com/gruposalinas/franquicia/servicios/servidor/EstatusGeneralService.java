package com.gruposalinas.franquicia.servicios.servidor;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.gruposalinas.franquicia.business.CargaArchivosPedestalDigitalBI;
import com.gruposalinas.franquicia.business.EstatusGeneralBI;
import com.gruposalinas.franquicia.business.FilesSucursalPedestalBI;
import com.gruposalinas.franquicia.dao.PedestalDigitalDAO;
import com.gruposalinas.franquicia.domain.AdminParametroPDDTO;
import com.gruposalinas.franquicia.domain.BachDescargasPDDTO;
import com.gruposalinas.franquicia.domain.CatalogoEstatusDocPDDTO;
import com.gruposalinas.franquicia.domain.CecoDataPDDTO;
import com.gruposalinas.franquicia.domain.CecoInfoGraphPDDTO;
import com.gruposalinas.franquicia.domain.CecosByNegPDDTO;
import com.gruposalinas.franquicia.domain.DataTabletPDDTO;
import com.gruposalinas.franquicia.domain.DatosTablaEGDTO;
import com.gruposalinas.franquicia.domain.DocumentoSucursalDTO;
import com.gruposalinas.franquicia.domain.FolioDetailPDDTO;
import com.gruposalinas.franquicia.domain.FolioFilterDataPDDTO;
import com.gruposalinas.franquicia.domain.FoliosEstatusPDDTO;
import com.gruposalinas.franquicia.domain.GeoDataPDDTO;
import com.gruposalinas.franquicia.domain.HistoricTabletDataPDDTO;
import com.gruposalinas.franquicia.domain.IndicadoresDataEGDTO;
import com.gruposalinas.franquicia.domain.IndicadoresDataETDTO;
import com.gruposalinas.franquicia.domain.ListaCategoriasPDDTO;
import com.gruposalinas.franquicia.domain.ListaCecoPorCSVPDDTO;
import com.gruposalinas.franquicia.domain.ListaEstatusTabletaPDDTO;
import com.gruposalinas.franquicia.domain.LoginPDDTO;
import com.gruposalinas.franquicia.domain.ParamNegocioPDDTO;
import com.gruposalinas.franquicia.domain.QuejasPDDTO;
import com.gruposalinas.franquicia.domain.RepDetaCecoPDDTO;
import com.gruposalinas.franquicia.domain.RepGralCecoPDDTO;
import com.gruposalinas.franquicia.domain.SucEstTabPDDTO;
import com.gruposalinas.franquicia.domain.SucursalInfoPDDTO;
import com.gruposalinas.franquicia.domain.SucursalTareasListPDDTO;
import com.gruposalinas.franquicia.domain.TablaEstatusGeneralDTO;
import com.gruposalinas.franquicia.domain.TabletDataFilterETDTO;
import com.gruposalinas.franquicia.domain.TabletInformationPDDTO;
import com.gruposalinas.franquicia.domain.TabletPerformancePDDTO;
import com.gruposalinas.franquicia.domain.TaskInTabletPDDAO;
import com.gruposalinas.franquicia.domain.TipoQuejaPDDTO;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("central/pedestalDigital/")
public class EstatusGeneralService {

    private static Logger logger = LogManager.getLogger(EstatusGeneralService.class);

    @Autowired
    EstatusGeneralBI estatusGeneralBI;

    @Autowired
    CargaArchivosPedestalDigitalBI cargaArchivos;

    @Autowired
    PedestalDigitalDAO pdDAO;

    @Autowired
    FilesSucursalPedestalBI filesSucursalBI;

    private String path = File.listRoots()[0].getAbsolutePath() + "/franquicia/pedestal_digital/carga_archivos/";

    /**
     * *************************
     */
    @RequestMapping(value = "revistaDigital.htm", method = RequestMethod.GET)
    public ModelAndView revistaDigital(HttpServletRequest request, HttpServletResponse response) throws Exception {

        ModelAndView mv = new ModelAndView("demoRevistaDigital");
        return mv;
    }

    /**
     * *************************
     */
    @RequestMapping(value = "loadTabletInfo.htm", method = RequestMethod.GET)
    public ModelAndView loadTabletInfo(HttpServletRequest request, HttpServletResponse response, int idTableta) throws Exception {

        ModelAndView mv = new ModelAndView("infoTabletaPD");
        Gson gson = new Gson();

        List<TabletInformationPDDTO> tabletInformation = estatusGeneralBI.getTabletInfo(1, idTableta);
        List<TabletPerformancePDDTO> tabletPerformance = estatusGeneralBI.getTabletPerformance(2, idTableta);

        if (tabletInformation.isEmpty() && tabletPerformance.isEmpty()) {
            mv = new ModelAndView("redirect:estatusTableta.htm");
            logger.info("Pedestal Digital - LoginPDController - redirect:estatusTableta.htm");

            ArrayList<ParamNegocioPDDTO> list = estatusGeneralBI.getParamNegocios();
            for (ParamNegocioPDDTO paramNegocioPDDTO : list) {
                if (paramNegocioPDDTO.getFcreferencia().equals("General")) {
                    mv.addObject("generalNegVal", paramNegocioPDDTO.getFcvalor());
                } else if (paramNegocioPDDTO.getFcreferencia().equals("Propios")) {
                    mv.addObject("propiosNegVal", paramNegocioPDDTO.getFcvalor());
                } else if (paramNegocioPDDTO.getFcreferencia().equals("Terceros")) {
                    mv.addObject("tercerosNegVal", paramNegocioPDDTO.getFcvalor());
                }
            }

            mv.addObject("sucNotFound", "1");
            return mv;
        }

        String tfStr = gson.toJson(tabletInformation);
        String tpStr = gson.toJson(tabletPerformance);

        mv.addObject("tfStr", tfStr);
        mv.addObject("tpStr", tpStr);
        mv.addObject("idTableta", idTableta);

        return mv;
    }

    @RequestMapping(value = "loadSucursalInfo.htm", method = RequestMethod.GET)
    public ModelAndView loadSucursalInfo(HttpServletRequest request, HttpServletResponse response, int idTableta) throws Exception {

        ModelAndView mv = new ModelAndView("infoSucursalPD");
        Gson gson = new Gson();

        List<SucursalInfoPDDTO> sucursalIndo = estatusGeneralBI.getSucursalInfo(0, idTableta);
        List<SucursalTareasListPDDTO> sucursalTable = estatusGeneralBI.getSucursalTable(1, idTableta);

        String siStr = gson.toJson(sucursalIndo);
        String stStr = gson.toJson(sucursalTable);

        mv.addObject("siStr", siStr);
        mv.addObject("stStr", stStr);
        mv.addObject("idTableta", idTableta);

        // Obtiene parametro para mostrar u ocultar la pestaña de licencias de funcionamiento
        AdminParametroPDDTO obj = new AdminParametroPDDTO();
        obj.setDescParametro("getPermisoLicenciasFunc");
        List<AdminParametroPDDTO> resp = pdDAO.admonParametrosByDescRef(3, obj);
        mv.addObject("showTab", resp.get(0).getParametro());

        return mv;
    }

    @RequestMapping(value = "getIndicadoresEG.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<IndicadoresDataEGDTO> getIndicadoresEG(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "negocio", required = false) String negocio) throws Exception {

        List<IndicadoresDataEGDTO> indicadores = null;
        indicadores = estatusGeneralBI.getIndicadoresEG(0, negocio);

        return indicadores;
    }

    @RequestMapping(value = "getDatosGraficaEG.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<DatosTablaEGDTO> getDatosGraficaEG(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "anio", required = true) int anio,
            @RequestParam(value = "negocio", required = true) String negocio) throws Exception {

        int op = 1;
        List<DatosTablaEGDTO> datos = null;
        datos = estatusGeneralBI.getDatosGraficaEG(op, anio, negocio);

        return datos;
    }

    @RequestMapping(value = "getDataForExcelEG.json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean getDataForExcelEG(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "year", required = true) Integer year,
            @RequestParam(value = "negocio", required = true) String negocio) throws Exception {

        ServletOutputStream sos = null;
        List<TablaEstatusGeneralDTO> tabla = null;
        StringBuilder archivo = new StringBuilder();
        // String archivo = "";

        try {
            tabla = estatusGeneralBI.getDataForExcelEG(0, year.intValue(), negocio);

            archivo.append("<html><head><meta charset='UTF-8'></head><body>");
            archivo.append("<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' style='border-collapse: collapse;'>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Número de sucursal</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Nombre</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Número de serie</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Fecha de instalación</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Estatus de la tableta</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Estatus de la sucursal</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Última Actualización</th>"
                    + "</tr>"
                    + "</thead>"
                    + "<tbody>");

            for (TablaEstatusGeneralDTO row : tabla) {
                archivo.append("<tr>");

                if (row.getEstatus().equals("CORRECTO")) {
                    // #fdee7b
                    if (row.getActivo().equals("CERRADO")) {
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getNumEconomico() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getNombre() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getNumSerie() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getFechaInstalacion() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getEstatus() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getActivo() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getUltimaActual() + "</td>");
                    } else {
                        archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getNumEconomico() + "</td>");
                        archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getNombre() + "</td>");
                        archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getNumSerie() + "</td>");
                        archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getFechaInstalacion() + "</td>");
                        archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getEstatus() + "</td>");
                        archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getActivo() + "</td>");
                        archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getUltimaActual() + "</td>");
                    }
                } else if (row.getEstatus().equals("CON PROBLEMA")) {
                    if (row.getActivo().equals("CERRADO")) {
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getNumEconomico() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getNombre() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getNumSerie() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getFechaInstalacion() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getEstatus() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getActivo() + "</td>");
                        archivo.append("<td align='center' style='background-color: #fdee7b; color: #2f2f2f;'>" + row.getUltimaActual() + "</td>");
                    } else {
                        archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getNumEconomico() + "</td>");
                        archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getNombre() + "</td>");
                        archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getNumSerie() + "</td>");
                        archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getFechaInstalacion() + "</td>");
                        archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getEstatus() + "</td>");
                        archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getActivo() + "</td>");
                        archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getUltimaActual() + "</td>");
                    }
                } else if (row.getEstatus().equals("EN MANTENIMIENTO")) {
                    archivo.append("<td align='center' style='background-color: #cecece; color: #2f2f2f;'>" + row.getNumEconomico() + "</td>");
                    archivo.append("<td align='center' style='background-color: #cecece; color: #2f2f2f;'>" + row.getNombre() + "</td>");
                    archivo.append("<td align='center' style='background-color: #cecece; color: #2f2f2f;'>" + row.getNumSerie() + "</td>");
                    archivo.append("<td align='center' style='background-color: #cecece; color: #2f2f2f;'>" + row.getFechaInstalacion() + "</td>");
                    archivo.append("<td align='center' style='background-color: #cecece; color: #2f2f2f;'>" + row.getEstatus() + "</td>");
                    archivo.append("<td align='center' style='background-color: #cecece; color: #2f2f2f;'>" + row.getActivo() + "</td>");
                    archivo.append("<td align='center' style='background-color: #cecece; color: #2f2f2f;'>" + row.getUltimaActual() + "</td>");
                }

                archivo.append("</tr>");
            }

            archivo.append("</tbody></table>");
            archivo.append("</body></html>");

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            byte[] _data = archivo.toString().getBytes();
            response.setContentType("Content-type: application/vnd.ms-excel; charset='UTF-8';");
            response.setHeader("Content-Disposition", "attachment; filename=estatus_general_" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            response.setHeader("Content-Length", Integer.toString(_data.length));
            sos = response.getOutputStream();
            sos.write(archivo.toString().getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @RequestMapping(value = "downloadExcelST.json", method = RequestMethod.GET)
    public @ResponseBody
    Boolean downloadExcelST(HttpServletRequest request, HttpServletResponse response,
            @RequestParam int idTableta,
            @RequestParam String cecoInfo) throws Exception {

        ServletOutputStream sos = null;
        List<SucursalTareasListPDDTO> sucursal = null;

        String archivo = "";

        try {
            cecoInfo = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(cecoInfo));
            sucursal = estatusGeneralBI.getSucursalTable(1, idTableta);

            archivo = "<html><head><meta charset='UTF-8'></head><body>";
            archivo += "<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' style='border-collapse: collapse;'>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>"
                    + "Sucursal: " + cecoInfo
                    + "</th>"
                    + "</tr>"
                    + "<tr>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Folio de envío</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Nombre de documento</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Tipo de envío</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Estatus del documento</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Fecha de creación</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Fecha de recepción</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Fecha de visualización</th>"
                    + "</tr>"
                    + "</thead>"
                    + "<tbody>";

            if (sucursal.size() > 0) {
                for (SucursalTareasListPDDTO row : sucursal) {
                    archivo += "<tr>";
                    archivo += "<td align='center' style='background-color: #fff; color: #2f2f2f;'>" + row.getFolio() + "</td>";
                    archivo += "<td align='center' style='background-color: #fff; color: #2f2f2f;'>" + row.getNombreDocumento() + "</td>";
                    archivo += "<td align='center' style='background-color: #fff; color: #2f2f2f;'>" + row.getTipoEnvio() + "</td>";
                    archivo += "<td align='center' style='background-color: #fff; color: #2f2f2f;'>" + row.getEstatusDocumentoSucursal() + "</td>";
                    archivo += "<td align='center' style='background-color: #fff; color: #2f2f2f;'>" + row.getFechaCreacionDocumento() + "</td>";
                    archivo += "<td align='center' style='background-color: #fff; color: #2f2f2f;'>" + row.getFechaRecepcion() + "</td>";
                    archivo += "<td align='center' style='background-color: #fff; color: #2f2f2f;'>" + row.getFechaVisualizacionDocumento() + "</td>";
                    archivo += "</tr>";
                }
            }

            archivo += "</tbody>";
            archivo += "</table></body></html>";

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            response.setContentType("Content-type: application/vnd.ms-excel; charset='UTF-8';");
            response.setHeader("Content-Disposition", "attachment; filename=lista_archivos_sucursal_" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            sos = response.getOutputStream();
            sos.write(archivo.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Servicio para consultar tabla con los posibles estatus de tableta
    @RequestMapping(value = "getListaEstatusTableta.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaEstatusTabletaPDDTO> getListaEstatusTableta(HttpServletRequest request, HttpServletResponse response) throws Exception {

        int op = 0;
        List<ListaEstatusTabletaPDDTO> lista = null;
        lista = estatusGeneralBI.getListaEstatusTableta(op);

        return lista;
    }

    // Servicio para insertar los datos y estatus de la tableta
    @RequestMapping(value = "insertEstatusTableta.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertEstatusTableta(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("data") String data) throws Exception {

        int op = 0;
        String resp = null;
        // List<ListaEstatusTabletaPDDTO> listaEstatus = estatusGeneralBI.getListaEstatusTableta(op);
        List<ListaEstatusTabletaPDDTO> listaEstatus = new ArrayList<ListaEstatusTabletaPDDTO>();
        resp = estatusGeneralBI.insertEstatusTableta(op, listaEstatus, data);

        return resp;
    }

    // Servicio para insertar datos internos de la tableta que no construyen un historico; fabricante, modelo, etc
    @RequestMapping(value = "insertInternalDataTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertInternalDataTablet(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("numSerie") String numSerie,
            @RequestParam("fabricante") String fabricante,
            @RequestParam("modelo") String modelo,
            @RequestParam("sistemaOperativo") String sistemaOperativo,
            @RequestParam("versionApp") String versionApp,
            @RequestParam("totalEspacioAlmacenamiento") String totalEspacioAlmacenamiento,
            @RequestParam("ramTotal") String ramTotal,
            @RequestParam("espacioDisponible") String espacioDisponible,
            @RequestParam("ramDisponible") String ramDisponible) throws Exception {

        int op = 0;
        DataTabletPDDTO data = new DataTabletPDDTO(numSerie, fabricante, modelo, sistemaOperativo, versionApp, totalEspacioAlmacenamiento, ramTotal, espacioDisponible, ramDisponible);
        String resp = null;
        resp = estatusGeneralBI.insertInternalDataTablet(op, data);

        return resp;
    }

    // Servicio para insertar datos historicos de la tableta; salud de la bateria, temperatura, etc
    @RequestMapping(value = "insertHistoricDataTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertHistoricDataTablet(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("numSerie") String numSerie,
            @RequestParam("alimentacion") String alimentacion,
            @RequestParam("intensidadSenial") String intensidadSenial,
            @RequestParam("nivelBateria") String nivelBateria,
            @RequestParam("saludBateria") String saludBateria,
            @RequestParam("temperaturaBateria") String temperaturaBateria,
            @RequestParam("activo") String activo) throws Exception {

        int op = 1;
        DataTabletPDDTO data = new DataTabletPDDTO(numSerie, alimentacion, intensidadSenial, nivelBateria, saludBateria, temperaturaBateria, activo);
        String resp = null;
        resp = estatusGeneralBI.insertHistoricDataTablet(op, data);

        return resp;
    }

    // Servicio para obtener los datos de los indicadores de Estatus de Tabletas
    @RequestMapping(value = "getIndicadoresET.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<IndicadoresDataETDTO> getIndicadoresET(HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<IndicadoresDataETDTO> indicadores = null;
        indicadores = estatusGeneralBI.getIndicadoresET(1);

        return indicadores;
    }

    // Servicio para realizar búsqueda por sucursal; Filtro de Estatus de Tableta
    @RequestMapping(value = "getTabletBySucursal.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TabletDataFilterETDTO> getTabletBySucursal(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("numSucursal") int numSucursal) throws Exception {

        int op = 0;
        List<TabletDataFilterETDTO> data = null;
        data = estatusGeneralBI.getTabletBySucursal(op, numSucursal);

        return data;
    }

    // Servicio para realizar búsqueda por Territorio; Filtro de Estatus de Tableta
    @RequestMapping(value = "getTabletByTerritorio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TabletDataFilterETDTO> getTabletByTerritorio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("ceco") String ceco,
            @RequestParam("estatus") int estatus,
            @RequestParam("rendimiento") int rendimiento) throws Exception {

        int op = 1;
        List<TabletDataFilterETDTO> data = null;
        data = estatusGeneralBI.getTabletByTerritorio(op,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(ceco)),
                estatus, rendimiento);

        return data;
    }

    // Servicio para realizar búsqueda por País; Filtro de Estatus de Tableta
    @RequestMapping(value = "getTabletByPais.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TabletDataFilterETDTO> getTabletByPais(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("geo") String geo,
            @RequestParam("estatus") int estatus,
            @RequestParam("rendimiento") int rendimiento) throws Exception {

        int op = 2;
        List<TabletDataFilterETDTO> data = null;
        data = estatusGeneralBI.getTabletByPais(op,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(geo)),
                estatus, rendimiento);

        return data;
    }

    // Servicio para actualizar estatus de tableta, cuando se registre un error de tipo 3 (Critico)
    @RequestMapping(value = "insertCriticTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertCriticTablet(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("numSerie") String numSerie,
            @RequestParam("estatus") int estatus) throws Exception {

        int op = 1;
        String resp = null;
        resp = estatusGeneralBI.insertCriticTablet(op, numSerie, estatus);
        return resp;
    }

    // Servicio para exportar datos y descargar la informacion en Excel
    @RequestMapping(value = "exportFilterDataToExcel.json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean exportFilterDataToExcel(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("json") String json) throws Exception {

        String _json = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(json));
        JsonObject jsonObject = new JsonParser().parse(_json).getAsJsonObject();
        List<TabletDataFilterETDTO> data = null;

        if (jsonObject.get("tipoFiltro").getAsString().equals("S")) {
            data = estatusGeneralBI.getTabletBySucursal(0, jsonObject.get("numSucursal").getAsInt());
        } else if (jsonObject.get("tipoFiltro").getAsString().equals("T")) {
            data = estatusGeneralBI.getTabletByTerritorio(1, jsonObject.get("ceco").getAsString(), jsonObject.get("estatus").getAsInt(), jsonObject.get("rendimiento").getAsInt());
        } else if (jsonObject.get("tipoFiltro").getAsString().equals("P")) {
            data = estatusGeneralBI.getTabletByPais(2, jsonObject.get("geo").getAsString(), jsonObject.get("estatus").getAsInt(), jsonObject.get("rendimiento").getAsInt());
        }

        // Código para generar Excel de filtro de tableta con datos de PROD
        // Ejemplo: http://10.53.33.83/franquicia/central/pedestalDigital/getTabletByPais.json?geo=1&estatus=0&rendimiento=0
        /**
         * Type listType = new
         * TypeToken<ArrayList<TabletDataFilterETDTO>>(){}.getType(); Gson gson
         * = new Gson(); String _json = "AQUI VA JSON DE PROD"; data =
         * gson.fromJson(_json, listType);
         */
        StringBuilder archivo = new StringBuilder();
        ServletOutputStream sos = null;

        try {
            archivo.append("<html><head><meta charset='UTF-8'></head><body>");
            archivo.append("<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' style='border-collapse: collapse;'>"
                    + "<thead>"
                    + "<tr>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Número económico</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Sucursal</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Fecha de instalación</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Última actualización</th>"
                    + "<th align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>Estatus de la tableta</th>"
                    + "</tr>"
                    + "</thead>"
                    + "<tbody>");

            for (TabletDataFilterETDTO row : data) {
                if (row.getIdEstatus() == 1) {
                    archivo.append("<tr>");
                    archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getSucursal() + "</td>");
                    archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getNombre() + "</td>");
                    archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getFechaInstalacion() + "</td>");
                    archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getUltimaActualizacion() + "</td>");
                    archivo.append("<td align='center' style='background-color: #b2cdb2; color: #2f2f2f;'>" + row.getDescIdEstatus() + "</td>");
                    archivo.append("</tr>");
                } else if (row.getIdEstatus() == 2) {
                    archivo.append("<tr>");
                    archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getSucursal() + "</td>");
                    archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getNombre() + "</td>");
                    archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getFechaInstalacion() + "</td>");
                    archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getUltimaActualizacion() + "</td>");
                    archivo.append("<td align='center' style='background-color: #e0858d; color: #2f2f2f;'>" + row.getDescIdEstatus() + "</td>");
                    archivo.append("</tr>");
                }
            }

            archivo.append("</tbody></table>");
            archivo.append("</body></html>");

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            byte[] _data = archivo.toString().getBytes();
            response.setContentType("Content-type: application/vnd.ms-excel; charset='UTF-8';");
            response.setHeader("Content-Disposition", "attachment; filename=estatus_general_" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            response.setContentLength(_data.length);
            sos = response.getOutputStream();
            sos.write(_data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Servicio para exportar tabla de folios y convertir en Excel
    @RequestMapping(value = "exportTableFolioExcel.json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean exportTableFolioExcel(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("json") String json) throws Exception {

        String _json = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(json));
        JsonObject jsonObject = new JsonParser().parse(_json).getAsJsonObject();
        List<FolioFilterDataPDDTO> data = null;

        if (jsonObject.get("tipoFiltro").getAsString().equals("T")) {
            data = cargaArchivos.getFolioDataPorTerritorio(0, jsonObject.get("ceco").getAsString(), jsonObject.get("categoria").getAsString(), jsonObject.get("fecha").getAsString(), jsonObject.get("estatus").getAsString(), jsonObject.get("tipo").getAsString());
        } else if (jsonObject.get("tipoFiltro").getAsString().equals("G")) {
            data = cargaArchivos.getFolioDataPorGeografia(0, jsonObject.get("geo").getAsString(), jsonObject.get("nivel").getAsInt(), jsonObject.get("categoria").getAsString(), jsonObject.get("fecha").getAsString(), jsonObject.get("estatus").getAsString(), jsonObject.get("tipo").getAsString());
        }

        StringBuilder archivo = new StringBuilder();
        ServletOutputStream sos = null;

        try {
            archivo.append("<html><head><meta charset='UTF-8'></head><body>");
            archivo.append("<table class='tblGeneral'>");
            archivo.append("<thead>");
            archivo.append("<tr>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Folio de envío</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Tipo de envío</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Fecha / Hora de creación de folio</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Sucursales afectadas</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Estatus de envío</th>");
            archivo.append("</tr>");
            archivo.append("</thead>");
            archivo.append("<tbody>");

            for (FolioFilterDataPDDTO row : data) {
                archivo.append("<tr>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + row.getIdFolio() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + row.getTipoEnvio() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + row.getFechaEnvio() + " Hrs</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + row.getSucursalesAfectadas() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + row.getEstatus() + "</td>");
                archivo.append("</tr>");
            }

            archivo.append("</tbody></table>");
            archivo.append("</body></html>");

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            byte[] _data = archivo.toString().getBytes();
            response.setContentType("Content-type: application/vnd.ms-excel; charset='UTF-8';");
            response.setHeader("Content-Disposition", "attachment; filename=estatus_general_" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            response.setHeader("Content-Length", Integer.toString(_data.length));
            sos = response.getOutputStream();
            sos.write(_data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Servicio para obtener informacion de la tableta (Vista: Informacion de la Tableta)
    @RequestMapping(value = "getTabletInfo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TabletInformationPDDTO> getTabletInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTableta") int idTableta) throws Exception {

        int op = 1;
        List<TabletInformationPDDTO> resp = null;
        resp = estatusGeneralBI.getTabletInfo(op, idTableta);
        return resp;
    }

    // Servicio para obtener el rendimiento de la tableta (Vista: Informacion de la Tableta)
    @RequestMapping(value = "getTabletPerformance.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TabletPerformancePDDTO> getTabletPerformance(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTableta") int idTableta) throws Exception {

        int op = 2;
        List<TabletPerformancePDDTO> resp = null;
        resp = estatusGeneralBI.getTabletPerformance(op, idTableta);
        return resp;
    }

    // Servicio para obtener indicadores de de vista Información de sucursal
    @RequestMapping(value = "getSucursalInfo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<SucursalInfoPDDTO> getSucursalInfo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTableta") int idTableta) throws Exception {

        int op = 0;
        List<SucursalInfoPDDTO> resp = null;
        resp = estatusGeneralBI.getSucursalInfo(op, idTableta);
        return resp;
    }

    // Servicio para obtener tabla de 'Lista de archivos cargados' para vista Información de sucursal
    @RequestMapping(value = "getSucursalTable.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<SucursalTareasListPDDTO> getSucursalTable(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTableta") int idTableta) throws Exception {

        int op = 1;
        List<SucursalTareasListPDDTO> resp = null;
        resp = estatusGeneralBI.getSucursalTable(op, idTableta);
        return resp;
    }

    // Servicio para obtener todos los documentos de una tableta
    @RequestMapping(value = "getTasksInTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TaskInTabletPDDAO> getTasksInTablet(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("sucursal") int sucursal) throws Exception {

        int op = 0;
        List<TaskInTabletPDDAO> resp = null;
        resp = estatusGeneralBI.getTasksInTablet(op, sucursal, 0); // se comenta negocio momentaneamente
        return resp;
    }

    // Servicio que te devuelve la versión mas reciente de un documento en una tableta
    @RequestMapping(value = "getLatestTaskInTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TaskInTabletPDDAO> getLatestTaskInTablet(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("sucursal") int sucursal,
            @RequestParam("tipoDocumento") int tipoDocumento) throws Exception {

        int op = 1;
        List<TaskInTabletPDDAO> resp = null;
        resp = estatusGeneralBI.getLatestTaskInTablet(op, sucursal, tipoDocumento);
        return resp;
    }

    // Servicio para actualizar el estatus de un documento en una tableta
    @RequestMapping(value = "updateStatusOfTaskInTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateStatusOfTaskInTablet(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("sucursal") int sucursal,
            @RequestParam("negocio") int negocio,
            @RequestParam("tipoDocumento") int tipoDocumento,
            @RequestParam("idDocumento") int idDocumento,
            @RequestParam("idDocPedestal") int idDocPedestal,
            @RequestParam("idEstatus") int idEstatus,
            @RequestParam("activo") int activo) throws Exception {

        int op = 3;
        String resp = null;
        resp = estatusGeneralBI.updateStatusOfTaskInTablet(op, sucursal, negocio, tipoDocumento, idDocumento, idDocPedestal, idEstatus, activo);
        return resp;
    }

    // updateStatusOfTaskInTablet2
    // Servicio para actualizar el estatus de un documento en una tableta
    @RequestMapping(value = "updateStatusOfTaskInTablet2.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateStatusOfTaskInTablet2(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("estatus") int estatus,
            @RequestParam("idDocPedestal") int idDocPedestal) throws Exception {

        int op = 2;
        String resp = null;
        resp = estatusGeneralBI.updateStatusOfTaskInTablet2(op, estatus, idDocPedestal);
        return resp;
    }

    // Servicio para obtener los documentos y el detalle de un folio
    @RequestMapping(value = "getFolioDetail.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<FolioDetailPDDTO> getFolioDetail(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idFolio") int idFolio) throws Exception {

        int op = 1;
        List<FolioDetailPDDTO> resp = null;
        resp = estatusGeneralBI.getFolioDetail(op, idFolio);
        return resp;
    }

    // Servicio para exportar en un excel el detalle de un folio
    @RequestMapping(value = "exportFolioDetailExcel.json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean exportFolioDetailExcel(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idFolio") int idFolio) throws Exception {

        StringBuilder archivo = new StringBuilder();
        ServletOutputStream sos = null;
        List<FolioDetailPDDTO> data = estatusGeneralBI.getFolioDetail(1, idFolio);

        try {
            archivo.append("<html><head><meta charset='UTF-8'></head><body>");
            archivo.append("<table class='tblGeneral'>");
            archivo.append("<thead>");
            archivo.append("<tr>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Nombre del documento</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Categoría</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>% de Avance</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Visible en sucursal</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Fecha de visualización</th>");
            archivo.append("</tr>");
            archivo.append("</thead>");
            archivo.append("<tbody>");

            for (FolioDetailPDDTO row : data) {
                int totalSucursales = row.getTotalSucursal();
                int a = row.getDocVisSuc();
                float c = (a / totalSucursales) * 100;

                DecimalFormat df = new DecimalFormat();
                df.setMaximumFractionDigits(2);

                archivo.append("<tr>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + row.getNombreDocumento() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + row.getCategoriaDesc() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + df.format(c) + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + row.getVisibleSucursalDesc() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + row.getFechaVisualizacion() + " Hrs</td>");
                archivo.append("</tr>");
            }

            archivo.append("</tbody></table>");
            archivo.append("</body></html>");

            byte[] _data = archivo.toString().getBytes();
            response.setContentType("Content-type: application/vnd.ms-excel; charset='UTF-8';");
            response.setHeader("Content-Disposition", "attachment; filename=detalle_folio_" + idFolio + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            response.setContentLength(_data.length);
            sos = response.getOutputStream();
            sos.write(_data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * *********************************
     */
    /**
     * SERVICIOS PARA MODIFICAR FOLIO *
     */
    /**
     * *********************************
     */
    // Servicio para actualizar la fecha de visualización de un folio
    @RequestMapping(value = "updateFechaVisualizacionFolio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateFechaVisualizacionFolio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idFolio") int idFolio,
            @RequestParam("fecha") String fecha) throws Exception {

        int op = 0;
        String resp = estatusGeneralBI.updateFechaVisualizacionFolio(op, idFolio,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fecha)));
        return resp;
    }

    // Servicio que agregar sucursales a un folio
    @RequestMapping(value = "addSucursalesFolio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<ListaCecoPorCSVPDDTO> addSucursalesFolio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idFolio") int idFolio,
            @RequestParam("cecos") String cecos) throws Exception {

        int op = 1;
        List<ListaCecoPorCSVPDDTO> resp = estatusGeneralBI.addSucursalesFolio(op, idFolio,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(cecos)));
        return resp;
    }

    // Servicio para sustituir documento
    @RequestMapping(value = "changeDocumentFolio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<String> changeDocumentFolio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("documentoNuevo") int documentoNuevo,
            @RequestParam("documentoAnterior") int documentoAnterior) throws Exception {

        int op = 2;
        List<String> resp = estatusGeneralBI.changeDocumentFolio(op, documentoNuevo, documentoAnterior);
        return resp;
    }

    // Servicio para validar si el documento que va a ser sustituido, tiene lanzamientos
    @RequestMapping(value = "validaDocumentoDeFolio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String validaDocumentoDeFolio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idDocumentoAnt") int idDocumentoAnt) throws Exception {

        int op = 3;
        String resp = estatusGeneralBI.validaDocumentoDeFolio(op, idDocumentoAnt);
        return resp;
    }

    // TOOD: Servicio para inactivar los lanzamientos de un documento en un folio
    @RequestMapping(value = "inactivaLanzamientosDocFolio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String inactivaLanzamientosDocFolio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idFolio") int idFolio,
            @RequestParam("idDocs") String idDocs) throws Exception {

        String resp = null;
        JsonObject js = null;
        JsonElement je = null;
        Gson gson = new Gson();

        // Inactivar folio
        resp = cargaArchivos.updateFolio(1, idFolio, null, null, 1);
        je = gson.fromJson(resp, JsonElement.class);
        js = je.getAsJsonObject();

        if (js.get("respuesta").getAsString().equalsIgnoreCase("0")) {
            // TODO: Inactivar documento
            // Inactivar Lanzamientos del documento
            String[] array = idDocs.split(",");

            if (array.length > 0) {
                for (String string : array) {

                    resp = cargaArchivos.updateDocto(1, Integer.parseInt(string), idFolio, null, 0, 0, 0, null, null, null, null, 0, 0);
                    je = gson.fromJson(resp, JsonElement.class);
                    js = je.getAsJsonObject();

                    if (js.get("respuesta").getAsString().equalsIgnoreCase("0")) {
                        resp = estatusGeneralBI.inactivaLanzamientosDocFolio(3, Integer.parseInt(string), 0);
                    } else {
                        // ????????????
                    }
                }
            } else {
                return "{\n\t\"respuesta\":\"1\",\n\t\"msj\":\"0\"\n}";
            }
        } else {
            return "";
        }

        return resp;
    }

    // TODO: Servicio que maneja todo el flujo para sustituir un documento de un folio.
    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "sustituyeDocumentoFolio.htm", method = RequestMethod.POST)
    public ResponseEntity sustituyeDocumentoFolio(MultipartHttpServletRequest request, HttpServletResponse response, int idFolio, String jsonData) throws NumberFormatException, IOException, Exception {

        Gson gson = new Gson();
        LoginPDDTO usuario = (LoginPDDTO) request.getSession().getAttribute("userBean");

        if (usuario == null) {
            return new ResponseEntity<>("{\t\n\"idFolio\":\"" + "0" + "\"\n}", HttpStatus.OK);
        }

        // TODO: Validar si el negocio de obtiene desde FRONT o desde el LOGIN
        String negocio = "41"; // (String) request.getSession().getAttribute("idNegocio");
        String categoria = "";
        String newFileName = "";

        // TODO: Obtener datos del documento (fileType, fileSize, etc)
        // String docData = (String) request.getSession().getAttribute("docData");
        DocData[] docDataArr = gson.fromJson(jsonData, DocData[].class);
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String strDate = dateFormat.format(new Date());
        String _insDate = dateFormat.format(new Date());

        String _resp = estatusGeneralBI.validaDocumentoDeFolio(3, idFolio);
        JsonObject jsonObj = gson.fromJson(jsonData, JsonObject.class);

        if (jsonObj.get("lanzamientos").getAsInt() > 0) {
            // Se realiza el proceso de carga del archivo
            try {
                Iterator<String> itr = request.getFileNames();

                int documentoNuevo = 0; // Obtener ID del nuevo documento (despues de haber sido insertado en base)
                // TODO: Insertar informacion del documento

                while (itr.hasNext()) {
                    String uploadedFile = itr.next();
                    MultipartFile file = request.getFile(uploadedFile);
                    String filename = file.getOriginalFilename();

                    for (int x = 0; x < docDataArr.length; x++) {
                        int docId = docDataArr[x].getDocumentId();

                        if (filename.equalsIgnoreCase(docDataArr[x].getOgFileName())) {
                            newFileName = idFolio + "_" + docId + "_" + _insDate + "." + docDataArr[x].getFileType();
                            List<ListaCategoriasPDDTO> listaCategorias = cargaArchivos.obtieneCategorias(2, 1, 0);

                            for (ListaCategoriasPDDTO listaCategoriasPDDTO : listaCategorias) {
                                if (docDataArr[x].getIdCategory().equalsIgnoreCase(Integer.toString(listaCategoriasPDDTO.getIdTipoDoc()))) {
                                    categoria = listaCategoriasPDDTO.getNombreDoc() + "/";
                                    break;
                                }
                            }
                        }
                    }

                    // TODO: aaaaaaa
                    List<String> resp = estatusGeneralBI.changeDocumentFolio(2, documentoNuevo, docDataArr[0].getDocumentId());

                    logger.info("Path: " + path + categoria + newFileName);
                    FileUtils.writeByteArrayToFile(new File(path + categoria, newFileName), file.getBytes());
                }
            } catch (Exception e) {
                return new ResponseEntity<>("{\t\n\"idFolio\":\"0\"\n}", HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>("{\t\n\"idFolio\":\"0\"\n}", HttpStatus.OK);
        }

        return new ResponseEntity<>("{\t\n\"idFolio\":\"0\"\n}", HttpStatus.OK);
    }

    /**
     * ***********************
     */
    /**
     * quejas y comentarios *
     */
    /**
     * ***********************
     */
    // Servicio para obtener dudas y comentarios
    @RequestMapping(value = "getQuejas.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<QuejasPDDTO> getQuejas(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idComentario") int idComentario) throws Exception {

        int op = 2;
        List<QuejasPDDTO> resp = estatusGeneralBI.getQuejas(op, idComentario);
        return resp;
    }

    // Servicio para obtener dudas y comentarios en formato Excel
    @RequestMapping(value = "exportQuejas.json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean exportQuejas(HttpServletRequest request, HttpServletResponse response) throws Exception {

        StringBuilder archivo = new StringBuilder();
        ServletOutputStream sos = null;
        List<QuejasPDDTO> lista = estatusGeneralBI.getQuejas(2, 0);

        try {
            archivo.append("<html><head><meta charset='UTF-8'></head><body>");
            archivo.append("<table class='tblGeneral'>");
            archivo.append("<thead>");
            archivo.append("<tr>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Sucursal</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Cliente</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Descripción</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Comentario</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Teléfono</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Email</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Fecha</th>");
            archivo.append("</tr>");
            archivo.append("</thead>");
            archivo.append("<tbody>");

            for (QuejasPDDTO queja : lista) {
                archivo.append("<tr>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + queja.getDescCeco() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + queja.getNombreCliente() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + queja.getDesc() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + queja.getComentario() + "</td>");

                if (queja.getIdContacto() == 1) {
                    archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + queja.getTelefono() + "</td>");
                    archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + queja.getEmail() + "</td>");
                } else {
                    archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>-</td>");
                    archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>-</td>");
                }

                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + queja.getFecha() + "</td>");
                archivo.append("</tr>");
            }

            archivo.append("</tbody></table>");
            archivo.append("</body></html>");

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            byte[] _data = archivo.toString().getBytes();
            response.setContentType("Content-type: application/vnd.ms-excel; charset='UTF-8';");
            response.setHeader("Content-Disposition", "attachment; filename=listado_quejas_" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            response.setContentLength(_data.length);
            sos = response.getOutputStream();
            sos.write(_data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Servicio para insertar una queja
    @RequestMapping(value = "insertQueja.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertQueja(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("data") String data) throws Exception {

        int op = 0;
        String resp = estatusGeneralBI.insertQueja(op, ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(data)));
        return resp;
    }

    /**
     * **********************************************
     */
    /**
     * SERVICIOS DE REPORTERIA GENERAL Y DETALLADA *
     */
    /**
     * **********************************************
     */
    // Servicio para obtener resultados por filtro de reportería general con cecos
    @RequestMapping(value = "filtroRepGralCec.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<RepGralCecoPDDTO> filtroRepGralCec(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fechaIni") String fechaIni,
            @RequestParam("fechaEnv") String fechaEnv,
            @RequestParam("doctoVis") String doctoVis,
            @RequestParam("categoria") String categoria,
            @RequestParam("ceco") String ceco) throws Exception {

        int op = 0;
        List<RepGralCecoPDDTO> resp = estatusGeneralBI.filtroRepGralCec(op,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fechaIni)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fechaEnv)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(doctoVis)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(categoria)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(ceco)));
        return resp;
    }

    // Servicio para obtener resultados por filtro de reportería general con geo
    @RequestMapping(value = "filtroRepGralGeo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<RepGralCecoPDDTO> filtroRepGralGeo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fechaIni") String fechaIni,
            @RequestParam("fechaEnv") String fechaEnv,
            @RequestParam("doctoVis") String doctoVis,
            @RequestParam("categoria") String categoria,
            @RequestParam("geo") String geo) throws Exception {

        int op = 0;
        List<RepGralCecoPDDTO> resp = estatusGeneralBI.filtroRepGralGeo(op,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fechaIni)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fechaEnv)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(doctoVis)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(categoria)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(geo)));
        return resp;
    }

    // Servicio para exportar reporte general en un archivo de Excel
    @RequestMapping(value = "exportTablaReporteria.json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean exportTablaReporteria(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("json") String data) throws Exception {

        Gson gson = new Gson();
        data = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(data)); // ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(data));
        JsonElement je = gson.fromJson(data, JsonElement.class);
        JsonObject js = je.getAsJsonObject();
        List<RepGralCecoPDDTO> resp = null;

        if (data.length() < 1) {
            return null;
        }

        if (js.get("tipo").getAsString().equals("1")) {
            // reporte general por ceco
            resp = estatusGeneralBI.filtroRepGralCec(0,
                    js.get("fechaIni").getAsString(),
                    js.get("fechaEnv").getAsString(),
                    js.get("doctoVis").getAsString(),
                    js.get("categoria").getAsString(),
                    js.get("ceco").getAsString());
        } else if (js.get("tipo").getAsString().equals("2")) {
            // reporte general por geo
            resp = estatusGeneralBI.filtroRepGralGeo(0,
                    js.get("fechaIni").getAsString(),
                    js.get("fechaEnv").getAsString(),
                    js.get("doctoVis").getAsString(),
                    js.get("categoria").getAsString(),
                    js.get("geo").getAsString());
        }

        String archivo = "";
        StringBuffer arch = new StringBuffer();
        ServletOutputStream sos = null;

        try {
            arch.append("<html><head><meta charset='UTF-8'></head><body>");
            arch.append("<table class='tblGeneral'>");
            arch.append("<thead>");
            arch.append("<tr>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Nombre del documento</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Categoria</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Visible en sucursal</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Estatus</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Fecha envío</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;'>Fecha visualización</th>");
            arch.append("</tr>");
            arch.append("</thead>");
            arch.append("<tbody>");

            for (RepGralCecoPDDTO obj : resp) {
                arch.append("<tr>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getNombreDoc() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getDescCategoria() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getVisibleSuc() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getDescEstatus() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getFechaEnv() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getVigenciaFin() + "</td>");
                arch.append("</tr>");
            }

            arch.append("</tbody></table>");
            arch.append("</body></html>");

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            byte[] _data = arch.toString().getBytes();
            response.setContentType("Content-type: application/vnd.ms-excel; charset='UTF-8';");
            response.setHeader("Content-Disposition", "attachment; filename=reporte_general_" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            response.setContentLength(_data.length);
            sos = response.getOutputStream();
            sos.write(_data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Servicio para obtener resultados por filtro de reportería detallado con cecos
    @RequestMapping(value = "filtroRepDetaCec.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<RepDetaCecoPDDTO> filtroRepDetaCec(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fechaIni") String fechaIni,
            @RequestParam("fechaEnv") String fechaEnv,
            @RequestParam("doctoVis") String doctoVis,
            @RequestParam("tipoEnvio") String tipoEnvio,
            @RequestParam("categoria") String categoria,
            @RequestParam("ceco") String ceco) throws Exception {

        int op = 0;
        List<RepDetaCecoPDDTO> resp = estatusGeneralBI.filtroRepDetaCec(op,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fechaIni)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fechaEnv)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(doctoVis)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(tipoEnvio)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(categoria)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(ceco)));
        return resp;
    }

    // Servicio para obtener resultados por filtro de reportería detallado con geo
    @RequestMapping(value = "filtroRepDetaGeo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<RepDetaCecoPDDTO> filtroRepDetaGeo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fechaIni") String fechaIni,
            @RequestParam("fechaEnv") String fechaEnv,
            @RequestParam("doctoVis") String doctoVis,
            @RequestParam("tipoEnvio") String tipoEnvio,
            @RequestParam("categoria") String categoria,
            @RequestParam("geo") String geo) throws Exception {

        int op = 0;
        List<RepDetaCecoPDDTO> resp = estatusGeneralBI.filtroRepDetaGeo(op,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fechaIni)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(fechaEnv)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(doctoVis)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(tipoEnvio)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(categoria)),
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(geo)));
        return resp;
    }

    // Servicio para obtener resultados por filtro de reportería detallado con numero de folio
    @RequestMapping(value = "filtroRepDetaIdFolio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<RepDetaCecoPDDTO> filtroRepDetaIdFolio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idFolio") int idFolio) throws Exception {

        int op = 1;
        List<RepDetaCecoPDDTO> resp = estatusGeneralBI.filtroRepDetaIdFolio(op, idFolio);
        return resp;
    }

    // Servicio para exportar archivo de excel con los resultados del filtro de reporteria detallada
    @RequestMapping(value = "exportReporteriaDetallada.json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean exportReporteriaDetallada(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("data") String data) throws Exception {

        Gson gson = new Gson();
        data = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(data));
        JsonElement je = gson.fromJson(data, JsonElement.class);
        JsonObject js = je.getAsJsonObject();
        List<RepDetaCecoPDDTO> resp = null;

        if (data.length() < 1) {
            return null;
        }

        if (js.get("tipo").getAsString().equals("1")) {
            // reporte detallado por ceco
            resp = estatusGeneralBI.filtroRepDetaCec(0,
                    js.get("fechaIni").getAsString(),
                    js.get("fechaEnv").getAsString(),
                    js.get("doctoVis").getAsString(),
                    js.get("tipoEnvio").getAsString(),
                    js.get("categoria").getAsString(),
                    js.get("ceco").getAsString());
        } else if (js.get("tipo").getAsString().equals("2")) {
            // reporte detallado por geo
            resp = estatusGeneralBI.filtroRepDetaGeo(0,
                    js.get("fechaIni").getAsString(),
                    js.get("fechaEnv").getAsString(),
                    js.get("doctoVis").getAsString(),
                    js.get("tipoEnvio").getAsString(),
                    js.get("categoria").getAsString(),
                    js.get("geo").getAsString());
        } else if (js.get("tipo").getAsString().equals("3")) {
            // reporte detallado por numero de folio
            resp = estatusGeneralBI.filtroRepDetaIdFolio(1, js.get("idFolio").getAsInt());
        }

        // String archivo = "";
        StringBuffer arch = new StringBuffer();
        ServletOutputStream sos = null;

        try {
            arch.append("<html><head><meta charset='UTF-8'></head><body>");
            arch.append("<table class='tblGeneral'>");
            arch.append("<thead>");
            arch.append("<tr>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Territorio</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Zona</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Regi&oacute;n</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Pa&iacute;s</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Estado</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Municipio</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Nombre (N&uacute;mero de Sucursal)</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Categor&iacute;a</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Nombre del documento</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Estatus documento</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Fecha de env&iacute;o</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Fecha de visualizaci&oacute;n</th>");
            arch.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Vigencia final</th>");
            arch.append("</tr>");
            arch.append("</thead>");
            arch.append("<tbody>");

            for (RepDetaCecoPDDTO obj : resp) {
                arch.append("<tr>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getTerritorio() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getZona() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getRegion() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getPais() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getEstado() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getMunicipio() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getNombre() + "(" + obj.getIdSucursal() + ") </td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getDescCategoria() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getNombreDoc() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getDescVisibleSuc() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getFechaEnv() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getVigenciaIni() + "</td>");
                arch.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getVigenciaFin() + "</td>");
                arch.append("</tr>");
            }

            arch.append("</tbody></table>");
            arch.append("</body></html>");

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "").replace("/", "");

            byte[] _data = arch.toString().getBytes();
            response.setContentType("Content-type: application/vnd.ms-excel; charset='UTF-8';");
            response.setHeader("Content-Disposition", "attachment; filename=reporte_detallado_" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            response.setContentLength(_data.length);
            sos = response.getOutputStream();
            sos.write(_data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * *************************************************************************
     */
    /**
     * SERVICIOS PARA INSERTAR, ACTUALIZAR Y OBTENER INFORMACION DE GEOGRAFIA *
     */
    /**
     * *************************************************************************
     */
    // Servicio para insertar informaion de geografia
    @RequestMapping(value = "insertGeo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertGeo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idGeo") int idGeo,
            @RequestParam("tipo") int tipo,
            @RequestParam("idcc") int idcc,
            @RequestParam("desc") String desc,
            @RequestParam("dependeDe") int dependeDe,
            @RequestParam("activo") int activo) throws Exception {

        int op = 0;
        String resp = estatusGeneralBI.insertGeo(op, idGeo, tipo, idcc,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(desc)),
                dependeDe, activo);
        return resp;
    }

    // Servicio para actualizar informaion de geografia
    @RequestMapping(value = "updateGeo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateGeo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idGeo") int idGeo,
            @RequestParam("tipo") int tipo,
            @RequestParam("idcc") int idcc,
            @RequestParam("desc") String desc,
            @RequestParam("dependeDe") int dependeDe,
            @RequestParam("activo") int activo) throws Exception {

        int op = 1;
        String resp = estatusGeneralBI.updateGeo(op, idGeo, tipo, idcc,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(desc)),
                dependeDe, activo);
        return resp;
    }

    // Servicio para obtener informaion de geografia
    @RequestMapping(value = "getGeo.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<GeoDataPDDTO> getGeo(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idGeo") int idGeo,
            @RequestParam("tipo") int tipo,
            @RequestParam("dependeDe") int dependeDe) throws Exception {

        int op = 2;
        List<GeoDataPDDTO> resp = estatusGeneralBI.getGeo(op, idGeo, tipo, dependeDe);
        return resp;
    }

    /**
     * ************************************************************
     */
    /**
     * SERVICIOS PARA INSERTAR / ACTUALIZAR INFORMACION DE CECOS *
     */
    /**
     * ************************************************************
     */
    // Servicio que maneja todo el flujo de insercion de información de cecos
    // TODO: Validar como se obtiene la información del archivo Excel
    // TODO: Convertir data (json?) a ArrayList de objetos de Java
    // TODO: Iterar lista de objetos para ir insertando la informacion de cecos
    @RequestMapping(value = "insertCecoData.json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String insertCecoData(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("data") String data) throws Exception {

        String resp = estatusGeneralBI.insertCecoData(0,
                data // ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(data))
        );

        return resp;
    }

    // Servicio que actualiza el idGeo de un ceco
    @RequestMapping(value = "updateGeoInCeco.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateGeoInCeco(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idCeco") String idCeco,
            @RequestParam("idPais") int idPais,
            @RequestParam("idEstado") int idEstado,
            @RequestParam("idMunicipio") int idMunicipio) throws Exception {

        String resp = estatusGeneralBI.updateGeoInCeco(1, idCeco, idPais, idEstado, idMunicipio);
        return resp;
    }

    /**
     * ****************************************************************
     */
    /**
     * SERVICIOS PARA INSERTAR / ACTUALIZAR INFORMACION DE GEOGRAFIA *
     */
    /**
     * ****************************************************************
     */
    // Servicio que maneja el flujo para la inserción y actualización de datos de la tabla de geografía
    @RequestMapping(value = "updateGeoData.json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String updateGeoData(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("data") String data) throws Exception {

        String resp = estatusGeneralBI.updateGeoData(0, data);
        return resp;
    }

    // Servicio que devuelve un ceco por FIID_GEOGRAFIA
    // Sirve para validar que un registro ya se encuentra insertado en la tabla
    // Si ya se encuentra el registro en tabla, entonces se actualiza, en vez de insertarse.
    @RequestMapping(value = "getGeoDataById.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<GeoDataPDDTO> getGeoDataById(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idGeo") int idGeo) throws Exception {

        List<GeoDataPDDTO> resp = estatusGeneralBI.getGeoDataById(2, idGeo);
        return resp;
    }

    // TODO: Servicio que devuelve los documentos Visibles y No Visibles
    /**
     * ********************************
     */
    // Servicio que inserta un tipo de queja en el catalogo de quejas
    @RequestMapping(value = "insertTipoQueja.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertTipoQueja(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idComent") int idComent,
            @RequestParam("coment") String coment,
            @RequestParam("activo") int activo) throws Exception {

        String resp = estatusGeneralBI.insertTipoQueja(3, idComent, coment, activo);
        return resp;
    }

    // Servicio que actualiza un registro en el catalogo de tipos de quejas
    @RequestMapping(value = "updateTipoQueja.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateTipoQueja(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idComent") int idComent,
            @RequestParam("coment") String coment,
            @RequestParam("activo") int activo) throws Exception {

        String resp = estatusGeneralBI.updateTipoQueja(4, idComent, coment, activo);
        return resp;
    }

    // Servicio que obtiene el catalogo de tipos de quejas
    @RequestMapping(value = "getTipoQueja.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TipoQuejaPDDTO> getTipoQueja(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTipoCat") int idTipoCat) throws Exception {

        List<TipoQuejaPDDTO> resp = estatusGeneralBI.getTipoQueja(5, idTipoCat);
        return resp;
    }

    // Servicio que devuelve parametro por id
    @RequestMapping(value = "getParametroPD.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AdminParametroPDDTO> getParametroPD(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idParametro") int idParametro) throws Exception {

        AdminParametroPDDTO obj = new AdminParametroPDDTO();
        obj.setIdParametro(idParametro);
        List<AdminParametroPDDTO> resp = pdDAO.admonParametros(obj, 2);
        return resp;
    }

    // Servicio que devuelve parametro por descripcion o referencia
    @RequestMapping(value = "getParametroByDesc.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AdminParametroPDDTO> getParametroByDesc(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "desc", required = false) String desc,
            @RequestParam(value = "ref", required = false) String ref) throws Exception {

        AdminParametroPDDTO obj = new AdminParametroPDDTO();
        obj.setDescParametro(desc);
        obj.setReferencia(ref);

        List<AdminParametroPDDTO> resp = pdDAO.admonParametrosByDescRef(3, obj);
        return resp;
    }

    /**
     * ***************************************************************************
     */
    /**
     * SERVICIOS PARA CONSULTA Y ACTUALIZACION DE ESTATUS DE DESCARGAS (TABLETA)
     * *
     */
    /**
     * ***************************************************************************
     */
    // Servicio para consultar los parametros de una tableta
    @RequestMapping(value = "getParamTabletBatch.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getParamTabletBatch(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("numSerie") String numSerie,
            @RequestParam("tipo") String tipo) throws Exception {

        BachDescargasPDDTO foo = new BachDescargasPDDTO();

        foo.setNumSerie(numSerie);
        foo.setTipoDescarga(ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(tipo)));

        BachDescargasPDDTO resp = pdDAO.admonDescarga(foo, 3);
        Gson gson = new Gson();

        return gson.toJson(resp, BachDescargasPDDTO.class);
    }

    // Servicio para actualizar los parametros de una tableta
    @RequestMapping(value = "updateParamTabletBatch.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateParamTabletBatch(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("numSerie") String numSerie,
            @RequestParam("estatus") int estatus,
            @RequestParam("tipo") String tipo) throws Exception {

        BachDescargasPDDTO foo = new BachDescargasPDDTO();

        foo.setNumSerie(numSerie);
        foo.setIdEstatus(estatus);
        foo.setTipoDescarga(ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(tipo)));

        BachDescargasPDDTO resp = pdDAO.admonDescarga(foo, 2);
        Gson gson = new Gson();

        return gson.toJson(resp, BachDescargasPDDTO.class);
    }

    /**
     * ***************************************************
     */
    /**
     * SERVICIOS PARA EJECUTAR PROCESOS BATCH DESDE URL *
     */
    /**
     * ***************************************************
     */
    // Servicio que ejecuta el proceso BATCH para actualizar estatus de documentos y folios
    @RequestMapping(value = "actualizaEstatusFoliosPD.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String actualizaEstatusFoliosPD(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fecha") String fecha) throws Exception {
        String resp = estatusGeneralBI.actualizaEstatusFoliosPD(fecha);
        return resp;
    }

    // Servicio que ejecuta el proceso BATCH para actualizar estatus de APK
    @RequestMapping(value = "actualizaEstatusTabletasAPK.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String actualizaEstatusTabletasAPK(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fecha") String fecha) throws Exception {
        String resp = estatusGeneralBI.actualizaEstatusTabletasAPK(fecha);
        return resp;
    }

    // Servicio que ejecuta el proceso BATCH para actualizar estatus de APK
    @RequestMapping(value = "actualizaEstatusTabletasDOC.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String actualizaEstatusTabletasDOC(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("fecha") String fecha) throws Exception {
        String resp = estatusGeneralBI.actualizaEstatusTabletasDOC(fecha);
        return resp;
    }

    /**
     * ***********************************************************
     */
    /**
     * SERVICIOS PARA INSERTAR CATALOGO DE ESTATUS DE DOCUMENTO *
     */
    /**
     * ***********************************************************
     */
    // Servicio para insertar un estatus al catálogo de estatus de documentos
    @RequestMapping(value = "insertEstatusDocInCat.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertEstatusDocInCat(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idEstatusDoc") int idEstatusDoc,
            @RequestParam("descEstatus") String descEstatus,
            @RequestParam("activo") int activo) throws Exception {

        String resp = estatusGeneralBI.insertEstatusDocInCat(0, idEstatusDoc,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(descEstatus)),
                activo);
        return resp;
    }

    // Servicio para actualizar un estatus al catálogo de estatus de documentos
    @RequestMapping(value = "updateEstatusDocInCat.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateEstatusDocInCat(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idEstatusDoc") int idEstatusDoc,
            @RequestParam("descEstatus") String descEstatus,
            @RequestParam("activo") int activo) throws Exception {

        String resp = estatusGeneralBI.updateEstatusDocInCat(1, idEstatusDoc,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(descEstatus)),
                activo);
        return resp;
    }

    // Servicio para consultar el catálogo de estatus de documentos
    @RequestMapping(value = "getEstatusDocInCat.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<CatalogoEstatusDocPDDTO> getEstatusDocInCat(HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<CatalogoEstatusDocPDDTO> resp = estatusGeneralBI.getEstatusDocInCat(2);
        return resp;
    }

    /**
     * ***********************************************************
     */
    // Servicio que devuelve informacion de cecos
    @RequestMapping(value = "getCecoDataByCecoPadre.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<CecoDataPDDTO> getCecoDataByCecoPadre(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("dependeDe") int dependeDe) throws Exception {

        List<CecoDataPDDTO> resp = estatusGeneralBI.getCecoDataByCecoPadre(2, dependeDe);
        return resp;
    }

    // Servicio que realiza el siguiente flujo:
    // Valida si existen tabletas instaladas en esa sucursal
    // Si existe alguna tableta, la actualiza a 'EN MANTENIMIENTO'
    @RequestMapping(value = "validaMantenimientoTableta.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String validaMantenimientoTableta(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("sucursal") String sucursal) throws Exception {

        String resp = estatusGeneralBI.validaMantenimientoTableta(3,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(sucursal)));
        return resp;
    }

    // Servicio que devuelve el estatus de documentos de una categoria
    @RequestMapping(value = "getEstatusFoliosTableta.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<FoliosEstatusPDDTO> getEstatusFoliosTableta(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("sucursal") String sucursal,
            @RequestParam("idCategoria") int idCategoria) throws Exception {

        List<FoliosEstatusPDDTO> resp = estatusGeneralBI.getEstatusFoliosTableta(4,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(sucursal)),
                idCategoria);
        return resp;
    }

    /**
     * ***********************************************************
     */
    // Servicio para obtener el historico del estatus de una tableta
    @RequestMapping(value = "getHistoricTabletData.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<HistoricTabletDataPDDTO> getHistoricTabletData(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idSucursal") int idSucursal) throws Exception {

        List<HistoricTabletDataPDDTO> resp = estatusGeneralBI.getHistoricTabletData(3, idSucursal);
        return resp;
    }

    /**
     * ***********************************************************
     */
    // Servicio para actualizar categorías (RAIZ)
    @RequestMapping(value = "actualizaRaizEnCategoria.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String actualizaRaizEnCategoria(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTipoDoc") String idTipoDoc) throws Exception {

        String resp = estatusGeneralBI.actualizaRaizEnCategoria(4, idTipoDoc);
        return resp;
    }

    // Servicio que actualiza el estatus a 'Con problema' si la tableta no tiene conexion
    @RequestMapping(value = "updateTabletsEstatus.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateTabletsEstatus(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String resp = estatusGeneralBI.updateTabletsEstatus();
        return resp;
    }

    // Servicio para obtener resultados por filtro de reportería detallado con numero de folio
    @RequestMapping(value = "filtroRepDetaIdFolioExcel.json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean filtroRepDetaIdFolioExcel(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idFolio") int idFolio) throws Exception {

        ServletOutputStream sos = null;
        List<RepDetaCecoPDDTO> resp = null;
        StringBuilder archivo = new StringBuilder();

        try {
            resp = estatusGeneralBI.filtroRepDetaIdFolio(1, idFolio);

            archivo.append("<html><head><meta charset='UTF-8'></head><body>");
            archivo.append("<table width='50%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse: collapse;'>");
            archivo.append("<thead>");
            archivo.append("<tr>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Territorio</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Zona</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Regi&oacute;n</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Pa&iacute;s</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Estado</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Municipio</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Nombre (N&uacute;mero de Sucursal)</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Categor&iacute;a</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Nombre del documento</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Estatus documento</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Fecha de env&iacute;o</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Fecha de visualizaci&oacute;n</th>");
            archivo.append("<th style='background-color: #b2cdb2; color: #2f2f2f;' align='center'>Vigencia final</th>");
            archivo.append("</tr>");
            archivo.append("</thead>");
            archivo.append("<tbody>");

            for (RepDetaCecoPDDTO obj : resp) {
                archivo.append("<tr>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getTerritorio() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getZona() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getRegion() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getPais() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getEstado() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getMunicipio() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getNombre() + "(" + obj.getIdSucursal() + ") </td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getDescCategoria() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getNombreDoc() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getDescVisibleSuc() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getFechaEnv() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getVigenciaIni() + "</td>");
                archivo.append("<td align='center' style='background-color: #e0e0e0; color: #2f2f2f;'>" + obj.getVigenciaFin() + "</td>");
                archivo.append("</tr>");
            }

            archivo.append("</tbody></table>");
            archivo.append("</body></html>");

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            byte[] _data = archivo.toString().getBytes();
            response.setContentType("Content-type: application/x-msexcel; charset='UTF-8'; name='excel';");
            response.setHeader("Content-Disposition", "attachment; filename=reporte_tabletas_" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            response.setContentLength(_data.length);
            sos = response.getOutputStream();
            sos.write(_data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    // Servicio que devuelve las licencias de funcionamiento de acuerdo al numero de sucursal
    @RequestMapping(value = "getLicenciasSuc.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ArrayList<DocumentoSucursalDTO> getLicenciasSuc(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("sucursal") int sucursal) throws Exception {

        ArrayList<DocumentoSucursalDTO> list = filesSucursalBI.getDocumentoBySucursal(sucursal);
        return list;
    }

    /**
     * Servicio para obtener parametros con informacion de negocios
     */
    @RequestMapping(value = "getParamNegocios.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ArrayList<ParamNegocioPDDTO> getParamNegocios(HttpServletRequest request, HttpServletResponse response) throws Exception {

        ArrayList<ParamNegocioPDDTO> list = estatusGeneralBI.getParamNegocios();
        return list;
    }

    /**
     * Servicio para obtener información de indicadores en estatus de tableta,
     * de acuerdo al negocio suministrado
     */
    @RequestMapping(value = "getIndiByNeg.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<IndicadoresDataETDTO> getIndiByNeg(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "negocio", required = true) String negocio) throws Exception {

        List<IndicadoresDataETDTO> indicadores = null;
        indicadores = estatusGeneralBI.getIndiByNeg(1, negocio);

        return indicadores;
    }

    /**
     * Servicio que obtiene informacion de cecos para construir graficas en
     * estatus de tabletas
     */
    @RequestMapping(value = "getCecosNegocio.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<CecosByNegPDDTO> getCecosNegocio(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "tipo", required = true) String tipo,
            @RequestParam(value = "ceco", required = false) String ceco,
            @RequestParam(value = "negocio", required = false) String negocio) throws Exception {

        List<CecosByNegPDDTO> list = estatusGeneralBI.getCecosNegocio(tipo, ceco, negocio);
        return list;
    }

    /**
     * Servicio para obtener informacion adicional y grafica de un ceco en
     * estatus de tableta
     */
    @RequestMapping(value = "getCecoInfoGraph.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<CecoInfoGraphPDDTO> getCecoInfoGraph(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "op", required = true) String op,
            @RequestParam(value = "negocio", required = false) String sucursal,
            @RequestParam(value = "ceco", required = false) String ceco) throws Exception {

        List<CecoInfoGraphPDDTO> list = estatusGeneralBI.getCecoInfoGraph(op, sucursal, ceco);
        return list;
    }

    /**
     * Servicio para obtener informacion de sucursales
     */
    @RequestMapping(value = "getSucEstTabByCeco.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<SucEstTabPDDTO> getSucEstTabByCeco(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "op", required = true) String op,
            @RequestParam(value = "negocio", required = false) String negocio,
            @RequestParam(value = "ceco", required = false) String ceco) throws Exception {

        List<SucEstTabPDDTO> list = estatusGeneralBI.getSucEstTabByCeco(op, negocio, ceco);
        return list;
    }

    /**
     * Servicio para exportar informacion de cecos en formato Excel
     */
    @RequestMapping(value = "exportDataCecoExcel.json", method = RequestMethod.POST)
    public @ResponseBody
    Boolean exportDataCecoExcel(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "op", required = true) String op,
            @RequestParam(value = "ceco", required = false) String ceco) throws Exception {

        StringBuilder archivo = new StringBuilder();
        ServletOutputStream sos = null;

        try {

            List<SucEstTabPDDTO> data = null;
            if (op.equals("0")) {
                data = estatusGeneralBI.getSucEstTabByCeco(op, null, ceco);
            } else if (op.equals("1")) {
                data = estatusGeneralBI.getSucEstTabByCeco(op, ceco, null);
            } else if (op.equals("2")) {
                data = estatusGeneralBI.getSucEstTabByCeco(op, null, ceco);
            }

            archivo.append("<html><head><meta charset='UTF-8'></head><body>");
            archivo.append("<table class='tblGeneral tblEstatusZona display scroll'>"
                    + "<thead>"
                    + "<tr>"
                    + "<th>&nbsp;</th>"
                    + "<th># Tableta</th>"
                    + "<th>N&uacute;mero de serie</th>"
                    + "<th>Sucursal</th>"
                    + "<th>Usuario</th>"
                    + "<th>Estatus</th>"
                    + "<th>Fecha de instalaci&oacute;n</th>"
                    + "<th>Fabricante</th>"
                    + "<th>Modelo</th>"
                    + "<th>Sistema Op.</th>"
                    + "<th>Versi&oacute;n</th>"
                    + "<th>&Uacute;ltima actualizaci&oacute;n</th>"
                    + "<th>Estatus CECO</th>"
                    + "</tr>"
                    + "</thead>"
                    + "<tbody>");

            for (SucEstTabPDDTO row : data) {

                archivo.append("<tr>");

                if (row.getFcactivo().equals("0")) {
                    // SUCURSAL CERRADA
                    archivo.append("<td style='background-color: #ff9f17; width: 10px;'></td>");
                } else {
                    if (row.getFiid_estatus() == 1) {
                        // CORRECTO
                        archivo.append("<td style='background-color: #4b7262; width: 10px;'></td>");
                    } else if (row.getFiid_estatus() == 2) {
                        // CON PROBLEMA
                        archivo.append("<td style='background-color: #d96169; width: 10px;'></td>");
                    } else if (row.getFiid_estatus() == 3) {
                        // EN MANTENIMIENTO
                        archivo.append("<td style='background-color: #c4d6c4; width: 10px;'></td>");
                    } else if (row.getFiid_estatus() == 0) {
                        // SIN TABLETA
                        archivo.append("<td style='background-color: #fff; width: 10px;'></td>");
                    }
                }

                archivo.append("<td>" + row.getFiid_tableta() + "</td>");
                archivo.append("<td>" + row.getFcnum_serie() + "</td>");
                archivo.append("<td> (" + row.getSucursal() + ") " + row.getNom_ceco() + "</td>");
                archivo.append("<td>" + row.getUsuario_tablet() + "</td>");
                archivo.append("<td>" + row.getDesc_id_estatus() + "</td>");
                archivo.append("<td>" + row.getFdfecha_instala() + "</td>");
                archivo.append("<td>" + row.getFcfabricante() + "</td>");
                archivo.append("<td>" + row.getFcmodelo() + "</td>");
                archivo.append("<td>" + row.getFcsistema_o() + "</td>");
                archivo.append("<td>" + row.getFcversion_app() + "</td>");
                archivo.append("<td>" + row.getUltima_actualizacion() + "</td>");
                archivo.append("<td>" + row.getDescactivo_ceco() + "</td>");

                archivo.append("</tr>");
            }

            archivo.append("</tbody></table>");
            archivo.append("</body></html>");

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Date date = new Date();
            String fecha = dateFormat.format(date).replace(" ", "_").replace(":", "_").replace("/", "_");

            byte[] _data = archivo.toString().getBytes();
            response.setContentType("Content-type: application/vnd.ms-excel; charset='UTF-8';");
            response.setHeader("Content-Disposition", "attachment; filename=estatus_tableta_" + fecha + ".xls");
            response.setHeader("Pragma: no-cache", "Expires: 0");
            response.setContentLength(_data.length);
            sos = response.getOutputStream();
            sos.write(_data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}

class DocData {

    private String ogFileName;
    private String documentName;
    private int documentId;
    private String idCategory;
    private String fileType;
    private long fileSize;
    private int idCategoria;

    public String getOgFileName() {
        return ogFileName;
    }

    public void setOgFileName(String ogFileName) {
        this.ogFileName = ogFileName;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public int getDocumentId() {
        return documentId;
    }

    public void setDocumentId(int documentId) {
        this.documentId = documentId;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }
}
