package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.ArchiveroBI;
import com.gruposalinas.franquicia.business.CanalBI;
import com.gruposalinas.franquicia.business.CecoBI;
import com.gruposalinas.franquicia.business.ChecklistNegocioBI;
import com.gruposalinas.franquicia.business.EmpFijoBI;
import com.gruposalinas.franquicia.business.FormArchiverosBI;
import com.gruposalinas.franquicia.business.GrupoBI;
import com.gruposalinas.franquicia.business.HorarioBI;
import com.gruposalinas.franquicia.business.NegocioAdicionalBI;
import com.gruposalinas.franquicia.business.NegocioBI;
import com.gruposalinas.franquicia.business.NivelBI;
import com.gruposalinas.franquicia.business.OrdenGrupoBI;
import com.gruposalinas.franquicia.business.PaisBI;
import com.gruposalinas.franquicia.business.PaisNegocioBI;
import com.gruposalinas.franquicia.business.ParametroBI;
import com.gruposalinas.franquicia.business.PerfilBI;
import com.gruposalinas.franquicia.business.PerfilUsuarioBI;
import com.gruposalinas.franquicia.business.PeriodoBI;
import com.gruposalinas.franquicia.business.PosibleTipoPreguntaBI;
import com.gruposalinas.franquicia.business.PosiblesBI;
import com.gruposalinas.franquicia.business.PuestoBI;
import com.gruposalinas.franquicia.business.RecursoBI;
import com.gruposalinas.franquicia.business.RecursoPerfilBI;
import com.gruposalinas.franquicia.business.StatusFormArchiverosBI;
import com.gruposalinas.franquicia.business.SucursalBI;
import com.gruposalinas.franquicia.business.TipoArchivoBI;
import com.gruposalinas.franquicia.business.Usuario_ABI;
import com.gruposalinas.franquicia.domain.ArchiveroDTO;
import com.gruposalinas.franquicia.domain.CanalDTO;
import com.gruposalinas.franquicia.domain.CecoDTO;
import com.gruposalinas.franquicia.domain.EmpFijoDTO;
import com.gruposalinas.franquicia.domain.FormArchiverosDTO;
import com.gruposalinas.franquicia.domain.GrupoDTO;
import com.gruposalinas.franquicia.domain.HorarioDTO;
import com.gruposalinas.franquicia.domain.NegocioDTO;
import com.gruposalinas.franquicia.domain.NivelDTO;
import com.gruposalinas.franquicia.domain.OrdenGrupoDTO;
import com.gruposalinas.franquicia.domain.PaisDTO;
import com.gruposalinas.franquicia.domain.ParametroDTO;
import com.gruposalinas.franquicia.domain.PerfilDTO;
import com.gruposalinas.franquicia.domain.PerfilUsuarioDTO;
import com.gruposalinas.franquicia.domain.PeriodoDTO;
import com.gruposalinas.franquicia.domain.PuestoDTO;
import com.gruposalinas.franquicia.domain.RecursoDTO;
import com.gruposalinas.franquicia.domain.RecursoPerfilDTO;
import com.gruposalinas.franquicia.domain.StatusFormArchiveroDTO;
import com.gruposalinas.franquicia.domain.SucursalDTO;
import com.gruposalinas.franquicia.domain.TipoArchivoDTO;
import com.gruposalinas.franquicia.domain.Usuario_ADTO;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("/catalogosService")
public class AltaCatalogosService {

    @Autowired
    CanalBI canalbi;
    @Autowired
    PerfilBI perfilbi;
    @Autowired
    PuestoBI puestobi;
    @Autowired
    TipoArchivoBI tipoarchivobi;
    @Autowired
    CecoBI cecobi;
    @Autowired
    SucursalBI sucursalbi;
    @Autowired
    Usuario_ABI usuarioabi;
    @Autowired
    PaisBI paisbi;
    @Autowired
    HorarioBI horariobi;
    @Autowired
    ParametroBI parametrobi;
    @Autowired
    NegocioBI negociobi;
    @Autowired
    NivelBI nivelbi;
    @Autowired
    PerfilUsuarioBI perfilusuariobi;
    @Autowired
    RecursoBI recursobi;
    @Autowired
    RecursoPerfilBI recursoperfilbi;
    @Autowired
    PosiblesBI posiblesbi;
    @Autowired
    PosibleTipoPreguntaBI posibletipopreguntabi;
    @Autowired
    ChecklistNegocioBI checklistNegocioBI;
    @Autowired
    NegocioAdicionalBI adicionalBI;
    @Autowired
    GrupoBI grupoBI;
    @Autowired
    OrdenGrupoBI ordenGrupoBI;
    @Autowired
    PeriodoBI periodoBI;
    @Autowired
    PaisNegocioBI paisNegocioBI;
    @Autowired
    EmpFijoBI empFijoBI;
    @Autowired
    StatusFormArchiverosBI statusFormArchiverosBI;
    @Autowired
    FormArchiverosBI formArchiverosBI;
    @Autowired
    ArchiveroBI archiveroBI;

    private static final Logger logger = LogManager.getLogger(AltaCatalogosService.class);

    // http://localhost:8080/franquicia/catalogosService/altaCanal.json?setActivo=<?>&descripcion=<?>
    @RequestMapping(value = "/altaCanal", method = RequestMethod.GET)
    public ModelAndView altaCanal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String activo = request.getParameter("setActivo");
            String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");

            CanalDTO canal = new CanalDTO();
            canal.setActivo(Integer.parseInt(activo));
            canal.setDescrpicion(descripcion);
            int idCanal = canalbi.insertaCanal(canal);
            canal.setIdCanal(idCanal);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID DE CANAL CREADO");
            mv.addObject("res", idCanal);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaPerfil.json?setDescripcion=<?>
    @RequestMapping(value = "/altaPerfil", method = RequestMethod.GET)
    public ModelAndView altaPerfil(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String descripcion = new String(request.getParameter("setDescripcion").getBytes("ISO-8859-1"), "UTF-8");

            PerfilDTO perfil = new PerfilDTO();
            perfil.setDescripcion(descripcion);
            int idPerfil = perfilbi.insertaPerfil(perfil);
            perfil.setIdPerfil(idPerfil);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID DE PERFIL CREADO");
            mv.addObject("res", idPerfil);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaPuesto.json?idPuesto=<?>&setActivo=<?>&setCodigo=<?>&descripcion=<?>&idCanal=<?>&idNegocio=<?>&idNivel=<?>&idSubnegocio=<?>&idTipoPuesto=<?>
    @RequestMapping(value = "/altaPuesto", method = RequestMethod.GET)
    public ModelAndView altaPuesto(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idPuestoParam = request.getParameter("idPuesto");
            String activo = request.getParameter("setActivo");
            String codigo = request.getParameter("setCodigo");
            String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
            String idCanal = request.getParameter("idCanal");
            String idNegocio = request.getParameter("idNegocio");
            String idNivel = request.getParameter("idNivel");
            String idSubnegocio = request.getParameter("idSubnegocio");
            String idTipoPuesto = request.getParameter("idTipoPuesto");

            PuestoDTO puesto = new PuestoDTO();
            puesto.setIdPuesto(Integer.parseInt(idPuestoParam));
            puesto.setActivo(Integer.parseInt(activo));
            puesto.setCodigo(codigo);
            puesto.setDescripcion(descripcion);
            puesto.setIdCanal(Integer.parseInt(idCanal));
            puesto.setIdNegocio(Integer.parseInt(idNegocio));
            puesto.setIdNivel(Integer.parseInt(idNivel));
            puesto.setIdSubnegocio(Integer.parseInt(idSubnegocio));
            puesto.setIdTipoPuesto(Integer.parseInt(idTipoPuesto));

            int puestoId = puestobi.insertaPuesto(puesto);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID DE PUESTO CREADO");
            mv.addObject("res", puestoId);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaTipoArchivo.json?setNombreTipo=<?>
    @RequestMapping(value = "/altaTipoArchivo", method = RequestMethod.GET)
    public ModelAndView altaTipoArchivo(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String nombre = new String(request.getParameter("setNombreTipo").getBytes("ISO-8859-1"), "UTF-8");

            TipoArchivoDTO tipoArchivo = new TipoArchivoDTO();
            tipoArchivo.setNombreTipo(nombre);
            int idArchivo = tipoarchivobi.insertaTipoArchivo(tipoArchivo);
            tipoArchivo.setIdTipoArchivo(idArchivo);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID TIPO ARCHIVO CREADO");
            mv.addObject("res", idArchivo);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaCeco.json?activo=<?>&calle=<?>&ciudad=<?>&cp=<?>&idCeco=<?>&descCeco=<?>&faxContacto=<?>&idCanal=<?>&idCecoSuperior=<?>&idEstado=<?>&idNegocio=<?>&idNivel=<?>&idPais=<?>&nombreContacto=<?>&puestoContacto=<?>&telefonoContacto=<?>&fecha=<?>&usuarioMod=<?>
    @RequestMapping(value = "/altaCeco", method = RequestMethod.GET)
    public ModelAndView altaCeco(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String activo = request.getParameter("activo");
            String calle = new String(request.getParameter("calle").getBytes("ISO-8859-1"), "UTF-8");
            String ciudad = new String(request.getParameter("ciudad").getBytes("ISO-8859-1"), "UTF-8");
            String cp = new String(request.getParameter("cp").getBytes("ISO-8859-1"), "UTF-8");
            String idCeco = request.getParameter("idCeco");
            String descCeco = new String(request.getParameter("descCeco").getBytes("ISO-8859-1"), "UTF-8");
            String faxContacto = new String(request.getParameter("faxContacto").getBytes("ISO-8859-1"), "UTF-8");
            String idCanal = request.getParameter("idCanal");
            String idCecoSuperior = request.getParameter("idCecoSuperior");
            String idEstado = request.getParameter("idEstado");
            String idNegocio = request.getParameter("idNegocio");
            String idNivel = request.getParameter("idNivel");
            String idPais = request.getParameter("idPais");
            String nombreContacto = new String(request.getParameter("nombreContacto").getBytes("ISO-8859-1"), "UTF-8");
            String puestoContacto = new String(request.getParameter("puestoContacto").getBytes("ISO-8859-1"), "UTF-8");
            String telefonoContacto = new String(request.getParameter("telefonoContacto").getBytes("ISO-8859-1"), "UTF-8");
            String fecha = request.getParameter("fecha");
            String usuarioMod = request.getParameter("usuarioMod");

            CecoDTO ceco = new CecoDTO();
            ceco.setActivo(Integer.parseInt(activo));
            ceco.setCalle(calle);
            ceco.setCiudad(ciudad);
            ceco.setCp(cp);
            ceco.setDescCeco(descCeco);
            ceco.setFaxContacto(faxContacto);
            ceco.setIdCanal(Integer.parseInt(idCanal));
            ceco.setIdCeco(idCeco);
            ceco.setIdCecoSuperior(Integer.parseInt(idCecoSuperior));
            ceco.setIdEstado(Integer.parseInt(idEstado));
            ceco.setIdNegocio(Integer.parseInt(idNegocio));
            ceco.setIdNivel(Integer.parseInt(idNivel));
            ceco.setIdPais(Integer.parseInt(idPais));
            ceco.setNombreContacto(nombreContacto);
            ceco.setPuestoContacto(puestoContacto);
            ceco.setTelefonoContacto(telefonoContacto);
            ceco.setFechaModifico(fecha);
            ceco.setUsuarioModifico(usuarioMod);
            boolean cecoId = cecobi.insertaCeco(ceco);

            //ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "CECO CREADO");
            mv.addObject("res", cecoId);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaSucursal.json?idCanal=<?>&idPais=<?>&latitud=<?>&longitud=<?>&nombreSuc=<?>&nuSuc=<?>&idSuc=<?>
    @RequestMapping(value = "/altaSucursal", method = RequestMethod.GET)
    public ModelAndView altaSucursal(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCanal = request.getParameter("idCanal");
            String idPais = request.getParameter("idPais");
            String latitud = request.getParameter("latitud");
            String longitud = request.getParameter("longitud");
            String nombreSuc = new String(request.getParameter("nombreSuc").getBytes("ISO-8859-1"), "UTF-8");
            String nuSuc = request.getParameter("nuSuc");
            String idSuc = request.getParameter("idSuc");

            SucursalDTO sucursal = new SucursalDTO();
            sucursal.setIdCanal(Integer.parseInt(idCanal));
            sucursal.setIdPais(Integer.parseInt(idPais));
            sucursal.setLatitud(Double.parseDouble(latitud));
            sucursal.setLongitud(Double.parseDouble(longitud));
            sucursal.setNombresuc(nombreSuc);
            sucursal.setNuSucursal(nuSuc);
            sucursal.setIdSucursal(Integer.parseInt(idSuc));
            int idS = sucursalbi.insertaSucursal(sucursal);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID SUCURSAL CREADA");
            mv.addObject("res", idS);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaSucursalGCC.json?idCanal=<?>&idPais=<?>&latitud=<?>&longitud=<?>&nombreSuc=<?>&nuSuc=<?>
    @RequestMapping(value = "/altaSucursalGCC", method = RequestMethod.GET)
    public ModelAndView altaSucursalGCC(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String idCanal = request.getParameter("idCanal");
            String idPais = request.getParameter("idPais");
            String latitud = request.getParameter("latitud");
            String longitud = request.getParameter("longitud");
            String nombreSuc = new String(request.getParameter("nombreSuc").getBytes("ISO-8859-1"), "UTF-8");
            String nuSuc = request.getParameter("nuSuc");

            SucursalDTO sucursal = new SucursalDTO();
            sucursal.setIdCanal(Integer.parseInt(idCanal));
            sucursal.setIdPais(Integer.parseInt(idPais));
            sucursal.setLatitud(Double.parseDouble(latitud));
            sucursal.setLongitud(Double.parseDouble(longitud));
            sucursal.setNombresuc(nombreSuc);
            sucursal.setNuSucursal(nuSuc);

            int idS = sucursalbi.insertaSucursalGCC(sucursal);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID SUCURSAL CREADA");
            mv.addObject("res", idS);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaUsuarioA.json?activo=<?>&fecha=<?>&idCeco=<?>&idPuesto=<?>&idUsuario=<?>&nombre=<?>
    @RequestMapping(value = "/altaUsuarioA", method = RequestMethod.GET)
    public ModelAndView altaUsuarioA(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String activo = request.getParameter("activo");
            String fecha = request.getParameter("fecha");
            String idCeco = request.getParameter("idCeco");
            String idPuesto = request.getParameter("idPuesto");
            String idUsuario = request.getParameter("idUsuario");
            String nombre = new String(request.getParameter("nombre").getBytes("ISO-8859-1"), "UTF-8");

            Usuario_ADTO usuarioA = new Usuario_ADTO();
            usuarioA.setActivo(Integer.parseInt(activo));
            usuarioA.setFecha(fecha);
            usuarioA.setIdCeco(idCeco);
            usuarioA.setIdPuesto(Integer.parseInt(idPuesto));
            usuarioA.setIdUsuario(Integer.parseInt(idUsuario));
            usuarioA.setNombre(nombre);
            boolean usuA = usuarioabi.insertaUsuario(usuarioA);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "FRTA Usuario CREADO");
            mv.addObject("res", usuA);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaPais.json?activo=<?>&nombre=<?>
    @RequestMapping(value = "/altaPais", method = RequestMethod.GET)
    public ModelAndView altaPais(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String activo = request.getParameter("activo");
            String nombre = new String(request.getParameter("nombre").getBytes("ISO-8859-1"), "UTF-8");

            PaisDTO pais = new PaisDTO();
            pais.setActivo(Integer.parseInt(activo));
            pais.setNombre(nombre);
            int idP = paisbi.insertaPais(pais);
            pais.setIdPais(idP);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID PAIS CREADO");
            mv.addObject("res", idP);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaHorario.json?cveHorario=<?>&valorIni=<?>&valorFin=<?>
    @RequestMapping(value = "/altaHorario", method = RequestMethod.GET)
    public ModelAndView altaHorario(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String cveHorario = request.getParameter("cveHorario");
            String valorIni = request.getParameter("valorIni");
            String valorFin = request.getParameter("valorFin");

            HorarioDTO horario = new HorarioDTO();
            horario.setCveHorario(cveHorario);
            horario.setValorIni(valorIni);
            horario.setValorFin(valorFin);
            int idHora = horariobi.insertaHorario(horario);
            horario.setIdHorario(idHora);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID HORARIO CREADO");
            mv.addObject("res", idHora);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaParametro.json?setActivo=<?>&setClave=<?>&setValor=<?>
    //http://localhost:8080/franquicia/catalogosService/altaParametro.json?setActivo=1&setClave=correoBuzon&setValor=franquiciabaz@bancoazteca.com.mx
    @RequestMapping(value = "/altaParametro", method = RequestMethod.GET)
    public ModelAndView altaParametro(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String activo = request.getParameter("setActivo");
            String clave = request.getParameter("setClave");
            String valor = request.getParameter("setValor");

            ParametroDTO parametro = new ParametroDTO();
            parametro.setActivo(Integer.parseInt(activo));
            parametro.setClave(clave);
            parametro.setValor(valor);
            boolean param = parametrobi.insertaPrametros(parametro);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PARAMETRO CREADO CORRECTAMENTE");
            mv.addObject("res", param);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaNegocio.json?setActivo=<?>&descripcion=<?>
    @RequestMapping(value = "/altaNegocio", method = RequestMethod.GET)
    public ModelAndView altaNegocio(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String activo = request.getParameter("setActivo");
            String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");

            NegocioDTO negocio = new NegocioDTO();
            negocio.setActivo(Integer.parseInt(activo));
            negocio.setDescripcion(descripcion);
            int id = negociobi.insertaNegocio(negocio);
            negocio.setIdNegocio(id);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "NEGOCIO CREADO CORRECTAMENTE");
            mv.addObject("res", id);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaNivel.json?setActivo=<?>&codigo=<?>&descripcion=<?>&idNegocio=<?>
    @RequestMapping(value = "/altaNivel", method = RequestMethod.GET)
    public ModelAndView altaNivel(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String activo = request.getParameter("setActivo");
            String codigo = request.getParameter("codigo");
            String descripcion = new String(request.getParameter("descripcion").getBytes("ISO-8859-1"), "UTF-8");
            String idNegocio = request.getParameter("idNegocio");

            NivelDTO nivel = new NivelDTO();
            nivel.setActivo(Integer.parseInt(activo));
            nivel.setCodigo(codigo);
            nivel.setDescripcion(descripcion);
            nivel.setIdNegocio(Integer.parseInt(idNegocio));
            int id = nivelbi.insertaNivel(nivel);
            nivel.setIdNivel(id);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "NIVEL CREADO CORRECTAMENTE");
            mv.addObject("res", id);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/catalogosService/altaPerfilUsuario.json?idUsuario=<?>&idPerfil=<?>
    @RequestMapping(value = "/altaPerfilUsuario", method = RequestMethod.GET)
    public ModelAndView altaPerfilUsuario(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");

            PerfilUsuarioDTO perfilUsuarioDTO = new PerfilUsuarioDTO();
            perfilUsuarioDTO.setIdPerfil(Integer.parseInt(idPerfil));
            perfilUsuarioDTO.setIdUsuario(Integer.parseInt(idUsuario));

            boolean res = perfilusuariobi.insertaPerfilUsuario(perfilUsuarioDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "PERFIL ASIGNADO : ");
            mv.addObject("res", res);

            return mv;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/catalogosService/altaRecurso.json?nombRecurso=<?>
    @RequestMapping(value = "/altaRecurso", method = RequestMethod.GET)
    public ModelAndView altaRecurso(HttpServletRequest request, HttpServletResponse response) {
        try {
            String nombRecurso = request.getParameter("nombRecurso");

            RecursoDTO recursoDTO = new RecursoDTO();
            recursoDTO.setNombreRecurso(nombRecurso);

            int res = recursobi.insertaRecurso(recursoDTO);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID RECURSO ASIGNADO ");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/catalogosService/altaRecursoPerfil.json?idPerfil=<?>&idRecurso=<?>&inserta=<?>&consulta=<?>&elimina=<?>&modifca=<?>
    @RequestMapping(value = "/altaRecursoPerfil", method = RequestMethod.GET)
    public ModelAndView altaRecursoPerfil(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idPerfil = request.getParameter("idPerfil");
            String idRecurso = request.getParameter("idRecurso");
            String consulta = request.getParameter("consulta");
            String elimina = request.getParameter("elimina");
            String inserta = request.getParameter("inserta");
            String modifica = request.getParameter("modifca");

            RecursoPerfilDTO recursoPerfilDTO = new RecursoPerfilDTO();

            recursoPerfilDTO.setIdPerfil(idPerfil);
            recursoPerfilDTO.setIdRecurso(idRecurso);
            recursoPerfilDTO.setInserta(Integer.parseInt(inserta));
            recursoPerfilDTO.setConsulta(Integer.parseInt(consulta));
            recursoPerfilDTO.setElimina(Integer.parseInt(elimina));
            recursoPerfilDTO.setModifica(Integer.parseInt(modifica));

            boolean res = recursoperfilbi.insertaRecursoP(recursoPerfilDTO);
            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "RECURSO PERFIL ASIGNADO: ");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/catalogosService/altaPosible.json?respuesta=<?>
    @RequestMapping(value = "/altaPosible", method = RequestMethod.GET)
    public ModelAndView altaPosible(HttpServletRequest request, HttpServletResponse response) {
        try {
            String respuesta = new String(request.getParameter("respuesta").getBytes("ISO-8859-1"), "UTF-8");

            int res = posiblesbi.insertaPosible(respuesta);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID POSIBLE ASIGNADO ");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/catalogosService/altaPosibleTipo.json?tipoPreg=<?>&idPosible=<?>
    @RequestMapping(value = "/altaPosibleTipo", method = RequestMethod.GET)
    public ModelAndView altaPosibleTipo(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idTipoPregunta = request.getParameter("tipoPreg");
            String idPosible = request.getParameter("idPosible");

            int res = posibletipopreguntabi.insertaPosibleTipoPregunta(Integer.parseInt(idPosible), Integer.parseInt(idTipoPregunta));

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "ID POSIBLE TIPO ASIGNADO ");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/catalogosService/altaChecklistNegocio.json?idChecklist=<?>&idNegocio=<?>
    @RequestMapping(value = "/altaChecklistNegocio", method = RequestMethod.GET)
    public ModelAndView altaChecklistNegocio(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idChecklist = request.getParameter("idChecklist");
            String idNegocio = request.getParameter("idNegocio");

            boolean res = checklistNegocioBI.insertaChecklistNegocio(Integer.parseInt(idChecklist), Integer.parseInt(idNegocio));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Checklist-Negocio Asignado :");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/catalogosService/altaNegocioAdicional.json?idUsuario=<?>&idNegocio=<?>
    @RequestMapping(value = "/altaNegocioAdicional", method = RequestMethod.GET)
    public ModelAndView altaNegocioAdicional(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsuario = request.getParameter("idUsuario");
            String idNegocio = request.getParameter("idNegocio");

            boolean res = adicionalBI.insertaNegocioAdicional(Integer.parseInt(idUsuario), Integer.parseInt(idNegocio));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Usuario-Negocio Adicional Asignado :");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/catalogosService/altaNegocioPais.json?idNegocio=<?>&idPais=<?>
    @RequestMapping(value = "/altaNegocioPais", method = RequestMethod.GET)
    public ModelAndView altaNegocioPais(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idNegocio = request.getParameter("idNegocio");
            String idPais = request.getParameter("idPais");

            boolean res = paisNegocioBI.insertaPaisNegocio(Integer.parseInt(idNegocio), Integer.parseInt(idPais));

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Usuario-Negocio Adicional Asignado :");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/catalogosService/altaGrupo.json?commit=<?>&descripcion=<?>
    @RequestMapping(value = "/altaGrupo", method = RequestMethod.GET)
    public ModelAndView altaGrupo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String commit = request.getParameter("commit");
            String descripcion = request.getParameter("descripcion");

            GrupoDTO grupo = new GrupoDTO();
            grupo.setCommit(Integer.parseInt(commit));
            grupo.setDescripcion(descripcion);

            int res = grupoBI.insertaGrupo(grupo);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "Id Grupo");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/catalogosService/altaOrdenGrupo.json?idGrupo=<?>&idChecklist=<?>&orden=<?>&commit=<?>
    @RequestMapping(value = "/altaOrdenGrupo", method = RequestMethod.GET)
    public ModelAndView altaOrdenGrupo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idGrupo = request.getParameter("idGrupo");
            String idChecklist = request.getParameter("idChecklist");
            String orden = request.getParameter("orden");
            String commit = request.getParameter("commit");

            OrdenGrupoDTO grupoOrden = new OrdenGrupoDTO();
            grupoOrden.setIdGrupo(Integer.parseInt(idGrupo));
            grupoOrden.setIdChecklist(Integer.parseInt(idChecklist));
            grupoOrden.setOrden(Integer.parseInt(orden));
            grupoOrden.setCommit(Integer.parseInt(commit));

            int res = ordenGrupoBI.insertaGrupo(grupoOrden);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "Id Orden Grupo");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/catalogosService/altaPeriodo.json?commit=<?>&descripcion=<?>
    @RequestMapping(value = "/altaPeriodo", method = RequestMethod.GET)
    public ModelAndView altaPeriodo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String commit = request.getParameter("commit");
            String descripcion = request.getParameter("descripcion");

            PeriodoDTO periodo = new PeriodoDTO();
            periodo.setIdPeriodo(Integer.parseInt(commit));
            periodo.setDescripcion(descripcion);

            int res = periodoBI.insertaPeriodo(periodo);

            ModelAndView mv = new ModelAndView("altaModuloRes");
            mv.addObject("tipo", "Id Grupo");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    //http://localhost:8080/franquicia/catalogosService/altaEmpfijo.json?idUsuario=<?>&idActivo=<?>
    @RequestMapping(value = "/altaEmpfijo", method = RequestMethod.GET)
    public ModelAndView altaEmpfijo(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String idActivo = request.getParameter("idActivo");

            EmpFijoDTO empFijoDTO = new EmpFijoDTO();
            empFijoDTO.setIdUsuario(Integer.parseInt(idUsuario));
            empFijoDTO.setIdActivo(Integer.parseInt(idActivo));

            int res = empFijoBI.inserta(empFijoDTO);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Alta empleado " + idUsuario);
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaStatusFormArchiveros.json?idStatus=<?>&descripcion=<?>
    @RequestMapping(value = "/altaStatusFormArchiveros", method = RequestMethod.GET)
    public ModelAndView altaStatusFormArchiveros(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idStatus = 0;
            String idStatusS = request.getParameter("idStatus");
            if (idStatusS != null) {
                idStatus = Integer.parseInt(idStatusS);
            }

            String descripcion = request.getParameter("descripcion");

            StatusFormArchiveroDTO bean = new StatusFormArchiveroDTO();
            bean.setIdStatus(idStatus);
            bean.setDescripcion(descripcion);

            boolean res = statusFormArchiverosBI.insertaFormArchiveros(bean);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Alta Status formato archiveros");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaFormArchiveros.json?idFormArchiveros=<?>&idUsuario=<?>&nombre=<?>&idCeco=<?>&idUsrRecibe=<?>&nomRecibe=<?>&idStatus=<?>
    @RequestMapping(value = "/altaFormArchiveros", method = RequestMethod.GET)
    public ModelAndView altaFormArchiveros(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idFormArchiveros = 0;
            String idFormArchiverosS = request.getParameter("idFormArchiveros");
            if (idFormArchiverosS != null) {
                idFormArchiveros = Integer.parseInt(idFormArchiverosS);
            }

            int idUsuario = 0;
            String idUsuarioS = request.getParameter("idUsuario");
            if (idUsuarioS != null) {
                idUsuario = Integer.parseInt(idUsuarioS);
            }

            String nombre = request.getParameter("nombre");

            String idCeco = request.getParameter("idCeco");

            int idUsrRecibe = 0;
            String idUsrRecibeS = request.getParameter("idUsrRecibe");
            if (idUsrRecibeS != null) {
                idUsrRecibe = Integer.parseInt(idUsrRecibeS);
            }

            String nomRecibe = request.getParameter("nomRecibe");

            int idStatus = 0;
            String idStatusS = request.getParameter("idStatus");
            if (idStatusS != null) {
                idStatus = Integer.parseInt(idStatusS);
            }

            String fecha = request.getParameter("fecha");

            FormArchiverosDTO bean = new FormArchiverosDTO();
            bean.setIdFromArchivero(idFormArchiveros);
            bean.setIdUsuario(idUsuario);
            bean.setNombre(nombre);
            bean.setIdCeco(idCeco);
            bean.setIdRecibe(idUsrRecibe);
            bean.setNomRecibe(nomRecibe);
            bean.setIdStatus(idStatus);
            bean.setFecha(fecha);

            int res = formArchiverosBI.insertaFormArchiveros(bean);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Alta formato archiveros");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/catalogosService/altaArchivero.json?idArchivero=<?>&placa=<?>&marca=<?>&descripcion=<?>&observaciones=<?>&idFormato=<?>
    @RequestMapping(value = "/altaArchivero", method = RequestMethod.GET)
    public ModelAndView altaArchivero(HttpServletRequest request, HttpServletResponse response) {
        try {

            int idArchivero = 0;
            String idArchiveroS = request.getParameter("idArchivero");
            if (idArchiveroS != null) {
                idArchivero = Integer.parseInt(idArchiveroS);
            }

            String placa = request.getParameter("placa");

            String marca = request.getParameter("marca");

            String descripcion = request.getParameter("descripcion");

            String observaciones = request.getParameter("observaciones");

            int idFormato = 0;
            String idFormatoS = request.getParameter("idFormato");
            if (idFormatoS != null) {
                idFormato = Integer.parseInt(idFormatoS);
            }

            ArchiveroDTO bean = new ArchiveroDTO();
            bean.setIdArchivero(idArchivero);
            bean.setPlaca(placa);
            bean.setMarca(marca);
            bean.setDescripcion(descripcion);
            bean.setObservaciones(observaciones);
            bean.setIdFormArchivero(idFormato);

            boolean res = archiveroBI.insertaArchivero(bean);

            ModelAndView mv = new ModelAndView("muestraServiciosBoleanos");
            mv.addObject("tipo", "Alta archivero");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }
}
