package com.gruposalinas.franquicia.servicios.servidor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.gruposalinas.franquicia.business.BitacoraBI;
import com.gruposalinas.franquicia.business.CargaArchivosPedestalDigitalBI;
import com.gruposalinas.franquicia.business.CecoBI;
import com.gruposalinas.franquicia.business.ChecklistBI;
import com.gruposalinas.franquicia.business.ChecksOfflineBI;
import com.gruposalinas.franquicia.business.CorreoBI;
import com.gruposalinas.franquicia.business.EstatusGeneralBI;
import com.gruposalinas.franquicia.business.FilesSucursalPedestalBI;
import com.gruposalinas.franquicia.business.LoginBI;
import com.gruposalinas.franquicia.business.MovilInfoBI;
import com.gruposalinas.franquicia.business.NotificacionBI;
import com.gruposalinas.franquicia.business.ParametroBI;
import com.gruposalinas.franquicia.business.RespuestaBI;
import com.gruposalinas.franquicia.business.RespuestaPdfBI;
import com.gruposalinas.franquicia.business.TipoCifradoBI;
import com.gruposalinas.franquicia.business.Usuario_ABI;
import com.gruposalinas.franquicia.business.VersionBI;
import com.gruposalinas.franquicia.business.VisitaTiendaBI;
import com.gruposalinas.franquicia.dao.PedestalDigitalDAO;
import com.gruposalinas.franquicia.domain.AdminParametroPDDTO;
import com.gruposalinas.franquicia.domain.BachDescargasPDDTO;
import com.gruposalinas.franquicia.domain.BitacoraDTO;
import com.gruposalinas.franquicia.domain.CecoDTO;
import com.gruposalinas.franquicia.domain.ChecklistCompletoDTO;
import com.gruposalinas.franquicia.domain.ChecklistDTO;
import com.gruposalinas.franquicia.domain.ChecksOfflineCompletosDTO;
import com.gruposalinas.franquicia.domain.ChecksOfflineDTO;
import com.gruposalinas.franquicia.domain.CompromisoDTO;
import com.gruposalinas.franquicia.domain.DocumentoSucursalDTO;
import com.gruposalinas.franquicia.domain.FoliosEstatusPDDTO;
import com.gruposalinas.franquicia.domain.ListaEstatusTabletaPDDTO;
import com.gruposalinas.franquicia.domain.LoginDTO;
import com.gruposalinas.franquicia.domain.MovilInfoDTO;
import com.gruposalinas.franquicia.domain.ParametroDTO;
import com.gruposalinas.franquicia.domain.RegistroRespuestaDTO;
import com.gruposalinas.franquicia.domain.TabletDataPDDTO;
import com.gruposalinas.franquicia.domain.TaskInTabletPDDAO;
import com.gruposalinas.franquicia.domain.TipoCifradoDTO;
import com.gruposalinas.franquicia.domain.TipoDocumentoUrlsDTO;
import com.gruposalinas.franquicia.domain.TipoQuejaPDDTO;
import com.gruposalinas.franquicia.domain.UsuarioDTO;
import com.gruposalinas.franquicia.domain.Usuario_ADTO;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import com.gruposalinas.franquicia.util.StrCipher;
import com.gruposalinas.franquicia.util.UtilCryptoGS;
import com.gruposalinas.franquicia.util.UtilDate;
import com.gruposalinas.franquicia.util.UtilFRQ;
import com.gruposalinas.franquicia.util.UtilMail;
import com.gruposalinas.franquicia.util.UtilObject;
import com.gruposalinas.franquicia.util.UtilResource;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/servicios")
public class ServicioLogin {

    private static Logger logger = LogManager.getLogger(ServicioLogin.class);

    @Autowired
    LoginBI loginBI;
    @Autowired
    ChecklistBI checklistBI;
    @Autowired
    BitacoraBI bitacoraBI;
    @Autowired
    RegistroRespuestaDTO respuestaRespuestaDTO;
    @Autowired
    RespuestaBI respuestaBI;
    @Autowired
    Usuario_ABI usuario_ABI;
    @Autowired
    ChecksOfflineBI checksOfflineBI;
    @Autowired
    TipoCifradoBI tipoCifradoBI;
    @Autowired
    MovilInfoBI movilInfoBI;
    @Autowired
    CorreoBI correoBI;
    @Autowired
    ParametroBI parametroBI;
    @Autowired
    VisitaTiendaBI visitaBI;
    @Autowired
    CecoBI cecoBI;
    @Autowired
    VisitaTiendaBI visitaTiendaBI;
    @Autowired
    NotificacionBI notificacionBI;
    @Autowired
    RespuestaPdfBI respuestaPdfBI;
    @Autowired
    VersionBI versionBI;
    @Autowired
    CargaArchivosPedestalDigitalBI cargaArchivos;
    @Autowired
    EstatusGeneralBI estatusGeneralBI;
    @Autowired
    PedestalDigitalDAO pdDAO;

    @Autowired
    FilesSucursalPedestalBI filesSucursalBI;

    // servicios/loginRest.json?id=191312&pass=Uriel
    @SuppressWarnings("static-access")
    // servicios/loginRest.json?Lg/M4L45zj4Zl05KmbXZmB0FfUnUG08UnAcW0OXp8q0=&token=
    @RequestMapping(value = "/loginRest", method = RequestMethod.GET)
    public @ResponseBody
    String getUsrArts(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        // urides = cifra.decryptParams(uri);
        int uriinf3 = Integer.parseInt(urides.split("=")[1]);

        LoginDTO regEmpleado = null;
        String json = "";
        JsonObject object = new JsonObject();
        List<Usuario_ADTO> regChecklist = null;

        int user = 0;
        // String pass = "";

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);

        try {
            regChecklist = usuario_ABI.obtieneUsuario(uriinf3);
        } catch (Exception e) {
            UtilFRQ.printErrorLog(null, e);
            logger.info("No hay usuario en login");
        }
        if (regChecklist.size() > 0) {
            try {
                user = uriinf3;
                // pass = uriinf4;
                if (user > 0) {

                    try {
                        regEmpleado = loginBI.consultaUsuario(user);
                    } catch (Exception e) {
                        UtilFRQ.printErrorLog(null, e);
                        logger.info("Ocurrió algo  al retornar Objeto usuario");
                    }
                    if (regEmpleado != null) {
                        // LoginDTO userSession = new LoginDTO();
                        object.addProperty("nombre", regEmpleado.getNombreEmpelado());
                        object.addProperty("idPerfil", regEmpleado.getIdperfil());
                        object.addProperty("fecha", fecha);
                        object.addProperty("idPuesto", regEmpleado.getIdPuesto());
                        object.addProperty("descripcion", regEmpleado.getDescPuesto());
                        json = object.toString();
                    } else {
                        object.addProperty("nombre", "null");
                        object.addProperty("fecha", fecha);
                        json = object.toString();
                    }
                } else {
                    object.addProperty("nombre", "null");
                    object.addProperty("fecha", fecha);
                    json = object.toString();
                }
            } catch (Exception e) {
                logger.info("Ocurrió algo  al validar el usuario " + e);
            }
        } else {
            object.addProperty("nombre", "null");
            object.addProperty("fecha", fecha);
            json = object.toString();
        }
        response.setContentType("application/json");
        response.getWriter().write(json);
        return null;
    }

    /*-----------------------------------------------------------NUEVO LOGIN-----------------------------------------------------------*/
    // servicios/loginRest2.json?id=191312
    // servicios/loginRest2.json?Lg/M4L45zj4Zl05KmbXZmB0FfUnUG08UnAcW0OXp8q0=&token=
    @SuppressWarnings({"static-access", "unchecked", "null"})
    @RequestMapping(value = "/loginRest2", method = RequestMethod.GET)
    public @ResponseBody
    String getUsrArts2(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        int uriinf3 = Integer.parseInt(urides.split("=")[1]);

        String json = "";
        JsonObject object1 = null;
        JsonArray array = new JsonArray();
        JsonObject object = null;
        List<Usuario_ADTO> regChecklist = null;

        Map<String, Object> regEmpleado = null;
        List<LoginDTO> regListaUsuarios = null;
        List<ChecklistDTO> reglistaChecklist = null;

        int user = 0;
        // String pass = "";

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);

        try {
            regChecklist = usuario_ABI.obtieneUsuario(uriinf3);
        } catch (Exception e) {
            UtilFRQ.printErrorLog(null, e);
            logger.info("Ocurrió algo  al retornar el usuario en login");
        }
        if (regChecklist.size() > 0) {
            try {
                user = uriinf3;
                // pass = uriinf4;
                if (user > 0) {

                    try {
                        regEmpleado = loginBI.buscaUsuarioCheck(user);
                    } catch (Exception e) {
                        UtilFRQ.printErrorLog(null, e);
                        logger.info("Ocurrió algo  al retornar Objeto usuario");
                    }

                    if (regEmpleado != null) {

                        Iterator<Map.Entry<String, Object>> enMap = regEmpleado.entrySet().iterator();
                        while (enMap.hasNext()) {
                            Map.Entry<String, Object> enMaps = enMap.next();
                            String nomH = enMaps.getKey();
                            if (nomH == "listaUsuarios") {
                                regListaUsuarios = (List<LoginDTO>) enMaps.getValue();
                            }
                            if (nomH == "listaChecklist") {
                                reglistaChecklist = (List<ChecklistDTO>) enMaps.getValue();
                            }
                        }

                        if (regListaUsuarios != null) {

                            object1 = new JsonObject();

                            object1.addProperty("nombre", regListaUsuarios.get(0).getNombreEmpelado());
                            object1.addProperty("idPerfil", regListaUsuarios.get(0).getIdperfil());
                            object1.addProperty("fecha", fecha);
                            object1.addProperty("idPuesto", regListaUsuarios.get(0).getIdPuesto());
                            object1.addProperty("descripcion", regListaUsuarios.get(0).getDescPuesto());
                            object1.addProperty("idPais", regListaUsuarios.get(0).getIdPais());
                            object1.addProperty("idTerritorio", regListaUsuarios.get(0).getIdTerritorio());
                            object1.addProperty("idZona", regListaUsuarios.get(0).getZona());
                            object1.addProperty("idRegion", regListaUsuarios.get(0).getRegion());
                            object1.addProperty("desCeco", regListaUsuarios.get(0).getDescCeco());

                            if (reglistaChecklist != null) {
                                Iterator<ChecklistDTO> it = reglistaChecklist.iterator();
                                while (it.hasNext()) {
                                    ChecklistDTO o = it.next();
                                    object = new JsonObject();
                                    // checkList
                                    object.addProperty("id", o.getIdChecklist());
                                    object.addProperty("nombre", o.getNombreCheck());
                                    array.add(object);
                                }
                                object1.add("checklist", array);
                            } else {
                                object1.addProperty("checklist", "null");
                            }
                        } else {
                            object1.addProperty("nombre", "null");
                            object1.addProperty("fecha", fecha);
                        }
                    } else {
                        object1.addProperty("nombre", "null");
                        object1.addProperty("fecha", fecha);
                    }
                } else {
                    object1.addProperty("nombre", "null");
                    object1.addProperty("fecha", fecha);
                }
            } catch (Exception e) {
                logger.info("Ocurrió algo  al validar el usuario " + e);
            }
        } else {
            object1.addProperty("nombre", "null");
            object1.addProperty("fecha", fecha);
        }
        json = object1.toString();
        response.setContentType("application/json");
        response.getWriter().write(json);
        return null;
    }

    // servicios/checklist.json?Uh4D1aCk/i/+pjNGXM+AgNhCmS05nnARS/JueMF0L0GseeiwsjOK7DPo0wdWpwmOm6HSL49nkrdVBE9Q4SCXoQ==
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/checklist", method = RequestMethod.GET)
    public @ResponseBody
    String getChecklistDTOInJSON(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        uri = uri.split("&")[0];
        UtilDate fec = new UtilDate();
        String fechaprueba = fec.getSysDate("ddMMyyyyHHmmss");

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        // String urides = cifra.decryptParams(uri);
        String uriinf[] = urides.split("&");
        int use = Integer.parseInt(uriinf[0].split("=")[1]);
        double latitud = Double.parseDouble(uriinf[1].split("=")[1]);
        double longitud = Double.parseDouble(uriinf[2].split("=")[1]);

        List<ChecklistDTO> regChecklist = null;
        JsonArray array = new JsonArray();
        JsonObject object = null;
        JsonObject princ = new JsonObject();

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);
        String json = null;

        try {
            if (use > 0) {
                try {
                    regChecklist = checklistBI.consultaChecklist(use, latitud, longitud);
                } catch (Exception e) {
                    UtilFRQ.printErrorLog(null, e);
                    logger.info("Ocurrió algo  al retornar regChecklist");
                }
                if (regChecklist != null) {
                    Iterator<ChecklistDTO> it = regChecklist.iterator();
                    while (it.hasNext()) {
                        ChecklistDTO o = it.next();
                        object = new JsonObject();
                        JsonObject object1 = new JsonObject();
                        JsonObject object2 = new JsonObject();
                        JsonObject object3 = new JsonObject();
                        // tipoCheck
                        object1.addProperty("id", o.getIdTipoChecklist().getIdTipoCheck());
                        object1.addProperty("descripcion", o.getIdTipoChecklist().getDescTipo());
                        // checkList
                        object2.addProperty("id", o.getIdChecklist());
                        object2.addProperty("nombre", o.getNombreCheck());
                        object2.addProperty("fechaInicio", o.getFecha_inicio());
                        object2.addProperty("fechaFin", o.getFecha_fin());
                        // checkUsuario
                        object3.addProperty("id", o.getIdCheckUsua());

                        object.add("tipoCheck", object1);
                        object.add("checkList", object2);
                        object.add("checkUsuario", object3);
                        // nombre
                        object.addProperty("nombre", o.getNombresuc());
                        object.addProperty("zona", o.getZona());
                        object.addProperty("latitud", latitud);
                        object.addProperty("longitud", longitud);
                        object.addProperty("ultimaVisita", o.getUltimaVisita());

                        array.add(object);
                    }
                    princ.add("tiendasProx", array);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                } else {
                    princ.add("tiendasProx", array);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                }
            } else {
                princ.add("tiendasProx", array);
                princ.addProperty("fecha", fecha);
                json = princ.toString();
            }
            response.setContentType("application/json");
            response.getWriter().write(json);
        } catch (Exception e) {
            logger.info("Ocurrió algo  en regChecklist " + e);
        }
        return null;
    }

    // servicios/checklist.json?Uh4D1aCk/i/+pjNGXM+AgNhCmS05nnARS/JueMF0L0GseeiwsjOK7DPo0wdWpwmOm6HSL49nkrdVBE9Q4SCXoQ==
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/checklist2", method = RequestMethod.GET)
    public @ResponseBody
    String getChecklist2DTOInJSON(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        uri = uri.split("&")[0];
        UtilDate fec = new UtilDate();
        String fechaprueba = fec.getSysDate("ddMMyyyyHHmmss");

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        // String urides = cifra.decryptParams(uri);
        String uriinf[] = urides.split("&");
        int use = Integer.parseInt(uriinf[0].split("=")[1]);
        //double latitud = Double.parseDouble(uriinf[1].split("=")[1]);
        //double longitud = Double.parseDouble(uriinf[2].split("=")[1]);
        String latitud = uriinf[1].split("=")[1];
        String longitud = uriinf[2].split("=")[1];
        int idCheck = Integer.parseInt(uriinf[3].split("=")[1]);

        List<ChecklistDTO> regChecklist = null;
        JsonArray array = new JsonArray();
        JsonObject object = null;
        JsonObject princ = new JsonObject();

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);
        String json = null;

        try {
            if (use > 0) {
                try {
                    regChecklist = checklistBI.buscaChecklistCompletoPrueba(use, idCheck, latitud, longitud);
                } catch (Exception e) {
                    UtilFRQ.printErrorLog(null, e);
                    logger.info("Ocurrió algo  al retornar regChecklist");
                }
                if (regChecklist != null) {
                    Iterator<ChecklistDTO> it = regChecklist.iterator();
                    while (it.hasNext()) {
                        ChecklistDTO o = it.next();
                        object = new JsonObject();
                        JsonObject object1 = new JsonObject();
                        JsonObject object2 = new JsonObject();
                        JsonObject object3 = new JsonObject();
                        // tipoCheck
                        object1.addProperty("id", o.getIdTipoChecklist().getIdTipoCheck());
                        object1.addProperty("descripcion", o.getIdTipoChecklist().getDescTipo());
                        // checkList
                        object2.addProperty("id", o.getIdChecklist());
                        object2.addProperty("nombre", o.getNombreCheck());
                        object2.addProperty("fechaInicio", o.getFecha_inicio());
                        object2.addProperty("fechaFin", o.getFecha_fin());
                        // checkUsuario
                        object3.addProperty("id", o.getIdCheckUsua());

                        object.add("tipoCheck", object1);
                        object.add("checkList", object2);
                        object.add("checkUsuario", object3);
                        // nombre
                        object.addProperty("idCeco", o.getIdCeco());
                        object.addProperty("nombre", o.getNombresuc());
                        object.addProperty("zona", o.getZona());
                        object.addProperty("latitud", o.getLatitud());
                        object.addProperty("longitud", o.getLongitud());
                        object.addProperty("ultimaVisita", o.getUltimaVisita());

                        array.add(object);
                    }
                    princ.add("tiendasProx", array);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                } else {
                    princ.add("tiendasProx", array);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                }
            } else {
                princ.add("tiendasProx", array);
                princ.addProperty("fecha", fecha);
                json = princ.toString();
            }
            response.setContentType("application/json");
            response.getWriter().write(json);
        } catch (Exception e) {
            logger.info("Ocurrió algo  en regChecklist " + e);
        }
        return null;
    }

    // servicios/checklistUsuario.json?id=191312&idCheck=1
    // servicios/checklistUsuario.json?xzOx3NpCcqin/sVUqLeRdHqaR0E/Riq4JX6WGLh0L8k=
    @SuppressWarnings({"unchecked", "static-access", "unused"})
    @RequestMapping(value = "/checklistUsuario", method = RequestMethod.GET)
    public @ResponseBody
    String getChecklistUsuarioDTOInJSON(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        uri = uri.split("&")[0];
        UtilDate fec = new UtilDate();
        String fechaprueba = fec.getSysDate("ddMMyyyyHHmmss");

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        urides = urides.split("&")[1];
        // String urides = (cifra.decryptParams(uri)).split("&")[1];

        int idcheck = Integer.parseInt(urides.split("=")[1]);

        List<ChecklistCompletoDTO> regChecklist = null;
        List<Usuario_ADTO> regUsuariosADTO = null;
        Map<String, Object> reg = new HashMap<String, Object>();
        JsonArray array0 = new JsonArray();
        JsonArray array1 = new JsonArray();
        JsonArray array2 = new JsonArray();
        JsonArray array3 = new JsonArray();
        JsonArray array4 = new JsonArray();
        JsonObject object = null;
        JsonObject princ = new JsonObject();

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);
        String json = null;

        try {
            if (idcheck > 0) {
                try {
                    reg = checklistBI.buscaChecklistCompleto(idcheck);
                } catch (Exception e) {
                    UtilFRQ.printErrorLog(null, e);
                    logger.info("Ocurrió algo  al retornar regChecklistUsuario");
                }

                if (reg != null) {

                    Iterator<Map.Entry<String, Object>> enMap = reg.entrySet().iterator();
                    while (enMap.hasNext()) {
                        Map.Entry<String, Object> enMaps = enMap.next();
                        String nomH = enMaps.getKey();
                        if (nomH == "checklistCompleto") {
                            regChecklist = (List<ChecklistCompletoDTO>) enMaps.getValue();
                        }
                        if (nomH == "listaUsuarios") {
                            regUsuariosADTO = (List<Usuario_ADTO>) enMaps.getValue();
                        }
                    }
                    if (regChecklist != null) {
                        Iterator<ChecklistCompletoDTO> it = regChecklist.iterator();
                        Iterator<ChecklistCompletoDTO> it1 = regChecklist.iterator();
                        Iterator<ChecklistCompletoDTO> it2 = regChecklist.iterator();
                        Iterator<ChecklistCompletoDTO> it3 = regChecklist.iterator();
                        Iterator<ChecklistCompletoDTO> it4 = regChecklist.iterator();

                        JsonObject object01 = null;
                        JsonObject object1 = null;
                        JsonObject object2 = null;
                        JsonObject object3 = null;
                        JsonObject object4 = null;

                        boolean comparaobj1 = true;
                        boolean comparaobj2 = true;
                        boolean comparaobj3 = true;
                        boolean comparaobj4 = true;
                        while (it.hasNext()) {
                            ChecklistCompletoDTO o = it.next();
                            object1 = new JsonObject();
                            object2 = new JsonObject();
                            object3 = new JsonObject();
                            object4 = new JsonObject();

                            // modulo FRTAMODULO
                            if (comparaobj1) {
                                object1.addProperty("id", o.getIdModulo());
                                object1.addProperty("nombre", o.getNombreModulo());
                                object1.addProperty("idPadre", o.getIdModuloPadre());
                                object1.addProperty("nomPadre", o.getNombrePadre());
                                comparaobj1 = false;
                                array1.add(object1);
                            } else if (o != null) {
                                ChecklistCompletoDTO o1 = it1.next();
                                if (o1.getIdModulo() != o.getIdModulo()) {
                                    object1.addProperty("id", o.getIdModulo());
                                    object1.addProperty("nombre", o.getNombreModulo());
                                    object1.addProperty("idPadre", o.getIdModuloPadre());
                                    object1.addProperty("nomPadre", o.getNombrePadre());
                                    array1.add(object1);
                                }
                            }
                            // tipoPreg FRCTTIPO_PREG
                            if (comparaobj2) {
                                object2.addProperty("id", o.getIdTipoPregunta());
                                object2.addProperty("clave", o.getCvePregunta());
                                object2.addProperty("descripcion", o.getDescripcionTipo());
                                comparaobj2 = false;
                                array2.add(object2);
                            } else if (o != null) {
                                ChecklistCompletoDTO o2 = it2.next();
                                if (o2.getIdTipoPregunta() != o.getIdTipoPregunta()) {
                                    object2.addProperty("id", o.getIdTipoPregunta());
                                    object2.addProperty("clave", o.getCvePregunta());
                                    object2.addProperty("descripcion", o.getDescripcionTipo());
                                    array2.add(object2);
                                }
                            }

                            // preguntas FRTAPREGUNTA
                            if (comparaobj3) {
                                object3.addProperty("id", o.getIdPregunta());
                                object3.addProperty("idModulo", o.getIdModulo());
                                object3.addProperty("idTipo", o.getIdTipoPregunta());
                                object3.addProperty("descripcion", o.getPregunta());
                                object3.addProperty("orden", o.getOrdenPregunta());
                                comparaobj3 = false;
                                array3.add(object3);
                            } else if (o != null) {
                                ChecklistCompletoDTO o3 = it3.next();
                                if (o3.getIdPregunta() != o.getIdPregunta()) {
                                    object3.addProperty("id", o.getIdPregunta());
                                    object3.addProperty("idModulo", o.getIdModulo());
                                    object3.addProperty("idTipo", o.getIdTipoPregunta());
                                    object3.addProperty("descripcion", o.getPregunta());
                                    object3.addProperty("orden", o.getOrdenPregunta());
                                    array3.add(object3);
                                }
                            }
                            // resDes FRTARES_DES
                            if (comparaobj4) {
                                object4.addProperty("id", o.getIdArbolDesicion());
                                object4.addProperty("idCheck", idcheck);
                                object4.addProperty("idPreg", o.getIdPregunta());
                                object4.addProperty("respuesta", o.getRespuesta());
                                object4.addProperty("estatusE", o.getEstatusEvidencia());
                                object4.addProperty("ordenCheck", o.getOrdenCheckRespuesta());
                                object4.addProperty("reqAccion", o.getReqAccion());
                                object4.addProperty("reqObs", o.getReqObs());
                                object4.addProperty("obliga", o.getObliga());
                                object4.addProperty("etiqueta", o.getDesEvidencia());
                                comparaobj4 = false;
                                array4.add(object4);
                            } else if (o != null) {
                                ChecklistCompletoDTO o4 = it4.next();
                                if (o4.getIdArbolDesicion() != o.getIdArbolDesicion()) {
                                    object4.addProperty("id", o.getIdArbolDesicion());
                                    object4.addProperty("idCheck", idcheck);
                                    object4.addProperty("idPreg", o.getIdPregunta());
                                    object4.addProperty("respuesta", o.getRespuesta());
                                    object4.addProperty("estatusE", o.getEstatusEvidencia());
                                    object4.addProperty("ordenCheck", o.getOrdenCheckRespuesta());
                                    object4.addProperty("reqAccion", o.getReqAccion());
                                    object4.addProperty("reqObs", o.getReqObs());
                                    object4.addProperty("obliga", o.getObliga());
                                    object4.addProperty("etiqueta", o.getDesEvidencia());
                                    array4.add(object4);
                                }
                            }
                        }

                        if (regUsuariosADTO != null) {
                            Iterator<Usuario_ADTO> it0 = regUsuariosADTO.iterator();
                            while (it0.hasNext()) {
                                Usuario_ADTO oUS = it0.next();
                                object01 = new JsonObject();
                                // modulo USUARIOS
                                object01.addProperty("id", oUS.getIdUsuario());
                                object01.addProperty("nombre", oUS.getNombre());
                                array0.add(object01);
                            }
                        }

                        object = new JsonObject();
                        object.add("modulo", array1);
                        object.add("tipoPreg", array2);
                        object.add("preguntas", array3);
                        object.add("resDes", array4);

                        princ.add("visita", object);
                        princ.add("usuarios", array0);

                        princ.addProperty("fecha", fecha);
                        json = princ.toString();

                    } else {
                        princ.addProperty("fecha", fecha);
                        json = princ.toString();
                    }

                } else {
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                }
            } else {
                princ.addProperty("fecha", fecha);
                json = princ.toString();
            }
            response.setContentType("application/json");
            response.getWriter().write(json);
        } catch (Exception e) {
            logger.info("Ocurrió algo  en regChecklistUsuario " + e);
        }

        return null;
    }

    // servicios/bitacora.json?id=191312&idCheck=1
    @SuppressWarnings("static-access")
    // servicios/bitacora.json?KYUXWXqY5lkJ2vt8NyDBXtgRzFF/qJGHQ64Gz/6wH/Y=
    @RequestMapping(value = "/checkBitacora", method = RequestMethod.GET)
    public @ResponseBody
    String getBitacora(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS descifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        uri = uri.split("&")[0];

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }

        // String urides = descifra.decryptParams(uri);
        String uriinf = urides.split("&")[1];
        int uriidBit = Integer.parseInt(uriinf.split("=")[1]);

        String json = "";
        JsonObject object = new JsonObject();
        int regBitacora = 0;

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);

        try {
            if (uriidBit > 0) {
                try {
                    regBitacora = bitacoraBI.verificaBitacora(uriidBit);
                } catch (Exception e) {
                    UtilFRQ.printErrorLog(null, e);
                    logger.info("Ocurrió algo  al retornar bitacora");
                }
                if (regBitacora > 0) {
                    // LoginDTO userSession = new LoginDTO();
                    object.addProperty("regBitacora", regBitacora);
                    object.addProperty("fecha", fecha);
                    json = object.toString();
                } else {
                    object.addProperty("regBitacora", 0);
                    object.addProperty("fecha", fecha);
                    json = object.toString();
                }
            } else {
                object.addProperty("regBitacora", 0);
                object.addProperty("fecha", fecha);
                json = object.toString();
            }
            response.setContentType("application/json");
            response.getWriter().write(json);

        } catch (Exception e) {
            logger.info("Ocurrió algo  en bitacora " + e);
        }
        return null;
    }

    // servicios/respuesta.json?id=191312
    // servicios/respuesta.json?/LrmW7p+t0tWWlzvO5N3vg===
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/respuesta", method = RequestMethod.POST)
    public @ResponseBody
    String addRespuesta(@RequestBody String provider, HttpServletRequest request)
            throws KeyException, GeneralSecurityException, IOException {
        System.out.println(":::::::::::::::::::::DENTRO DE RESPUESTA:::::::::::::::::::::");
        /*
		 * System.out.println("SI ENTRO MIKE AL SERVICIO!!!!!");
		 * System.out.println("LO QUE ME MANDA MIKE____________");
		 * System.out.println(provider);
		 * System.out.println("____________________________"); String result =
		 * java.net.URLDecoder.decode(provider, "UTF-8"); result =
		 * result.split("finiOSswift")[0].split("inicioiOSswift")[1];
         */

        UtilCryptoGS descifra = new UtilCryptoGS();
        Gson gson = new Gson();
        String aux = "";

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        if (provider != null) {

            String json = "";

            if (idLlave == 7) {
                try {
                    System.out.println(provider);
                    String result = provider.split("finiOSswift")[0].split("inicioiOSswift")[1];
                    System.out.println("LO QUE TRAE RESULT------>" + result);
                    result = java.net.URLDecoder.decode(result, "UTF-8");
                    System.out.println("DECODEADO----->" + result);
                    result = result.replaceAll(" ", "+");
                    System.out.println("DESCODEADO Y MANEJADO----->" + result);
                    // :::::: CAMBIAMOS EL METODO DE LA CLASE StrCripher POR
                    // decrypt2(?)
                    json = (cifraIOS.decrypt2(result, FRQConstantes.getLlaveEncripcionLocalIOS()));
                    System.out.println("A VER SI ES CIERTO--->" + json);
                    json = json.replaceAll("iOsCI", "[");
                    json = json.replaceAll("iOSCD", "]");
                    System.out.println("RESPUESTA---->" + json);
                } catch (Exception e) {
                    System.out.println("Ocurrió algo  en servicio respuesta" + e);
                    return "Ocurrió algo ";
                }
            } else if (idLlave == 666) {
                json = (descifra.decryptParams(provider));
            }

            // System.out.println(json);
            logger.info("JSON ENVIADO:\n " + json);

            // String json = descifra.decryptParams(provider);
            respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
            boolean respuestas = respuestaBI.registraRespuestas(respuestaRespuestaDTO);
            if (respuestas) {
                aux = "Realizado";
            } else {
                aux = "Ocurrió algo ";
            }

        } else {
            aux = "Ocurrió algo ";
        }
        return aux;
    }

    // SERVICIO PARA AGREGAR RESPUESTAS SIN ENCRIPTADO
    @SuppressWarnings({"unused"})
    @RequestMapping(value = "/alternativo", method = RequestMethod.POST)
    public @ResponseBody
    String alternativo(@RequestBody String provider, HttpServletRequest request)
            throws KeyException, GeneralSecurityException, IOException {
        System.out.println(":::::::::::::::::::::DENTRO DE ALTERNATIVO:::::::::::::::::::::");
        UtilCryptoGS descifra = new UtilCryptoGS();
        Gson gson = new Gson();
        String aux = "";

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        if (provider != null) {
            String json = "";
            if (idLlave == 7) {
                try {
                    String result = provider.split("finiOSswift")[0].split("inicioiOSswift")[1];
                    result = java.net.URLDecoder.decode(result, "UTF-8");
                    result = result.replaceAll(" ", "+");
                    json = new String(Base64.decodeBase64(result.getBytes()));
                    json = json.replaceAll("iOsCI", "[");
                    json = json.replaceAll("iOSCD", "]");
                } catch (Exception e) {
                    logger.info("Ocurrió algo  en servicio alternativo" + e);
                }

            } else if (idLlave == 666) {
                json = (descifra.decryptParams(provider));
            }

            // String json = descifra.decryptParams(provider);
            respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
            boolean respuestas = respuestaBI.registraRespuestas(respuestaRespuestaDTO);
            if (respuestas) {
                aux = "Realizado";
            } else {
                aux = "Ocurrió algo ";
            }

        } else {
            aux = "Ocurrió algo ";
        }
        return aux;
    }

    // servicios/tipoCifrado.json?id=191312&tipoSer=1&tipoApp=1
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/tipoCifrado", method = RequestMethod.POST)
    public @ResponseBody
    String tipoCifrado(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS descifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        String uri = (request.getQueryString()).split("&")[0];
        String aux = "";
        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }

        int id = Integer.parseInt((urides.split("&")[0]).split("=")[1]);
        int tipoSer = Integer.parseInt((urides.split("&")[1]).split("=")[1]);
        int tipoApp = Integer.parseInt((urides.split("&")[2]).split("=")[1]);

        if (provider != null) {
            if (idLlave == 7) {
                try {
                    String result = provider.split("finiOSswift")[0].split("inicioiOSswift")[1];
                    result = java.net.URLDecoder.decode(result, "UTF-8");
                    result = result.replaceAll(" ", "+");
                    provider = result.replaceAll("iOsCI", "[");
                    provider = result.replaceAll("iOSCD", "]");
                } catch (Exception e) {
                    logger.info("FALLO EN EL SERVICIO DE tipoCofrado" + e.getStackTrace());
                }
            }
            // provider=(java.net.URLDecoder.decode(provider,
            // "UTF-8")).replaceAll("token=", "");

            boolean resultado = false;
            TipoCifradoDTO tipoCifradoDTO = new TipoCifradoDTO();
            tipoCifradoDTO.setNumEmpleado(id);
            tipoCifradoDTO.setTipoServicio(tipoSer);
            tipoCifradoDTO.setTipoApp(tipoApp);
            tipoCifradoDTO.setFecha(null);
            tipoCifradoDTO.setJson(provider);

            try {
                resultado = tipoCifradoBI.insertaTipoCifrado(tipoCifradoDTO);
                if (resultado) {
                    aux = "Realizado";
                } else {
                    aux = "Ocurrió algo ";
                }
            } catch (Exception e) {
                logger.info("Ocurrió algo  en servicio tipoCifrado... " + e);
            }
        } else {
            aux = "Ocurrió algo ";
        }
        return aux;
    }

    // servicios/tipoCifrado.json?id=191312&idSO=1&version=1&modelo=w810&fabricante=samsung&numMovil=1234&tipoCon=0&identif=abc
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/infoMovil", method = RequestMethod.GET)
    public @ResponseBody
    String infoMovil(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS descifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        String uri = (request.getQueryString()).split("&")[0];
        String urides = "";
        String json = "";
        JsonObject object = new JsonObject();
        boolean resultado = false;
        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (descifra.decryptParams(uri));
        }

        int id = Integer.parseInt((urides.split("&")[0]).split("=")[1]);
        String idSO = (urides.split("&")[1]).split("=")[1];
        String version = (urides.split("&")[2]).split("=")[1];
        String modelo = (urides.split("&")[3]).split("=")[1];
        String fabricante = (urides.split("&")[4]).split("=")[1];
        String numMovil = (urides.split("&")[5]).split("=")[1];
        String tipoCon = (urides.split("&")[6]).split("=")[1];
        String identif = (urides.split("&")[7]).split("=")[1];

        MovilInfoDTO movilInfoDTO = new MovilInfoDTO();
        movilInfoDTO.setIdUsuario(id);
        movilInfoDTO.setSo(idSO);
        movilInfoDTO.setVersion(version);
        movilInfoDTO.setModelo(modelo);
        movilInfoDTO.setFabricante(fabricante);
        movilInfoDTO.setNumMovil(numMovil);
        movilInfoDTO.setTipoCon(tipoCon);
        movilInfoDTO.setIdentificador(identif);
        movilInfoDTO.setFecha(null);

        try {
            resultado = movilInfoBI.insertaInfoMovil(movilInfoDTO);

            if (resultado) {
                object.addProperty("nombre", "aceptado");
                object.addProperty("fecha", fecha);
                json = object.toString();
            } else {
                object.addProperty("nombre", "null");
                object.addProperty("fecha", fecha);
                json = object.toString();
            }

            response.setContentType("application/json");
            response.getWriter().write(json);
        } catch (Exception e) {
            logger.info("Ocurrió algo  en servicio infoMovil... " + e);
        }
        return null;
    }

    // servicios/checklistOffline.json?id=191312
    // servicios/checklistOffline.json?Lg/M4L45zj4Zl05KmbXZmB0FfUnUG08UnAcW0OXp8q0=
    @SuppressWarnings({"unchecked", "static-access"})
    @RequestMapping(value = "/checklistOffline", method = RequestMethod.GET)
    public @ResponseBody
    String getChecklistOfflineDTOInJSON(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        String uri = request.getQueryString();
        uri = uri.split("&")[0];

        StrCipher cifraIOS = new StrCipher();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        // String urides = cifra.decryptParams(uri);
        int use = Integer.parseInt(urides.split("=")[1]);

        Map<String, Object> regChecklistOffline = null;
        JsonArray array = new JsonArray();
        JsonObject object = null;
        JsonObject princ = new JsonObject();

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);
        String json = null;

        try {
            if (use > 0) {
                try {
                    regChecklistOffline = checksOfflineBI.obtieneChecksOffline(use);
                } catch (Exception e) {
                    logger.info("Ocurrió algo  al retornar el Map regChecklistOffline");
                }
                if (regChecklistOffline != null) {
                    List<ChecksOfflineDTO> listaChecksOfflineDTO = null;
                    Map<String, Object> mapGeneralChecksOfflineCompletosDTO = null;
                    List<Object> listaUsuariosChecksOfflineCompletosDTO = null;
                    Iterator<Map.Entry<String, Object>> entries = regChecklistOffline.entrySet().iterator();
                    while (entries.hasNext()) {
                        Map.Entry<String, Object> entry = entries.next();
                        String nom = entry.getKey();
                        if (nom == "checklists") {
                            listaChecksOfflineDTO = (List<ChecksOfflineDTO>) entry.getValue();
                        } else if (nom == "checklistsCompletos") {
                            mapGeneralChecksOfflineCompletosDTO = (Map<String, Object>) entry.getValue();
                        } else if (nom == "listaUsuarios") {
                            listaUsuariosChecksOfflineCompletosDTO = (List<Object>) entry.getValue();
                        }
                    }
                    List<ChecksOfflineCompletosDTO> listaGeneralChecksOfflineCompletosDTO = null;
                    if (listaChecksOfflineDTO != null) {
                        Iterator<ChecksOfflineDTO> it = listaChecksOfflineDTO.iterator();
                        while (it.hasNext()) {
                            ChecksOfflineDTO o = it.next();

                            object = new JsonObject();
                            JsonObject object1 = new JsonObject();
                            JsonObject object2 = new JsonObject();
                            JsonObject object3 = new JsonObject();
                            // tipoCheck
                            object1.addProperty("id", o.getIdTipoCheck());
                            object1.addProperty("descripcion", o.getDescripcion());
                            // checkList
                            object2.addProperty("id", o.getIdCheck());
                            object2.addProperty("nombre", o.getNombreCheck());
                            object2.addProperty("fechaInicio", o.getFechaInicio());
                            object2.addProperty("fechaFin", o.getFechaFin());
                            // checkUsuario
                            object3.addProperty("id", o.getIdCheckUsua());

                            object.add("tipoCheck", object1);
                            object.add("checkList", object2);
                            object.add("checkUsuario", object3);
                            // nombre
                            object.addProperty("idCeco", o.getIdCeco());
                            object.addProperty("nombre", o.getNombreCeco());
                            object.addProperty("zona", o.getZona());
                            object.addProperty("latitud", o.getLatitud());
                            object.addProperty("longitud", o.getLongitud());
                            object.addProperty("ultimaVisita", o.getUltimaVisita());

                            /* EMPIEZA EL CHECKCOMPLETO */
                            JsonArray array1 = new JsonArray();
                            JsonArray array2 = new JsonArray();
                            JsonArray array3 = new JsonArray();
                            JsonArray array4 = new JsonArray();
                            JsonObject object01 = null;

                            if (mapGeneralChecksOfflineCompletosDTO != null) {
                                listaGeneralChecksOfflineCompletosDTO = (List<ChecksOfflineCompletosDTO>) mapGeneralChecksOfflineCompletosDTO
                                        .get("" + o.getIdCheckUsua());
                                if (listaGeneralChecksOfflineCompletosDTO != null) {
                                    Iterator<ChecksOfflineCompletosDTO> it0 = listaGeneralChecksOfflineCompletosDTO
                                            .iterator();
                                    Iterator<ChecksOfflineCompletosDTO> it1 = listaGeneralChecksOfflineCompletosDTO
                                            .iterator();
                                    Iterator<ChecksOfflineCompletosDTO> it2 = listaGeneralChecksOfflineCompletosDTO
                                            .iterator();
                                    Iterator<ChecksOfflineCompletosDTO> it3 = listaGeneralChecksOfflineCompletosDTO
                                            .iterator();
                                    Iterator<ChecksOfflineCompletosDTO> it4 = listaGeneralChecksOfflineCompletosDTO
                                            .iterator();

                                    JsonObject object11 = null;
                                    JsonObject object12 = null;
                                    JsonObject object13 = null;
                                    JsonObject object14 = null;

                                    boolean comparaobj1 = true;
                                    boolean comparaobj2 = true;
                                    boolean comparaobj3 = true;
                                    boolean comparaobj4 = true;
                                    while (it0.hasNext()) {
                                        ChecksOfflineCompletosDTO oC = it0.next();
                                        object11 = new JsonObject();
                                        object12 = new JsonObject();
                                        object13 = new JsonObject();
                                        object14 = new JsonObject();

                                        // modulo FRTAMODULO
                                        if (comparaobj1) {
                                            object11.addProperty("id", oC.getIdModulo());
                                            object11.addProperty("nombre", oC.getNombreModulo());
                                            object11.addProperty("idPadre", oC.getIdModuloPadre());
                                            object11.addProperty("nomPadre", oC.getModuloPadre());
                                            comparaobj1 = false;
                                            array1.add(object11);
                                        } else if (oC != null) {
                                            ChecksOfflineCompletosDTO o1 = it1.next();
                                            if (o1.getIdModulo() != oC.getIdModulo()) {
                                                object11.addProperty("id", oC.getIdModulo());
                                                object11.addProperty("nombre", oC.getNombreModulo());
                                                object11.addProperty("idPadre", oC.getIdModuloPadre());
                                                object11.addProperty("nomPadre", oC.getModuloPadre());
                                                array1.add(object11);
                                            }
                                        }
                                        // tipoPreg FRCTTIPO_PREG
                                        if (comparaobj2) {
                                            object12.addProperty("id", oC.getIdTipoPreg());
                                            object12.addProperty("clave", oC.getCveTipoPregunta());
                                            object12.addProperty("descripcion", oC.getDescripcionTipo());
                                            comparaobj2 = false;
                                            array2.add(object12);
                                        } else if (oC != null) {
                                            ChecksOfflineCompletosDTO o2 = it2.next();
                                            if (o2.getIdTipoPreg() != oC.getIdTipoPreg()) {
                                                object12.addProperty("id", oC.getIdTipoPreg());
                                                object12.addProperty("clave", oC.getCveTipoPregunta());
                                                object12.addProperty("descripcion", oC.getDescripcionTipo());
                                                array2.add(object12);
                                            }
                                        }

                                        // preguntas FRTAPREGUNTA
                                        if (comparaobj3) {
                                            object13.addProperty("id", oC.getIdPregunta());
                                            object13.addProperty("idModulo", oC.getIdModulo());
                                            object13.addProperty("idTipo", oC.getIdTipoPreg());
                                            object13.addProperty("descripcion", oC.getPregunta());
                                            object13.addProperty("orden", oC.getOrdepregunta());
                                            object13.addProperty("pregPadre", oC.getPreguntaPadre());
                                            comparaobj3 = false;
                                            array3.add(object13);
                                        } else if (oC != null) {
                                            ChecksOfflineCompletosDTO o3 = it3.next();
                                            if (o3.getIdPregunta() != oC.getIdPregunta()) {
                                                object13.addProperty("id", oC.getIdPregunta());
                                                object13.addProperty("idModulo", oC.getIdModulo());
                                                object13.addProperty("idTipo", oC.getIdTipoPreg());
                                                object13.addProperty("descripcion", oC.getPregunta());
                                                object13.addProperty("orden", oC.getOrdepregunta());
                                                object13.addProperty("pregPadre", oC.getPreguntaPadre());
                                                array3.add(object13);
                                            }
                                        }
                                        // resDes FRTARES_DES
                                        if (comparaobj4) {
                                            object14.addProperty("id", oC.getIdArbolDes());
                                            object14.addProperty("idCheck", oC.getIdCheckUsua());
                                            object14.addProperty("idPreg", oC.getIdPregunta());
                                            object14.addProperty("respuesta", oC.getRespuesta());
                                            object14.addProperty("estatusE", oC.getEstatusEvidencia());
                                            object14.addProperty("ordenCheck", oC.getSiguienteOrden());
                                            object14.addProperty("reqAccion", oC.getReqAccion());
                                            object14.addProperty("reqObs", oC.getReqObs());
                                            object14.addProperty("obliga", oC.getObliga());
                                            object14.addProperty("etiqueta", oC.getEtiqueta());
                                            comparaobj4 = false;
                                            array4.add(object14);
                                        } else if (oC != null) {
                                            ChecksOfflineCompletosDTO o4 = it4.next();
                                            if (o4.getIdArbolDes() != oC.getIdArbolDes()) {
                                                object14.addProperty("id", oC.getIdArbolDes());
                                                object14.addProperty("idCheck", oC.getIdCheckUsua());
                                                object14.addProperty("idPreg", oC.getIdPregunta());
                                                object14.addProperty("respuesta", oC.getRespuesta());
                                                object14.addProperty("estatusE", oC.getEstatusEvidencia());
                                                object14.addProperty("ordenCheck", oC.getSiguienteOrden());
                                                object14.addProperty("reqAccion", oC.getReqAccion());
                                                object14.addProperty("reqObs", oC.getReqObs());
                                                object14.addProperty("obliga", oC.getObliga());
                                                object14.addProperty("etiqueta", oC.getEtiqueta());
                                                array4.add(object14);
                                            }
                                        }
                                    }

                                    object01 = new JsonObject();
                                    object01.add("modulo", array1);
                                    object01.add("tipoPreg", array2);
                                    object01.add("preguntas", array3);
                                    object01.add("resDes", array4);

                                    object.add("visita", object01);
                                    object.addProperty("bitacora", o.getIdBitacora());

                                }
                            } else {
                                object.addProperty("fecha", fecha);
                            }

                            /* TERMINA EL COMPLETO */
                            array.add(object);

                        }
                    }

                    JsonArray array0 = new JsonArray();
                    JsonObject object02 = null;

                    if (listaUsuariosChecksOfflineCompletosDTO != null) {

                        Iterator<Object> it01 = listaUsuariosChecksOfflineCompletosDTO.iterator();
                        while (it01.hasNext()) {
                            List<Usuario_ADTO> listaUsuarios = (List<Usuario_ADTO>) it01.next();
                            if (listaUsuarios != null) {
                                Iterator<Usuario_ADTO> it02 = listaUsuarios.iterator();
                                while (it02.hasNext()) {
                                    Usuario_ADTO objUsuarios = (Usuario_ADTO) it02.next();
                                    object02 = new JsonObject();
                                    // modulo USUARIOS
                                    object02.addProperty("id", objUsuarios.getIdUsuario());
                                    object02.addProperty("nombre", objUsuarios.getNombre());
                                    object02.addProperty("sucursal", objUsuarios.getDesPuestof());
                                    array0.add(object02);
                                }
                            }
                        }
                    }
                    princ.add("tiendasProx", array);
                    princ.add("usuarios", array0);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();

                } else {
                    princ.add("tiendasProx", array);
                    princ.addProperty("fecha", fecha);
                    json = princ.toString();
                }
            } else {
                princ.add("tiendasProx", array);
                princ.addProperty("fecha", fecha);
                json = princ.toString();
            }
            response.setContentType("application/json");
            response.getWriter().write(json);
        } catch (Exception e) {
            logger.info("Ocurrió algo  en servicio regChecklistOffline " + e);
        }
        return null;
    }

    // servicio para enviar una pregunta
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/preguntaOnline", method = RequestMethod.POST)
    public @ResponseBody
    boolean setPreguntaOnline(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        Gson gson = new Gson();

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                logger.info("JSON IOS: " + json);

                respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
                boolean respuestas = respuestaBI.insertaUnaRespuesta(respuestaRespuestaDTO);
                if (respuestas) {
                    logger.info("SE AGREGO CORRECTAMENTE");
                    return true;

                }

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                logger.info("JSON ANDROID: " + json);
                respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
                boolean respuestas = respuestaBI.insertaUnaRespuesta(respuestaRespuestaDTO);
                if (respuestas) {
                    logger.info("SE AGREGO CORRECTAMENTE");
                    return true;

                }

            }
        } catch (Exception e) {
            logger.info("Ocurrió algo  " + e.getMessage());
        }

        return false;
    }

    // servicio para enviar una pregunta
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/preguntaOnlineEliminar", method = RequestMethod.POST)
    public @ResponseBody
    boolean setPreguntaOnlineEliminar(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        Gson gson = new Gson();

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                logger.info("JSON IOS: " + json);

                respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
                boolean respuestas = respuestaBI.eliminaRespuestas(respuestaRespuestaDTO);
                if (respuestas) {
                    logger.info("SE ELIMINO CORRECTAMENTE");
                    return true;

                }

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                logger.info("JSON ANDROID: " + json);
                respuestaRespuestaDTO = gson.fromJson(json, RegistroRespuestaDTO.class);
                boolean respuestas = respuestaBI.eliminaRespuestas(respuestaRespuestaDTO);
                if (respuestas) {
                    logger.info("SE ELIMINO CORRECTAMENTE");
                    return true;

                }

            }
        } catch (Exception e) {
            logger.info("Ocurrió algo  " + e.getMessage());
        }

        return false;
    }

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getCompromisosUsuario", method = RequestMethod.GET)
    public @ResponseBody
    String getCompromisosUsuario(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        List<CompromisoDTO> compromisos = null;

        Gson g = new Gson();

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        int usuario = Integer.parseInt(urides.split("=")[1].split("&")[0]);
        int idChecklist = Integer.parseInt(urides.split("&")[1].split("=")[1]);
        int idCeco = Integer.parseInt(urides.split("&")[2].split("=")[1]);

        logger.info("OBTENER COMPROMISOS DEL USUARIO:  " + usuario);

        try {
            compromisos = checklistBI.compromisosChecklist(idChecklist, idCeco);

        } catch (Exception e) {
            compromisos = null;
            logger.info("Ocurrió algo  AL OBTENER LOS COMPROMISOS " + e.getMessage());
        }

        String jsonSalida = "";
        jsonSalida = "{\"compromisosSucursal\":" + g.toJson(compromisos) + "}";

        logger.info("JSON DE SALIDA: " + jsonSalida);

        return jsonSalida;
    }

    /*
	 * @SuppressWarnings( "static-access" )
	 * 
	 * @RequestMapping(value = "/getCompromisosUsuario", method =
	 * RequestMethod.GET) public @ResponseBody Map<String,Object>
	 * getCompromisosUsuario(HttpServletRequest request, HttpServletResponse
	 * response) throws KeyException, GeneralSecurityException, IOException {
	 * 
	 * UtilCryptoGS cifra = new UtilCryptoGS(); StrCipher cifraIOS = new
	 * StrCipher(); String uri = request.getQueryString(); String uriAp =
	 * request.getParameter("token"); int idLlave =
	 * Integer.parseInt((uriAp.split(";")[1]).split("=")[1]); uri =
	 * uri.split("&")[0]; String urides = ""; String json = "";
	 * Map<String,Object> compromisos=null;
	 * 
	 * // se lee los valores enviados para la pregunta UtilCryptoGS descifra =
	 * new UtilCryptoGS();
	 * 
	 * 
	 * if (idLlave == 7) { urides = (cifraIOS.decrypt(uri,
	 * FRQConstantes.getLlaveEncripcionLocalIOS())); } else if (idLlave == 666)
	 * { urides = (cifra.decryptParams(uri)); }
	 * 
	 * int usuario = Integer.parseInt(urides.split("=")[1]);
	 * 
	 * logger.info("OBTENER COMPROMISOS DEL USUARIO:  "+usuario);
	 * 
	 * try { compromisos= checklistBI.compromisosChecklist(usuario); } catch
	 * (Exception e) { compromisos=null; logger.info(
	 * "Ocurrió algo  AL OBTENER LOS COMPROMISOS "+e.getMessage()); } return
	 * compromisos; }
     */

    // servicio para enviar una pregunta
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/estadisticaDispositivo", method = RequestMethod.POST)
    public @ResponseBody
    boolean setEstadisticaDispositivo(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        Gson gson = new Gson();

        String fabricante = null;
        String identificador = null;
        String modelo = null;
        String so = null;
        String versionSO = null;
        String numCelular = null;
        String numEmpleado = null;
        String tipoConexion = null;
        String versionApp = null;
        String token = null;

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                logger.info("JSON IOS: " + json);

                JSONObject jsonObject = new JSONObject(json);

                fabricante = jsonObject.getString("fabricante");
                identificador = jsonObject.getString("identificador");
                modelo = jsonObject.getString("modelo");
                so = jsonObject.getString("so");
                versionSO = jsonObject.getString("versionSO");
                numCelular = jsonObject.getString("numCelular");
                numEmpleado = jsonObject.getString("numEmpleado");
                tipoConexion = jsonObject.getString("tipoConexion");
                versionApp = jsonObject.getString("versionApp");
                token = jsonObject.getString("tokenUsuario");

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                logger.info("JSON ANDROID: " + json);

                JSONObject jsonObject = new JSONObject(json);

                fabricante = jsonObject.getString("fabricante");
                identificador = jsonObject.getString("identificador");
                modelo = jsonObject.getString("modelo");
                so = jsonObject.getString("so");
                versionSO = jsonObject.getString("versionSO");
                numCelular = jsonObject.getString("numCelular");
                numEmpleado = jsonObject.getString("numEmpleado");
                tipoConexion = jsonObject.getString("tipoConexion");
                versionApp = jsonObject.getString("versionApp");
                token = jsonObject.getString("tokenUsuario");

            }

            int usuario = Integer.parseInt(urides.split("=")[1]);

            MovilInfoDTO movilInfoDTO = new MovilInfoDTO();
            movilInfoDTO.setIdUsuario(usuario);
            movilInfoDTO.setSo(so);
            movilInfoDTO.setVersion(versionSO);
            movilInfoDTO.setModelo(modelo);
            movilInfoDTO.setFabricante(fabricante);
            movilInfoDTO.setNumMovil(numCelular);
            movilInfoDTO.setTipoCon(tipoConexion);
            movilInfoDTO.setVersionApp(versionApp);
            movilInfoDTO.setIdentificador(identificador);
            movilInfoDTO.setToken(token);

            logger.info("MOVIL DTO: " + movilInfoDTO);

            boolean infoMovil = movilInfoBI.insertaInfoMovil(movilInfoDTO);

            if (!infoMovil) {
                logger.info("Ocurrió algo  AL INSERTA LA INFORMACIÓN DEL MOVIL");
            }
            return infoMovil;

        } catch (Exception e) {
            logger.info("Ocurrió algo  " + e.getMessage());
        }

        return false;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/enviarFechaTermino", method = RequestMethod.POST)
    public @ResponseBody
    boolean setFechaTermino(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        boolean retorno = false;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        int idBitacora = 0;

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                logger.info("JSON IOS: " + json);

                JSONObject jsonObject = new JSONObject(json);
                idBitacora = jsonObject.getInt("idBitacora");
                String fecha = jsonObject.getString("fecha");

                boolean respuestas = checklistBI.actualizaFechatermino(fecha, idBitacora);
                if (respuestas) {

                    logger.info("SE ACTUALIZO LA FECHA TERMINO CORRECTAMENTE");
                    retorno = true;

                }

            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                logger.info("JSON ANDROID: " + json);

                JSONObject jsonObject = new JSONObject(json);
                idBitacora = jsonObject.getInt("idBitacora");
                String fecha = jsonObject.getString("fecha");

                boolean respuestas = checklistBI.actualizaFechatermino(fecha, idBitacora);
                if (respuestas) {

                    logger.info("SE ACTUALIZO LA FECHA TERMINO CORRECTAMENTE");
                    retorno = true;

                }

            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            retorno = false;
        }

        // empieza a ejecutar la lista de compromisos para enviar el correo
        int usuario = Integer.parseInt(urides.split("=")[1]);

        List<CompromisoDTO> listaCompromiso = new ArrayList<CompromisoDTO>();
        String letraNegocio = "";
        int numNegocio = 0;
        String numTienda = "";
        String idUsuario = "";
        int numChecklist = 0;
        try {
            logger.info("Obtiene la lista de compromisos... ");
            logger.info("idBitacora... " + idBitacora);
            listaCompromiso = checklistBI.compromisosChecklistWeb(idBitacora);

            if (listaCompromiso != null) {
                numTienda = listaCompromiso.get(0).getEconomico();
                idUsuario = listaCompromiso.get(0).getIdUsuario();
                numChecklist = listaCompromiso.get(0).getIdChecklist();
                numNegocio = listaCompromiso.get(0).getIdNegocio();
                // idUsuario="189870";
                logger.info("Numero de Negocio... " + numNegocio);
                logger.info("Numero de Tienda... " + numTienda);

                letraNegocio = (parametroBI.obtieneParametros("negocio" + numNegocio)).get(0).getValor();
                logger.info("Letra de Negocio... " + letraNegocio);

                if (numTienda.length() == 4) {
                    numTienda = letraNegocio + numTienda;
                } else if (numTienda.length() == 3) {
                    numTienda = letraNegocio + "0" + numTienda;
                } else if (numTienda.length() == 2) {
                    numTienda = letraNegocio + "00" + numTienda;
                } else if (numTienda.length() == 1) {
                    numTienda = letraNegocio + "000" + numTienda;
                }

                logger.info("Numero de Negocio Final... " + numTienda);
                logger.info("idUsuario... " + idUsuario);
                logger.info("Numero de checklist... " + numChecklist);
            } else {
                logger.info("No cuenta con compromisos...");
            }
        } catch (Exception e1) {
            logger.info("Ocurrio algo: " + e1);
        }

        logger.info("listaCompromiso... " + listaCompromiso);

        UsuarioDTO user = new UsuarioDTO();
        user.setIdUsuario("" + usuario);
        user.setNombre(numTienda);

        //INICIO ENVIO ACUEDOS
        List<ParametroDTO> valida = null;
        String arr[] = null;
        boolean bandeCorreo = true;

        if (listaCompromiso != null) {
            try {
                valida = parametroBI.obtieneParametros("enviaCorreo");
                logger.info("Estado del activo " + valida.get(0).getActivo());
                if (valida.get(0).getActivo() == 1) {
                    bandeCorreo = true;
                    logger.info("Valores del parametro enviaCorreo " + valida.get(0).getValor());
                    arr = valida.get(0).getValor().split(",");
                    logger.info("Tamaño del array de parametros " + arr.length);
                    for (int i = 0; i < arr.length; i++) {
                        logger.info("Valor de parametro " + (i + 1) + ": " + arr[i]);
                        if (Integer.parseInt(arr[i]) == numChecklist) {
                            if (listaCompromiso != null) {
                                try {
                                    logger.info("Mandando correo... ");
                                    correoBI.sendMailAcuerdos(user, listaCompromiso);
                                    logger.info("Correo Enviado");
                                    bandeCorreo = false;
                                } catch (Exception e) {
                                    logger.info(e);
                                }
                            }
                        }
                    }
                    if (bandeCorreo) {
                        logger.info("Correo no enviado por que el checklist no existe en el activo");
                    }
                } else {
                    logger.info("Cerro bitacora pero no envio correo por que esta desactivado... ");
                }
            } catch (Exception e) {
                logger.info("Ocurrio algo " + e);
            }
        } else {
            logger.info("No envia correo por que la lista de compromisos esta vacia...");
        }
        //FIN ENVIO ACUERDOS

        //ENVIO DE PDF DE 5 CHECKS'
        List<ParametroDTO> validaPDF = null;
        Map<String, String> resultado = new HashMap<String, String>();
        String cecoAux = "";

        try {
            validaPDF = parametroBI.obtieneParametros("checklistsPDF");
            logger.info("Estado del activo " + validaPDF.get(0).getActivo());
            if (validaPDF.get(0).getActivo() == 1) {
                bandeCorreo = true;
                int banderaEnvio = 0;
                try {
                    logger.info("COMPROBANDO ENVIO DE PDF COMPROMISOS 5 CHECKS... ");
                    //VERUFICAMOS SI SE DEBE ENVIAR O NO
                    logger.info("VERIFICANDO ENVIO... ");
                    logger.info("idUsuario " + usuario);
                    logger.info("idBitacora " + idBitacora);

                    banderaEnvio = respuestaPdfBI.obtieneEnvioCompromiso(usuario, idBitacora);
                    logger.info("RESPUESTA DE VERIFICANDO ENVIO... " + banderaEnvio);
                    if (banderaEnvio == 1) {
                        logger.info("CONSULTA CECO POR BITACORA... ");
                        resultado = respuestaPdfBI.obtieneCecoPorBitacora(usuario, idBitacora);
                        String ceco = resultado.get("idCeco");
                        String cecoCom = resultado.get("idCeco");
                        String nNegocio = resultado.get("numNegocio");
                        logger.info("CECO QUE ESTOY REGRESANDO " + ceco);
                        logger.info("NUMERO NEGOCIO QUE ESTOY REGRESANDO " + nNegocio);

                        letraNegocio = (parametroBI.obtieneParametros("negocio" + nNegocio)).get(0).getValor();
                        logger.info("Letra de Negocio... " + letraNegocio);

                        if (ceco.length() == 6) {
                            ceco = ceco.substring(2, 6);
                        }
                        if (ceco.length() == 5) {
                            ceco = ceco.substring(1, 5);
                        }

                        if (ceco.length() == 4) {
                            cecoAux = letraNegocio + ceco;
                        } else if (ceco.length() == 3) {
                            cecoAux = letraNegocio + "0" + ceco;
                        } else if (ceco.length() == 2) {
                            cecoAux = letraNegocio + "00" + ceco;
                        } else if (ceco.length() == 1) {
                            cecoAux = letraNegocio + "000" + ceco;
                        }

                        logger.info("CECO AUX ---> " + cecoAux);
                        user.setNombre(cecoAux);
                        logger.info("ESTES ES EL VALOR DE MI CECO!!!" + ceco);
                        logger.info("Mandando correo compromisos... ");
                        correoBI.sendMailCompromisosChecklist(user, cecoCom);
                        logger.info("Correo compromisos Enviado");
                        bandeCorreo = false;
                    }
                } catch (Exception e) {
                    logger.info("Ocurrio algo " + e);
                }
                if (banderaEnvio == 0) {
                    logger.info("Correo no enviado por que no se tienen completados los checklist correspondientes");
                }
            } else {
                logger.info("Cerro bitacora pero no envio correo por que esta desactivado ENVIO DE PDF COMPROMISOS 5 CHECKS.... ");
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
        }
        //FIN ENVIO PEDF 5 CHECKS
        return retorno;
    }

    // http://10.51.210.239:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCorreosAcuerdos.json?id=189870&idtienda=G2244&idBitacora=3611
    // COMPROMINSOS  http://10.51.210.239:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCorreosAcuerdos.json?id=189870&idtienda=189871&idBitacora=5774
    // 5 CHECKS http://10.51.210.239:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCorreosAcuerdos.json?id=189870&idtienda=189871&idBitacora=9926
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getCorreosAcuerdos", method = RequestMethod.GET)
    public @ResponseBody
    boolean getCorreosAcuerdos(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        Map<String, Object> compromisos = null;

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        int usuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
        String idTienda = urides.split("&")[1].split("=")[1];
        int idBitacora = Integer.parseInt(urides.split("&")[2].split("=")[1]);

        List<CompromisoDTO> listaCompromiso = new ArrayList<CompromisoDTO>();
        String letraNegocio = "";
        int numNegocio = 0;
        String numTienda = "";
        String idUsuario = "";
        int numChecklist = 0;

        try {
            logger.info("Obtiene la lista de compromisos... ");
            logger.info("idBitacora... " + idBitacora);
            listaCompromiso = checklistBI.compromisosChecklistWeb(idBitacora);
            if (listaCompromiso != null) {
                numTienda = listaCompromiso.get(0).getEconomico();
                idUsuario = listaCompromiso.get(0).getIdUsuario();
                numChecklist = listaCompromiso.get(0).getIdChecklist();
                numNegocio = listaCompromiso.get(0).getIdNegocio();
                // idUsuario="189870";
                logger.info("Numero de Negocio... " + numNegocio);
                logger.info("Numero de Tienda... " + numTienda);

                letraNegocio = (parametroBI.obtieneParametros("negocio" + numNegocio)).get(0).getValor();
                logger.info("Letra de Negocio... " + letraNegocio);

                if (numTienda.length() == 4) {
                    numTienda = letraNegocio + numTienda;
                } else if (numTienda.length() == 3) {
                    numTienda = letraNegocio + "0" + numTienda;
                } else if (numTienda.length() == 2) {
                    numTienda = letraNegocio + "00" + numTienda;
                } else if (numTienda.length() == 1) {
                    numTienda = letraNegocio + "000" + numTienda;
                }

                logger.info("Numero de Negocio Final... " + numTienda);
                logger.info("idUsuario... " + idUsuario);
                logger.info("Numero de checklist... " + numChecklist);
            } else {
                logger.info("No cuenta con compromisos...");
            }
        } catch (Exception e1) {
            logger.info("Ocurrio algo: " + e1);
        }

        logger.info("listaCompromiso... " + listaCompromiso);

        UsuarioDTO user = new UsuarioDTO();
        user.setIdUsuario("" + usuario);
        user.setNombre(idTienda);

        //INICIO ENVIO ACUEDOS
        List<ParametroDTO> valida = null;
        String arr[] = null;
        boolean bandeCorreo = true;

        if (listaCompromiso != null) {
            try {
                valida = parametroBI.obtieneParametros("enviaCorreo");
                logger.info("Estado del activo " + valida.get(0).getActivo());
                if (valida.get(0).getActivo() == 1) {
                    bandeCorreo = true;
                    logger.info("Valores del parametro enviaCorreo " + valida.get(0).getValor());
                    arr = valida.get(0).getValor().split(",");
                    logger.info("Tamaño del array de parametros " + arr.length);
                    for (int i = 0; i < arr.length; i++) {
                        logger.info("Valor de parametro " + (i + 1) + ": " + arr[i]);
                        if (Integer.parseInt(arr[i]) == numChecklist) {
                            if (listaCompromiso != null) {
                                try {
                                    logger.info("Mandando correo... ");
                                    correoBI.sendMailAcuerdos(user, listaCompromiso);
                                    logger.info("Correo Enviado");
                                    bandeCorreo = false;
                                } catch (Exception e) {
                                    logger.info(e);
                                }
                            }
                        }
                    }
                    if (bandeCorreo) {
                        logger.info("Correo no enviado por que el checklist no existe en el activo");
                    }
                } else {
                    logger.info("Cerro bitacora pero no envio correo por que esta desactivado... ");
                }
            } catch (Exception e) {
                logger.info("Ocurrio algo " + e);
            }
        } else {
            logger.info("No envia correo por que la lista de compromisos esta vacia...");
        }
        //FIN ENVIO ACUERDOS

        //ENVIO DE PDF DE 5 CHECKS'
        List<ParametroDTO> validaPDF = null;
        Map<String, String> resultado = new HashMap<String, String>();
        String cecoAux = "";

        try {
            validaPDF = parametroBI.obtieneParametros("checklistsPDF");
            logger.info("Estado del activo " + validaPDF.get(0).getActivo());
            if (validaPDF.get(0).getActivo() == 1) {
                bandeCorreo = true;
                int banderaEnvio = 0;
                try {
                    logger.info("COMPROBANDO ENVIO DE PDF COMPROMISOS 5 CHECKS... ");
                    //VERUFICAMOS SI SE DEBE ENVIAR O NO
                    logger.info("VERIFICANDO ENVIO... ");
                    logger.info("idUsuario " + usuario);
                    logger.info("idBitacora " + idBitacora);

                    banderaEnvio = respuestaPdfBI.obtieneEnvioCompromiso(usuario, idBitacora);
                    logger.info("RESPUESTA DE VERIFICANDO ENVIO... " + banderaEnvio);
                    if (banderaEnvio == 1) {
                        logger.info("CONSULTA CECO POR BITACORA... ");
                        resultado = respuestaPdfBI.obtieneCecoPorBitacora(usuario, idBitacora);
                        String ceco = resultado.get("idCeco");
                        String cecoCom = resultado.get("idCeco");
                        String nNegocio = resultado.get("numNegocio");
                        logger.info("CECO QUE ESTOY REGRESANDO " + ceco);
                        logger.info("NUMERO NEGOCIO QUE ESTOY REGRESANDO " + nNegocio);

                        letraNegocio = (parametroBI.obtieneParametros("negocio" + nNegocio)).get(0).getValor();
                        logger.info("Letra de Negocio... " + letraNegocio);

                        if (ceco.length() == 6) {
                            ceco = ceco.substring(2, 6);
                        }
                        if (ceco.length() == 5) {
                            ceco = ceco.substring(1, 5);
                        }

                        if (ceco.length() == 4) {
                            cecoAux = letraNegocio + ceco;
                        } else if (ceco.length() == 3) {
                            cecoAux = letraNegocio + "0" + ceco;
                        } else if (ceco.length() == 2) {
                            cecoAux = letraNegocio + "00" + ceco;
                        } else if (ceco.length() == 1) {
                            cecoAux = letraNegocio + "000" + ceco;
                        }

                        logger.info("CECO AUX ---> " + cecoAux);
                        user.setNombre(cecoAux);
                        logger.info("ESTES ES EL VALOR DE MI CECO!!!" + ceco);
                        logger.info("Mandando correo compromisos... ");
                        correoBI.sendMailCompromisosChecklist(user, cecoCom);
                        logger.info("Correo compromisos Enviado");
                        bandeCorreo = false;
                    }
                } catch (Exception e) {
                    logger.info("Ocurrio algo " + e);
                }
                if (banderaEnvio == 0) {
                    logger.info("Correo no enviado por que no se tienen completados los checklist correspondientes");
                }
            } else {
                logger.info("Cerro bitacora pero no envio correo por que esta desactivado ENVIO DE PDF COMPROMISOS 5 CHECKS.... ");
            }
        } catch (Exception e) {
            logger.info("Ocurrio algo " + e);
        }
        //FIN ENVIO PEDF 5 CHECKS
        return true;
    }

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/envioBDdispositivo", method = RequestMethod.POST)
    public @ResponseBody
    boolean envioBDdispositivo(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        boolean retorno = false;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";
        // int idBitacora=0;

        List<BitacoraDTO> bitacora = null;
        List<CecoDTO> ceco = null;
        List<ChecklistDTO> checklist = null;
        List<Usuario_ADTO> usr = null;

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        // logger.info("JSON SIN DESENCRIPTAR: " + provider);

        List<ParametroDTO> valida = parametroBI.obtieneParametros("correoSoporte");
        if (valida.size() > 0) {
            if (valida.get(0).getActivo() == 1) {
                try {
                    if (idLlave == 7) {
                        urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                        logger.info("URIDES IOS: " + urides);

                        String tmp = "";
                        tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                        tmp = tmp.replaceAll(" ", "+");
                        logger.info("JSON IOS TMP: " + tmp);
                        contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                        logger.info("JSON IOS: " + contenido);
                    } else if (idLlave == 666) {
                        urides = (cifra.decryptParams(uri));
                        contenido = descifra.decryptParams(provider);
                        // logger.info("JSON ANDROID: " + contenido);
                    }

                    int usuario = Integer.parseInt(urides.split("=")[1].split("&")[0]);
                    int idChecklist = Integer.parseInt(urides.split("&")[1].split("=")[1]);
                    int idCeco = Integer.parseInt(urides.split("&")[2].split("=")[1]);
                    int idBitacora = Integer.parseInt(urides.split("&")[3].split("=")[1]);

                    logger.info("usuario: " + usuario);
                    logger.info("idChecklist: " + idChecklist);
                    logger.info("idCeco: " + idCeco);
                    logger.info("idBitacora: " + idBitacora);

                    usr = usuario_ABI.obtieneUsuario(usuario);
                    bitacora = bitacoraBI.buscaBitacora(null, idBitacora + "", null, null);
                    ceco = cecoBI.buscaCeco(idCeco);
                    checklist = checklistBI.buscaChecklist(idChecklist);

                } catch (Exception e) {
                    logger.info(e);
                    retorno = false;
                }

                try {

                    if (ceco != null && bitacora != null && checklist != null && usr != null) {

                        if (ceco.size() > 0 && bitacora.size() > 0 && checklist.size() > 0 && usr.size() > 0) {

                            ArrayList<String> copiados = new ArrayList<String>();
                            String titulo = "Correo de soporte " + " " + checklist.get(0).getNombreCheck() + " "
                                    + ceco.get(0).getIdCeco() + " - " + ceco.get(0).getDescCeco() + " Fecha: "
                                    + bitacora.get(0).getFechaInicio();

                            // contenido=contenido.replace(";", ";<br>");
                            // contenido=contenido.replace("\n", "<br>");
                            // logger.info(contenido);
                            Resource correoColab = UtilResource.getResourceFromInternet("mailSoporteBD.html");

                            String strCorreoOwner = UtilObject.inputStreamAsString(correoColab.getInputStream());
                            strCorreoOwner = strCorreoOwner.replace("@Titulo", "Correo de soporte");

                            strCorreoOwner = strCorreoOwner.replace("@datosChecklist",
                                    "<br>ID USUARIO: " + usr.get(0).getIdUsuario() + "<br>USUARIO: "
                                    + usr.get(0).getNombre() + "<br>CECO: " + ceco.get(0).getIdCeco()
                                    + "<br>DESCRIPCION CECO: " + ceco.get(0).getDescCeco() + "<br>CHECKLIST: "
                                    + checklist.get(0).getNombreCheck() + "<br>CHECKUSUA: "
                                    + bitacora.get(0).getIdCheckUsua() + "<br>BITACORA: "
                                    + bitacora.get(0).getIdBitacora() + "<br>FECHA DE INICIO: "
                                    + bitacora.get(0).getFechaInicio() + "<br>FECHA DE TERMINO: "
                                    + bitacora.get(0).getFechaFin()
                            );
                            strCorreoOwner = strCorreoOwner.replace("@contenido", contenido);

                            CorreosLotus correosLotus = new CorreosLotus();
                            // String destinatario = "mdelgadoa@elektra.com.mx";
                            String destinatario = valida.get(0).getValor().split(";")[0];

                            // copiados.add("mdelgadoa@elektra.com.mx");
                            // copiados.add("cvidal@bancoazteca.com.mx");
                            for (int i = 1; i < valida.get(0).getValor().split(";").length; i++) {
                                copiados.add(valida.get(0).getValor().split(";")[i]);
                            }

                            retorno = UtilMail.enviaCorreo(destinatario, copiados, "" + titulo, strCorreoOwner, null);
                        } else {
                            retorno = false;
                        }
                    } else {
                        retorno = false;
                    }

                } catch (IOException e) {
                    logger.info(e);
                    retorno = false;
                }
            } else {
                // en caso de que se desactive el parametro
                retorno = true;
            }
        } else {
            retorno = false;
        }
        return retorno;
    }

    // http://localhost:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getVisitas.json?id=191312&idChecklist=54&nuMes=1&anio=<?>
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getVisitas", method = RequestMethod.GET)
    public @ResponseBody
    String getVisitas(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        String usuario = urides.split("&")[0].split("=")[1];
        String idChecklist = urides.split("&")[1].split("=")[1];
        String nuMes = urides.split("&")[2].split("=")[1];
        String anio = urides.split("&")[3].split("=")[1];

        json = visitaBI.obtieneVisitas(idChecklist, usuario, nuMes, anio);

        return json;

    }

    // http://localhost:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getValidaCheckUsua.json?id=191312&idCheckUsua=&latitud=&longitud=
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getValidaCheckUsua", method = RequestMethod.GET)
    public @ResponseBody
    String getValidaCheckUsua(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }

        String usuario = urides.split("&")[0].split("=")[1];
        String idCheckUsua = urides.split("&")[1].split("=")[1];
        String latitud = urides.split("&")[2].split("=")[1];
        String longitud = urides.split("&")[3].split("=")[1];

        //List<ChecklistDTO> regChecklist = checklistBI.validaCheckUsua(Integer.parseInt(idCheckUsua), Double.parseDouble(latitud), Double.parseDouble(longitud));
        List<ChecklistDTO> regChecklist = checklistBI.validaCheckUsua(Integer.parseInt(idCheckUsua), latitud, longitud);

        JsonArray array = new JsonArray();
        JsonObject object = null;
        JsonObject princ = new JsonObject();

        Gson gsonfecha = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        String jsonfecha = gsonfecha.toJson(new Date());
        String fecha = jsonfecha.substring(1, 11);
        String json = null;

        if (regChecklist != null) {
            Iterator<ChecklistDTO> it = regChecklist.iterator();
            while (it.hasNext()) {
                ChecklistDTO o = it.next();
                object = new JsonObject();
                JsonObject object1 = new JsonObject();
                JsonObject object2 = new JsonObject();
                JsonObject object3 = new JsonObject();
                // tipoCheck
                object1.addProperty("id", o.getIdTipoChecklist().getIdTipoCheck());
                object1.addProperty("descripcion", o.getIdTipoChecklist().getDescTipo());
                // checkList
                object2.addProperty("id", o.getIdChecklist());
                object2.addProperty("nombre", o.getNombreCheck());
                object2.addProperty("fechaInicio", o.getFecha_inicio());
                object2.addProperty("fechaFin", o.getFecha_fin());
                // checkUsuario
                object3.addProperty("id", o.getIdCheckUsua());

                object.add("tipoCheck", object1);
                object.add("checkList", object2);
                object.add("checkUsuario", object3);
                // nombre
                object.addProperty("nombre", o.getNombresuc());
                object.addProperty("idCeco", o.getIdCeco());
                object.addProperty("zona", o.getZona());
                object.addProperty("latitud", o.getLatitud());
                object.addProperty("longitud", o.getLongitud());
                object.addProperty("ultimaVisita", o.getUltimaVisita());
                object.addProperty("estatus", o.getEstatus());
                object.addProperty("distancia", o.getDistancia());

                array.add(object);
            }
            princ.add("tiendasProx", array);
            princ.addProperty("fecha", fecha);
            json = princ.toString();
        } else {
            princ.add("tiendasProx", array);
            princ.addProperty("fecha", fecha);
            json = princ.toString();
        }
        return json;
    }
    // http://localhost:8080/franquicia/consultaChecklistService/getVisitas.json?idChecklist=<?>&idUsuario=<?>&nuMes=<?>

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getVisitasService", method = RequestMethod.GET)
    public @ResponseBody
    String getRepVisitas(HttpServletRequest request, HttpServletResponse response) {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String visitas = "";
        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        String idUsuario = "";
        String idChecklist = "";
        String nuMes = "";
        String anio = "";
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            idUsuario = urides.split("&")[0].split("=")[1];
            idChecklist = urides.split("&")[1].split("=")[1];
            nuMes = urides.split("&")[2].split("=")[1];
            anio = urides.split("&")[3].split("=")[1];
            logger.info("usuario: " + idUsuario);
            logger.info("idChecklist: " + idChecklist);
            logger.info("mes: " + nuMes);
            logger.info("anio: " + anio);

            visitas = visitaTiendaBI.obtieneVisitas(idChecklist, idUsuario, nuMes, anio);
        } catch (Exception e) {

        }

        /*
		 * String idChecklist = request.getParameter("idChecklist"); String
		 * idUsuario = request.getParameter("idUsuario"); String nuMes =
		 * request.getParameter("nuMes");
         */
        return visitas;
    }

    // http://localhost:8080/franquicia/consultaChecklistService/getVisitas.json?idChecklist=<?>&idUsuario=<?>&nuMes=<?>
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getOffline", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getOffline(HttpServletRequest request, HttpServletResponse response) {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String visitas = "";
        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();

        String idUsuario = "";
        Map<String, Object> checksUsuario = null;
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }
            idUsuario = urides.split("&")[0].split("=")[1];
            checksUsuario = checksOfflineBI.obtieneChecksNuevo(Integer.parseInt(idUsuario));
        } catch (Exception e) {
            checksUsuario = null;
        }
        return checksUsuario;
    }

    // servicios/loginRest2.json?id=191312
    // servicios/loginRest2.json?Lg/M4L45zj4Zl05KmbXZmB0FfUnUG08UnAcW0OXp8q0=&token=
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/getReportePila", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getReportePila(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        Map<String, Object> reporte = null;

        String urides = "";

        if (idLlave == 7) {
            urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
        } else if (idLlave == 666) {
            urides = (cifra.decryptParams(uri));
        }
        int idUsuario = Integer.parseInt(urides.split("=")[1]);
        logger.info("USUARIO > " + idUsuario);
        try {
            reporte = checklistBI.ReportePila(idUsuario);
        } catch (Exception e) {
            logger.info("ERROR AL OBTENER EL REPORTE PILA: " + e.getMessage());
            reporte = null;
        }
        return reporte;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/enviaCorreoBuzon", method = RequestMethod.POST)
    public @ResponseBody
    boolean setCorreosBuzon(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        boolean retorno = true;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        int idUsuario = 0;
        String nombre = "";
        String asunto = "";
        String comentario = "";
        String lugar = "";
        String telefono = "";
        String idCeco = "";
        String puesto = "";

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));

                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");

                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());

                logger.info("JSON IOS: " + json);

                JSONObject jsonObject = new JSONObject(json);
                idUsuario = Integer.parseInt(urides.split("=")[1]);
                nombre = jsonObject.getString("nombre");
                asunto = jsonObject.getString("asunto");
                lugar = jsonObject.getString("lugar");
                telefono = jsonObject.getString("telefono");
                comentario = jsonObject.getString("comentario");
                idCeco = jsonObject.getString("idCeco");
                puesto = jsonObject.getString("puesto");
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));

                json = descifra.decryptParams(provider);

                logger.info("JSON ANDROID: " + json);

                JSONObject jsonObject = new JSONObject(json);
                idUsuario = Integer.parseInt(urides.split("=")[1]);
                nombre = jsonObject.getString("nombre");
                asunto = jsonObject.getString("asunto");
                lugar = jsonObject.getString("lugar");
                telefono = jsonObject.getString("telefono");
                comentario = jsonObject.getString("comentario");
                idCeco = jsonObject.getString("idCeco");
                puesto = jsonObject.getString("puesto");
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            retorno = false;
        }

        UsuarioDTO user = new UsuarioDTO();
        user.setIdUsuario("" + idUsuario);
        user.setNombre(nombre);
        List<ParametroDTO> valida = null;
        List<ParametroDTO> copiados = null;
        boolean bandeCorreo = true;

        try {
            valida = parametroBI.obtieneParametros("enviaBuzon");
            copiados = parametroBI.obtieneParametros("correoBuzon");
            logger.info("Estado del activo " + valida.get(0).getActivo());
            if (valida.get(0).getActivo() == 1) {
                bandeCorreo = true;

                try {
                    logger.info("Mandando correo... ");
                    correoBI.sendMailBuzon(user, asunto, idCeco, puesto, lugar, telefono, comentario, copiados.get(0).getValor());
                    logger.info("Correo Enviado");
                    bandeCorreo = false;
                } catch (Exception e) {
                    logger.info(e);
                }

                if (bandeCorreo) {
                    logger.info("Correo no enviado...");
                }
            } else {
                logger.info("Correo no enviado por que el parametro esta desactivado... ");
            }
        } catch (Exception e) {
            logger.info(e);
            retorno = false;
        }

        return retorno;
    }

    // http://10.51.210.239:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/enviaCorreoCompromisosCheck.json?id=191312&idCeco=484747
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/enviaCorreoCompromisosCheck", method = RequestMethod.GET)
    public @ResponseBody
    boolean setCorreosCompromisosCheck(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        boolean retorno = true;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        int idUsuario = 0;
        String nombre = "";

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        logger.info("JSON SIN DESENCRIPTAR: " + provider);
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                json = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());
                logger.info("JSON IOS: " + json);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                json = descifra.decryptParams(provider);
                logger.info("JSON ANDROID: " + json);
            }
        } catch (Exception e) {
            logger.info(e.getMessage());
            retorno = false;
        }

        String idUser = urides.split("&")[0].split("=")[1];
        String idCeco = urides.split("&")[1].split("=")[1];
        logger.info("DATOS!!!!! idUser  ----->" + idUser + "idCeco--->" + idCeco);
        //SET USER
        UsuarioDTO user = new UsuarioDTO();
        user.setIdUsuario("" + idUser);
        user.setNombre(nombre);

        List<ParametroDTO> valida = null;
        List<ParametroDTO> copiados = null;
        boolean bandeCorreo = true;

        try {

            valida = parametroBI.obtieneParametros("enviaBuzon");
            copiados = parametroBI.obtieneParametros("correoBuzon");
            logger.info("Estado del activo " + valida.get(0).getActivo());

            if (valida.get(0).getActivo() == 1) {
                bandeCorreo = true;

                try {

                    logger.info("Mandando correo... ");
                    //IDUSUARIO-CECO
                    correoBI.sendMailCompromisosChecklist(user, idCeco);
                    logger.info("Correo Enviado");
                    /*bandeCorreo = false;*/
                } catch (Exception e) {
                    logger.info(e);
                }

                if (bandeCorreo) {
                    logger.info("Correo no enviado...");
                }
            } else {
                logger.info("Correo no enviado por que el parametro esta desactivado... ");
            }
        } catch (Exception e) {
            logger.info(e);
            retorno = false;
        }

        return true;
    }

    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/notificacionPorcentaje", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> notificacionPorcentaje(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        boolean retorno = true;
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String json = "";
        int idUsuario = 0;
        Map<String, Object> reporte = null;

        // se lee los valores enviados para la pregunta
        UtilCryptoGS descifra = new UtilCryptoGS();
        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            }
            reporte = notificacionBI.NotificacionPorcentaje(idUsuario);
        } catch (Exception e) {
            logger.info(e.getMessage());
            reporte = null;
        }
        return reporte;
    }

    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getOffline2", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getOffline2(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, GeneralSecurityException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        int idUsuario = 0;
        Map<String, Object> listasOffline = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                idUsuario = Integer.parseInt(urides.split("=")[1]);
            }
            listasOffline = checksOfflineBI.obtieneChecksNuevo(idUsuario);
        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
            listasOffline = null;
        }
        return listasOffline;
    }

    // http://10.51.210.239:8080/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCompruebaVersion.json?id=189870&so=ANDROID
    // http://10.50.50.109.47:8080/franquicia/servicios/getCompruebaVersion.json?id=189870&so=IOS
    @SuppressWarnings("static-access")
    @RequestMapping(value = "/getCompruebaVersion", method = RequestMethod.GET)
    public @ResponseBody
    String getCompruebaVersion(HttpServletRequest request, HttpServletResponse response)
            throws KeyException, IOException {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        List<String> listaVersion = null;
        String sitemaOpe = "";
        JsonObject object = new JsonObject();
        String json = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                sitemaOpe = (urides.split("&")[1]).split("=")[1];
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                sitemaOpe = (urides.split("&")[1]).split("=")[1];
            }

            listaVersion = (List<String>) versionBI.obtieneUltVersion(sitemaOpe);

            if (listaVersion != null) {
                object.addProperty("version", listaVersion.get(0));
                object.addProperty("fecha", listaVersion.get(1));
            } else {
                object.addProperty("version", "null");
                object.addProperty("fecha", "null");
            }
            json = object.toString();
        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        logger.info("json:" + json);
        return json;
    }

    //servicios/getCeco.json?id=xxxx&ceco=0
    @RequestMapping(value = "/getCeco", method = RequestMethod.GET)
    public @ResponseBody
    CecoDTO getCeco(HttpServletRequest request, HttpServletResponse response) {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";

        CecoDTO cecoReturn = null;

        try {

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            int ceco = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            List<CecoDTO> cecos = cecoBI.buscaCeco(ceco);

            if (cecos != null && !cecos.isEmpty()) {

                cecoReturn = cecos.get(0);

            }

        } catch (Exception e) {
            logger.info("ocurrio algo :" + e);
        }

        return cecoReturn;

    }

    /**
     * **************************************************************************
     */
    /**
     * ************************ SERVICIOS PARA TABLETAS
     * *************************
     */
    /**
     * **************************************************************************
     */
    @RequestMapping(value = "insertTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertTablet(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String resp = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int op = 0;
            int idTableta = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            String numSerie = urides.split("&")[2].split("=")[1];
            String idCeco = urides.split("&")[3].split("=")[1];
            int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            int activo = Integer.parseInt(urides.split("&")[6].split("=")[1]);
            int idEstatus = Integer.parseInt(urides.split("&")[4].split("=")[1]);
            String fechaInstalacion = urides.split("&")[5].split("=")[1];

            resp = cargaArchivos.insertTablet(op, idTableta, numSerie, idCeco, idUsuario, idEstatus, fechaInstalacion, activo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    @RequestMapping(value = "updateTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateTablet(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTableta") int idTableta,
            @RequestParam("numSerie") String numSerie,
            @RequestParam("idCeco") String idCeco,
            @RequestParam("idUsuario") int idUsuario,
            @RequestParam("idEstatus") int idEstatus,
            @RequestParam("fechaInstalacion") String fechaInstalacion,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {

        int op = 1;
        String resp = null;
        resp = cargaArchivos.updateTablet(op, idTableta, numSerie, idCeco, idUsuario, idEstatus, fechaInstalacion, activo);

        return resp;
    }

    @RequestMapping(value = "getTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TabletDataPDDTO> getTablet(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTableta") int idTableta,
            @RequestParam("numSerie") String numSerie) throws NumberFormatException, Exception {

        int op = 2;
        List<TabletDataPDDTO> resp = null;
        resp = cargaArchivos.getTablet(op, idTableta,
                ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(numSerie)));

        return resp;
    }

    // Servicio para insertar los datos y estatus de la tableta
    @RequestMapping(value = "insertEstatusTableta.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String insertEstatusTableta(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String resp = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int op = 0;
            // List<ListaEstatusTabletaPDDTO> listaEstatus = estatusGeneralBI.getListaEstatusTableta(op);
            List<ListaEstatusTabletaPDDTO> listaEstatus = new ArrayList<ListaEstatusTabletaPDDTO>();
            int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            String data = urides.split("&")[1].split("=")[1];
            resp = estatusGeneralBI.insertEstatusTableta(op, listaEstatus, data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    // Servicio para obtener documentos con estatus 0 y 4
    @RequestMapping(value = "getTasksInTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TaskInTabletPDDAO> getTasksInTablet(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        List<TaskInTabletPDDAO> resp = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            int b = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            int c = Integer.parseInt(urides.split("&")[2].split("=")[1]);

            resp = estatusGeneralBI.getTasksInTablet(0, b, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    // Servicio para validar si hay un documento nuevo
    @RequestMapping(value = "getLatestTaskInTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TaskInTabletPDDAO> getLatestTaskInTablet(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        List<TaskInTabletPDDAO> resp = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            int b = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            int c = Integer.parseInt(urides.split("&")[2].split("=")[1]);

            resp = estatusGeneralBI.getLatestTaskInTablet(1, b, c);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    // Servicio para actualizar el estatus de un documento en tableta
    @RequestMapping(value = "updateStatusOfTaskInTablet.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateStatusOfTaskInTablet(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String resp = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            int idDocPedestal = Integer.parseInt(urides.split("&")[1].split("=")[1]);
            int estatus = Integer.parseInt(urides.split("&")[2].split("=")[1]);

            resp = estatusGeneralBI.updateStatusOfTaskInTablet2(2, estatus, idDocPedestal);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    // Servicio para insertar queja
    @SuppressWarnings("static-access")
    @RequestMapping(value = "insertQueja.json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    boolean insertQueja(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response, Model model) throws KeyException, GeneralSecurityException, IOException {

        String json = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";
        boolean guardada = false;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                logger.info("URIDES IOS: " + urides);
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());
                logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            String usuario = (urides.split("&")[0]).split("=")[1];
            String resp = estatusGeneralBI.insertQueja(0, contenido);
            Gson gson = new Gson();
            JsonElement jsonEl = gson.fromJson(resp, JsonElement.class);
            JsonObject jsonObj = jsonEl.getAsJsonObject();

            if (jsonObj.get("respuesta").getAsInt() == 0) {
                guardada = true;
            }
        } catch (Exception e) {
            guardada = false;
        }

        return guardada;
    }

    // Servicio para obtener el catalogo de tipos de quejas
    @RequestMapping(value = "getTipoQueja.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<TipoQuejaPDDTO> getCatalogoQuejas(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        List<TipoQuejaPDDTO> resp = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            int idTipoQueja = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            resp = estatusGeneralBI.getTipoQueja(5, idTipoQueja);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    // Servicio para obtener parámetros
    @RequestMapping(value = "getParametroPD.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<AdminParametroPDDTO> getParametroPD(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        List<AdminParametroPDDTO> resp = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            int idParam = Integer.parseInt(urides.split("&")[1].split("=")[1]);

            AdminParametroPDDTO obj = new AdminParametroPDDTO();
            obj.setIdParametro(idParam);

            resp = pdDAO.admonParametros(obj, 2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    // TODO: Servicio para consultar estatus de parametro de una tableta
    @RequestMapping(value = "getParamTabletBatch.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String getParamTabletBatch(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String resp = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            String numSerie = urides.split("&")[1].split("=")[1];
            String tipo = urides.split("&")[2].split("=")[1];

            Gson gson = new Gson();
            BachDescargasPDDTO foo = new BachDescargasPDDTO();

            foo.setNumSerie(numSerie);
            foo.setTipoDescarga(tipo);

            BachDescargasPDDTO respAdmonDes = pdDAO.admonDescarga(foo, 3);
            resp = gson.toJson(respAdmonDes, BachDescargasPDDTO.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    // Servicio para actualizar los parametros de una tableta
    @RequestMapping(value = "updateParamTabletBatch.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String updateParamTabletBatch(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String resp = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            String numSerie = urides.split("&")[1].split("=")[1];
            int estatus = Integer.parseInt(urides.split("&")[2].split("=")[1]);
            String tipo = urides.split("&")[3].split("=")[1];

            Gson gson = new Gson();
            BachDescargasPDDTO foo = new BachDescargasPDDTO();

            foo.setNumSerie(numSerie);
            foo.setIdEstatus(estatus);
            foo.setTipoDescarga(tipo);

            BachDescargasPDDTO respAdmonDes = pdDAO.admonDescarga(foo, 2);
            resp = gson.toJson(respAdmonDes, BachDescargasPDDTO.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    // Servicio que realiza el siguiente flujo:
    // Valida si existen tabletas instaladas en esa sucursal
    // Si existe alguna tableta, la actualiza a 'EN MANTENIMIENTO'
    @RequestMapping(value = "validaMantenimientoTableta.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    String validaMantenimientoTableta(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String resp = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            String sucursal = urides.split("&")[1].split("=")[1];

            resp = estatusGeneralBI.validaMantenimientoTableta(3, sucursal);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    // Servicio que devuelve el estatus de documentos de una categoria
    @RequestMapping(value = "getEstatusFoliosTableta.json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<FoliosEstatusPDDTO> getEstatusFoliosTableta(HttpServletRequest request, HttpServletResponse response) throws Exception {

        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        List<FoliosEstatusPDDTO> resp = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
            String sucursal = urides.split("&")[1].split("=")[1];
            int idCategoria = Integer.parseInt(urides.split("&")[2].split("=")[1]);

            resp = estatusGeneralBI.getEstatusFoliosTableta(4, sucursal, idCategoria);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    /*@RequestMapping (value="getFilesBySucursal.json", method= RequestMethod.GET, produces="application/json")
	public @ResponseBody ArrayList<DocumentoSucursalDTO> getFilesBySucursal(HttpServletRequest request, HttpServletResponse response) throws Exception  {
		
		UtilCryptoGS cifra = new UtilCryptoGS();
		StrCipher cifraIOS = new StrCipher();
		String uri = request.getQueryString();
		String uriAp = request.getParameter("token");
		int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
		uri = uri.split("&")[0];
		String urides = "";
		
		ArrayList<DocumentoSucursalDTO> arrayDocumentos = null;
		
		try {
			
			if (idLlave == 7) {
				urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
			} else if (idLlave == 666) {
				urides = (cifra.decryptParams(uri));
			}

			int idUsuario = Integer.parseInt(urides.split("&")[0].split("=")[1]);
		    String numEconomico = urides.split("&")[1].split("=")[1];
		
		    

		    arrayDocumentos =filesSucursalBI.getDocumentoBySucursal(Integer.parseInt(numEconomico));
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
			arrayDocumentos = new ArrayList<>();
		}
		
		return arrayDocumentos;
	}
     */
    @RequestMapping(value = "getFilesBySucursal.json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    ArrayList<DocumentoSucursalDTO> getFilesBySucursal(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

        String json = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        ArrayList<DocumentoSucursalDTO> arrayDocumentos = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                logger.info("URIDES IOS: " + urides);
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());
                logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            arrayDocumentos = filesSucursalBI.getDocumentoBySucursal(contenido);

        } catch (Exception e) {
            arrayDocumentos = new ArrayList<>();
        }

        return arrayDocumentos;
    }

    @RequestMapping(value = "getFilesBySucursalURLS.json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    ArrayList<TipoDocumentoUrlsDTO> getFilesBySucursalURLS(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

        String json = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        ArrayList<TipoDocumentoUrlsDTO> arrayDocumentos = null;

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                logger.info("URIDES IOS: " + urides);
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());
                logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            arrayDocumentos = filesSucursalBI.getDocumentoBySucursalUrls(contenido);

        } catch (Exception e) {
            arrayDocumentos = new ArrayList<>();
        }

        return arrayDocumentos;
    }

    @RequestMapping(value = "getFileBase64.json", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String getFileBase64(@RequestBody String provider, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {

        String json = "";
        UtilCryptoGS cifra = new UtilCryptoGS();
        StrCipher cifraIOS = new StrCipher();
        UtilCryptoGS descifra = new UtilCryptoGS();

        String uri = request.getQueryString();
        String uriAp = request.getParameter("token");
        int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
        uri = uri.split("&")[0];
        String urides = "";
        String contenido = "";

        String fileBase64 = "";

        try {
            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
                logger.info("URIDES IOS: " + urides);
                String tmp = "";
                tmp = java.net.URLDecoder.decode(provider.split("finiOSswift")[0].split("inicioiOSswift")[1], "UTF-8");
                tmp = tmp.replaceAll(" ", "+");
                logger.info("JSON IOS TMP: " + tmp);
                contenido = cifraIOS.decrypt2(tmp, FRQConstantes.getLlaveEncripcionLocalIOS());
                logger.info("JSON IOS: " + contenido);
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
                contenido = descifra.decryptParams(provider);
                // logger.info("JSON ANDROID: " + contenido);
            }

            fileBase64 = filesSucursalBI.getFilesBase64(contenido);

        } catch (Exception e) {
            fileBase64 = "";
        }

        return "{ \"fileBase64\":\"" + fileBase64 + "\"}";
    }

}
