/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.ObtieneCecos;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author kramireza
 */
@Controller
@RequestMapping("/cecowsService")
public class CecosWSService {

    @Autowired
    ObtieneCecos obtieneCecos;

    private static Logger logger = LogManager.getLogger(CecosWSService.class);

    // http://localhost:8080/franquicia/cecowsService/getCecosWS.json
    @SuppressWarnings({"static-access", "unused"})
    @RequestMapping(value = "/getCecosWS", method = RequestMethod.GET)
    public @ResponseBody
    String getCecosFromWS(@RequestBody String provider, HttpServletRequest request,
            HttpServletResponse response) throws KeyException, GeneralSecurityException, IOException {

        logger.info("*************Comienza carga cecos*************");

        try {
            /*    UtilCryptoGS descifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uriAp = request.getParameter("token");
            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            String uri = (request.getQueryString()).split("&")[0];
            String urides = "";
            String json = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, GTNConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (descifra.decryptParams(uri));
            }*/

            String tokenResult = obtieneCecos.getTokenCecos();
            if (tokenResult != null && !tokenResult.equals("")) {

                JSONObject jsonObj = new JSONObject(tokenResult);

                String token = jsonObj.getString("token");
                //        System.out.println("Token Recibido" + token);
                obtieneCecos.getData(token);
                logger.info("*************Termina carga cecos*************");
            } else {
                logger.info("Error Carga Cecos Ws:: El servicio del Token no responde...");
            }
        } catch (Exception c) {
            c.printStackTrace();
        }
        return "";
    }
}
