package com.gruposalinas.franquicia.servicios.servidor;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gruposalinas.franquicia.business.DatosUsuarioBI;
import com.gruposalinas.franquicia.business.SesionFirmaBI;
import com.gruposalinas.franquicia.domain.DatosUsuarioDTO;
import com.gruposalinas.franquicia.resources.FRQConstantes;
import com.gruposalinas.franquicia.util.StrCipher;
import com.gruposalinas.franquicia.util.UtilCryptoGS;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/servicios")
public class Servicios {

    @Autowired
    SesionFirmaBI sesionFirmaBI;

    @Autowired
    DatosUsuarioBI datosUsuarioBI;

    private static Logger logger = LogManager.getLogger(Servicios.class);


    /*----------------------------
	/*-----------------------------------------------------------Login por Firma azteca-----------------------------------------------------------*/
    // http://localhost:8080/franquicia/servicios/validaFirmaAzteca.json?id=<?>&firma=<?>&key=<?>&flag=<?>
    @SuppressWarnings({"static-access"})
    @RequestMapping(value = "/validaFirmaAzteca", method = RequestMethod.GET)
    public @ResponseBody
    String consumeWebService(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {

        String ticketSession = "";
        String json = "";
        DatosUsuarioDTO datosUsuarioDTO = null;
        Gson g = new GsonBuilder().serializeNulls().create();

        try {
            UtilCryptoGS cifra = new UtilCryptoGS();
            StrCipher cifraIOS = new StrCipher();
            String uri = request.getQueryString();
            String uriAp = request.getParameter("token");

            int idLlave = Integer.parseInt((uriAp.split(";")[1]).split("=")[1]);
            uri = uri.split("&")[0];
            String urides = "";

            if (idLlave == 7) {
                urides = (cifraIOS.decrypt(uri, FRQConstantes.getLlaveEncripcionLocalIOS()));
            } else if (idLlave == 666) {
                urides = (cifra.decryptParams(uri));
            }

            String idUsuario = urides.split("&")[0].split("=")[1];
            String usuarioAdmin = urides.split("&")[1].split("=")[1];
            String token = urides.split("&")[2].split("=")[1];
            String key = urides.split("&")[3].split("=")[1];
            String flag = "";

            String[] usuariosP = {"515725", "722878", "203977", "196228", "189140", "191841", "189870", "189871", "191312", "331952", "643965", "664899"};

            if (urides.split("&").length == 4) {
                flag = "0";
            } else {
                flag = urides.split("&")[4].split("=")[1];
            }

            logger.info("Usuario: " + idUsuario);
            logger.info("Firma: " + token);
            logger.info("key: " + key);
            logger.info("flag: " + flag);

            /* Usuarios */
            boolean bandera = false;

            if (usuarioAdmin.compareTo("0") != 0) {
                for (int i = 0; i < usuariosP.length; i++) {
                    if (usuarioAdmin.compareTo(usuariosP[i]) == 0) {
                        bandera = true;
                        break;
                    }
                }
            }
            String ws = FRQConstantes.XML_WS_FIRMA_AZTECA;

            if (bandera) {
                ws = ws.replace("@idEmpleado", String.valueOf(usuarioAdmin));
                ws = ws.replace("@token", token);
            } else {
                ws = ws.replace("@idEmpleado", String.valueOf(idUsuario));
                ws = ws.replace("@token", token);
            }

            if (flag.equals("1")) {

                if (SesionFirmaBI.wsTokenUsuario(FRQConstantes.URL_WS_FIRMA_AZTECA, ws)) {
                    ticketSession = "TEMP";
                } else {
                    ticketSession = "ERROR";
                }
                //ticketSession = sesionFirmaBI.createSession(idUsuario, token, key);
            } else {
                //ticketSession = "TEMP";

                if (SesionFirmaBI.wsTokenUsuario(FRQConstantes.URL_WS_FIRMA_AZTECA, ws)) {
                    ticketSession = "TEMP";
                } else {
                    ticketSession = "ERROR";
                }
            }
            if (!ticketSession.equals("ERROR")) {

                datosUsuarioDTO = new DatosUsuarioDTO();
                datosUsuarioDTO = datosUsuarioBI.buscaUsuario(idUsuario);
                datosUsuarioDTO.setTicket(ticketSession);

            }

        } catch (Exception e) {
            logger.info("Ocurrio algo ");

        }

        json = "{\"datosUsuario\":" + g.toJson(datosUsuarioDTO) + "}";

        return json;
    }

}
