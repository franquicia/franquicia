package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.util.UtilString;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Base64;
import java.util.Calendar;
import javax.imageio.ImageIO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("restriction")
public class ServicioImagen {

    private static Logger logger = LogManager.getLogger(ServicioImagen.class);

    @SuppressWarnings({"static-access"})
    public boolean enviarImagen(String img, String nombreImg, String extension) {
        boolean respuesta = false;
        File r = null;
        String rootPath = File.listRoots()[0].getAbsolutePath();
        Calendar cal = Calendar.getInstance();

        //DEFINIR RUTA
        String rutaImagen = "";
        String rutaImagenPreview = "";

        //1.- :::  VERIFICAR EN DONDE ESTOY PARA GUARDAR EN ( /Sociounico/franquicia/imagenes/ - /Sociounico/franquicia/preview/ ) O EN EL NUEVO
        //COMENTO ESTO POR QUE AUN NO SE APLICA LA VERSION DE FRANQUICIA
        try {
            if (!this.verificaServidor(InetAddress.getLocalHost().getHostAddress().toString())) {
                rutaImagen = "/Sociounico/franquicia/imagenes/";
                rutaImagenPreview = "/Sociounico/franquicia/preview/";
            } else {
                //DONDE GUARDO CUANDO ESTE EN FRANQUICIA
                rutaImagen = "/franquicia/imagenes/";
                rutaImagenPreview = "/franquicia/preview/";
            }
        } catch (UnknownHostException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        String ruta = rootPath + rutaImagen + cal.getInstance().get(Calendar.YEAR) + "/";
        String rutaPreview = rootPath + rutaImagenPreview + cal.getInstance().get(Calendar.YEAR) + "/";
        // System.out.println(cal.getTime().toString());
        //System.out.println(File.listRoots()[0].getAbsolutePath());
        r = new File(ruta);

        if (r.mkdirs()) {
            logger.info("SE HA CREADA LA CARPETA");
        } else {
            logger.info("EL DIRECTORIO YA EXISTE");
        }

        FileOutputStream fos = null;
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            byte[] imgDecodificada = decoder.decode(img);
            byte[] nomDecodificado = decoder.decode(nombreImg);
            byte[] extDecodificada = decoder.decode(extension);
            // System.out.println(ruta+new String(imgDecodificada)+"."+new
            // String(extDecodificada));
            String nomDecodificadoStr = UtilString.cleanFilename(new String(nomDecodificado));
            String extDecodificadaStr = UtilString.cleanFilename(new String(extDecodificada));

            String rutaTotal = ruta + nomDecodificadoStr + "." + extDecodificadaStr;

            String rutalimpia = cleanString(rutaTotal);

            FileOutputStream fileOutput = new FileOutputStream(rutalimpia);
            try {
                BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput);
                // se escribe el archivo decodificado
                bufferOutput.write(imgDecodificada);
                bufferOutput.close();

                // procedimiento para escalar la imagen
                BufferedImage imgS = ImageIO.read(new File(rutalimpia));
                // int scaleX=(int)(img.getWidth()*factor);
                // int scaleY=(int)(img.getHeight()*factor);
                int scaleX;
                int scaleY;
                if (imgS.getHeight() > imgS.getWidth()) {
                    scaleX = 120;
                    scaleY = 160;
                } else {
                    scaleX = 160;
                    scaleY = 120;
                }
                Image imgEscala = imgS.getScaledInstance(scaleX, scaleY, Image.SCALE_SMOOTH);
                BufferedImage bufferImg = new BufferedImage(scaleX, scaleY, BufferedImage.TYPE_INT_RGB);
                bufferImg.getGraphics().drawImage(imgEscala, 0, 0, null);
                File imgSalida = new File(rutaPreview);

                if (imgSalida.mkdirs()) {
                    logger.info("SE HA CREADO LA ESTRUCTURA PARA LAS IMAGENES DE PREVIEW");
                } else {
                    logger.info("LA ESTRUCTURA DE IMAGES PREVIEW YA EXISTE");
                }

                String rutaTotal2 = rutaPreview + nomDecodificadoStr + "." + extDecodificadaStr;

                String rutalimpia2 = cleanString(rutaTotal2);

                //fos = new FileOutputStream(rutaPreview + nomDecodificadoStr + "." + extDecodificadaStr);
                fos = new FileOutputStream(rutalimpia2);

                try {
                    ImageIO.write(bufferImg, "JPG", fos);
                } finally {
                    if (fos != null) {
                        fos.close();
                    }
                }
            } finally {
                fileOutput.close();
            }
            respuesta = true;

            //AL FINALIZAR DE GUARDAR LA IMAGEN SE INICIA EL PROCESO DE REENVIO, TRUE SI ESTOY EN FRANQUICIA
            //SE COMENTA POR QUE AUN NO ESTA IMPLEMENTADO
            if (!this.verificaServidor(InetAddress.getLocalHost().getHostAddress().toString())) {
                this.enviarImagenFrq(img, nombreImg, extension);
            }
            //FIN DE PROCESO DE REENVIO
        } catch (Exception e) {
            // System.out.println("ERROR: "+e.getMessage());
            logger.info("Algo paso " + e.getMessage());
            respuesta = false;
        }

        return respuesta;

    }

    /* ******************************  METODOS PARA EL REPLICADO DE IMAGENES ****************************** */
    //TRUE CUANDO ESTOY EN FRANQUICIA
    public boolean verificaServidor(String ipActual) {
        boolean resp = false;
        String[] ipFrq = {"10.53.33.74", "10.53.33.75", "10.53.33.76", "10.53.33.77"};

        for (String data : ipFrq) {
            if (data.contains(ipActual)) {
                return true;
            }
        }
        return resp;
    }

    public String codificarBase64(byte[] cadena) {
        return new String(cadena);
    }

    /* METODO PARA ENVIAR LA IMAGEN */
    @SuppressWarnings("unused")
    public String enviarImagenFrq(String img, String nombreImagen, String extension) {
        // boolean estatus = false;
        String estatus = "-";
        String imagen;
        String nombreImg;
        String ext;

        BufferedInputStream buffers = null;
        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection rc = null;

        try {
            // se codifica en base 64
            imagen = img;
            nombreImg = nombreImagen;
            ext = extension;

            String xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                    + "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"
                    + "<soap:Body>" + "<enviarImagen xmlns=\"http://servidor.servicios.franquicia.gruposalinas.com\">"
                    + "<img>" + imagen + "</img>" + "<nombreImg>" + nombreImg + "</nombreImg>" + "<extension>" + ext
                    + "</extension>"
                    + "</enviarImagen>" + "</soap:Body>" + "</soap:Envelope>";

            //SOLO PONDRE EL WSDL DE FRANQUICIA POR QUE AQUI ENTRA SOLO SI NO ESTA EN
            //2.- :::  DEFINIR EL WSDL DE FRANQUICIA
            URL url = new URL("http://10.53.33.83:80/franquicia/services/ServicioImagen?wsdl");
            rc = (HttpURLConnection) url.openConnection();

            //SUSTITUIR POR FRANQUICIA
            //3.- :::  DEFINIR EL HOST DE FRANQUICIA
            rc.setRequestProperty("Host", "10.53.33.83");
            rc.setRequestMethod("POST");
            rc.setDoOutput(true);
            rc.setDoInput(true);
            rc.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

            wr = new OutputStreamWriter(rc.getOutputStream());
            wr.write(xml, 0, xml.length());
            wr.flush();

            rd = new BufferedReader(new InputStreamReader(rc.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                if (line.contains("true")) {
                    logger.info("ESTATUS ENVIO: SE HA ENVIADO CORRECTAMENTE LA FOTO");
                    estatus = "SUCCESS";
                } else {
                    estatus = line;
                }
            }
        } catch (Exception e) {
            logger.info("Algo paso al enviar imagen en ServicioImagen " + e.toString());
            e.printStackTrace();
            estatus = e.getMessage();
        } finally {
            try {
                if (wr != null) {
                    wr.close();
                }
            } catch (Exception e) {
                logger.info("Algo paso " + e);
            }
            try {
                if (rd != null) {
                    rd.close();
                }
            } catch (Exception e) {
                logger.info("Algo paso " + e);
            }
            try {
                if (rc != null) {
                    rc.disconnect();
                }
            } catch (Exception e) {
                logger.info("Algo paso " + e);
            }
        }
        return estatus;
    }

    /* ****************************** FIN METODOS PARA REPLICADO DE IMAGENES ****************************** */
    public static void safeClose(BufferedReader rd) {
        if (rd != null) {
            try {
                rd.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String cleanString(String aString) {
        if (aString == null) {
            return null;
        }
        String cleanString = "";
        for (int i = 0; i < aString.length(); ++i) {
            cleanString += cleanChar(aString.charAt(i));
        }
        return cleanString;
    }

    private static char cleanChar(char aChar) {

        // 0 - 9
        for (int i = 48; i < 58; ++i) {
            if (aChar == i) {
                return (char) i;
            }
        }

        // 'A' - 'Z'
        for (int i = 65; i < 91; ++i) {
            if (aChar == i) {
                return (char) i;
            }
        }

        // 'a' - 'z'
        for (int i = 97; i < 123; ++i) {
            if (aChar == i) {
                return (char) i;
            }
        }

        // other valid characters
        switch (aChar) {
            case '/':
                return '/';
            case '.':
                return '.';
            case '-':
                return '-';
            case '_':
                return '_';
            case ' ':
                return ' ';
        }
        return '%';
    }
}
