package com.gruposalinas.franquicia.servicios.servidor;

import com.google.gson.Gson;
import com.gruposalinas.franquicia.business.ArchiveroBI;
import com.gruposalinas.franquicia.business.CanalBI;
import com.gruposalinas.franquicia.business.CecoBI;
import com.gruposalinas.franquicia.business.ChecklistNegocioBI;
import com.gruposalinas.franquicia.business.EmpFijoBI;
import com.gruposalinas.franquicia.business.FiltrosCecoBI;
import com.gruposalinas.franquicia.business.FormArchiverosBI;
import com.gruposalinas.franquicia.business.GrupoBI;
import com.gruposalinas.franquicia.business.HorarioBI;
import com.gruposalinas.franquicia.business.MovilInfoBI;
import com.gruposalinas.franquicia.business.NegocioAdicionalBI;
import com.gruposalinas.franquicia.business.NegocioBI;
import com.gruposalinas.franquicia.business.NivelBI;
import com.gruposalinas.franquicia.business.NotificacionBI;
import com.gruposalinas.franquicia.business.OrdenGrupoBI;
import com.gruposalinas.franquicia.business.PaisBI;
import com.gruposalinas.franquicia.business.PaisNegocioBI;
import com.gruposalinas.franquicia.business.ParametroBI;
import com.gruposalinas.franquicia.business.PerfilBI;
import com.gruposalinas.franquicia.business.PerfilUsuarioBI;
import com.gruposalinas.franquicia.business.PeriodoBI;
import com.gruposalinas.franquicia.business.PosibleTipoPreguntaBI;
import com.gruposalinas.franquicia.business.PosiblesBI;
import com.gruposalinas.franquicia.business.PuestoBI;
import com.gruposalinas.franquicia.business.RecursoBI;
import com.gruposalinas.franquicia.business.RecursoPerfilBI;
import com.gruposalinas.franquicia.business.StatusFormArchiverosBI;
import com.gruposalinas.franquicia.business.SucursalBI;
import com.gruposalinas.franquicia.business.SucursalesCercanasBI;
import com.gruposalinas.franquicia.business.TareasBI;
import com.gruposalinas.franquicia.business.TipoArchivoBI;
import com.gruposalinas.franquicia.business.Usuario_ABI;
import com.gruposalinas.franquicia.domain.ArchiveroDTO;
import com.gruposalinas.franquicia.domain.CanalDTO;
import com.gruposalinas.franquicia.domain.CecoDTO;
import com.gruposalinas.franquicia.domain.ChecklistNegocioDTO;
import com.gruposalinas.franquicia.domain.EmpFijoDTO;
import com.gruposalinas.franquicia.domain.FiltrosCecoDTO;
import com.gruposalinas.franquicia.domain.FormArchiverosDTO;
import com.gruposalinas.franquicia.domain.GrupoDTO;
import com.gruposalinas.franquicia.domain.HorarioDTO;
import com.gruposalinas.franquicia.domain.MovilInfoDTO;
import com.gruposalinas.franquicia.domain.NegocioAdicionalDTO;
import com.gruposalinas.franquicia.domain.NegocioDTO;
import com.gruposalinas.franquicia.domain.NivelDTO;
import com.gruposalinas.franquicia.domain.OrdenGrupoDTO;
import com.gruposalinas.franquicia.domain.PaisDTO;
import com.gruposalinas.franquicia.domain.PaisNegocioDTO;
import com.gruposalinas.franquicia.domain.ParametroDTO;
import com.gruposalinas.franquicia.domain.PerfilDTO;
import com.gruposalinas.franquicia.domain.PerfilUsuarioDTO;
import com.gruposalinas.franquicia.domain.PeriodoDTO;
import com.gruposalinas.franquicia.domain.PosiblesDTO;
import com.gruposalinas.franquicia.domain.PosiblesTipoPreguntaDTO;
import com.gruposalinas.franquicia.domain.PuestoDTO;
import com.gruposalinas.franquicia.domain.RecursoDTO;
import com.gruposalinas.franquicia.domain.RecursoPerfilDTO;
import com.gruposalinas.franquicia.domain.StatusFormArchiveroDTO;
import com.gruposalinas.franquicia.domain.SucursalDTO;
import com.gruposalinas.franquicia.domain.SucursalesCercanasDTO;
import com.gruposalinas.franquicia.domain.TareaDTO;
import com.gruposalinas.franquicia.domain.TipoArchivoDTO;
import com.gruposalinas.franquicia.domain.Usuario_ADTO;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.ESAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

@Controller
@RequestMapping("/consultaCatalogosService")
public class ConsultaCatalogosService {

    @Autowired
    CanalBI canalbi;
    @Autowired
    PerfilBI perfilbi;
    @Autowired
    PuestoBI puestobi;
    @Autowired
    TipoArchivoBI tipoarchivobi;
    @Autowired
    CecoBI cecobi;
    @Autowired
    SucursalBI sucursalbi;
    @Autowired
    Usuario_ABI usuarioabi;
    @Autowired
    PaisBI paisbi;
    @Autowired
    HorarioBI horariobi;
    @Autowired
    ParametroBI parametrobi;
    @Autowired
    NegocioBI negociobi;
    @Autowired
    NivelBI nivelbi;
    @Autowired
    PerfilUsuarioBI perfilusuariobi;
    @Autowired
    RecursoPerfilBI recursoperfilbi;
    @Autowired
    RecursoBI recursobi;
    @Autowired
    PosiblesBI posiblesbi;
    @Autowired
    PosibleTipoPreguntaBI posibletipopreguntabi;
    @Autowired
    FiltrosCecoBI filtrosCecosBI;
    @Autowired
    ChecklistNegocioBI checklistNegocioBI;
    @Autowired
    NegocioAdicionalBI adicionalBI;
    @Autowired
    PaisNegocioBI paisNegocioBI;
    @Autowired
    SucursalesCercanasBI sucursalesCercanasBI;
    @Autowired
    NotificacionBI notificacionBI;
    @Autowired
    MovilInfoBI movilInfoBI;
    @Autowired
    GrupoBI grupoBI;
    @Autowired
    OrdenGrupoBI ordenGrupoBI;
    @Autowired
    PeriodoBI periodoBI;
    @Autowired
    TareasBI tareasBI;
    @Autowired
    EmpFijoBI empFijoBI;
    @Autowired
    StatusFormArchiverosBI statusFormArchiverosBI;
    @Autowired
    FormArchiverosBI formArchiverosBI;
    @Autowired
    ArchiveroBI archiveroBI;

    private static final Logger logger = LogManager.getLogger(ConsultaCatalogosService.class);

    // http://localhost:8080/franquicia/consultaCatalogosService/getCanales.json?activo=<?>
    @RequestMapping(value = "/getCanales", method = RequestMethod.GET)
    public ModelAndView getCanales(HttpServletRequest request, HttpServletResponse response) {
        try {
            String activo = request.getParameter("activo");

            List<CanalDTO> lista = canalbi.buscaCanales(Integer.parseInt(activo));

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA CANALES");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getPerfiles.json
    @RequestMapping(value = "/getPerfiles", method = RequestMethod.GET)
    public ModelAndView getPerfiles(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<PerfilDTO> lista = perfilbi.obtienePerfil();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PERFILES");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getPuestos.json
    @RequestMapping(value = "/getPuestos", method = RequestMethod.GET)
    public ModelAndView getPuestos(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<PuestoDTO> lista = puestobi.obtienePuesto();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "PUESTOS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getTiposArchivo.json
    @RequestMapping(value = "/getTiposArchivo", method = RequestMethod.GET)
    public ModelAndView getTiposArchivo(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<TipoArchivoDTO> lista = tipoarchivobi.obtieneTipoArchivo();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA TIPOS ARCHIVO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getCecos.json?activo=<?>
    @RequestMapping(value = "/getCecos", method = RequestMethod.GET)
    public ModelAndView getCecos(HttpServletRequest request, HttpServletResponse response) {
        try {
            String activo = request.getParameter("activo");

            List<CecoDTO> lista = cecobi.buscaCecos(Integer.parseInt(activo));

            // ModelAndView mv = new ModelAndView("muestraServicios");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "LISTA CECOS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getSucursales.json
    @RequestMapping(value = "/getSucursales", method = RequestMethod.GET)
    public ModelAndView getSucursales2(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<SucursalDTO> lista = sucursalbi.obtieneSucursal();

            // ModelAndView mv = new ModelAndView("muestraServicios");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "LISTA SUCURSALES");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getUsuarios.json
    @RequestMapping(value = "/getUsuarios", method = RequestMethod.GET)
    public ModelAndView getUsuarios(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<Usuario_ADTO> lista = usuarioabi.obtieneUsuario();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA USUARIOS A");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getPaises.json
    @RequestMapping(value = "/getPaises", method = RequestMethod.GET)
    public ModelAndView getPaises(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<PaisDTO> lista = paisbi.obtienePais();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PAISES");
            mv.addObject("res", lista);
            return mv;

        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getHorarios.json
    @RequestMapping(value = "/getHorarios", method = RequestMethod.GET)
    public ModelAndView getHorarios(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<HorarioDTO> lista = horariobi.obtieneHorario();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA HORARIOS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getParametros.json
    @RequestMapping(value = "/getParametros", method = RequestMethod.GET)
    public ModelAndView getParametros(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<ParametroDTO> lista = parametrobi.obtieneParametro();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PARAMETROS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getNegocios.json
    @RequestMapping(value = "/getNegocios", method = RequestMethod.GET)
    public ModelAndView getNegocios(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<NegocioDTO> lista = negociobi.obtieneNegocio();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA NEGOCIOS");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getNiveles.json
    @RequestMapping(value = "/getNiveles", method = RequestMethod.GET)
    public ModelAndView getNiveles(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<NivelDTO> lista = nivelbi.obtieneNivel();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA NIVELES");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getSucursalById.json?idSuc=<?>
    @RequestMapping(value = "/getSucursalById", method = RequestMethod.GET)
    public ModelAndView getSucursalById(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idSuc = request.getParameter("idSuc");

            List<SucursalDTO> lista = sucursalbi.obtieneSucursal(idSuc);

            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
            mv.addObject("tipo", "SUCURSAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getSucursalByIdGCC.json?idSuc=<?>
    @RequestMapping(value = "/getSucursalByIdGCC", method = RequestMethod.GET)
    public ModelAndView getSucursalByIdGCC(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idSuc = request.getParameter("idSuc");

            List<SucursalDTO> lista = sucursalbi.obtieneSucursalGCC(idSuc);

            // ModelAndView mv = new ModelAndView("muestraServicios");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "SUCURSAL GCC");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getSucursalPaso.json?idSuc=<?>
    @RequestMapping(value = "/getSucursalPaso", method = RequestMethod.GET)
    public ModelAndView getSucursalPaso(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idSuc = request.getParameter("idSuc");

            List<SucursalDTO> lista = sucursalbi.obtieneSucursalPaso(idSuc);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "SUCURSAL");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getCecoById.json?idCeco=<?>
    @RequestMapping(value = "/getCecoById", method = RequestMethod.GET)
    public ModelAndView getCecoById(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idCeco = request.getParameter("idCeco");

            List<CecoDTO> lista = cecobi.buscaCeco(Integer.parseInt(idCeco));

            // ModelAndView mv = new ModelAndView("muestraServicios");
            ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());

            mv.addObject("tipo", "OBTIENE CECO POR IDCECO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getUsuarioById.json?idUsu=<?>
    @RequestMapping(value = "/getUsuarioById", method = RequestMethod.GET)
    public ModelAndView getUsuarioById(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsu = request.getParameter("idUsu");

            List<Usuario_ADTO> lista = usuarioabi.obtieneUsuario(Integer.parseInt(idUsu));

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "USUARIO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getCargaSucursal.json
    @RequestMapping(value = "/getCargaSucursal", method = RequestMethod.GET)
    public ModelAndView getCargaSucursal(HttpServletRequest request, HttpServletResponse response) {
        try {
            boolean res = sucursalbi.cargaSucursales();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "SUCURSALES Cargadas Correctamente -->");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getCargaUsuariosPaso.json
    @RequestMapping(value = "/getCargaUsuariosPaso", method = RequestMethod.GET)
    public ModelAndView getCargaUsuariosPaso(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<Usuario_ADTO> res = usuarioabi.obtieneUsuarioPaso();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "USUARIOS PASO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getCargaUsuariosPasoByParams.json?idUsu=<?>&idPuesto=<?>&idCeco=<?>
    @RequestMapping(value = "/getCargaUsuariosPasoByParams", method = RequestMethod.GET)
    public ModelAndView getCargaUsuariosPasoByParams(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idUsu = request.getParameter("idUsu");
            String idPuesto = request.getParameter("idPuesto");
            String idCeco = request.getParameter("idCeco");

            List<Usuario_ADTO> res = usuarioabi.obtieneUsuarioPaso(idUsu, idPuesto, idCeco);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "USUARIOS PASO PARAMS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getCecosPaso.json
    @RequestMapping(value = "/getCecosPaso", method = RequestMethod.GET)
    public ModelAndView getCecosPaso(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<CecoDTO> res = cecobi.buscaCecosPaso();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CECO PASO");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getCecosPasoByParams.json?ceco=<?>&cecoPadre=<?>&desc=<?>
    @RequestMapping(value = "/getCecosPasoByParams", method = RequestMethod.GET)
    public ModelAndView getCecosPasoByParams(HttpServletRequest request, HttpServletResponse response)
            throws UnsupportedEncodingException {
        try {
            String ceco = request.getParameter("ceco");
            String cecoPadre = request.getParameter("cecoPadre");
            String descripcion = new String(request.getParameter("desc").getBytes("ISO-8859-1"), "UTF-8");

            List<CecoDTO> res = cecobi.buscaCecoPaso(ceco, cecoPadre, descripcion);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "CECO PASO PARAMS");
            mv.addObject("res", res);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getRecursosPerfil.json?idRecurso=<?>&idPerfil=<?>
    @RequestMapping(value = "/getRecursosPerfil", method = RequestMethod.GET)
    public ModelAndView getRecursosPerfil(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idRecurso = request.getParameter("idRecurso");
            String idPerfil = request.getParameter("idPerfil");

            List<RecursoPerfilDTO> res = recursoperfilbi.buscaRecursoP(idRecurso, idPerfil);
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "GET RECURSOS PERFIL");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getPerfilUsuarios.json?idUsuario=<?>&idPerfil=<?>
    @RequestMapping(value = "/getPerfilUsuarios", method = RequestMethod.GET)
    public ModelAndView getPerfilUsuarios(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idUsuario = request.getParameter("idUsuario");
            String idPerfil = request.getParameter("idPerfil");

            List<PerfilUsuarioDTO> res = perfilusuariobi.obtienePerfiles(idUsuario, idPerfil);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "PERFILES USUARIO");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }

    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getRecursos.json?idRecurso=<?>
    @RequestMapping(value = "/getRecursos", method = RequestMethod.GET)
    public ModelAndView getRecursos(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idRecurso = request.getParameter("idRecurso");

            List<RecursoDTO> res = recursobi.buscaRecurso(idRecurso);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "RECURSOS");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getPosibles.json
    @RequestMapping(value = "/getPosibles", method = RequestMethod.GET)
    public ModelAndView getPosibles(HttpServletRequest request, HttpServletResponse response) {

        try {

            List<PosiblesDTO> res = posiblesbi.buscaPosible();

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "POSIBLES ");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getPosibleTipoPregunta.json?idTipoPregunta=<?>
    @RequestMapping(value = "/getPosibleTipoPregunta", method = RequestMethod.GET)
    public ModelAndView getPosibleTipoPregunta(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idTipoPregunta = request.getParameter("idTipoPregunta");

            if (idTipoPregunta.equals("")) {
                idTipoPregunta = null;
            }

            List<PosiblesTipoPreguntaDTO> res = posibletipopreguntabi.obtienePosiblesRespuestas(idTipoPregunta);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "POSIBLES TIPO PREGUNTA");
            mv.addObject("res", res);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getPosibleTipoPreguntaService.json?idTipoPregunta=<?>
    @RequestMapping(value = "/getPosibleTipoPreguntaService", method = RequestMethod.GET)
    public @ResponseBody
    List<PosiblesTipoPreguntaDTO> getPosibleTipoPreguntaService(HttpServletRequest request,
            HttpServletResponse response) {

        try {
            String idTipoPregunta = request.getParameter("idTipoPregunta");

            if (idTipoPregunta.equals("")) {
                idTipoPregunta = null;
            }

            List<PosiblesTipoPreguntaDTO> res = posibletipopreguntabi.obtienePosiblesRespuestas(idTipoPregunta);

            return res;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getFiltrosCecos.json?idUsuario=<?>
    @RequestMapping(value = "/getFiltrosCecos", method = RequestMethod.GET)
    public @ResponseBody
    Map<String, Object> getFiltrosCecos(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idUsuario = request.getParameter("idUsuario");

            Map<String, Object> res = filtrosCecosBI.obtieneFiltros(Integer.parseInt(idUsuario));

            return res;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getCecosCombo.json?idCeco=<?>&tipoBusqueda=<?>
    @RequestMapping(value = "/getCecosCombo", method = RequestMethod.GET)
    public @ResponseBody
    List<FiltrosCecoDTO> getCecosCombo(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idCeco = request.getParameter("idCeco");
            String tipoBusqueda = request.getParameter("tipoBusqueda");

            List<FiltrosCecoDTO> res = filtrosCecosBI.obtieneCecos(Integer.parseInt(idCeco),
                    Integer.parseInt(tipoBusqueda));

            return res;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getPaisesCombo.json?idNegocio=<?>
    @RequestMapping(value = "/getPaisesCombo", method = RequestMethod.GET)
    public @ResponseBody
    List<PaisDTO> getPaisesCombo(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idNegocio = request.getParameter("idNegocio");

            List<PaisDTO> res = filtrosCecosBI.obtienePaises(Integer.parseInt(idNegocio));

            return res;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getTerritoriosCombo.json?idPais=<?>&idNegocio=<?>
    @RequestMapping(value = "/getTerritoriosCombo", method = RequestMethod.GET)
    public @ResponseBody
    List<FiltrosCecoDTO> getTerritoriosCombo(HttpServletRequest request,
            HttpServletResponse response) {

        try {
            String idPais = request.getParameter("idPais");
            String idNegocio = request.getParameter("idNegocio");

            List<FiltrosCecoDTO> res = filtrosCecosBI.obtieneTerritorios(Integer.parseInt(idPais),
                    Integer.parseInt(idNegocio));

            return res;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getChecklistNegocio.json?idChecklist=<?>&idNegocio=<?>
    @RequestMapping(value = "/getChecklistNegocio", method = RequestMethod.GET)
    public ModelAndView getChecklistNegocio(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idChecklist = request.getParameter("idChecklist");
            String idNegocio = request.getParameter("idNegocio");

            List<ChecklistNegocioDTO> lista = checklistNegocioBI.obtieneChecklistNegocio(idChecklist, idNegocio);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA CHECKLIST NEGOCIO");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getNegocioAdicional.json?idUsuario=<?>&idNegocio=<?>
    @RequestMapping(value = "/getNegocioAdicional", method = RequestMethod.GET)
    public ModelAndView getNegocioAdicional(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idUsuario = request.getParameter("idUsuario");
            String idNegocio = request.getParameter("idNegocio");

            List<NegocioAdicionalDTO> lista = adicionalBI.obtieneNegocioAdicional(idUsuario, idNegocio);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA NEGOCIO ADICIONAL");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getNegocioPais.json?idPais=<?>&idNegocio=<?>
    @RequestMapping(value = "/getNegocioPais", method = RequestMethod.GET)
    public ModelAndView getNegocioPais(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idPais = request.getParameter("idPais");
            String idNegocio = request.getParameter("idNegocio");

            List<PaisNegocioDTO> lista = paisNegocioBI.obtienePaisNegocio(idNegocio, idPais);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PAIS NEGOCIO");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getSucursalesCercanas.json?latitud=<?>&longitud=<?>
    @RequestMapping(value = "/getSucursalesCercanas", method = RequestMethod.GET)
    public @ResponseBody
    List<SucursalesCercanasDTO> getSucursalesCercanas(HttpServletRequest request,
            HttpServletResponse response) {

        try {
            String latitud = request.getParameter("latitud");
            String longitud = ESAPI.encoder().decodeFromURL(ESAPI.encoder().canonicalize(request.getParameter("longitud")));

            List<SucursalesCercanasDTO> lista = sucursalesCercanasBI.obtieneSucursales(latitud, longitud);

            return lista;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getNotificacion.json?idUsuario=<?>
    @RequestMapping(value = "/getNotificacion", method = RequestMethod.GET)
    public ModelAndView getNotificacion(HttpServletRequest request, HttpServletResponse response) {

        Map<String, Object> listaNotificacion = null;
        try {
            String idUsuario = request.getParameter("idUsuario");
            Gson g = new Gson();

            listaNotificacion = notificacionBI.NotificacionPorcentaje(Integer.parseInt(idUsuario));

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "NOTIFICACION REGIONAL");
            mv.addObject("res", g.toJson(listaNotificacion));
            // mv.addObject("res", "SE HA RECIBIDO NOTIFICACION");

            logger.info("JSON: " + g.toJson(listaNotificacion));

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getUsuaNotificacion.json?so=<?>
    @SuppressWarnings("unused")
    @RequestMapping(value = "/getUsuaNotificacion", method = RequestMethod.GET)
    public ModelAndView getUsuaNotificacion(HttpServletRequest request, HttpServletResponse response) {

        List<MovilInfoDTO> listaNotificacion = null;
        try {
            String so = request.getParameter("so");
            Gson g = new Gson();

            listaNotificacion = notificacionBI.obtieneUsuariosNoti(so);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA NOTIFICACION");
            mv.addObject("res", listaNotificacion);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getMovilInfo.json?idUsuario=<?>
    @RequestMapping(value = "/getMovilInfo", method = RequestMethod.GET)
    public ModelAndView getMovilInfo(HttpServletRequest request, HttpServletResponse response) {

        try {
            String idUsuario = request.getParameter("idUsuario");

            MovilInfoDTO movil = new MovilInfoDTO();
            movil.setIdUsuario(Integer.parseInt(idUsuario));

            List<MovilInfoDTO> lista = movilInfoBI.obtieneInfoMovil(movil);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA MOVIL");
            mv.addObject("res", lista);

            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getGrupo.json?idGrupo=<?>
    @RequestMapping(value = "/getGrupo", method = RequestMethod.GET)
    public ModelAndView getGrupo(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idGrupo = request.getParameter("idGrupo");

            List<GrupoDTO> lista = grupoBI.obtieneGrupo(idGrupo);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA GRUPO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getOrdenGrupo.json?=idOrdenGrupo=<?>&idChecklist=<?>&idGrupo=<?>
    @RequestMapping(value = "/getOrdenGrupo", method = RequestMethod.GET)
    public ModelAndView getOrdenGrupo(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idOrdenGrupo = request.getParameter("idOrdenGrupo");
            String idChecklist = request.getParameter("idChecklist");
            String idGrupo = request.getParameter("idGrupo");

            List<OrdenGrupoDTO> lista = ordenGrupoBI.obtieneOrdenGrupo(idOrdenGrupo, idChecklist, idGrupo);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA ORDEN GRUPO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getPeriodo.json?idPeriodo=<?>
    @RequestMapping(value = "/getPeriodo", method = RequestMethod.GET)
    public ModelAndView getPeriodo(HttpServletRequest request, HttpServletResponse response) {
        try {
            String idPeriodo = request.getParameter("idPeriodo");

            List<PeriodoDTO> lista = periodoBI.obtienePeriodo(idPeriodo);

            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA PERIODO");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getObtieneTareas.json
    @RequestMapping(value = "/getObtieneTareas", method = RequestMethod.GET)
    public ModelAndView getObtieneTareas(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<TareaDTO> listaTareas = tareasBI.obtieneTareas();

            logger.info("listaTareas " + listaTareas);
            ModelAndView mv = new ModelAndView("muestraServicios");
            mv.addObject("tipo", "LISTA DE TAREAS");
            mv.addObject("res", listaTareas);
            return mv;
        } catch (Exception e) {
            logger.info(e);
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getObtieneEmpFijo.json?idUsuario=<?>
    @RequestMapping(value = "/getObtieneEmpFijo", method = RequestMethod.GET)
    public ModelAndView getObtieneEmpFijo(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idusu = 0;
            if (request.getParameter("idUsuario") != null) {
                idusu = Integer.parseInt(request.getParameter("idUsuario"));
            }
            List<EmpFijoDTO> lista = empFijoBI.obtieneDatos(idusu);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Fijo");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio Algo");
            return null;
        }

    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getObtieneEmpleadosFijos.json
    @RequestMapping(value = "/getObtieneEmpleadosFijos", method = RequestMethod.GET)
    public ModelAndView getObtieneEmpleadosFijos(HttpServletRequest request, HttpServletResponse response) {
        try {
            List<EmpFijoDTO> lista = empFijoBI.obtieneInfo();
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Fijo");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getObtieneStatusFormArchiveros.json?idStatus=<?>
    @RequestMapping(value = "/getObtieneStatusFormArchiveros", method = RequestMethod.GET)
    public ModelAndView getObtieneStatusFormArchiveros(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idStatus = 0;
            String idStatusS = request.getParameter("idStatus");
            if (idStatusS != null) {
                idStatus = Integer.parseInt(idStatusS);
            }

            List<StatusFormArchiveroDTO> lista = statusFormArchiverosBI.obtieneArchiveros(idStatus);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Status");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getObtieneFormArchiveros.json?idFormArchiveros=<?>
    @RequestMapping(value = "/getObtieneFormArchiveros", method = RequestMethod.GET)
    public ModelAndView getObtieneFormArchiveros(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idFormArchiveros = 0;
            String idFormArchiverosS = request.getParameter("idFormArchiveros");
            if (idFormArchiverosS != null) {
                idFormArchiveros = Integer.parseInt(idFormArchiverosS);
            }

            List<FormArchiverosDTO> lista = formArchiverosBI.obtieneArchiveros(idFormArchiveros);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "FormatoArch");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getObtieneFormArchiverosCeco.json?idCeco=<?>
    @RequestMapping(value = "/getObtieneFormArchiverosCeco", method = RequestMethod.GET)
    public ModelAndView getObtieneFormArchiverosCeco(HttpServletRequest request, HttpServletResponse response) {
        try {

            String idCecoS = request.getParameter("idCeco");

            List<FormArchiverosDTO> lista = formArchiverosBI.obtieneFormCeco(idCecoS);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "FormatoArch");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getObtieneArchivero.json?idArchivero=<?>
    @RequestMapping(value = "/getObtieneArchivero", method = RequestMethod.GET)
    public ModelAndView getObtieneArchivero(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idArchivero = 0;
            String idArchiveroS = request.getParameter("idArchivero");
            if (idArchiveroS != null) {
                idArchivero = Integer.parseInt(idArchiveroS);
            }

            List<ArchiveroDTO> lista = archiveroBI.obtieneArchivero(idArchivero);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Archivero");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio Algo");
            return null;
        }
    }

    // http://localhost:8080/franquicia/consultaCatalogosService/getObtieneArchiveroFormato.json?idFormato=<?>
    @RequestMapping(value = "/getObtieneArchiveroFormato", method = RequestMethod.GET)
    public ModelAndView getObtieneArchiveroFormato(HttpServletRequest request, HttpServletResponse response) {
        try {
            int idFormato = 0;
            String idFormatoS = request.getParameter("idFormato");
            if (idFormatoS != null) {
                idFormato = Integer.parseInt(idFormatoS);
            }

            List<ArchiveroDTO> lista = archiveroBI.obtieneArchiveroFormato(idFormato);
            ModelAndView mv = new ModelAndView("muestraServicios");
            logger.info(lista);
            mv.addObject("tipo", "Archivero");
            mv.addObject("res", lista);
            return mv;
        } catch (Exception e) {
            logger.info("Ocurrio Algo");
            return null;
        }
    }

}
