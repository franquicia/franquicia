package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.LlaveBI;
import com.gruposalinas.franquicia.domain.LlaveDTO;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/consultaLlavesService")
public class ConsultaLlavesService {

    @Autowired
    LlaveBI llavebi;

    // http://localhost:8080/franquicia/consultaLlavesService/getLlaves.json
    @RequestMapping(value = "/getLlaves", method = RequestMethod.GET)
    public ModelAndView getLlaves(HttpServletRequest request, HttpServletResponse response) {

        List<LlaveDTO> lista = llavebi.obtieneLlave();

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "LISTA LLAVES");
        mv.addObject("res", lista);
        return mv;
    }

    // http://localhost:8080/franquicia/consultaLlavesService/getLlave.json?idLlave=<?>
    @RequestMapping(value = "/getLlave", method = RequestMethod.GET)
    public ModelAndView getLlave(HttpServletRequest request, HttpServletResponse response) {
        String idLlave = request.getParameter("idLlave");

        List<LlaveDTO> lista = llavebi.obtieneLlave(Integer.parseInt(idLlave));

        ModelAndView mv = new ModelAndView("muestraServicios");
        mv.addObject("tipo", "LLAVE");
        mv.addObject("res", lista);
        return mv;
    }

}
