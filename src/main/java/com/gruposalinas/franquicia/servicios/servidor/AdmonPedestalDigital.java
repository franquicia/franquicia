package com.gruposalinas.franquicia.servicios.servidor;

import com.gruposalinas.franquicia.business.PedestalDigitalBI;
import com.gruposalinas.franquicia.domain.AdminMenuPDDTO;
import com.gruposalinas.franquicia.domain.AdminParametroPDDTO;
import com.gruposalinas.franquicia.domain.AdminTipoDocPDDTO;
import com.gruposalinas.franquicia.domain.AdmonEstatusTabletaPDDTO;
import com.gruposalinas.franquicia.domain.BachDescargasPDDTO;
import com.gruposalinas.franquicia.domain.CanalDTO;
import com.gruposalinas.franquicia.domain.NegocioDTO;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("central/pedestalDigital")
public class AdmonPedestalDigital {

    @Autowired
    PedestalDigitalBI pd;

    @RequestMapping(value = "/adminNegocio", method = RequestMethod.GET)
    public @ResponseBody
    void adminNegocioPedestalDigital(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {
        NegocioDTO negocio = new NegocioDTO();

//		Administracion de negocio, OPCIONES: 
//		0->Inserta...
//		1->Actualiza... 
//		2->Obtiene(si das el id de negocio solo te obtiene ese negocio, si no te trae todo.
        int opcion = 0;

        negocio.setActivo(1);
        negocio.setDescripcion("new");
        negocio.setIdNegocio(5);

        pd.admonNegocio(negocio, opcion);

    }

    @RequestMapping(value = "/adminCanal", method = RequestMethod.GET)
    public @ResponseBody
    void adminCanalPedestalDigital(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {
        CanalDTO canal = new CanalDTO();

//		Administracion de negocio, OPCIONES: 
//		0->Inserta...
//		1->Actualiza... 
//		2->Obtiene(si das el id de negocio solo te obtiene ese negocio, si no te trae todo.
        int opcion = 2;

        canal.setIdCanal(104);
        canal.setDescrpicion("canal004");
        canal.setActivo(1);

        pd.admonCanal(canal, opcion);
    }

    @RequestMapping(value = "/adminParametro", method = RequestMethod.GET)
    public @ResponseBody
    void adminParametroPedestalDigital(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {
        AdminParametroPDDTO parametros = new AdminParametroPDDTO();

//		Administracion de negocio, OPCIONES: 
//		0->Inserta... no insertes el idParametro, para que lleve secuencia en la base
//		1->Actualiza... 
//		2->Obtiene(si das el id de negocio solo te obtiene ese negocio, si no te trae todo.
        int opcion = 1;

        parametros.setIdParametro(101);
        parametros.setParametro("101");
        parametros.setDescParametro("101");
        parametros.setReferencia("101");
        parametros.setValor("101");
        parametros.setActivo(1);

        pd.admonParametros(parametros, opcion);
    }

    @RequestMapping(value = "/adminMenu", method = RequestMethod.GET)
    public @ResponseBody
    void adminPerfilPedestalDigital(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {
        AdminMenuPDDTO menu = new AdminMenuPDDTO();

//		  0-> inserta menú 
//		  1-> actualiza menú  
//		  2-> selecciona menú
//        3-> inserta perfil con menú 
//        4-> actualiza activo del perfil con menú 
//        5-> selecciona menú y perfil
        int opcion = 2;

        menu.setIdMenu(300);				//obligatoriio no hay secuencia
        menu.setDescripcion("menu100");		//solo menu
        menu.setNivel(300);					//solo menu
        menu.setActivo(1);					//ambas tablas
        menu.setDependeDe(300);				//solo menu
        menu.setIdPerfil(300);				//ambas tablas

        pd.admonMenu(menu, opcion);
    }

    @RequestMapping(value = "/adminInsertTipoDoc", method = RequestMethod.GET)
    public @ResponseBody
    void adminInsertTipoDocPedestalDigital(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTipoDoc") int idTipoDoc,
            @RequestParam("nombre") String nombreDoc,
            @RequestParam("nivel") int nivel,
            @RequestParam("depende de") int dependeDe,
            @RequestParam("activo") int activo) throws NumberFormatException, Exception {
        AdminTipoDocPDDTO tipoDoc = new AdminTipoDocPDDTO();

//		  0-> inserta tipoDc 
//		  1-> actualiza tipoDc  
        int opcion = 2;

        tipoDoc.setIdTipoDoc(idTipoDoc);
        tipoDoc.setNombre(nombreDoc);
        tipoDoc.setNivel(nivel);
        tipoDoc.setActivo(activo);
        tipoDoc.setDependeDe(dependeDe);

        pd.admonTipoDoc(tipoDoc, opcion);
    }

    @RequestMapping(value = "/adminConsultaTipoDoc", method = RequestMethod.GET)
    public @ResponseBody
    void adminConsultaTipoDocPedestalDigital(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTipoDoc") int idTipoDoc) throws NumberFormatException, Exception {
        AdminTipoDocPDDTO tipoDoc = new AdminTipoDocPDDTO();

//		  2-> selecciona tipoDc
        int opcion = 0;

        tipoDoc.setIdTipoDoc(305);
        tipoDoc.setNombre("Nombre de tipooooo");
        tipoDoc.setNivel(2);
        tipoDoc.setDependeDe(2);
        tipoDoc.setActivo(1);

        pd.admonTipoDoc(tipoDoc, opcion);
    }

    @RequestMapping(value = "/adminEstadusTableta", method = RequestMethod.GET)
    public @ResponseBody
    void adminCatalogoEstatusTabletaPedestalDigital(HttpServletRequest request, HttpServletResponse response,
            @RequestParam("idTipoDoc") int idTipoDoc) throws NumberFormatException, Exception {
        AdmonEstatusTabletaPDDTO estatus = new AdmonEstatusTabletaPDDTO();

//		  2-> selecciona tipoDc
        int opcion = 1;

        estatus.setIdEstadoTableta(46);
        estatus.setDependeDe(19);
        estatus.setDescJSON("300");
        estatus.setDescWEB("300");
        estatus.setVisible("N");
        estatus.setIdStatus(3);
        estatus.setActivo(1);

        pd.admonEstatusTablet(estatus, opcion);
    }

    @RequestMapping(value = "/admindescarga", method = RequestMethod.GET)
    public @ResponseBody
    void adminDescargaPedestalDigital(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {
        BachDescargasPDDTO descarga = new BachDescargasPDDTO();

        int opcion = 2;

        descarga.setFechaActu("26062019");
        descarga.setTipoDescarga("D");

        descarga = pd.admonDescarga(descarga, opcion);
    }

    @RequestMapping(value = "/admindescargas", method = RequestMethod.GET)
    public @ResponseBody
    void adminDescargasPedestalDigital(HttpServletRequest request, HttpServletResponse response) throws NumberFormatException, Exception {
        List<BachDescargasPDDTO> descarga = new ArrayList<BachDescargasPDDTO>();
        BachDescargasPDDTO desc = new BachDescargasPDDTO();
        int opcion = 2;

        desc.setFechaActu("24062019");
        desc.setTipoDescarga("A");

        descarga = pd.admonDesc(desc, opcion);
    }
}
