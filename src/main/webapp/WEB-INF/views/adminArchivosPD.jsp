<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<script type="text/javascript">
		var contextPath = '${pageContext.request.contextPath}';
		var _repoUrl = '${repoUrl}';
		var _containerT, _nivelT;
		var _containerP, _nivelP;
		var _containerC, _nivelC;
		var _cecos, _negocio, _boxId;
		var _objBackup = '';
		var _objNameBackup = '';
		var exportData;
		var _jsonData;
		var _backupHTML;

		$(document).ready(function() {
			_negocio = 41; // Pruebas

			$('#btnTerritorio').click(function(e) {
				_nivelT = 0;
				_cecos = '0';
				_boxId = 1;
				getAdminTerritorioList();
			});

			$('#btnPais').click(function(e) {
				_nivelP = 0;
				_cecos = '0';
				_boxId = 2;
				getAdminPaisList();
			});

			$('#btnCategoria').click(function(e) {
				_nivelC = 1;
				_cecos = '0';
				_boxId = 3;
				getAdminCategoriaList();
			});

			$('#exportarFolios').on('click', exportarTablaFolios);
			$('#buscarPorFolio').on('keypress', function(e) {
				if (e.which === 13) {
					var val = $('#buscarPorFolio').val();
					var list = $('#listaLanzamientos').find('.divEliminar');
					if (list.length > 0) {
						for (var x = 0; x < list.length; x++) {
							var pass = true;
							var id = $(list[x]).attr('id');
							if (id === val) {
								pass = false;
								break;
							}
						}

						if (pass) {
							$('#listaLanzamientos').prepend(
								'<div id="' + val + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1% 5px 1%;">' +
								'<a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">Folio: ' + val + '</a></div>'
							);
						}
					}
				}
			});

			$('#buscarPorFolio').on('keypress', function(e) {
				if (!/^\d*$/.test(e.key)) {
					return false;
				}
			});

			$("#buscarPorSucursal").prop('maxLength', 4);
			$('#buscarPorSucursal').on('keypress', function(e) {
				if (e.which === 13) {
					var val = $('#buscarPorSucursal').val();
					var list = $('#listaLanzamientos').find('.divEliminar');
					if (list.length > 0) {
						for (var x = 0; x < list.length; x++) {
							var pass = true;
							var id = $(list[x]).attr('id');
							if (id === val) {
								pass = false;
								break;
							}
						}

						if (pass) {
							$('#listaLanzamientos').prepend(
								'<div id="' + val + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1% 5px 1%;">' +
								'<a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">Sucursal: ' + val + '</a></div>'
							);
						}
					}
				} else {
					if (/^\d*$/.test(e.key)) {
						return true;
					} else {
						return false;
					}
				}
			});

			$('#btnNuevaBusqueda').click(function(e) {
				nuevaBusqueda(); // function que limpia todos los campos y los habilita.
			});

			$('#btnAplicar').click(function(e) {
				validateFilters(); // Servicio que valida que filtro aplicar
			});

			var dkEstatusEnvio = new Dropkick('#estatusEnvio');
			var dkTipoEnvio = new Dropkick('#tipoEnvio');

			dkTipoEnvio.select(0);
			dkEstatusEnvio.select(0);

			//Calendario
			$.datepicker.regional['es'] = {
				closeText: 'Cerrar',
				prevText: '',
				nextText: ' ',
				currentText: 'Hoy',
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
				dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
				dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
				weekHeader: 'Sm',
				dateFormat: 'dd/mm/yy',
				firstDay: 1,
				isRTL: false,
				showMonthAfterYear: false,
				yearSuffix: ''
			};
			$.datepicker.setDefaults($.datepicker.regional['es']);
		});
	</script>

	<div class="clear"></div>
	<div class="titulo">
		<div class="ruta">
			<a href="estatusGeneral.htm">Página Principal</a> / <a href="cifrasControl.htm">Administración de folios</a>
		</div>
		Administración de folios
	</div>

	<!-- Contenido -->
	<div class="contSecc">

		<div class="titSec">Filtros</div>
		<div class="gris">

			<div class="divCol3">
				<div class="col3">
					<a href="#" id="btnTerritorio" class="btnDistrib" style="width: 95%;"> <img src="../../img/icoZonaW.svg"><br>Territorio
					</a>
				</div>
				<div class="col3">
					<a href="#" id="btnPais" class="btnDistrib" style="width: 95%;"> <img src="../../img/icoPaisW.svg"><br>País
					</a>
				</div>
				<div class="col3">
					<a href="#" id="btnCategoria" class="btnDistrib" style="width: 95%;"> <img src="../../img/icoCategoriaW.svg"><br>Categoría
					</a>
				</div>
			</div>

			<div class="divCol3">
				<div class="col3">
					Fecha de envío<br>
					<input type="text" placeholder="DD/MM/AAAA" class="datapicker1 date" id="datepickerAdminEnv" autocomplete="off">
				</div>
				<div class="col3">
					Estatus de folio<br>
					<select id="estatusEnvio" onchange="validaEstatusDeEnvio(this);">
						<%--<option>Selecciona una opción</option>--%>
						<option>En progreso</option>
						<option>Completado</option>
						<option>Cancelado</option>
					</select>
				</div>
				<div class="col3">
					Tipo de folio<br>
					<select id="tipoEnvio" onchange="validaTipoDeEnvio(this);">
						<%--<option>Selecciona una opción</option>--%>
						<option>Instantáneo</option>
						<option>Batch</option>
						<option>Calendarizado</option>
					</select>
				</div>
			</div>
			<div class="divCol2">
				<div class="col2">
					Buscar por Folio<br>
					<form id="porFolio" name="porFolio" action="" method="get" onsubmit="return false;">
						<div class="pRelative">
							<input type="text" id="buscarPorFolio" autocomplete="off" alt="Número de Folio" oninput="valInputFilter(this);" >
							<input type="submit" class="buscar" value="">
						</div>
					</form>
				</div>
				<div class="col2">
					Buscar por Sucursal<br>
					<form id="porFolio" name="porSucursal" action="" method="get" onsubmit="return false;">
						<div class="pRelative">
							<input type="text" id="buscarPorSucursal" autocomplete="off" alt="Número de sucursal" oninput="valInputFilter(this);" >
							<input type="submit" class="buscar" value="">
						</div>
					</form>
				</div>
			</div>

			<p style="margin-bottom: 2px; margin-left: 15px; color: #006341;">
				<strong>Filtros por aplicar</strong>
			</p>
			<div id="listaLanzamientos" class="divCol3 divListLanzamientos" style="height: 65px; margin-left: 15px; margin-right: 10px; min-height: 105px;">
				<%-- lista de objetos para realizar búsqueda de folio --%>
			</div>
			<div class="btnCenter">
				<a href="#" id="btnNuevaBusqueda" class="btnA btnG" style="margin: 0px 10px 0 px 0px;">Nueva búsqueda</a>
				<a href="#" id="btnAplicar" class="btnA" style="margin: 0px;">Aplicar</a>
			</div>
		</div>

		<div class="titSec">Búsqueda</div>
		<div class="gris">
			<table id="tblFolios" class="tblGeneral" style="display: block; width: 100%;">
				<thead style="display: block; width: 100%;">
					<tr style="display: inline-table; width: 100%;">
						<th style="width: 15%;">Folio de envío</th>
						<th style="width: 20%;">Tipo de envío</th>
						<th style="width: 20%;">Fecha / Hora de creación de folio</th>
						<th style="width: 20%;">Sucursales afectadas</th>
						<th style="width: 15%;">Estatus de envío</th>
					</tr>
				</thead>
				<tbody id="tblBody" style="display: block; width: 100%; max-height: 240px; overflow-y: scroll;"></tbody>
			</table>
		</div>
		<div class="btnCenter">
			<a href="#" class="btnA btnB" id="exportarFolios">Exportar folios</a>
		</div>
	</div>

	<!-- Modales -->
	<div id="modalDetalle" class="modal">
		<div class="cuadro cuadroDetalleFolio">
			<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
			<br>
			<div class="titModal">Detalle del folio</div>
			<br>
			<div class="contModal">
				<div class="tLeft">
					Folio del envío: <strong id="sIdFolio"></strong>
					<input id="faIdDocs" name="faIdDocs" type="hidden" value="">
					<input id="faPass" name="faPass" type="hidden" value="1">
					<input id="faPass4" name="faPass4" type="hidden" value="1">
				</div>
				<br>
				<table class="tblGeneral">
					<thead>
						<tr>
							<th style="width: 30%;">Nombre del documento</th>
							<th style="width: 20%;">Categoría</th>
							<th style="width: 20%;">% de Avance</th>
							<th style="width: 10%;">Visible en sucursal</th>
							<th style="width: 15%;">Fecha de visualización</th>
						</tr>
					</thead>
					<tbody id="tbodyFolioDetail"></tbody>
				</table>
			</div>

			<br>
			<div class="btnCenter">
				<a href="#" class="btnA btnG" id="modificarFolioDetail">Modificar folio</a>
				<a href="#" class="btnA btnB" id="exportarHistorialModal" >Exportar</a>
			</div>
		</div>
		<div class="clear"></div>
		<br>
	</div>

	<!-- modal para mostrar el detalle de un folio -->
	<div id="modalAdminList" class="modal">
		<div class="cuadro cuadroG">
			<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
			<div id="adminTitle" class="titModal">ListName</div>
			<br>
			<div id="adminList" class="contModal" style="text-align: left; overflow-y: scroll; height: 290px; max-height: 360px;">
				<!-- lista de cecos generada por codigo -->
			</div>
			<br>
			<div class="btnModalList">
				<a href="#" id="btnListOk" class="btnA" style="margin-right: 10px;" onclick="createAdminDestinyBox(_boxId);" >Aceptar</a>
				<a href="#" id="btnListSigNivel" class="btnA" style="margin-left: 10px;" onclick="nextLevelAdminList();" >Siguiente nivel</a>
			</div>
		</div>
	</div>

	<!-- modal para mostrar la vista previa de un documento -->
	<div id="vistaPrevia" class="modal">
		<div class="cuadroFolioDetailVP">
			<!--
			<a href="#" class="simplemodal-close btnCerrar" id="vpBtnCerrar">
				<img src="../../img/icoCerrar.svg">
			</a>
			-->
			<br>
			<div id="vpTitle" class="titModal" style="margin-top: 0px;">Vista previa de archivo</div>
			<br>
			<div id="vpContainer" style="overflow-y: scroll; height: 68vh; width: 76vw; margin: 0 auto;">
				<!-- Se generan elementos por codigo -->
			</div>
			<div class="btnCenter">
				<a href="#" class="btnA" onclick="vpBtnCerrar();">Cerrar</a>
			</div>
		</div>
	</div>

	<!-- modal para mostrar las opciones del folio -->
	<div id="modalFolioDetail" class="modal">
		<div class="cuadro cuadroG">
			<!--
			<a href="#" class="simplemodal-close btnCerrar" id="vpBtnCerrar">
				<img src="../../img/icoCerrar.svg">
			</a>
			-->
			<br>
			<input id="faPass" name="faPass" type="hidden" value="1">
			<input id="faPass4" name="faPass4" type="hidden" value="1">
			<input id="fdFechaActual" name="faPass" type="hidden" value="">
			<input id="faIdDocs" name="faIdDocs" type="hidden" value="">
			<input id="fdDocList" name="fdDocList" type="hidden" value="">
			<div id="fdTitle" class="titModal" style="margin-top: 5px;">Modificar folio</div>
			<br>
			<p style="text-align: left; margin-left: 20px; margin-bottom: 15px;">
				Folio de envío: <strong id="fdIdFolio"></strong>
			</p>
			<p style="text-align: left; margin-left: 20px; margin-bottom: 5px;">
				<strong>Selecciona la opción deseada</strong>
			</p>
			<br>
			<div id="fdOptions" style="display: grid; flex-wrap: wrap; margin: 0px 25px;">
				<input type="radio" name="radiobtn" id="fd1">
				<label for="fd1" style="margin-left: 15px; text-align: left; margin-bottom: 5px;">Agregar sucursales</label>
				<input type="radio" name="radiobtn" id="fd2">
				<label for="fd2" style="margin-left: 15px; text-align: left; margin-bottom: 5px;">Modificar fecha de visualización</label>
				<%--
				<input type="radio" name="radiobtn" id="fd3">
				<label for="fd3" style="margin-left: 15px; text-align: left; margin-bottom: 5px;">Sustituir documento</label>
				--%>
				<input type="radio" name="radiobtn" id="fd4">
				<label for="fd4" style="margin-left: 15px; text-align: left; margin-bottom: 5px;">Cancelar folio</label>
			</div>
			<div class="btnCenter">
				<a href="#" class="btnA btnG" onclick="vpBtnCerrar();">Cancelar</a>
				<a href="#" class="btnA" onclick="selectOptionEditFolio();">Siguiente</a>
			</div>
		</div>
	</div>

	<!-- modal para cambiar fecha de visualización -->
	<div id="modalCambiaFechaFolio" class="modal">
		<div class="cuadro cuadroG">
			<!--
			<a href="#" class="simplemodal-close btnCerrar" id="vpBtnCerrar">
				<img src="../../img/icoCerrar.svg">
			</a>
			-->
			<br>
			<div id="fdTitle" class="titModal" style="margin-top: 5px;">Modificar folio</div>
			<input id="fdFechaActual" name="faPass" type="hidden" value="">
			<br>
			<p style="text-align: left; margin-left: 20px; margin-bottom: 15px;">
				Folio de envío: <strong id="fdIdFolio"></strong>
			</p>
			<p style="text-align: left; margin-left: 20px; margin-bottom: 5px;">
				<strong>Selecciona la nueva fecha de visualización</strong>
			</p>
			<br>
			<div style="text-align: left; margin-left: 40px; margin-bottom: 10px; margin-right: 40px;">
				<p style="text-align: left; margin-left: 0px; margin-bottom: 10px;">Fecha de visualización</p>
				<input type="text" placeholder="DD/MM/AAAA" class="datapicker1 date" id="dpNuevaFecha" autocomplete="off">
			</div>
			<div class="btnCenter">
				<a href="#" class="btnA btnG" onclick="vpBtnCerrarNuevaFecha();">Cancelar</a>
				<a href="#" class="btnA" onclick="updateFechaVisualizacionFolio();">Actualizar fecha</a>
			</div>
		</div>
	</div>

	<!-- modal para sustituir documento -->
	<div id="modalSustituyeDocumentoFolio" class="modal">
		<div class="cuadro cuadroG">
			<!--
			<a href="#" class="simplemodal-close btnCerrar" id="vpBtnCerrar">
				<img src="../../img/icoCerrar.svg">
			</a>
			-->
			<br>
			<div id="fdTitle" class="titModal" style="margin-top: 5px;">Modificar folio</div>
			<br>
			<p style="text-align: left; margin-left: 20px; margin-bottom: 15px;">
				Folio de envío: <strong id="fdIdFolio"></strong>
			</p>
			<p style="text-align: left; margin-left: 20px; margin-bottom: 5px;">
				<strong>Selecciona un documento de la lista</strong>
			</p>
			<br>
			<div id="fdDocOptions" style="display: grid; flex-wrap: wrap; margin: 0px 25px;"></div>
			<br>
			<form id="formChangeDocFolio" action="sustituyeDocumentoFolio.htm">
				<div style="text-align: left; margin-left: 40px; margin-bottom: 10px; margin-right: 40px;">
					<input type="hidden" id="fdIdFolio" name="fdIdFolio" value="">
					<input type="hidden" id="fdJsonDoc" name="fdJsonDoc" value="">
					<input type="file" id="cargaDocFolio" class="inputfile carga">
					<label for="cargaDocFolio" class="btnAgregaFoto"><span>Adjuntar documento</span></label>
				</div>
			</form>
			<div class="btnCenter">
				<a href="#" class="btnA btnG" onclick="vpBtnCerrarSustituyeDoc();">Cancelar</a>
				<a href="#" class="btnA" onclick="changeDocOfFolio();">Sustituir documento</a>
			</div>
		</div>
	</div>

	<!-- modal para agregar sucursales al folio -->
	<div id="modalAgregaSucursalesFolio" class="modal">
		<div class="cuadro cuadroG">
			<br>
			<div id="fdTitle" class="titModal" style="margin-top: 5px;">Modificar folio</div>
			<br>
			<p style="text-align: left; margin-left: 20px; margin-bottom: 15px;">
				Folio de envío: <strong id="fdIdFolio"></strong>
			</p>
			<div id="agregaSucursalesContainer" style="margin-left: 20px; margin-right: 20px;">
				<p style="text-align: left; margin-left: 20px; margin-bottom: 5px;">
					<strong>Agrega tu archivo CSV</strong>
				</p>
				<br>
	
				<form id="formAgregaSucursalesFolio" action="sustituyeDocumentoFolio.htm">
					<div style="text-align: left; margin-left: 40px; margin-bottom: 10px; margin-right: 40px;">
						<input id="fdIdFolioH" name="fdIdFolioH" type="hidden" value="">
						<input id="faIdDocs" name="faIdDocs" type="hidden" value="">
						<input type="file" id="cargaSucursalesFolio" class="inputfile carga" onchange="readCSVFile('cargaSucursalesFolio'); showLoadingSpinner(); this.value=null; return false;">
						<label for="cargaSucursalesFolio" class="btnAgregaFoto"><span>Adjuntar documento</span></label>
					</div>
				</form>
			</div>

			<div id="totalContainer" class="btnCenter">
				<p id="totalCargado" style="text-align: left; margin-left: 20px; margin-bottom: 5px;"></p>
				<p id="totalMostrado" style="text-align: left; margin-left: 20px; margin-bottom: 5px;"></p>
			</div>

			<div class="btnCenter">
				<a href="#" id="btnCancelarAgrSuc" class="btnA btnG" onclick="vpBtnCerrarAgregaSucursales();">Cancelar</a>
				<a href="#" id="btnAgregarSucursales" class="btnA desac" onclick="agregaSucursalesFolio();">Agregar sucursales</a>
				<a href="#" id="btnAceptarAgrSuc" class="btnA" onclick="vpBtnCerrarAgregaSucursales();">Aceptar</a>
			</div>
		</div>
	</div>

	<!-- MODAL DESCARGA -->
	<div class="toastDescarga">
		<div class="progresoDescarga">
			<div class="grafica">
				<ul>
					<li class="circle-gauge">
						<a href="#" style="-gauge-value: 0;"></a>
					</li>
				</ul>
			</div>
			<div class="tiempo" id="countdown"></div>
			<div class="tamano" id="tamanoArchivo"></div>
		</div>
		<div class="cerrar">
			<img src="../../img/cerrar.svg" alt="">
		</div>
	</div>

	<!-- LOADER -->
	<div class="loaderContainer" style="display: none;">
		<div class="ml-loader loader" style="">
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
		</div>
	</div>
	<div class="blackScreen" style="display: none;"></div>

</body>
</html>