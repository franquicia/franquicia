<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="sbig"><b> ACTA DE ENTREGA PARA CONCENTRACIÓN DE EXPEDIENTE</b></span>
    	</div>
    	<div class="page">
            <div class="contHome">
                <div class="fix">
                    <iframe width="100%" height="100%" src="${pageContext.request.contextPath}/pdf/1.pdf" frameborder="0" allowfullscreen></iframe> 
                </div>
                <div class="botones btnfix"> 
                <div class="botones "> 
                    <div class="tleft w50">
                        <a href="acta-entrega-expediente2.htm" class="btn arrowl">
                            <div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
                            <div>VOLVER AL LISTADO</div>
                        </a> 
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
    </body>
</html>


