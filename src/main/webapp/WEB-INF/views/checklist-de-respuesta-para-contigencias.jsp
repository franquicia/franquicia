<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> CHECKLIST DE RESPUESTA PARA CONTINGENCIAS</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome top60">
	    		<div class="tituloScont"><b> Selecciona la fecha para consultar el Checklist.</b></div><br>
	    		<div class="sbModal">Colaboradores</div>

	    		<div class="border borderLeft">  
					<div class=" overflow ">   


							<table class="items ChModal">
								<tbody>
									<tr>   
										<td><b>Contactados</b></td>
										<td><b>Sí</b></td>
									</tr>  
									<tr>   
										<td><b>¿Cuántos?</b></td>
										<td><b>12</b></td> 
									</tr>
									<tr>   
										<td><b>Desaparecidos</b></td>
										<td><b>No</b></td> 
									</tr>
									<tr>   
										<td><b>Atrapados en Búnker</b></td>
										<td><b>No</b></td> 
									</tr>
									<tr>   
										<td><b>Heridos</b></td> 
										<td><b>No</b></td>
									</tr>
									<tr>   
										<td><b>Fallecidos</b></td>
										<td><b>No</b></td>
									</tr>
									<tr>   
										<td><b>¿Algún socio presenta afectación en su hogar o familia?</b></td>
										<td><b>Sí</b></td>
									</tr>
									<tr>   
										<td><b>¿Qué apoyos requieren?</b></td>
										<td><b>Apoyo para localizar familiares</b></td>
									</tr>
								</tbody>
							</table> 



					</div>  
				</div>
				
							<div class="paginador tright"">
			                    <a href="#">1</a>
			                    <span>/</span> 
			                    <a href="#">2</a>
			                    <a href="#">
			                        <img src="${pageContext.request.contextPath}/img/parrow.png" class="paginadorArroW">
			                    </a>
			                </div>
	    		<div class="botones"> 
					<a href="inicio7s.htm" class="btn arrowl">
						<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
						<div>VOLVER AL LISTADO</div>
					</a>   
				</div>
			</div>
    	</div>


        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    	<!--MODAL-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.FolioUno').click(function (e) {
                        setTimeout(function() {
                            $('#folioUno').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            }); 
        </script>
    </body>
</html>



