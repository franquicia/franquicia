<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div class="contSecc">
		<div class="titSec">Consulta ${tipoConsulta}</div>
		<div class="gris">
			<form action="consultaParam.htm" method="POST">
				<div class="divCol3">
					<b>Selecciona el tipo de consulta que quieras</b> (si deseas un id en especifico introducelo)
				</div>

				<div id="divTipos" class="divCol1">
					<div class="col1">
						<input type="radio" name="tipo" id="tipo1" value="param" checked="checked">
						<label for="tipo1">Consulta Parametros</label>
						<input type="radio" name="tipo" id="tipo2" value="negocio">
						<label for="tipo2">Consulta Negocio</label>
						<input type="radio" name="tipo" id="tipo3" value="canal">
						<label for="tipo3">Consulta Canal</label>
						<input type="radio" name="tipo" id="tipo4" value="menu">
						<label for="tipo4">Consulta menu</label>
						<input type="radio" name="tipo" id="tipo5" value="perfil">
						<label for="tipo5">Consulta Perfil</label>
						<input type="radio" name="tipo" id="tipo6" value="estatus">
						<label for="tipo6">Consulta Estatus tablet</label>
						<input type="radio" name="tipo" id="tipo7" value="tipoDoc">
						<label for="tipo7">Consulta Tipo de documento</label>
					</div>
				</div>

				<div class="divCol3">
					<div class="col3">
						<label for="tipo3">Inserta el id que deseas consultar.</label>
						<input type="text" name="idParametro" id="idParam">
						<input id="btn" type="submit" class="btnA" value="Enviar">
					</div>
				</div>

				<div class="divCol1" id="consulta">
					<div class="col1">
						Consulta: ${tipoConsulta}
						<br>
						<div style="margin: 10px; padding: 10px; width: 98%; height: 200px; overflow: scroll; background: white; border-style: solid; border-color: #6e7975; text-align: center;">
							<table align="center" border="1" cellpadding="15px" style="width: 100%">
								<thead>
									<tr>
										<c:forEach items="${ listHeader}" var="item">
											<td style="margin: 5px; padding: 5px;">${item}</td>
										</c:forEach>
									</tr>
								</thead>
								<tbody>
									<c:if test="${tipoConsulta == 'Parametros'}">
										<c:if test="${(fn:length(listDatos)) > 0}">
											<c:forEach begin="0" end="${fn:length(listDatos)-1}" varStatus="loop">
												<tr>
													<%-- <c:out value="${fn:length(listParametros)}"></c:out>  --%>
													<td>${listDatos[loop.index].idParametro}</td>
													<td>${listDatos[loop.index].parametro}</td>
													<td>${listDatos[loop.index].descParametro}</td>
													<td>${listDatos[loop.index].valor}</td>
													<td>${listDatos[loop.index].referencia}</td>
													<td>${listDatos[loop.index].usuario}</td>
													<td>${listDatos[loop.index].fecha}</td>
													<td>${listDatos[loop.index].activo}</td>
												</tr>
											</c:forEach>
										</c:if>
									</c:if>
									<c:if test="${tipoConsulta == 'Negocio'}">
										<c:if test="${(fn:length(listDatos)) > 0}">
											<c:forEach begin="0" end="${fn:length(listDatos)-1}" varStatus="loop">
												<tr>
													<%-- <c:out value="${fn:length(listParametros)}"></c:out>  --%>
													<td>${listDatos[loop.index].idNegocio}</td>
													<td>${listDatos[loop.index].negocio}</td>
													<td>${listDatos[loop.index].usuario}</td>
													<td>${listDatos[loop.index].fechaCambio}</td>
													<td>${listDatos[loop.index].activo}</td>
												</tr>
											</c:forEach>
										</c:if>
									</c:if>
									<c:if test="${tipoConsulta == 'Canal'}">
										<c:if test="${(fn:length(listDatos)) > 0}">
											<c:forEach begin="0" end="${fn:length(listDatos)-1}" varStatus="loop">
												<tr>
													<%-- <c:out value="${fn:length(listParametros)}"></c:out>  --%>
													<td>${listDatos[loop.index].idCanal}</td>
													<td>${listDatos[loop.index].canal}</td>
													<td>${listDatos[loop.index].usuario}</td>
													<td>${listDatos[loop.index].fechaCambio}</td>
													<td>${listDatos[loop.index].activo}</td>
												</tr>
											</c:forEach>
										</c:if>
									</c:if>
									<c:if test="${tipoConsulta == 'Menu'}">
										<c:if test="${(fn:length(listDatos)) > 0}">
											<c:forEach begin="0" end="${fn:length(listDatos)-1}" varStatus="loop">
												<tr>
													<%-- <c:out value="${fn:length(listParametros)}"></c:out>  --%>
													<td>${listDatos[loop.index].idMenu}</td>
													<td>${listDatos[loop.index].descripcion}</td>
													<td>${listDatos[loop.index].nivel}</td>
													<td>${listDatos[loop.index].cursor}</td>
													<td>${listDatos[loop.index].fechaCambio}</td>
													<td>${listDatos[loop.index].activo}</td>
													<td>${listDatos[loop.index].dependeDe}</td>
												</tr>
											</c:forEach>
										</c:if>
									</c:if>
									<c:if test="${tipoConsulta == 'Perfil'}">
										<c:if test="${(fn:length(listDatos)) > 0}">
											<c:forEach begin="0" end="${fn:length(listDatos)-1}" varStatus="loop">
												<tr>
													<%-- <c:out value="${fn:length(listParametros)}"></c:out>  --%>
													<td>${listDatos[loop.index].idMenu}</td>
													<td>${listDatos[loop.index].descripcion}</td>
													<td>${listDatos[loop.index].nivel}</td>
													<td>${listDatos[loop.index].activo}</td>
													<td>${listDatos[loop.index].dependeDe}</td>
													<td>${listDatos[loop.index].descDependeDe}</td>
													<td>${listDatos[loop.index].idPerfil}</td>
													<td>${listDatos[loop.index].descPerfil}</td>
													<td>${listDatos[loop.index].activoPerfil}</td>
												</tr>
											</c:forEach>
										</c:if>
									</c:if>
									<c:if test="${tipoConsulta == 'Estatus Tableta'}">
										<c:if test="${(fn:length(listDatos)) > 0}">
											<c:forEach begin="0" end="${fn:length(listDatos)-1}" varStatus="loop">
												<tr>
													<%-- <c:out value="${fn:length(listParametros)}"></c:out>  --%>
													<td>${listDatos[loop.index].idEstadoTableta}</td>
													<td>${listDatos[loop.index].descWEB}</td>
													<td>${listDatos[loop.index].descJSON}</td>
													<td>${listDatos[loop.index].visible}</td>
													<td>${listDatos[loop.index].idStatus}</td>
													<td>${listDatos[loop.index].usuario}</td>
													<td>${listDatos[loop.index].fechaCambio}</td>
													<td>${listDatos[loop.index].dependeDe}</td>
													<td>${listDatos[loop.index].activo}</td>
												</tr>
											</c:forEach>
										</c:if>
									</c:if>
									<c:if test="${tipoConsulta == 'Tipo de documento'}">
										<c:if test="${(fn:length(listDatos)) > 0}">
											<c:forEach begin="0" end="${fn:length(listDatos)-1}" varStatus="loop">
												<tr>
													<td>${listDatos[loop.index].idTipoDoc}</td>
													<td>${listDatos[loop.index].nombre}</td>
													<td>${listDatos[loop.index].nivel}</td>
													<td>${listDatos[loop.index].dependeDe}</td>
													<td>${listDatos[loop.index].descDependeDe}</td>
													<td>${listDatos[loop.index].user}</td>
													<td>${listDatos[loop.index].fecha}</td>
													<td>${listDatos[loop.index].activo}</td>
												</tr>
											</c:forEach>
										</c:if>
									</c:if>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>