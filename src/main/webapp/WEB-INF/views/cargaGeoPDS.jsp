<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<script type="text/javascript">
		var contextPath = '${pageContext.request.contextPath}';
		$(document).ready(function() {
			$('#radio1').click();
			$('#radio3').click();
		});
	</script>

	<div class="contSecc">
		<div class="titSec">Carga de cecos</div>
		<div class="gris">
			<div class="divCol3">
				<div class="col3">
					Subir cecos por Excel<br>
					<input type="file" id="cargaGeoExcel" class="inputfile carga" onchange="readExcelFile(); showLoadingSpinner(); this.value=null; return false;">
					<label for="carga01" class="btnAgregaFoto" onclick="$('#cargaGeoExcel').click();"><span>Adjuntar archivo</span></label>
				</div>
				<div class="col3"></div>
				<div class="col3"></div>
			</div>
			<div id="geoContainer"></div>
		</div>

		<div class="btnCenter">
			<div id="btnClear" class="btnA btnG" onclick="$('#geoContainer').html('');">Nueva carga</div>
		</div>
	</div>

	<!-- LOADER -->
	<div class="loaderContainer" style="display: none;">
		<div class="ml-loader loader" style="">
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
		</div>
	</div>
	<div class="blackScreen" style="display: none;"></div>
</body>
</html>