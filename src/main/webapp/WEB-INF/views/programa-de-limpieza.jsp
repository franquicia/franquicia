<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> PROGRAMA DE LIMPIEZA</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome top60"> 


	            <div class="paddingMuser"> 
					<c:forEach var="lista" items="${lista}">
	                <div class="sbModal"><c:out value="${lista.fecha}"></c:out></div><br>
	                </c:forEach>
	                <div class="border">  
						<div class=" overflow  w100"> 
						    <table class="items LimModal ">
							    <thead>
								    <tr>
								      <th>Área a Limpiar</th>
								      <th>Artículos</th>
								      <th>Frecuencia/Periodicidad</th>
								      <th>Supervisor</th>
								      <th>Responsable</th>
								    </tr>
							    </thead>
							    
							    <c:forEach var="listaProg" items="${listaProg}">
                                    <tr>
								      <td><c:out value="${listaProg.area}"></c:out></td>
								      <td><c:out value="${listaProg.articulos}"></c:out></td>
								      <td><c:out value="${listaProg.frecuencia}"></c:out> / <c:out value="${listaProg.periodicidad}"></c:out></b></td>
								      <td><c:out value="${listaProg.supervisor}"></c:out></td>
								      <td><c:out value="${listaProg.responsable}"></c:out></td>
								    </tr>
                                    	</c:forEach>
							    
                                <!-- <tr>
								      <td>Patio Bancario</td>
								      <td>Pisos</td>
								      <td>Diario / 1 vez al día</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Escritorios</td>
								      <td>Mesas, sillas</td>
								      <td>Semanal / 1 vez al día</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Comedor</td>
								      <td>Microondas</td>
								      <td>Diario / 2 veces al día</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Baños</td>
								      <td>Inodoro</td>
								      <td>Diario / 2 veces al día</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Bunker</td>
								      <td>Archivero</td>
								      <td>Semanal / 2 veces al día</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Site</td>
								      <td>Paredes</td>
								      <td>Semanal / 1 vez al día</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr> -->
								</tbody>
                            </table>
						</div>
				    </div><br>
				</div>



	    		<div class="botones"> 
					<a href="inicio7s.htm" class="btn arrowl">
						<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
						<div>VOLVER AL MENú</div>
					</a>   
				</div>
			</div>
    	</div>

    	<!--MODAL-->
        <div id="limpieza" class="modal">
            <div class="cuadrobigmax">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="sbModal">4 octubre 2018</div><br>
	                <div class="border">  
						<div class=" overflow mh300 w100"> 
						    <table class="items LimModal ">
							    <thead>
								    <tr>
								      <th>Área a Limpiar</th>
								      <th>Artículos</th>
								      <th>Frecuencia</th>
								      <th>Supervisor</th>
								      <th>Responsable</th>
								    </tr>
							    </thead>
                                <tbody>
								    <tr>
								      <td>Patio Bancario</td>
								      <td>Pisos</td>
								      <td>Diario 2:00 PM</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Escritorios</td>
								      <td>Mesas, sillas</td>
								      <td>Semanal 3:30 PM</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Comedor</td>
								      <td>Microondas</td>
								      <td>Diario 9:00 AM</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Baños</td>
								      <td>Inodoro</td>
								      <td>Diario</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Bunker</td>
								      <td>Archivero</td>
								      <td>Diario</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Site</td>
								      <td>Paredes</td>
								      <td>Semanal 1:00 PM</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
                                </tbody>
                            </table>
						</div>
				    </div><br>
				</div>
           </div>
            <div class="bModalUser"> 
				<div class="tleft w50">
    				<a href="#" class="btn  bm arrowml">
    					<div></div>
    					<div>ENTRADA ANTERIOR</div>
    				</a> 
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr">
                        <div></div>
                        <div>SIGUIENTE ENTRADA</div>
                    </a> 
                </div> 
			</div>
        </div>
        <!-- -->
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>

    	<!--MODAL-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.MLimpieza').click(function (e) {
                        setTimeout(function() {
                            $('#limpieza').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            }); 
        </script>
    </body>
</html>



