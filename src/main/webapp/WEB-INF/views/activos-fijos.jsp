<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>



<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> ACTA DE HECHOS DE DEPURACIÓN DE ACTIVOS FIJOS</b></span>
    	</div>
    	<div class="page">
            <div class="contHome top60">
                <div class="tapoyo">Acta de Hechos ${FECHA}</div>
                    <div class="border">  
                        <div class=" overflow mh500">   
                            <table class="items tbActa">
                                <thead>
                                    <tr>
                                        <th>Sucursal</th>
                                        <th>${DESC_CECO}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>   
                                        <td>
                                            <b>Número Económico</b>
                                        </td>
                                        <td>
                                            <div><b>${CECO}</b></div>
                                        </td> 
                                    </tr>  
                                        <tr>   
                                            <td>
                                                <div><b>Ubicación</b></div>
                                            </td>
                                            <td>
                                                <div><b>${UBICACION}</b></div>
                                            </td> 
                                        </tr>
                                        <tr>   
                                            <td>
                                                <div><b>Involucrados</b></div>
                                            </td>
                                            <td>
                                                <div><b>${INVOLUCRADOS}</b></div>
                                            </td> 
                                        </tr>
                                        <tr>   
                                            <td>
                                                <div><b>Razón Social</b></div>
                                            </td>
                                            <td>
                                                <div><b>${RAZON_SOCIAL}</b></div>
                                            </td> 
                                        </tr>
                                        <tr>   
                                            <td>
                                                <div><b>Domicilio</b></div>
                                            </td>
                                            <td>
                                                <div><b>${DOMICILIO}</b></div>
                                            </td> 
                                        </tr>
                                        <tr>   
                                            <td>
                                                <div><b>RFC</b></div>
                                            </td>
                                            <td>
                                                <div><b>${RFC}</b></div>
                                            </td> 
                                        </tr>
                                        <tr>   
                                            <td>
                                                <div><b>Nombre del Responsable</b></div>
                                            </td>
                                            <td>
                                                <div><b>${RESPONSABLE}</b></div>
                                            </td> 
                                        </tr>
                                        <tr>   
                                            <td>
                                                <div><b>Nombre del Testigo</b></div>
                                            </td>
                                            <td>
                                                <div><b>${TESTIGO}</b></div>
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table> 
                            </div>  
                        </div>
                    
<!-- 
                        <div class="w48a">
                            <div class="tfoto"><b>Fotografía de Activos</b></div>
                            <div class="cfActivo pointer MActa1">
                                <div class="overflow">
                                    <img src="${pageContext.request.contextPath}/img/imgticket.png" class="imgfoto">
                                </div>
                            </div>
                        </div>

                        <div class="w48b">
                            <div class="tfoto"><b>Fotografía del Ticket</b></div>
                            <div class="cfTicket  pointer MActa2">
                                <div class="overflow">
                                    <img src="${pageContext.request.contextPath}/img/imgticket.png" class="imgfoto">
                                </div>
                            </div>
                        </div>

                        <div class="clear"></div>


                        <div class="w48a">
                            <div class="tfoto"><b>Fotografía de Propuestas Económicas</b></div>
                            <div class="cfActivo  pointer MActa3">
                                <div class="overflow">
                                    <img src="${pageContext.request.contextPath}/img/imgticket.png" class="imgfoto">
                                </div>
                            </div>
                        </div>

                        <div class="w48b">
                            <div class="tfoto"><b>&nbsp;</b></div>
                            <div class="cfTicket  pointer MActa3">
                                <div class="overflow">
                                    <img src="${pageContext.request.contextPath}/img/imgticket.png" class="imgfoto">
                                </div>
                            </div>
                        </div>
-->
                        <div class="clear"></div>

                        <div class="tcenter">
                            <a href="#" class="Sboton MLactivo">
                                CONSULTAR LISTA DE ACTIVOS
                            </a>   
                        </div>

                		<div class="botones"> 
                            <div class="tleft w50">
                                <a href="acta-de-hechos-de-depuracion-de-activos-fijos.htm" class="btn arrowl">
                                    <div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
                                    <div>VOLVER A LISTADO</div>
                                </a> 
                            </div>
                        </div>
            </div>
            <!--MODAL-->
                <div id="acta1" class="modal">
                    <div class="w100">
                        <div class="tModal"><b>Fotografía de Activos</b></div>
                        <div class="cuadrobig">  
                            <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                            <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">
                        </div>
                    </div>
                    <div class="bModalUser"> 
                        <div class="tleft w50">
                            <a href="#" class="btn  bm arrowml simplemodal-close MActa3">
                                <div></div>
                                <div class="MActa2">FOTO ANTERIOR</div>
                            </a> 
                        </div>

                        <div class="tright w50"> 
                            <a href="#" class="btn bm arrowmr simplemodal-close MActa2">
                                <div></div>
                                <div>SIGUIENTE FOTO</div>
                            </a> 
                        </div> 
                    </div>
                </div>
                <div id="acta2" class="modal">
                    <div class="w100">
                        <div class="tModal"><b>Fotografía del Ticket</b></div>
                        <div class="cuadrobig">  
                            <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                            <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">
                        </div>
                    </div>
                    <div class="bModalUser"> 
                        <div class="tleft w50">
                            <a href="#" class="btn  bm arrowml simplemodal-close MActa1">
                                <div></div>
                                <div class="MActa2">FOTO ANTERIOR</div>
                            </a> 
                        </div>

                        <div class="tright w50"> 
                            <a href="#" class="btn bm arrowmr simplemodal-close  MActa3">
                                <div></div>
                                <div>SIGUIENTE FOTO</div>
                            </a> 
                        </div> 
                    </div>
                </div>
                <div id="acta3" class="modal">
                    <div class="w100">
                        <div class="tModal"><b>Fotografía de Propuestas Económicas</b></div>
                        <div class="cuadrobig">  
                            <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                            <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">
                        </div>
                    </div>
                    <div class="bModalUser"> 
                        <div class="tleft w50">
                            <a href="#" class="btn  bm arrowml simplemodal-close MActa2">
                                <div></div>
                                <div class="MActa2">FOTO ANTERIOR</div>
                            </a> 
                        </div>

                        <div class="tright w50"> 
                            <a href="#" class="btn bm arrowmr simplemodal-close MActa1">
                                <div></div>
                                <div>SIGUIENTE FOTO</div>
                            </a> 
                        </div> 
                    </div>
                </div>


            <div id="lconsulta" class="modal">
                <<div class="cuadromed">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
                    <div class="sbModal">${FECHA}</div><br>
                    <div class="border">  
                        <div class=" overflow mh500 w100">   
                            <table class="items GiModal tcenter">
                                <thead>
                                    <tr>
                                        <th>Placa</th>
                                        <th>Descripción</th>
                                        <th>Marca</th>
                                        <th>Serie</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="listaActivos" items="${listaActivos}">
                                    <tr>   
                                        <td><b><c:out value="${listaActivos.placa}"></c:out></b></td>
                                        <td><b><c:out value="${listaActivos.descripcion}"></c:out></b></td>
                                        <td><b><c:out value="${listaActivos.marca}"></c:out></b></td>
                                        <td><b><c:out value="${listaActivos.serie}"></c:out></b></td>
                                    </tr>  
                                    </c:forEach>
                                </tbody>
                            </table> 
                        </div>
                    </div><br>
                  
                  
                </div>
            </div>
        </div>
        <!-- -->
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {


                    $('.MActa1').click(function (e) {
                        setTimeout(function() {
                            $('#acta1').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.MActa2').click(function (e) {
                        setTimeout(function() {
                            $('#acta2').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.MActa3').click(function (e) {
                        setTimeout(function() {
                            $('#acta3').modal({
                            });
                            return false;
                        }, 1);
                    });


                    $('.MLactivo').click(function (e) {
                        setTimeout(function() {
                            $('#lconsulta').modal({
                            });
                            return false;
                        }, 1);
                    });


                }); 
            }); 
        </script>
    </body>
</html>



