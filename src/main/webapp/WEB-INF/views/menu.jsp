<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script src="${pageContext.request.contextPath}/js/script-menu.js"></script>

<body>
	<div id="menu">
		<div id="menuContainer">
			<div id="opcion" >			
				<div id="linkMenu">
					<ul class="nav">
						<li>
						<a id="linkMenu" href="/franquicia/">
								 <img id="subir"
								src="${pageContext.request.contextPath}/images/menu/triunfo.png"
								alt="subir">&nbsp;&nbsp; Sistematización de actividades
						</a>
							<ul>
								<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>¿Qu&eacute; es?</p></a></li>
								<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>&nbsp;¿Qu&eacute; tengo que hacer?</p></a></li>
								<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>¿C&oacute;mo lo tengo que hacer?</p></a></li>
								<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>¿Qu&eacute; tengo que verificar?</p></a></li>

								<c:if test="${hasPedestalDigital == 1}">
									<li id="linkMenuHijo"><a id="linkMenuHijo" href="../central/pedestalDigital/validaLoginPD.htm?idUsuario=${user.idUsuario}"><p>Pedestal Digital</p></a></li>
								</c:if>

								<!--
								<li id="linkMenuHijo"><a id="linkMenuHijo" href="../central/pedestalDigital/validaLoginPD.htm?idUsuario=${user.idUsuario}"><p>Pedestal Digital</p></a></li>
								-->
								<c:if test="${puesto=='632' || puesto=='634' || puesto=='775'}">
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Expediente inactivos</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedienteInactivo.htm'"><p>Alta Acta de Entrega</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpediente.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoExpediente.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
								</c:if>

								<c:if test="${puesto=='652'}">
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Expediente inactivos</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedienteInactivo.htm'"><p>Alta Acta de Entrega</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpediente.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoExpediente.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
									<!-- Menus para credito y captacion -->
									<!-- 
									<li id="linkMenuHijo">
									 <a id="linkMenuHijo"  href="#"> <p> Expedientes Crédito</p></a>
									   <ul id="subMenuLinkMenuHijo">
									      <li id="linkMenuHijoSub" onclick="#'"><p>Expedientes Operaciones Activas</p>
									        <ul id="subMenuLinkMenuHijo">
											   <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedientesCreditoActivos.htm'"><p>Alta Acta de Entrega</p></li>
											   <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpeCreditoActivas.htm'"><p>Consulta Actas</p></li>
											   <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoCreditoActivo.htm'"><p>Subir Documento</p></li>
										    </ul>
									      </li>
										  <li id="linkMenuHijoSub" onclick="#"><p>Expedientes Operaciones Canceladas</p>
										     <ul id="subMenuLinkMenuHijo">
											    <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedientesCreditoCancelados.htm'"><p>Alta Acta de Entrega</p></li>
											    <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpeCreditoCanceladas.htm'"><p>Consulta Actas</p></li>
											    <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoCreditoCancelados.htm'"><p>Subir Documento</p></li>
										     </ul>
										  </li>
									   
									   </ul>

									
								    </li>
								    
								    <li id="linkMenuHijo">
									  <a id="linkMenuHijo"  href="#"><p> Expedientes Captación</p></a>
									   <ul id="subMenuLinkMenuHijo">
									      <li id="linkMenuHijoSub" onclick="#'"><p>Expedientes Operaciones Activas</p>
									        <ul id="subMenuLinkMenuHijo">
											   <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedientesCaptacionActivos.htm'"><p>Alta Acta de Entrega</p></li>
											   <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpeCaptacionActivas.htm'"><p>Consulta Actas</p></li>
											   <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoCaptacionActivo.htm'"><p>Subir Documento</p></li>
										    </ul>
									      </li>
										  <li id="linkMenuHijoSub" onclick="#"><p>Expedientes Operaciones Canceladas</p>
										     <ul id="subMenuLinkMenuHijo">
											    <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedientesCaptacionCancelados.htm'"><p>Alta Acta de Entrega</p></li>
											    <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpeCaptacionCanceladas.htm'"><p>Consulta Actas</p></li>
											    <li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoCaptacionCancelados.htm'"><p>Subir Documento</p></li>
										     </ul>
										  </li>
									   
									   </ul>
								    </li>
								     -->
									
								
									<li id="linkMenuHijo" onclick="javascript:location.href='/franquicia/central/inicio7s.htm'"><a id="linkMenuHijo"  href="#"><p>Carpeta 7S</p></a>
									</li>
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Recolecci&oacute;n de archiveros</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/altaArchiveros.htm'"><p>Alta Acta de Entrega de Archiveros</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaArchiveros.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoArchiveros.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
									
									
									
								</c:if>
								<c:if test="${puesto=='653'}">
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Expediente inactivos</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedienteInactivo.htm'"><p>Alta Acta de Entrega</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpediente.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoExpediente.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
									<!-- Menus para credito y captacion -->
									
									<li id="linkMenuHijo">
									  <p> Expedientes Crédito</p>
									   <ul id="subMenuLinkMenuHijo">
									      <li id="linkMenuHijoSub" onclick="#'"><p>Expedientes Operaciones Activas</p>
									        <ul id="subMenuLinkMenuHijo">
											   <li id="linkMenuHijoSub" onclick="#"><p>Alta Acta de Entrega</p></li>
											   <li id="linkMenuHijoSub" onclick="#"><p>Consulta Actas</p></li>
											   <li id="linkMenuHijoSub" onclick="#"><p>Subir Documento</p></li>
										    </ul>
									      </li>
										  <li id="linkMenuHijoSub" onclick="#"><p>Expedientes Operaciones Canceladas</p>
										     <ul id="subMenuLinkMenuHijo">
											    <li id="linkMenuHijoSub" onclick="#"><p>Alta Acta de Entrega</p></li>
											    <li id="linkMenuHijoSub" onclick="#"><p>Consulta Actas</p></li>
											    <li id="linkMenuHijoSub" onclick="#"><p>Subir Documento</p></li>
										     </ul>
										  </li>
									   
									   </ul>

									
								    </li>
								    
								    <li id="linkMenuHijo">
									  <p> Expedientes Captación</p>
									   <ul id="subMenuLinkMenuHijo">
									      <li id="linkMenuHijoSub" onclick="#'"><p>Expedientes Operaciones Activas</p>
									        <ul id="subMenuLinkMenuHijo">
											   <li id="linkMenuHijoSub" onclick="#"><p>Alta Acta de Entrega</p></li>
											   <li id="linkMenuHijoSub" onclick="#"><p>Consulta Actas</p></li>
											   <li id="linkMenuHijoSub" onclick="#"><p>Subir Documento</p></li>
										    </ul>
									      </li>
										  <li id="linkMenuHijoSub" onclick="#"><p>Expedientes Operaciones Canceladas</p>
										     <ul id="subMenuLinkMenuHijo">
											    <li id="linkMenuHijoSub" onclick="#"><p>Alta Acta de Entrega</p></li>
											    <li id="linkMenuHijoSub" onclick="#"><p>Consulta Actas</p></li>
											    <li id="linkMenuHijoSub" onclick="#"><p>Subir Documento</p></li>
										     </ul>
										  </li>
									   
									   </ul>

									
								    </li>
								    
									<!-- 
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Expediente Crédito Operaciones Activas</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedienteInactivo.htm'"><p>Alta Acta de Entrega</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpediente.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoExpediente.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Expediente Crédito Operaciones Canceladas</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedienteInactivo.htm'"><p>Alta Acta de Entrega</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpediente.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoExpediente.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Expediente Captación Operaciones Activas</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedienteInactivo.htm'"><p>Alta Acta de Entrega</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpediente.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoExpediente.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Expediente Captación Operaciones Canceladas</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedienteInactivo.htm'"><p>Alta Acta de Entrega</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpediente.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoExpediente.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
									-->
									<li id="linkMenuHijo" onclick="javascript:location.href='/franquicia/central/inicio7s.htm'"><a id="linkMenuHijo"  href="#"><p>Carpeta 7S</p></a>
									</li>
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Recolecci&oacute;n de archiveros</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/altaArchiveros.htm'"><p>Alta Acta de Entrega de Archiveros</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaArchiveros.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoArchiveros.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
								</c:if>
								<c:if test="${puesto=='420'}">
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Expediente inactivo</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedienteInactivo.htm'"><p>Alta Acta de Entrega</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpediente.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoExpediente.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
									
								</c:if>
								<c:if test="${puesto=='425'}">
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Expediente inactivo</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedienteInactivo.htm'"><p>Alta Acta de Entrega</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpediente.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoExpediente.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
									
								</c:if>
								<c:if test="${puesto=='430'}">
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Expediente inactivo</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedienteInactivo.htm'"><p>Alta Acta de Entrega</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpediente.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoExpediente.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
									
									
									
								</c:if>
								<c:if test="${puesto=='8011'}">
									<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Expediente inactivo</p></a>
										<ul id="subMenuLinkMenuHijo">
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/expedienteInactivo.htm'"><p>Alta Acta de Entrega</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/consultaExpediente.htm'"><p>Consulta Actas</p></li>
											<li id="linkMenuHijoSub" onclick="javascript:location.href='/franquicia/central/documentoExpediente.htm'"><p>Subir Documento</p></li>
										</ul>
									</li>
									
									
									
								</c:if>
								
							
							<!-- <div id="opcionAdminForm">
								<form name="formulario" method="POST" action="/franquicia/central/resumenCheck.htm" id="formActivaAdmin">							
									<li id="linkMenuHijo"><a onclick="resumen()" id="linkMenuHijo"><p>ADMINISTRADOR CHECKLIST</p></a></li>
									<input type="hidden" name="idUserAdmin" value="adminCheckFranquicias">
								</form>							
							</div>-->
							
							<c:if test="${perfilAdmin==1}">
								<form name="formulario" method="POST" action="/franquicia/central/resumenCheck.htm" id="formActivaAdmin">							
									<li id="linkMenuHijo"><a onclick="resumen()" id="linkMenuHijo"><p>Administrador Checklist</p></a></li>
									<input type="hidden" name="idUserAdmin" value="adminCheckFranquicias">
								</form>	
							</c:if>	
							<c:if test="${perfilReportes==1}">
								<li id="linkMenuHijo"><a id="linkMenuHijo"  href="#"><p>Reportes</p></a>
									<ul id="subMenuLinkMenuHijo">
										<li id="linkMenuHijoSub" onclick="javascript:location.href='reporteOnlineApertura.htm'"><p>¿Cómo operan mis sucursales?</li>
										<li id="linkMenuHijoSub" onclick="javascript:location.href='vistaCumplimientoVisitas.htm'"><p>¿Cómo supervisamos?</p></li>
									</ul>
								</li>				
							</c:if>																												
							</ul>
						</li>
					</ul>
				</div>
			</div>
	
			<div id="opcion">
				<div id="opcionMenu">
					<ul class="nav">
						<li><a id="linkMenu" href="carrusel7s.htm"> <img id="subir"
								src="${pageContext.request.contextPath}/images/menu/7s.png"
								alt="subir">&nbsp;&nbsp; 7S
						</a>
						</li>
					</ul>
				</div>
			</div>
		
		<!-- se crean div temporales para posibles llenados -->
			
			<div id="opcion2"></div>	
			
			
			
			<c:choose>
				<c:when test="${nomsucursal == 'Central'}">
					<div id="opcion1">
						<div id="opcionMenu">
							<div id="linkMenu2">
								<ul class="nav">
									<li><a id="linkMenu" href="#"> <img
											id="agendaCierreFuera"> &nbsp;&nbsp;Salir
									</a>
										<ul>
											<li id="linkMenuCierre"
												onclick="javascript:location.href='cierraSesion.htm'"><img
												id="agendaCierre"
												src="http://botonrojo.socio.gs/homebr/imgs/cerrar_sesion2.png"
												alt="agenda">&nbsp;&nbsp;Cerrar sesión</li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</c:when>
			</c:choose>
			
		</div>
	</div>
</body>





<!--
<div id="opcion"><div id="opcionMenu"><a id="linkMenu"  href="cargaForm.htm"><img id="subir" src="images/menu/subir_archivo.png" alt="subir">&nbsp;SUBIR ARCHIVO</a></div></div>
		<div id="opcion"><div id="opcionMenu"><a id="linkMenu" href="irBuscador.htm"><img id="saber" src="images/menu/para_saber.png" alt="saber">&nbsp;PARA SABER M&Aacute;S</a></div></div>
		<div id="opcion"><div id="opcionMenu"><a id="linkMenu" href="#"><img id="reportes" src="images/menu/reportes.png" alt="reportes">REPORTES</a></div></div>
		<div id="opcion"><div id="opcionMenu"><a id="linkMenu" href="#"><img id="agenda" src="images/menu/agenda.png" alt="agenda">&nbsp;AGENDA</a></div></div>
		<div id="opcion"><div id="opcionMenu"><a id="linkMenu" href="muestraFormatoChecklist.htm"><img id="checklist" src="images/menu/checklist.png" alt="checklist">&nbsp;CHECKLIST</a></div></div>
	
  -->
