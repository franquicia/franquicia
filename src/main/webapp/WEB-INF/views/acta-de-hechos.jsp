<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>



<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> ACTA DE HECHOS (CATÁLOGO DE MINUCIAS)</b></span>
    	</div>
    	<div class="page">
            <div class="contHome top60">
                <div class="border">  
                    <div class=" overflow mh500">   
                        <table class="items tbLugar">
                            <tbody>
                                <tr>   
                                    <td>
                                        <div><b>Lugar de los hechos</b></div>
                                    </td>
                                    <td>
                                        <div><b>${LUGAR_HECHOS}</b></div>
                                    </td> 
                                </tr>  
                                <tr>   
                                    <td>
                                        <div><b>Hora y fecha</b></div>
                                    </td>
                                    <td>
                                        <div><b>${FECHA}</b></div>
                                    </td> 
                                </tr>
                                <tr>   
                                    <td>
                                       <div><b>Dirección</b></div>
                                    </td>
                                    <td>
                                        <div><b>${DIRECCION}</b></div>
                                    </td> 
                                </tr>
                            </tbody>
                        </table> 
                    </div>  
                </div>

                <div class="text">
                    <b>
                        Los que suscriben la presente acta se reunieron con la finalidad de hacer constar que los bienes descritos en la relación anexa a la presente acta.
                    </b>
                </div>

                <div class="text">
                    <b>
                        Y que forma parte integrante de la presente acta fueron entregados al servicio público de recolección y transporte de basura de esta localidad, lo anterior, debido a que dichos bienes han dejado de ser funcionales para la operación que se realiza en Banco Azteca, S.A. así como la atención e imagen que se tiene para brindar en el servicio que ameritan nuestros clientes.
                    </b>
                </div>

                <div class="text">
                    <b>
                        Previa lectura de la presente acta con los involucrados y que se muestran al calce de la presente y no habiendo más que hacer se da por concluida a las HORA del DÍA, firmando de conformidad. Conservando el original de este documento la tienda y copia digital de su original el área de sistemas de Pago para cualquier revisión posterior requerida por cualquier autoridad.
                    </b>
                </div>

                <div class="border">  
                    <div class=" overflow mh300">   
                        <table class="items tbVobo">
                            <tbody>
                                <tr>   
                                    <td>
                                        <div><b>VoBo. Gerente de Tienda</b></div>
                                    </td>
                                    <td>
                                        <div><b>${VOBO}</b><!--<a href="#" class="Mfirma fRight">Ver Firma Digital</a> --></div>
                                    </td> 
                                </tr>  
                                <tr>   
                                    <td>
                                        <div><b>Destruye</b></div>
                                    </td>
                                    <td>
                                        <div><b>${DESTRUYE}</b><!--<a href="#" class="Mfirma fRight">Ver Firma Digital</a> --></div>
                                    </td> 
                                </tr>
                                <tr>   
                                    <td>
                                        <div><b>Testigo</b></div>
                                    </td>
                                    <td>
                                        <div><b>${TESTIGO1}</b><!--<a href="#" class="Mfirma fRight">Ver Firma Digital</a> --></div>
                                    </td> 
                                </tr>
                                <tr>   
                                    <td>
                                        <div><b>Testigo</b></div>
                                    </td>
                                    <td>
                                        <div><b>${TESTIGO2}</b><!--<a href="#" class="Mfirma fRight">Ver Firma Digital</a> --></div>
                                    </td> 
                                </tr>
                                <tr>   
                                    <td>
                                        <div><b>Evidencias</b></div>
                                    </td>
                                    <td>
                                    	<!--
                                    	<div><a href="#" class="MActa1"><b>Acta Hechos Minucias 01</b></a></div>
                                        <div><a href="#" class="MActa2"><b>Acta Hechos Minucias 02</b></a></div>
                                        <div><a href="#" class="MActa3"><b>Acta Hechos Minucias 03</b></a></div>
                                        -->
                                        <c:set var="nFilters" value="${1}" scope="request"/>
                                    	<c:forEach var="listaEvid" items="${listaEvid}">
                                    		<div><a href="#" class="MActa<c:out value="${nFilters}"/>"><b> <c:out value="${listaEvid.nomArchivo}"></c:out></b> </a></div>
                                    		<!--<c:out value="${nFilters}"/> -->
                                    		<!--<c:out value="${listaEvid.ruta}"/>-->
                                    		<c:set var="nFilters" value="${nFilters + 1}"  scope="request" />
                                    	</c:forEach>
                                        
                                    </td> 
                                </tr>
                            </tbody>
                        </table> 
                    </div>  
                </div>

        		<div class="botones"> 
                    <div class="tleft w50">
                        <a href="acta-de-hecho-minucias.htm" class="btn arrowl">
                            <div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
                            <div>VOLVER AL LISTADO</div>
                        </a> 
                    </div>
                </div>

                <!--MODAL-->
                <%-- <div id="acta1" class="modal">
                    <div class="w100">
                        <div class="tModal"><b>Acta Hechos Minucias 01</b></div>
                        <div class="cuadrobig">  
                            <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                            <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">
                        </div>
                    </div>
                    <div class="bModalUser"> 
                        <div class="tleft w50">
                            <a href="#" class="btn  bm arrowml simplemodal-close MActa3">
                                <div></div>
                                <div>FOTO ANTERIOR</div>
                            </a> 
                        </div>

                        <div class="tright w50"> 
                            <a href="#" class="btn bm arrowmr simplemodal-close MActa2">
                                <div></div>
                                <div>SIGUIENTE FOTO</div>
                            </a> 
                        </div> 
                    </div>
                </div>
                <div id="acta2" class="modal">
                    <div class="w100">
                        <div class="tModal"><b>Acta Hechos Minucias 02</b></div>
                        <div class="cuadrobig">  
                            <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                            <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">
                        </div>
                    </div>
                    <div class="bModalUser"> 
                        <div class="tleft w50">
                            <a href="#" class="btn  bm arrowml simplemodal-close MActa1">
                                <div></div>
                                <div>FOTO ANTERIOR</div>
                            </a> 
                        </div>

                        <div class="tright w50"> 
                            <a href="#" class="btn bm arrowmr simplemodal-close MActa3">
                                <div></div>
                                <div>SIGUIENTE FOTO</div>
                            </a> 
                        </div> 
                    </div>
                </div>
                <div id="acta3" class="modal">
                    <div class="w100">
                        <div class="tModal"><b>Acta Hechos Minucias 03</b></div>
                        <div class="cuadrobig">  
                            <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                            <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">
                        </div>
                    </div>
                    <div class="bModalUser"> 
                        <div class="tleft w50">
                            <a href="#" class="btn  bm arrowml simplemodal-close MActa2">
                                <div></div>
                                <div class="MActa2">FOTO ANTERIOR</div>
                            </a> 
                        </div>

                        <div class="tright w50"> 
                            <a href="#" class="btn bm arrowmr simplemodal-close MActa1">
                                <div></div>
                                <div>SIGUIENTE FOTO</div>
                            </a> 
                        </div> 
                    </div>
                </div> --%>
			
			<c:set var="cont" value="${1}" scope="request"/>
            <c:forEach var="listaEvid" items="${listaEvid}">
                <div id="acta<c:out value="${cont}"/>" class="modal">
                    <div class="w100">
                        <div class="tModal"><b>Acta Hechos Minucias 0<c:out value="${cont}"/></b></div>
                        <div class="cuadrobig">  
                            <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                            <img src="http://10.53.33.82<c:out value="${listaEvid.ruta}"/>" class="imgDefault">
                            <!-- <img src="http://10.53.33.82/franquicia/archivosActMinucias/48644418263511032019ImgMinucias_20190311062440.png" class="imgDefault"> -->
                        </div>
                    </div>
                    <!-- <div class="bModalUser"> 
                        <div class="tleft w50">
                            <a href="#" class="btn  bm arrowml simplemodal-close MActa2">
                                <div></div>
                                <div class="MActa2">FOTO ANTERIOR</div>
                            </a> 
                        </div>

                        <div class="tright w50"> 
                            <a href="#" class="btn bm arrowmr simplemodal-close MActa1">
                                <div></div>
                                <div>SIGUIENTE FOTO</div>
                            </a> 
                        </div> 
                    </div> -->
                </div>                    		
            	<c:set var="cont" value="${cont + 1}"  scope="request" />
            </c:forEach>

			<div id="firma" class="modal">
                    <div class="w100">
                        <div class="tModal"><b>Firma de Vo.Bo.</b></div>
                        <div class="cuadrobig">  
                            <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                            <img src="${pageContext.request.contextPath}/img/firma.png" class="imgDefault">
                        </div>
                    </div>
                    <!-- <div class="bModalUser"> 
                        <div class="tleft w50">
                            <a href="#" class="btn  bm arrowml">
                                <div></div>
                                <div>FOTO ANTERIOR</div>
                            </a> 
                        </div>

                        <div class="tright w50"> 
                            <a href="#" class="btn bm arrowmr">
                                <div></div>
                                <div>SIGUIENTE FOTO</div>
                            </a> 
                        </div> 
                    </div> -->
                </div>
            </div>
        </div>
        <!-- -->
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.MActa1').click(function (e) {
                        setTimeout(function() {
                            $('#acta1').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.MActa2').click(function (e) {
                        setTimeout(function() {
                            $('#acta2').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.MActa3').click(function (e) {
                        setTimeout(function() {
                            $('#acta3').modal({
                            });
                            return false;
                        }, 1);
                    });
                    
                    $('.Mfirma').click(function (e) {
                        setTimeout(function() {
                            $('#firma').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            }); 
        </script>
    </body>
</html>



