<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>7S</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> ACTA DE HECHOS DE DEPURACIÓN DE ACTIVOS FIJOS</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome">
				<div class="tcenter top60">
					<a href="activos-fijos-pdf.htm" class="Sboton MLactivo">
					    <img src="${pageContext.request.contextPath}/img/pdfIco.png">
					    CONSULTAR EL CATÁLOGO DE ACTIVOS FIJOS VIGENTES
					</a>
				</div>
				<c:choose>
	        	<c:when test="${datos == 1}">
		    		<div class="top60">
		    			<div class="tituloScont"><b>Selecciona la fecha para consultar el Acta de Hechos.</b></div><br>
		    			<div class="border borderLeft">  
							<div class=" overflow mh500">
								  
								<table class="items anexoDos">
									<tbody>
										<c:forEach var="lista" items="${lista}">
											<tr>
												<td>
													<div><a href="activos-fijos.htm?idActa=<c:out value="${lista.idActa}"></c:out>">Acta de Hechos <c:out value="${lista.fecha}"></c:out></a></div>
												</td> 
											</tr>
										</c:forEach>  
										
									</tbody>
								</table> 
								
							</div>  
					    </div>
		    		</div>
		    	</c:when>
			    <c:when test="${datos == 0}">
					<div class="contHome top60">
						<div class="buscar"><img src="${pageContext.request.contextPath}/img/7s/buscar.png"></div>
						<span class="sinDatos"><b> NO SE ENCONTRARON DETALLES PARA CONSULTA.</b></span>
						<div class="tituloScont"><b>No existe ningún acta para mostrar en este momento.</b></div><br>
						
					</div>
				</c:when>
		    	</c:choose>
    		
	    		<div class="botones"> 
					<a href="inicio7s.htm" class="btn arrowl">
						<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
						<div>VOLVER AL MENú</div>
					</a>   
				</div>
			</div>
    	</div>
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    </body>
</html>