<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    	
    	<script type="text/javascript">
	    	 function Hora() {
	             var hora = new Date()
	             var hrs = hora.getHours();
	             var min = hora.getMinutes();
	             var hoy = new Date();
	             var m = new Array();
	             var d = new Array()
	             var an= hoy.getYear();
	             m[0]="Enero";  m[1]="Febrero";  m[2]="Marzo";
	             m[3]="Abril";   m[4]="Mayo";  m[5]="Junio";
	             m[6]="Julio";    m[7]="Agosto";   m[8]="Septiembre";
	             m[9]="Octubre";   m[10]="Noviembre"; m[11]="Diciembre";
	             document.write(" "+hrs+":"+min+" ");
	             var fecha = new Date();
	             //alert("Día: "+fecha.getDate()+"\nMes: "+(fecha.getMonth()+1)+"\nAño: "+fecha.getFullYear());
	             //alert("Hora: "+fecha.getHours()+"\nMinuto: "+fecha.getMinutes()+"\nSegundo: "+fecha.getSeconds()+"\nMilisegundo: "+fecha.getMilliseconds());
	           }
	
	        function Dia() {
	             var hora = new Date()
	             var hrs = hora.getHours();
	             var min = hora.getMinutes();
	             var hoy = new Date();
	             var m = new Array();
	             var d = new Array()
	             var an= hoy.getYear();
	             m[0]="Enero";  m[1]="Febrero";  m[2]="Marzo";
	             m[3]="Abril";   m[4]="Mayo";  m[5]="Junio";
	             m[6]="Julio";    m[7]="Agosto";   m[8]="Septiembre";
	             m[9]="Octubre";   m[10]="Noviembre"; m[11]="Diciembre";
	             
	             document.write(hoy.getDate());
	             
	           }
	        function Mes() {
	             var hora = new Date()
	             var hrs = hora.getHours();
	             var min = hora.getMinutes();
	             var hoy = new Date();
	             var m = new Array();
	             var d = new Array()
	             var an= hoy.getYear();
	             m[0]="Enero";  m[1]="Febrero";  m[2]="Marzo";
	             m[3]="Abril";   m[4]="Mayo";  m[5]="Junio";
	             m[6]="Julio";    m[7]="Agosto";   m[8]="Septiembre";
	             m[9]="Octubre";   m[10]="Noviembre"; m[11]="Diciembre";
	             
	             document.write(m[hoy.getMonth()]);
	           }
	        function Anio() {
	             var hora = new Date()
	             var hrs = hora.getHours();
	             var min = hora.getMinutes();
	             var hoy = new Date();
	             var m = new Array();
	             var d = new Array()
	             var an= hoy.getYear();
	             m[0]="Enero";  m[1]="Febrero";  m[2]="Marzo";
	             m[3]="Abril";   m[4]="Mayo";  m[5]="Junio";
	             m[6]="Julio";    m[7]="Agosto";   m[8]="Septiembre";
	             m[9]="Octubre";   m[10]="Noviembre"; m[11]="Diciembre";
	             
	             document.write(hoy.getFullYear());
	           }
    	</script>
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="sbigt"><b> ACTA DE ENTREGA PARA CONCENTRACIÓN DE EXPEDIENTE</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome top60">
	    		
	    			<div class="sbModal">
	    				${fecha2}
		    			<%-- <a href="acta-de-entrega-para-concentracion.htm" class="titprint"><img src="${pageContext.request.contextPath}/img/cimbpresora.png"></a> --%>
		    		</div>

	    			<br>
	                <div class="border">  
						<div class=" overflow mh300 w100">   
							<table class="items BiModal">
								<tbody>
									<tr>   
										<td><b>Nombre Gerente</b></td>
										<td><b>${nomGerente}</b></td>
									</tr>  
									<tr>   
										<td><b>Número de Credencial</b></td>
										<td><b>${numGerente}</b></td> 
									</tr>
									<tr>   
										<td><b>Nombre Regional</b></td>
										<td><b>${nomRegional}</b></td> 
									</tr>
									<tr>   
										<td><b>Número de Credencial</b></td>
										<td><b>${numRegional}</b></td>
									</tr>
								</tbody>
							</table> 
						</div>
				    </div><br>

 
					Gerente hace constar en la presente acta que todos los expedientes físicos tienen más de dos años de antigüedad y que los clientes a que corresponden dichos expedientes no han realizado operaciones o movimientos durante los últimos dos años, circunstancia que ha verificado el área de auditoría y que coinciden con los registros de la Institución.
					<br><br>
					En la ${ciudad}, siendo las  ${hora} horas del día <script>Dia();</script> del mes de <script>Mes();</script> del año <script>Anio();</script>, se hace constar que el -Gerente- de la Sucursal Número ${sucursal} de Banco Azteca S.A., Institución de Banca Múltiple, de nombre ${nomGerente} con número de credencial ${numGerente} (en lo sucesivo el -Gerente-) hace entrega de los expedientes físicos al -Regional- de nombre ${nomRegional}, con número de credencial ${numRegional}, quien recibe para su concentración, los cuales se integran por el nombre del cliente, número de cliente único y tipo de operación.
					<br><br><br>			 

					<div class="sbModal">Comentarios</div><br>
					<div class="cdescripcion">
						<div class="padFooter tleft">
							<b>${descripcion}</b>
						</div>
					</div>
					<br><br>


	    		<div class="botones"> 
					<a href="acta-entrega-expediente.htm" class="btn arrowl">
						<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
						<div>VOLVER AL LISTADO</div>
					</a>   
				</div>
			</div>
    	</div>



    	<!--MODAL-->
        <div id="limpieza" class="modal">
            <div class="cuadrobig">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="tsModal">Número de Sucursal 477</div>
	                <div class="sbModal">Enero 2018</div><br>
	                <div class="border">  
						<div class=" overflow mh300 w100"> 
						    <table class="items ActModal ">
                                <tbody>
								    <tr>
								      <td><b>Nombre Gerente</b></td>
								      <td><b>Marco Antonio Layún</td>
								    </tr>
								    <tr>
								      <td><b>Número de Credencial</b></td>
								      <td><b>565666</b></td>
								    </tr> 
								    <tr style="border-top: 4px solid #000000;">
								      <td><b>Nombre Regional</b></td>
								      <td><b>José Luis Villa Ruíz</b></td>
								    </tr>
								    <tr>
								      <td><b>Número de Credencial</b></td>
								      <td><b>204567</b></td>
								    </tr>
                                </tbody>
                            </table>
						</div>
				    </div><br>

				    <div class="sbModal">Comentarios</div><br>
					<div class="cdescripcion">
						<div class="padFooter">
							<b> 
								Los expedientes fuerno concentrados y entregados a la persona correspondiente.
							</b>
						</div>
					</div>
				</div>
           </div>
            <div class="bModalUser"> 
				<div class="tleft w50">
    				<a href="#" class="btn  bm arrowml">
    					<div></div>
    					<div>FOLIO ANTERIOR</div>
    				</a> 
                </div>
                <div class="cMimpresora">
                	<a href="acta-de-entrega-para-concentracion.htm"><img src="${pageContext.request.contextPath}/img/cimpresora.png"></a>
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr">
                        <div></div>
                        <div>SIGUIENTE FOLIO</div>
                    </a> 
                </div> 
			</div>
        </div>
        <!-- -->

        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    	<!--MODAL-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.MLimpieza').click(function (e) {
                        setTimeout(function() {
                            $('#limpieza').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            }); 
        </script>
    </body>
</html>



