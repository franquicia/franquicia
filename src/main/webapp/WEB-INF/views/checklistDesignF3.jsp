<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<form:form method="post" action="asignacionChecklist.htm">
	<input type="hidden" id="idChecklist" name="idChecklist" value="${idChecklist}" />
	<input type="hidden" id="arbolAdmin" name="arbolAdmin" value="${arbolAdmin }" />
	<input type="hidden" id="numVersion" name="numVersion" value="${checklist.numVersion}" />
	<input type="hidden" id="origen" name="origen" value="true" />
	
	
	<!-- contenedor para mostrar el formulario de la definicion del arbol de decision -->
	<div class="seccionArbol" id="contenedorArbol"
		style=" background: #fff; z-index: 0;">
		<h2>Titulo del checklist: ${nombreChecklist }</h2>
		<h3>Configuración de preguntas</h3>
		<!-- lista donde se mostran las preguntas de la vista previa -->
		<h3>Preguntas</h3>

		<table class="tabla">
			<thead>
				<tr class="encabezado">
					<th>Respuesta</th>
					<th>Evidencia</th>
					<th style='width: 210px;'>Tipo de evidencia</th>
					<th>Evidencia obligatoria</th>
					<th>Observaciones</th>
					<th>Acciones</th>
					<th style='width: 210px;'>Pregunta siguiente</th>
					<th>Etiqueta Evidencia</th>
					<th>Estatus</th>
					
				</tr>

			</thead>
			<tbody id="preguntasArbol">

			</tbody>
		</table>
		<!--  
		<ul id="preguntasArbol">
			</ul>
			-->

		
	</div>
	<div style="margin-top:1.5%; height: 50px; width: 20%; float:right;">
	
		<button class="logEnviar-btn" style="width: 150px;" id="continuar" name="continuar" value="${nombreChecklist}">Continuar</button>
	</div>
	
	<div style="margin-top:1.5%; height: 50px; width: 20%; float:right;">
		<button class="logEnviar-btn" style="width: 150px;" id="guardar" name="guardar" >Guardar</button>
	</div>
</form:form>
	
	
	<form:form  name="formAtras" method="POST" action="checklistDesignF2.htm">
	
		<input type="hidden" id="idChecklistAtras" name="idChecklist" value="${idChecklist}" />
		
		<div style="margin-top:1.5%; height: 50px; width: 20%; float:right;">
			<button class="logEnviar-btn" style="width: 150px; " id="asignarChecklist" name="atras" >Atras</button>
		</div>
	</form:form>

	