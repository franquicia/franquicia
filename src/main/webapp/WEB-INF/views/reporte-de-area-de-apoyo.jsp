<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> REPORTE DE ÁREAS DE APOYO</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome top60">
    			<div class="tituloScont"><b>Selecciona el Folio para consultar los detalles.</b></div><br>
    			<div class="border borderLeft">  
					<div class=" overflow mh500">   
						<table class="items anexoDos">
							<tbody>
								
								<c:forEach var="lista" items="${lista}">
										<tr>
											<td>
												<div><a href="datalle-apoyo.htm?folio=<c:out value="${lista.folio}" ></c:out>" >Folio <c:out value="${lista.folio}" ></c:out> - <c:out value="${lista.fecha}" ></c:out> </a></div>
											</td> 
										</tr>
									</c:forEach>  
								
							</tbody>
						</table> 
					</div>  
			    </div>
			    
	    		<div class="botones"> 
					<a href="inicio7s.htm" class="btn arrowl">
						<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
						<div>VOLVER AL MENÚ</div>
					</a>   
				</div>
			</div>
    	</div>
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7sjquery.js"></script>
    </body>
</html>



