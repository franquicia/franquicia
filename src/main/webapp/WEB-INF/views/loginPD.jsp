<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script>
		$(document).ready( function() {
			$('#txtUsername').val('');
			$('#txtLlaveMaestra').val('');
		});
	</script>
	<div class="ContLogin">
		<div class="imagen"></div>
		<div class="login">
			<div class="titHome">
				Bienvenido a<br> <strong>Administración de Pedestal Digital</strong>
			</div>

			<form:form action="validaUsuarioPD.htm" method="POST" autocomplete="off">
				<input type="hidden" value="true" name="valida" />
				<div class="divLogin">
					<div class="subtit">Iniciar sesión</div>
					<div>
						<form:input type="text" class="inputlogin" id="txtUsername" path="user" placeholder="Usuario" />
					</div>
					<!--
					<div>
						<form:input type="text" class="inputlogin" id="txtLlaveMaestra" path="pass" placeholder="Llave maestra" />
					</div>
					-->
					<div class="txtRojo">${invalido}</div>
					<div>
						<input type="submit" class="btnLogin" value="Ingresar">
					</div>
				</div>
			</form:form>

		</div>
	</div>
</body>
</html>