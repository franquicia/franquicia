<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> PROTOCOLO DE MEDICIÓN 7S</b></span>
    	</div>

    	<div class="page">
    		<div class="contHome">
    		    <div class="tituloScont"><b>Selecciona la Semana del Mes que deseas consultar.</b></div>
    			
    			<c:set var="nFilters" value="${1}" scope="request"/>
                <c:forEach var="meses" items="${meses}">
                		
	                	<div class="tituloSbcont"><b><c:out value="${meses}"/> 2019</b></div>
		    			<div class="border borderLeft">  
							<div class=" overflow mh500">   
								<table class="items semanas">
									<tbody>
										<c:forEach var="algo" items="${A2019}">
											
											<c:choose>
	            							<c:when test="${nFilters == algo.mes}">
												<tr>   
													<td>
														
														<%-- <a href="protocolo-semana-uno.htm"><b>Semana <c:out value="${algo.semana}"/></b></a> --%>
														<b>Semana <c:out value="${algo.semana}"/></b>
													</td> 
													<td><b><c:out value="${algo.calificacion}"/>%</b></td>
													<td>
														<c:choose>
	            										<c:when test="${algo.bandera == 1}">
															<div class="estado ok"></div>
														</c:when>
														</c:choose>
														<c:choose>
	            										<c:when test="${algo.bandera == 0}">
															<div class="estado warning"></div>
														</c:when>
														</c:choose>
														<c:choose>
	            										<c:when test="${algo.bandera == 2}">
															<div class="estado "></div>
														</c:when>
														</c:choose>
													</td> 
												</tr>
											</c:when>
											</c:choose>
										</c:forEach>
										  
										
									</tbody>
								</table> 
							</div>  
					    </div>
				    
                                    		
                    <c:set var="nFilters" value="${nFilters + 1}"  scope="request" />
                </c:forEach>
                
                
                <c:set var="nFilters2" value="${1}" scope="request"/>
                <c:forEach var="meses" items="${meses}">
                		
	                	<div class="tituloSbcont"><b><c:out value="${meses}"/> 2020</b></div>
		    			<div class="border borderLeft">  
							<div class=" overflow mh500">   
								<table class="items semanas">
									<tbody>
										<c:forEach var="algo" items="${A2020}">
											
											<c:choose>
	            							<c:when test="${nFilters2 == algo.mes}">
												<tr>   
													<td>
														
														<%-- <a href="protocolo-semana-uno.htm"><b>Semana <c:out value="${algo.semana}"/></b></a> --%>
														<b>Semana <c:out value="${algo.semana}"/></b>
													</td> 
													<td><b><c:out value="${algo.calificacion}"/>%</b></td>
													<td>
														<c:choose>
	            										<c:when test="${algo.bandera == 1}">
															<div class="estado ok"></div>
														</c:when>
														</c:choose>
														<c:choose>
	            										<c:when test="${algo.bandera == 0}">
															<div class="estado warning"></div>
														</c:when>
														</c:choose>
														<c:choose>
	            										<c:when test="${algo.bandera == 2}">
															<div class="estado "></div>
														</c:when>
														</c:choose>
													</td> 
												</tr>
											</c:when>
											</c:choose>
										</c:forEach>
										  
										
									</tbody>
								</table> 
							</div>  
					    </div>
				    
                                    		
                    <c:set var="nFilters2" value="${nFilters2 + 1}"  scope="request" />
                </c:forEach>
    			<!-- <div class="tituloSbcont"><b>Febrero 2019</b></div>
    			<div class="border borderLeft">  
					<div class=" overflow mh500">   
						<table class="items semanas">
							<tbody>
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 4</b></a>
									</td> 
									<td><b>100%</b></td>
									<td>
										<div class="estado ok"></div>
									</td> 
								</tr>  
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 3</b></a>
									</td> 
									<td><b>0%</b></td>
									<td>
										<div class="estado "></div>
									</td>  
								</tr>
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 2</b></a>
									</td> 
									<td><b>0%</b></td>
									<td>
										<div class="estado "></div>
									</td> 
								</tr>
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 1</b></a>
									</td> 
									<td><b>0%</b></td>
									<td>
										<div class="estado "></div>
									</td>
								</tr>
							</tbody>
						</table> 
					</div>  
			    </div>

			    <div class="tituloSbcont"><b>Enero 2019</b></div>
    			<div class="border borderLeft">  
					<div class=" overflow mh500">   
						<table class="items semanas">
							<tbody>
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 4</b></a>
									</td> 
									<td><b>100%</b></td>
									<td>
										<div class="estado ok"></div>
									</td> 
								</tr>  
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 3</b></a>
									</td> 
									<td><b>60%</b></td>
									<td>
										<div class="estado warning"></div>
									</td>  
								</tr>
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 2</b></a>
									</td> 
									<td><b>100%</b></td>
									<td>
										<div class="estado ok"></div>
									</td> 
								</tr>
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 1</b></a>
									</td> 
									<td><b>100%</b></td>
									<td>
										<div class="estado ok"></div>
									</td>
								</tr>
							</tbody>
						</table> 
					</div>  
			    </div>

			    <div class="tituloSbcont"><b>Diciembre 2018</b></div>
    			<div class="border borderLeft">  
					<div class=" overflow mh500">   
						<table class="items semanas">
							<tbody>
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 4</b></a>
									</td> 
									<td><b>100%</b></td>
									<td>
										<div class="estado ok"></div>
									</td> 
								</tr>  
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 3</b></a>
									</td> 
									<td><b>100%</b></td>
									<td>
										<div class="estado ok"></div>
									</td>  
								</tr>
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 2</b></a>
									</td> 
									<td><b>100%</b></td>
									<td>
										<div class="estado ok"></div>
									</td> 
								</tr>
								<tr>   
									<td>
										<a href="protocolo-semana-uno.htm"><b>Semana 1</b></a>
									</td> 
									<td><b>100%</b></td>
									<td>
										<div class="estado ok"></div>
									</td>
								</tr>
							</tbody>
						</table> 
					</div>  
			    </div> 

	    		<div class="paginador fright">
	                <a href="#">1</a>
	                <span>/</span> 
	                <a href="#">10</a>
	                <a href="#">
	                    <img src="${pageContext.request.contextPath}/img/parrow.png" class="paginadorArroW">
	                </a>
	            </div><br>
    			-->
	    		<div class="botones"> 
					<a href="inicio7s.htm" class="btn arrowl">
						<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
						<div>VOLVER AL MENÚ</div>
					</a>   
				</div>
				
			</div>
    	</div>
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    </body>
</html>



