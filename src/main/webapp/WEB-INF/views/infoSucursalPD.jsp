<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script type="text/javascript">
		var contextPath = '${pageContext.request.contextPath}';
		var siStr = '${siStr}';
		var stStr = '${stStr}';
		var _idTableta = '${idTableta}';

		var jsonSI = siStr.length > 0 ? JSON.parse(siStr) : JSON.parse('[]');
		var jsonST = stStr.length > 0 ? JSON.parse(stStr) : JSON.parse('[]');

		$(document).ready(function() {
			showLoadingSpinner();
			loadSucursalData(jsonSI, jsonST);
			loadLicenciasSuc(jsonSI);

			$('#goToInfoTableta').attr('href', 'loadTabletInfo.htm?idTableta=' + _idTableta);
			$('#goPreviousInfoTablet').attr('href', 'loadTabletInfo.htm?idTableta=' + _idTableta);
			$('#goToInfoSucursal').attr('href', 'loadSucursalInfo.htm?idTableta=' + _idTableta);

			$('.contenedorTab, .tab[tab=""]').hide();
			$('.contenedorTab[contenedorTab="1"]').show();
			$('.tab').click(function () {
				let tab = $(this);
				$('.tab').removeClass('active');
				tab.addClass('active');
				$('.contenedorTab').hide();
				$('.contenedorTab[contenedorTab="' + tab.attr('tab') + '"]').show();
			});

			// paginado
			var dataSrc = [];
			$('#tblSucursal').DataTable({
				'initComplete': function () {
					var api = this.api();
					// Populate a dataset for autocomplete functionality
					// using data from first, second and third columns
					api.cells('tr', [0, 1, 2]).every(function () {
						// Get cell data as plain text
						var data = $('<div>').html(this.data()).text();
						if (dataSrc.indexOf(data) === -1) {
							dataSrc.push(data);
						}
					});
					// Sort dataset alphabetically
					dataSrc.sort();
					// Initialize Typeahead plug-in
					$('.dataTables_filter input[type="search"]', api.table().container())
						.typeahead({
							source: dataSrc,
							afterSelect: function (value) {
								api.search(value).draw();
							}
						});
				},
				"ordering": false,
				responsive: true,
				language: {
					paginate: {
						first: 'Premier',
						previous: '<img src="../../img/iconos/firstIco.svg" class="flechas1" />',
						next: '<img src="../../img/iconos/nextIco.svg" class="flechas2" />',
						last: 'Dernier',
					},
					search: 'Buscar:',
				},
			});
			$('.docTab').click(function () {
				$(this).addClass("active");
				$('.docTab').not(this).removeClass('active');
				var tabNum = $(this).attr('tab');
				$('.tabCont').removeClass('active');
				$('.tabCont[tab=' + tabNum + ']').addClass('active');
			});
		});
	</script>

	<div class="clear"></div>
	<div class="titulo">
		<div class="ruta">
			<a href="estatusGeneral.htm">Página Principal</a> /
			<a href="estatusTableta.htm">Estatus de Tabletas</a> /
			<a id="goToInfoTableta" href="infoTabletaPD.htm">Información de la tableta</a> /
			<a id="goToInfoSucursal" href="infoSucursalPD.htm" id="toInfoSucursal" >Información de sucursal</a>
		</div>
		Información de sucursal
	</div>

    <!-- Contenido -->
	<div class="contSecc">
		<div class="titSec">Información de Sucursal</div>
		<div class="divInfoSucursal">
			<div class="divCol2 divCol2SB">
				<div class="w48">
					<div class="iconoSucursal">
						<img src="../../img/iconosSucursal/iconoSucursal.svg" alt="iconoSucursal">
					</div>
					<div class="etiqueta">Sucursal:</div>
					<div class="infoSucursal" id="indiSucursal"></div>
				</div>
				<div class="w48">
					<div class="iconoSucursal">
						<img src="../../img/iconosSucursal/iconoCC.svg" alt="iconoCentroCostos">
					</div>
					<div class="etiqueta">Centro de costos:</div>
					<div class="infoSucursal" id="indiCeco"></div>
				</div>
				<div class="w48">
					<div class="iconoSucursal">
						<img src="../../img/iconosSucursal/iconoDireccion.svg" alt="iconoDireccion">
					</div>
					<div class="etiqueta">Dirección:</div>
					<div class="infoSucursal" id="indiDireccion"></div>
				</div>
				<div class="w48">
					<div class="iconoSucursal">
						<img src="../../img/iconosSucursal/iconoFechaInstalacion.svg" alt="iconoCentroCostos">
					</div>
					<div class="etiqueta">
						Fecha de instalación<br> de pedestal:
					</div>
					<div class="infoSucursal" id="indiFechaInstalacion"></div>
				</div>
			</div>
		</div>

		<div class="menuTabs" style="background-color: #fff;">
			<div class="contenedorTabs">
				<div class="tab active" tab="1">
					<div></div>
					<div>Archivos Cargados</div>
				</div>
				<c:if test="${showTab == 0}">
					<div class="tab" tab="2">
						<div></div>
						<div>Licencias de funcionamiento</div>
					</div>
				</c:if>
			</div>
		</div>
		<div class="contenedorTab" contenedortab="1" style="display: block;">
			<div class="titSec">Lista de archivos cargados</div>
			<div class="divResultadosZona scroll">
				<table id=tblSucursal class="tblGeneral display">
					<thead>
						<tr>
							<th>Folio de env&iacute;o</th>
							<th>Nombre del documento</th>
							<th>Tipo de env&iacute;o</th>
							<th>Estatus del documento</th>
							<th>Fecha de creaci&oacute;n</th>
							<th>Fecha de recepci&oacute;n</th>
							<th>Fecha de visualizaci&oacute;n</th>
						</tr>
					</thead>
					<tbody id="tblBody"></tbody>
				</table>
			</div>
			<div class="btnCenter contBtnExp4">
				<a href="#" class="link btnRegresarZona" id="goPreviousInfoTablet">Regresar</a>
				<a href="downloadSucursalTable.json" id="downloadSucursalTable" class="btnA" style="max-width: 200px; height: 42px;">Descargar lista de archivos</a>
			</div>
		</div>
		<c:if test="${showTab == 0}">
			<div class="contenedorTab" contenedortab="2" style="display: block;">
				<div class="titSec">Lista de licencias de funcionamiento</div>
				<div class="docsCont">
					<div class="docTabs" id="listLicDocs">
					</div>
					<div class="tabConts" id="listContainer">
					</div>
				</div>
			</div>
		</c:if>
	</div>

	<!-- LOADER -->
	<div class="loaderContainer" style="display: none;">
		<div class="ml-loader loader" style="">
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
		</div>
	</div>
	<div class="blackScreen" style="display: none;"></div>

</body>
</html>