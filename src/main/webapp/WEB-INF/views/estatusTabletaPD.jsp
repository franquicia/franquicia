<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<script type="text/javascript">
	var contextPath = '${pageContext.request.contextPath}';
	var generalNegVal = '${generalNegVal}';
	var propiosNegVal = '${propiosNegVal}';
	var tercerosNegVal = '${tercerosNegVal}';
	var sucNotFound = '${sucNotFound}';

	var _containerT, _nivelT;
	var _containerP, _nivelP;
	var _containerC, _nivelC;

	var _cecos, _negocio, _boxId;
	var foliosJsonData;
	var sucursalPass = true;
	var _objBackup, _objNameBackup;

	var verde1 = 'rgb(42, 99, 61)';
	var rojo = 'rgb(217, 97, 105)';

	$(document).ready(function() {
		updateStatusTablets();

		_negocio = 41; // Pruebas
		// getIndicadoresInfoET();

		$('#btnTerritorio').click(function(e) {
			_nivelT = 0;
			_cecos = '0';
			_boxId = 1;
			getTerriListEstTableta();
		});

		$('#btnPais').click(function(e) {
			_nivelP = 0;
			_cecos = '0';
			_boxId = 2;
			getPaisListEstTableta();
		});

		$('#buscarPorSucursal').on('keypress', function(e) {
			if (e.which === 13 && sucursalPass) {
				var val = $('#buscarPorSucursal').val();
				var list = $('#listaLanzamientos').find('.divEliminar');

				if (list.length > 0 && val.length < 5) {
					for (var x = 0; x < list.length; x++) {
						var pass = true;
						var id = $(list[x]).attr('id');
						if (id === val) {
							pass = false;
							break;
						}
					}

					if (pass) {
						$('#listaLanzamientos').prepend(
							'<div id="' + val + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1% 5px 1%;">' +
							'<a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">Folio: ' + val + '</a></div>'
						);
						sucursalPass = false;
					}
				} else if (list.length === 0 && val.length < 5) {
					$('#listaLanzamientos').prepend(
						'<div id="' + val + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1% 5px 1%;">' +
						'<a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">Folio: ' + val + '</a></div>'
					);
					sucursalPass = false;
				} else {
					return false;
				}
			}
		});

		$('#btnNuevaBusqueda').click(function(e) {
			$('#buscarPorSucursal').val('');
			$('#buscarPorSucursal').prop('disabled', false);
			$('#btnTerritorio').removeClass('btnDistribDis');
			$('#btnTerritorio').addClass('btnDistrib');
			$('#btnPais').removeClass('btnDistribDis');
			$('#btnPais').addClass('btnDistrib');

			$('#btnTerritorio').click(function(e) {
				_nivelT = 0;
				_cecos = '0';
				_boxId = 1;
				getTerriListEstTableta();
			});

			$('#btnPais').click(function(e) {
				_nivelP = 0;
				_cecos = '0';
				_boxId = 2;
				getPaisListEstTableta();
			});

			var dkEstatusTableta = new Dropkick('#estadoTableta');
			var dkRendimientoTableta = new Dropkick('#rendimientoTableta');

			dkEstatusTableta.select(0);
			dkRendimientoTableta.select(0);

			$('#listaLanzamientos').html('');
		});

		$('#btnAplicar').click(function(e) {
			validateFiltersET();
		});

		$('#exportarHistorial').on('click', estTabletExportarHistorial);

		// ////////////////////////////////// //
		// Nuevo modulo de estatus de tableta //
		// ////////////////////////////////// //

		$(".contenedorTab, .tab[tab='']").hide();
		$(".contenedorTab[contenedorTab='1']").show();
		$(".tab").click(function() {
			let tab = $(this);
			$(".tab").removeClass('active');
			tab.addClass('active');
			$(".contenedorTab").hide();
			$(".contenedorTab[contenedorTab='" + tab.attr('tab') + "']").show();
		});

		getIndicadoresEstTab();

		$('#generalEstTitle').html('Estatus por negocio');
		$('#propiosEstTitle').html('Estatus por territorio');
		$('#tercerosEstTitle').html('Estatus por territorio');

		$('.btnPropios').click(function() {
			$('.tabGeneral').removeClass('active');
			$('.tabPropios').addClass('active');
			$('.contenedorTabGeneral').hide();
			$('.contenedorTabPropios').show();
		});

		$(".btnTerceros").click(function(){
			$(".tabGeneral").removeClass('active');
			$('.tabTerceros').addClass('active');
			$(".contenedorTabGeneral").hide();
			$(".contenedorTabTerceros").show();
		});

		$('.buscarSuc').on('click', function () {
			if ($(this).parent().parent().attr('id') === 'miForm1') {
				var _text = $('#busca1').val();
				if (_text.match(/^[0-9]*$/) == null) {
					$('#busca1').val('');
					return false;
				}

				if (_text.length > 4) {
					$('#busca1').val(_text.substring(0, 4));
					return false;
				}

				$(this).parent().parent().submit();
			} else if ($(this).parent().parent().attr('id') == 'miForm2') {
				var _text = $('#busca2').val();
				if (_text.match(/^[0-9]*$/) == null) {
					$('#busca2').val('');
					return false;
				}

				if (_text.length > 4) {
					$('#busca2').val(_text.substring(0, 4));
					return false;
				}

				$(this).parent().parent().submit();
			} else if ($(this).parent().parent().attr('id') == 'miForm3') {
				var _text = $('#busca3').val();
				if (_text.match(/^[0-9]*$/) == null) {
					$('#busca3').val('');
					return false;
				}

				if (_text.length > 4) {
					$('#busca3').val(_text.substring(0, 4));
					return false;
				}

				$(this).parent().parent().submit();
			}
		});

		$('.buscadorSucursal').on('input', function () {
			if (this.id == 'miForm1') {
				var _text = $('#busca1').val();
				if (_text.match(/^[0-9]*$/) == null) {
					$('#busca1').val('');
					return false;
				}

				if (_text.length > 4) {
					$('#busca1').val(_text.substring(0, 4));
					return false;
				}
			} else if (this.id == 'miForm2') {
				var _text = $('#busca2').val();
				if (_text.match(/^[0-9]*$/) == null) {
					$('#busca2').val('');
					return false;
				}

				if (_text.length > 4) {
					$('#busca2').val(_text.substring(0, 4));
					return false;
				}
			} else if (this.id == 'miForm3') {
				var _text = $('#busca3').val();
				if (_text.match(/^[0-9]*$/) == null) {
					$('#busca3').val('');
					return false;
				}

				if (_text.length > 4) {
					$('#busca3').val(_text.substring(0, 4));
					return false;
				}
			}
		});

		$('.buscadorSucursal').on('submit', function () {
			if (this.id == 'miForm1') {
				var _text = $('#busca1').val();

				$('#contenedorGrafica0').hide();
				$('#sucursalContainer0').show();
				$('#btnContainer0').show();

				$('.expStatus00').off();
				$('.expStatus00').on('click', function () {
					exportDataCecoExcel('2', _text);
				});

				if (_text != '' || _text != undefined) {
					loadSucursalesBySearch(0, _text, propiosNegVal); // generalNegVal
				} else {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
							'Debe agregar un n&uacute;mero de sucursal v&aacute;lido de m&aacute;ximo 4 caracteres' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
				}
			} else if (this.id == 'miForm2') {
				var _text = $('#busca2').val();

				$('#graphContainer1').hide();
				$('#zoneContainer1').hide();
				$('#sucursalContainer1').show();
				$('#btnContainer1').show();

				$('#btnBack2').off();
				$('#btnBack2').on('click', function () {
					exportDataCecoExcel('2', _text);
				});

				if (_text != '' || _text != undefined) {
					loadSucursalesBySearch(1, _text, propiosNegVal);
				} else {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
							'Debe agregar un n&uacute;mero de sucursal v&aacute;lido de m&aacute;ximo 4 caracteres' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
				}
			} else if (this.id == 'miForm3') {
				var _text = $('#busca3').val();

				$('#graphContainer2').hide();
				$('#zoneContainer2').hide();
				$('#sucursalContainer2').show();
				$('#btnContainer2').show();

				$('#btnBack3').off();
				$('#btnBack3').on('click', function () {
					exportDataCecoExcel('2', _text);
				});

				if (_text != '' || _text != undefined) {
					loadSucursalesBySearch(2, _text, tercerosNegVal);
				} else {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
							'Debe agregar un n&uacute;mero de sucursal v&aacute;lido de m&aacute;ximo 4 caracteres' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
				}
			}

			/**
			$('#contenedorGrafica').hide();
			if (this.id == 'miForm1') {
				var _text = $('#busca1').val();
				if (_text != '' || _text != undefined) {
					loadSucInfoDetailBySearch(_text);
				} else {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
							'Debe agregar un n&uacute;mero de sucursal v&aacute;lido de m&aacute;ximo 4 caracteres' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
				}
			} else if (this.id == 'miForm2') {
				var _text = $('#busca2').val();
				if (_text != '' || _text != undefined) {
					loadSucInfoDetailBySearch(_text);
				} else {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
							'Debe agregar un n&uacute;mero de sucursal v&aacute;lido de m&aacute;ximo 4 caracteres' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
				}
			} else if (this.id == 'miForm3') {
				var _text = $('#busca3').val();
				if (_text != '' || _text != undefined) {
					loadSucInfoDetailBySearch(_text);
				} else {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
							'Debe agregar un n&uacute;mero de sucursal v&aacute;lido de m&aacute;ximo 4 caracteres' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
				}
			}
			*/

			return false;
		});

		if (sucNotFound != '' && sucNotFound == '1') {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
					'La sucursal no pudo ser encontrada' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}

		$('#expStatusA').off();
		$('#expStatusA').on('click', function () {
			exportDataCecoExcel('0', generalNegVal);
		});

		$('#expStatusB').off();
		$('#expStatusB').on('click', function () {
			exportDataCecoExcel('1', propiosNegVal);
		});

		$('#expStatusC').off();
		$('#expStatusC').on('click', function () {
			exportDataCecoExcel('1', tercerosNegVal);
		});
	});
	</script>

	<div class="clear"></div>
    <div class="titulo">
		<div class="ruta">
			<a href="estatusGeneral.htm">Página Principal</a> /
			<a href="estatusTableta.htm">Estatus de Tabletas</a> 
		</div>
		Estatus de Tabletas
	</div>

	<!-- Contenido -->
	<div class="contSecc">

		<!-- NEW CODE -->
		<div class="contenedorTabs">
			<div class="tab active tabGeneral" tab="1">
				<div></div><div>General</div>
			</div>
			<div class="tab tabPropios" tab="2">
				<div></div><div>Propios</div>
			</div>
			<div class="tab tabTerceros" tab="3">
				<div></div><div>Terceros</div>
			</div>
		</div>
		<div class="contenedorTab contenedorTabGeneral" contenedorTab="1">
			<div class="titulos">Estatus de Tabletas</div>
			<div class="divRow">
				<div class="card justifyStart">
					<div class="cardContenedor cardBordeVerde cardEstTab">
						<div class="cardContTexto">
							<div class="cardTitulo" id="generalEst01"></div>
							<div>Tabletas funcionando correctamente</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="cardContenedor cardBordeRojo cardEstTab">
						<div class="cardContTexto">
							<div class="cardTitulo" id="generalEst02"></div>
							<div>Tabletas que presentan algún problema</div>
						</div>
					</div>
				</div>
				<div class="card justifyEnd">
					<div class="cardContenedor cardBordeVerde2 cardEstTab">
						<div class="cardContTexto">
							<div class="cardTitulo" id="generalEst03"></div>
							<div>Tabletas que se encuentran en mantenimiento</div>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div>
				<strong class="titulos" id="generalEstTitle"></strong>
			</div>
			<div class="contenedorFormularioEstatu">
				<div>
					Buscar sucursal: <br>
					<form id="miForm1" class="buscadorSucursal">
						<div class="buscador">
							<input type="text" id="busca1" class="buscSucInput" placeholder="Ingrese la sucursal que desea consultar">
							<input type="button" class="buscar buscarSuc" value="">
						</div>
					</form>
				</div>
			</div>

			<!-- GRAFICAS -->
			<div id="contenedorGrafica0" class="contenedorGrafica">
				<div class="legend">
					<div>
						<div class="legendBgVerde"></div>
						<div>
							<strong>Correcto</strong>
						</div>
					</div>
					<div>
						<div class="legendBgRojo"></div>
						<div>
							<strong>Con fallas</strong>
						</div>
					</div>
				</div>

				<div class=graficasNegocios>
					<div class="contPie">
						<div class="divTablaGraficas">
							<table class="tblInfoGraficas">
								<thead>
									<tr>
										<th>Total de Zonas</th>
										<th>Total de Sucursales</th>
										<th>En mantenimiento</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="verdeClaro2 bold" id="numTabMantGen2A"></td>
										<td class="verdeOscuro2 bold" id="numTabMantGen2B"></td>
										<td class="gris3 bold" id="numTabMantGen2C"></td>
									</tr>
								</tbody>
							</table>
						</div>
						<a class="btnVerDetalle2 btnPropios"><strong>Propios</strong></a>
						<div id="grafGen02" style="height: 250px; width: 100%;"></div>
						<div class="clear"></div>
					</div>
					<div class="contPie ">
						<div class="divTablaGraficas">
							<table class="tblInfoGraficas">
								<thead>
									<tr>
										<th>Total de Zonas</th>
										<th>Total de Sucursales</th>
										<th>En mantenimiento</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="verdeClaro2 bold" id="numTabMantGen3A"></td>
										<td class="verdeOscuro2 bold" id="numTabMantGen3B"></td>
										<td class="gris3 bold" id="numTabMantGen3C"></td>
									</tr>
								</tbody>
							</table>
						</div>
						<a class="btnVerDetalle2 btnTerceros"><strong>Terceros</strong></a>
						<div id="grafGen03" style="height: 250px; width: 100%;"></div>
						<div class="clear"></div>
					</div>
				</div>
			</div>

			<div id="sucursalContainer0" style="display: none; max-height: 300px; overflow-x: scroll; overflow: auto;"></div>
			<div id="btnContainer0" class="btnCenter contBtnExp4" style="display: none;">
				<a href="#" class="link btnRegresarZona" id="expStatusA">Regresar</a>
				<a href="#" class="btnA btnSecundario expStatus00">Exportar</a>
			</div>
		</div>
		<div class="contenedorTab contenedorTabPropios" contenedorTab="2">
			<div class="titulos">Estatus de Tabletas</div>
			<div class="divRow">
				<div class="card justifyStart">
					<div class="cardContenedor cardBordeVerde cardEstTab">
						<div class="cardContTexto">
							<div class="cardTitulo" id="propiosEst01"></div>
							<div>Tabletas funcionando correctamente</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="cardContenedor cardBordeRojo cardEstTab">
						<div class="cardContTexto">
							<div class="cardTitulo" id="propiosEst02"></div>
							<div>Tabletas que presentan algún problema</div>
						</div>
					</div>
				</div>
				<div class="card justifyEnd">
					<div class="cardContenedor cardBordeVerde2 cardEstTab">
						<div class="cardContTexto">
							<div class="cardTitulo" id="propiosEst03"></div>
							<div>Tabletas que se encuentran en mantenimiento</div>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div>
				<strong class="titulos" id="propiosEstTitle"></strong>
			</div>
			<div class="contenedorFormularioEstatu">
				<div>
					Buscar sucursal: <br>
					<form id="miForm2" class="buscadorSucursal">
						<div class="buscador">
							<input type="text" id="busca2" class="buscSucInput" placeholder="Ingrese la sucursal que desea consultar">
							<input type="button" class="buscar buscarSuc" value="">
						</div>
					</form>
				</div>
			</div>
			<div id="graphContainer1" class="contenedorGrafica">
				<div class="legend">
					<div>
						<div class="legendBgVerde"></div>
						<div>
							<strong>Correcto</strong>
						</div>
					</div>
					<div>
						<div class="legendBgRojo"></div>
						<div>
							<strong>Con fallas</strong>
						</div>
					</div>
				</div>
				<!-- GRAFICAS -->
				<div class="graficasNegocios" id="graficasPropiosContainer"></div>
				<div class="btnCenter contBtnExp0" style="">				
					<a href="#" id="expStatusB" class="btnA btnSecundario expStatus0">Exportar</a>
				</div>
			</div>
			<div id="zoneContainer1" style="display: none;"></div>
			<div id="sucursalContainer1" style="display: none; max-height: 300px; overflow-x: scroll; overflow: auto;"></div>
			<div id="btnContainer1" class="btnCenter contBtnExp4" style="display: none;">
				<a href="#" id="btnBack2" class="link btnRegresarZona">Regresar</a>
				<a href="#" id="btnDownload2" class="btnA btnSecundario expStatus3">Exportar</a>
			</div>
		</div>
		<div class="contenedorTab contenedorTabTerceros" contenedorTab="3">
			<div class="titulos">Estatus de Tabletas</div>
			<div class="divRow">
				<div class="card justifyStart">
					<div class="cardContenedor cardBordeVerde cardEstTab">
						<div class="cardContTexto">
							<div class="cardTitulo" id="tercerosEst01"></div>
							<div>Tabletas funcionando correctamente</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="cardContenedor cardBordeRojo cardEstTab">
						<div class="cardContTexto">
							<div class="cardTitulo" id="tercerosEst02"></div>
							<div>Tabletas que presentan algún problema</div>
						</div>
					</div>
				</div>
				<div class="card justifyEnd">
					<div class="cardContenedor cardBordeVerde2 cardEstTab">
						<div class="cardContTexto">
							<div class="cardTitulo" id="tercerosEst03"></div>
							<div>Tabletas que se encuentran en mantenimiento</div>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div>
				<strong class="titulos" id="tercerosEstTitle"></strong>
			</div>
			<div class="contenedorFormularioEstatu">
				<div>
					Buscar sucursal: <br>
					<form id="miForm3" class="buscadorSucursal">
						<div class="buscador">
							<input type="text" id="busca3" class="buscSucInput" placeholder="Ingrese la sucursal que desea consultar">
							<input type="button" class="buscar buscarSuc" value="">
						</div>
					</form>
				</div>
			</div>
			<div id="graphContainer2" class="contenedorGrafica">
				<div class="legend">
					<div>
						<div class="legendBgVerde"></div>
						<div>
							<strong>Correcto</strong>
						</div>
					</div>
					<div>
						<div class="legendBgRojo"></div>
						<div>
							<strong>Con fallas</strong>
						</div>
					</div>
				</div>
				<!-- GRAFICAS -->
				<div class="graficasNegocios" id="graficasTercerosContainer"></div>
				<div class="btnCenter contBtnExp0" style="">				
					<a href="#" id="expStatusC" class="btnA btnSecundario expStatus0">Exportar</a>
				</div>
			</div>
			<div id="zoneContainer2" style="display: none;"></div>
			<div id="sucursalContainer2" style="display: none; max-height: 300px; overflow-x: scroll; overflow: auto;"></div>
			<div id="btnContainer2" class="btnCenter contBtnExp4" style="display: none;">
				<a href="#" id="btnBack3" class="link btnRegresarZona">Regresar</a>
				<a href="#" id="btnDownload3" class="btnA btnSecundario expStatus3">Exportar</a>
			</div>
		</div>
	</div>

	<!-- MODALES -->
	<div id="modalAdminList" class="modal">
		<div class="cuadro cuadroG">
			<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
			<div id="adminTitle" class="titModal">ListName</div>
			<br>
			<div id="adminList" class="contModal" style="text-align: left; overflow-y: scroll; height: 290px; max-height: 360px;">
				<!-- lista de cecos generada por codigo -->
			</div>
			<br>
			<div class="btnModalList">
				<a href="#" id="btnListOk" class="btnA" style="margin-right: 10px;" onclick="createEstTabletDestinyBox(_boxId);" >Aceptar</a>
				<a href="#" id="btnListSigNivel" class="btnA" style="margin-left: 10px;" onclick="nextLevelEstTabletList();" >Siguiente nivel</a>
			</div>
		</div>
	</div>

	<!-- MODAL DESCARGA -->
	<div class="toastDescarga">
		<div class="progresoDescarga">
			<div class="grafica">
				<ul>
					<li class="circle-gauge">
						<a href="#" style="-gauge-value: 0;"></a>
					</li>
				</ul>
			</div>
			<div class="tiempo" id="countdown"></div>
			<div class="tamano" id="tamanoArchivo"></div>
		</div>
		<div class="cerrar">
			<img src="../../img/cerrar.svg" alt="">
		</div>
	</div>

	<!-- LOADER -->
	<div class="loaderContainer" style="display: none;">
		<div class="ml-loader loader" style="">
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
		</div>
	</div>
	<div class="blackScreen" style="display: none;"></div>

</body>
</html>