
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<form:form name="formulario" id="formulario" method="POST"	action="checklistDesignF2.htm" >

	<div id="definicionPregunta" class="seccionD">

		<input type="hidden" name="idChecklist"  id="idChecklist" value="${idChecklist}"/>
		<h3>Diseño de checklist: ${checklist.nombreCheck}</h3>

		<ul class="listaAgregarPregunta">

			<li>
				<input type="text" 
					id="nombreChecklist"
					name="nombreChecklist"
					value="${checklist.nombreCheck}"
					placeholder="Titulo" class="textBoxEstilo"
					style="text-align: center;" />
			</li>

		

				<li>
				<div id="tipoChecklist" class="caja" style="margin-left: 10px;">
				<c:choose>
				
					<c:when test="${idChecklist != 0 }">
						<span class="seleccionado" id="${ tipoChecklist.idTipoCheck}">${tipoChecklist.descTipo }</span>
					</c:when>
				
					<c:when test="${idChecklist == 0  }">
						<span class="seleccionado" id="-1">Tipo de checklist</span>
					</c:when>
				</c:choose>
				
					<ul class="listaselect">
						<li value="-1"><a href="#">Tipo de checklist</a></li>
						<li value="-2"><a href="#">Agregar nuevo</a></li>

					</ul>
					<span class="trianguloinf"></span>
				</div>

				<div class="tooltip" style="width: 25px;">
					<span class="tooltiptext">Editar</span>
					<button class='botonEditar'
						onclick="return activarEdicionTipoChecklist();"
						style="position: relative; right: 0%; margin: 0px;"></button>
				</div>

				<div class="tooltip" style="width: 25px; margin: 5px;">
					<span class="tooltiptext">Eliminar</span>
					<button class='botonEliminar'
						onclick="return eliminarTipoChecklist();"
						style="position: relative; right: 0%; margin: 0px;"></button>
				</div>
				
				
					<input type="text" id="nuevoTipoChecklist" 
						class="textBoxEstilo" placeholder="Nombre del tipo" 
						 style="width:20%; display:none;"  />
						
					<button id="agregarTipoChecklist" 
						onclick="return agregarNuevoTipoChecklist();"
						class="botonAgregar" style="display:none;" >+</button>
				
				</li>
				
				
				
				<c:choose>
					<c:when test="${idChecklist!=0 }">
							
						<li>
						
						<div class="tooltip">
							<span class="tooltiptext">Fecha de inicio</span> 
							<input type="text"
								id="fechaInicio" class="textBoxEstilo"
								placeholder="Fecha de inicio" 
								style="width: 100%;" 
								value="${checklist.fecha_inicio }"
								/>
						</div>
						<div class="tooltip">
							<span class="tooltiptext">Fecha de termino</span> 
							<input
								type="text" id="fechaTermino" class="textBoxEstilo"
								placeholder="Fecha de termino" 
								style="width: 100%;" 
								
								value="${checklist.fecha_fin }"
								/>
						</div>
						</li>
							
					</c:when>
					
					
					<c:when test="${idChecklist==0 }">
					
						<li>
						
						<div class="tooltip">
							<span class="tooltiptext">Fecha de inicio</span> 
							<input type="text"
								id="fechaInicio" class="textBoxEstilo"
								placeholder="Fecha de inicio" 
								style="width: 100%;" 
								/>
						</div>
						<div class="tooltip">
							<span class="tooltiptext">Fecha de termino</span> 
							<input
								type="text" id="fechaTermino" class="textBoxEstilo"
								placeholder="Fecha de termino" 
								style="width: 100%;" 
								
								/>
						</div>
						</li>
							
					</c:when>
					
					
				</c:choose>
				

				<li>
				
				<div id="horario" class="caja"
					style="margin-left: 10px; ">
					
				<c:choose>
				
					<c:when test="${idChecklist!=0 }">
						<span class="seleccionado" id="${ horario.idHorario}">${horario.cveHorario}:${horario.valorIni}-${horario.valorFin}</span>
					</c:when>
				
					<c:when test="${idChecklist==0 }">
						<span class="seleccionado" id="-1">Selecciona el horario</span>
					</c:when>
				</c:choose>
					
					<ul class="listaselect">
						<li value="-1"><a href="#">Selecciona el horario</a></li>
						<c:forEach items="${horarios}" var="item">
							<li value="${item.idHorario}"><a href="#">
									${item.cveHorario}:${item.valorIni}-${item.valorFin} </a></li>
						</c:forEach>
					</ul>
					<span class="trianguloinf"></span>
				</div>
				
				<!-- SE AGREGA EL SELECT PARA LA PERIODICIDAD -->
				<!--  
				<div id="periodicidad" class="caja"
					style="margin-left: 10px; ">
					
				<c:choose>
					<c:when test="${idChecklist!=0 }">
						<span class="seleccionado" id="${checklist.periodicidad}">${checklist.periodicidad}</span>
					</c:when>
				
					<c:when test="${idChecklist==0 }">
						<span class="seleccionado" id="-1">Selecciona periodicidad</span>
					</c:when>
				</c:choose>
					
				<ul class="listaselect">
						<li value="-1"><a href="#">Selecciona periodicidad</a></li>
						<c:forEach items="${periodicidad}" var="item">
							<li value="${item}"><a href="#">${item}</a></li>
						</c:forEach>
					</ul>
					<span class="trianguloinf"></span>
				</div>
				-->
				
				<!-- SE AGREGA EL SELECT PARA LA SELECCION SEMANAL-->
				<!--  
				
				<div id="fecha" class="caja"
					style="margin-left: 10px; ">
				
				<c:choose>
				
					<c:when test="${idChecklist!=0 }">
						<span class="seleccionado" id="${checklist.dia}">${checklist.dia}</span>
					</c:when>
				
					<c:when test="${idChecklist==0 }">
						<span class="seleccionado" id="-1">Selecciona el dia</span>
					</c:when>
				</c:choose>
				
				
					
				<ul class="listaselect">
						<li value="-1"><a href="#">Selecciona el dia</a></li>
						<c:forEach items="${diaSemana}" var="item">
							<li value="${item}"><a href="#">${item}</a></li>
						</c:forEach>
						
					</ul>
					<span class="trianguloinf"></span>
				</div>
				</li>
					</ul>
				</div>
				-->
		<div class="seccionFinal">
			<button id="continuar" class="log-btn2" style="margin-left:10%; ">Continuar</button> 
	
		</div>

</form:form>