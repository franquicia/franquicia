<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    	
    	<script type="text/javascript">
    		
    		function myFunction(imagen){
  	 			document.getElementById("mi_imagen").src ='http://10.53.33.82'+imagen;
    			
        	}
    		function myFunction2(imagen){
  	 			document.getElementById("mi_imagen2").src ='http://10.53.33.82'+imagen;
    			
        	}
    		function myFunction3(imagen){
  	 			document.getElementById("mi_imagen3").src ='http://10.53.33.82'+imagen;
    			
        	}
    		function myFunction4(imagen){
  	 			document.getElementById("mi_imagen4").src ='http://10.53.33.82'+imagen;
    			
        	}
    		
    	</script>
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> CARTA DE ASIGNACIÓN DE ACTIVO FIJO</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome top60">
	    		<div class="tituloScont"><b>Selecciona la imagen del Trimestre que deseas.</b></div>
	    		<div class="tituloSbcont"><b>${TRIM} ${ANIO}</b></div>

	    		<div class="border borderLeft">  
						<div class=" overflow mh500">
							  
							<table class="items anexoDos">
								<tbody>
									<c:forEach var="lista" items="${lista}">
										<tr>
											<td>
											<a href="#" class="seccionPreguntaUno"  onclick='myFunction("${lista.ruta}")'>Carta Asignación Activo Fijo - <c:out value="${lista.fecha}"></c:out></a>
												
											</td> 
										</tr>
									</c:forEach>  
									
								</tbody>
							</table> 
							
						</div>  
				    </div>
				    
				    
				    <div class="tituloSbcont"><b>${TRIM2} ${ANIO}</b></div>
	    		<div class="border borderLeft">  
					<div class=" overflow mh500">   
						<table class="items tbCarta">
							<tbody>
								<tr>   
									<c:forEach var="lista2" items="${lista2}">
										<tr>
											<td>
												<a href="#" class="seccionPreguntaDos" onclick='myFunction2("${lista2.ruta}")'>Carta Asignación Activo Fijo - <c:out value="${lista2.fecha}"></c:out></a>
											</td> 
										</tr>
									</c:forEach> 
								</tr>
							</tbody>
						</table> 
					</div>  
				</div>
				
				    <div class="tituloSbcont"><b>${TRIM3} ${ANIO}</b></div>
	    		<div class="border borderLeft">  
					<div class=" overflow mh500">   
						<table class="items tbCarta">
							<tbody>
								<tr>   
									<c:forEach var="lista2" items="${lista3}">
										<tr>
											<td>
												<a href="#" class="seccionPreguntaTres" onclick='myFunction3("${lista2.ruta}")'>Carta Asignación Activo Fijo - <c:out value="${lista2.fecha}"></c:out></a>
											</td> 
										</tr>
									</c:forEach> 
								</tr>
							</tbody>
						</table> 
					</div>  
				</div>
				    <div class="tituloSbcont"><b>${TRIM4} ${ANIO}</b></div>
	    		<div class="border borderLeft">  
					<div class=" overflow mh500">   
						<table class="items tbCarta">
							<tbody>
								<tr>   
									<c:forEach var="lista2" items="${lista4}">
										<tr>
											<td>
												<a href="#" class="seccionPreguntaCuatro" onclick='myFunction4("${lista2.ruta}")'>Carta Asignación Activo Fijo - <c:out value="${lista2.fecha}"></c:out></a>
											</td> 
										</tr>
									</c:forEach> 
								</tr>
							</tbody>
						</table> 
					</div>  
				</div>
				
	    		<div class="botones"> 
					<a href="inicio7s.htm" class="btn arrowl">
						<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
						<div>VOLVER AL MENú</div>
					</a>   
				</div>
			</div>
    	</div>

    	 <!--MODAL-->
        <div id="PreguntaUno" class="modal">
            <div class="w100">
                <div class="tModal"><b>${TRIM} ${ANIO} - Carta Asignación Activo Fijo</b></div>
                <div class="cuadrobig">  
                    <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                    <img id="mi_imagen" src="http://10.53.33.82${IMG}" class="imgDefault">
                </div>
        </div>
        
        
        <div id="PreguntaDos" class="modal">
            <div class="w100">
                <div class="tModal"><b>${TRIM2} ${ANIO} - Carta Asignación Activo Fijo</b></div>
                <div class="cuadrobig">  
                    <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                    <img id="mi_imagen2" src="http://10.53.33.82${IMG2}" class="imgDefault">                    
                </div>
        </div>
        
        <div id="PreguntaTres" class="modal">
            <div class="w100">
                <div class="tModal"><b>${TRIM3} ${ANIO} - Carta Asignación Activo Fijo</b></div>
                <div class="cuadrobig">  
                    <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                    <img id="mi_imagen3" src="http://10.53.33.82${IMG}" class="imgDefault">
                </div>
        </div>
        
        
        <div id="PreguntaCuatro" class="modal">
            <div class="w100">
                <div class="tModal"><b>${TRIM4} ${ANIO} - Carta Asignación Activo Fijo</b></div>
                <div class="cuadrobig">  
                    <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                    <img id="mi_imagen4" src="http://10.53.33.82${IMG2}" class="imgDefault">                    
                </div>
        </div>

        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    	<!-- MODAL -->
        <script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.seccionPreguntaUno').click(function (e) {
                        setTimeout(function() {
                            $('#PreguntaUno').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.seccionPreguntaDos').click(function (e) {
                        setTimeout(function() {
                            $('#PreguntaDos').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.seccionPreguntaTres').click(function (e) {
                        setTimeout(function() {
                            $('#PreguntaTres').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.seccionPreguntaCuatro').click(function (e) {
                        setTimeout(function() {
                            $('#PreguntaCuatro').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            });
        </script>
    </body>
</html>



