<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="contSecc">
		<div class="titSec">CONSULTA DE DESCARGAS</div>
		<div class="gris">
			<form:form action="consultaDesc.htm" method="POST">

				<div id="divTipos" class="divCol3">
					<div class="col3">
						<label for="tipo3">Inserta tipo descarca a consultar.</label>
						<input type="text" name="tipoDesc" id="idParam">
						<label for="tipo3">Inserta la fecha a consultar(DDMMYYY).</label>
						<input type="text" name="fecha" id="idParam">
						<input id="btn" type="submit" class="btnA" value="Enviar">
					</div>
				</div>

				<div class="divCol1" id="consulta">
					<div class="col1">
						Consulta: ${tipoConsulta} <br>
						<div style="margin: 10px; padding: 10px; width: 98%; height: 200px; overflow: scroll; background: white; border-style: solid; border-color: #6e7975; text-align: center;">
							<table align="center" border="1" cellpadding="15px" style="width: 100%">
								<thead>
									<tr>
										<c:forEach items="${ listHeader}" var="item">
											<td style="margin: 5px; padding: 5px;">${item}</td>
										</c:forEach>
									</tr>
								</thead>
								<tbody>
									<c:if test="${(fn:length(listDatos)) > 0}">
										<c:forEach begin="0" end="${fn:length(listDatos)-1}" varStatus="loop">
											<tr>
												<%-- <c:out value="${fn:length(listParametros)}"></c:out>  --%>
												<td>${listDatos[loop.index].idControlDesc}</td>
												<td>${listDatos[loop.index].tipoDescarga}</td>
												<td>${listDatos[loop.index].idTablet}</td>
												<td>${listDatos[loop.index].idEstatus}</td>
												<td>${listDatos[loop.index].fcUser}</td>
												<td>${listDatos[loop.index].fechaActu}</td>
												<td>${listDatos[loop.index].activo}</td>
											</tr>
										</c:forEach>
									</c:if>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</form:form>
		</div>
	</div>

</body>
</html>