<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<head>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<script type="text/javascript" src="../js/script-menu.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/jquery/jquery-2.2.4.min.js"></script>



<link rel="stylesheet" type="text/css" href="../css/estilos.css">
<link rel="stylesheet" type="text/css" href="../css/header-style.css">
<link rel="stylesheet" type="text/css" href="../css/menuCheck.css">
<link rel="stylesheet" type="text/css" href="../css/carousel.css">

<link rel="stylesheet" media="screen"
	href="../css/vistaCumplimientoVisitas/modal.css" />

<script src="../js/script-storeIndex.js"></script>
<link rel="stylesheet" type="text/css" href="../css/css-storeIndex.css"></link>

<title>Acta de entrega para concentración de expedientes de
	Operaciones Activas</title>

<style type="text/css">
div.apps {
	border: 2px solid #F0F0F0;
}
</style>

<%
	response.addHeader("X-Frame-Options", "SAMEORIGIN");
%>
</head>

<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateHeader.jsp"
		flush="true">
		<tiles:putAttribute name="cabecera"
			value="/WEB-INF/templates/templateHeader.jsp" />
	</tiles:insertTemplate>

	<tiles:insertTemplate template="/WEB-INF/views/menu.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/views/menu.jsp" />
	</tiles:insertTemplate>

	<!-- ********************************** Body************************************************ -->

    <c:set var="nombreActa" scope="session" value="ACTA DE ENTREGA PARA CONCENTRACION DE EXPEDIENTES DE OPERACIONES CANCELADO" />


	<!-- ********************************** Paso 1 llenar el formulario************************************************ -->
	<c:choose>
		<c:when test="${paso=='no'}">
			<c:url value="/central/expedientesCaptacionCancelados.htm" var="archivoPasivo" />
			<form:form method="POST" action="${archivoPasivo}" model="command"
				name="form1" id="form1">
				<div id="Imprimir">

					<br>
					<div align="left">
						<img id="logo" width="100px" height="50px"
							style="margin-right: 15px; margin-left: 15px;"
							src="../images/logo-baz.png" align="left" />
					</div>

					<br> <br> <br> <br> <br>
					<p align="center">
						<b>CAPTACIÓN</b>
					</p>
					<br>
					<p align="center">
						<b>${nombreActa}</b>
					</p>


					<p
						style="text-align: justify; margin: 20px; border: 8px solid white;">
						En la Ciudad de <input type="text" class="form-control"
							id="ciudad" name="ciudad" placeholder="Ciudad" />, siendo las
						${hora} horas del día
						<script>
							Dia();
						</script>
						del mes de
						<script>
							Mes();
						</script>
						del año
						<script>
							Anio();
						</script>
						, se hace constar que el Gerente de la Sucursal Número ${sucursal}
						${nomsucursal} de <b>Banco Azteca</b>, S.A., Institución de Banca
						Múltiple, de nombre ${nombre} con número de credencial
						${numempleado} (en lo sucesivo el “Gerente”) <b>hace
							entrega</b> de los expedientes físicos al Regional de nombre <input
							type="text" class="form-control" id="nomRegional"
							name="nomRegional" placeholder="Nombre  del Regional" />, con
						número de empleado <input type="text" class="form-control"
							id="numRegional" name="numRegional"
							placeholder="Numero de credencial"
							onChange="validarSiNumero(this.value);" /> , <b>quien recibe
							para su concentración</b>, los cuales se integran por el nombre del
						cliente, tal y como se enlistan a continuación:
					</p>

					<input id="idSucursal" name="idSucursal" type="hidden"
						value="${sucursal}" /> <input id="nomSucursal" name="nomSucursal"
						type="hidden" value="${nomsucursal}" /> <input id="fecha"
						name="fecha" type="hidden" value="${fecha}" /> <input
						id="numGerente" name="numGerente" type="hidden"
						value="${numempleado}" /> <input id="nomGerente"
						name="nomGerente" type="hidden" value="${nombre}" />
					<table style="text-align: center;">
						<tr>
							<th>Descripción - Observaciones</th>

						</tr>
						<tr>
							<td><textarea rows="10" cols="100" class="form-control"
									id="descripcion" name="descripcion" placeholder="Descripcion" /></textarea>
							</td>
						</tr>

					</table>
					<p
						style="text-align: justify; margin: 20px; border: 8px solid white;">
						Haciendo constar el Gerente en esta acta de entrega que todos los
						expedientes físicos arriba enumerados son expedientes de <b>crédito
						cancelados</b>. Estos no incluyen pagaré en fisico y que actualmente estan cancelados,<span style="color:red;font-weight:bold">estos se deberán digitalizar de forma completa y
						correcta</span> para almacenar el expediente en electrónico.
						Circunstancia que ha verificado el área de auditoría que es
						concorde con los registros de la Institución.
				</div>

				<br>


				<div style="border-radius: 20px; margin: 20px;" class="apps">

					<table style="text-align: center; border-collapse: separate;">
						<tr>
							<th><br> Entrega <br>(Gerente)</th>
							<th><br> Recibe <br> (Regional)</th>
							<!--  
				<th>
						<br>
						Testigo
						<br>(Auditoría)
				</th>
						-->
						</tr>
						<tr>
							<td>
								<div class="inputData" style="border-right: 3px solid #F0F0F0;">
									<table style="text-align: center; border-collapse: separate;">
										<tr>
											<td><label>Usuario:</label><br> <br> <input
												id="usuario" name="usuario"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;" /></td>
										</tr>

										<tr>
											<td><label>Token:</label><br> <br> <input
												id="llave" name="llave" type="password"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;" /></td>
										</tr>

										<tr>
											<br>
											<br>
											<td colspan="2"><input id="botonEntrega"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;"
												type="button" value="Aceptar" onClick="return valida();" /></td>
										</tr>
									</table>
									<br>
									<div class="error" id="error">${response}</div>
									<input id="usr" name="usr" type="hidden" value="" /> <input
										id="token1" name="token1" type="hidden" value="" />
								</div>
							</td>
							<td>
								<div class="inputData" style="border-right: 3px solid #F0F0F0;">
									<table style="text-align: center; border-collapse: separate;">
										<tr>
											<td><label>Usuario:</label><br> <br> <input
												id="usuario2" name="usuario2"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;" /></td>
										</tr>

										<tr>
											<td><label>Token:</label><br> <br> <input
												id="llave2" name="llave2" type="password"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;" /></td>
										</tr>

										<tr>
											<br>
											<br>
											<td colspan="2"><input id="botonEntrega2"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;"
												type="submit" value="Aceptar" onClick="return valida2();" /></td>
										</tr>
									</table>
									<br>
									<div class="error" id="error2">${response}</div>
									<input id="usr2" name="usr2" type="hidden" value="" /> <input
										id="token2" name="token2" type="hidden" value="" />
								</div>
							</td>
							<!--  
				<td>
					<div class="inputData">
								<table style="text-align:center; border-collapse: separate;">
									<tr>
										<td><label >Usuario:</label><br><br>
											<input  id="usuario3" name="usuario3" style="height: 30px; width: 150px; text-align: center; 	font-size: 20px;"/></td>
									</tr>
									
									<tr>
										<td><label >Token:</label><br><br>
											<input  id="llave3" name="llave2" type="password" style="height: 30px; width: 150px; text-align: center; 	font-size: 20px;"/></td>
									</tr>
									
									<tr>
									<br><br>
										<td colspan="2">
										<input id="botonEntrega3" style="height: 30px; width: 150px; text-align: center; font-size: 20px;" type="submit" value="Aceptar"  onClick="return valida3();" /></td>
									</tr>
								</table>
							<br>
								<div class="error" id="error3">${response}</div>
								<input id="usr3" name="usr3" type="hidden" value=""/>
								<input id="token3" name="token3" type="hidden" value=""/>
					</div>		
				</td>
				-->
						</tr>

					</table>
				</div>

				<br>
				<input type="submit" id="Gardar" name="Agregar"
					style="height: 40px; width: 250px; text-align: center; font-size: 35px;"
					class="btn btn-default btn-lg" value="Guardar"
					onclick="return validaExpediente();" />

			</form:form>

		</c:when>

		<c:when test="${paso=='no3'}">
			<c:url value="/central/expedientesCaptacionCancelados.htm" var="archivoPasivo" />
			<form:form method="POST" action="${archivoPasivo}" model="command"
				name="form1" id="form1">
				<div id="Imprimir">

					<br>
					<div align="left">
						<img id="logo" width="100px" height="50px"
							style="margin-right: 15px; margin-left: 15px;"
							src="../images/logo-baz.png" align="left" />
					</div>


					<br> <br> <br> <br> <br>

					<p align="center">
						<b>${nombreActa}</b>
					</p>

					<p
						style="text-align: justify; margin: 20px; border: 8px solid white;">
						En la Ciudad de ${ciudad}, siendo las ${hora} horas del día
						<script>
							Dia();
						</script>
						del mes de
						<script>
							Mes();
						</script>
						del año
						<script>
							Anio();
						</script>
						,se hace constar que el Gerente de la Sucursal Número ${sucursal}
						${nomsucursal} de <b>Banco Azteca</b>, S.A., Institución de Banca
						Múltiple, de nombre ${nombre} con número de credencial
						${numempleado} (en lo sucesivo el “Gerente”) <b>hace
							entrega</b> de los expedientes físicos al Regional de nombre ${nomRegional}, con
						número de empleado ${numRegional}, <b>quien recibe
							para su concentración</b>, los cuales se integran por el nombre del
						cliente, tal y como se enlistan a continuación:
						
					</p>
					<input id="ciudad" name="ciudad" type="hidden" value="${ciudad}" />
					<input id="sucursal" name="sucursal" type="hidden"
						value="${sucursal}" /> <input id="nomRegional" name="nomRegional"
						type="hidden" value="${nomRegional}" /> <input id="numRegional"
						name="numRegional" type="hidden" value="${numRegional}" /> <input
						id="descripcion" name="descripcion" type="hidden"
						value="${descripcion}" /> <input id="idSucursal"
						name="idSucursal" type="hidden" value="${idSucursal}" /> <input
						id="nomSucursal" name="nomSucursal" type="hidden"
						value="${nomsucursal}" /> <input id="fecha" name="fecha"
						type="hidden" value="${fecha}" /> <input id="numGerente"
						name="numGerente" type="hidden" value="${numGerente}" /> <input
						id="nomGerente" name="nomGerente" type="hidden"
						value="${nomGerente}" /> <input id="numExpediente"
						name="numExpediente" type="hidden" value="${numExpediente}" />
					<table
						style="text-align: center; border: 1px black solid; margin: 25px;">
						<tr>
							<th style="border: 1px black solid;">Descripción -
								Observaciones</th>

						</tr>
						<tr>
							<td style="border: 1px black solid;">${descripcion}</td>
						</tr>

					</table>

					<p
						style="text-align: justify; margin: 20px; border: 8px solid white;">
						Haciendo constar el Gerente en esta acta de entrega que todos los
						expedientes físicos arriba enumerados son expedientes de <b>crédito
						cancelados</b>. Estos no incluyen pagaré en fisico y que actualmente estan cancelados,<span style="color:red;font-weight:bold">estos se deberán digitalizar de forma completa y
						correcta</span> para almacenar el expediente en electrónico.
						Circunstancia que ha verificado el área de auditoría que es
						concorde con los registros de la Institución.</p>

				</div>

				<br>


				<input id="tok" name="tok" type="hidden" value="${tok}" />

				<div style="border-radius: 20px;; margin: 20px;" class="apps">
					<table style="text-align: center; border-collapse: separate;">
						<tr>
							<th><br> Entrega <br>(Gerente)</th>
							<th><br> Recibe <br> (Regional)</th>
							<!--  
				<th>
						<br>
						Testigo
						<br>(Auditoría)
				</th>		
				-->

						</tr>
						<tr>
							<td>
								<div class="inputData" style="border-right: 3px solid #F0F0F0;">
									<table style="text-align: center; border-collapse: separate;">
										<tr>
											<td><label>Usuario:</label><br> <br> <input
												id="usuario" name="usuario"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;" /></td>
										</tr>

										<tr>
											<td><label>Token:</label><br> <br> <input
												id="llave" name="llave" type="password"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;" /></td>
										</tr>

										<tr>
											<br>
											<br>
											<td colspan="2"><input id="botonEntrega"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;"
												type="button" value="Aceptar" onClick="return valida();" /></td>
										</tr>
									</table>
									<br>
									<div class="error" id="error">${response}</div>
									<input id="usr" name="usr" type="hidden" value="" /> <input
										id="token1" name="token1" type="hidden" value="" />
								</div>
							</td>
							<td>
								<div class="inputData" style="border-right: 3px solid #F0F0F0;">
									<table style="text-align: center; border-collapse: separate;">
										<tr>
											<td><label>Usuario:</label><br> <br> <input
												id="usuario2" name="usuario2"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;" /></td>
										</tr>

										<tr>
											<td><label>Token:</label><br> <br> <input
												id="llave2" name="llave2" type="password"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;" /></td>
										</tr>

										<tr>
											<br>
											<br>
											<td colspan="2"><input id="botonEntrega2"
												style="height: 30px; width: 150px; text-align: center; font-size: 20px;"
												type="submit" value="Aceptar" onClick="return valida2();" /></td>
										</tr>
									</table>
									<br>
									<div class="error" id="error2">${response}</div>
									<input id="usr2" name="usr2" type="hidden" value="" /> <input
										id="token2" name="token2" type="hidden" value="" />
								</div>
							</td>
							<!--  
				<td>
					<div class="inputData">
								<table style="text-align:center; border-collapse: separate;">
									<tr>
										<td><label >Usuario:</label><br><br>
											<input  id="usuario3" name="usuario3" style="height: 30px; width: 150px; text-align: center; 	font-size: 20px;"/></td>
									</tr>
									
									<tr>
										<td><label >Token:</label><br><br>
											<input  id="llave3" name="llave2" type="password" style="height: 30px; width: 150px; text-align: center; 	font-size: 20px;"/></td>
									</tr>
									
									<tr>
									<br><br>
										<td colspan="2">
										<input id="botonEntrega3" style="height: 30px; width: 150px; text-align: center; font-size: 20px;" type="submit" value="Aceptar"  onClick="return valida3();" /></td>
									</tr>
								</table>
							<br>
								<div class="error" id="error3">${response}</div>
								<input id="usr3" name="usr3" type="hidden" value=""/>
								<input id="token3" name="token3" type="hidden" value=""/>
					</div>		
				</td>
				-->
						</tr>

					</table>
				</div>

				<br>
				<input type="submit" id="Agregar" name="Gardar"
					style="height: 40px; width: 250px; text-align: center; font-size: 35px;"
					class="btn btn-default btn-lg" value="Guardar"
					onclick="return validaFirmas();" />



			</form:form>
		</c:when>

		<c:when test="${paso=='no2'}">


			<c:url value="/central/expedientesCaptacionCancelados.htm" var="archivoPasivo" />
			<form:form method="POST" action="${archivoPasivo}" model="command"
				name="form1" id="form1">
				<div id="Imprimir">

					<br>
					<div align="left">
						<img id="logo" width="100px" height="50px"
							style="margin-right: 15px; margin-left: 15px;"
							src="../images/logo-baz.png" align="left" />
					</div>

			
					<br> <br> <br> <br> <br>
					<p align="center">
						<b>${nombreActa}</b>
					</p>


					<p
						style="text-align: justify; margin: 20px; border: 8px solid white;">
						En la Ciudad de ${ciudad}, siendo las ${hora} horas del día
						<script>
							Dia();
						</script>
						del mes de
						<script>
							Mes();
						</script>
						del año
						<script>
							Anio();
						</script>
						se hace constar que el Gerente de la Sucursal Número ${sucursal}
						${nomsucursal} de <b>Banco Azteca</b>, S.A., Institución de Banca
						Múltiple, de nombre ${nombre} con número de credencial
						${numempleado} (en lo sucesivo el “Gerente”) <b>hace
							entrega</b> de los expedientes físicos al Regional de nombre ${nomRegional}, con
						número de empleado ${numRegional}, <b>quien recibe
							para su concentración</b>, los cuales se integran por el nombre del
						cliente, tal y como se enlistan a continuación:
					</p>

					<input id="idSucursal" name="idSucursal" type="hidden"
						value="${idSucursal}" /> <input id="nomSucursal"
						name="nomSucursal" type="hidden" value="${nomsucursal}" /> <input
						id="fecha" name="fecha" type="hidden" value="${fecha}" /> <input
						id="numGerente" name="numGerente" type="hidden"
						value="${numGerente}" /> <input id="nomGerente" name="nomGerente"
						type="hidden" value="${nomGerente}" /> <input id="numExpediente"
						name="numExpediente" type="hidden" value="${numExpediente}" />
					<table
						style="text-align: center; border: 1px black solid; margin: 25px;">
						<tr>
							<th style="border: 1px black solid;">Descripción -
								Observaciones</th>

						</tr>
						<tr>
							<td style="border: 1px black solid;">${descripcion}</td>
						</tr>

					</table>
					<p
						style="text-align: justify; margin: 20px; border: 8px solid white;">
						Haciendo constar el Gerente en esta acta de entrega que todos los
						expedientes físicos arriba enumerados son expedientes de <b>crédito
						cancelados</b>. Estos no incluyen pagaré en fisico y que actualmente estan cancelados,<span style="color:red;font-weight:bold">estos se deberán digitalizar de forma completa y
						correcta</span> para almacenar el expediente en electrónico.
						Circunstancia que ha verificado el área de auditoría que es
						concorde con los registros de la Institución.</p>
					<br>
					<table style="text-align: center;">
						<tr>
							<td>Entrega <br> <br> ____________________________<br>
								${nomGerente} <br>(${txtGerenteJefe} de sucursal)


							</td>
							<td>Recibe <br> <br> ____________________________<br>
								${nomusuario2} <br> (Regional)
							</td>
						</tr>
						<!--  
									<tr>
										
										<td colspan="2"> 
											Testigo
											<br><br>
											____________________________<br>
											${nomusuario3}
											<br>(Auditoría)
										</td>
									</tr>
									-->

					</table>

				</div>

				<br>
				<br>
				<input id="impreso" name="impreso" type="hidden" value="OK" />
				<div id="btImprimir">
					<input type="button" onclick="printDiv('Imprimir');return false;"
						style="height: 40px; width: 250px; text-align: center; font-size: 35px;"
						value="Imprimir" />
				</div>
				<div id="botones" style="display: none;">
					<table style="text-align: center;">
						<tr>
							<th colspan="2">¿Se imprimio correctamente? <br> <br>

							</th>

						</tr>
						<tr>
							<td><br> <input type="submit" id="SI" name="SI"
								style="height: 40px; width: 250px; text-align: center; font-size: 35px;"
								class="btn btn-default btn-lg" value="SI"
								onclick="muestra_oculta('btImprimir'); muestra_oculta('botones'); carga2(); carga(); return true;" />
							</td>
							<td><br> <input type="submit" id="NO" name="NO"
								style="height: 40px; width: 250px; text-align: center; font-size: 35px;"
								class="btn btn-default btn-lg" value="NO"
								onclick="muestra_oculta('btImprimir'); muestra_oculta('botones'); return false;" />
							</td>
						</tr>

					</table>
				</div>

			</form:form>
		</c:when>
		<c:when test="${paso=='no4'}">
			<h1>Formato guardado exitosamente</h1>
		</c:when>
		<c:when test="${paso=='error'}">
			<h1>Algo paso al guardar el formato</h1>
			<img src="../images/error/algo_paso.png" />
			<br />
			<a href="/franquicia/"> <img
				src="../images/error/boton_volver.png" />
			</a>


		</c:when>
	</c:choose>
	<!-- ********************************** Cierre Body************************************************ -->
	<!-- jquery Start -->
	<script type="text/javascript"
		src="../js/vistaCumplimientoVisitas/jquery.js"></script>

	<div id="cargando"
		style="display: none; position: fixed; z-index: 1000; top: 0; left: 0; height: 100%; width: 100%; background: rgba(255, 255, 255, .8) url('/franquicia/images/ajax-loader.gif') 50% 50% no-repeat;"></div>



</body>

</html>