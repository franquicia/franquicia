<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body ng-app="app" ng-controller="validarForm">
	<script type="text/javascript">
		var contextPath = '${pageContext.request.contextPath}';
		var tfStr = '${tfStr}';
		var tpStr = '${tpStr}';
		var _idTableta = '${idTableta}';

		var jsonTF = tfStr.length > 0 ? JSON.parse(tfStr) : JSON.parse('[]');
		var jsonTP = tpStr.length > 0 ? JSON.parse(tpStr) : JSON.parse('[]');

		$(document).ready(function() {
			showLoadingSpinner();
			updateStatusTablets();
			loadTabletData(jsonTF, jsonTP);
			$('#goToInfoTableta').attr('href', 'loadTabletInfo.htm?idTableta=' + _idTableta);
			$('#showTechInfo').off();
			$('#showTechInfo').on('click', function () {
				$('#estatusContainer').hide();
				$('#goBackContainer').hide();
				$('#tblInformacion').show();
				$('#goBackToEstatus').show();
			});

			// TODO: Cambiar funcionalidad de botón de regresar...
			$('#goToEstatusTableta2').off();
			$('#goToEstatusTableta2').on('click', function () {
				$('#estatusContainer').show();
				$('#goBackContainer').show();
				$('#tblInformacion').hide();
				$('#goBackToEstatus').hide();
			});
		});
	</script>

	<div class="clear"></div>
    <div class="titulo">
    	<div class="ruta">
			<a href="estatusGeneral.htm">Página Principal</a> /
			<a id="goToEstatusTableta" href="estatusTableta.htm">Estatus de Tabletas</a> /
            <a id="goToInfoTableta" href="infoTabletaPD.htm">Información de la tableta</a>
		</div>
    	Información de la tableta
    </div>

	<!-- New content -->
	<div class="contSecc">
		<div class="infoTablenta">
			<div class="infoTablentaCont1">
				<div class="textoInfoTable">
					<div class="tituloInfoTab">
						<div id="datosSucursal"></div>
					</div>
					<div id="fechaInstalacion"></div>
					<div id="estatusTableta"></div>
					<br>
					<div class="imagenTableta">
						<img src="../../img/galaxy_tab_a.png" class="tabletaImg" id="imgTableta" alt="tableta">
					</div>
				</div>
			</div>

			<div id="estatusContainer" class="infoTablentaCont2">
				<a href="#" class="card cardDesac">
					<div><strong>Alimentación</strong></div>
					<div><img class="icoInfoTable" id="ind1C" src="../../img/iconos/alimentacionIco.svg" ></div>
					<div>
						<div id="ind1A"></div>
						<div class="forTexTar">Última actualización</div>
						<div id="ind1B" class="forTexTar"></div>
					</div>
				</a>
				<a href="#" class="card cardDesac">
					<div><strong>Conectividad</strong></div>
					<div><img class="icoInfoTable" id="ind2C" src="../../img/iconos/conexionIco.svg"></div>
					<div>
						<div id="ind2A"></div>
						<div class="forTexTar">Última actualización</div>
						<div id="ind2B" class="forTexTar"></div>
					</div>
				</a>
				<a href="#" class="card cardDesac">
					<div><strong>Salud de la bateria</strong></div>
					<div><img class="icoInfoTable" id="ind3C" src="../../img/iconos/bateriaIco.svg"></div>
					<div>
						<div id="ind3A"></div>
						<div class="forTexTar">Última actualización</div>
						<div id="ind3B" class="forTexTar"></div>
					</div>
				</a>
				<a href="#" class="card cardDesac">
					<div><strong>Temperatura</strong></div>
					<div><img class="icoInfoTable" id="ind4C" src="../../img/iconos/temperaturaIco.svg"></div>
					<div>
						<div id="ind4A">0 - 25</div>
						<div class="forTexTar">Última actualización</div>
						<div id="ind4B" class="forTexTar"></div>
					</div>
				</a>
				<a id="showTechInfo" href="#" class="card cardVerde">
					<div><strong>Información Técnica</strong></div>
					<div><img class="icoInfoTable" src="../../img/iconos/tecnicaIco.svg"></div>
					<div>
						<div class="forTexTar textUnderline">Información relevante <br> al dispositivo</div>
					</div>
				</a>
				<a id="toSucursalInfo" href="#" onclick="showLoadingSpinner();" class="card cardVerde">
					<div><strong>Información <br> de Sucursal</strong></div>
					<div><img  class="icoInfoTable" src="../../img/iconos/infoSucursal.svg"></div>
					<div>
						<div class="forTexTar textUnderline">Listado de documentos y <br> Licencias de funcionamiento</div>
					</div>
				</a>
			</div>

			<div id="tblInformacion" class="infoTablentaCont2" style="display: none;"></div>
		</div>		
		<div class="clear"></div>
		<div id="goBackContainer" class="btnCenter">
			<a id="goBackToStatusSuc" href="estatusTableta.htm" class="btnA btnRegresar">Regresar</a>
		</div>

		<div id="goBackToEstatus" class="btnCenter" style="display: none;">
			<a id="goToEstatusTableta2" href="#" class="btnA btnRegresar">Regresar</a>
		</div>
	</div>

	<!-- LOADER -->
	<div class="loaderContainer" style="display: none;">
		<div class="ml-loader loader" style="">
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
		</div>
	</div>
	<div class="blackScreen" style="display: none;"></div>

</body>
</html>