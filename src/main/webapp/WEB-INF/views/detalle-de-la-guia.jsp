<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>



<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    	
    	<script type="text/javascript">
		    var carName = "Volvo";
		    function myFunction(aux, imagen){
		    document.getElementById('contenido').innerHTML = aux;
		    document.getElementById("mi_imagen").src =imagen;
		    //document.getElementById("mi_imagen").src ="http://10.53.33.82/franquicia/archivosGuiaDHL/48606418432416042019.GUIA_DHL_20190416184139.jpg";
   
        }     
    </script>
    	
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="titulags"><b> COPIA DE GUÍA DE DHL DE LA ENTREGA DEL PAQUETE OPERATIVO</b></span>
    	</div>
    	<div class="wrapper">
            <div class="contHome top60">
                <div class="tapoyo">Detalle de la Guía.</div>
                <div class="border">  
                    <div class=" overflow mh500">   
                        <table class="items tbGuia">
                            <tbody>
                                <tr>   
                                    <td>
                                        <b>Reporte</b>
                                    </td>
                                    <td>
                                        <div><b>${FECHA}</b></div>
                                    </td> 
                                </tr>  
                                <tr>   
                                    <td>
                                        <b>Sucursal</b>
                                    </td>
                                    <td>
                                        <b>${DESC_CECO}</b>
                                    </td> 
                                </tr>
                                <tr>   
                                    <td>
                                        <b>Número Económico</b>
                                    </td>
                                    <td>
                                        <div><b>${CECO}</b></div>
                                    </td> 
                                </tr>
                                <tr>   
                                    <td>
                                        <b>Evidencias</b>
                                    </td>
                                    <td>
                                        <c:set var="nFilters" value="${1}" scope="request"/>
                                    	<c:forEach var="listaGuia" items="${listaGuia}">
                                    		<div><a href="#" class="MActa" onclick='myFunction("${listaGuia.coment}","http://10.53.33.82${listaGuia.ruta}")'><b> <c:out value="${listaGuia.nombreArch}"></c:out></b> </a></div>
                                    		<!--<c:out value="${nFilters}"/> -->
                                    		<!--<c:out value="${listaEvid.ruta}"/>-->
                                    		<c:set var="nFilters" value="${nFilters + 1}"  scope="request" />
                                    	</c:forEach>
                                        
                                    </td> 
                                </tr>
                            </tbody>
                        </table> 
                    </div>  
                </div>
              

        		<div class="botones"> 
                    <div class="tleft w50">
                        <a href="copia-de-la-guia-dhl.htm" class="btn arrowl">
                            <div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
                            <div>VOLVER AL LISTADO</div>
                        </a> 
                    </div>

                    <!--<div class="tright w50"> 
                        <a href="#" class="btn arrowr">
                            <div><img src="${pageContext.request.contextPath}/img/arrowright.png"></div>
                            <div>SIGUIENTE GUÍA</div>
                        </a> 
                    </div> -->
                </div>
            </div>

            <!--MODAL-->
            
            <div id="acta"  class="modal">
                <div class="w100">
                    <div class="tModal"><b>Guia DHL</b></div>
                    <div class="cuadrobig ">  
                        <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                        <div class="paddingMuser">
                            <img id="mi_imagen" src="${pageContext.request.contextPath}/img/fotomModla.png" class="imgDefault">
                            

                            <div class="sbModal tcenter w100">Comentarios</div><br>
                            <div class="cdescripcion">
                                <div class="padFooter">
                                   
                                    <b><div id="contenido"> </div></b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="bModalUser"> 
                    <div class="tleft w50">
                        <a href="#" class="btn  bm arrowml simplemodal-close MGuia3">
                            <div></div>
                            <div>FOTO ANTERIOR</div>
                        </a> 
                    </div>

                    <div class="tright w50"> 
                        <a href="#" class="btn bm arrowmr simplemodal-close MGuia2">
                            <div></div>
                            <div>SIGUIENTE FOTO</div>
                        </a> 
                    </div> 
                </div>	-->
            </div>
            
            
            <div id="guia2" class="modal">
                <div class="w100">
                    <div class="tModal"><b>Guia DHL 02</b></div>
                    <div class="cuadrobig ">  
                        <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                        <div class="paddingMuser">
                            <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">

                            <div class="sbModal tcenter w100">Comentarios</div><br>
                            <div class="cdescripcion">
                                <div class="padFooter">
                                    <b>Aquí se colocan las especificaciones de las evidencias tomadas.</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bModalUser"> 
                    <div class="tleft w50">
                        <a href="#" class="btn  bm arrowml simplemodal-close MGuia1">
                            <div></div>
                            <div>FOTO ANTERIOR</div>
                        </a> 
                    </div>

                    <div class="tright w50"> 
                        <a href="#" class="btn bm arrowmr simplemodal-close MGuia3">
                            <div></div>
                            <div>SIGUIENTE FOTO</div>
                        </a> 
                    </div> 
                </div>
            </div>
            <!--
            <div id="guia3" class="modal">
                <div class="w100">
                    <div class="tModal"><b>Guia DHL 03</b></div>
                    <div class="cuadrobig ">  
                        <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                        <div class="paddingMuser">
                            <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">

                            <div class="sbModal tcenter w100">Comentarios</div><br>
                            <div class="cdescripcion">
                                <div class="padFooter">
                                    <b>Aquí se colocan las especificaciones de las evidencias tomadas.</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bModalUser"> 
                    <div class="tleft w50">
                        <a href="#" class="btn  bm arrowml simplemodal-close MGuia3">
                            <div></div>
                            <div>FOTO ANTERIOR</div>
                        </a> 
                    </div>

                    <div class="tright w50"> 
                        <a href="#" class="btn bm arrowmr simplemodal-close MGuia1">
                            <div></div>
                            <div>SIGUIENTE FOTO</div>
                        </a> 
                    </div> 
                </div>
            </div> -->
        </div>
        <!-- -->
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.MActa').click(function (e) {
                        setTimeout(function() {
                            $('#acta').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.MGuia2').click(function (e) {
                        setTimeout(function() {
                            $('#guia2').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.MGuia3').click(function (e) {
                        setTimeout(function() {
                            $('#guia3').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            }); 
        </script>
    </body>
</html>



