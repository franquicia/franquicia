<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script type="text/javascript">
		var contextPath = '${pageContext.request.contextPath}';
		var canvasURI = '';
		var indicadoresGeneral = '${indicadoresGeneral}';
		var indicadoresPropios = '${indicadoresPropios}';
		var indicadoresTerceros = '${indicadoresTerceros}';

		$(document).ready(function() {
			var dkYS = new Dropkick('#yearSelect');
			var dkYS2 = new Dropkick('#yearSelect2');
			var dkYS3 = new Dropkick('#yearSelect3');
			dkYS.select(1);
			dkYS2.select(1);
			dkYS3.select(1);

			$('.contenedorTab, .tab[tab=""]').hide();
			$('.contenedorTab[contenedorTab="1"]').show();
			$('.tab').click(function() {
				$(this).attr('tab')
				$('.tab').removeClass('active');
				$(this).addClass('active');
				$('.contenedorTab').hide();
				$('.contenedorTab[contenedorTab="' + $(this).attr('tab') + '"]').show();
			});

			$('.btnDescargar').on('click', function() {
				$('.contenedorMenuDescarga').toggle();
			});
			$('.contenedorMenuDescarga ul li').click(function() {
				$('.contenedorMenuDescarga').css('display', 'none');
				// $('.toastDescarga').addClass('active');
			});

			getIndicadoresInfo();
			makeGraphicGeneral();
			makeGraphicPropios();
			makeGraphicTerceros();
		});
	</script>

	<div class="clear"></div>
	<div class="titulo">
		<div class="ruta">
			<a href="estatusGeneral.htm">Página Principal</a> / <a href="estatusGeneral.htm">Estatus General</a>
		</div>
		Estatus General
	</div>

	<!-- Contenido -->
	<div class="contSecc">
		<div class="menuTabz" style="background-color: white;">
			<div class="contenedorTabs">
				<div class="tab active" tab="1">
					<div></div>
					<div>General</div>
				</div>
				<div class="tab" tab="2">
					<div></div>
					<div>Propios</div>
				</div>
				<div class="tab" tab="3">
					<div></div>
					<div>Terceros</div>
				</div>
			</div>

			<div class="contenedorTab" contenedorTab="1">
				<div class="titulos">Estatus General</div>
				<div class="divCol2">
					<div class="col2">
						<div class="card">
							<div class="cardContenedor cardBordeVerde">
								<div class="tCenter">
									<div>
										<div class="cardTitulo" id="genA">0</div>
										<div>Tabletas instaladas</div>
									</div>
									<br>
									<div>
										<div class="cardTitulo" id="genB">0.00%</div>
										<div>Porcentaje de instalaci&oacute;n</div>
									</div>
								</div>
								<div></div>
								<div class="tCenter">
									<div>
										<div>Inicio de instalaciones:</div>
										<div>06/04/2019</div>
									</div>
									<div class="divider">
										<div></div>
									</div>
									<div>
										<div>Última instalación:</div>
										<div id="genC">01/01/2020</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col2">
						<div class="card">
							<div class="cardContenedor cardBordeRojo">
								<div class="tCenter">
									<div>
										<div class="cardTitulo" id="genD">0</div>
										<div>Sucursales sin tablet</div>
									</div>
									<br>
									<div>
										<div class="cardTitulo" id="genE">0.00%</div>
										<div>Pendientes de instalaci&oacute;n</div>
									</div>
								</div>
								<div></div>
								<div class="tCenter">
									<div>
										<div>
											Total final <br> de instalaciones
										</div>
										<div class="cardTituloSecundario" id="genF">0</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="contenedorFormulario">
					<div>
						<br> <strong class="titulos">Cronograma de instalaciones</strong>
					</div>
					<div>
						<div class="contenedorSelect">
							Año:<br> <select id="yearSelect" onchange="makeGraphicGeneral();">
								<option value="2019">2019</option>
								<option value="2020">2020</option>
								<option value="2021">2021</option>
								<option value="2022">2022</option>
								<option value="2023">2023</option>
								<option value="2024">2024</option>
								<option value="2025">2025</option>
							</select>
						</div>
					</div>
					<div>
						<br>
						<div class="btnDer">
							<div class="menuDescarga">
								<button href="" class="btn btnDescargar">Descargar</button>
								<div class="contenedorMenuDescarga">
									<ul>
										<li onclick="descargaDatosTablaEGGen();">Tabla</li>
										<li onclick="saveImageETGen();">Gráfica</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="contenedorGrafica">
					<canvas id="graficaGeneral" class="graVerical" style="height: 600px; max-height: 600px; width: 100%; "></canvas>
					<div class="clear"></div>
				</div>
			</div>
			<div class="contenedorTab" contenedorTab="2">
				<div class="titulos">Estatus de Canales Propios</div>
				<div class="divCol2">
					<div class="col2">
						<div class="card">
							<div class="cardContenedor cardBordeVerde">
								<div class="tCenter">
									<div>
										<div class="cardTitulo" id="propA">0</div>
										<div>Tabletas instaladas</div>
									</div>
									<br>
									<div>
										<div class="cardTitulo" id="propB">0.00%</div>
										<div>Pendientes de instalaci&oacute;n</div>
									</div>
								</div>
								<div></div>
								<div class="tCenter">
									<div>
										<div>Inicio de instalaciones:</div>
										<div>06/04/2019</div>
									</div>
									<div class="divider">
										<div></div>
									</div>
									<div>
										<div>Última instalación:</div>
										<div id="propC">01/01/2020</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col2">
						<div class="card">
							<div class="cardContenedor cardBordeRojo">
								<div class="tCenter">
									<div>
										<div class="cardTitulo" id="propD">0</div>
										<div>Sucursales sin tablet</div>
									</div>
									<br>
									<div>
										<div class="cardTitulo" id="propE">0.00%</div>
										<div>Pendientes de instalaci&oacute;n</div>
									</div>
								</div>
								<div></div>
								<div class="tCenter">
									<div>
										<div>
											Total final <br> de instalaciones
										</div>
										<div class="cardTituloSecundario" id="propF">0</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="contenedorFormulario">
					<div>
						<br> <strong class="titulos">Cronograma de instalaciones</strong>
					</div>
					<div>
						<div class="contenedorSelect">
							Año:<br> <select id="yearSelect2" onchange="makeGraphicPropios();">
								<option value="2019">2019</option>
								<option value="2020">2020</option>
								<option value="2021">2021</option>
								<option value="2022">2022</option>
								<option value="2023">2023</option>
								<option value="2024">2024</option>
								<option value="2025">2025</option>
							</select>
						</div>
					</div>
					<div>
						<br>
						<div class="btnDer">
							<div class="menuDescarga">
								<button href="" class="btn btnDescargar">Descargar</button>
								<div class="contenedorMenuDescarga">
									<ul>
										<li onclick="descargaDatosTablaEGProp();">Tabla</li>
										<li onclick="saveImageETProp();">Gráfica</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="contenedorGrafica">
					<canvas id="graficaPropios" class="graVerical" style="height: 600px; max-height: 600px; width: 100%; "></canvas>
					<div class="clear"></div>
				</div>
			</div>
			<div class="contenedorTab" contenedorTab="3">
				<div class="titulos">Estatus de Canales de Terceros</div>
				<div class="divCol2">
					<div class="col2">
						<div class="card">
							<div class="cardContenedor cardBordeVerde">
								<div class="tCenter">
									<div>
										<div class="cardTitulo" id="terA">0</div>
										<div>Tabletas instaladas</div>
									</div>
									<br>
									<div>
										<div class="cardTitulo" id="terB">0.00%</div>
										<div>Pendientes de instalaci&oacute;n</div>
									</div>
								</div>
								<div></div>
								<div class="tCenter">
									<div>
										<div>Inicio de instalaciones:</div>
										<div>06/04/2019</div>
									</div>
									<div class="divider">
										<div></div>
									</div>
									<div>
										<div>Última instalación:</div>
										<div id="terC">01/01/2020</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col2">
						<div class="card">
							<div class="cardContenedor cardBordeRojo">
								<div class="tCenter">
									<div>
										<div class="cardTitulo" id="terD">0</div>
										<div>Sucursales sin tablet</div>
									</div>
									<br>
									<div>
										<div class="cardTitulo" id="terE">0.00%</div>
										<div>Pendientes de instalaci&oacute;n</div>
									</div>
								</div>
								<div></div>
								<div class="tCenter">
									<div>
										<div>
											Total final <br> de instalaciones
										</div>
										<div class="cardTituloSecundario" id="terF">0</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="contenedorFormulario">
					<div>
						<br> <strong class="titulos">Cronograma de instalaciones</strong>
					</div>
					<div>
						<div class="contenedorSelect">
							Año:<br> <select id="yearSelect3" onchange="makeGraphicTerceros();">
								<option value="2019">2019</option>
								<option value="2020">2020</option>
								<option value="2021">2021</option>
								<option value="2022">2022</option>
								<option value="2023">2023</option>
								<option value="2024">2024</option>
								<option value="2025">2025</option>
							</select>
						</div>
					</div>
					<div>
						<br>
						<div class="btnDer">
							<div class="menuDescarga">
								<button href="" class="btn btnDescargar">Descargar</button>
								<div class="contenedorMenuDescarga">
									<ul>
										<li onclick="descargaDatosTablaEGTer();">Tabla</li>
										<li onclick="saveImageETTer();">Gráfica</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="contenedorGrafica">
					<canvas id="graficaTerceros" class="graVerical" style="height: 600px; max-height: 600px; width: 100%; "></canvas>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- MODAL DESCARGA -->
	<div class="toastDescarga">
		<div class="progresoDescarga">
			<div class="grafica">
				<ul>
					<li class="circle-gauge"><a href="#" style="-gauge-value: 0;"></a></li>
				</ul>
			</div>
			<div class="tiempo" id="countdown"></div>
			<div class="tamano" id="tamanoArchivo"></div>
		</div>
		<div class="cerrar">
			<img src="../../img/cerrar.svg" alt="">
		</div>
	</div>

	<!-- LOADER -->
	<div class="loaderContainer" style="display: none;">
		<div class="ml-loader loader" style="">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>
	</div>
	<div class="blackScreen" style="display: none;"></div>

</body>
</html>