<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>



<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="titulags"><b> COPIA DE GUÍA DE DHL DE LA ENTREGA DEL PAQUETE OPERATIVO</b></span>
    	</div>
    	<div class="page">
    		<c:choose>
	        <c:when test="${datos == 1}">
	    		<div class="contHome top60">
		    		<div class="tituloScont"><b>Selecciona la fecha para consultar el detalle de la guía.</b></div><br>
		    		<div class="border borderLeft">  
							<div class=" overflow mh500">
								  
								<table class="items anexoDos">
									<tbody>
										<c:forEach var="lista" items="${lista}">
											<tr>
												<td>
													<div><a href="detalle-de-la-guia.htm?idGuia=<c:out value="${lista.idGuia}"></c:out>"><c:out value="${lista.fecha}"></c:out></a></div>
												</td> 
											</tr>
										</c:forEach>  
										
									</tbody>
								</table> 
								
							</div>  
					    </div>
		    		
		    		<div class="botones"> 
						<a href="inicio7s.htm" class="btn arrowl">
							<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
							<div>VOLVER AL MENÚ</div>
						</a>   
					</div>
				</div>
			</c:when>
			<c:when test="${datos == 0}">
				<div class="contHome top60">
					<div class="buscar"><img src="${pageContext.request.contextPath}/img/7s/buscar.png"></div>
					<span class="sinDatos"><b> NO SE ENCONTRARON DETALLES PARA CONSULTA.</b></span>
					<div class="tituloScont"><b>No existe ningúna guia para mostrar en este momento.</b></div><br>
					
					<div class="botones"> 
						<a href="inicio7s.htm" class="btn arrowl">
							<div><img src="${pageContext.request.contextPath}/img/7s/arrowleft.png"></div>
							<div>VOLVER AL MENÚ</div>
						</a>   
					</div>
				</div>
			</c:when>
			</c:choose>
    	</div>
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    </body>
</html>



