<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>



<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> BITÁCORA DE MANTENIMIENTO</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome top60"> 



	            <div class="paddingMuser"> 
	                <div class="tsModal">Anexo Bitácora de Mantenimiento</div>
	                <div class="sbModal"><b>${FECHA_SOLICITUD}</b></div><br>
	                <div class="sbModal">Folio <b>${FOLIO}</b></div>
	                <div class="border">  
						<div class=" overflow mh300 w100">   
							<table class="items BiModal">
								<tbody>
									<tr>   
										<td><b>Nombre del Proveedor</b></td>
										<td><div><b>${NOM_PROVEE}</b></div></td>
									</tr>  
									<tr>   
										<td><b>Tipo de Mantenimiento</b></td>
										<td><div><b>${TIPO_MTTO}</b></div></td> 
									</tr>
									<tr>   
										<td><b>Fecha de solicitud</b></td>
										<td><div><b>${FECHA_SOLICITUD}</b></div></td> 
									</tr>
									<tr>   
										<td><b>Hora de entrada</b></td>
										<td><div><b>${HORA_ENTRA}</b></div></td> 
									</tr>
									<tr>   
										<td><b>Hora de salida</b></td> 
										<td><div><b>${HORA_SALE}</b></div></td>
									</tr>
									<tr>   
										<td><b>Número de personas</b></td>
										<td><div><b>${NUM_PERSONAS}</b></div></td>
									</tr>
								</tbody>
							</table> 
						</div>
				    </div><br>
				  
					<div class="sbModal">Detalles de los trabajos realizados</div><br>
					<div class="cdescripcion">
						<div class="padFooter tleft">
							<b>${DETALLES}</b>
						</div>
					</div>
					<br><br>
<!-- 
					    <div class="w100">
                            <div class="tfoto"><b>Bitácora de Mantenimiento 01</b></div>
                            <div class="cfActivo">
                                <a href="#" class="overflow MActa">
                                    <img src="${pageContext.request.contextPath}/img/imgticket.png" class="imgfoto">
                                </a>
                            </div>
                        </div>


				</div>

 -->

	    		<div class="botones"> 
					<a href="bitacora-de-mantenimiento.htm" class="btn arrowl">
						<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
						<div>VOLVER AL LISTADO</div>
					</a>   
				</div>
			</div>
    	</div>

    	<!--MODAL-->

                <div id="acta" class="modal">
                    <div class="w100">
                        <div class="tModal"><b>Bitácora de Mantenimiento</b></div>
                        <div class="cuadrobig">  
                            <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                            <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">
                        </div>
                    </div>
                    <div class="bModalUser"> 
                        <div class="tleft w50">
                            <a href="#" class="btn  bm arrowml">
                                <div></div>
                                <div>FOTO ANTERIOR</div>
                            </a> 
                        </div>

                        <div class="tright w50"> 
                            <a href="#" class="btn bm arrowmr">
                                <div></div>
                                <div>SIGUIENTE FOTO</div>
                            </a> 
                        </div> 
                    </div>
                </div>



        <div id="limpieza" class="modal">
            <div class="cuadrobigmax">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="sbModal">4 octubre 2018</div><br>
	                <div class="border">  
						<div class=" overflow mh300 w100"> 
						    <table class="items LimModal ">
							    <thead>
								    <tr>
								      <th>Área a Limpiar</th>
								      <th>Artículos</th>
								      <th>Frecuencia</th>
								      <th>Supervisor</th>
								      <th>Responsable</th>
								    </tr>
							    </thead>
                                <tbody>
								    <tr>
								      <td>Patio Bancario</td>
								      <td>Pisos</td>
								      <td>Diario 2:00 PM</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Escritorios</td>
								      <td>Mesas, sillas</td>
								      <td>Semanal 3:30 PM</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Comedor</td>
								      <td>Microondas</td>
								      <td>Diario 9:00 AM</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Baños</td>
								      <td>Inodoro</td>
								      <td>Diario</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Bunker</td>
								      <td>Archivero</td>
								      <td>Diario</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
								    <tr>
								      <td>Site</td>
								      <td>Paredes</td>
								      <td>Semanal 1:00 PM</td>
								      <td>Líder Responsable</td>
								      <td>Cajero Supervisor</td>
								    </tr>
                                </tbody>
                            </table>
						</div>
				    </div><br>
				</div>
           </div>
            <div class="bModalUser"> 
				<div class="tleft w50">
    				<a href="#" class="btn  bm arrowml">
    					<div></div>
    					<div>ENTRADA ANTERIOR</div>
    				</a> 
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr">
                        <div></div>
                        <div>SIGUIENTE ENTRADA</div>
                    </a> 
                </div> 
			</div>
        </div>
        <!-- -->
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>

    	<!--MODAL-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.MLimpieza').click(function (e) {
                        setTimeout(function() {
                            $('#limpieza').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.MActa').click(function (e) {
                        setTimeout(function() {
                            $('#acta').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            }); 
        </script>
    </body>
</html>



