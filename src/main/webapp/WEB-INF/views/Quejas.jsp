<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<script type="text/javascript">
		var contextPath = '${pageContext.request.contextPath}';

		$(document).ready(function() {
			_negocio = 41; // Pruebas
			loadQuejasTable();
		});
	</script>

	<div class="clear"></div>
	<div class="titulo">
		<div class="ruta">
			<a href="estatusGeneral.htm">Página Principal</a> / <a href="quejas.htm">Quejas y Sugerencias</a>
		</div>
		Quejas y Sugerencias
	</div>

	<!-- Contenido -->
	<div class="contSecc">
		<div class="titSec">Listado de quejas y sugerencias</div>
		<div class="gris">
			<table id="tblQuejas" class="tblGeneral" style="display: block; width: 100%">
				<thead style="display: block; width: 100%;">
					<tr style="display: inline-table; width: 100%;">
						<th style="width: 20%;">Sucursal</th>
						<th style="width: 10%;">Cliente</th>
						<th style="width: 20%;">Descripción</th>
						<th style="width: 20%;">Comentario</th>
						<th style="width: 10%;">Teléfono</th>
						<th style="width: 10%;">Email</th>
						<th style="width: 10%;">Fecha</th>
					</tr>
				</thead>
				<tbody id="tblBody" style="display: block; width: 100%; max-height: 540px; overflow-y: scroll;"></tbody>
			</table>
		</div>
		<div class="btnCenter">
			<a href="#" class="btnA btnB" id="exportarQuejas" onclick="exportQuejas();";>Exportar tabla</a>
		</div>
	</div>

	<!-- MODAL DESCARGA -->
	<div class="toastDescarga">
		<div class="progresoDescarga">
			<div class="grafica">
				<ul>
					<li class="circle-gauge">
						<a href="#" style="-gauge-value: 0;"></a>
					</li>
				</ul>
			</div>
			<div class="tiempo" id="countdown"></div>
			<div class="tamano" id="tamanoArchivo"></div>
		</div>
		<div class="cerrar">
			<img src="../../img/cerrar.svg" alt="">
		</div>
	</div>

	<!-- LOADER -->
	<div class="loaderContainer" style="display: none;">
		<div class="ml-loader loader" style="">
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
		</div>
	</div>
	<div class="blackScreen" style="display: none;"></div>

</body>
</html>