<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>


<head>

<script type="text/javascript" src="../js/script-menu.js"></script>

<link rel="stylesheet" type="text/css" href="../css/estilos.css">
<link rel="stylesheet" type="text/css" href="../css/header-style.css">
<link rel="stylesheet" type="text/css" href="../css/menuCheck.css">
<link rel="stylesheet" type="text/css" href="../css/carousel.css">


<script src="../js/jquery/jquery-2.2.4.min.js"></script>

<script src="../js/script-storeIndex.js"></script>
<link rel="stylesheet" type="text/css" href="../css/css-storeIndex.css"></link>

<title>Acta de entrega de Expedientes Inactivos</title>

<style type="text/css">
.table-striped>tbody>tr:nth-child(odd)>td, .table-striped>tbody>tr:nth-child(odd)>th
	{
	background-color: #f2f2f2;
}

.table-striped>tbody>tr:hover>td {
	background-color: #EBF8FC;
}

.table-striped>tbody>tr:nth-child(even)>td, .table-striped>tbody>tr:nth-child(even)>th
	{
	background-color: #fbfbfb;
}

.table-striped>thead>tr>th {
	background-color: #006141;
	color: white;
}
</style>

</head>

<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateHeader.jsp"
		flush="true">
		<tiles:putAttribute name="cabecera"
			value="/WEB-INF/templates/templateHeader.jsp" />
	</tiles:insertTemplate>

	<tiles:insertTemplate template="/WEB-INF/views/menu.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/views/menu.jsp" />
	</tiles:insertTemplate>

	
	<!-- ********************************** Body************************************************ -->

	<c:set var="nombreActa" scope="session" value="ACTA DE ENTREGA PARA CONCENTRACION DE EXPEDIENTES DE OPERACIONES ACTIVAS" />

	<!-- ********************************** Paso 1 llenar el formulario************************************************ -->
	<c:choose>
		<c:when test="${paso=='cons1'}">
			<h1>Consulta expediente Crédito Operaciones Activas</h1>
			<c:url value="/central/recuperaExpedienteActivosCancelados.htm" var="archivoPasivo" />
			<form:form method="POST" action="${archivoPasivo}" model="command"
				name="form1" id="form1">
				<div style="overflow-x: auto; margin: 20px;">
					<table class="table table-striped">
						<thead>
							<tr>
								<th data-field="idCheckUsuario" data-sortable="true"
									data-searchable="true">ID Expediente</th>
								<th data-field="idChecklist" data-sortable="true">ID
									Gerente</th>
								<th data-field="idChecklist" data-sortable="true">ID Ceco</th>
								<th data-field="idUsuario" data-sortable="true"
									data-searchable="true">Fecha</th>
								<th data-field="idCeco" data-sortable="true"
									data-searchable="true">Estatus</th>
								<th data-field="activo" data-sortable="true">Descripción</th>

							</tr>
						</thead>
						<tbody>
							<c:forEach var="list" items="${list}">
								<tr>
									<td><INPUT TYPE="radio" id="command" name="command"
										value="${list.idExpediente}" /> <c:out
											value="${list.idExpediente}"></c:out></td>
									<td><c:out value="${list.idGerente}"></c:out></td>
									<td><c:out value="${list.idCeco}"></c:out></td>
									<td><c:out value="${list.fecha}"></c:out></td>


									<td><c:if test="${list.estatusExpediente == 1}">
									    	Lleno
										</c:if> <c:if test="${list.estatusExpediente == 2}">
									    	Firmado
										</c:if> <c:if test="${list.estatusExpediente == 3}">
									    	Impreso
										</c:if></td>

									<td><c:out value="${list.descripcion}"></c:out></td>

								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<input id="idExpediente" name="idExpediente" type="hidden" value="" />
				<input id="idTipoActa" name="idTipoActa" type="hidden" value="2" />
				<input type="submit" id="Recuperar" name="Recuperar"
					style="height: 40px; width: 250px; text-align: center; font-size: 35px;"
					class="btn btn-default btn-lg" value="Recuperar"
					onclick="return recupera(); return false;" />
			</form:form>

		</c:when>


		<c:when test="${paso=='impreso'}">
			<h1>Recuperacion de Formato Impreso</h1>
			<div id="Imprimir">

				<br>
				<div align="left">
					<img id="logo" width="100px" height="50px"
						style="margin-right: 15px; margin-left: 15px;"
						src="../images/logo-baz.png" align="left" />
				</div>

				

				<br> <br>
				<br>
				<br>
				<br>
				<p align="center">
						<b>CRÉDITO</b>
					</p>
					<br>
				<p align="center">
					<b>${nombreActa}</b>
				</p>


				<p
					style="text-align: justify; margin: 20px; border: 8px solid white;">
					En la Ciudad de ${ciudad}, siendo las ${hora} horas del día ${dia}
					del mes de ${mes} del año ${anio}, se hace constar que el <b>-Gerente-</b>
					de la Sucursal Número ${sucursal} ${nomsucursal} de <b>Banco
					Azteca</b>, S.A., Institución de Banca Múltiple, de nombre
					${nomGerente} con número de credencial ${numGerente} (en lo
					sucesivo el “Gerente”) <b>hace entrega</b> de los expedientes
					físicos al <b>-Regional-</b> de nombre ${nomRegional}, con número
					de credencial ${numRegional}, quien recibe <b>para su
					concentración</b>,  los cuales se integran por el nombre del cliente, tal y
					como se enlistan a continuación:
				</p>


				<table
					style="text-align: center; border: 1px black solid; margin: 25px;">
					<tr>
						<th style="border: 1px black solid;">Descripción -
							Observaciones</th>

					</tr>
					<tr>
						<td style="border: 1px black solid;">${descripcion}</td>
					</tr>

				</table>

				<p
					style="text-align: justify; margin: 20px; border: 8px solid white;">
					Haciendo constar el Gerente en esta acta de entrega que todos los
					expedientes físicos arriba enumerados son expedientes de <b>crédito
					activas</b>. <span style="color:red;font-weight:bold">El pagaré en físico deberá quedarse en sucursal en sobres</span>
					y los demas expedientes vigentes se concentrarán para su
					recolección, <span style="color:red;font-weight:bold">estos se deberán digitalizar de forma completa y
					correcta</span> para almacenar el expediente en electrónico.
					Circunstancia que ha verificado el área de auditoría que es
					concorde con los registros de la Institución.</p>
				<br>
				<table style="text-align: center;">
					<tr>
						<td>Entrega <br>
						<br> ____________________________<br> ${nomGerente} <br>(Gerente)


						</td>
						<td>Recibe <br>
						<br> ____________________________<br> ${nomusuario2} <br>
							(Regional)
						</td>
					</tr>
					<!--  
									<tr>
										
										<td colspan="2"> 
											Testigo
											<br><br>
											____________________________<br>
											${nomusuario3}
											<br>(Auditoría)
										</td>
									</tr>
									-->

				</table>

			</div>

			<br>
			<br>

		</c:when>
		<c:when test="${paso=='no'}">
			<h1>No se encontró ningún formato para la sucursal</h1>
		</c:when>
	</c:choose>
	<!-- ********************************** Cierre Body************************************************ -->
</body>

</html>