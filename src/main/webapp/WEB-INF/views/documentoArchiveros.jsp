<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE>

<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<!--  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap.css">

	<script	src="${pageContext.request.contextPath}/js/jquery/jquery-2.2.4.min.js"></script>
	<script	src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
	-->
	
	<script type="text/javascript" src="../js/script-menu.js"></script> 

	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" type="text/css" href="../css/header-style.css">
	<link rel="stylesheet" type="text/css" href="../css/menuCheck.css">
	<link rel="stylesheet" type="text/css" href="../css/carousel.css">
	

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	
	<script src="../js/script-storeIndex.js"></script>    
	<link rel="stylesheet" type="text/css" href="../css/css-storeIndex.css"></link>
	
	<script>
	function checkInput() {
		try {
			var tAction = document.getElementById("fileAction").value;
			//alert("paso");
			var oFile = document.getElementById("uploadedFile").files[0];

			
	
			//alert("Paso: "+oFile.name);
			//alert("paso '"+oFile.name+"'");
			if (tAction == '' || tAction == null) {
				alert("La ruta no puede ir vacía");
				return false;
			}
			else {
				if (oFile.name=="" && oFile.name==null) {
					alert("Ningún archivo seleccionado");
					return false;
				}
				else {
					if (oFile.size > (1024*1024*100)) {
						alert("El archivo pesa más de 100MB!");
						return false;
				 	}
					else {
						return true;
					}
				}
			}
		}
		catch (e) {
			alert("Ningún archivo seleccionado");
			//console.error(error); 
		  return false;
		}
	}
		function confirmRename() {
			var renameFile = document.getElementById("newFileName").value;
			var renameConfirm = confirm("¿Desea renombrar el archivo seleccionado?");
			
			if (renameConfirm == true) {
				if (renameFile == '' || renameFile == null) {
					alert("El campo del nombre del archivo no puede ir vacío");
					return false;
				}
				else {
					if (document.getElementById('oldFileName').checked) {
						return true;
					}
					else {
						alert("¡Debe seleccionar un archivo antes de realizar esa acción!");
						return false;
					}
				}
			}
			else {
				return false;
			}
		}

		function confirmDelete() {
			var retVal = confirm("¿Desea eliminar los archivos seleccionados?");

			if ( retVal == true ) {
				return true;
			}
			else {
				return false;
			}
		}

		function fileValidation(){
		    var fileInput = document.getElementById('uploadedFile');
		    var filePath = fileInput.value;
		    var allowedExtensions = /(.pdf|.PDF)$/i;
		    if(!allowedExtensions.exec(filePath)){
		        alert('Solo se permite subir archivos en formato PDF');
		        fileInput.value = '';
		        return false;
		    }else{
		        return true;
		    }
		}
	</script>
	
	
<title>Soporte - File center</title>
</head>
<body>

	<tiles:insertTemplate template="/WEB-INF/templates/templateHeader.jsp" flush="true">
		<tiles:putAttribute name="cabecera" value="/WEB-INF/templates/templateHeader.jsp" />
	</tiles:insertTemplate>
	
	<tiles:insertTemplate template="/WEB-INF/views/menu.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/views/menu.jsp" />
	</tiles:insertTemplate>

	
	<h1>Subir Acta Entrega de Archiveros Firmado en Formato PDF</h1>
	<br>
	<c:choose>
		<c:when test="${OKARCH=='OK'}">
			<iframe src="http://10.53.33.82/franquicia/imagenes/${NomArchivo}" style="width:90%;  height:70%;" frameborder="0"></iframe>
			<br>
			<br>
			<c:url value="/central/uploadExpedFiles.htm" var="uploadFilesUrl" />
						<form:form method="POST" action="${uploadFilesUrl}" modelAttribute="command" name="form-uploadFiles" id="form-uploadFiles" enctype="multipart/form-data">
							<div class="form-group">
								<label for="inputFile">Actualizar archivo:</label>
								<input type="hidden" id="fileAction" name="fileAction" value="${fileAction}" />
								<input type="hidden" id="fileRoute" name="fileRoute" value="${fileRoute}" />
								<input type="file" id="uploadedFile" name="uploadedFile" onchange="return fileValidation()" />
				 				<input type="submit" class="btn btn-default btn-lg" value="Subir archivo" onclick="return checkInput()" />
				 				<p class="help-block">Tamaño máximo: 100MB</p>
							</div>
						</form:form>
		</c:when>
		<c:otherwise>
			<c:url value="/central/uploadExpedFiles.htm" var="uploadFilesUrl" />
						<form:form method="POST" action="${uploadFilesUrl}" modelAttribute="command" name="form-uploadFiles" id="form-uploadFiles" enctype="multipart/form-data">
							<div class="form-group">
								<label for="inputFile">Subir archivo:</label>
								<input type="hidden" id="fileAction" name="fileAction" value="${fileAction}" />
								<input type="hidden" id="fileRoute" name="fileRoute" value="${fileRoute}" />
								<input type="file" id="uploadedFile" name="uploadedFile"  onchange="return fileValidation()" />
				 				<input type="submit" class="btn btn-default btn-lg" value="Subir archivo" onclick="return checkInput()" />
				 				<p class="help-block">Tamaño máximo: 100MB</p>
							</div>
						</form:form>
		</c:otherwise>
	</c:choose>
	
	

</body>
</html>