<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<script type="text/javascript">
		var contextPath = '${pageContext.request.contextPath}';
		$(document).ready(function() {
			$('#radio1').click();
			$('#radio3').click();
		});
	</script>

	<div class="contSecc">
		<div class="titSec">Administración de cecos</div>
		<div class="gris">
			<div class="divCol3" id="elementos">
				<!-- ------ -->
				<div class="col3">
					<div class="divCol2Input">
						<div><input type="radio" name="group1" id="radio1"><label for="radio1">Producción</label></div>
						<div><input type="radio" name="group1" id="radio2"><label for="radio2">Desarrollo</label></div>
					</div>
				</div>
				<div class="col3">
					<div class="divCol2Input">
						<div><input type="radio" name="group2" id="radio3" onclick="betweenInsertAndUpdate(this);"><label for="radio3">Insertar</label></div>
						<div><input type="radio" name="group2" id="radio4" onclick="betweenInsertAndUpdate(this);"><label for="radio4">Actualizar</label></div>
					</div>
				</div>
				<div class="col3">
					Subir cecos por Excel<br>
					<input type="file" id="cargaCecosExcel" class="inputfile carga">
					<label for="carga01" class="btnAgregaFoto "><span>Adjuntar archivo</span></label>
				</div>

				<!-- <span class="obliga" style="display: none;">*</span> -->
				<div class="col3">
					Ceco:<br>
					<input type="text" placeholder="Centro de costos" id="idCeco">
				</div>
				<div class="col3">
					Número de sucursal:<br>
					<input type="text" placeholder="Número de sucursal" id="numSucursal">
				</div>
				<div class="col3">
					Nombre:<br>
					<input type="text" placeholder="Nombre del centro de costos" id="nombreCeco">
				</div>

				<!-- ------ -->
				<div class="col3">
					Ceco padre:<br>
					<input type="text" placeholder="Centro de costos padre" id="idCecoPadre">
				</div>
				<div class="col3">
					País:<br>
					<input type="text" placeholder="País" id="idPais">
				</div>
				<div class="col3">
					Estado:<br>
					<input type="text" placeholder="Estado" id="idEstado">
				</div>

				<!-- ------ -->
				<div class="col3">
					Municipio:<br>
					<input type="text" placeholder="Municipio" id="idMunicipio">
				</div>
				<div class="col3">
					Calle:<br>
					<input type="text" placeholder="Calle" id="calle">
				</div>
				<div class="col3">
					Nivel:<br>
					<input type="text" placeholder="Nivel" id="nivel">
				</div>

				<!-- ------ -->
				<div class="col3">
					Negocio:<br>
					<input type="text" placeholder="Negocio" id="negocio">
				</div>
				<div class="col3">
					Canal:<br>
					<input type="text" placeholder="Canal" id="canal">
				</div>
				<div class="col3">
					Responsable:<br>
					<input type="text" placeholder="Responsable" id="responsable">
				</div>

				<!-- ------ -->
				<div class="col3">
					FCCP:<br>
					<input type="text" placeholder="FCCP" id="fccp">
				</div>
				<div class="col3">
					Id_Geografia:<br>
					<input type="text" placeholder="Id_Geografia" id="idGeografia">
				</div>
				<div class="col3">
					Estado:<br>
					<input type="text" placeholder="Estado" id="estado">
				</div>

				<!-- ------ -->
				<div class="col3">
					Municipio:<br>
					<input type="text" placeholder="Municipio" id="municipio">
				</div>
				<div class="col3">
					Colonia:<br>
					<input type="text" placeholder="Colonia" id="colonia">
				</div>
				<div class="col3">
					Número exterior:<br>
					<input type="text" placeholder="Número exterior" id="numExterior">
				</div>

				<!-- ------ -->
				<div class="col3">
					Número interior:<br>
					<input type="text" placeholder="Número interior" id="numInterior">
				</div>
				<div class="col3">
					Activo<br>
					<select id="activo">
						<option value="1" >Activo</option>
						<option value="0" >Inactivo</option>
					</select>
				</div>
			</div>

			<!-- ------ -->
			<div class="btnCenter">
				<input type="submit" class="btnLogin" value="Insertar" id="btnCecos">
			</div>
		</div>
	</div>

</body>
</html>