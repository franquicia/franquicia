<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<script>
		var contextPath = '${pageContext.request.contextPath}';
		// Se consume el servicio que carga las Listas de distribucion del usuario
		var idLista = window.opener.document.getElementById('idLista').value;
		var idUsuario = window.opener.document.getElementById('idUsuario').value;
		var negocio = window.opener.document.getElementById('negocio').value;

		$(document).ready(function() {
			loadDistributionList(idLista, idUsuario, negocio);
		});
	</script>

	<div class="clear"></div>
	<div class="titulo">
		<!--
		<div class="ruta">
			<a href="estatusGeneral.htm">Página Principal</a> / <a href="cargaArchivos.htm">Carga de archivos</a>
		</div>
		-->
		<br>
		Listas de distribución
	</div>

	<div class="contSecc">
		<div class="divGrow">
			<div class="col2">
				<div class="titSec">Listas de distribución</div>
				<div class="gris">
					<div id="distribListContainer" class="distribListContainer">
						<!-- lista de distribucion creada por codigo -->
					</div>
					<div class="btnCenter">
						<!--
						<a href="#" id="btnCancelarLista" class="btnCancelarList">Cancelar</a>
						-->
						<a href="#" id="btnEditarLista" class="btnEditarList" onclick="editDistribList();">Editar</a>
						<a href="#" id="btnEnviarLista" class="btnEnviarList" onclick="sendDistribListToFilter(3);">Enviar</a>
					</div>
				</div>
			</div>
			<div class="col2">
				<div class="titSec">Nueva lista de distribución</div>
				<div class="gris" style="min-height: 345px;">
					<div style="margin-left: 40px; margin-right: 40px;">
						<p style="text-align: left; margin-bottom: 2px;"> * Nombre de la lista</p>
						<input id="listName" type="text">
						<br><br>

						<p style="text-align: left; margin-bottom: 2px;">Negocio</p>
	    				<select id="listNegocio">
	    					<option value="41">Sistemas financieros</option>
	    				</select>
						<br>

						<div class="divAdjuntar">
							<div class="adjunto">
								<div>
									<input type="file" id="cargaCSV" class="inputfile carga" onclick="checkListName(event);" onchange="readCSVFile('cargaCSV'); showLoadingSpinner(); this.value=null; return false;">
									<label for="cargaCSV" class="btnAgregaFoto "><span>Adjuntar CSV</span></label>
								</div>
								<p style="margin-top: 5px; margin-left: 12px; text-align: left; margin-bottom: 0px; color: #5fAC98; font-size: 12px;"><strong> * Agrega el archivo CSV.</strong></p>
							</div>
						</div>
					</div>
					<!--
					<div class="btnCenter" style="margin-top: 95px;">
						<a href="#" id="btnCancelarNuevaLista" class="btnEditarList">Cancelar</a>
						<a href="#" id="btnGuardarNuevaLista" class="btnEnviarList">Guardar</a>
					</div>
					-->
				</div>
			</div>
		</div>
	</div>

	<div class="loaderContainer" style="display: none;">
		<div class="ml-loader loader" style="">
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
		</div>
	</div>
	<div class="blackScreen" style="display: none;"></div>

	<div id="modalDistribList" class="modal">
		<div class="cuadroDistrib">
			<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
			<div id="distribTitle" class="titModal"></div>
			<br>

			<div style="margin-left: 15px; margin-right: 15px;">
				<p style="text-align: left; margin-bottom: 2px;">Nombre de la lista</p>
				<input id="listNameModal" type="text">
			</div>
			<br>

			<div style="margin-left: 15px; margin-right: 15px;">
				<p style="text-align: left; margin-bottom: 2px;">Negocio</p>
   				<select id="listNegocioModal">
   					<option value="41">Sistemas financieros</option>
   				</select>
			</div>
			<br>

			<div id="theList" class="contModal" style="border-color: #C4C4C4; border-style: solid; border-width: 1px; background-color: #FFF; text-align: left; overflow-y: scroll; max-height: 232px;">
				<%-- lista generada por código --%>
			</div>
			<br>
			<div class="btnModalList">
				<a href="#" id="btnCancelarList" class="btnEditarList" style="margin-right: 10px;" onclick="$('.btnCerrar').click();">Cancelar</a>
				<a href="#" id="btnGuardarList" class="btnEnviarList" style="margin-left: 10px;" onclick="saveChangesDistribList();" >Guardar</a>
			</div>
		</div>
	</div>

</body>
</html>