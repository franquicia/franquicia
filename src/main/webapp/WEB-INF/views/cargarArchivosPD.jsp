<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script>
		var contextPath = '${pageContext.request.contextPath}';
		var _containerT, _containerC, _tipoT, _tipoC, _cecos, _nivelT, _nivelC, _negocio, _boxId;
		var _listName = '';
		var dzDataList = [];
		var caDataList = [];
		var validaDistrib = false;
		var validaDocs = false;
		var validaEnv = false;
		var jsonDistrib = [];
		var jsonDocs = [];
		var jsonEnv = [];
		var ref;
		var _objBackup = '';
		var _objNameBackup = '';

		$(document).ready(function() {
			_tipoT = 1;
			_cecos = '0';
			_nivelT = 0;
			_tipoC = 0;
			_nivelC = 0;
			_negocio = 41;

			//Calendario
			$.datepicker.regional['es'] = {
				closeText: 'Cerrar',
				prevText: '',
				nextText: ' ',
				currentText: 'Hoy',
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
				dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
				dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
				weekHeader: 'Sm',
				dateFormat: 'dd/mm/yy',
				firstDay: 1,
				isRTL: false,
				showMonthAfterYear: false,
				yearSuffix: ''
			};
			$.datepicker.setDefaults($.datepicker.regional['es']);
			$('#datepickerPD').datepicker({
				firstDay: 1
			});
			$('#dpVigenciaFinal').datepicker({
				firstDay: 1
			});
			$('#btnListTerritorio').on('click', function(e) {
				_cecos = '0';
				_nivelC = 0;
				_boxId = 1;
				getCecoDataList();
			});
			$('#btnListPais').on('click', function(e) {
				_cecos = '0';
				_nivelT = 0;
				_boxId = 2;
				getGeoDataList();
			});
			$('#btnListaDistribucion').on('click', function() {
				showDistribListPopUp('listaDistribucionPD.htm', 'Listas de Distribuci\u00F3n', 1000, 600);
			});
			// $('#checkVF').click();
			validaTipoEnvio($('#tipoEnvio')[0]); // function que oculta los campos fecha y hora en la sección 'Programar envio'			
		});
	</script>

	<input type="hidden" id="idLista" name="idLista" value="0">
	<input type="hidden" id="idUsuario" name="idUsuario" value="${userPicId}">
	<input type="hidden" id="negocio" name="negocio" value="41">

	<div class="clear"></div>
    <div class="titulo">
    	<div class="ruta">
			<a href="estatusGeneral.htm">Página Principal</a> / <a href="cargaArchivos.htm">Carga de archivos</a>
		</div>
    	Carga de archivos
    </div>

    <!-- Contenido -->
    <div class="contSecc">
    	<div class="titSec">Carga de archivos</div>
	    <div class="divGrow gris">
	    	<div class="col2">
    			<p style="margin-bottom: 2px; color: #006341;"><strong>Agrega tus archivos</strong></p>
				<form id="pdDropzone" class="dropzone files-container" action="/franquicia/central/pedestalDigital/upload/multiFileUploadPD.htm" type="file" name="file" enctype="multipart/form-data" method="post" >
					<input type="hidden" id="titulo" name="titulo" value="titulo" >
					<input type="hidden" id="carpeta" name="carpeta" value="carpeta" >
					<div class="fallback">
						<input id="filesPD" name="files" type="file" path="files" multiple required />
					</div>
					<div class="preview-container dz-preview uploaded-files">
						<div id="previews">
							<div id="onyx-dropzone-template">
								<div class="onyx-dropzone-info" style="cursor: pointer;" onclick="showPreviewOfFile(this);" >
									<div class="thumb-container">
										<a href="#">
											<img data-dz-thumbnail />
										</a>
									</div>
									<div class="details">
										<div>
											<span data-dz-name class="n-elip"></span> <span data-dz-size></span>
										</div>
										<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
										<div class="dz-error-message"><span data-dz-errormessage>pdDropzone</span></div>
										<div class="actions">
											<a href="#!" data-dz-remove onclick="deleteFileDataFromArray(this);">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="btnCenter">
						<a href="#" id="btnRemoveFiles" class="btnA btnG">Eliminar</a> <!-- modaPDFPrev_view -->
						<a href="#" id="btnCategorias" class="btnA" onclick="validaArchivosDropzone();" >Subir</a>
					</div>
				</form>
    		</div>
			<div class="col2">
				<p style="margin-bottom: 2px;margin-left: 15px;color: #006341;"><strong>Distribución del documento</strong></p>
				<div class="divDist">
					<a href="#" id="btnListTerritorio" class="btnDistrib"><img src="../../img/icoZonaW.svg"><br>Territorio</a>
					<a href="#" id="btnListPais" class="btnDistrib"><img src="../../img/icoPaisW.svg"><br>País</a>
					<a href="#" id="btnListaDistribucion" class="btnDistrib"><img src="../../img/icoListasW.svg"><br>Listas de distribución</a>
					<a href="#" id="btnCargaCSV" class="btnDistrib" onclick="addFileToCargarCsvInput();" ><img src="../../img/icoEnviosW.svg"><br>Envíos CSV</a>
					<input type="file" id="cargaCsvInput" style="display: none;" onchange="readCSVFile('cargaCsvInput'); showLoadingSpinner(); this.value=null; return false;" >
				</div>
			</div>
		</div>

		<div class="titSec">Programar envío</div>
		<div class="gris">
			<p style="margin-bottom: 2px; margin-left: 15px; color: #006341;"><strong>Filtro</strong></p>
			<!-- <div class="titCol">Filtro</div> -->
			<div id="listaLanzamientos" class="divCol4 divListLanzamientos" style="margin-left: 15px;">
				<%-- lista de lanzamientos generada por codigo --%>
			</div>
			<div class="clear">&nbsp;</div><br>


			<p style="margin-bottom: 2px; margin-left: 15px; color: #006341;"><strong>Programar envío</strong></p>

			<div class="divCol3">
				<div class="col3">
					<div id="divCheckVF" class="listBox col3" style="text-align: left;">
						<input type="checkbox" name="checkVF" id="checkVF" class="checkM" onchange="enabledivDPVF(this);">
						<label for="checkVF">Añadir fecha expiraci&oacute;n</label>
					</div>
				</div>
			</div>

			<div class="divCol3">
   				<div class="col3">
    				<select id="tipoEnvio" onchange="validaTipoEnvio(this);">
    					<option id="tipoEnvio1">Tipo de envío</option>
    					<option id="tipoEnvio2">En batch</option>
						<option id="tipoEnvio3">Instantáneo</option>
    					<option id="tipoEnvio4">Calendarizado</option>
    				</select>
    			</div>
				<div id="divFecha" class="col3" >
					<input type="text" placeholder="Fecha de visualización (DD/MM/AAAA)" class="datapicker1 date" id="datepickerPD" autocomplete="off">
    			</div>
				<div id=divDPVF class="col3" style="display: block;">
					<input type="text" placeholder="Fecha de vigencia final (DD/MM/AAAA)" class="datapicker1 date" id="dpVigenciaFinal" autocomplete="off">
				</div>
			</div>
			<div class="btnCenter">
				<a href="#" id="btnNuevaCarga" class="btnA btnG" style="margin: 0px 10px 0 px 0px;" onclick="cleanNuevaCarga();">Nueva carga</a>
				<a id="btnEnviarDocs" href="#" class="btnA">Enviar</a>
			</div>
		</div>
	</div>

<!-- MODALES -->
<div id="modalList" class="modal">
	<div class="cuadro cuadroG">
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
		<div class="titModal">ListName</div>
		<br>
		<div id="theList" class="contModal" style="text-align: left; overflow-y: scroll; height: 290px; max-height: 360px;">
			<!-- lista de cecos generada por codigo -->
		</div>
		<br>
		<div class="btnModalList">
			<a href="#" id="btnListOk" class="btnA" style="margin-right: 10px;" onclick="createDestinyBox(_boxId);" >Aceptar</a>
			<a href="#" id="btnListSigNivel" class="btnA" style="margin-left: 10px;" onclick="nextLevelList();" >Siguiente nivel</a>
		</div>
	</div>
</div>

<!-- MODALES DE EJEMPLO -->
<div id="modalPais" class="modal">
	<div class="cuadroDistrib">
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
		<div class="titModal">País</div><br>
		<div class="contModal" style="text-align: left; overflow-y: scroll; max-height: 360px;">
			<%-- lista creada por codigo --%>
		</div><br>
		<div class="btnModalPais">
			<a href="#" class="btnA simplemodal-close" style="margin-right: 10px;">Aceptar</a>
			<a href="#" class="btnA simplemodal-close" style="margin-left: 10px;">Siguiente nivel</a>
		</div>
	</div>
	<div class="clear"></div><br>
</div>

<div id="modaPDFPrev" class="modal">
	<div class="cuadroPrueba cuadroM">
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
		<div class="titModal">Previsualización</div><br>
		<iframe src="../../pdf/pdf.pdf" width="90%" height="450px"></iframe>
		<div class="btnCenter">
			<a href="#" class="btnA btnG simplemodal-close">Cancelar</a>
			<a href="#" class="btnA simplemodal-close">Aceptar</a>
		</div>
	</div>
	<div class="clear"></div><br>
</div>

<div id="cargaCSVModal" class="modal">
	<div class="cuadroDistrib">
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
		<div class="titModal">Crear lista por carga de CSV</div>
		<br>

		<div style="margin-left: 15px; margin-right: 15px;">
			<p style="text-align: left; margin-bottom: 2px;">Nombre de la lista</p>
			<input id="listNameModal" type="text">
		</div>
		<br>

		<div style="margin-left: 15px; margin-right: 15px;">
			<p style="text-align: left; margin-bottom: 2px;">Negocio</p>
  				<select id="listNegocioModal">
  					<option value="41">Sistemas financieros</option>
  				</select>
		</div>
		<br>

		<div id="theList" class="contModal" style="border-color: #C4C4C4; border-style: solid; border-width: 1px; background-color: #FFF; text-align: left; overflow-y: scroll; max-height: 232px;">
			<%-- lista generada por código --%>
		</div>
		<br>

		<div class="btnModalList">
			<a href="#" id="btnCancelarList" class="btnEditarList" style="margin-right: 10px;" onclick="closeCSVModalCA();">Cancelar</a>
			<a href="#" id="btnGuardarList" class="btnEnviarList" style="margin-left: 10px;" onclick="createDestinyBoxForCargaCSV(4);" >Agregar</a>
		</div>
	</div>
</div>

<div id="modalCarga2" class="modal">
	<div class="cuadroCategorias">
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
		<br>

		<div class="titModal">Carga de archivos</div>
		<br>

		<div id="cargaContainer" style="max-height: 240px; overflow-y: scroll;">
			<!-- se genera por codigo la table con la informacion y categorias de los archivos a cargar -->
		</div>
		<br><br>

		<div id="docContainer" style="min-height: 320px;">
			<div class="titModal">Documentos de la categoría</div><br>
			<div id="listDoc" style="max-height: 200px; overflow-y: scroll;">
				<!-- se genera lista de documentos -->
			</div>
			<br><br>

			<div class="titModal">Documento nuevo</div><br>
			<div style="margin-left: 4%; margin-right: 4%;">
				<p style="text-align: left; margin-bottom: 2px;">Nombre del documento</p>
				<div style="display: flex; left: auto; position: absolute;">
					<input id="docName" type="text" style="width: 70vh;">
					<a href="#" id="btnDocAgregar" class="btnEnviarList" style="margin-left: 6%;" onclick="saveSelectedDoc();">Agregar</a>
				</div>
			</div>
		</div>
		<br><br>

		<div class="btnModalList">
			<a href="#" id="btnCancelarList" class="btnEditarList" style="margin-right: 10px;" onclick="closeCSVModalCA();">Cancelar</a>
			<a href="#" id="btnGuardar" class="btnEnviarList" style="margin-left: 10px;" onclick="saveDocumentData();" >Guardar</a>
		</div>
	</div>
</div>

<div id="vistaPrevia" class="modal">
	<div class="cuadroVistaPrevia">
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a><br>
		<div id="vpTitle" class="titModal">Vista previa de archivo</div>
		<br>
		<div id="vpContainer" style="overflow-y: scroll; max-height: 75vh;">
			<!-- Se generan elementos por codigo -->
		</div>
	</div>
</div>

<div id="modalDocsBU" class="modal">
	<div class="cuadroDocsBU">
		<!--
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
		-->
		<br><br>
		<div class="titModal" id="docsBUTitle">Listado de documentos</div>
		<br>
		<div style="overflow-y: scroll; max-height: 45vh;">
			<div class="gris">
				<p>Se ha encontrado un documento agendado en una fecha posterior para esta categoría. ¿Qué desea hacer?</p>
				<table class="tblGeneral" style="display: block;">
					<thead style="display: block;">
						<tr style="display: inline-table; width: 100%;">
							<th style="width: 16%;">N&uacute;mero de folio</th>
							<th style="width: 16%;">Nombre del documento</th>
							<th style="width: 16%;">Categor&iacute;a</th>
							<th style="width: 16%;">Tipo de envio</th>
							<th style="width: 16%;">Vigencia inicial</th>
							<th style="width: 16%;">Visible en sucursal</th>
						</tr>
					</thead>
					<tbody id="listDocsBU" style="display: block; max-height: 240px; overflow-y: auto;"></tbody>
				</table>
			</div>
		</div>
		<div class="btnCenter">
			<a href="#" id="btnCancelProcess" class="btnA btnG" style="margin: 0px 10px 0 px 0px;">Detener carga</a>
			<a href="#" id="btnCancelDocs" class="btnA" style="margin: 0px;">Continuar</a>
		</div>
	</div>
</div>

<div class="loaderContainer" style="display: none;">
	<div class="ml-loader loader" style="">
		<div></div><div></div><div></div><div></div>
		<div></div><div></div><div></div><div></div>
		<div></div><div></div><div></div><div></div>
	</div>
</div>
<div class="blackScreen" style="display: none;"></div>

</body>
</html>