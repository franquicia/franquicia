<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<script type="text/javascript">
		var contextPath = '${pageContext.request.contextPath}';
		$(document).ready(function() {
			$('#query').on('input', function() {
				var val = $('#query').val();
				if (val.trim().length > 0) {
					$(':input[type="submit"]').prop('disabled', false);
					console.log('disabled');
				} else {
					$(':input[type="submit"]').prop('disabled', true);
					console.log('enabled');
				}
			});

			$('#tipo1').click(function(e) {
				$('#formato1').click();
				$('#divFormatos').show();
			});

			$('#tipo2').click(function(e) {
				$('#divFormatos').hide();
			});

			$('#tipo3').click(function(e) {
				$('#divFormatos').hide();
			});

			$('#tipo1').click();
			$('#formato1').click();
		});
	</script>

	<div class="contSecc">
		<div class="titSec">Consultas</div>
		<div class="gris">
			<form action="#" method="POST"> <!-- consultaQuery.htm -->

				<div id="divTipos" class="divCol2">
					<div class="col2">
						<input type="radio" name="tipo" id="tipo1" value="SELECT">
						<label for="tipo1">Consulta SELECT</label>
						<input type="radio" name="tipo" id="tipo2" value="DML">
						<label for="tipo2">DML Por Línea</label>
						<input type="radio" name="tipo" id="tipo3" value="DML2">
						<label for="tipo3">DML Por Archivo</label>
					</div>
				</div>

				<div id="divFormatos" class="divCol2">
					<div class="col2">
						<input type="radio" name="formato" id="formato1" value="HTML">
						<label for="formato1">Tabla HTML</label>
						<input type="radio" name="formato" id="formato2" value="XLSX">
						<label for="formato2">Archivo Excel</label>
						<input type="radio" name="formato" id="formato3" value="TXT">
						<label for="formato3">Archivo TXT</label>
					</div>
				</div>

				<div class="divCol2">
					<div class="col2">
						Consulta:
						<br>
						<textarea id="query" style="font-family: monospace; font-size: 16px; font-style: normal; height: 32vh;" name="query"></textarea>
					</div>
				</div>

				<div class="divCol3">
					<div class="col3">
						<input id="btn" type="submit" class="btnA" value="Enviar">
					</div>
				</div>
			</form>
		</div>
	</div>

</body>
</html>