<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>



<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> PROTOCOLO DE MEDICIÓN 7S</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome top60">
    			<div class="tsccion">
	    			<div class="tituloBoold"><b>Febrero 2019</b></div>
	    			<div class="sbBoold">Semana1</div><br>
                    <div>Da click en la sección para consultar el protocolo.</div>
	    			<div class="ccheks">
                        <div class=" overflow mh500">   
                            <table class="items tbCheck">
                                <tbody>
                                    <tr>   
                                        <td>
                                            <a href="cuestionario-semana1.htm"><b>I.SELECCIÓN</b></a>
                                        </td> 
                                        <td>
                                            <div class="estado ok"></div>
                                        </td>
                                    </tr>  
                                    <tr>   
                                        <td>
                                            <a href="cuestionario-semana1.htm"><b>I.ORDEN</b></a>
                                        </td> 
                                        <td>
                                            <div class="estado ok"></div>
                                        </td>
                                    </tr>
                                    <tr>   
                                        <td>
                                            <a href="cuestionario-semana1.htm"><b>III.LIMPIEZA</b></a>
                                        </td> 
                                        <td>
                                            <div class="estado ok"></div>
                                        </td>
                                    </tr>
                                    <tr>   
                                        <td>
                                            <a href="cuestionario-semana1.htm"><b>IV.SERVICIO</b></a>
                                        </td> 
                                        <td>
                                            <div class="estado warning"></div>
                                        </td>
                                    </tr>
                                    <tr>   
                                        <td>
                                            <a href="cuestionario-semana1.htm"><b>V.SEGURIDAD</b></a>
                                        </td> 
                                        <td>
                                            <div class="estado ok"></div>
                                        </td>
                                    </tr>
                                    <tr>   
                                        <td>
                                            <a href="cuestionario-semana1.htm"><b>VI.ESTANDARIZACIÓN</b></a>
                                        </td> 
                                        <td>
                                            <div class="estado ok"></div>
                                        </td>
                                    </tr>
                                    <tr>   
                                        <td>
                                            <a href="cuestionario-semana1.htm"><b>VII.DISCIPLINA Y SEGUIMIENTO</b></a>
                                        </td> 
                                        <td>
                                            <div class="estado ok"></div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table> 
                        </div> 
    			    </div>
    			</div>

                <div class="botones"> 
                    <a href="protocolo-de-medicion.htm" class="btn arrowl">
                        <div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
                        <div>VOLVER AL LISTADO</div>
                    </a>   
                </div>
    		</div>
    	</div>
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    </body>
</html>



