<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>



<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> FORMATO CUMPLIMIENTO METODOLOGÍA 7S</b></span>
    	</div>
    	<div class="page">
    		<c:choose>
	        <c:when test="${datos == 1}">
	    		<div class="contHome top60">
	    			<div class="ctAnex">
	    				<div class="tapoyo"> 
	    				${NOM_USUARIO}
	    				   				
		    			<div class="subAnexo"><b>Selecciona al líder responsable para visualizar los registros de su plantilla.</b></div><br>
	    		    </div>
	
	    			<div class="border borderLeft">  
						<div class=" overflow mh500">   
							<table class="items anexoDos">
								<tbody>
								 <c:forEach var="lista" items="${lista}">
									<tr>   
										<td>
	                                    	 <a href="#" class="Nusuario" onclick='myFunction("${lista.fecha}","${lista.ordenador}","${lista.venderMejor}","${lista.atenderMejor}")'><b><c:out value="${lista.fecha}"></c:out></b></a><br>	
										</td> 
									</tr>
									</c:forEach>
								</tbody>
							</table> 
						</div>  
				    </div>
					                
		    		<div class="botones"> 
						<a href="formato-cumplimiento.htm" class="btn arrowl">
							<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
							<div>VOLVER AL LISTADO</div>
						</a>   
					</div>
				</div>
	    	</div>
		</c:when>
		<c:when test="${datos == 0}">
				<div class="contHome top60">
					<div class="buscar"><img src="${pageContext.request.contextPath}/img/7s/buscar.png"></div>
					<span class="sinDatos"><b> NO SE ENCONTRARON DETALLES PARA CONSULTA.</b></span>
					<div class="tituloScont"><b>No existe ningún formato de cumplimento del usuario ${NOM_USUARIO} para mostrar en este momento.</b></div><br>
					
					<div class="botones"> 
						<a href="formato-cumplimiento.htm" class="btn arrowl">
							<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
							<div>VOLVER AL LISTADO</div>
						</a>   
					</div>
				</div>
			</c:when>
		</c:choose>

    	<!--MODAL-->	
        <div id="Usuario" class="modal">
            <div class="cuadromed">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="tsModal">${NOM_USUARIO}</div>
	                <div class="sbModal" id="fecha"></div>
	                <div class="border">  
						<div class=" overflow mh200 w100">   
							<table class="items UsModal">
								<tbody>
									<tr>   
										<td><b>Mantiene su lugar ordenado</b></td>
										<td><b><div id="ordenador"></div></b></td>
									</tr>  
									<tr>   
										<td><b>Lleva a cabo el camino para vender mejor</b></td>
										<td><b><div id="vmejor"></div></b></td> 
									</tr>
									<tr>   
										<td><b>Lleva a cabo el camino para atender mejor</b></td>
										<td><b><div id="amejor"></div></b></td> 
									</tr>
								</tbody>
							</table> 
						</div>  
				    </div><br>
				</div>
           </div>
           <!-- 
            <div class="bModalUser"> 
				<div class="tleft w50">
    				<a href="#" class="btn  bm arrowml">
    					<div></div>
    					<div>FORMATO ANTERIOR</div>
    				</a> 
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr">
                        <div></div>
                        <div>SIGUIENTE FORMATO</div>
                    </a> 
                </div> 
			</div>-->
        </div>
        
        <!-- -->


        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>

    	<!--MODAL-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.Nusuario').click(function (e) {
                        setTimeout(function() {
                            $('#Usuario').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            });
            
        </script>
         							<script type="text/javascript">
								 	
									    function myFunction(fecha,ordenador,vmejor,amejor){
										    if(ordenador==1){
										    	 var ordenador="NO";
											    }
										    else if(ordenador==0){
										    	 var ordenador="SI";
											    }
										    else if(ordenador==2){
											    var ordenador="NA";
											    }
										    if(vmejor==1){
										    	 var vmejor="NO";
											    }
										    else if(vmejor==0){
										    	 var vmejor="SI";
											    }
										    else if(vmejor==2){
											    var vmejor="NA";
											    }
										    if(amejor==1){
										    	 var amejor="NO";
											    }
										    else if(amejor==0){
										    	 var amejor="SI";
											    }
										    else if(amejor==2){
											    var amejor="NA";
											    }
										  
										    
									    document.getElementById('fecha').innerHTML = fecha;
									    document.getElementById('ordenador').innerHTML = ordenador;
									    document.getElementById('vmejor').innerHTML = vmejor;
									    document.getElementById('amejor').innerHTML = amejor;
									        }
									</script>


    </body>
</html>



