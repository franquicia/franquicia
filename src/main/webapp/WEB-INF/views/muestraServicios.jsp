<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script src="/franquicia/js/jquery/jquery-2.2.4.min.js"></script>
<script src="/franquicia/css/jQuery/jQuery.js"></script>
<script src="/franquicia/js/adminBD/checkpreg.js"></script>
<script src="/franquicia/js/adminBD/arbolDes.js"></script>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Response</title>

<style>
table, th, td {
	
	border: 1px solid black;

}

th {
    background-color: #4CAF50;
    color: white;
}
</style>

</head>
<body>
	<!-- MODULO -->
	<c:choose>
	
		<c:when test="${tipo == 'LISTA CANALES'}">
			<table>
				<thead> 
				<tr> 
				   <th>ID CANAL</th>
				   <th>DESCRIPCION</th>
				   <th>ACTIVO</th>
				</tr>				 
				</thead>
				<tbody>
			    	<c:forEach items="${res}" var="item">				
						<tr>
				   			<td>${item.idCanal}</td>
				   			<td>${item.descrpicion}</td>
				   			<td>${item.activo}</td>
						</tr>
					</c:forEach>
				</tbody>			
			</table>			
		</c:when>
		<c:when test="${tipo == 'LISTA PERFILES'}">			
			<table>
				<thead>
					<tr>
					  <th>ID PERFIL</th>
					  <th>DESCRIPCION</th>
					</tr>			
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idPerfil} </td>
							<td>${item.descripcion}</td>
						</tr>
	  				</c:forEach>
				</tbody>
			</table>			
		</c:when>
		<c:when test="${tipo == 'PUESTOS'}">
		
			<table>
				<thead>
					<tr> 
						<th>ID PUESTO</th>
						<th>ID NIVEL</th>
						<th>ID NEGOCIO</th>
						<th>ID TIPO PUESTO</th>
						<th>ID CANAL</th>
						<th>CODIGO</th>
						<th>DESCRIPCION</th>
						<th>ID SUB NEGOCIO</th>
						<th>ACTIVO</th>					
					</tr>
				</thead>	
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idPuesto}</td>
							<td>${item.idNivel}</td>
							<td>${item.idNegocio}</td>
							<td>${item.idTipoPuesto}</td>
							<td>${item.idCanal}</td>
							<td>${item.codigo}</td>
							<td>${item.descripcion}</td>
							<td>${item.idSubnegocio}</td>
							<td>${item.activo}</td>
							<td></td>
						</tr>
					</c:forEach>				
				</tbody>
			</table>
			
		</c:when>
		<c:when test="${tipo == 'LISTA TIPOS ARCHIVO'}">
			<c:forEach items="${res}" var="item">
				ID TIPO ARCHIVO:${item.idTipoArchivo} <br />
				NOMBRE TIPO: ${item.nombreTipo}<br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA CECOS'}">
			<c:forEach items="${res}" var="item">
				ID CECO: ${item.idCeco} <br />
				IP PAIS: ${item.idPais} <br />
				ID NEGOCIO: ${item.idNegocio} <br />
				ID CANAL: ${item.idCanal} <br />
				ID ESTADO: ${item.idEstado} <br />
				DESC CECO: ${item.descCeco} <br />
				ID CECO SUPERIOR${item.idCecoSuperior} <br />
				ACTIVO: ${item.activo} <br />
				ID NIVEL: ${item.idNivel} <br />
				CALLE: ${item.calle} <br />
				CIUDAD: ${item.ciudad} <br />
				CP: ${item.cp} <br />
				NOMBRE CONTACTO: ${item.nombreContacto} <br />
				PUESTO CONTACTO: ${item.puestoContacto} <br />
				TELEFONO CONTACTO: ${item.telefonoContacto} <br />
				FAX CONTACTO: ${item.faxContacto}	<br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA SUCURSALES'}">
			<c:forEach items="${res}" var="item">
				ID SUCURSAL: ${item.idSucursal} <br />
				ID PAIS: ${item.idPais} <br />
				ID CANAL: ${item.idCanal} <br />
				NU SUCURSAL: ${item.nuSucursal} <br />
				NOMBRE SUC: ${item.nombresuc} <br />
				LATITUD: ${item.latitud} <br />
				LONGITUD: ${item.longitud} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA USUARIOS A'}">
			<c:forEach items="${res}" var="item">
				ID USUARIO: ${item.idUsuario} <br />
				ID PUESTO: ${item.idPuesto} <br />
				ID CECO: ${item.idCeco} <br />
				ID NOMBRE: ${item.nombre} <br />
				ID ACTIVO: ${item.activo} <br />
				ID FECHA: ${item.fecha} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA PAISES'}">
			<c:forEach items="${res}" var="item">
				ID PAIS: ${item.idPais} <br />
				NOMBRE: ${item.nombre} <br />
				ACTIVO: ${item.activo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA HORARIOS'}">

			<c:forEach items="${res}" var="item">
				ID HORARIO: ${item.idHorario} <br />
				CVE HORARIO: ${item.cveHorario} <br />
				VALOR FIN: ${item.valorFin} <br />
				VALOR INI: ${item.valorIni} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA PARAMETROS'}">
			<c:forEach items="${res}" var="item">
				CLAVE: ${item.clave} <br />
				VALOR: ${item.valor} <br />
				ACTIVO: ${item.activo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>


		<c:when test="${tipo == 'LISTA ESTADO CHECKLIST'}">
			<c:forEach items="${res}" var="item">
				ID EDO CHECKLIST: ${item.idEdochecklist} <br />
				DESCRIPCION: ${item.descripcion} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA TIPOS CHECKLIST'}">
			<c:forEach items="${res}" var="item">
				ID TIPO CHECKLIST: ${item.idTipoCheck} <br />
				DESCRIPCION TIPO: ${item.descTipo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA TIPOS PREGUNTA'}">
			<c:forEach items="${res}" var="item">
				ID TIPO PREGUNTA: ${item.idTipoPregunta} <br />
				CVE PREGUNTA: ${item.cvePregunta} <br />
				DESCRICPION: ${item.descripcion} <br />
				ACTIVO: ${item.activo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA NEGOCIOS'}">
			<c:forEach items="${res}" var="item">
				ID NEGOCIO: ${item.idNegocio} <br />
				DESCRIPCION: ${item.descripcion} <br />
				ACTIVO: ${item.activo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA BITACORA'}">
			<c:forEach items="${res}" var="item">
				ID BITACORA: ${item.idBitacora} <br />
				ID CHECK USUA: ${item.idCheckUsua} <br />
				LONGITUD: ${item.longitud} <br />
				LATITUD: ${item.latitud} <br />
				FECHA INICIO: ${item.fechaInicio} <br />
				FECHA FIN: ${item.fechaFin} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA RESPUESTAS'}">
			<c:forEach items="${res}" var="item">
				ID RESPUESTA: ${item.idRespuesta} <br />
				ID BITACORA: ${item.idBitacora}<br />
				ID ARBOL: ${item.idArboldecision} <br />
				OBSERVACIONES: ${item.observacion} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA COMPROMISOS'}">
			<c:forEach items="${res}" var="item">
				ID COMPROMISO: ${item.idCompromiso} <br />
				ID RESPUESTA: ${item.idRespuesta} <br />
				DESRCRICPION: ${item.descripcion}<br />
				ESTATUS: ${item.estatus} <br />
				FECHA COMPROMISO: ${item.fechaCompromiso} <br />
				ID RESPONSABLE: ${item.idResponsable} <br />
				COMMIT: ${item.commit} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA EVIDENCIAS'}">
			<c:forEach items="${res}" var="item">
				ID EVIDENCIA: ${item.idEvidencia} <br />
				ID RESPUESTA: ${item.idRespuesta} <br />
				ID TIPO: ${item.idTipo}<br />
				RUTA: ${item.ruta} <br />
				COMMIT: ${item.commit} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA NIVELES'}">
			<c:forEach items="${res}" var="item">
				ID NIVEL: ${item.idNivel} <br />
				ID NEGOCIO: ${item.idNegocio} <br />
				CODIGO: ${item.codigo}<br />
				DESCRIPCION: ${item.descripcion} <br />
				ACTIVO: ${item.activo} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'ARBOL DESICION'}">
		
		<table  style="position:relative;  width:50%; ">
		  <thead>
		     <tr>
		        <th>ID ARBOL DESICION</th>
		        <th>ID CHECKLIST</th>
		        <th>ID PREGUNTA</th>
		        <th>RESPUESTA</th>
		        <th>EVIDENCIA</th>
		        <th>SIGUIENTE PREGUNTA</th>
		        <th>ACCION</th>
		        <th>OBSERVACIONES</th>
		        <th>EVIDENCIA OBLIGATORIA</th>
		        <th>ETIQUETA EVIDENCIA</th>
		        <th>Editar</th>
		     </tr>
		  </thead>
		  <tbody>
		     <c:forEach items="${res}" var="item">
				<tr align="center">				
				  <td>${item.idArbolDesicion}</td>
				  <td>${item.idCheckList} </td>
				  <td>${item.idPregunta}</td>
				  <td>${item.respuesta} </td>
				  <td>${item.estatusEvidencia} </td>
				  <td>${item.ordenCheckRespuesta} </td>				  
				  <td>${item.reqAccion}</td>
				  <td>${item.reqObservacion}</td>
				  <td>${item.reqOblig}</td>
				  <td>${item.descEvidencia} </td>
				  <td align="center"><input type="checkbox" id="${item.idArbolDesicion}" value="Editar" onclick="transformarEnEditable(this,${item.idArbolDesicion})" /> </td>
				
				</tr>
			</c:forEach>		  
		        
		  </tbody>		   
		</table>

	    <div id="contenedorForm" style="position:relative; display:inline-block; margin-top: 30px" > </div>
		</c:when>
		<c:when test="${tipo == 'GET CHECKLIST PREGUNTA'}">
			<table>
				<thead>
					<tr>
						<th>ID CHECKLIST</th>
						<th>ID PREGUNTA</th>
						<th>ORDEN PREGUNTA</th>
						<th>PREGUNTA PADRE</th>
						<th>EDITAR</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idChecklist}</td>
							<td>${item.idPregunta}</td>
							<td>${item.ordenPregunta}</td>
							<td> ${item.pregPadre}</td>
							 <td align="center"><input type="checkbox" id="${item.idPregunta}" value="Editar" onclick="transformarEnEditableCP(this,${item.idPregunta})" /> </td>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		
		<c:when test="${tipo == 'GET CHECK COMPLETO'}">
			<table  style="position:relative; width: 80%;">
				<thead>
					<tr>
						<th>ID CHECK USUARIO</th>
						<th>NOMBRE USUARIO</th>
						<th>ID BITACORA</th>
						<th>FECHA INICIO</th>
						<th>FECHA FIN</th>
						<th>FECHA MODIFICACION</th>
						<th>ID CECO</th>
						<th>NOMBRE CECO</th>
						<th>ID RESPUESTAS</th>
						<th>ID COMPROMISOS</th>
						<th>ID RESPUESTAS AD</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCheckUsuario}</td>
							<td>${item.nombreU}</td>
							<td>${item.idBitacora}</td>
							<td>${item.fechaIni}</td>
							<td>${item.fechaFin}</td>
							<td>${item.fechaModificacion}</td>
							<td>${item.idCeco}</td>
							<td> ${item.nombreCeco}</td>
							<td> ${item.respuestas}</td>
							<td> ${item.compromisos}</td>
							<td> ${item.respuestas_Ad}</td>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		
		
		<c:when test="${tipo == 'LISTA GEO'}">
			<table  style="position:relative; width: 80%;">
				<thead>
					<tr>
						<th>ID CECO</th>
						<th>ID REGION</th>
						<th>REGION</th>
						<th>ID ZONA</th>
						<th>ZONA</th>
						<th>ID TERRITORIO</th>
						<th>TERRITORIO</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCeco}</td>
							<td>${item.idRegion}</td>
							<td>${item.region}</td>
							<td>${item.idZona}</td>
							<td>${item.zona}</td>
							<td>${item.idTerritorio}</td>
							<td>${item.territorio}</td>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		
		<c:when test="${tipo == 'BITACORA CERRADA'}">
			<table  style="position:relative; width: 80%;">
				<thead>
					<tr>
						<th>ID CHECK USUARIO</th>
						<th>ID USUARIO</th>
						<th>NOMBRE USUARIO</th>
						<th>ID BITACORA</th>
						<th>FECHA INICIO</th>
						<th>FECHA FIN</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCheckUsua}</td>
							<td>${item.idUsuario}</td>
							<td>${item.usuario}</td>
							<td>${item.idBitacora}</td>
							<td>${item.fechaInicio}</td>
							<td>${item.fechaFin}</td>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		
		<c:when test="${tipo == 'GET CHECKLIST USUARIO'}">
			<c:forEach items="${res}" var="item">
				ID CHECKLIST USUARIO: ${item.idCheckUsuario} <br />
				ID CHECKLIST: ${item.idChecklist} <br />
				ID USUARIO: ${item.idUsuario} <br />
				ID CECO: ${item.idCeco} <br />
				ID ACTIVO: ${item.activo} <br />
				ID FECHA INI: ${item.fechaIni} <br />
				ID FECHA RESP: ${item.fechaResp} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'GET CHECKLIST'}">
			<c:forEach items="${res}" var="item">
				ID TIPO CHECKLIST: ${item.idTipoChecklist.idTipoCheck} <br />
				ID CHECKLIST: ${item.idChecklist} <br />
				NOMBRE CHECK: ${item.nombreCheck} <br />
				FECHA INI: ${item.fecha_inicio} <br />
				FECHA FIN: ${item.fecha_fin} <br />
				ID HORARIO: ${item.idHorario} <br />
				VIGENTE: ${item.vigente} <br />
				ID ESTADO: ${item.idEstado} <br />
				ID USUARIO: ${item.idUsuario} <br />
				DIA: ${item.dia} <br />
				PERIODO: ${item.periodicidad} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'GET MODULOS'}">
			<c:forEach items="${res}" var="item">
				ID MODULO: ${item.idModulo} <br />
				NOMBRE: ${item.nombre} <br />
				ID MODULO PADRE: ${item.idModuloPadre} <br />
				NOMBRE PADRE: ${item.nombrePadre} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'GET PREGUNTAS'}">
			<c:forEach items="${res}" var="item">
				ID PREGUNTA: ${item.idPregunta} <br />
				ID MODULO: ${item.idModulo} <br />
				ID TIPO: ${item.idTipo} <br />
				ESTATUS: ${item.estatus} <br />
				PREGUNTA: ${item.pregunta} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'REPORTE'}">
			<table id="reporte" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID CHECKLIST</th>
						<th>ID USUARIO</th>
						<th>NOMBRE USUARIO</th>
						<th>NOMBRE SUCURSAL</th>
						<th>NOMBRE CECO</th>
						<th>FECHA RESPUESTA</th>
						<th>SEMANA</th>
						<th>NOMBRE MODULO PADRE</th>
						<th>NOMBRE MODULO</th>
						<th>ID PREGUNTA</th>
						<th>DESCRIPCION PREGUNTA</th>
						<th>RESPUESTA</th>
						<th>NOMBRE CHECKLIST</th>
						<th>METODO CHECKLIST</th>
					</tr>
				</thead>

				</tbody>
				<c:forEach items="${res}" var="item">
					<tr>
						<td>${item.idChecklist}</td>
						<td>${item.idUsuario}</td>
						<td>${item.nombreUsuario}</td>
						<td>${item.nombreSucursal}</td>
						<td>${item.nombreCeco}<br />
						<td>${item.fechaRespuesta}<br />
						<td>${item.semana}</td>
						<td>${item.nombreModuloP}</td>
						<td>${item.nombreMod}</td>
						<td>${item.idPregunta}</td>
						<td>${item.desPregunta}</td>
						<td>${item.respuesta}</td>
						<td>${item.nombreChecklist}</td>
						<td>${item.metodoChecklist}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</c:when>
		<c:when test="${tipo == 'LISTA TOKENS'}">
			<c:forEach items="${res}" var="item">
				ID TOKEN: ${item.idToken} <br />
				ID USUARIO: ${item.usuario} <br />
				TOKEN: ${item.token} <br />
				ESTATUS: ${item.estatus}<br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LISTA LLAVES'}">
			<c:forEach items="${res}" var="item">
				ID LLAVE: ${item.idLlave} <br />
				LLAVE: ${item.llave} <br />
				DESCRIPCION: ${item.descripcion} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'LLAVE'}">
			<c:forEach items="${res}" var="item">
				ID LLAVE: ${item.idLlave} <br />
				LLAVE: ${item.llave} <br />
				DESCRIPCION: ${item.descripcion} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'GET CHECKLIST-USUARIO POR ID USUARIO'}">
			<c:forEach items="${res}" var="item">
				ID CHECKLIST USUARIO: ${item.idCheckUsuario} <br />
				ID CHECKLIST: ${item.idChecklist} <br />
				ID USUARIO: ${item.idUsuario} <br />
				ID CECO: ${item.idCeco} <br />
				ID ACTIVO: ${item.activo} <br />
				ID FECHA INI: ${item.fechaIni} <br />
				ID FECHA RESP: ${item.fechaResp} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'SUCURSAL'}">
			<c:forEach items="${res}" var="item">
				ID SUCURSAL: ${item.idSucursal} <br />
				ID PAIS: ${item.idPais} <br />
				ID CANAL: ${item.idCanal} <br />
				NU SUCURSAL: ${item.nuSucursal} <br />
				NOMBRE SUC: ${item.nombresuc} <br />
				LATITUD: ${item.latitud} <br />
				LONGITUD: ${item.longitud} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		
		<c:when test="${tipo == 'CECO'}">
			<c:forEach items="${res}" var="item">
				ID CECO: ${item.idCeco} <br />
				IP PAIS: ${item.idPais} <br />
				ID NEGOCIO: ${item.idNegocio} <br />
				ID CANAL: ${item.idCanal} <br />
				ID ESTADO: ${item.idEstado} <br />
				DESC CECO: ${item.descCeco} <br />
				ID CECO SUPERIOR${item.idCecoSuperior} <br />
				ACTIVO: ${item.activo} <br />
				ID NIVEL: ${item.idNivel} <br />
				CALLE: ${item.calle} <br />
				CIUDAD: ${item.ciudad} <br />
				CP: ${item.cp} <br />
				NOMBRE CONTACTO: ${item.nombreContacto} <br />
				PUESTO CONTACTO: ${item.puestoContacto} <br />
				TELEFONO CONTACTO: ${item.telefonoContacto} <br />
				FAX CONTACTO: ${item.faxContacto}	<br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		<c:when test="${tipo == 'USUARIO'}">
			<c:forEach items="${res}" var="item">
				ID USUARIO: ${item.idUsuario} <br />
				ID PUESTO: ${item.idPuesto} <br />
				ID PERFIL: ${item.idPerfil} <br />
				ID CECO: ${item.idCeco} <br />
				ID NOMBRE: ${item.nombre} <br />
				ID ACTIVO: ${item.activo} <br />
				ID FECHA: ${item.fecha} <br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		
		<c:when test="${tipo == 'ERRORES'}">
			<c:forEach items="${res}" var="item">
				ID LOG: ${item.idLog} <br />
				FECHA ERROR: ${item.fechaError} <br />
				CODIGO ERROR: ${item.codigoError} <br />
				MENSAJE ERROR: ${item.mensajeError} <br />
				ORIGEN ERROR: ${item.origenError}<br />
				<br />
				<br />
			</c:forEach>
		</c:when>
		
		<c:when test="${tipo == 'USUARIOS PASO'}">
			<table>
				<thead>
					<tr>
						<th>ID USUARIO</th>
						<th>ID PUESTO</th>
						<th>ID PERFIL</th>
						<th>ID CECO</th>
						<th>ID NOMBRE</th>
						<th>ID ACTIVO</th>
						<th>ID FECHA</th>
						<th>PUESTO F</th>
						<th>DESC PUESTO</th>
						<th>DESC PUESTO F</th>
						<th>PAIS</th>
						<th>FECHA BAJA</th>
						<th>ID EMPLEADO REP</th>
						<th>CC EMPLEADO REP</th>
					</tr>
				</thead>
				<c:forEach items="${res}" var="item">
					<tr>
						<td>${item.idUsuario}</td>
						<td>${item.idPuesto}</td>
						<td>${item.idPerfil}</td>
						<td>${item.idCeco}</td>
						<td>${item.nombre}</td>
						<td>${item.activo}</td>
						<td>${item.fecha}</td>
						<td>${item.puestof}</td>
						<td>${item.descPuesto}</td>
						<td>${item.desPuestof}</td>
						<td>PAIS: ${item.pais}</td>
						<td>${item.fechaBaja}</td>
						<td>${item.idEmpleadoRep}</td>
						<td>${item.ccEmpleadoRep}</td>
					</tr>
				</c:forEach>
			</table>
		</c:when>
		
		
		
		<c:when test="${tipo == 'LISTA MOVIL'}">
			<table>
				<thead>
					<tr>
						<th>ID MOVIL</th>
						<th>ID USUARIO</th>
						<th>FECHA</th>
						<th>SO</th>
						<th>VERSION</th>
						<th>VERSION APP</th>
						<th>MODELO</th>
						<th>FABRICANTE</th>
						<th>NUMERO MOVIL</th>
						<th>TIPO CONEXION</th>
						<th>IDENTIFICADOR</th>
						<th>TOKEN</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idMovil}</td>
							<td>${item.idUsuario}</td>
							<td>${item.fecha}</td>
							<td>${item.so}</td>
							<td>${item.version}</td>
							<td>${item.versionApp}</td>
							<td>${item.modelo}</td>
							<td>${item.fabricante}</td>
							<td>${item.numMovil}</td>
							<td>${item.tipoCon}</td>
							<td>${item.identificador}</td>
							<td>${item.token}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>
		

		<c:when test="${tipo == 'USUARIOS PASO PARAMS'}">
			<table>
				<thead>
					<tr>
						<th>ID USUARIO</th>
						<th>ID PUESTO</th>
						<th>ID PERFIL</th>
						<th>ID CECO</th>
						<th>ID NOMBRE</th>
						<th>ID ACTIVO</th>
						<th>ID FECHA</th>
						<th>PUESTO F</th>
						<th>DESC PUESTO</th>
						<th>DESC PUESTO F</th>
						<th>PAIS</th>
						<th>FECHA BAJA</th>
						<th>ID EMPLEADO REP</th>
						<th>CC EMPLEADO REP</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idUsuario}</td>
							<td>${item.idPuesto}</td>
							<td>${item.idPerfil}</td>
							<td>${item.idCeco}</td>
							<td>${item.nombre}</td>
							<td>${item.activo}</td>
							<td>${item.fecha}</td>
							<td>${item.puestof}</td>
							<td>${item.descPuesto}</td>
							<td>${item.desPuestof}</td>
							<td>PAIS: ${item.pais}</td>
							<td>${item.fechaBaja}</td>
							<td>${item.idEmpleadoRep}</td>
							<td>${item.ccEmpleadoRep}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>

		<c:when test="${tipo == 'CECO PASO'}">
			<table>
				<thead>
					<tr>
						<th>ID CECO</th>
						<th>ID PAIS</th>
						<th>ID NEGOCIO</th>
						<th>ID CANAL</th>
						<th>ID ESTADO</th>
						<th>DESC CECO</th>
						<th>ID CECO SUPERIOR</th>
						<th>ACTIVO</th>
						<th>ID NIVEL</th>
						<th>CALLE</th>
						<th>CIUDAD</th>
						<th>CP</th>
						<th>NOMBRE CONTACTO</th>
						<th>TELEFONO CONTACTO</th>
						<th>ID ENTIDAD</th>
						<th>NUMERO ECONOMICO</th>
						<th>NOMBRE ENTIDAD</th>
						<th>ENTIDAD PADRE</th>
						<th>NOMBRE CECO PADRE</th>
						<th>TIPO CANAL</th>
						<th>NOMBRE TIPO CANAL</th>
						<th>ACTIVO</th>
						<th>TIPO CECO</th>
						<th>TIPO OPERACION</th>
						<th>TIPO SUCURSAL</th>
						<th>NOMBRE TIPO SUCURSAL</th>
						<th>ID RESPONSABLE</th>
						<th>ID SIE</th>
						<th>CLASIFICACION SIE</th>
						<th>GASTO NEGOCIO</th>
						<th>GASTO X NEGOCIO</th>
						<th>FECHA APERTURA</th>
						<th>FECHA CIERRE</th>
						<th>PAIS</th>
						<th>FECHA CARGA</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idCeco}</td>
							<td>${item.idPais}</td>
							<td>${item.idNegocio}</td>
							<td>${item.idCanal}</td>
							<td>${item.idEstado}</td>
							<td>${item.descCeco}</td>
							<td>${item.idCCSuperior}</td>
							<td>${item.activo}</td>
							<td>${item.idNivel}</td>
							<td>${item.calle}</td>
							<td>${item.ciudad}</td>
							<td>${item.cp}</td>
							<td>${item.nombreContacto}</td>
							<td>${item.telefonoContacto}</td>
							<td>${item.entidadid}</td>
							<td>${item.numEconomico}</td>
							<td>${item.nombreEnt}</td>
							<td>${item.entidadPadre}</td>
							<td>${item.nombrePadre}</td>
							<td>${item.desCanal}</td>
							<td>${item.nomTipoCanal}</td>
							<td>${item.estatusCC}</td>
							<td>${item.tipoCC}</td>
							<td>${item.tipoOperacion}</td>
							<td>${item.tipoSucursal}</td>
							<td>${item.nomTipoSucursal}</td>
							<td>${item.idResponsable}</td>
							<td>${item.idSie}</td>
							<td>${item.clasfSie}</td>
							<td>${item.gastoNegocio}</td>
							<td>${item.gastoxNegocio}</td>
							<td>${item.fechaApertura}</td>
							<td>${item.fechaCierre}</td>
							<td>${item.desPais}</td>
							<td>${item.fechaCarga}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>

		<c:when test="${tipo == 'CECO PASO PARAMS'}">
			<table>
				<thead>
					<tr>
						<th>ID CECO</th>
						<th>ID PAIS</th>
						<th>ID NEGOCIO</th>
						<th>ID CANAL</th>
						<th>ID ESTADO</th>
						<th>DESC CECO</th>
						<th>ID CECO SUPERIOR</th>
						<th>ACTIVO</th>
						<th>ID NIVEL</th>
						<th>CALLE</th>
						<th>CIUDAD</th>
						<th>CP</th>
						<th>NOMBRE CONTACTO</th>
						<th>TELEFONO CONTACTO</th>
						<th>ID ENTIDAD</th>
						<th>NUMERO ECONOMICO</th>
						<th>NOMBRE ENTIDAD</th>
						<th>ENTIDAD PADRE</th>
						<th>NOMBRE CECO PADRE</th>
						<th>TIPO CANAL</th>
						<th>NOMBRE TIPO CANAL</th>
						<th>ACTIVO</th>
						<th>TIPO CECO</th>
						<th>TIPO OPERACION</th>
						<th>TIPO SUCURSAL</th>
						<th>NOMBRE TIPO SUCURSAL</th>
						<th>ID RESPONSABLE</th>
						<th>ID SIE</th>
						<th>CLASIFICACION SIE</th>
						<th>GASTO NEGOCIO</th>
						<th>GASTO X NEGOCIO</th>
						<th>FECHA APERTURA</th>
						<th>FECHA CIERRE</th>
						<th>PAIS</th>
						<th>FECHA CARGA</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach items="${res}" var="item">
					<tr>
						<td>${item.idCeco}</td>
							<td>${item.idPais}</td>
							<td>${item.idNegocio}</td>
							<td>${item.idCanal}</td>
							<td>${item.idEstado}</td>
							<td>${item.descCeco}</td>
							<td>${item.idCCSuperior}</td>
							<td>${item.activo}</td>
							<td>${item.idNivel}</td>
							<td>${item.calle}</td>
							<td>${item.ciudad}</td>
							<td>${item.cp}</td>
							<td>${item.nombreContacto}</td>
							<td>${item.telefonoContacto}</td>
							<td>${item.entidadid}</td>
							<td>${item.numEconomico}</td>
							<td>${item.nombreEnt}</td>
							<td>${item.entidadPadre}</td>
							<td>${item.nombrePadre}</td>
							<td>${item.desCanal}</td>
							<td>${item.nomTipoCanal}</td>
							<td>${item.estatusCC}</td>
							<td>${item.tipoCC}</td>
							<td>${item.tipoOperacion}</td>
							<td>${item.tipoSucursal}</td>
							<td>${item.nomTipoSucursal}</td>
							<td>${item.idResponsable}</td>
							<td>${item.idSie}</td>
							<td>${item.clasfSie}</td>
							<td>${item.gastoNegocio}</td>
							<td>${item.gastoxNegocio}</td>
							<td>${item.fechaApertura}</td>
							<td>${item.fechaCierre}</td>
							<td>${item.desPais}</td>
							<td>${item.fechaCarga}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS GUATEMALA'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS SF: ${res[1]} <br />
				EJECUCION: ${res[2]} <br />
				REGISTROS AFECTADOS COMERCIO: ${res[3]} <br />
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS PERU'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS SF: ${res[1]} <br />
				EJECUCION: ${res[2]} <br />
				REGISTROS AFECTADOS COMERCIO: ${res[3]} <br />
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS HONDURAS'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS SF: ${res[1]} <br />
				EJECUCION: ${res[2]} <br />
				REGISTROS AFECTADOS COMERCIO: ${res[3]} <br />
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS PANAMA'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS SF: ${res[1]} <br />
				
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS SALVADOR'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS SF: ${res[1]} <br />
				
		</c:when>
		
		<c:when test="${tipo == 'CARGA CECOS LAM'}">
				PAIS: GUATEMALA <br />
				EJECUCION SF: ${res.get("ejecucionSFGuatemala")} <br />
				REGISTROS AFECTADOS SF: ${res.get("registrosSFGuatemala")} <br />
				EJECUCION COMERCIO: ${res.get("ejecucionCOMGuatemala")} <br />
				REGISTROS AFECTADOS COMERCIO: ${res.get("registrosCOMGuatemala")} <br />
				<br />
				PAIS: PERU <br />
				EJECUCION SF: ${res.get("ejecucionSFPeru")} <br />
				REGISTROS AFECTADOS SF: ${res.get("registrosSFPeru")} <br />
				EJECUCION COMERCIO: ${res.get("ejecucionCOMPeru")} <br />
				REGISTROS AFECTADOS COMERCIO: ${res.get("registrosCOMPeru")} <br />
				<br />
				PAIS: HONDURAS <br />
				EJECUCION SF: ${res.get("ejecucionSFHonduras")} <br />
				REGISTROS AFECTADOS SF: ${res.get("registrosSFHonduras")} <br />
				EJECUCION COMERCIO: ${res.get("ejecucionCOMHonduras")} <br />
				REGISTROS AFECTADOS COMERCIO: ${res.get("registrosCOMHonduras")} <br />
				<br />
				PAIS: PANAMA <br />
				EJECUCION SF: ${res.get("ejecucionSFPanama")} <br />
				REGISTROS AFECTADOS SF: ${res.get("registrosSFPanama")} <br />
				<br />
				PAIS: EL SALVADOR <br />
				EJECUCION SF: ${res.get("ejecucionSFSalvador")} <br />
				REGISTROS AFECTADOS SF: ${res.get("registrosSFSalvador")} <br />
		</c:when>
		
		<c:when test="${tipo == 'CARGA PERFILES'}">
				EJECUCION: ${res[0]} <br />
				REGISTROS AFECTADOS : ${res[1]} <br />				
		</c:when>		
		
		<c:when test="${tipo == 'CECOS TRABAJO'}">
				COLUMNAS AFECTADAS: ${res} <br />
		</c:when>
		
		<c:when test="${tipo == 'PERFILES USUARIO'}">
			<table align="left">
				<thead>
					<tr>
						<th>ID USUARIO</th>
						<th>ID PERFIL</th>												
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idUsuario}</td>
							<td>${item.idPerfil}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:when>
		
		<c:when test="${tipo == 'RECURSOS'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID RECURSO</th>
						<th>RECURSO</th>												
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idRecurso}</td>
							<td>${item.nombreRecurso}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'POSIBLES TIPO PREGUNTA'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID</th>
						<th>ID TIPO PREGUNTA</th>
						<th>ID POSIBLE</th>
						<th>RESPUESTA</th>																		
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idPosibleTipoPregunta}</td>
							<td>${item.idTipoPregunta}</td>
							<td>${item.idPosibleRespuesta}</td>
							<td>${item.descripcionPosible}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'POSIBLES '}">
           <table align="left">
				<thead>
					<tr>
						<th>ID</th>
						<th>RESPUESTA</th>																		
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idPosible}</td>
							<td>${item.descripcion}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'GET RESPUESTAS ADICIONALES'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID RESPUESTA</th>
						<th>ID RESPUESTA ADICIONAL</th>
						<th>RESPUESTA</th>																		
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idRespuestaAd}</td>
							<td>${item.idRespuesta}</td>
							<td>${item.descripcion}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'GET RECURSOS PERFIL'}">
			<table>
				<thead>
					<tr>
						<th>ID RECURSO</th>
						<th>ID PERFIL</th>
						<th>CONSULTA</th>
						<th>INSERTA</th>
						<th>MODIFICA</th>
						<th>ELIMINA</th>
					</tr>					
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idRecurso}</td>
							<td>${item.idPerfil}</td>
							<td>${item.consulta}</td>
							<td> ${item.inserta}</td>
							<td> ${item.modifica}</td>
							<td> ${item.elimina}</td>							
<%-- 							 <td align="center"><input type="checkbox" id="${item.idPregunta}" value="Editar" onclick="transformarEnEditableCP(this,${item.idPregunta})" /> </td> --%>
						</tr>			
					</c:forEach>
				</tbody>			
			</table>
		</c:when>
		<c:when test="${tipo == 'LISTA CHECKLIST NEGOCIO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID CHECKLIST</th>
						<th>ID NEGOCIO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.checklist}</td>
							<td>${item.negocio}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'LISTA NEGOCIO ADICIONAL'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID USUARIO</th>
						<th>ID NEGOCIO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.usuario}</td>
							<td>${item.negocio}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'LISTA PAIS NEGOCIO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID NEGOCIO</th>
						<th>ID PAIS</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idNegocio}</td>
							<td>${item.idPais}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'GET IMAGENES ARBOL'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID IMAGEN</th>
						<th>ID ARBOL</th>
						<th>URL IMG</th>
						<th>DESCRIPCION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idImagen}</td>
							<td>${item.idArbol}</td>
							<td>${item.urlImg}</td>
							<td>${item.descripcion}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		
		<c:when test="${tipo == 'LISTA GRUPO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID GRUPO</th>
						<th>DESCRIPCION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idGrupo}</td>
							<td>${item.descripcion}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		
		<c:when test="${tipo == 'LISTA ORDEN GRUPO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID ORDEN GRUPO</th>
						<th>ID CHECKLIST</th>
						<th>ID GRUPO</th>
						<th>ORDEN</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idOrdenGrupo}</td>
							<td>${item.idChecklist}</td>
							<td>${item.idGrupo}</td>
							<td>${item.orden}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		
		
		<c:when test="${tipo == 'LISTA PERIODO'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID GRUPO</th>
						<th>DESCRIPCION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idGrupo}</td>
							<td>${item.descripcion}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'LISTA NOTIFICACION'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID MOVIL</th>
						<th>ID USUARIO</th>
						<th>NOMBRE USUARIO</th>
						<th>ID PUESTO</th>
						<th>FECHA</th>
						<th>SO</th>
						<th>VERSION</th>
						<th>VERSION APP</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idMovil}</td>
							<td>${item.idUsuario}</td>
							<td>${item.nombreU}</td>
							<td>${item.idPuesto}</td>
							<td>${item.fecha}</td>
							<td>${item.so}</td>
							<td>${item.version}</td>
							<td>${item.versionApp}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'Status'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID STATUS</th>
						<th>DESCRIPCION</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idStatus}</td>
							<td>${item.descripcion}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'FormatoArch'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID FOR_MARCHIVERO</th>
						<th>ID_USUARIO</th>
						<th>NOMBRE</th>
						<th>CECO</th>
						<th>ID_RECIBE</th>
						<th>NOMBRE_RECIBE</th>
						<th>ID_STATUS</th>
						<th>FECHA</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idFromArchivero}</td>
							<td>${item.idUsuario}</td>
							<td>${item.nombre}</td>
							<td>${item.idCeco}</td>
							<td>${item.idRecibe}</td>
							<td>${item.nomRecibe}</td>
							<td>${item.idStatus}</td>
							<td>${item.fecha}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'Archivero'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID_ARCHIVERO</th>
						<th>PLACA</th>
						<th>MARCA</th>
						<th>DESCRIPCION</th>
						<th>OBSERVACIONES</th>
						<th>ID_FORM_ARCHIVERO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idArchivero}</td>
							<td>${item.placa}</td>
							<td>${item.marca}</td>
							<td>${item.descripcion}</td>
							<td>${item.observaciones}</td>
							<td>${item.idFormArchivero}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		
		<c:when test="${tipo == 'GET ASIGNACIONES'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID CHEKLIST</th>
						<th>ID CECO</th>
						<th>ID PUESTO</th>
						<th>ACTIVO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idChecklist}</td>
							<td>${item.ceco}</td>
							<td>${item.idPuesto}</td>
							<td>${item.activo}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'EJECUTE ASIGNACIONES' }">
		   <c:out value="${res['respuestaString']} "></c:out>
		   <br>
		   <c:out value="${res['respuestaBoolean']} "></c:out>
		</c:when>
	   <c:when test="${tipo == 'EJECUTE ASIGNACIONES_NEW' }">
		   Ejecucion :<c:out value="${res['respuestaString']} " />
		   <br>
		   Errores :<c:out value="${res['respuestaBoolean']} "></c:out>
		</c:when>
		<c:when test="${tipo == 'SUCURSAL GCC'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID SUCURSAL</th>
						<th>ID PAIS</th>
						<th>ID CANAL</th>
						<th>NO. SUCURSAL</th>
						<th>NOMBRE</th>
						<th>LONGITUD</th>
						<th>LATITUD</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idSucursal}</td>
							<td>${item.idPais}</td>
							<td>${item.idCanal}</td>
							<td>${item.nuSucursal}</td>
							<td>${item.nombresuc}</td>
							<td>${item.longitud}</td>
							<td>${item.latitud}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
		<c:when test="${tipo == 'NOTIFICACION REGIONAL' }">
		   <c:out value="${res} "></c:out>
		</c:when>
		
		
		<c:when test="${tipo == 'LISTA DE TAREAS'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID TAREA</th>
						<th>CLAVE</th>
						<th>DATE</th>
						<th>FECHA</th>
						<th>ACTIVO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idTarea}</td>
							<td>${item.cveTarea}</td>
							<td>${item.fechaTarea}</td>
							<td>${item.strFechaTarea}</td>
							<td>${item.activo}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
			
			<c:when test="${tipo == 'Fijo'}">
           <table align="left">
				<thead>
					<tr>
						<th>ID EMPLEADO FIJO</th>
						<th>ID USUARIO</th>
						<th>ACTIVO</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${res}" var="item">
						<tr>
							<td>${item.idEmpFijo}</td>
							<td>${item.idUsuario}</td>
							<td>${item.idActivo}</td>
							
						</tr>
					</c:forEach>
				</tbody>
			</table>		
		</c:when>
						
		<c:otherwise>
			<p>ERROR!!!!</p>
		</c:otherwise>
	</c:choose>
</body>
</html>