<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<script type="text/javascript">
		var contextPath = '${pageContext.request.contextPath}';
		$(document).ready(function() {
			$('#radio1').click();
			$('#radio3').click();
		});
	</script>

	<div class="contSecc">
		<div class="titSec">Carga de cecos</div>
		<div class="gris">
			Consulta:
			<br>
			<textarea id="data" name="data" style="font-family: monospace; font-size: 16px; font-style: normal; height: 32vh;"></textarea>
		</div>

		<div class="btnCenter">
			<input type="submit" class="btnLogin" value="Cargar cecos" id="btnCecos" onclick="sendCecosData();">
		</div>
	</div>

	<!-- LOADER -->
	<div class="loaderContainer" style="display: none;">
		<div class="ml-loader loader" style="">
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
			<div></div><div></div><div></div><div></div>
		</div>
	</div>
	<div class="blackScreen" style="display: none;"></div>

</body>
</html>