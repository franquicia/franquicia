<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    	<script type="text/javascript">
    		var carName = "Volvo";
    		function myFunction(aux, imagen){
    			document.getElementById('contenido').innerHTML = aux;

    			document.getElementById("mi_imagen").src =imagen+'/img/7s/cerrar.png';
    			alert(aux+" , "+imagen);
        	}

    		function fechas(fantes,fdespues,hdespues,hantes,rFotoAntes,rFotoDespues){
			    document.getElementById('fechaantes').innerHTML = fantes;
			    document.getElementById('fechadespues').innerHTML = fdespues;
			    document.getElementById('horadespues').innerHTML = hdespues;
			    document.getElementById('horaantes').innerHTML = hantes;
			    document.getElementById('fAntes').src = "http://10.53.33.82"+rFotoAntes;
			    document.getElementById('fAnteszoom').src = "http://10.53.33.82"+rFotoAntes;
			    document.getElementById('fDespues').src = "http://10.53.33.82"+rFotoDespues;
			    document.getElementById('fDespueszoom').src = "http://10.53.33.82"+rFotoDespues;
			 
			        } 
    	</script>
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> EVIDENCIAS FOTOGRÁFICAS DEL ANTES Y DESPUÉS</b></span>
    	</div>
    	<div class="page">
    		<c:choose>
	        <c:when test="${datos == 1}">
	    		<div class="contHome top60">
	    			<div class="tituloScont"><b>Selecciona la fecha para consultar la Evidencia Fotográfica.</b></div><br>
	    			<div class="border borderLeft">  
						<div class="overflow mh500">   
							<table class="items anexoDos">
								<tbody>
									<c:forEach var="lista12" items="${lista12}">
											<tr>
												<td>
													<a href="#" class="MLimpieza" onclick='fechas("${lista12.fechaantes}","${lista12.fechadespues}","${lista12.horadespues }","${lista12.horaantes }","${lista12.rutafotoantes}","${lista12.rutafotodespues}")'>Evidencia Fotográfica <c:out value="${lista12.fechaantes}" ></c:out></a>
												</td> 
											</tr>
										</c:forEach>
									
								</tbody>
							</table> 
						</div>  
				    </div>
	    		
		    		<div class="botones"> 
						<a href="inicio7s.htm" class="btn arrowl">
							<div><img src="${pageContext.request.contextPath}/img/7s/arrowleft.png"></div>
							<div>VOLVER AL MENÚ</div>
						</a>   
					</div>
	
				</div>
			</c:when>
			<c:when test="${datos == 0}">
				<div class="contHome top60">
					<div class="buscar"><img src="${pageContext.request.contextPath}/img/7s/buscar.png"></div>
					<span class="sinDatos"><b> NO SE ENCONTRARON DETALLES PARA CONSULTA.</b></span>
					<div class="tituloScont"><b>No existe ningúna evidencia para mostrar en este momento.</b></div><br>
					
					<div class="botones"> 
						<a href="inicio7s.htm" class="btn arrowl">
							<div><img src="${pageContext.request.contextPath}/img/7s/arrowleft.png"></div>
							<div>VOLVER AL MENÚ</div>
						</a>   
					</div>
				</div>
			</c:when>
			</c:choose>
    	</div>




    	<!--MODAL-->
        <div id="limpieza" class="modal">
            <div class="cuadrobig">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser">

	                <div class="sbModal"><b>Fotografía Antes</b></div><br>

	                <div class="cfModal">
	                	<!--IMAGEN PREVIA-->

	                	<img id="fAntes" src="${pageContext.request.contextPath}/img/fotomModla.png" height="210" >

	                	<!--BOTON ZOOM-->
	                	<div class="zoom">
	                		<img src="${pageContext.request.contextPath}/img/zoom.png">
	                	</div>
	                </div>


	                <b><div id="fechaantes"></div></b>
	                <b><div id="horaantes"></div></b>
	                

	                <div class="sbModal"><b>Fotografía Después</b></div><br>

	                <div class="cfModal">
	                	<!--IMAGEN PREVIA-->
	                	<img id="fDespues" src="${pageContext.request.contextPath}/img/fotomModla.png" height="210" >
	                	
	                	<!--BOTON ZOOM-->
	                	<div class="zoom2">
	                		<img src="${pageContext.request.contextPath}/img/zoom.png">
	                	</div>
	                </div>

	                <!--CONTENEDOR DONDE SE VISUALIZA LA IMAGEN EN TAMAÑO REAL-->
		            <div class="imgfull">
		                <div class="full">
		                	<img id="fAnteszoom" src="${pageContext.request.contextPath}/img/img-full.jpg">
		                	
		                	
		                </div>
		                <div class="zomDos">
		                	<img src="${pageContext.request.contextPath}/img/zoom-out.png">
		                </div>
		            </div>
		            
		              <div class="imgfull2">
		                <div class="full">
		                	<img id="fDespueszoom" src="${pageContext.request.contextPath}/img/img-full.jpg">
		                	
		                </div>
		                <div class="zomDos2">
		                	<img src="${pageContext.request.contextPath}/img/zoom-out.png">
		                </div>
		            </div>
		            
	                <b><div id="fechadespues"></div></b>
	                <b><div id="horadespues"></div></b>
				</div>
           </div>
           <!-- 
            <div class="bModalUser"> 
				<div class="tleft w50">
    				<a href="#" class="btn  bm arrowml">
    					<div></div>
    					<div>FOTO ANTERIOR</div>
    				</a> 
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr">
                        <div></div>
                        <div>FOTO ENTRADA</div>
                    </a> 
                </div> 
			</div> -->
        </div>
        <!-- -->

        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    	<!--MODAL-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.MLimpieza').click(function (e) {
                        setTimeout(function() {
                            $('#limpieza').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            }); 
        </script>
       <script>

			$( ".zoom" ).click(function() {
			  $( ".imgfull" ).addClass("box");
			 
			});
			$( ".zomDos" ).click(function() {
			  $( ".imgfull" ).removeClass("box");
			  
			});

			$( ".zoom2" ).click(function() {
				  $( ".imgfull2" ).addClass("box");
				 
				});
				$( ".zomDos2" ).click(function() {
				  $( ".imgfull2" ).removeClass("box");
				  
				});
			
		

		</script>
		      
</body>
