<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script>
		var contextPath = '${pageContext.request.contextPath}';
		var _negocio = 41; // Pruebas

		$(document).ready(function() {
			loadHAF();
		});
	</script>

	<div class="clear"></div>
	<div class="titulo">
		<div class="ruta">
			<a href="estatusGeneral.htm">Página Principal</a> / <a href="historicoFolios.htm">Histórico de folios</a>
		</div>
		Histórico de folios
	</div>

	<!-- Contenido -->
	<div class="contSecc">
		<div class="titSec">Histórico de folios</div>
		<div class="contOp">
			<table id="historicTable" class="tblPaginador tblGeneral">
				<thead>
					<tr>
						<th class="thNotificacion">Número de folio</th>
						<th class="thNotificacion">Nombre del documento</th>
						<th class="thNotificacion">Categoría</th>
						<th class="thNotificacion">Fecha de carga</th>
						<th class="thNotificacion">Fecha de visualización</th>
						<th class="thNotificacion">Vigencia</th>
						<th class="thNotificacion">Tipo de envio</th>
						<th class="thNotificacion">Visibilidad</th>
					</tr>
				</thead>
				<tbody id="htContainer"></tbody>
			</table>
		</div>
	</div>

	<div class="loaderContainer" style="display: none;">
		<div class="ml-loader loader" style="">
			<div></div><div></div>
			<div></div><div></div>
			<div></div><div></div>
			<div></div><div></div>
			<div></div><div></div>
			<div></div><div></div>
		</div>
	</div>
	<div class="blackScreen" style="display: none;"></div>

</body>
</html>