<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> ESTANDARIZACIÓN POR PUESTO</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome top60">
    			<div class="tituloScont"><b>Selecciona el tipo de mobiliario que deseas consultar.</b></div><br>


    			<div class="w46a">
	    			<div class="tPuesto"><b>SERVICIOS FINANCIEROS</b></div>
	    			<div class="border borderLeft">  
						<div class=" overflow mh500">   
							
								
									<table class="items tbFinancieros">
										<tbody>
											 
											<c:forEach var="listaF" items="${listaF}">
												<c:if test="${listaF.tipoMueble == 1}">	
												<tr>
													<td>
													<a href="#" class="Mfinancieros2">Mobiliario 2 cajones</a>
													</td> 
												</tr>
												</c:if>
												<c:if test="${listaF.tipoMueble == 0 }">	
												<tr>
													<td>
													<a href="#" class="Mfinancieros3">Mobiliario 3 cajones</a>
													</td> 
												</tr>
												
												</c:if>
											</c:forEach> 
											
										</tbody>
									</table> 
								
							
						</div>  
			        </div>
			    </div>

			    <div class="w46b">
	    			<div class="tPuesto"><b>CAJEROS</b></div>
	    			<div class="border borderLeft">  
						<div class=" overflow mh200"> 
							 
									<table class="items tbCaja">
										<tbody>
											<c:forEach var="listaC" items="${listaC}">
												<c:if test="${listaC.tipoMueble == 1}">	
												<tr>
													<td>
													<a href="#" class="cajeros2">Mobiliario 2 cajones</a>
													
													
													
													</td> 
												</tr>
												</c:if>
												<c:if test="${listaC.tipoMueble == 0}">	
												<tr>
													<td>
													<a href="#" class="cajeros3">Mobiliario 3 cajones</a>
													</td> 
												</tr>
												</c:if>
											</c:forEach>  
										</tbody>
									</table>
								
						</div>  
			        </div>
			    </div>

    		    <div class="clear"></div><br>
		


			    <div class="w46a">
	    			<div class="tPuesto"><b>GERENTES</b></div>
	    			<div class="border borderLeft">  
						<div class=" overflow mh200">
							
									<table class="items tbCaja">
										<tbody>
											<c:forEach var="listaG" items="${listaG}">
											<c:if test="${listaG.tipoMueble == 1}">	
												<tr>
													<td>
												<a href="#" class="gerentes2">Mobiliario 2 cajones</a>
													
													</td> 
												</tr>
											</c:if>
											<c:if test="${listaG.tipoMueble == 0}">
												<tr>
													<td>
												<a href="#" class="gerentes3">Mobiliario 3 cajones</a>
													</td> 
												</tr>
											</c:if>
											
										

											</c:forEach>  
										</tbody>
									</table>
								
						</div>  
			        </div>
			    </div>

    		    <div class="clear"></div><br>


	    		<div class="botones"> 
					<a href="inicio7s.htm" class="btn arrowl">
						<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
						<div>VOLVER AL MENÚ</div>
					</a>   
				</div>
			</div>
    	</div>

    	<!--MODAL-->
<c:forEach var="listaF" items="${listaF}">
<c:if test="${listaF.tipoMueble == 1}">	
    	<div id="financieros2" class="modal">
            <div class="cuadromed">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="tsModal">Servicios Financieros</div>
	                <div class="sbModal"><b>Mobiliario 2 cajones</b></div><br>

	                <div class="cArticulo admodal">

		                <div class="w50 cmueble">
		                	<img src="${pageContext.request.contextPath}/img/mueble2.png" class="mueble01">
		                </div>
		                
		                <div class="w50 cmueble">
		                	<div class="detProducto overflow">
		                		
		                		<div class="hed">
		                		    <b>Cajón 1</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>
			                		${listaF.cajon1}
									</b>
			                	</div>
			                	
			                	<div class="hed">
		                		    <b>Cajón 2</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaF.cajon2}</b> 
			                	</div>
		                	</div>
		                </div>
		                
		                <div class="clear"></div>
	                </div>
				</div>
				<div class="clear"></div><br><br><br>
           </div>
				<c:choose>
    				<c:when test="${listaF.tipoMueble==1}">
						
    				</c:when>    
    			<c:otherwise>
    				<div class="bModalUser"> 
						<div class="tleft w50">
		    				<a href="#" class="btn  bm arrowml simplemodal-close Mfinancieros3">
		    					<div></div>
		    					<div>MUEBLE ANTERIOR</div>
		    				</a> 
		                </div>
		
		                <div class="tright w50"> 
		                    <a href="#" class="btn bm arrowmr simplemodal-close Mfinancieros3">
		                        <div></div>
		                        <div>SIGUIENTE MUEBLE</div>
		                    </a> 
		                </div> 
					</div>
				</c:otherwise>
			</c:choose>
        </div>
</c:if>
<c:if test="${listaF.tipoMueble == 0}">	
    	<div id="financieros3" class="modal">
            <div class="cuadromed">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="tsModal">Servicios Financieros</div>
	                <div class="sbModal"><b>Mobiliario 3 cajones</b></div><br>

	                <div class="cArticulo admodal">

		                <div class="w50 cmueble">
		                	<img src="${pageContext.request.contextPath}/img/mueble.png" class="mueble01">
		                </div>
		                
		                <div class="w50 cmueble">
		                	<div class="detProducto overflow">
		                		<div class="hed">
		                		    <b>Cajón 1</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaF.cajon1}</b>
			                	</div>
			                	<div class="hed">
		                		    <b>Cajón 2</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaF.cajon2}</b> 
			                	</div>

			                	<div class="hed">
		                		    <b>Cajón 3</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaF.cajon3}</b>
			                	</div>
		                	</div>
		                </div>
		                <div class="clear"></div>
	                </div>
				</div>
				<div class="clear"></div><br><br><br>
           </div>

           <c:choose>
    				<c:when test="${listaF.tipoMueble==0}">
						
    				</c:when>    
    			<c:otherwise>
    				<div class="bModalUser"> 
						<div class="tleft w50">
		    				<a href="#" class="btn  bm arrowml simplemodal-close Mfinancieros2 ">
		    					<div></div>
		    					<div>MUEBLE ANTERIOR</div>
		    				</a> 
		                </div>
		
		                <div class="tright w50"> 
		                    <a href="#" class="btn bm arrowmr simplemodal-close Mfinancieros2 ">
		                        <div></div>
		                        <div>SIGUIENTE MUEBLE</div>
		                    </a> 
		                </div> 
					</div>
				</c:otherwise>
			</c:choose>
        </div>
        </c:if>
      </c:forEach>

      <c:forEach var="listaC" items="${listaC}">
		<c:if test="${listaC.tipoMueble == 1}">	
    	<div id="cajeros2" class="modal">
            <div class="cuadromed">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="tsModal">Servicios Cajeros</div>
	                <div class="sbModal"><b>Mobiliario 2 cajones</b></div><br>

	                <div class="cArticulo admodal">

		                <div class="w50 cmueble">
		                	<img src="${pageContext.request.contextPath}/img/mueble2.png" class="mueble01">
		                </div>
		                
		                <div class="w50 cmueble">
		                	<div class="detProducto overflow">
		                		
		                		<div class="hed">
		                		    <b>Cajón 1</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>
			                		${listaC.cajon1}
									</b>
			                	</div>
			                	
			                	<div class="hed">
		                		    <b>Cajón 2</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaC.cajon2}</b> 
			                	</div>
		                	</div>
		                </div>
		                
		                <div class="clear"></div>
	                </div>
				</div>
				<div class="clear"></div><br><br><br>
           </div>

            <c:choose>
    				<c:when test="${listaC.tipoMueble==1}">
						
    				</c:when>    
    			<c:otherwise>
    				<div class="bModalUser"> 
						<div class="tleft w50">
		    				<a href="#" class="btn  bm arrowml simplemodal-close cajeros3 ">
		    					<div></div>
		    					<div>MUEBLE ANTERIOR</div>
		    				</a> 
		                </div>
		
		                <div class="tright w50"> 
		                    <a href="#" class="btn bm arrowmr simplemodal-close cajeros3 ">
		                        <div></div>
		                        <div>SIGUIENTE MUEBLE</div>
		                    </a> 
		                </div> 
					</div>
				</c:otherwise>
			</c:choose>
        </div>
</c:if>
<c:if test="${listaC.tipoMueble == 0}">	
    	<div id="cajeros3" class="modal">
            <div class="cuadromed">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="tsModal">Servicios Cajeros</div>
	                <div class="sbModal"><b>Mobiliario 3 cajones</b></div><br>

	                <div class="cArticulo admodal">

		                <div class="w50 cmueble">
		                	<img src="${pageContext.request.contextPath}/img/mueble.png" class="mueble01">
		                </div>
		                
		                <div class="w50 cmueble">
		                	<div class="detProducto overflow">
		                		<div class="hed">
		                		    <b>Cajón 1</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaC.cajon1}</b>
			                	</div>
			                	<div class="hed">
		                		    <b>Cajón 2</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaC.cajon2}</b> 
			                	</div>

			                	<div class="hed">
		                		    <b>Cajón 3</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaC.cajon3}</b>
			                	</div>
		                	</div>
		                </div>
		                <div class="clear"></div>
	                </div>
				</div>
				<div class="clear"></div><br><br><br>
           </div>

            <c:choose>
    				<c:when test="${listaC.tipoMueble==0}">
						
    				</c:when>    
    			<c:otherwise>
    				<div class="bModalUser"> 
						<div class="tleft w50">
		    				<a href="#" class="btn  bm arrowml simplemodal-close cajeros2 ">
		    					<div></div>
		    					<div>MUEBLE ANTERIOR</div>
		    				</a> 
		                </div>
		
		                <div class="tright w50"> 
		                    <a href="#" class="btn bm arrowmr simplemodal-close cajeros2">
		                        <div></div>
		                        <div>SIGUIENTE MUEBLE</div>
		                    </a> 
		                </div> 
					</div>
				</c:otherwise>
			</c:choose>
        </div>
        </c:if>
      </c:forEach>
      
      
	<c:forEach var="listaG" items="${listaG}">
		<c:if test="${listaG.tipoMueble == 1}">	
    	<div id="gerentes2" class="modal">
            <div class="cuadromed">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="tsModal">Servicios Gerentes</div>
	                <div class="sbModal"><b>Mobiliario 2 cajones</b></div><br>

	                <div class="cArticulo admodal">

		                <div class="w50 cmueble">
		                	<img src="${pageContext.request.contextPath}/img/mueble2.png" class="mueble01">
		                </div>
		                
		                <div class="w50 cmueble">
		                	<div class="detProducto overflow">
		                		
		                		<div class="hed">
		                		    <b>Cajón 1</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>
			                		${listaG.cajon1}
									</b>
			                	</div>
			                	
			                	<div class="hed">
		                		    <b>Cajón 2</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaG.cajon2}</b> 
			                	</div>
		                	</div>
		                </div>
		                
		                <div class="clear"></div>
	                </div>
				</div>
				<div class="clear"></div><br><br><br>
           </div>

            <div class="bModalUser"> 
				<div class="tleft w50">
    				<a href="#" class="btn  bm arrowml simplemodal-close gerentes3">
    					<div></div>
    					<div>MUEBLE ANTERIOR</div> 
    				</a> 
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr simplemodal-close gerentes3">
                        <div></div>
                        <div>SIGUIENTE MUEBLE</div>
                    </a> 
                </div> 
			</div>
        </div>
</c:if>
<c:if test="${listaG.tipoMueble == 0}">	
    	<div id="gerentes3" class="modal">
            <div class="cuadromed">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="tsModal">Servicios Gerentes</div>
	                <div class="sbModal"><b>Mobiliario 3 cajones</b></div><br>

	                <div class="cArticulo admodal">

		                <div class="w50 cmueble">
		                	<img src="${pageContext.request.contextPath}/img/mueble.png" class="mueble01">
		                </div>
		                
		                <div class="w50 cmueble">
		                	<div class="detProducto overflow">
		                		<div class="hed">
		                		    <b>Cajón 1</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaG.cajon1}</b>
			                	</div>
			                	<div class="hed">
		                		    <b>Cajón 2</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaG.cajon2}</b> 
			                	</div>

			                	<div class="hed">
		                		    <b>Cajón 3</b>
		                	    </div>
			                	<div class="hcont">
			                		<b>${listaG.cajon3}</b>
			                	</div>
		                	</div>
		                </div>
		                <div class="clear"></div>
	                </div>
				</div>
				<div class="clear"></div><br><br><br>
           </div>

            <div class="bModalUser"> 
				<div class="tleft w50">
    				<a href="#" class="btn  bm arrowml simplemodal-close gerentes2">
    					<div></div>
    					<div>MUEBLE ANTERIOR</div>
    				</a> 
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr simplemodal-close gerentes2">
                        <div></div>
                        <div>SIGUIENTE MUEBLE</div>
                    </a> 
                </div> 
			</div>
        </div>
        </c:if>
      </c:forEach>
      
      

      
      
      
        <!---->
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    	<!--MODAL-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.Mfinancieros2').click(function (e) {
                        setTimeout(function() {
                            $('#financieros2').modal({
                            });
                            return false;
                        }, 1);
                    });
                    
                    $('.Mfinancieros3').click(function (e) {
                        setTimeout(function() {
                            $('#financieros3').modal({
                            });
                            return false;
                        }, 1);
                    });
                   
                    $('.gerentes2').click(function (e) {
                        setTimeout(function() {
                            $('#gerentes2').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.gerentes3').click(function (e) {
                        setTimeout(function() {
                            $('#gerentes3').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.cajeros2').click(function (e) {
                        setTimeout(function() {
                            $('#cajeros2').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.cajeros3').click(function (e) {
                        setTimeout(function() {
                            $('#cajeros3').modal({
                            });
                            return false;
                        }, 1);
                    });

                    $('.FolioDos').click(function (e) {
                        setTimeout(function() {
                            $('#folioDos').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            }); 
        </script>
    </body>
</html>



