<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="estatusGeneral.htm">Página Principal</a> /
			<a href="cifrasControl.htm">Cifras de Control</a> 
		</div>
    	Cifras de Control
    </div>

    <!-- Contenido -->
    <div class="contSecc">
    
    	<div class="titSec">Cifras de control</div>
    	<div class="gris">
    		<table class="tblGeneral">
			  <tbody>
				<tr>
				  <th>Selección</th>
				  <th>Nombre de archivo</th>
				  <th>Progreso de carga</th>
				  <th>Estatus del archivo</th>
				</tr>
				<tr>
				  <td><input type="checkbox" name="grupo1" id="check1" class="alone"><label for="check1">&nbsp;</label></td>
				  <td>Circular CNBV</td>
				  <td><div class="avance"><div class="barra"><div style="width: 70% !important"><div class="bgAma" ></div></div></div></div></td>
				  <td>En progreso</td>
				</tr>
				<tr>
				  <td><input type="checkbox" name="grupo1" id="check2" class="alone"><label for="check2">&nbsp;</label></td>
				  <td>Comunicado Banxico</td>
				  <td><div class="avance"><div class="barra"><div style="width: 90% !important"><div class="bgAma3" ></div></div></div></div></td>
				  <td>Error en la carga</td>
				</tr>
				<tr>
				  <td><input type="checkbox" name="grupo1" id="check3" class="alone"><label for="check3">&nbsp;</label></td>
				  <td>Poster UNE</td>
				  <td><div class="avance"><div class="barra"><div style="width: 100% !important"><div class="bgAma" ></div></div></div></div></td>
				  <td>Completado</td>
				</tr>
				<tr>
				  <td><input type="checkbox" name="grupo1" id="check4" class="alone"><label for="check4">&nbsp;</label></td>
				  <td>Video PLD</td>
				  <td><div class="avance"><div class="barra"><div style="width: 90% !important"><div class="bgAma3" ></div></div></div></div></td>
				  <td>Error en la carga</td>
				</tr>
			  </tbody>
			</table>
   			<div class="btnCenter"><a href="#" class="btnA">Reenviar</a></div>
    	</div>
    
	</div>	<!-- Fin conSecc -->
</body>
</html>