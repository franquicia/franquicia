<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Insert title here</title>
	</head>
	<body ng-app="app" ng-controller="validarForm">
		<script type="text/javascript">
			var contextPath = '${pageContext.request.contextPath}';
			var _containerT, _nivelT;
			var _containerP, _nivelP;
			var _containerC, _nivelC;
			var _cecos, _negocio, _boxId, _tabId;
			var _objBackup = '';
			var _objNameBackup = '';
			var _jsonReport = '';

			$(document).ready(function() {
				_negocio = 41; // Pruebas

				$('#btnTerritorioA').click(function(e) {
					_nivelT = 0;
					_cecos = '0';
					_boxId = 1;
					_tabId = 1;
					_objBackup = '';
					_objNameBackup = '';
					getTerritorioReporteria(1);
				});
				$('#btnPaisA').click(function(e) {
					_nivelP = 0;
					_cecos = '0';
					_boxId = 2;
					_tabId = 1;
					getGeografiaReporteria(1);
				});
				$('#btnCategoriaA').click(function(e) {
					_nivelC = 1;
					_cecos = '0';
					_boxId = 3;
					_tabId = 1;
					getCaategoriasReporteria(1);
				});

				$('#btnTerritorioB').click(function(e) {
					_nivelT = 0;
					_cecos = '0';
					_boxId = 1;
					_tabId = 2;
					_objBackup = '';
					_objNameBackup = '';
					getTerritorioReporteria(2);
				});
				$('#btnPaisB').click(function(e) {
					_nivelP = 0;
					_cecos = '0';
					_boxId = 2;
					_tabId = 2;
					getGeografiaReporteria(2);
				});
				$('#btnCategoriaB').click(function(e) {
					_nivelC = 1;
					_cecos = '0';
					_boxId = 3;
					_tabId = 2;
					getCaategoriasReporteria(2);
				});

				$.datepicker.regional['es'] = {
						closeText: 'Cerrar',
						prevText: '',
						nextText: ' ',
						currentText: 'Hoy',
						monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
						monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
						dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
						dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
						dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
						weekHeader: 'Sm',
						dateFormat: 'dd/mm/yy',
						firstDay: 1,
						isRTL: false,
						showMonthAfterYear: false,
						yearSuffix: ''
				};
				$.datepicker.setDefaults($.datepicker.regional['es']);
				$("#dpFechaVisibleA").datepicker({
					firstDay: 1,
					onSelect: function(date, datepicker) {
						// console.log('dpFechaVisibleA - onSelect');
					}
				});
				$("#dpFechaEnvioA").datepicker({
					firstDay: 1,
					onSelect: function(date, datepicker) {
						// console.log('dpFechaEnvioA - onSelect');
					}
				});
				$("#dpFechaVisibleB").datepicker({
					firstDay: 1,
					onSelect: function(date, datepicker) {
						// console.log('dpFechaVisibleB - onSelect');
					}
				});
				$("#dpFechaEnvioB").datepicker({
					firstDay: 1,
					onSelect: function(date, datepicker) {
						// console.log('dpFechaEnvioB - onSelect');
					}
				});

				$('#buscarPorSucursalA').on('keyup', function(e) {
					var _val = $('#buscarPorSucursalA').val();
					if (_val.length > 0) {
						$('#btnPaisA').off('click');
						$('#btnPaisA').removeClass('btnDistrib');
						$('#btnPaisA').addClass('btnDistribDis');
						$('#btnTerritorioA').off('click');
						$('#btnTerritorioA').removeClass('btnDistrib');
						$('#btnTerritorioA').addClass('btnDistribDis');
					} else if (_val.length < 1) {
						$('#btnPaisA').addClass('btnDistrib');
						$('#btnPaisA').removeClass('btnDistribDis');
						$('#btnPaisA').click(function(e) {
							_nivelP = 0;
							_cecos = '0';
							_boxId = 2;
							_tabId = 1;
							getGeografiaReporteria(1);
						});

						$('#btnTerritorioA').addClass('btnDistrib');
						$('#btnTerritorioA').removeClass('btnDistribDis');
						$('#btnTerritorioA').click(function(e) {
							_nivelT = 0;
							_cecos = '0';
							_boxId = 1;
							_tabId = 1;
							_objBackup = '';
							_objNameBackup = '';
							getTerritorioReporteria(1);
						});
					}
				});

				$('#buscarPorSucursalB').on('keyup', function(e) {
					var _val = $('#buscarPorSucursalB').val();
					if (_val.length > 0) {
						$('#btnPaisB').off('click');
						$('#btnPaisB').removeClass('btnDistrib');
						$('#btnPaisB').addClass('btnDistribDis');
						$('#btnTerritorioB').off('click');
						$('#btnTerritorioB').removeClass('btnDistrib');
						$('#btnTerritorioB').addClass('btnDistribDis');
					} else if (_val.length < 1) {
						$('#btnPaisB').addClass('btnDistrib');
						$('#btnPaisB').removeClass('btnDistribDis');
						$('#btnPaisB').click(function(e) {
							_nivelP = 0;
							_cecos = '0';
							_boxId = 2;
							_tabId = 1;
							getGeografiaReporteria(1);
						});

						$('#btnTerritorioB').addClass('btnDistrib');
						$('#btnTerritorioB').removeClass('btnDistribDis');
						$('#btnTerritorioB').click(function(e) {
							_nivelT = 0;
							_cecos = '0';
							_boxId = 1;
							_tabId = 1;
							_objBackup = '';
							_objNameBackup = '';
							getTerritorioReporteria(1);
						});
					}
				});
			});
		</script>

		<div class="clear"></div>
		<div class="titulo">
			<div class="ruta">
				<a href="estatusGeneral.htm">Página Principal</a> /
				<a href="reporteria.htm">Reportería</a> 
			</div>
			Reportería
		</div>

		<!-- Contenido -->
		<div class="contSecc">

			<!-- TABS -->
			<ul class="menuTabs2" style="margin-bottom: 10px;">
				<li ng-class="{ active: isSet(1) }" class="active">
					<a href="" ng-click="setTab(1)">Consulta general</a>
				</li>
				<li ng-class="{ active: isSet(2) }" class="">
					<a href="" ng-click="setTab(2)">Consulta detallada</a>
				</li>
			</ul>

			<!-- TAB CONTAINERS -->
			<div ng-show="isSet(1)" class="">
				<div class="titSec">Filtros</div>
				<div class="gris">
					<div class="divCol3">
						<div class="col3">
							<a href="#" id="btnTerritorioA" class="btnDistrib" style="width: 95%;">
								<img src="../../img/icoZonaW.svg">
								<br>Territorio
							</a>
						</div>
						<div class="col3">
							<a href="#" id="btnPaisA" class="btnDistrib" style="width: 95%;">
								<img src="../../img/icoPaisW.svg">
								<br>País
							</a>
						</div>
						<div class="col3">
							<a href="#" id="btnCategoriaA" class="btnDistrib" style="width: 95%;">
								<img src="../../img/icoCategoriaW.svg">
								<br>Categoría
							</a>
						</div>
					</div>
					<div class="divCol3">
						<div class="col3">
		    				<select id="visibleSucursalA" onchange="return false;">
		    					<option value="V">Vigente</option>
		    					<option value="N">No vigentes</option>
		    					<option value="T">Todos</option>
		    				</select>
						</div>
						<div class="col3">
							<input type="text" placeholder="Fecha de envío" class="datapicker1 date" id="dpFechaEnvioA" autocomplete="off">
						</div>
						<div class="col3">
							<input type="text" placeholder="Fecha visible en sucursal" class="datapicker1 date" id="dpFechaVisibleA" autocomplete="off">
						</div>
					</div>
					<div class="divCol2">
						<div class="col2">
							Buscar por Sucursal<br>
							<form id="porSucursalA" name="porSucursal" action="" method="get" onsubmit="return false;">
								<div id="psA" class="pRelative">
									<input type="text" id="buscarPorSucursalA" autocomplete="off" placeholder="Número de Sucursal" oninput="return false;">
								</div>
							</form>
						</div>
					</div>
	
					<p style="margin-bottom: 2px; margin-left: 15px; color: #006341;">
						<strong>Filtros por aplicar</strong>
					</p>
					<div id="listaLanzamientosA" class="divCol3 divListLanzamientos" style="height: 65px; margin-left: 15px; margin-right: 10px; min-height: 105px;">
						<%-- lista de objetos para realizar búsqueda de folio --%>
					</div>
	
					<div class="btnCenter">
						<a href="#" id="btnNuevaBusquedaA" class="btnA btnG" style="margin: 0px 10px 0 px 0px;" onclick="reporteriaNuevaBusq(1);">Nueva búsqueda</a>
						<a href="#" id="btnBuscarA" class="btnA" onclick="validateFilterReport(1);">Buscar</a>
					</div>
	
					<div class="titSec">Resultados de la búsqueda</div>
					<div class="gris">
						<div class="scroll">
							<table id="tblReporteriaA" class="tblGeneral" style="display: block; width: 100%;">
								<thead style="display: block; width: 100%;">
									<tr style="display: inline-table; width: 100%;">
										<th style="width: 30%;">Nombre del documento</th>
										<th style="width: 20%;">Categoría</th>
										<th style="width: 10%;">Vigencia</th>
										<th style="width: 10%;">Estatus</th>
										<th style="width: 15%;">Fecha envío</th>
										<th style="width: 15%;">Fecha visualización</th>
									</tr>
								</thead>
								<tbody id="tblBody" style="display: block; width: 100%; max-height: 300px; overflow-y: scroll;"></tbody>
							</table>
						</div>
		
						<div class="btnCenter">
							<div><a href="#" class="btnA btnB" onclick="exportReporteGeneral();">Exportar</a></div>
						</div>
					</div>
				</div>
			</div>
			<div ng-show="isSet(2)" class="ng-hide">
				<div class="titSec">Filtros</div>
				<div class="gris">
					<div class="divCol3">
						<div class="col3">
							<a href="#" id="btnTerritorioB" class="btnDistrib" style="width: 95%;">
								<img src="../../img/icoZonaW.svg">
								<br>Territorio
							</a>
						</div>
						<div class="col3">
							<a href="#" id="btnPaisB" class="btnDistrib" style="width: 95%;">
								<img src="../../img/icoPaisW.svg">
								<br>País
							</a>
						</div>
						<div class="col3">
							<a href="#" id="btnCategoriaB" class="btnDistrib" style="width: 95%;">
								<img src="../../img/icoCategoriaW.svg">
								<br>Categoría
							</a>
						</div>
					</div>
					<div class="divCol4">
						<div class="col4">
							<select id="tipoEnvioB" onchange="validaTipoDeEnvio(this);">
								<option value="I">Instantáneo</option>
								<option value="B">Batch</option>
								<option value="C">Calendarizado</option>
							</select>
						</div>
						<div class="col4">
		    				<select id="visibleSucursalB" onchange="">
		    					<option value="V">Visibles en sucursal</option>
								<option value="P">Pendientes de descarga</option>
								<option value="T">Todos</option>
		    				</select>
						</div>
						<div class="col4">
							<input type="text" placeholder="Fecha de envío" class="datapicker1 date" id="dpFechaEnvioB" autocomplete="off">
						</div>
						<div class="col4">
							<input type="text" placeholder="Fecha visible en sucursal" class="datapicker1 date" id="dpFechaVisibleB" autocomplete="off">
						</div>
					</div>
					<div class="divCol2">
						<div class="col2">
							Buscar por Sucursal<br>
							<form id="porSucursal" name="porSucursalB" action="" method="get" onsubmit="return false;">
								<div id="psB" class="pRelative">
									<input type="text" id="buscarPorSucursalB" autocomplete="off" placeholder="Número de Sucursal" oninput="return false;">
								</div>
							</form>
						</div>
						<div class="col2">
							Buscar por Folio<br>
							<form id="porFolio" name="porFolioB" action="" method="get" onsubmit="return false;">
								<div id="pfB" class="pRelative">
									<input type="text" id="buscarPorFolioB" autocomplete="off" placeholder="Número de Folio" oninput="return false;">
								</div>
							</form>
						</div>
					</div>
	
					<p style="margin-bottom: 2px; margin-left: 15px; color: #006341;">
						<strong>Filtros por aplicar</strong>
					</p>
					<div id="listaLanzamientosB" class="divCol3 divListLanzamientos" style="height: 65px; margin-left: 15px; margin-right: 10px; min-height: 105px;">
						<%-- lista de objetos para realizar búsqueda de folio --%>
					</div>
	
					<div class="btnCenter">
						<a href="#" id="btnNuevaBusquedaB" class="btnA btnG" style="margin: 0px 10px 0 px 0px;" onclick="reporteriaNuevaBusq(2);">Nueva búsqueda</a>
						<a href="#" id="btnBuscarB" class="btnA" onclick="validateFilterReport(2);">Buscar</a>
					</div>

					<div class="titSec">Resultados de la búsqueda</div>
					<div class="gris">
						<div class="scroll">
							<table id="tblReporteriaB" class="tblGeneral" style="display: block; width: 100%;"></table>
						</div>
		
						<div class="btnCenter">
							<div><a href="#" class="btnA btnB" onclick="exportReporteDetallado();">Exportar</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- MODALES -->
		<div id="modalReport" class="modal">
			<div class="cuadro cuadroG">
				<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
				<div id="reportTitle" class="titModal">ListName</div>
				<br>
				<div id="reportList" class="contModal" style="text-align: left; overflow-y: scroll; height: 290px; max-height: 360px;">
					<!-- lista de cecos generada por codigo -->
				</div>
				<br>
				<div class="btnModalList">
					<a href="#" id="btnListOk" class="btnA" style="margin-right: 10px;" onclick="createFilterBoxReporteria();">Aceptar</a>
					<a href="#" id="btnListSigNivel" class="btnA" style="margin-left: 10px;" onclick="nextLevelReports();">Siguiente nivel</a>
				</div>
			</div>
		</div>

		<!-- MODAL DESCARGA -->
		<div class="toastDescarga">
			<div class="progresoDescarga">
				<div class="grafica">
					<ul>
						<li class="circle-gauge">
							<a href="#" style="-gauge-value: 0;"></a>
						</li>
					</ul>
				</div>
				<div class="tiempo" id="countdown"></div>
				<div class="tamano" id="tamanoArchivo"></div>
			</div>
			<div class="cerrar">
				<img src="../../img/cerrar.svg" alt="">
			</div>
		</div>

		<!-- LOADER -->
		<div class="loaderContainer" style="display: none;">
			<div class="ml-loader loader" style="">
				<div></div><div></div><div></div><div></div>
				<div></div><div></div><div></div><div></div>
				<div></div><div></div><div></div><div></div>
			</div>
		</div>
		<div class="blackScreen" style="display: none;"></div>
	</body>
</html>