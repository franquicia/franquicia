<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="clear"></div>

    <div class="titulo">
    	<div class="ruta">
			<a href="estatusGeneral.htm">Página Principal</a> /
			<a href="historialArchivos.htm">Historial de archivos</a> 
		</div>
    	Historial de archivos
    </div>

    <!-- Contenido -->
    <div class="contSecc">
    	<div class="titSec">Filtros</div>
    	<div class="gris">
    		<div class="divCol4">
    			<div class="col4">Zona<br>
    				<select>
    					<option>Selecciona una opción</option>
    					<option>Opción 1</option>
    				</select>
    			</div>
    			<div class="col4">Región<br>
    				<select>
    					<option>Selecciona una opción</option>
    					<option>Opción 1</option>
    				</select>
    			</div>
    			<div class="col4">Sucursal<br>
    				<select>
    					<option>Selecciona una opción</option>
    					<option>Opción 1</option>
    				</select>
    			</div>
    			<div class="col4">Fecha de envío<br>
    				<input type="text" placeholder="DD/MM/AAAA" class="datapicker1 date" id="datepicker" autocomplete="off"> 
    			</div>
			</div>
			<div class="btnCenter"><a href="#" class="btnA">Filtrar</a></div>
		</div>
   		<div class="clear">&nbsp;</div><br>
   		
    	<div class="titSec">Lista de archivos cargados</div>
    	<div class="gris">
    		<table class="tblGeneral tblHistorial">
			  <tbody>
				<tr>
				  <th>Selección</th>
				  <th>Zona</th>
				  <th>Región</th>
				  <th>Sucursal</th>
				  <th>Nombre de archivo</th>
				  <th>Fecha/Hora de envío</th>
				  <th colspan="2">Acciones</th>
				</tr>
				<tr>
				  <td><input type="checkbox" name="grupo1" id="check1" class="alone"><label for="check1">&nbsp;</label></td>
				  <td>Metro</td>
				  <td>Bajío Norte</td>
				  <td>Aztecas</td>
				  <td>Circular CNBV</td>
				  <td>30/12/2017 - 15:30 hrs</td>
				  <td><a href="#" class="modaPDFPrev_view"><img src="../../img/icoVer.svg"></a></td>
				   <td><a href="#"><img src="../../img/delete.svg" class="eliminarAccion"></a></td>
				</tr>
				<tr>
				  <td><input type="checkbox" name="grupo1" id="check2" class="alone"><label for="check2">&nbsp;</label></td>
				  <td>Metro</td>
				  <td>Bajío Norte</td>
				  <td>Aztecas</td>
				  <td>Comunicado Banxico</td>
				  <td>11/12/2017 - 13:00 hrs</td>
				  <td><a href="#"><img src="../../img/icoVer.svg"></a></td>
				   <td><a href="#"><img src="../../img/delete.svg" class="eliminarAccion"></a></td>
				</tr>
				<tr>
				  <td><input type="checkbox" name="grupo1" id="check3" class="alone"><label for="check3">&nbsp;</label></td>
				  <td>Metro</td>
				  <td>Bajío Norte</td>
				  <td>Aztecas</td>
				  <td>Poster UNE</td>
				  <td>13/12/2017 - 10:00 hrs</td>
				  <td><a href="#"><img src="../../img/icoVer.svg"></a></td>
				   <td><a href="#"><img src="../../img/delete.svg" class="eliminarAccion"></a></td>
				</tr>
				<tr>
				  <td><input type="checkbox" name="grupo1" id="check4" class="alone"><label for="check4">&nbsp;</label></td>
				  <td>Metro</td>
				  <td>Bajío Norte</td>
				  <td>Aztecas</td>
				  <td>Video PLD</td>
				  <td>22/11/2017 - 11:30 hrs</td>
				  <td><a href="" class="modaPDFPrev_view"><img src="../../img/icoVer.svg"></a></td>
				  <td><a href="#"><img src="../../img/delete.svg" class="eliminarAccion"></a></td>
				</tr>
			  </tbody>
			</table>
 			
   			<div class="btnCenter">
   				<a href="#" class="btnA btnG modalEliminar_view">Eliminar</a>
   				<a href="#" class="btnA">Cancelar</a>
   			</div>
    	</div>
    
	</div>	<!-- Fin conSecc -->
	
	<!-- Modal -->
<div id="modalEliminar" class="modal">
	<div class="cuadro cuadroM">
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
		<div class="titModal">Eliminar archivos</div><br>
		<div class="contModal ">
			<div class="tLeft">Confirme que sean correctos los archivos que desea eliminar.</div><br>

			<div class="divCol2 ">
				<div class="col2 divEliminar"><a  href="#" class="btnCerrar1" ></a>Circular CNBV</div>
				<div class="col2 divEliminar"><a  href="#" class="btnCerrar1" ></a>Circular CNBV</div>
			</div>
			<div class="tLeft"><input type="checkbox" name="grupo3" id="checkBorrar" class=""><label for="checkBorrar">Archivos y distribución correctos</label></div>
		</div><br>
		<div class="btnCenter">
			<a href="#" class="btnA btnG simplemodal-close">Cancelar</a>
			<a href="#" class="btnA simplemodal-close">Enviar</a>
		</div>
	</div>
	<div class="clear"></div><br>
</div>

<div id="modaPDFPrev" class="modal">
	<div class="cuadroPrueba cuadroM">
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
		<div class="titModal">Previsualización</div><br>

		<iframe src="../../pdf/pdf.pdf" width="90%" height="450px"></iframe>

		<div class="btnCenter">
			<a href="#" class="btnA btnG simplemodal-close">Cancelar</a>
			<a href="#" class="btnA simplemodal-close">Aceptar</a>
		</div>
	</div>
	<div class="clear"></div><br>
</div>
</body>
</html>