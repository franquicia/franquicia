<head>
<link rel="stylesheet" type="text/css" href="css/buscador/main.css" />
<link rel="stylesheet" type="text/css" href="css/header-style.css" />
<script type="text/javascript">
	var urlServer = '${urlServer}';
</script>
</head>
<body id="buscador" onload="init()">
	<h1 id="tituloBuscador">
		PARA <strong id="fuerte">SABER</strong> M&Aacute;S <img
			src="images/buscador/simBuscador.png" alt="img2" width="50"
			height="50">
	</h1>

	<div id="containerBuscador">
		<div id="containerDataBuscador">
			<input type="text" id="w-input-search" />
		</div>
		<div id="containerButtonBuscador">
			<a id="button-id" type="button" href="javascript:muestraSeleccion();"><img
				id="buttonImage" src="images/buscador/lupaBuscador.png" alt="img1"></a>
		</div>
	</div>
	<br>
	<br>
	<div id="containerResultadosBusqueda"></div>
	<h2 id="tituloBuscador">B&uacute;squedas Frecuentes</h2>
	<div id="busquedasFrecuentes">
		<div id="frecuente1">${buscado1.tagName}<br>
			${buscado1.descripcion}
		</div>
		<br>
		<br>
		<div id="frecuente2">${buscado2.tagName}<br>
			${buscado2.descripcion}
		</div>
	</div>
	</br>
	<div id="linkSalirCenterContainer">
		<a id="buttonLink" href="mensajes.htm" title="pdf">Inicio</a>
	</div>
</body>


