<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<head>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	
	<script type="text/javascript" src="../js/script-menu.js"></script> 
	<script type="text/javascript" src="../js/bootstrap.min.js"></script> 
	<script type="text/javascript" src="../js/jquery/jquery-2.2.4.min.js"></script>
	
	
	
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" type="text/css" href="../css/header-style.css">
	<link rel="stylesheet" type="text/css" href="../css/menuCheck.css">
	<link rel="stylesheet" type="text/css" href="../css/carousel.css">
	
	<link rel="stylesheet" media="screen" href="../css/vistaCumplimientoVisitas/modal.css" />
	
	<script src="../js/script-storeIndex.js"></script>    
	<link rel="stylesheet" type="text/css" href="../css/css-storeIndex.css"></link>
	
	<title>Acta de Entrega de Archiveros</title>
	
	<style type="text/css">
		div.apps{ border: 2px solid #F0F0F0;
				 }
	</style>
	
	<% response.addHeader("X-Frame-Options", "SAMEORIGIN"); %>
	<script type="text/javascript">
			var cont=1;
			function agregarFila(){
				var placa=document.getElementById("placa").value;
				var marca=document.getElementById("marca").value;
				var descripcion=document.getElementById("descripcion").value;
				var observaciones=document.getElementById("observaciones").value;
				var flag1=true;
				if(placa.length == 0){
					flag1=false;
				}
				var flag2=true;
				if(marca.length == 0){
					flag2=false;
				}
				var flag3=true;
				if(descripcion.length == 0){
					flag3=false;
				}
				var flag4=true;
				if(observaciones.length == 0){
					flag4=false;
				}
					
				if(flag1==true && flag2==true &&flag3==true && flag4==true){
					if(placa.length > 30){
						alert('PAso');
						flag1=false;
					}
					if(marca.length > 40){
						flag2=false;
					}
					if(descripcion.length > 300){
						flag3=false;
					}
					if(observaciones.length > 200){
						flag4=false;
					}
					if(flag1==true && flag2==true &&flag3==true && flag4==true){
						document.getElementById("tablaprueba").insertRow(-1).innerHTML = '<th> <INPUT TYPE="radio" id="command" name="command" value="'+cont+'"/> <p style="text-align:center; border: 3px solid white; font-size: 14px;">'+cont+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px; ">'+placa+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+marca+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+descripcion+'</p></th><th colspan="2" ><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+observaciones+'</p></th>';
						//document.getElementById("tablaprueba").insertRow(-1).innerHTML = '<th> <p style="text-align:center; border: 3px solid white; font-size: 14px;">'+cont+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px; ">'+placa+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+marca+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+descripcion+'</p></th><th colspan="2" ><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+observaciones+'</p></th>';
						if(cont==1){
							document.getElementById("PlacaBD").value=placa;
							document.getElementById("MarcaBD").value=marca;
							document.getElementById("DescripcionBD").value=descripcion;
							document.getElementById("ObservacionesBD").value=observaciones;
						}
						else{
							document.getElementById("PlacaBD").value=document.getElementById("PlacaBD").value+'&'+placa;
							document.getElementById("MarcaBD").value=document.getElementById("MarcaBD").value+'&'+marca;
							document.getElementById("DescripcionBD").value=document.getElementById("DescripcionBD").value+'&'+descripcion;
							document.getElementById("ObservacionesBD").value=document.getElementById("ObservacionesBD").value+'&'+observaciones;
						}
						document.getElementById("placa").value='';
						document.getElementById("marca").value='';
						document.getElementById("descripcion").value='';
						document.getElementById("observaciones").value='';
						cont++;
					}
					else{
						var mensaje='El campo ';
						if(!flag1)
							mensaje=mensaje+'placa no puede superar los 30 caracteres, ';
						if(!flag2)
							mensaje=mensaje+'marca no puede superar los 40 caracteres, ';
						if(!flag3)
							mensaje=mensaje+'descripcion no puede superar los 300 caracteres, ';
						if(!flag4)
							mensaje=mensaje+'observaciones no puede superar los 200 caracteres, ';
						var numcad=mensaje.length-2;
						
						mensaje=mensaje.substr(0,numcad)+'.';
						alert(mensaje);
					}
				}else{
					var mensaje='El campo ';
					if(!flag1)
						mensaje=mensaje+'placa, ';
					if(!flag2)
						mensaje=mensaje+'marca, ';
					if(!flag3)
						mensaje=mensaje+'descripcion, ';
					if(!flag4)
						mensaje=mensaje+'observaciones, ';
					var numcad=mensaje.length-2;
					
					mensaje=mensaje.substr(0,numcad)+' no puede ser vacio.';
					alert(mensaje);
				}
			}

			
	
				function editarArchivero(){
				  var table = document.getElementById("tablaprueba");
				  var rowCount = table.rows.length;
				  //console.log(rowCount);
				  
				  if(rowCount <6)
				    alert('Aún no agrega un activo');
				  else{
					  
						var resultado='0';
						  
					    var porNombre=document.getElementsByName("command");
	
					    for(var i=0;i<porNombre.length;i++)
					    {
					          if(porNombre[i].checked)
					                resultado=porNombre[i].value;
					    }
					    if(resultado=='0')
					    	alert("No ha seleccionado ningún activo");
					    else{
					    	
							//alert("Paso "+resultado);
							var pos = parseInt(resultado, 10);
							//alert("Paso 2 "+pos);
							
							var placa=document.getElementById("PlacaBD").value;
							var marca=document.getElementById("MarcaBD").value;
							var descripcion=document.getElementById("DescripcionBD").value;
							var observaciones=document.getElementById("ObservacionesBD").value;

							//alert("Paso 2 "+placa);
							
							var arrPlaca=placa.split('&');
							var arrMarca=marca.split('&');
							var arrDescripcion=descripcion.split('&');
							var arrObservaciones=observaciones.split('&');

							document.getElementById("placa").value=arrPlaca[pos-1];
							document.getElementById("marca").value=arrMarca[pos-1];
							document.getElementById("descripcion").value=arrDescripcion[pos-1];
							document.getElementById("observaciones").value=arrObservaciones[pos-1];

							//alert("Placa:  "+arrPlaca[pos-1]+",Marca: "+arrMarca[pos-1]);

							document.getElementById("PlacaBD").value='';
							document.getElementById("MarcaBD").value='';
							document.getElementById("DescripcionBD").value='';
							document.getElementById("ObservacionesBD").value='';
							
							var contador=cont;
							cont=1;
							var posExclus=pos-1;
							for (var i = 0; i < contador-1; i++){
						    	delFila();
						    	//alert("Paso 3: "+cont);
						    }
						    for (var i = 0; i < contador-1; i++){
						    	if(i!=posExclus){
						    		editarFilas(arrPlaca[i],arrMarca[i],arrDescripcion[i],arrObservaciones[i]);
								}
						    }
						    
							//cont=0;
					    }
				  }
				}
				function delFila(){

					var table = document.getElementById("tablaprueba");
					  var rowCount = table.rows.length;
					  //console.log(rowCount);
					  
					  if(rowCount <6)
					    alert('Aún no agrega un activo');
					  else{
						  table.deleteRow(rowCount -1);

						}
				}

				function editarFilas(placa,marca,descripcion,observaciones){
							document.getElementById("tablaprueba").insertRow(-1).innerHTML = '<th> <INPUT TYPE="radio" id="command" name="command" value="'+cont+'"/> <p style="text-align:center; border: 3px solid white; font-size: 14px;">'+cont+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px; ">'+placa+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+marca+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+descripcion+'</p></th><th colspan="2" ><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+observaciones+'</p></th>';
							//document.getElementById("tablaprueba").insertRow(-1).innerHTML = '<th> <p style="text-align:center; border: 3px solid white; font-size: 14px;">'+cont+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px; ">'+placa+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+marca+'</p></th><th><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+descripcion+'</p></th><th colspan="2" ><p style="text-align:center; border: 3px solid white; font-size: 14px;">'+observaciones+'</p></th>';
							if(cont==1){
								document.getElementById("PlacaBD").value=placa;
								document.getElementById("MarcaBD").value=marca;
								document.getElementById("DescripcionBD").value=descripcion;
								document.getElementById("ObservacionesBD").value=observaciones;
							}
							else{
								document.getElementById("PlacaBD").value=document.getElementById("PlacaBD").value+'&'+placa;
								document.getElementById("MarcaBD").value=document.getElementById("MarcaBD").value+'&'+marca;
								document.getElementById("DescripcionBD").value=document.getElementById("DescripcionBD").value+'&'+descripcion;
								document.getElementById("ObservacionesBD").value=document.getElementById("ObservacionesBD").value+'&'+observaciones;
							}
							cont++;
				}

				function eliminarArchivero(){
					  var table = document.getElementById("tablaprueba");
					  var rowCount = table.rows.length;
					  //console.log(rowCount);
					  
					  if(rowCount <6)
					    alert('Aún no agrega un activo');
					  else{
						  
							var resultado='0';
							  
						    var porNombre=document.getElementsByName("command");
		
						    for(var i=0;i<porNombre.length;i++)
						    {
						          if(porNombre[i].checked)
						                resultado=porNombre[i].value;
						    }
						    if(resultado=='0')
						    	alert("No ha seleccionado ningún activo");
						    else{
						    	
								//alert("Paso "+resultado);
								var pos = parseInt(resultado, 10);
								//alert("Paso 2 "+pos);
								
								var placa=document.getElementById("PlacaBD").value;
								var marca=document.getElementById("MarcaBD").value;
								var descripcion=document.getElementById("DescripcionBD").value;
								var observaciones=document.getElementById("ObservacionesBD").value;

								//alert("Paso 2 "+placa);
								
								var arrPlaca=placa.split('&');
								var arrMarca=marca.split('&');
								var arrDescripcion=descripcion.split('&');
								var arrObservaciones=observaciones.split('&');
								
								//alert("Placa:  "+arrPlaca[pos-1]+",Marca: "+arrMarca[pos-1]);

								document.getElementById("PlacaBD").value='';
								document.getElementById("MarcaBD").value='';
								document.getElementById("DescripcionBD").value='';
								document.getElementById("ObservacionesBD").value='';
								
								var contador=cont;
								cont=1;
								var posExclus=pos-1;
								for (var i = 0; i < contador-1; i++){
							    	delFila();
							    	//alert("Paso 3: "+cont);
							    }
							    for (var i = 0; i < contador-1; i++){
							    	if(i!=posExclus){
							    		editarFilas(arrPlaca[i],arrMarca[i],arrDescripcion[i],arrObservaciones[i]);
									}
							    }
							    
								//cont=0;
						    }
					  }
					}
				
				function validaEntrega(){
					banderaUsuario = false;
					bandera = true;
					
					document.getElementById('error').innerHTML = '';
					document.getElementById("usuario").value=document.getElementById("usuario").value.trim();
					if (document.getElementById("usuario").value == null
							|| document.getElementById("usuario").valuelength == 0
							|| /^\s*$/.test(document.getElementById("usuario").value)) {
						document.getElementById('error').innerHTML = 'El campo Usuario debe tener un valor';
						bandera = false;
					} else if (document.getElementById("llave").value == null
							|| document.getElementById("llave").valuelength == 0
							|| /^\s*$/.test(document.getElementById("llave").value)) {
						document.getElementById('error').innerHTML = 'El campo TOKEN debe tener un valor';
						bandera = false;
					} else{
						
							if (!/^([0-9])*$/.test(document.getElementById("usuario").value)){
								alert("El valor del campo usuario debe ser numerico");
						    	document.getElementById("usuario").value = '';
						    	
							}
							else{
								
								if(document.getElementById("usuario").value>=9999999){
									alert("El campo \"usuario\" debe contener el numero de empleado del gerente");
									document.getElementById("usuario").value = '';
								}
								else{
									carga2();
									validaTokenX(document.getElementById("usuario").value , document.getElementById("llave").value);
								}
							}
							
							
						
						
					}
					 return false;
				}

				function validaFirmasArch(){
					var flags= new Array(2);
					
					if (document.getElementById("token1").value == null
							|| document.getElementById("token1").valuelength == 0
							|| /^\s*$/.test(document.getElementById("token1").value)) {
						flags[0]=false;
						document.getElementById('error').innerHTML = 'Usuario no logueado';
					}
					var flagA=true;
					for (i = 0; i < flags.length; i++) {
					    if(flags[i]==false){
					    	flagA=false;
					    }
					}
					
					if(flagA==false){
				    	alert("Debes firmar el documento!!!");
				    	return false;
				    }
				    else{
				    	carga2();
				    	carga();
				    	return true;
				    }
				}

				
				function carga() {
					 setTimeout("document.getElementById('cargando').style.display= 'none'", 3000);
					}
				
				function carga2() {
					 document.getElementById('cargando').style.display= 'Block';
					}

				var doc = new jsPDF();
				var specialElementHandlers = {
				    '#editor': function (element, renderer) {
				        return true;
				    }
				};

				$('#cmd').click(function () {
				    doc.fromHTML($('#content').html(), 15, 15, {
				        'width': 170,
				            'elementHandlers': specialElementHandlers
				    });
				    doc.save('sample-file.pdf');
				});				
	</script>
</head>

<body >

	<tiles:insertTemplate template="/WEB-INF/templates/templateHeader.jsp" flush="true">
		<tiles:putAttribute name="cabecera" value="/WEB-INF/templates/templateHeader.jsp" />
	</tiles:insertTemplate>
	
	<tiles:insertTemplate template="/WEB-INF/views/menu.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/views/menu.jsp" />
	</tiles:insertTemplate>
	
	<!-- ********************************** Body************************************************ -->
	
	
	<!-- ********************************** Paso 1 llenar el formulario************************************************ -->
	<c:choose>
	<c:when test="${paso=='no'}">
		<c:url value = "/central/altaArchiveros.htm" var = "archivoPasivo" />
				<form:form method="POST" action="${archivoPasivo}" model="command" name="form1" id="form1">
					<div id="Imprimir">
						<br><br>
						<p align="center">
							<b>ACTA DE ENTREGA DE ARCHIVEROS</b>
						<br>
						<br>
						</p>
						
						<table id="tablaprueba" style="margin-right: 20px; margin-left: 20px; " width="95%" border="2" bordercolor="#000000">
									<tr>
										<th colspan="6">
											<p style="margin-bottom: 15px; margin-top: 15px">
											<img id="codigo-barras" width="100px" src="../images/codigo-barras.png" align="center" style="margin-right: 25px"/>
											<img id="logo" width="100px" src="../images/logo-baz.png" align="center" style="margin-left: 25px"/>
											</p>
										</th>								
									</tr>
									
									<tr>
										<th colspan="3" style="background-color: rgb(255, 255, 153);">
											<p style="text-align:center;margin-left: 30px;border: 5px solid rgb(255, 255, 153);font-size: 20px">
												FORMATO ACTA DE ENTREGA ACTIVO FIJO
											</p>
										</th>
										<th colspan="3">
										</th>
									</tr>
									<tr>
										<th colspan="6" style="background-color: rgb(234, 241, 221);">
										<p style="text-align:left;margin-left: 5px;border: 8px solid rgb(234, 241, 221);font-size: 18px">
												Nombre de Socio: ${nombre}
												<br>									
												N&uacute;mero de Socio: ${numempleado} 
												<br>											
												Centro de costos: ${sucursal}
												<br>										
												Nombre de Sucursal: ${nomsucursal}
												<br>
										</p>
										<input id="nomSucursal" name="nomSucursal" type="hidden" value="${nomsucursal}"/>
										<input id="numGerente" name="numGerente" type="hidden" value="${numempleado}"/>
										<input id="nomGerente" name="nomGerente" type="hidden" value="${nombre}"/>
										<input id="idSucursal" name="idSucursal" type="hidden" value="${sucursal}"/>						
										</th>
									</tr>
									<tr>
										<th>
										<p style="text-align:center;border: 5px solid white;font-size: 18px">
											NO.
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 8px;margin-right: 8px;border: 5px solid white;font-size: 18px">
											Placa
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 10px;margin-right: 10px;border: 5px solid white;font-size: 18px">
											Marca
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 30px;margin-right: 30px;border: 5px solid white;font-size: 18px">
											Descripci&oacute;n
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 30px;margin-right: 30px;border: 5px solid white;font-size: 18px">
											Observaciones
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 30px;margin-right: 30px;border: 5px solid white;font-size: 18px">
											Observaciones
										</p>
										</th>
										<th>
										
										</th>
									</tr>

									<tr>
										<th>
										<p style="text-align:center; border: 3px solid white; font-size: 18px">
											
										</p>
										</th>
										<th>
										<p style="text-align:justify;border: 3px solid white;">
											<input type="text" maxlength="30" size="11" class="form-control" id="placa" name="placa" placeholder="Placa" style="font-size: 12px;"/>
										</p>
										</th>
										<th>
										<p style="text-align:justify;border: 3px solid white;">
											<input type="text" maxlength="40" size="11" class="form-control" id="marca" name="marca" placeholder="Marca" style="font-size: 12px;"/>
										</p>
										</th>
										<th>
										<p style="text-align:justify;border: 3px solid white;">
											<textarea maxlength="300" rows="4" cols="30" class="form-control" id="descripcion" name="descripcion" placeholder="Descripción" style="font-size: 11px;"/></textarea>
										</p>
										</th>
										<th>
										<p style="text-align:justify; border: 3px solid white;">
											<textarea  maxlength="200" rows="4" cols="20" class="form-control" id="observaciones" name="observaciones" placeholder="Observaciones" style="font-size: 11px;"/></textarea>
										</p>
										</th>
										<th>
										<p style="text-align:justify; border: 3px solid white;">
											<input type="button" value="Agrega Archivero" onclick="agregarFila()"/>
											<br/>
											<input type="button" value="Editar Archivero" onclick="editarArchivero()"/>
											<br/>
											<input type="button" value="Eliminar Archivero" onclick="eliminarArchivero()"/>
										</p>
										</th>
										
									</tr>
								</table>
									
							<table id="tablaprueba" style="margin-right: 20px; margin-left: 20px; " width="95%" border="2" bordercolor="#000000">
									<tr>
										<th colspan="6">
										<p style="text-align:center;border: 8px solid white; font-size: 18px">
										Esta acta de entrega, sirve como soporte por la recolecci&oacute;n de los archiveros que se encuentran en desuso dentro de la sucursal.
										<br>
										La descarga de los activos dentro del sistema se efectuara una vez que el proveedor responsable de esteservicio finalice el proceso.
										</p>
										</th>
									</tr>
									<tr>
										<th colspan="3">
										<br><br><br><br><br>
										<p style="text-align:center; border: 8px solid white;">
										_____________________________________________
										<br/>
										${nombre}<br/>
										Socio que entrega los activos fijos
										</p>
										</th>
										<th colspan="3">
										<br><br><br><br><br>
										<p style="text-align:center; border: 8px solid white;">
										_____________________________________________
										<br/>
										<input type="text" maxlength="100" class="form-control" id="nombreUsr" name="nombreUsr" placeholder="Nombre Usuario" style="font-size: 15px; text-align: center;" onChange="validarSiLetra(this.value);"/>
										<!-- <input type="text" maxlength="10" class="form-control" id="numeroUsr" name="numeroUsr" placeholder="Número Usuario" style="font-size: 15px; text-align: center;" onChange="validarSiNumero5(this.value);"/> -->
										<br/>
										Persona que recibe los activos fijos
										</p>
										</th>
									</tr>
									<tr>
										<th colspan="6">
										<p style="text-align:left; border: 8px solid white; margin: 5px; font-size: 18px">
										Nota: Si el activo no cuenta con n&uacute;mero de placa favor de colocar S/P, si el activo no cuenta con marca favor de colocar S/M.
										</p>
										</th>
									</tr>
								</table>
					</div>	
		<div style="border-radius: 20px; margin: 20px;" class="apps">		
		<table style="text-align:center; border-collapse: separate;" >
				
			<tr>
				<td>
					<div class="inputData">
								<table style="text-align:center; border-collapse: separate;">
								<tr>
								<th><br>
								<br>Entrega
								<br>(Gerente de sucursal)<br><br>
								</th>
								</tr>
								
									<tr>
										<td>Usuario:<br>
											<input  id="usuario" name="usuario" maxlength="10" style="height: 30px; width: 150px; text-align: center; 	font-size: 20px;
											"/></td>
									</tr>
									
									<tr>
										<td>Token:<br>
											<input  id="llave" name="llave" maxlength="10" type="password" style="height: 30px; width: 150px; text-align: center; 	font-size: 20px;"/></td>
									</tr>
									
									<tr>
									<br><br>
										<td colspan="2">
										<input id="botonEntrega" style="height: 30px; width: 150px; text-align: center; font-size: 20px;" type="button" value="Aceptar"  onClick="return validaEntrega();" /></td>
									</tr>
								</table>
							<br>
								<div class="error" id="error">
								      ${response}
								</div>
								<input id="usr" name="usr" type="hidden" value=""/>
								<input id="token1" name="token1" type="hidden" value=""/>
					</div>		
				</td>
				
			</tr>
						
		</table>
		</div>
		<input id="PlacaBD" name="PlacaBD" type="hidden" value=""/>
		<input id="MarcaBD" name="MarcaBD" type="hidden" value=""/>
		<input id="DescripcionBD" name="DescripcionBD" type="hidden" value=""/>
		<input id="ObservacionesBD" name="ObservacionesBD" type="hidden" value=""/>
		<input id="tok" name="tok" type="hidden" value="${tok}"/>
		
		<input type="submit" id="Gardar" name="Agregar" style="height: 40px; width: 250px; text-align: center; font-size: 35px;" class="btn btn-default btn-lg" value="Guardar" onclick="return validaArchiveros();" />
		<br><br>
		</form:form>
		
	</c:when>
	
	<c:when test="${paso=='no3'}">	
		<c:url value = "/central/altaArchiveros.htm" var = "archivoPasivo" />
				<form:form method="POST" action="${archivoPasivo}" model="command" name="form1" id="form1">
					<div id="Imprimir">
						
						<br><br>
						<p align="center">
							<b>ACTA DE ENTREGA DE ARCHIVEROS</b>
						<br>
						<br>
						</p>
						
						<table style="margin: 0 auto; width="100%"; border="2";bordercolor="#000000"">
									<tr>
										<th colspan="5">
											<p style="margin-bottom: 15px; margin-top: 15px">
											<img id="codigo-barras" width="100px" src="../images/codigo-barras.png" align="center" style="margin-right: 25px"/>
											<img id="logo" width="100px" src="../images/logo-baz.png" align="center" style="margin-left: 25px"/>
											</p>
										</th>								
									</tr>
									
									<tr>
										<th colspan="3"; style="background-color: rgb(255, 255, 153);">
											<p style="text-align:center;margin-left: 30px;border: 5px solid rgb(255, 255, 153); font-size: 20px">
												FORMATO ACTA DE ENTREGA ACTIVO FIJO
											</p>
										</th>
										<th colspan="2">
										</th>
									</tr>
									<tr>
										<th colspan="5"; style="background-color: rgb(234, 241, 221);">
										<p style="text-align:left;margin-left: 5px;border: 8px solid rgb(234, 241, 221); font-size: 18px">
												Nombre de Socio: ${nombre}
												<br>									
												N&uacute;mero de Socio: ${numempleado} 
												<br>											
												Centro de costos: ${sucursal}
												<br>										
												Nombre de Sucursal: ${nomsucursal}
												<br>
										</p>
										<input id="idFormato" name="idFormato" type="hidden" value="${idFormato}"/>
										<input id="nomSucursal" name="nomSucursal" type="hidden" value="${nomsucursal}"/>
										<input id="numGerente" name="numGerente" type="hidden" value="${numempleado}"/>
										<input id="nomGerente" name="nomGerente" type="hidden" value="${nombre}"/>
										<input id="idSucursal" name="idSucursal" type="hidden" value="${sucursal}"/>
										<input id="descripcion" name="descripcion" type="hidden" value="${descripcion}"/>					
										</th>
									</tr>
									<tr>
										<th>
										<p style="text-align:center;border: 5px solid white;font-size: 18px">
											NO.
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 8px;margin-right: 8px;border: 5px solid white;font-size: 18px">
											Placa
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 10px;margin-right: 10px;border: 5px solid white;font-size: 18px">
											Marca
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 30px;margin-right: 30px;border: 5px solid white;font-size: 18px">
											Descripci&oacute;n
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 30px;margin-right: 30px;border: 5px solid white;font-size: 18px">
											Observaciones
										</p>
										</th>
									</tr>
									
									
									<!--  CONTEO --> 
									<c:set var="count" value="1" scope="page"/>
									<c:forEach var="list" items="${list}">
									<tr>
									<th><p style="text-align:center;"><c:out value="${count}"/></p></th>
									<th>
										<p style="text-align:justify;border: 3px solid white;">
											${list.placa}
										</p>
										</th>
										<th>
										<p style="text-align:justify;border: 3px solid white;">
											${list.marca}
										</p>
										</th>
										<th>
										<p style="text-align:justify;border: 3px solid white;">
											${list.descripcion}
										</p>
										</th>
									<th>
										<p style="text-align:justify; border: 3px solid white;">
											${list.observaciones}
										</p>
										</th>
									</tr>
									<c:set var="count" value="${count + 1}"  scope="page" />
									</c:forEach>
									
									
									<tr>
										<th colspan="5">
										<p style="text-align:center; border: 8px solid white; font-size: 18px">
										Esta acta de entrega, sirve como soporte por la recolecci&oacute;n de los archiveros que se encuentran en
										<br>
										desuso dentro de la sucursal.
										<br>
										La descarga de los activos dentro del sistema se efectuara una vez que el proveedor responsable de este
										<br>
										servicio finalice el proceso.
										</p>
										</th>
									</tr>
									<tr>
										<th colspan="3">
										<br><br><br><br><br><br>
										<p style="text-align:center; border: 8px solid white;">										
										_____________________________________________
										<br/>
										${nombre}<br>
										Socio que entrega los activos fijos
										</p>
										</th>
										<th colspan="2">
										<br><br><br><br><br><br>
										<p style="text-align:center; border: 8px solid white;">
										_____________________________________________
										<br/>
										${nomRecibe}
										<br/>
										Persona que recibe los activos fijos
										</p>
										</th>
									</tr>
									<tr>
										<th colspan="5">
										<p style="text-align:left; border: 8px solid white; margin: 5px; font-size: 18px">
										Nota: Si el activo no cuenta con n&uacute;mero de placa favor de colocar S/P, si el activo no cuenta con marca favor de colocar S/M.
										</p>
										</th>
									</tr>
								</table>
					</div>
		
		<br>
		
				
		<input id="tok" name="tok" type="hidden" value="${tok}"/>
		
		<div style="border-radius: 20px; ; margin: 20px;" class="apps">
		<table style="text-align:center; border-collapse: separate;" >
			<tr>
				<td>
					<div class="inputData">
								<table style="text-align:center; border-collapse: separate;">
								<tr>
								<th><br>
								<br>Entrega
								<br>(Gerente de sucursal)<br><br>
								</th>
								</tr>
									<tr>
										<td><label >Usuario:</label><br><br>
											<input  id="usuario" name="usuario" maxlength="10" style="height: 30px; width: 150px; text-align: center; 	font-size: 20px;
											"/></td>
									</tr>
									
									<tr>
										<td><label >Token:</label><br><br>
											<input  id="llave" name="llave" maxlength="10" type="password" style="height: 30px; width: 150px; text-align: center; 	font-size: 20px;"/></td>
									</tr>
									
									<tr>
									<br><br>
										<td colspan="2">
										<input id="botonEntrega" style="height: 30px; width: 150px; text-align: center; font-size: 20px;" type="button" value="Aceptar"  onClick="return validaEntrega();" /></td>
									</tr>
								</table>
							<br>
								<div class="error" id="error">${response}</div>
								<input id="usr" name="usr" type="hidden" value=""/>
								<input id="token1" name="token1" type="hidden" value=""/>
					</div>		
				</td>
				
			</tr>
						
		</table>
		</div>
		
		<br>
		<input type="submit" id="Agregar" name="Gardar" style="height: 40px; width: 250px; text-align: center; font-size: 35px;" class="btn btn-default btn-lg" value="Guardar" onclick="return validaFirmas();" />
		<br><br><br>
		
		
		</form:form>
	</c:when>
	
	<c:when test="${paso=='no2'}">	
		<c:url value = "/central/altaArchiveros.htm" var = "archivoPasivo" />
				<form:form method="POST" action="${archivoPasivo}" model="command" name="form1" id="form1">
					<div id="Imprimir">
						
						<br><br>
						<p align="center">
							<b>ACTA DE ENTREGA DE ARCHIVEROS</b>
						<br>
						<br>
						</p>
						<table style="margin: 0 auto; width="100%"; border="2";bordercolor="#000000"">
									<tr>
										<th colspan="5">
											<p style="margin-bottom: 15px; margin-top: 15px">
											<img id="codigo-barras" width="100px" src="../images/codigo-barras.png" align="center" style="margin-right: 25px"/>
											<img id="logo" width="100px" src="../images/logo-baz.png" align="center" style="margin-left: 25px"/>
											</p>
										</th>								
									</tr>
									
									<tr>
										<th colspan="3"; style="background-color: rgb(255, 255, 153);">
											<p style="text-align:center;margin-left: 30px;border: 5px solid rgb(255, 255, 153); font-size: 20px">
												FORMATO ACTA DE ENTREGA ACTIVO FIJO
											</p>
										</th>
										<th colspan="2">
										</th>
									</tr>
									<tr>
										<th colspan="5"; style="background-color: rgb(234, 241, 221);">
										<p style="text-align:left;margin-left: 5px;border: 8px solid rgb(234, 241, 221); font-size: 18px">
												Nombre de Socio: ${nombre}
												<br>									
												N&uacute;mero de Socio: ${numempleado} 
												<br>											
												Centro de costos: ${sucursal}
												<br>										
												Nombre de Sucursal: ${nomsucursal}
												<br>
										</p>
										<input id="idFormato" name="idFormato" type="hidden" value="${idFormato}"/>
										<input id="nomSucursal" name="nomSucursal" type="hidden" value="${nomsucursal}"/>
										<input id="numGerente" name="numGerente" type="hidden" value="${numempleado}"/>
										<input id="nomGerente" name="nomGerente" type="hidden" value="${nombre}"/>
										<input id="idSucursal" name="idSucursal" type="hidden" value="${sucursal}"/>
										<input id="descripcion" name="descripcion" type="hidden" value="${descripcion}"/>					
										</th>
									</tr>
									<tr>
										<th>
										<p style="text-align:center;border: 5px solid white;font-size: 18px">
											NO.
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 8px;margin-right: 8px;border: 5px solid white;font-size: 18px">
											Placa
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 10px;margin-right: 10px;border: 5px solid white;font-size: 18px">
											Marca
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 30px;margin-right: 30px;border: 5px solid white;font-size: 18px">
											Descripci&oacute;n
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 30px;margin-right: 30px;border: 5px solid white;font-size: 18px">
											Observaciones
										</p>
										</th>
									</tr>
									
									<!--  CONTEO --> 
									<c:set var="count" value="1" scope="page"/>
									<c:forEach var="list" items="${list}">
									<tr>
									<th><p style="text-align:center;"><c:out value="${count}"/></p></th>
									<th>
										<p style="text-align:justify;border: 3px solid white;">
											${list.placa}
										</p>
										</th>
										<th>
										<p style="text-align:justify;border: 3px solid white;">
											${list.marca}
										</p>
										</th>
										<th>
										<p style="text-align:justify;border: 3px solid white;">
											${list.descripcion}
										</p>
										</th>
									<th>
										<p style="text-align:justify; border: 3px solid white;">
											${list.observaciones}
										</p>
										</th>
									</tr>
									<c:set var="count" value="${count + 1}"  scope="page" />
									</c:forEach>
									
									<tr>
										<th colspan="5">
										<p style="text-align:center;border: 8px solid white; font-size: 18px">
										Esta acta de entrega, sirve como soporte por la recolecci&oacute;n de los archiveros que se encuentran en
										<br>
										desuso dentro de la sucursal.
										<br>
										La descarga de los activos dentro del sistema se efectuara una vez que el proveedor responsable de este
										<br>
										servicio finalice el proceso.
										</p>
										</th>
									</tr>
									<tr>
										<th colspan="3">
										<br><br><br><br><br><br>
										<p style="text-align:center; border: 8px solid white;">										
										_____________________________________________
										<br/>
										${nombre}<br>
										Socio que entrega los activos fijos
										</p>
										</th>
										<th colspan="2">
										<br><br><br><br><br><br>
										<p style="text-align:center; border: 8px solid white;">
										_____________________________________________
										<br/>
										${nomRecibe}
										<br/>
										Persona que recibe los activos fijos
										</p>
										</th>
									</tr>
									<tr>
										<th colspan="5">
										<p style="text-align:left; border: 8px solid white; margin: 5px; font-size: 18px">
										Nota: Si el activo no cuenta con n&uacute;mero de placa favor de colocar S/P, si el activo no cuenta con marca favor de colocar S/M.
										</p>
										</th>
									</tr>
								</table>
						</div>
		<br>
		
		<input id="impreso" name="impreso" type="hidden" value="OK"/>
		<div id="btImprimir">		
			<input type="button" onclick="printDiv('Imprimir');return false;" style="height: 40px; width: 250px; text-align: center; font-size: 35px;" value="Guardar" />
			<br><br><br>
		</div>
		<input id="tok" name="tok" type="hidden" value="${tok}"/>
		<div id="botones" style="display: none;">
			<table style="text-align:center;">
									<tr>
										<th colspan="2">
											¿Se guardó correctamente?
											<br><br>
											
										</th>
										
									</tr>
									<tr>
										<td>
											<br>
											<input type="submit" id="SI" name="SI" style="height: 40px; width: 250px; text-align: center; font-size: 35px;" class="btn btn-default btn-lg" value="SI" onclick="muestra_oculta('btImprimir'); muestra_oculta('botones'); carga2(); carga(); return true;" />
										</td>
										<td>
											<br>
											<input type="submit" id="NO" name="NO" style="height: 40px; width: 250px; text-align: center; font-size: 35px;" class="btn btn-default btn-lg" value="NO" onclick="muestra_oculta('btImprimir'); muestra_oculta('botones'); return false;" />
										</td>
									</tr>
									
								</table>
		</div>
		
		</form:form>
	</c:when>
	<c:when test="${paso=='no4'}">
		<h1>Formato guardado exitosamente</h1>
	</c:when>
	<c:when test="${paso=='error'}">
		<h1>Algo paso al guardar el formato</h1>
		<img src="../images/error/algo_paso.png"/>
		<br/>
		<a href="/franquicia/">
				<img src="../images/error/boton_volver.png"/>
		</a>
	     
					
	</c:when>
	</c:choose>
	<!-- ********************************** Cierre Body************************************************ -->
	<!-- jquery Start -->
	<script type="text/javascript" src="../js/vistaCumplimientoVisitas/jquery.js"></script>
	
	<div id="cargando" style="
	display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('/franquicia/images/ajax-loader.gif') 
                50% 50% 
                no-repeat;"></div>
                
    
	
</body>

</html>