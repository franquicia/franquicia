<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<head>

	<script type="text/javascript" src="../js/script-menu.js"></script> 

	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" type="text/css" href="../css/header-style.css">
	<link rel="stylesheet" type="text/css" href="../css/menuCheck.css">
	<link rel="stylesheet" type="text/css" href="../css/carousel.css">
	

	<script	src="../js/jquery/jquery-2.2.4.min.js"></script>
	
	<script src="../js/script-storeIndex.js"></script>    
	<link rel="stylesheet" type="text/css" href="../css/css-storeIndex.css"></link>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
	
	<title>Acta de entrega de Archiveros</title>
	
	<style type="text/css">
		
		.table-striped>tbody>tr:nth-child(odd)>td, 
      .table-striped>tbody>tr:nth-child(odd)>th {
       background-color: #f2f2f2;
      }
      .table-striped>tbody>tr:hover>td{
       background-color: #EBF8FC;
      }
      
      .table-striped>tbody>tr:nth-child(even)>td,
      .table-striped>tbody>tr:nth-child(even)>th{
       background-color: #fbfbfb;
      }
      
      .table-striped>thead>tr>th {
         background-color: #006141;
	     color: white;
      }
		
	</style>
	<script type="text/javascript">
	$(function() { 
		 $('#download').click(function() {
		  var options = {'width': 1500
		  };
		  //var pdf = new jsPDF('p', 'pt', 'letter');
		  var pdf = new jsPDF('p', 'mm', [800, 1000]);

		  pdf.addHTML($("#Imprimir"), 5, 5, options, function() {
		    pdf.save('informe.pdf');
		  });
		 });
		});

	
	</script>
</head>

<body >

	<tiles:insertTemplate template="/WEB-INF/templates/templateHeader.jsp" flush="true">
		<tiles:putAttribute name="cabecera" value="/WEB-INF/templates/templateHeader.jsp" />
	</tiles:insertTemplate>
	
	<tiles:insertTemplate template="/WEB-INF/views/menu.jsp" flush="true">
		<tiles:putAttribute name="menu" value="/WEB-INF/views/menu.jsp" />
	</tiles:insertTemplate>
	
	<!-- ********************************** Body************************************************ -->
	
	
	<!-- ********************************** Paso 1 llenar el formulario************************************************ -->
	<c:choose>
	<c:when test="${paso=='cons1'}">
	<br>
		<h1>Consulta de archiveros</h1>
			<c:url value = "/central/recuperaArchivero.htm" var = "archivoPasivo" />
			<form:form method="POST" action="${archivoPasivo}" model="command" name="form1" id="form1">
				<div style="overflow-x:auto; margin: 20px;">
					<table class="table table-striped"; style="margin: 0 auto; width="100%">
						<thead>
							<tr>
								<th  data-field="idCheckUsuario" data-sortable="true" data-searchable="true">ID Archivero</th>
								<th  data-field="idChecklist" data-sortable="true" >ID Gerente</th>
								<th  data-field="idChecklist" data-sortable="true" >ID Ceco</th>
								<th  data-field="idUsuario" data-sortable="true" data-searchable="true">Fecha</th>
								<th  data-field="idCeco" data-sortable="true" data-searchable="true">Estatus</th>
								
							</tr>
						</thead>
						<tbody >
							<c:forEach var="list" items="${list}">
								<tr>
									<td ><INPUT TYPE="radio" id="command" name="command" value="${list.idFromArchivero}"/>   <c:out value="${list.idFromArchivero}"></c:out></td>
									<td ><c:out value="${list.idUsuario}"></c:out></td>
									<td ><c:out value="${list.idCeco}"></c:out></td>
									<td ><c:out value="${list.fecha}"></c:out></td>
									
									
									<td >
										<c:if test="${list.idStatus == 1}">
									    	Lleno
										</c:if>
										<c:if test="${list.idStatus == 2}">
									    	Firmado
										</c:if>
										<c:if test="${list.idStatus == 3}">
									    	Impreso
										</c:if>
									</td>									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
					<input id="idExpediente" name="idExpediente" type="hidden" value=""/>
					<input type="submit" id="Recuperar" name="Recuperar" style="height: 40px; width: 250px; text-align: center; font-size: 35px;" class="btn btn-default btn-lg" value="Recuperar" onclick="return recupera(); return false;" />
					<br><br>
			</form:form>
	 
	</c:when>
	
	
	<c:when test="${paso=='impreso'}">
		<h1>Recuperaci&oacute;n de Formato Impreso</h1>	
					<div id="Imprimir"  style="background-color: white;width:100%;">
						
						<br><br>
						<p align="center">
							<b>ACTA DE ENTREGA DE ARCHIVEROS</b>
						</p>
						<br/>
						<br/>
						<table style="margin-right: 20px; margin-left: 20px; " width="95%" border="2" bordercolor="#000000">
									<tr>
										<th colspan="5">
											<p style="margin-bottom: 15px; margin-top: 15px">
											<img id="codigo-barras" width="100px" src="../images/codigo-barras.png" align="center" style="margin-right: 25px"/>
											<img id="logo" width="100px" src="../images/logo-baz.png" align="center" style="margin-left: 25px"/>
											</p>
										</th>								
									</tr>
									
									<tr>
										<th colspan="3"; style="background-color: rgb(255, 255, 153);">
											<p style="text-align:center;margin-left: 30px;border: 5px solid rgb(255, 255, 153); font-size: 20px">
												FORMATO ACTA DE ENTREGA ACTIVO FIJO
											</p>
										</th>
										<th colspan="2">
										</th>
									</tr>
									<tr>
										<th colspan="5"; style="background-color: rgb(234, 241, 221);">
										<p style="text-align:left;margin-left: 5px;border: 8px solid rgb(234, 241, 221); font-size: 18px">
												Nombre de Socio: ${nombre}
											
										<br/>								
												N&uacute;mero de Socio: ${numempleado} 
										<br/>												
												Centro de costos: ${sucursal}
										<br/>							
												Nombre de Sucursal: ${nomsucursal}
										</p>
										<input id="nomSucursal" name="nomSucursal" type="hidden" value="${nomsucursal}"/>
										<input id="numGerente" name="numGerente" type="hidden" value="${numempleado}"/>
										<input id="nomGerente" name="nomGerente" type="hidden" value="${nombre}"/>
										<input id="idSucursal" name="idSucursal" type="hidden" value="${sucursal}"/>
										<input id="descripcion" name="descripcion" type="hidden" value="${descripcion}"/>					
										</th>
									</tr>
									<tr>
										<th>
										<p style="text-align:center;border: 5px solid white;font-size: 18px">
											NO.
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 8px;margin-right: 8px;border: 5px solid white;font-size: 18px">
											Placa
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 10px;margin-right: 10px;border: 5px solid white;font-size: 18px">
											Marca
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 30px;margin-right: 30px;border: 5px solid white;font-size: 18px">
											Descripci&oacute;n
										</p>
										</th>
										<th>
										<p style="text-align:center;margin-left: 30px;margin-right: 30px;border: 5px solid white;font-size: 18px">
											Observaciones
										</p>
										</th>
									</tr>
									
									<!--  CONTEO --> 
									<c:set var="count" value="1" scope="page"/>
									<c:forEach var="list" items="${list}">
									<tr>
									<th><p style="text-align:center;"><c:out value="${count}"/></p></th>
									<th>
										<p style="text-align:justify;border: 3px solid white;">
											${list.placa}
										</p>
										</th>
										<th>
										<p style="text-align:justify;border: 3px solid white;">
											${list.marca}
										</p>
										</th>
										<th>
										<p style="text-align:justify;border: 3px solid white;">
											${list.descripcion}
										</p>
										</th>
									<th>
										<p style="text-align:justify; border: 3px solid white;">
											${list.observaciones}
										</p>
										</th>
									</tr>
									<c:set var="count" value="${count + 1}"  scope="page" />
									</c:forEach>
									
									<tr>
										<th colspan="5">
										<p style="text-align:center;border: 8px solid white; font-size: 18px">
										Esta acta de entrega, sirve como soporte por la recolecci&oacute;n de los archiveros que se encuentran en
										desuso dentro de la sucursal.
										</p>
										<p style="text-align:center;border: 8px solid white; font-size: 18px">
										La descarga de los activos dentro del sistema se efectuara una vez que el proveedor responsable de este
										servicio finalice el proceso.
										</p>
										</th>
									</tr>
									<tr>
										<th colspan="3">
										<br><br><br><br><br>
										<p style="text-align:center; border: 8px solid white;">
										
										_____________________________________________
										<p>${nombre}</p>
										<p>
										Socio que entrega los activos fijos
										</p>
										</th>
										<th colspan="2">
										<br><br><br><br><br>
										<p style="text-align:center; border: 8px solid white;">
										_____________________________________________
										<p>${nomRecibe}</p>
										<p>
										Persona que recibe los activos fijos
										</p>
										</th>
									</tr>
									<tr>
										<th colspan="5">
										<p style="text-align:left; border: 8px solid white; margin: 5px; font-size: 18px">
										Nota: Si el activo no cuenta con n&uacute;mero de placa favor de colocar S/P, si el activo no cuenta con marca favor de colocar S/M.
										</p>
										</th>
									</tr>
								</table>
						
								<br>
								
					</div>
		
		<br><br>
		<!-- 
			<button id="download">DESCARGAR PDF</button>
			 -->
	</c:when>
	<c:when test="${paso=='no'}">
		<h1>No se encontró ningún formato para la sucursal</h1>
	</c:when>
	</c:choose>
	<!-- ********************************** Cierre Body************************************************ -->
</body>

</html>