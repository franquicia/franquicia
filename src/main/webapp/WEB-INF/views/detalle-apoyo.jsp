<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> REPORTE DE ÁREAS DE APOYO</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome top60">
                <div class="tapoyo">Detalle del Folio</div>
                <div class="border">  
                    <div class=" overflow mh500">   
                        <table class="items tbAreas">
                            <tbody>
                                <tr>   
                                    <td>
                                        <b>Reporte</b>
                                    </td>
                                    <td>
                                        <b>${FOLIO}</b>
                                    </td> 
                                </tr>  
                                <tr>   
                                    <td>
                                        <b>Área Involucrada</b>
                                    </td>
                                    <td>
                                        <b>${AREA_INV}</b>
                                    </td> 
                                </tr>
                                <tr>   
                                    <td>
                                        <b>Prioridad</b>
                                    </td>
                                    <td>
                                        <b>${PRIORIDAD}</b>
                                    </td> 
                                </tr>
                                <tr>   
                                    <td>
                                        <b>Evidencias</b>
                                    </td>
                                    
                                    <td>
                                   <script type="text/javascript">
								 	
									    function RutaFoto(rutafoto){
									    	
									    document.getElementById('RutaFoto1').src = "http://10.53.33.82"+rutafoto;
									        }
									</script>
                                    	 <c:forEach var="listaevid" items="${listaevid}">
                                    	 <a href="#" class="MApollo2" onclick='RutaFoto("${listaevid.ruta}")'><b><c:out value="${listaevid.nomarchivo}"></c:out></b></a><br>
                                    		
                                    	</c:forEach>
                                    	
                                        <!-- <a href="#" class="MApollo1"><b>${listaevid}</b></a><br>
                                        <a href="#" class="MApollo2"><b>Reporte Areas Apoyo_02</b></a><br>
                                        <a href="#" class="MApollo3"><b>Reporte Areas Apoyo_03</b></a><br>  -->
                                    </td> 
                                </tr>
                            </tbody>
                        </table> 
                    </div>  
                </div>

        		<div class="botones"> 
                    <div class="tleft w50">
                        <a href="reporte-de-area-de-apoyo.htm" class="btn arrowl">
                            <div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
                            <div>VOLVER AL LISTADO</div>
                        </a> 
                    </div>

                    <%-- <div class="tright w50"> 
                        <a href="#" class="btn arrowr">
                            <div><img src="${pageContext.request.contextPath}/img/arrowright.png"></div>
                            <div>SIGUIENTE FOLIO</div>
                        </a> 
                    </div>  --%>
                </div>
    	    </div>
        </div>

        <!--MODAL-->
        <div id="apoyo1" class="modal">
            <div class="w100">
                <div class="tModal"><b>Reporte Areas Apoyo 01</b></div>
                <div class="cuadrobig">  
                    <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                    <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">
                    
                </div>
            </div>
            <!-- <div class="bModalUser"> 
                <div class="tleft w50">
                    <a href="#" class="btn  bm arrowml simplemodal-close MApollo3">
                        <div></div>
                        <div>FOTO ANTERIOR</div>
                    </a> 
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr simplemodal-close MApollo2">
                        <div></div>
                        <div>SIGUIENTE FOTO</div>
                    </a> 
                </div> 
            </div> -->
        </div>
        <div id="apoyo2" class="modal">
            <div class="w100">
                <div class="tModal"><b>Reporte Areas Apoyo 02</b></div>
                <div class="cuadrobig">  
                    <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                    <img id="RutaFoto1" src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">
                    
                </div>
            </div>
            <!-- <div class="bModalUser"> 
                <div class="tleft w50">
                    <a href="#" class="btn  bm arrowml simplemodal-close MApollo1">
                        <div></div>
                        <div>FOTO ANTERIOR</div>
                    </a> 
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr simplemodal-close MApollo3">
                        <div></div>
                        <div>SIGUIENTE FOTO</div>
                    </a> 
                </div> 
            </div>-->
        </div>
        <div id="apoyo3" class="modal">
            <div class="w100">
                <div class="tModal"><b>Reporte Areas Apoyo 03</b></div>
                <div class="cuadrobig">  
                    <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                    <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">
                </div>
            </div>
            <div class="bModalUser"> 
                <div class="tleft w50">
                    <a href="#" class="btn  bm arrowml simplemodal-close MApollo2">
                        <div></div>
                        <div>FOTO ANTERIOR</div>
                    </a> 
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr simplemodal-close MApollo1">
                        <div></div>
                        <div>SIGUIENTE FOTO</div>
                    </a> 
                </div> 
            </div>
        </div>
        <!-- -->
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.MApollo1').click(function (e) {
                        setTimeout(function() {
                            $('#apoyo1').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.MApollo2').click(function (e) {
                        setTimeout(function() {
                            $('#apoyo2').modal({
                            });
                            return false;
                        }, 1);
                    });
                    $('.MApollo3').click(function (e) {
                        setTimeout(function() {
                            $('#apoyo3').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            }); 
        </script>
    </body>
</html>



