<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="titulags"><b> FORMATO CUMPLIMIENTO METODOLOGÍA 7S</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome top60">
    			<div class="tituloScont"><b> Selecciona al líder responsable para visualizar los registros de su plantilla.</b></div><br>
    			<div class="border borderLeft">  
					<div class=" overflow mh500">   
						<table class="items anexoDos">
							<tbody>
								<c:forEach var="lista" items="${lista}">
										<tr>
											<td>
												<a href="formato-cumplimiento-usuario.htm?idUsu=<c:out value="${lista.idUser}"/>"><c:out value="${lista.nomUser}"/></a>
											</td> 
										</tr>
								</c:forEach>
							</tbody>
						</table> 
					</div>  
			    </div>

	    		<div class="botones"> 
					<a href="inicio7s.htm" class="btn arrowl">
						<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
						<div>VOLVER AL MENú</div>
					</a>   
				</div>
			</div>
    	</div>
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    </body>
</html>



