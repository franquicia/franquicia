<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div class="contSecc">
		<div class="titSec">INSERT/UPDATE CANAL</div>
		<div class="gris">
			<form:form action="insertUpdateCanalPD.htm" method="POST">
				<div id="divTipos" class="divCol2">
					<div class="col2">
						<input type="radio" name="tipo" id="tipo1" value="insert" checked="checked">
						<label for="tipo1">Inserta</label>
						<input type="radio" name="tipo" id="tipo2" value="update">
						<label for="tipo2">Actualiza</label>
					</div>
				</div>

				<div class="divCol3">
					<div class="col3">
						Inserta/Actualiza...
						<b style="color: red;">${resultado }</b>
						<p>
						<p>
							<label>ID CANAL</label>
							<form:input type="num" path="idCanal" />
						<p>
							<label>DESCRIPCION CANAL:</label>
							<form:input type="text" path="descrpicion" />
						<p>
							<label>ACTIVO</label>
							<form:input type="num" path="activo" />
							<br>
					</div>
				</div>

				<div class="divCol3">
					<div class="col3">
						<input id="btn" type="submit" class="btnA" value="Enviar">
					</div>
				</div>
			</form:form>
		</div>
	</div>

</body>
</html>