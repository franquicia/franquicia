<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>CONDUSEF</title>
  <link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/estilos.css">
  <link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/pedestaliPAD.css">
  <link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/avisoPrivacidad-BAZ.css">


<script type="text/javascript" src="/franquicia/js/pedestalPWA/jquery-1.12.4.js"></script>
<script type="text/javascript" src="/franquicia/js/pedestalPWA/jquery.dropkick.js"></script><!--Select -->
<script type="text/javascript" src="/franquicia/js/pedestalPWA/jquery-ui.js"></script>
<script src="/franquicia/js/pedestalPWA/jquery.simplemodal.js"></script>

<script type="text/javascript" src="/franquicia/js/pedestalPWA/factoryBD.js"></script>
<script type="text/javascript" src="/franquicia/js/pedestalPWA/funciones.js"></script>
<script type="text/javascript" src="/franquicia/js/pedestalPWA/pedestaliPad.js"></script>
<script type="text/javascript" src="/franquicia/js/pedestalPWA/funcionesPedestal.js"></script>


</head>
	
<body>

  <div class="page">
    <div class="header headerContenido">
        <div class="divRegresar">
            <a href="/franquicia/central/pedestal.html" class="linkRegresar tituloNegro"><img src="/franquicia/img/pedestalPWA/iconosIPAD/iconoRegresar.png" alt="flecha"> Regresar</a>
        </div>
		<div class="headerLogo">
		  <a href="/franquicia/central/pedestal.html"><img src="/franquicia/img/pedestalPWA/logo-baz-lema.svg" id="imgLogo"></a>
		</div>
	</div>

    <!-- Contenido -->
    <div class="contSecc pRelative">
		<!-- <div class="divBuscar">
			<form id="miForm">
				<div class="buscador">
					<input type="text" id="busca" placeholder="Busca el documento que deseas consultar">
					<input type="submit" class="buscar" value="">
				</div>
			</form>
		</div> -->
		<div class="wrapper">
            <h2 id="tituloCategoria" class="tituloPagina tituloNegro"></h2>
            <script>
                
                $('#tituloCategoria').text(localStorage.getItem('categoria'));

            </script>           
            <div id ='listaDocumentos' class="divDocumentos">
                <script type="text/javascript">

                    var storedArray = JSON.parse(localStorage.getItem("tipos_documentos"));

                    $('#listaDocumentos').empty();

                    var i;
                    for (i = 0; i < storedArray.length; i++) {
                        $('#listaDocumentos').append("<a id='"+storedArray[i].id+"' class='documento modalVisualizador_view'><div class='iconoPDF'><img src='/franquicia/img/pedestalPWA/iconosIPAD/iconoPDF.png' alt='iconoPDF'></div><div class='textoDocumento'>"+storedArray[i].descripcion+"</div>");        
                        
                    }

                 </script>
                <!-- <a href="#buro" class="documento modalVisualizador_view">
                    <div class="iconoPDF"><img src="img/iconosIPAD/iconoPDF.png" alt="iconoPDF"></div>
                    <div class="textoDocumento">Aviso del Buró de Entidades Financieras</div>
                </a>
                <a href="#une" class="documento modalVisualizador_view">
                    <div class="iconoPDF"><img src="img/iconosIPAD/iconoPDF.png" alt="iconoPDF"></div>
                    <div class="textoDocumento">Aviso de la UNE (Unidad Especializada).</div>
                </a>
                <a href="#" class="documento modalVisualizador_view">
                    <div class="iconoPDF"><img src="img/iconosIPAD/iconoPDF.png" alt="iconoPDF"></div>
                    <div class="textoDocumento">Relación de Registros de Despachos de Cobranza (REDECO)</div>
                </a> -->
            </div>
        </div>
		<div class="clear"></div>
	</div>
	<!-- Fin conSecc -->
	<div class="footer">
		<div class="textoFooter">Banco Azteca S.A. Instituci�n de Banca M�ltiple</div>
	</div>
	</div>
<!-- Fin page -->

</body>
</html>

<div id="modalVisualizador" class="modal">
    <div class="cuadro cuadroXG">
        <div class="divRegresarModal">
            <a id='backIframe' href="#" class="linkRegresar tituloBlanco simplemodal-close"><img src="/franquicia/img/pedestalPWA/iconosIPAD/iconoRegresarBlanco.png" alt="flecha"> Regresar</a>
        </div>
        <div class="clear"></div>
        <div class="w94Modal">
            <iframe id="iframePDF"></iframe>
        </div>
    </div>
</div>





