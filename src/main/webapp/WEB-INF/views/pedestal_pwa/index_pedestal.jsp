<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
  <title>Pedestal Digital</title>
  <link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/estilos.css">
  <link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/pedestaliPAD.css">
  <script type="text/javascript" src="/franquicia/js/pedestalPWA/factoryBD.js"></script>
  <script type="text/javascript" src="/franquicia/js/pedestalPWA/jquery-1.12.4.js"></script>
  <script type="text/javascript" src="/franquicia/js/pedestalPWA/funcionesPedestal.js"></script>
  
  <link rel="manifest" href="/franquicia/js/pedestalPWA/manifest.json">
  
</head>

<body>

  <div class="page">
    <div class="header">
		<div class="headerLogo">
		  <a href="#"><img src="/franquicia/img/pedestalPWA/logo-baz-lema.svg" id="imgLogo"></a>
		</div>
	</div>

    <!-- Contenido -->
    <div class="contSecc pRelative">
		<h1 class="tituloPrincipal tituloNegro">�Bienvenido!</h1>
		<!-- <div class="divBuscar">
			<form id="miForm">
				<div class="buscador">
					<input type="text" id="busca" placeholder="Busca el documento que deseas consultar">
					<input type="submit" class="buscar" value="">
				</div>
			</form>
		</div> -->
		<div class="divOpcionesInicio">
			<a value="9" href="#"  class="card" >
				<div><img class="iconoInicio" src="/franquicia/img/pedestalPWA/iconosIPAD/condusef.png"></div>
				<div class="tituloOpciones">CONDUSEF</div>
			</a>
			<a value="1" href="#" class="card">
				<div><img class="iconoInicio icoComisiones" src="/franquicia/img/pedestalPWA/iconosIPAD/comisiones.svg" style="height: 81px;"></div>
				<div class="tituloOpciones">Comisiones</div>
			</a>
			<a value="2" href="#" class="card">
				<div><img class="iconoInicio" src="/franquicia/img/pedestalPWA/iconosIPAD/documentos-legales.png"></div>
				<div class="tituloOpciones">Documentos <br> Legales</div>
			</a>
			<a value="3" href="#" class="card">
				<div><img class="iconoInicio" src="/franquicia/img/pedestalPWA/iconosIPAD/folletos-informativos.png"></div>
				<div class="tituloOpciones">Folletos <br> Informativos</div>
			</a>
			<a value="5" href="#" class="card">
				<div><img class="iconoInicio" src="/franquicia/img/pedestalPWA/iconosIPAD/mas-buscados.png"></div>
				<div class="tituloOpciones">SEPROBAN</div>
			</a>
			<a value="6" href="#" class="card">
				<div><img class="iconoInicio" src="/franquicia/img/pedestalPWA/iconosIPAD/licencias.png"></div>
				<div class="tituloOpciones">Licencias de <br> Funcionamiento</div>
			</a>
			<a value="7" href="#" class="card">
				<div><img class="iconoInicio" src="/franquicia/img/pedestalPWA/iconosIPAD/aviso-privacidad.png"></div>
				<div class="tituloOpciones">Aviso de privacidad <br> Afore Azteca</div>
			</a>
			<a value="8" href="#" class="card">
				<div><img class="iconoInicio" src="/franquicia/img/pedestalPWA/iconosIPAD/aviso-privacidad.png"></div>
				<div class="tituloOpciones">Aviso de privacidad <br> Banco Azteca</div>
			</a>
			<a value="11" href="#" class="card">
				<div><img class="iconoInicio" src="/franquicia/img/pedestalPWA/iconosIPAD/aviso-privacidad.png"></div>
				<div class="tituloOpciones">Aviso de privacidad <br> Seguros Azteca</div>
			</a>
			<a value="4" href="#" class="card">
				<div><img class="iconoInicio" src="/franquicia/img/pedestalPWA/iconosIPAD/portabilidad.png"></div>
				<div class="tituloOpciones">Portabilidad <br> de n�mina</div>
			</a>
		</div>
		<div class="clear"></div>
		<div class="btnBuzon">
			<a href="quejasSugerencias.html" class=""><img src="/franquicia/img/pedestalPWA/iconosIPAD/iconoBuzon.svg" alt="iconoBuzon"></a>
		</div>
	</div>
	<!-- Fin conSecc -->
	<div id='divFooter' class="footer">
	
		<script type="text/javascript">

		consultaUsuario().then(data=>{
			
			 $('#divFooter').append('<div class="bloqueSucursal">Te encuentras en la sucursal <span><span class="bold">'+data.ceco+'</span> - '+data.desc+'</span></div>');		
			 $('#divFooter').append('<div class="textoFooter">Banco Azteca S.A. Instituci�n de Banca M�ltiple</div>');	 
		 });
		 
		</script>
	
		
	</div>
	</div>
<!-- Fin page -->

</body>
</html>



<script type="text/javascript" src="/franquicia/js/pedestalPWA/app.js"></script>
<script type="text/javascript" src="/franquicia/js/pedestalPWA/jquery.dropkick.js"></script>
<script type="text/javascript" src="/franquicia/js/pedestalPWA/jquery-ui.js"></script>
<script type="text/javascript" src="/franquicia/js/pedestalPWA/funciones.js"></script>
<script type="text/javascript" src="/franquicia/js/pedestalPWA/pedestaliPad.js"></script>

