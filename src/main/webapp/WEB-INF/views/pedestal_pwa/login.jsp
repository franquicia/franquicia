<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
		<title>Bienvenido a Pedestal Digital</title>
		<link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/estilos.css">
		<link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/pedestaliPAD.css">
		<link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/bienvenida.css">

		<script src="/franquicia/js/pedestalPWA/jquery-1.12.4.min.js"></script>
		<script type="text/javascript" src="/franquicia/js/pedestalPWA/jquery-1.12.4.js"></script>
		<script type="text/javascript" src="/franquicia/js/pedestalPWA/jquery.dropkick.js"></script><!--Select -->
		<script type="text/javascript" src="/franquicia/js/pedestalPWA/jquery-ui.js"></script>
		<script src="/franquicia/js/pedestalPWA/jquery.simplemodal.js"></script>
	</head>

	<body>
		<div class="w768 bottom200">
			<div class="contenedor_info">

				<div class="cont_img_header">
					<img src="/franquicia/img/pedestalPWA/logo_azteca.png">
				</div>

				<div class="titulo_bienvenida">
					<div>Bienvenido a</div>
					<div><b>Pedestal Digital</b></div>
				</div>

				<div class="tb_form">
					<div class="cll_from">
						<div class="auto">
						
							<form>
								<div>
									<label class="info">Ingresar usuario</label>
									<input type="text" name="usurio" class="dato usuario" placeholder="Ingresar usuario">

									<div class="mensaje_alerta">
										<div class="tb_mensaje_alerta alertaUno" style="margin-top:-88px;">
											<div class="cll_mensaje_alerta">
												<img src="/franquicia/img/pedestalPWA/close.png">
											</div>
											<div class="cll_mensaje_alerta">
												<span class="descripcionUno">�No olvides este campo!</span>
											</div>
										</div>
									</div>

								</div>

								<div>
									<label class="info">Ingresar token</label>
								    <input type="password" class="dato token" placeholder="Ingresa los 6 d�gitos de tu token">

								    <div class="mensaje_alerta">
										<div class="tb_mensaje_alerta alertaDos" style="margin-top:-88px;">
											<div class="cll_mensaje_alerta">
												<img src="/franquicia/img/pedestalPWA/close.png">
											</div>
											<div class="cll_mensaje_alerta">
												<span class="descripcionDos">�Lo sentimos, el n�mero de token es inv�lido.
											</div>
										</div>
									</div>
								</div>
							</form>

					    </div>
					</div>
				</div>

				<div class="btnCenter top300">
	                <button class="btn5 validar_formulario ">Continuar</button> 
	            </div>
			</div>
			
					
		</div>

		<div class="footer">
			<div class="textoFooter">
			    Banco Azteca S.A. Instituci�n de Banca M�ltiple
			</div>
		</div>	
		

		
	</body>
	
	

	<script src="/franquicia/js/pedestalPWA/factoryBD.js"></script>
	<script src="/franquicia/js/pedestalPWA/funcionesPedestal.js"></script>
	<script src="/franquicia/js/pedestalPWA/bienvenida.js"></script>
</html>
