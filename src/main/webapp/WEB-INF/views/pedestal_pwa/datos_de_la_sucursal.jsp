<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
		<title>Datos de la sucursal</title>
		<link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/estilos.css">
		<link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/pedestaliPAD.css">
		<link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/datos_de_la_sucursal.css">
	</head>

	<body>
		<div class=" w768 bottom200">
			<div class="contenedor_info">

				<div class="cont_img_header">
					<img src="/franquicia/img/pedestalPWA/logo_azteca.png">
				</div>

				<div class="datos_sucursal">
					<div>Datos de la sucursal:</div>
					<div id='divCeco'></div>
				</div>

				<div class="tb_datos_alerta">
					<div class="cll_datos_alerta">
						<div class="auto_alerta">
							<div class="tb_alerta">
								<div class="cll_alerta">
									<div class="alerta_verde">
										<img src="/franquicia/img/pedestalPWA/alerta.svg">
									</div>
								</div>

								<div class="cll_alerta">
									<div class="leyenda_error">
										�La sucursal en la que se instalar�?<br>
										el pedestal es correcta?
									</div>
								</div>
							</div>

							<form class="formilario_ce_co" style="display:none;">
								<div>
									<label class="info">Ingresa ceco de la sucursal</label>
									<input type="text"  class="dato" placeholder="Ingresa los 6 d��gitos de ceco de la sucursal">


								    <div class="mensaje_alerta">
										<div class="tb_mensaje_alerta alertaUno" style="margin-top:-88px;">
											<div class="cll_mensaje_alerta">
												<img src="/franquicia/img/pedestalPWA/close.png">
											</div>
											<div class="cll_mensaje_alerta">
												<span class="descripcionUno">!Lo sentimos, el n�mero de token es inv�lido.
											</div>
										</div>
									</div>


								</div>
							</form>
							<div class="btnCenter top3">
				                <button class="btn5 btn_gris   b200 si_instalar">No, modificar</button>
				                <button class="btn5 btn_verde  b200 no_instalar">S��, instalar</button>
				                <button class="btn5 btn_verde  instalar" style="display:none;">Validar ceco</button>
				            </div>

		                </div>
					</div>
				</div>

			</div>
		</div>

		<div class="footer">
			
			<div class="textoFooter">
			    Banco Azteca S.A. Instituci�n de Banca M�ltiple
			</div>
		</div>
	</body>
	<script src="/franquicia/js/pedestalPWA/jquery-1.12.4.min.js"></script>
	<script src="/franquicia/js/pedestalPWA/factoryBD.js"></script>
	<script src="/franquicia/js/pedestalPWA/datos_de_la_sucural.js"></script>

</html>
