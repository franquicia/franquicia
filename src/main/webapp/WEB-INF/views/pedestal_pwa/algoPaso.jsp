<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" />
		<title>Dtos de la sucursal</title>
		<link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/estilos.css">
		<link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/pedestaliPAD.css">
		<link rel="stylesheet" type="text/css" href="/franquicia/css/pedestalPWA/datos_de_la_sucursal.css">
	</head>

	<body>
		<div class="contenedor_pagina">
			<div class="contenedor_info">

				<div class="logo_banco">
					<img src="/franquicia/img/pedestalPWA/logo-baz-lema.svg">
				</div>

				<div class="contenedor_mal">
					<img src="/franquicia/img/pedestalPWA/enchufe.svg">
				</div>

				<div class="leyenda_mal">
					<div>Parece que algo anda mal.</div>
					<div>
						Estamos trabajando para solucionarlo <br>
					    lo m�ss r�pido posible.
					</div>
				</div>

				<div class="btnCenter tp100">
				    <a href="#" class="btn6 btn_verde si_instalar">Regresar</a>
				</div>

			</div>
		</div>

		<div class="footer">
			<div class="bloqueSucursal">Te encuentras en la sucursal <span><span class="bold">528374</span> - Insurgentes Sur Tlalpan</span></div>
			<div class="textoFooter">
			    Banco Azteca S.A. Instituci�nn de Banca M�ltiple
			</div>
		</div>
	</body>
</html>
