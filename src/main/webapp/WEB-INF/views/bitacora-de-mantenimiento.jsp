<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b>BITÁCORA DE MANTENIMIENTO</b></span>
    	</div>
    	<div class="page">
    		<div class="contHome top60">
	    		<div class="tituloScont"><b>Selecciona la fecha para visualizar los folios.</b></div><br>
	    		<div class="border borderLeft">  
						<div class=" overflow mh500">
							  
							<table class="items anexoDos">
								<tbody>
									<c:forEach var="lista" items="${lista}">
										<tr>
											<td>
												<div><a href="bitacora-de-mantenimiento2.htm?folio=<c:out value="${lista.folio}"></c:out>"><c:out value="${lista.fechaSolicitud}"></c:out> <c:out value="${lista.nomProveedor}"></c:out></a></div>
											</td> 
										</tr>
									</c:forEach>  
									
								</tbody>
							</table> 
							
						</div>  
				    </div>
				
	    		<div class="botones"> 
					<a href="inicio7s.htm" class="btn arrowl">
						<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
						<div>VOLVER AL MENÚ</div>
					</a>   
				</div>
			</div>
    	</div>

    	<!--MODAL-->
        <div id="folioUno" class="modal">
            <div class="cuadrobigmax ">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="tsModal">Anexo Bitácora de Mantenimiento</div>
	                <div class="sbModal">4 marzo 2019</div><br>
	                <div class="sbModal">Folio 123123</div>
	                <div class="border">  
						<div class=" overflow mh300 w100">   
							<table class="items BiModal">
								<tbody>
									<tr>   
										<td><b>Nombre del Proveedor</b></td>
										<td><b>Reparaciones Gonzáles</b></td>
									</tr>  
									<tr>   
										<td><b>Tipo de Mantenimiento</b></td>
										<td><b>Correctivo</b></td> 
									</tr>
									<tr>   
										<td><b>Fecha de solicitud</b></td>
										<td><b>4 marzo 2019</b></td> 
									</tr>
									<tr>   
										<td><b>Hora de entrada</b></td>
										<td><b>2:30 PM</b></td> 
									</tr>
									<tr>   
										<td><b>Hora de salida</b></td> 
										<td><b>6:45 PM</b></td>
									</tr>
									<tr>   
										<td><b>Número de personas</b></td>
										<td><b>2</b></td>
									</tr>
								</tbody>
							</table> 
						</div>
				    </div><br>
				  
					<div class="sbModal">Detalles de los trabajos realizados</div><br>
					<div class="cdescripcion">
						<div class="padFooter">
							<b>La fachada de la sucursal tuvo que ser reparada ya que la pintura se cayó y <br>
							presenta daños por humedad y desgaste.</b>
						</div>
					</div>
				</div>
           </div>
            <div class="bModalUser"> 
				<div class="tleft w50">
    				<a href="#" class="btn  bm arrowml ">
    					<div></div>
    					<div>FOLIO ANTERIOR</div>
    				</a> 
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr  FolioDos simplemodal-close">
                        <div></div>
                        <div>SIGUIENTE FOLIO</div>
                    </a> 
                </div> 
			</div>
        </div>

        <div id="folioDos" class="modal">
            <div class="cuadromed">  
                <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a>
                <div class="paddingMuser"> 
	                <div class="tsModal">Anexo Bitácora de Mantenimiento</div>
	                <div class="sbModal">4 marzo 2019</div><br>
	                <div class="sbModal">Folio 678910</div>
	                <div class="border">  
						<div class=" overflow mh200 w100">   
							<table class="items BiModal">
								<tbody>
									<tr>   
										<td><b>Nombre del Proveedor</b></td>
										<td><b>Pinturas Rodríguez</b></td>
									</tr>  
									<tr>   
										<td><b>Tipo de Mantenimiento</b></td>
										<td><b>Preventivo</b></td> 
									</tr>
									<tr>   
										<td><b>Fecha de solicitud</b></td>
										<td><b>22 marzo 2019</b></td> 
									</tr>
									<tr>   
										<td><b>Hora de entrada</b></td>
										<td><b>12:00 PM</b></td> 
									</tr>
									<tr>   
										<td><b>Hora de salida</b></td> 
										<td><b>6:00 PM</b></td>
									</tr>
									<tr>   
										<td><b>Número de personas</b></td>
										<td><b>4</b></td>
									</tr>
								</tbody>
							</table> 
						</div>
				    </div><br>
				  
					<div class="sbModal">Detalles de los trabajos realizados</div><br>
					<div class="cdescripcion">
						<div class="padFooter">
							  <b>Se realizaron trabajos de pintura sobre las paredes interiores de la sucursal <br>
							  para colocar el nuevo color corporativo.</b>
						</div>
					</div>
				</div>
           </div>
            <div class="bModalUser"> 
				<div class="tleft w50">
    				<a href="#" class="btn  bm arrowml FolioUno simplemodal-close">
    					<div></div>
    					<div>FOLIO ANTERIOR</div>
    				</a> 
                </div>

                <div class="tright w50"> 
                    <a href="#" class="btn bm arrowmr ">
                        <div></div>
                        <div>SIGUIENTE FOLIO</div>
                    </a> 
                </div> 
			</div>
        </div>
        <!-- -->
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>

    	<!--MODAL-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.FolioUno').click(function (e) {
                        setTimeout(function() {
                            $('#folioUno').modal({
                            });
                            return false;
                        }, 1);
                    });

                    $('.FolioDos').click(function (e) {
                        setTimeout(function() {
                            $('#folioDos').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            }); 
        </script>
    </body>
</html>



