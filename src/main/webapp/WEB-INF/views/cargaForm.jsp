<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Ingresa Archivo</title>
<script src="js/script-validaciones.js"></script>
<style>
.error {
	color: #a93d35;
	font-style: italic;
}
</style>
</head>
<body>
	<div id="formArchiveContainer">
		<form:form method="POST" id="uploadForm" enctype="multipart/form-data"
			action="archiveLoad.htm">
			<table>
				<tr>
					<td><form:label path="titulo">T&iacute;tulo del Documento</form:label></br>
						<form:input path="titulo" id="titulo" /></td>
				</tr>
				<tr>
					<td><form:label path="descripcion">Descripci&oacute;n del Documento</form:label></br>
						<form:input path="descripcion" id="descripcion" /></td>
				</tr>
				<tr>
					<td><label>Selecciona el archivo (PDF):</label>
					 <form:input type="file" name="file" id="file" path="archive" /></td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="SUBIR DOCUMENTO"
						onClick="return validaFormArchive();" /></td>
				</tr>
			</table>
		</form:form>
	<div class="error" id="error">${response}</div>
	</div>
	<div id="linkSalirCenterContainer">
		<a id="buttonLink" href="mensajes.htm" title="out">Salir</a>
	</div>
</body>
</html>