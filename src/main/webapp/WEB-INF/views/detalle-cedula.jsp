<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> CÉDULA DE IDENTIFICACIÓN PERSONAL</b></span>
    	</div>
    	<div class="page">
    		<c:choose>
	        <c:when test="${datos == 1}">
	    		<div class="contHome top60">
	                <div class="w40">
	                    <div class="cdUser">
	                        <div class="padUser">                        
	                            <div class="imgCedula">
	                    		<img src="http://10.53.33.82${RUTA_FOTO}">
	                            </div>
	                        </div>
	                    </div>
	                </div>
	
	                <div class="w60">
	                    <div class="borderDos">  
	                        <div class=" overflow mh200">   
	                            <table class="items tbUser">
	                                <tbody>
	                                    <tr>   
	                                        <td>
	                                            <b>Nombre:</b>
	                                        </td>
	                                        <td>
	                                            <div><b>${NOM_USUARIO}</b></div>
	                                        </td> 
	                                    </tr>  
	                                    <tr>   
	                                        <td>
	                                            <b>Puesto:</b>
	                                        </td>
	                                        <td>
	                                            <div><b>${PUESTO}</b></div>
	                                        </td> 
	                                    </tr>
	                                    <tr>   
	                                        <td>
	                                            <b>Fecha:</b>
	                                        </td>
	                                        <td>
	                                            <div><b>${FECHA}</b></div>
	                                        </td> 
	                                    </tr>
	                                </tbody>
	                            </table> 
	                        </div>  
	                    </div>
	                </div>
	
	                <div class="clear"></div><br><br>
	
	                <div class="border">  
	                    <div class=" overflow mh500">   
	                        <table class="items tbDuser">
	                            <tbody>
	                                <tr>   
	                                    <td>
	                                        <b>Tipo de Sangre</b>
	                                    </td>
	                                    <td>
	                                        <div><b>${TIPO_SANGRE}</b></div>
	                                    </td> 
	                                </tr>  
	                                <tr>   
	                                    <td>
	                                        <b>Número de Seguro Social</b>
	                                    </td>
	                                    <td>
	                                        <div><b>${NSS}</b></div>
	                                    </td> 
	                                </tr>
	                                <tr>   
	                                    <td>
	                                        <b>¿Tiene alguna alergia?</b>
	                                    </td>
	                                    <td>
	                                        <div><b>${ALERGIA}</b></div>
	                                    </td> 
	                                </tr>
	                                <tr>   
	                                    <td>
	                                        <b>¿Tiene alguna enfermedad crónica?</b>
	                                    </td>
	                                    <td>
	                                        <div><b>${ENFERMEDAD}</b></div>
	                                    </td> 
	                                </tr>
	                                <tr>   
	                                    <td>
	                                        <b>¿Toma algún tratamiento especial?</b>
	                                    </td>
	                                    <td>
	                                        <div><b>${TRATAMIENTO}</b></div>
	                                    </td> 
	                                </tr>
	                                <tr>   
	                                    <td class="activeVerde">
	                                        <b>Contacto para emergencias</b>
	                                    </td>
	                                    <td>
	                                        <div><b>${CONTACTO}</b></div>
	                                    </td> 
	                                </tr>
	                                <tr>   
	                                    <td class="activeVerde">
	                                        <b>Teléfono</b>
	                                    </td>
	                                    <td>
	                                        <div><b>${TEL_CONTACTO}</b></div>
	                                    </td> 
	                                </tr>
	                            </tbody>
	                        </table> 
	                    </div>  
	                </div>
	
	        		<div class="botones"> 
	    				<div class="tleft w50">
	        				<a href="cedula-de-identificacion-personal.htm" class="btn arrowl">
	        					<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
	        					<div>VOLVER AL LISTADO</div>
	        				</a> 
	                    </div>
	
	                    <!-- <div class="tright w50"> 
	                        <a href="#" class="btn arrowr">
	                            <div><img src="${pageContext.request.contextPath}/img/arrowright.png"></div>
	                            <div>SIGUIENTE CÉDULA</div>
	                        </a> 
	                    </div> -->
	    			</div>
	    	    </div>
    	    </c:when>
    	    <c:when test="${datos == 0}">
				<div class="contHome top60">
					<div class="buscar"><img src="${pageContext.request.contextPath}/img/7s/buscar.png"></div>
					<span class="sinDatos"><b> NO SE ENCONTRARON DETALLES PARA CONSULTA.</b></span>
					<div class="tituloScont"><b>No existe cedula actualizada del usuario para mostrar en este momento.</b></div><br>
					
					<div class="botones"> 
						<div class="tleft w50">
	        				<a href="cedula-de-identificacion-personal.htm" class="btn arrowl">
	        					<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
	        					<div>VOLVER AL LISTADO</div>
	        				</a> 
	                    </div>   
					</div>
				</div>
			</c:when>
    	    </c:choose>
        </div>
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    </body>
</html>



