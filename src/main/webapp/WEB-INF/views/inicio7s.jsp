<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="page">
            <div class="contHome">
                <div class="ctitulo">
                    <div class="table">
                    	<div class="cell">
                            <div class="auto">
                                <div class="titulo">
                                    <b>Carpeta Maestra 7S</b>
                                    <img src="${pageContext.request.contextPath}/img/carpetaMaestra.png">
                                </div>
                                <div class="subTitulo">Consultas</div>
                            </div>
                    	</div>
                    </div>
                </div>

                <div class="menuPrincipal">
                    <ul id="accordion" class="accordion">
                        <li>
                            <div class="link">
                                <img src="${pageContext.request.contextPath}/img/seleccion.png" class="icon">
                                <span>Selección</span>
                                <img src="${pageContext.request.contextPath}/img/arrow.png" class="arrow">
                            </div>

                            <ul class="submenu"> 
                                <li><a href="evidencias-fotograficas.htm">Evidencias fotográficas del antes y después</a></li>
                                <li><a href="acta-de-hecho-minucias.htm">Acta de hechos (Catálogo de minucias)</a></li>
                                <li><a href="acta-entrega-expediente.htm">Acta de Entrega para Concentración de Expedientes</a></li>
                                <li><a href="acta-de-hechos-de-depuracion-de-activos-fijos.htm">Acta de hechos de depuración de activos fijos (Catálogo de activos fijos)</a></li>
                                <li><a href="carta-de-asignacion-de-activo-fijo.htm">Carta de asignación de activo fijo</a></li>
                            </ul>
                        </li>

                        <li>
                            <div class="link">
                                <img src="${pageContext.request.contextPath}/img/orden.png" class="icon">
                                <span>Orden</span>
                                <img src="${pageContext.request.contextPath}/img/arrow.png" class="arrow">
                            </div>
                            <ul class="submenu">
                                <li><a href="estandarizacion-por-puesto.htm">Estandarización por Puesto</a></li>
                                <li><a href="copia-de-la-guia-dhl.htm">Copia de guía de DHL de la entrega del paquete operativo</a></li>
                            </ul>
                        </li>
                        <li>
                            <div class="link">
                                <img src="${pageContext.request.contextPath}/img/limpieza.png" class="icon">
                                <span>Limpieza</span>
                                <img src="${pageContext.request.contextPath}/img/arrow.png" class="arrow">
                            </div>
                            <ul class="submenu">
                                <li><a href="programa-de-limpieza.htm">Programa de Limpieza</a></li>
                                <li><a href="reporte-de-area-de-apoyo.htm">Reporte de Áreas de Apoyo</a></li> 
                                <li><a href="bitacora-de-mantenimiento.htm">Bitácora de Mantenimiento</a></li>
                            </ul>
                        </li>

                        <li>
                            <div class="link">
                                <img src="${pageContext.request.contextPath}/img/servicio.png" class="icon">
                                <span>Servicio</span>
                                <img src="${pageContext.request.contextPath}/img/arrow.png" class="arrow">
                            </div>
                            <ul class="submenu">
                                <li><a href="camino-para-atender-mejor.htm">Camino para Atender Mejor</a></li>
                                <li><a href="camino-para-vender-mejor.htm">Camino para Vender Mejor</a></li>
                            </ul>
                        </li>

                        <li>
                            <div class="link">
                                <img src="${pageContext.request.contextPath}/img/seguridad.png" class="icon">
                                <span>Seguridad</span>
                                <img src="${pageContext.request.contextPath}/img/arrow.png" class="arrow">
                            </div>
                            <ul class="submenu">
                                <li><a href="contratacion-masiva.htm">Guía de Atención a Contingencia Concentración Masiva</a></li>
                                <li><a href="guia-de-atencion-falla.htm">Guía de Atención a Contingencia Falla Tecnológica</a></li>
                                <li><a href="guia-de-atencion-a-contigencia.htm">Guía de Atención a Contingencia Fenómeno Geológico</a></li>
                                <li><a href="contigencia-fenomeno.htm">Guía de Atención a Contingencia Fenómeno Hidrometeorológico</a></li>
                                <!-- <li><a href="checklist-de-respuesta-para-contigencias.htm">Checklist de Respuesta para Contingencias</a></li> -->
                            </ul>
                        </li>

                        <li>
                            <div class="link">
                                <img src="${pageContext.request.contextPath}/img/estandarizacion.png" class="icon">
                                <span>Estandarización</span>
                                <img src="${pageContext.request.contextPath}/img/arrow.png" class="arrow">
                            </div>
                            <ul class="submenu">
                                <li><a href="pantalla-actual-punto-de-venta.htm">Foto de la Plantilla Actual del Punto de Venta</a></li>
                                <li><a href="cedula-de-identificacion-personal.htm">Cédula de Identificación Personal</a></li>
                                <li><a href="formato-cumplimiento.htm">Formato de Revisión Metodología 7S</a></li>
                            </ul>
                        </li>

                        <li>
                            <div class="link">
                                <img src="${pageContext.request.contextPath}/img/disciplina.png" class="icon">
                                <span>Disciplina y Salud</span>
                                <img src="${pageContext.request.contextPath}/img/arrow.png" class="arrow">
                            </div>
                            <ul class="submenu">
                                <li><a href="manual-metodologia.htm">Manual Metodologia 7S</a></li>
                                <li><a href="protocolo-de-medicion.htm">Protocolo de Medicion 7S</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
    	</div>

        <!--JQUERY-->
        <script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
        <!--Menu Principal-->
        <script>
            $(function() {
                var Accordion = function(el, multiple) {
                    this.el = el || {};
                    this.multiple = multiple || false;
                    var links = this.el.find('.link');
                    links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
                }

                Accordion.prototype.dropdown = function(e) {
                    var $el = e.data.el;
                        $this = $(this),
                        $next = $this.next();

                    $next.slideToggle();
                    $this.parent().toggleClass('open');

                    if (!e.data.multiple) {
                        $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
                    };
                }
                var accordion = new Accordion($('#accordion'), false);
            });
        </script>
    </body>
</html>



