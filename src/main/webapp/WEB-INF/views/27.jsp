<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/dropkick.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body style="margin-bottom:200px;">
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> ACTA DE ENTREGA PARA CONCENTRACIÓN DE EXPEDIENTE</b></span>
    	</div>
    	<div class="wrapper">
    		<div class="conAnexoDos">

    			<div class="w49a">
    				<div class="borderTres">  
                        <div class=" overflow ">  
		    				<div class="padprevia">
		    					<table  class="tvPrebia w100">
									<tr>
										<td colspan="2">
											<div><img src="${pageContext.request.contextPath}/img/cimbpresora.png" class="imgImB"></div>
											<div class="timprimir">IMPRIMIR</div>
										</td>
									</tr>
									<tr>
										<td colspan="2">
										    <div class="txtImpr"><b>Acta de entrega de concentración de expediente 2 páginas</b></div>
									    </td>
									</tr>
									<tr>
										<td>
											<a href="#" class="Vboton">GUARDAR</a>
										</td>
										<td>
											<a href="#" class="Vboton"><b>IMPRIMIR</b></a>
										</td>
									</tr>
									<tr>
										<td><div class="txtImpr"><b>Destino:</b></div></td>
										<td><b>PDF</b></td>
									</tr>
									<tr>
										<td><div class="txtImpr"><b>Páginas:</b></div></td>
										<td>
											<b>Todo</b>

											<label class="switch m5">
										      <input type="checkbox">
										      <span></span>
										    </label>

										</td>
									</tr>

									<tr>
										<td><div class="txtImpr"><b></b></div></td>
										<td><input type="text" value="Pag. 1-8 " class=" inpIp w100"></td>
									</tr>

									<tr>
										<td colspan="2">
										    <div class="txtImpr"><b>Diseño:</b></div>
											<select class="normal" id="uno">  
											    <option>Diseño</option>
											    <option>Diseño</option>
											    <option>Diseño</option>
										    </select>
									    </td>
									</tr>

									<tr>
										<td colspan="2">
										    <div class="txtImpr"><b>Tamaño de papel:</b></div>
										     <select class="normal" id="dos">  
											    <option>Tamaño de papel</option>
											    <option>Tamaño de papel</option>
											    <option>Tamaño de papel</option>
										    </select>
									    </td>
									</tr>


									<tr>
										<td colspan="2">
										    <div class="txtImpr"><b>Márgenes:</b></div>
										     <select class="normal" id="tres">  
											    <option>Márgenes</option>
											    <option>Márgenes</option>
											    <option>Márgenes</option>
										    </select>
									    </td>
									</tr>
				                </table>
		    				</div>
		    			</div>
		    		</div>
    			</div>



    			<div class="w49b">
    				    <iframe id="iframepdf" src="${pageContext.request.contextPath}/pdf/1.pdf" width="100%" height="560px"></iframe>
    			
    			</div>
    			
    			
    		</div>
    		
    	</div>
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>
    	 <!--inputs de texto-->
		<script src="${pageContext.request.contextPath}/js/7s/jquery.dropkick.js"></script>
		<script>
		jQuery(document).ready(function($) {
			$( ".normal").dropkick({
				mobile: true
			});
		});
		</script>
    </body>
</html>
