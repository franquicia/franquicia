<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE HTML>
<html>
<tiles:importAttribute name="javascripts" />
<head>
<title><tiles:insertAttribute name="title" ignore="true" /></title>

<c:if test="${javascripts != null}">
	<c:forEach var="script" items="${javascripts}">
		<script src="<c:url value="${script}"/>"></script>
	</c:forEach>
</c:if>

<c:if test="${stylesheets != null}">
	<c:forEach var="css" items="${stylesheets}">
		<link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
	</c:forEach>
</c:if>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/calendario.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/dropzone.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/estilos.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/fonts.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/forms.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/login.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/menu.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/modal.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/paginador.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/secciones.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/style.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/datatables.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/responsive.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/pedestalDigital/rowReorder.dataTables.min.css">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/angular.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/Chart.PieceLabel.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/Chart.bundle.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/content_height.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/custom-file-input.js"></script>

<!--Unpload -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/dropzone.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/funciones.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/grafica.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/jquery-ui.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/jquery.dropkick.js"></script>
<!--Select -->
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/jquery.simplemodal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/utils.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/datatables.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/pedestalDigital/bootstrap3-typeahead.js"></script>

</head>

<!-- Se define la estructura de este template, el cual contendra un encabezado y un body. Su contenido se define en el tilesDef.xml -->
<header>
	<div class="franquicias">
		<tiles:insertAttribute name="header" />
	</div>
</header>

<body>
	<div id="cuerpo">
		<tiles:insertAttribute name="body" />
	</div>
</body>
</html>