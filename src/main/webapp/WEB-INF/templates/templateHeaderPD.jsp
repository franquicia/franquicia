<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script type="text/javascript">
	function startTime() {
		var today = new Date();
		var h = today.getHours();
		var m = today.getMinutes();
		m = checkTime(m);
		document.getElementById("reloj").innerHTML = h + ":" + m + " Hrs";
		var t = setTimeout('startTime()', 1000);

		function checkTime(i) {
			if (i < 10) {
				i = "0" + i;
			}
			return i;
		}
	}

	window.onload = function() {
		startTime();
	}

	$(document).ready(function() {
		loadNotificacionCenter(); // Carga centro de notificaciones

		// funcionalidad de tabs de ventana modal de alertas
		$('.contenedorAlerta, .tab2[tab=""]').hide();
		$('.contenedorAlerta[contenedorAlerta="1"]').show();
		$('.tab2').on('click', function() {
			let tab = $(this);
			$('.tab2').removeClass('active');
			tab.addClass('active');
			$('.contenedorAlerta').hide();
			$('.contenedorAlerta[contenedorAlerta="' + tab.attr('tab') + '"]').show();
		});
		$('.notificacionIco').on('click', function() {
			$('.contNotificaciones').toggle();
		});
		$('.contListaNotificaciones li').click(function() {
			$('.contNotificaciones').css('display', 'none');
		});
	});
</script>

<!--Definicion de un encabezado-->
<div class="header">
	<div class="headerMenu">
		<a href="#" id="hamburger" ><img src="../../img/btn_hamburguer.svg" alt="Menú principal" class="imgHamburgesa"></a>
	</div>
	<div class="headerLogo">
		<a href="estatusGeneral.htm"><img src="../../img/logo_baz.svg" id="imgLogo"></a>
	</div>
	<div class="headerUser">
		<div class="name">
			<strong>${userNamePD}</strong>
			<br>
			${userPositionPD}
		</div>
		<div class="pic" style="background-image: url('https://portal.socio.gs/foto/elektra/empleados/${userPicId}.jpg');"></div>
		<div class="user-menu">
			<a href="#"><img src="../../img/btn_desp_menu.svg" class="imgShowMenuUser"></a>
		</div>

		<div class="notificaciones">
			<a href="#" class="notificacionIco">
				<img src="../../img/iconos/notificacionesIco.svg">
				<span class="numNotificaciones" id="numNotifPD">0</span>
			</a>
			<div class="contNotificaciones" style="z-index: 100;">
				<div class="headNot" id="divHeadNot">
					<div>Notificaciones</div><div>0 nuevas</div>
				</div>
				<div class="tabsNotificaciones">
					<div class="tab2 active" tab="1">
						<div>Folios por vencer</div>
					</div>
					<div class="tab2" tab="2">
						<div>Folios nuevos</div>
					</div>
				</div>
				<div class="contenedorAlerta" contenedorAlerta="1">
					<ul class="contListaNotificaciones" id="alertsList"></ul>
				</div>
				<div class="contenedorAlerta" contenedorAlerta="2">
					<ul class="contListaNotificaciones" id="foliosList"></ul>
				</div>
			</div>
		</div>

		<div class="fecha">
			<b>${fechaPD}</b>
			<br>
			<span id="reloj">${horaPD} hrs.</span>
		</div>
		<div class="MenuUser" >
			<div class="MenuUser1">
				<a href="${pageContext.request.contextPath}/central/pedestalDigital/logout.htm" class="selMenu">
					<div>Logout</div>
					<div><img src="${pageContext.request.contextPath}/img/ico5.svg" class="imgConfig"></div>
				</a>
			</div>
		</div>
	</div>
</div>

<div id="effect" class="ui-widget-content ui-corner-all">
	<div id="menuPrincipal">
		<div class="header-usuario">
			<div id="foto">
				<img src="../../img/logo1.svg" class="imgLogo">
			</div>
			<div id="inf-usuario">
				<span>Portal</span><br> Menú Principal<br>
			</div>
		</div>
		<div id="menu">
			<ul class="l-navegacion nivel1">
				<c:if test="${fn:length(userMenu) > 0}">
					<c:forEach items="${userMenu}" var="menuItem" varStatus="loop" >
						<c:if test="${menuItem.idMenu == 2}">
							<li><a href="estatusGeneral.htm"><div><c:out value="${menuItem.descripcion}"></c:out></div></a></li>
						</c:if>
						<c:if test="${menuItem.idMenu == 3}">
							<li><a href="estatusTableta.htm"><div><c:out value="${menuItem.descripcion}"></c:out></div></a></li>
						</c:if>
						<c:if test="${menuItem.idMenu == 4}">
							<li><a href="cargarArchivos.htm"><div><c:out value="${menuItem.descripcion}"></c:out></div></a></li>
						</c:if>
						<c:if test="${menuItem.idMenu == 5}">
							<li><a href="adminArchivos.htm"><div><c:out value="${menuItem.descripcion}"></c:out></div></a></li>
						</c:if>
						<c:if test="${menuItem.idMenu == 12}">
							<li><a href="historicoFolios.htm"><div><c:out value="${menuItem.descripcion}"></c:out></div></a></li>
						</c:if>
						<c:if test="${menuItem.idMenu == 6}">
							<li><a href="reporteria.htm"><div><c:out value="${menuItem.descripcion}"></c:out></div></a></li>
						</c:if>
						<c:if test="${menuItem.idMenu == 11}">
							<li><a href="quejas.htm"><div><c:out value="${menuItem.descripcion}"></c:out></div></a></li>
						</c:if>
					</c:forEach>
				</c:if>
			</ul>
		</div>
	</div>
</div>

<div id="modalNotif" class="modal">
	<div class="cuadro cuadroNotif">
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
		<div class="contenedorModal">
			<img class="modalAlertIco" src="../../img/iconos/alertaIco.svg">
			<div><strong>Detalle del folio</strong></div>
			<br><br>
			<div class="scroll">
				<table class="tblGeneral">
					<thead>
						<tr>
							<th>Número de folio</th>
							<th>Nombre del documento</th>
							<th>Categoría</th>
							<th>Fecha de carga</th>
							<th>Fecha de visualización</th>
							<th>Vigencia</th>
							<th>Tipo de envio</th>
							<th>Visibilidad</th>
						</tr>
					</thead>
					<tbody id="notifData"></tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="clear"></div><br>
</div>

<div id="modalNotif2" class="modal">
	<div class="cuadro cuadroNotif">
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
		<div class="contenedorModal">
			<div><strong>Detalle del folio</strong></div>
			<br><br>
			<div class="scroll">
				<table class="tblGeneral">
					<thead>
						<tr>
							<th>Número de folio</th>
							<th>Nombre del documento</th>
							<th>Categoría</th>
							<th>Fecha de carga</th>
							<th>Vigencia inicial</th>
							<th>Vigencia final</th>
							<th>Tipo de envio</th>
							<th>Visibilidad</th>
						</tr>
					</thead>
					<tbody id="notifData2"></tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="clear"></div><br>
</div>

<div id="modalAllFolios" class="modal">
	<div class="cuadro cuadroG">
		<a href="#" class="simplemodal-close btnCerrar"><img src="../../img/icoCerrar.svg"></a>
		<div class="contenedorModal">
			<img class="modalAlertIco" src="../../img/iconos/alertaIco.svg">
			<div><strong>Folios creados</strong></div>
			<br><br>
			<table class="tblGeneral">
				<thead>
					<tr>
						<th>Documento</th>
						<th>Categoría</th>
						<th>Tiempo/ días restantes</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Circular CNVBV</td>
						<td>Folletos informativos</td>
						<td><strong>1 día</strong></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="clear"></div><br>
</div>