<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div class="clear"></div>
	<div class="titulo">
		<div class="ruta">
			<a href="../inicio.htm">Iniciar sesión</a> / <a href="consultasPD.htm">Consultas</a>
		</div>
		<br> Soporte
	</div>

	<div id="menu1">
		<ul class="Nivel1">
			<li><a href="consultasPD.htm">
					<p style="margin: 0px auto;">Consultas</p>
			</a></li>
			<li>Cecos
				<ul class="Nivel2">
					<li><a href="cecosPDS.htm">
							<p>Administrador de cecos</p>
					</a></li>
					<li><a href="cargaCecosPDS.htm">
							<p>Carga de cecos</p>
					</a></li>
					<li><a href="cecosPDS.htm">
							<p>Cecos 3</p>
					</a></li>
				</ul>
			</li>
			<li>Geografía
				<ul class="Nivel2">
					<li><a href="#">
							<p>Geografía 1</p>
					</a></li>
					<li><a href="cargaGeoPDS.htm">
							<p>Carga por Excel</p>
					</a></li>
					<li><a href="#">
							<p>Geografía 3</p>
					</a></li>
				</ul>
			</li>
			<li>Usuarios
				<ul class="Nivel2">
					<li><a href="#">
							<p>Usuarios 1</p>
					</a></li>
					<li><a href="#">
							<p>Usuarios 2</p>
					</a></li>
					<li><a href="#">
							<p>Usuarios 3</p>
					</a></li>
				</ul>
			</li>
			<li>Catálogo de documentos
				<ul class="Nivel2">
					<li><a href="#">
							<p>Documentos 1</p>
					</a></li>
					<li><a href="#">
							<p>Documentos 2</p>
					</a></li>
					<li><a href="#">
							<p>Documentos 3</p>
					</a></li>
				</ul>
			</li>
			<li>Tabletas
				<ul class="Nivel2">
					<li><a href="#">
							<p>Tabletas 1</p>
					</a></li>
					<li><a href="#">
							<p>Tabletas 2</p>
					</a></li>
					<li><a href="#">
							<p>Tabletas 3</p>
					</a></li>
				</ul>
			</li>
			<li>Consultas
				<ul class="Nivel2">
					<li><a href="soporteParametrosPD.htm">
							<p>Consultas</p>
					</a></li>
					<li><a href="consultaDescargalPD.htm">
							<p>consulta Descargas</p>
					</a></li>
				</ul>
			</li>
			<li>Insert/UPDATE
				<ul class="Nivel2">
					<li><a href="insertUpdateparametrosPD.htm">
							<p>Parametros</p>
					</a></li>
					<li><a href="insertUpdateNegocioPD.htm">
							<p>Negocio</p>
					</a></li>
					<li><a href="insertUpdateCanalPD.htm">
							<p>Canal</p>
					</a></li>
					<li><a href="insertUpdateMenuPerfilPD.htm">
							<p>Menu</p>
					</a></li>
					<li><a href="insertUpdatePerfilPD.htm">
							<p>Perfil</p>
					</a></li>
					<li><a href="insertUpdateEstatusTabletaPD.htm">
							<p>Estatus Tableta</p>
					</a></li>
					<li><a href="insertUpdateTipoDocumentoPD.htm">
							<p>Tipo documento</p>
					</a></li>
					<li><a href="insertUpdateDescargalPD.htm">
							<p>Descarga</p>
					</a></li>
				</ul>
			</li>
		</ul>
	</div>

</body>
</html>