<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="${pageContext.request.contextPath}/css/menuCheck.css" rel="stylesheet" type="text/css" />

<script	src="${pageContext.request.contextPath}/js/script-menu.js"></script>


<div class="margenGeneral">
	<div class="positionGeneral">

		<div class="opcionResumen">
			<form name="formulario" method="POST"	action="/franquicia/central/resumenCheck.htm" id="formActivaAdmin">
				<div class="textCenterMenu" onclick="resumen()">
					<input type="hidden" name="idUserAdmin" value="adminCheckFranquicias">
					<img class="tamImagenMenuCheck"	src="${pageContext.request.contextPath}/images/iconosAdmCheck/iconoResumen.png">
				RESUMEN
				</div>
			</form>
		</div>
		
		<div class="opcionDiseñar">
			<form name="formulario" method="POST"	action="/franquicia/central/checklistDesignF1.htm" id="formEnvia">
				<div class="textCenterMenu" onclick="diseñar()">
					<input type="hidden" name="idChecklist" value="0">
					<img class="tamImagenMenuCheck"	src="${pageContext.request.contextPath}/images/iconosAdmCheck/iconoDisenar.png">
					DISEÑAR
				</div>
			</form>
		</div>
		
		<div class="opcionRecopilar">
			<form name="formulario" method="POST"	action="/franquicia/central/exportaReporte.htm" id="formEnviaReporte">
				<div class="textCenterMenu" onclick="recopilar()">
					<input type="hidden" name="idReporte" value="adminCheckFranquicias">
					<img class="tamImagenMenuCheck"	src="${pageContext.request.contextPath}/images/iconosAdmCheck/iconoRecopilar.png">
					RECOPILAR
				</div>
			</form>
		</div>
		
		<div class="opcionVisualizar">
			<a href="/franquicia/central/visualizador.htm">
			<div class="textCenterMenu">
				<img class="tamImagenMenuCheck"	src="${pageContext.request.contextPath}/images/iconosAdmCheck/iconoOjoAbierto.png">
				VISUALIZAR
			</div>
			</a>
		</div>
	</div>

</div>