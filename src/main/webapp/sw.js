
importScripts('/franquicia/js/pedestalPWA/factoryBD.js');
importScripts('/franquicia/js/pedestalPWA/funcionesPedestal.js');

const CACHE_SATIC = 'static-v1';
const CACHE_DOCUMENTS = 'documentos-v1';

const CACHE_DYNAMIC_NAME = 'dynamic-v1';

const VERSION_BD_2 = 1;
const BD_NAME_2 = 'pedestal-bd';


self.addEventListener('install',e =>{

	console.log('SW: Instalando');
	
	const promesaConexion = abreConexion(BD_NAME_2,VERSION_BD_2)
		.then(async request =>{ 
			
			var ceco;
			
			 await checkData().then(data =>{
				 if(data){
					
					 console.log(data);
					 
					 ceco = data.ceco;
					
				 }else{
					 console.log('No hay usuario insertado');
					 
				 }	 
			 });	
			 
			
			//LLamada a webService para saber los archivos que se tienen que tener 
			//fetch('http://10.53.33.83/franquicia/central/pedestalDigital/getTasksInTablet.json?sucursal=165')
			 
			console.log('El ceco que se instalara sera: '+ceco.substr(2,4));
			
			fetch('/franquicia/central/pedestalDigital/getTasksInTablet.json?sucursal='+ ceco.substr(2,4) )
					.then(response => response.json())
					.then(resObjeto => {
							return workWithJSON(resObjeto);
					}).then(res =>{
						console.log('Termino de ejecutar el proceso de inicializar data: '+res);

						//console.log('Se inicializara el proceso en segundo plano');

						//executeInterval();


					}).catch(console.log);
 		})
		.catch(error =>{
			console.log('Ocurrio un error: '+error);
		});

    const promesaCache = caches.open(CACHE_SATIC)
        .then(cache => {
            return cache.addAll([
            	'/',
            	'/franquicia/central/loginPedestal.html',
				'/franquicia/central/pedestal.html',
				'/franquicia/central/menu.html',
				'/franquicia/central/algoPasoPedestal.html',
				'/franquicia/js/pedestalPWA/manifest.json',
				'/franquicia/css/pedestalPWA/estilos.css',
				'/franquicia/css/pedestalPWA/pedestaliPAD.css',
				'/franquicia/css/pedestalPWA/menu.css',
				'/franquicia/css/pedestalPWA/fonts.css', 
				'/franquicia/css/pedestalPWA/forms.css',
				'/franquicia/css/pedestalPWA/secciones.css',
				'/franquicia/css/pedestalPWA/descarga.css',
				'/franquicia/css/pedestalPWA/modal.css',
				'/franquicia/css/pedestalPWA/avisoPrivacidad-BAZ.css',
				'/franquicia/css/pedestalPWA/bienvenida.css',
				'/franquicia/css/pedestalPWA/datos_de_la_sucursal.css',
				'/franquicia/fonts/otf/Avenir_Next_Regular.otf',
				'/franquicia/fonts/otf/Avenir_Next_Demi_Bold.otf',
				'/franquicia/fonts/otf/Avenir_Next_Bold.otf',
				'/franquicia/fonts/woff/Avenir_Next_Bold.woff',
				'/franquicia/fonts/otf/AvenirNextLTPro-Medium.otf',
				'/franquicia/fonts/ttf/Avenir_Next_Bold.ttf',
				'/franquicia/img/pedestalPWA/logo-baz-lema.svg',
				'/franquicia/img/pedestalPWA/enchufe.svg',
				'/franquicia/img/pedestalPWA/logo_azteca.png',
				'/franquicia/img/pedestalPWA/close.png',
				'/franquicia/img/pedestalPWA/iconosIPAD/condusef.png',
				'/franquicia/img/pedestalPWA/iconosIPAD/comisiones.svg',
				'/franquicia/img/pedestalPWA/iconosIPAD/documentos-legales.png',
				'/franquicia/img/pedestalPWA/iconosIPAD/folletos-informativos.png',
				'/franquicia/img/pedestalPWA/iconosIPAD/mas-buscados.png',
				'/franquicia/img/pedestalPWA/iconosIPAD/licencias.png',
				'/franquicia/img/pedestalPWA/iconosIPAD/aviso-privacidad.png',
				'/franquicia/img/pedestalPWA/iconosIPAD/portabilidad.png',
				'/franquicia/img/pedestalPWA/iconosIPAD/iconoBuzon.svg',
				'/franquicia/img/pedestalPWA/iconosIPAD/iconoBuscar.svg',
				'/franquicia/js/pedestalPWA/app.js',
				'/franquicia/js/pedestalPWA/factoryBD.js',
				'/franquicia/js/pedestalPWA/funcionesPedestal.js',
				'/franquicia/js/pedestalPWA/jquery-1.12.4.js',
				'/franquicia/js/pedestalPWA/jquery-1.12.4.min.js',
				'/franquicia/js/pedestalPWA/jquery.dropkick.js',
				'/franquicia/js/pedestalPWA/jquery-ui.js',
				'/franquicia/js/pedestalPWA/jquery.simplemodal.js',
				'/franquicia/js/pedestalPWA/funciones.js',
				'/franquicia/js/pedestalPWA/pedestaliPad.js',
				'/franquicia/js/pedestalPWA/bienvenida.js',
				'/franquicia/img/pedestalPWA/iconosIPAD/iconoRegresar.png',
				'/franquicia/img/pedestalPWA/iconosIPAD/iconoRegresarBlanco.png',
				'/franquicia/img/pedestalPWA/iconosIPAD/iconoPDF.png'
				
            ]);

		});

		e.waitUntil(Promise.all([promesaCache,promesaConexion]));
		//e.waitUntil(promesaCache);

});


/*self.addEventListener('fetch',e =>{


//2.- Cache with Network FallBack
	const respuestaCache = caches.match(e.request)
		.then(response => {

			if(response) return response;

			//No existe tengo que ir a la web
			return fetch(e.request)
					.then(newResponse => {

						caches.open(CACHE_DYNAMIC_NAME)
							.then(cache => {

								cache.put(e.request, newResponse);
								
							});

                        return newResponse.clone(); // Se ocupa el clone por que estas ocupando nuevamente el response la respuesta del request
                        
					});

		});

    e.respondWith(respuestaCache);
});*/



self.addEventListener('fetch',e =>{
	
	//console.log(e.request.url);

	if(e.request.url.includes('.pdf')){
		
		const promesaPDF = caches.match(e.request).then(response =>{
						
			if(response) return response;
			
			//No existe, ve a la web
			return fetch(e.request).then(newResponse =>{
				caches.open(CACHE_DOCUMENTS).then(cache =>{
					cache.put(e.request,newResponse);
				});

			
				return newResponse.clone();
			})				
			.catch(error =>{
				
				console.log('No se puede ir por el archivo a cache por que esta offline');
				
				return cache.match('/franquicia/central/algoPasoPedestal.html');	 
				
			});
		});
		
		 e.respondWith( promesaPDF );
		 
	}else if(e.request.url.includes('.json') && !e.request.url.includes('manifest.json')){
		
		const promesa = fetch(e.request).then(response => {
	
			return response.clone();
				
		})
		.catch(error => {
				console.log('No es posible realizar la peticion del Web Service...');
				return Promise.reject();
		});
	
		e.respondWith( promesa );
		
	}else{
	

		const promesaCache = caches.open(CACHE_SATIC).then(cache =>{
		
			
			fetch(e.request).then(newResponse =>{
				cache.put(e.request,newResponse);	
			}).catch(error => {
				console.log('No es posible realizar la peticion cuando se esta OFFLINE...');
			});
	
				
			return cache.match(e.request)
				.then(match => {
					if(match){
						return match;
					}else{
						if(e.request.headers.get('accept').includes('text/html')){
							return cache.match('/franquicia/central/algoPasoPedestal.html');	 
						}
					}
				});
		
			
		});
		
		
		e.respondWith(promesaCache);

	}
	
		

});


