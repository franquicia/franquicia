<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>


<!DOCTYPE html>
<html lang="es">
    <head>
    	<meta charset="UTF-8"/>
    	<title>Franquicia</title>
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/modal.css">
    	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/7s/index.css">
    </head>

    <body>
    	<div class="header">
    		<span class="subSeccion">Carpeta Maestra 7S / </span>
    		<span class="tituloSeccion"><b> PROTOCOLO DE MEDICIÓN 7S</b></span>
    	</div>
    	<div class="page">
            <div class="contHome top60">
    			<div class="tsccion">
	    			<div class="tituloBoold"><b>Febrero 2019</b></div>
	    			<div class="sbBoold">Semana1</div>
    		    </div>

                <div class="indicacion tright">
                    <b>Da click en la foto para visualizarla.</b>
                </div><br>

                <div class="tcenter">
                    <div class="superBoton"><b>I. SELECCIÓN</b></div>
                </div><br>

                <div class="border">  
                    <div class=" overflow mh500">   
                        <table class="items smfebrero">
                            <tbody>
                                <tr>   
                                    <td>
                                        <div>
                                            1. En el lugar de trabajo ¿hay sólo artículos que son necesarios para la operación?
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>2%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mactive"></a>
                                    </td>
                                </tr>  
                                <tr>   
                                    <td>
                                        <div>
                                            2. ¿Existen espacios para materiales en “Cuarentena”?
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>1%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mactive"></a>
                                    </td>
                                </tr>  
                                <tr>   
                                    <td>
                                        <div>
                                            3. ¿Existe evidencia de la destrucción de minucias (Acta de hechos y 3 fotos)?
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>2%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mactive"></a>
                                    </td>
                                </tr>  
                                <tr>   
                                    <td>
                                        <div>
                                            4. ¿Existen evidencias de la depuración de activos?
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>1%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mfail"></a>
                                    </td>
                                </tr>
                                <tr>   
                                    <td>
                                        <div>
                                            5. ¿El personal selecciona y resguarda los expedientes correctamente? (Activos, inactivos en Judicial y Cancelados).
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>2%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mactive"></a>
                                    </td>
                                </tr>  
                                <tr>   
                                    <td>
                                        <div>
                                           6. ¿Hay evidencia del envío del paquete operativo?
                                        </div>
                                    </td> 
                                    <td><b>Si</b></td>
                                    <td><b>2%</b></td>
                                    <td>
                                        <a href="#" class="seccionPreguntaUno btnModal mactive"></a>
                                    </td>
                                </tr>    
                            </tbody>
                        </table> 
                    </div>  
                </div>

                <div class="paginador">
                    <a href="#">1</a>
                    <span>/</span> 
                    <a href="#">2</a>
                    <a href="#">
                        <img src="${pageContext.request.contextPath}/img/parrow.png" class="paginadorArroW">
                    </a>
                </div>

                <div class="tright">
                    <div class="superBotonDos"><b>TOTAL POR SECCIÓN 10%</b></div>
                </div>

        		<div class="botones"> 
    				<div class="tleft w50">
        				<a href="protocolo-semana-uno.html" class="btn arrowl">
        					<div><img src="${pageContext.request.contextPath}/img/arrowleft.png"></div>
        					<div>VOLVER AL LISTADO</div>
        				</a> 
                    </div>

                    <div class="tright w50"> 
                        <a href="#" class="btn arrowr">
                            <div><img src="${pageContext.request.contextPath}/img/arrowright.png"></div>
                            <div>SIGUIENTE SECCIÓN</div>
                        </a> 
                    </div> 
    			</div>
            </div>
    	</div>


        <!--MODAL-->
        <div id="PreguntaUno" class="modal">
            <div class="w100">
                <div class="tModal"><b>Selección - Pregunta 1</b></div>
                <div class="cuadrobig">  
                    <a href="#" class="simplemodal-close "><img src="${pageContext.request.contextPath}/img/cerrar.png" class="cerrar"></a> 
                    <img src="${pageContext.request.contextPath}/img/imgDefault.png" class="imgDefault">
                </div>
            </div>
        </div>
        <!-- -->
        <!--JQUERY-->
    	<script src="${pageContext.request.contextPath}/js/7s/jquery.js"></script>

        <!-- MODAL -->
        <script src="${pageContext.request.contextPath}/js/7s/jquery.simplemodal.js"></script>
        <script>
            $(document).ready(function(){
                jQuery(function ($) {
                    $('.seccionPreguntaUno').click(function (e) {
                        setTimeout(function() {
                            $('#PreguntaUno').modal({
                            });
                            return false;
                        }, 1);
                    });
                }); 
            });
        </script>
    </body>
</html>



