// ---------------------------------------------------------
var dom = document.getElementById("graficaInfoTab");
var graficaInfoTab = echarts.init(dom);
var app = {};

var verde1 = 'rgb(42, 99, 61)';
var gris = 'rgb(223, 223, 223)';


option = {
    title: {
        show: true,
        text: 'Rendimiento',
        left: 'middle',
        textAlign: 'center',
        textStyle: {
            fontFamily: 'Avenir_Next_Demi_Bold'
        } 
    },
    legend: {
        show: false,
    },
    series: [
        {
            type: 'pie',
            radius: ['50%', '70%'],
            avoidLabelOverlap: false,
            hoverAnimation: false,
            animationTypeUpdate: false,
            label: {
                normal: {
                    show: true,
                    position: 'center'
                }
            },
            labelLine: {
                normal: {
                    show: false
                },

            },
            data: [
                {
                    value: 80,
                    itemStyle: {color: verde1},
                    name: '80%',
                    label: {
                        fontSize: 40,
                        color: "#000000"
                    }
                },
                {
                    value: 20,
                    itemStyle: {color: gris},
                    
                }
            ]
        }
    ]
};

if (option && typeof option === "object") {
    graficaInfoTab.setOption(option, true);
}

$(".tab").on( "click", function() {   
    graficaInfoTab.resize();
});

window.onresize = function() {
    graficaInfoTab.resize();
};