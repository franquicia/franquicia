
const VERSION_BD = 1;
const BD_NAME = 'pedestal-bd';

const TRANSACTION_WRITE = 'readwrite';
const TRANSACTION_READ = 'readonly';


function abreConexion (nombreBD,version){

    // console.log('Abriendo la conexion ');

    var promiseConexion = new  Promise(function(resolve,reject){

        let request = indexedDB.open(nombreBD,version);
        

        request.onupgradeneeded = e=>{
            // console.log('Actualizacacion de BD');
             creaBD(e);
        }

        request.onsuccess = event =>{
            // console.log('Conexion abierta');
            resolve(event);
        };

        request.onerror = event =>{
            //console.log('Error en la conexion');
            reject('DB error: '+event.target.error);
        };

    });

    return promiseConexion;

}


function creaBD(e){

    let db = e.target.result;
    
    db.createObjectStore('usuario',{
        keyPath: 'id'
    });

    db.createObjectStore('tipo_documentos',{
        keyPath: 'id'
    });

    db.createObjectStore('documentos',{
        keyPath: 'id'
    });
    
    return db;
}

function obtieneTransaccion(tabla,tipo){

    // console.log('Obtienendo Transaccion...')

    return abreConexion(BD_NAME,VERSION_BD).then( event =>{

        //console.log("Tienes conexion a la BD");

        let bd = event.target.result;
    
        var promesaTransaccion = new Promise((resolve,reject)=>{

            let transaccion = bd.transaction(tabla,tipo);
            
            resolve(transaccion);

        });

        return promesaTransaccion;

    });

}

function insertaData(tabla,data){

    // console.log('Empieza a insertar');

    return obtieneTransaccion(tabla,TRANSACTION_WRITE)
        .then(transaction =>{

             // console.log('Voy a empezar a Insertar por que ya tengo la trnsaccion de la tabla');
            //console.log(transaction);

            var promesaInsert = new Promise((resolve,reject)=>{
                
                //Informa sobre el error de la transaccion
                transaction.onerror = event => {
                    //console.log('Error guardado', event.target.error);
                    resolve(event.target.error);
                    
                };

                //Informa sobre el exito de la trsaccion;
                transaction.oncomplete = event=>{
                    //console.log('Transaccion hecha', event);
                    resolve(true);
                };

                let store = transaction.objectStore(tabla);

                for( let objeto of data){
                    var res = store.add(objeto);
                    res.onsuccess =  event =>{
                        //console.log('Nuevo dato agregado a la tabla '+tabla );
                    };
                }


            });

           return promesaInsert;

        });

        
}

function consultaData(tabla){

   return obtieneTransaccion(tabla,TRANSACTION_READ).then(transaction =>{
        
        //console.log(transaction);

        var promesaConsulta = new Promise((resolve,reject)=>{

            let store = transaction.objectStore(tabla);
            
            resolve(store);
            


        });

        return promesaConsulta;
    });
}

function actualizaData(tabla, data){

}

function eliminaData(tabla, id){
	
	return obtieneTransaccion(tabla, TRANSACTION_WRITE)
		.then(transaction =>{
			

           //console.log('Voy a empezar a Eliminar por que ya tengo la trnsaccion de la tabla');
           //console.log(transaction);

           var promesaDelete = new Promise((resolve,reject)=>{
               
               //Informa sobre el error de la transaccion
               transaction.onerror = event => {
                   //console.log('Error guardado', event.target.error);
                   resolve(event.target.error);
                   
               };

               //Informa sobre el exito de la trsaccion;
               transaction.oncomplete = event=>{
                   //console.log('Transaccion hecha', event);
                   resolve(true);
               };

               let store = transaction.objectStore(tabla);
               
               var res = store.delete(id);
               res.onsuccess =  event =>{
            	   //console.log('Eliminado la tabla '+tabla );
                };
               

           });

          return promesaDelete;
			
		});


}









