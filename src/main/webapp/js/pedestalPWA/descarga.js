// CONTADOR TIEMPO

var upgradeTime = 69; //TIEMPO EN SEGUDOS
var seconds = upgradeTime;
function timer() {
  var days        = Math.floor(seconds/24/60/60);
  var hoursLeft   = Math.floor((seconds) - (days*86400));
  var hours       = Math.floor(hoursLeft/3600);
  var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
  var minutes     = Math.floor(minutesLeft/60);
  var remainingSeconds = seconds % 60;

  var minString = ((minutes) + " min ");
  var segString = (pad(remainingSeconds) + " seg ");

  function pad(n) {
    return (n < 10 ? "0" + n : n);
  }

  document.getElementById('countdown').innerHTML = "Su descarga finalizara en " + "<strong>" + "<span id='minutos'>" + (minString) + "</span>" + "<span id='segundos'>" + (segString) + "</span>" + "</strong>";


  if (minutes == 0){
    document.getElementById("minutos").remove();
  }
  if (seconds == 0) {
    clearInterval(countdownTimer);
    document.getElementById('countdown').innerHTML = "Descarga completa";
  } else {
    seconds--;
  }

  // TAMAÑO ARCHIVO

  var tamanoTotal = 120.6; //tamaño total del archivo en MB
  var segundosTotales =  ((minutes)*60)+(remainingSeconds)
  var segundosTranscurridos = ((upgradeTime)-(segundosTotales));
  var tamanoXSegundo = ((tamanoTotal)/(upgradeTime))
  var tamanoActual = ((tamanoXSegundo)*(segundosTranscurridos)).toFixed(1);
  document.getElementById('tamanoArchivo').innerHTML = (tamanoActual)+"/"+(tamanoTotal)+"MB";


  var segundos1Porciento = (100/(upgradeTime));
  var porcientoActual = Math.floor((segundos1Porciento)*(segundosTranscurridos));



// GRAFICA

function run() {
  Array.from(document.querySelectorAll('.circle-meter')).forEach(CircleMeter);
}


function CircleMeter(meterElement) {
  const circle = meterElement.querySelector('svg > circle + circle');


  const numberElement = meterElement.querySelector('.softskills-percentage');
  const score = meterElement.dataset.score;
  const normalizedScore = (100 - meterElement.dataset.score) / 100;

  const maxDuration = 13;

  //set initial stroke offset
  circle.style.strokeDashoffset = 1;
  const duration = Math.floor(Math.random() * Math.floor(maxDuration));

  const transitionEnd = event => {
    circle.removeEventListener('transitionend', transitionEnd);
    meterElement.classList.remove('animatable');
  }


  circle.addEventListener('transitionend', transitionEnd);

  setTimeout(() => {
    meterElement.classList.add('animatable');

    let transitionDuration = window.getComputedStyle(circle).transitionDuration;

    increaseNumber(numberElement, score, duration);
    circle.style.transitionDuration = `${duration}s`;
    circle.style.strokeDashoffset = normalizedScore;

  }, 1000);

}


function increaseNumber(numberElement, score, duration) {
  const startTime = Date.now();

  const callback = function() {
    const timePassed = (Date.now() - startTime) / 1000;
    const currentScore = Math.floor(score * (timePassed / duration), score);
    numberElement.textContent = `${currentScore}%`;

    if (timePassed < duration) {
      requestAnimationFrame(callback);
    }
  }

  requestAnimationFrame(callback);
}

run();

// GRAFICA
$(".circle-gauge").find("a").attr("style","--gauge-value:" + (porcientoActual));

}
var countdownTimer = setInterval('timer()', 1000);

// toastDescarga
  // $('.toastDescarga').delay(800).queue(function(next) {
  //   $(this).addClass('active');
  //   next();
  // });


$(".toastDescarga").find(".cerrar").click(function(){
  $(this).closest(".toastDescarga").removeClass("active");
})
