
$(document).ready(function() {
	// Menu hambuergesa
	$("#effect").toggle(false);
	$("#hamburger").click(function (event) {
		event.stopPropagation();
		 $( "#effect" ).toggle( "slide"); 
	});

	$(document).click(function() {
		$("#effect").toggle(false);
	});
	$("#effect").click (function (event){
		event.stopPropagation();
	}); 

	/*Menu User*/
	$(".MenuUser, .MenuUser1").hide();
	$('.imgShowMenuUser').click(function() {
		$(".MenuUser, .MenuUser1").toggle("ver");
	});

	$(".divRC").hide();
	$('#radioResCivil').on( "change", function() {
				$(".divRC").show();
			});
	$('#radioEmpre').on( "change", function() {
		$(".divRC").hide();
	});
	
	// Menu seguimiento
	$("#seguimiento").toggle(true);
	
	$(".hamburgerSeg").click(function (event) {
		event.stopPropagation();
		 $( ".seguimiento" ).toggle( "slide"); 
	});

	$(document).click(function() {
		$(".seguimiento").toggle(false);
	});
	$(".seguimiento").click (function (event){
		event.stopPropagation();
	});
	/********Para selects*******************/
	$( "select").dropkick({
		mobile: true
	});	
	
	$('.switch').click(function(){
		$(this).toggleClass("switchOn");
	}); 
	
	$( ".cerrarChip" ).click(function() {
	   $(this).parent().parent().remove()
	});


	$('#backIframe').click(function (){
		$("#iframePDF").attr('src','');
	});

	$('.card').click( async function(){

		var categoria = this.getAttribute('value');
		var name_categoria = this.getElementsByClassName('tituloOpciones')[0].innerText

		localStorage.removeItem('tipos_documentos');
		localStorage.removeItem('categoria');
		localStorage.setItem('categoria',name_categoria);

		console.log('Elemento clase card, id: ' +this);

		console.log('Categoria seleccionada '+categoria);

		var array = [];

		await consultaData('tipo_documentos').then(store=>{

			 store.openCursor().onsuccess = function(event){
				var cursor = event.target.result;

				if (cursor) {
					// En cursor.value tenemos el elemento actual
					var current = cursor.value;
					
					if(categoria == current.depende){
						var tipoDocumento = {
							"id": current.id,
							"descripcion": current.descripcion
											};
						

						array.push(tipoDocumento);
					}

					// Pasamos a procesar el siguiente resultado
					cursor.continue();
	
				}else{
					console.log('Ya son todos los registros');
					localStorage.setItem('tipos_documentos', JSON.stringify(array));
					window.location.href = "/franquicia/central/menu.html";
				}
			}
			
		});

		
		

	});
		
});



/*Detecta resolucion de pantalla*/
if (matchMedia) {
  const mq = window.matchMedia("(min-width: 780px)");
  mq.addListener(WidthChange);
  WidthChange(mq);
}
function WidthChange(mq) {
	if (mq.matches) {
	  	$("#menu ul").addClass("normal");
	  	$("#menu ul li").removeClass("in");
		$('ul.nivel1 >li > ul').slideUp();
		$('ul.nivel2 >li > ul').slideUp();
		$('ul.nivel1>li').off("click");
		$('ul.nivel2>li').off("click");
	} else {
	   $("#menu ul").removeClass("normal");

		$('ul.nivel1>li').on('click', function(event) {
			event.stopPropagation();
			
			$target = $(this).children();

			if ($(this).hasClass("in"))  {
			    $('ul.nivel2').slideUp();

				$(this).removeClass("in");
				$('.flecha').removeClass("rotar");
			}else {
			  	$('ul.nivel1 > li').removeClass("in");
				$('ul.nivel2').slideUp();
				$('ul.nivel3').slideUp();
				$('ul.nivel2>li').removeClass("in");
				$(this).addClass("in");
			  	$target.slideDown();
				$('ul.nivel1 > li > a .flecha').addClass("rotar");
				
			}
		});
		$('ul.nivel2>li').on('click', function(event) {
			event.stopPropagation();
		
			$target = $(this).children();

			if ($(this).hasClass("in"))  {
			    $('ul.nivel3').slideUp();
				$(this).removeClass("in");
				$('ul.nivel2 > li > a .flecha').removeClass("rotar");
			}else {
			  	$('ul.nivel2 > li').removeClass("in");
				$('ul.nivel3').slideUp();
				$(this).addClass("in");
			  	$target.slideDown();
				$('ul.nivel2 > li > a .flecha').addClass("rotar");
			}
		});
		$('ul.nivel3>li').on('click', function(event) {
			event.stopPropagation();
		});
	}
}
var allPanels = $('.accordion > dd').hide();

	jQuery('.accordion > dt').on('click', function() {
		$this = $(this);
		//the target panel content
		$target = $this.next();

		jQuery('.accordion > dt').removeClass('accordion-active');
		if ($target.hasClass("in")) {
		  $this.removeClass('accordion-active');
		  $target.slideUp();
		  $target.removeClass("in");

		} else {
		  $this.addClass('accordion-active');
		  jQuery('.accordion > dd').removeClass("in");
		  $target.addClass("in");
			$(".subSeccion").show();

		  jQuery('.accordion > dd').slideUp();
		  $target.slideDown();
		}
	});
	
	var allPanels = $('.accordionM > dd').hide();

	jQuery('.accordionM > dt').on('click', function() {
		$this = $(this);
		//the target panel content
		$target = $this.next();

		jQuery('.accordionM > dt').removeClass('accordion-active');
		if ($target.hasClass("in")) {
		  $this.removeClass('accordion-active');
		  $target.slideUp();
		  $target.removeClass("in");

		} else {
		  $this.addClass('accordion-active');
		  jQuery('.accordionM > dd').removeClass("in");
		  $target.addClass("in");
			$(".subSeccion").show();

		  jQuery('.accordionM > dd').slideUp();
		  $target.slideDown();
		}
	});



// Agregar / Elimina Divs
var contDiv=1;
$('.agregarDiv').click(function(e) {
  contDiv+=1;
  $(".agregaDiv").append( $( "<div class='col4'><a  href='#' class='btnCerrar1 eliminaDiv"+contDiv+"' ></a>"+contDiv+" </div>" ) );
  eliminarFoto2("eliminaDiv"+contDiv);
});

function eliminarFoto2(idElem2){
   $("."+idElem2).click(function (e) {
     $(this).parent().remove();
   });
}



//Tabs
$(".contenedorTab, .tab[tab='']").hide();
$(".contenedorTab[contenedorTab='3']").show();
$(".tab").click(function(){
	let tab = $(this);

	$(".tab").removeClass('active');
	tab.addClass('active');
	$(".contenedorTab").hide();
	$(".contenedorTab[contenedorTab='"+ tab.attr('tab') +"']").show();
});

// -----------------------------------------------
$(".contenedorAlerta, .tab2[tab='']").hide();
$(".contenedorAlerta[contenedorAlerta='1']").show();

$(".tab2").click(function(){
	let tab = $(this);

	$(".tab2").removeClass('active');
	tab.addClass('active');
	$(".contenedorAlerta").hide();
	$(".contenedorAlerta[contenedorAlerta='"+ tab.attr('tab') +"']").show();
});

$(".notificacionIco").on( "click", function() {	 
    $('.contNotificaciones').toggle();
});

$('.contListaNotificaciones li').click(function() {
	$('.contNotificaciones').css("display","none");
});

// Boton descargar
$(".btnDescargar").on( "click", function() {	 
    $('.contenedorMenuDescarga').toggle();
});

$('.contenedorMenuDescarga ul li').click(function() {
	$('.contenedorMenuDescarga').css("display","none");
	$('.toastDescarga').addClass('active');
});





