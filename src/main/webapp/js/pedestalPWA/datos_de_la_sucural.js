$(document).ready(function() {
	
	$('#divCeco').text(localStorage.getItem('id_ceco_login')+' - ' + localStorage.getItem('descripcion_ceco_login'));

	$('.si_instalar').click(function(){
		$(".formilario_ce_co").css({"display":"block"});
		$(".instalar").css({"display":"flex"});

		$(".no_instalar").css({"display":"none"});
		$(".si_instalar").css({"display":"none"});
	});

	$('.no_instalar').click( function(){
		
		let ceco = localStorage.getItem('id_ceco_login');
		let descripcion = localStorage.getItem('descripcion_ceco_login');
		let usuario = localStorage.getItem('descripcion_ceco_login');
				
		var data = [{id : usuario, ceco : ceco, desc: descripcion }];
		
		console.log('Se insertara en la tabla usuario');
		
		insertaData('usuario',data).then(res =>{
			console.log('Se reddirecciona a la pagina principal');
			window.location.replace("/franquicia/central/pedestal.html");
		});
		
	});
	
	$('.instalar').click(function(){

		validaCeco();
				
	});
		
	$(".dato").keypress(key =>{
		
		if (key.keyCode === 13) {
		    
			validaCeco();
			event.preventDefault();
		   
		}
		
	});

});

function validaCeco(){
	
	var texto = $(".dato").val();

	if (texto === "") {
		$(".descripcionUno").text("!No olvides este campo!");
		$(".alertaUno").addClass("error");
	}else{
		$(".dato").addClass("ok");
		$(".alertaUno").removeClass("error");
		
		getDatosCeco(texto);
	}
}

function getDatosCeco(ceco){
	
	console.log('Se consumira el servicio getCeco con el ceco: '+ceco);
		
	fetch('/franquicia/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/getCeco.json?id=191312&ceco='+ceco)
		.then(response => response.json())
		.then(json => {
		
			localStorage.setItem('id_ceco_login',json.idCeco);
			localStorage.setItem('descripcion_ceco_login',json.descCeco);
			
			console.log('Se redirecciona a datos sucursal con ceco: '+ceco);
			window.location.replace("/franquicia/central/datosSucursal.html");
			
		}).catch(error =>{
			
			$(".descripcionUno").text("!El ceco es incorrecto!");
			$(".alertaUno").addClass("error");
			
		});
	
}