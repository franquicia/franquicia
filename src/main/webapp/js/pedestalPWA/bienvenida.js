 
//Valida que se haya iniciado sesion.
checkUser();

$(document).ready(function() {
	
	$('.validar_formulario').click(async function() {
		
		var texto = $(".usuario").val();
		var textoDos = $(".token").val();
		
		var user = false;
		var pwd = false;

		if (texto === "") {
			$(".descripcionUno").text("¡No olvides este campo!");
			$(".alertaUno").addClass("error");
		}else{
			$(".usuario").addClass("ok");
			$(".alertaUno").removeClass("error");
			
			user = true;
		}


		if (textoDos === "" || textoDos.length < 0  ) {
			$(".descripcionDos").text("¡No olvides este campo debe ser de 6 digitos!");
			$(".alertaDos").addClass("error");
		}else{
			$(".token").addClass("ok");
			$(".alertaDos").removeClass("error");
			
			pwd = true;
		}

	
		
		if(pwd && user){
			
			console.log('Se hara la peticion para validar el token');
			
			//Se llama
			await fetch('/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/validaFirmaAzteca.json?id='+texto+'&usuarioAdmin=0&firma='+textoDos+'&key=1')
				.then(response => response.json())
				.then(resObjeto =>{
					
					if(resObjeto){
						
						var dataUsuario =  resObjeto.datosUsuario;
						
						console.log(dataUsuario);
						
						if(dataUsuario){
							localStorage.setItem('id_ceco_login',dataUsuario.ceco);
							localStorage.setItem('descripcion_ceco_login',dataUsuario.descCeco);
							localStorage.setItem('usuario_login',dataUsuario.usuario);							
							
							window.location.replace("/franquicia/central/datosSucursal.html");	
						}else{
							//validar cuando venga en null;
							
							console.log('EL servicio no respondio con datos.');
							
						}

						
					}
					
				}).catch(error=>{
					console.log('Ocurrio un error al querer consumir el servicio (validaFirmaAzteca)');
				});
			
		}
		
    });
});



