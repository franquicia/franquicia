async function checkUser(){
		
	 await checkData().then(res =>{
		 if(res){
			 
			 console.log('Se redirecciona al index');
			 window.location.replace("/franquicia/central/pedestal.html");
			 
		 }else{
			 console.log('Se redirecciona al login');
			 
		 }	 
	 });	
	 	
}

async function workWithJSON(json) {

        if(json.length > 0){

            var contador = 0;
            var objetoAnterior = null;


            while(contador < json.length){

                objetoAnterior = json[contador];
                
                console.log("Categoria: "+objetoAnterior.descCategoria);

                await consultaTipoDoc('tipo_documentos',objetoAnterior.idCategoria).then(async res =>{

                    if(!res){
                        // console.log("Inserta la categoria");

                        var data = [{id: objetoAnterior.idCategoria , descripcion: objetoAnterior.descCategoria , depende: 0}];

                        await insertaData('tipo_documentos',data).then(res =>{
                            console.log("La respuesta del insert es: "+res);
                        });

                    }

                });
               

                while(contador < json.length &&  objetoAnterior.idCategoria == json[contador].idCategoria){

                    console.log("Menu: "+json[contador].nombreDocumento);

                    await consultaTipoDoc('tipo_documentos',json[contador].idTipoDocumento).then(async res =>{

                        if(!res){
                            
                            //Inserta el item del menu
    
                            var data = [{id: json[contador].idTipoDocumento , descripcion: json[contador].nombreDocumento , depende: objetoAnterior.idCategoria}];
    
                            await insertaData('tipo_documentos',data).then(res =>{
                                    console.log("La respuesta del insert es: "+res);
                                });

    
                        }
    
                    });
                    
                    var idDocumentoPedestal= json[contador].idDocumentoPedestal ;
                    console.log("El documento pedestal es: "+ idDocumentoPedestal)
                    
                    await consultaTipoDoc('documentos',idDocumentoPedestal).then(async res =>{

                        if(!res){

                            // Inserta el documento.

                            var path_archivo ='/franquicia/pedestal_digital/cargas/'+json[contador].idCategoria+'/'+json[contador].nombreArchivo;

                            var data = [{id: idDocumentoPedestal, nombre: path_archivo, idTipoDocumento: json[contador].idTipoDocumento}];
                            
    
                            await insertaData('documentos',data).then(async res =>{
                            	
                            	
                            	console.log('El documento que se guardara es:' +path_archivo);
                                //await fetch('http://localhost'+path_archivo).then(async response =>{
                            	await fetch(path_archivo).then(async response =>{

                                    if(response.status === 200){
                                
                                        return caches.open(CACHE_DOCUMENTS)
                                            .then(cache => {
                                                return cache.put(path_archivo, response).then(async e =>{
                                                	
                                                
                                                	//actualiza estado del documento 
                                           		 	await actualizaEstado(idDocumentoPedestal,1);
                                                
                                                });
                                                
                                            });
    
                                    }
                                    /*else{
                                    	//Elimina el registro de IndexBD
                                    	await eliminaData('documentos',idDocumentoPedestal);
                                    	
                                    }*/
                                }).catch( async error =>{
                                	
                                	console.log('No existe el documento ' +path_archivo +' error: '+error );
                                	await eliminaData('documentos',idDocumentoPedestal);
                                	
                                	
                                });

                            });
    
                        }
    
                    });

                    contador++;

                }



            }

         
        }else{
        	console.log("No hay archivos para descargar");
        }
        
   
        //Termina proceso
        return Promise.resolve(true);
    

}

function consultaTipoDoc(tabla,idTipoDoc){

    return consultaData(tabla)
        .then(store =>{

            var promesa = new Promise(resolve=>{
                store.openCursor().onsuccess = function (event) {

                    // console.log("Vamos a buscar el id: " +idTipoDoc);
                    
                    var cursor  = event.target.result;
    
                    if(cursor){
    
                        var data = cursor.value;

                        
                        // console.log("El data.id es: " +data.id);
                        
                        if(data.id == idTipoDoc){
                            //console.log("Se encontro: " +idTipoDoc);
                            resolve(true);                            
                        }
    
                        cursor.continue();
    
                    }else{
                        //  console.log("no hay datos");
                         resolve(false);
                         
                    }
    
                    
                }
    
            });

            return promesa;
        });



}

function consultaDocumento(tabla,idDocumento){

    return consultaData(tabla)
        .then(store =>{

            var promesa = new Promise(resolve=>{
                store.openCursor().onsuccess = function (event) {

                    // console.log("Vamos a buscar el id: " +idTipoDoc);
                    
                    var cursor  = event.target.result;
    
                    if(cursor){
    
                        var data = cursor.value;

                        
                        // console.log("El data.id es: " +data.id);
                        
                        if(data.id == idDocumento){
                            //console.log("Se encontro: " +idDocumento);
                            resolve(true);                            
                        }
    
                        cursor.continue();
    
                    }else{
                        //  console.log("no hay datos");
                         resolve(false);
                         
                    }
    
                    
                }
    
            });

            return promesa;
        });
    
}

var cont = 0
var intervalo;
var ejecutando=false;


function executeInterval(){

	intervalo = setInterval(getData,1000);
	
}

function getData(){

	let fecha = new Date();

	let dia = fecha.getDate();
	let mes = fecha.getMonth();
	let año = fecha.getFullYear();
	let hora= fecha.getHours();
	let minutos = fecha.getMinutes();
	let segundos = fecha.getSeconds();

	console.log(año+'/'+mes+'/'+dia +" "+hora+":"+minutos+":"+segundos);

	cont++;
	if(cont>=300){ //5 minutos durara 
		clearInterval(intervalo);
	}
	
	
	if(ejecutando == false){
		
		console.log('Puedes ejecutar el proceso...');
		
		executeTask(4595);
				
	}else{
		
		console.log('El proceso ya se esta ejecutando...');
		
	}
	

}



function executeTask(sucursal){
	
	ejecutando = true;
	
	getDataSucursal(sucursal)
	.then(res =>{
		console.log('Termine de trabajar con la peticion');
		ejecutando = false; 
		})
	.catch(error => {
		
		ejecutando = false;
		
	} );
	
}


function getDataSucursal(sucursal) {	

	return fetch('/franquicia/central/pedestalDigital/getTasksInTablet.json?sucursal='+sucursal)
		.then(response => response.json())
		.then(resObjeto => {
			
			return workWithJSON(resObjeto);
			
		}).catch(error => {
			
			return Promise.resolve(false);
			
		});
	
}


function actualizaEstado(id,estado){

	
	return fetch('/franquicia/central/pedestalDigital/updateStatusOfTaskInTablet2.json?idDocPedestal='+id+'&estatus='+estado)
		.then(response => response.json())
		.then(resObjeto => {
		
			if(resObjeto.respuesta == 0){
				return promesa = Promise.resolve(true);
			}else{
				return promesa = Promise.resolve(false);
			}
			 
		})
		.catch( error =>{
	
			return promesa = Promise.reject(true);
		
		});



}

function obtieneUltimoSucursal(sucursal,tipoDoc){

	
	return fetch('/franquicia/central/pedestalDigital/getLatestTaskInTablet.json?sucursal='+sucursal+'&tipoDocumento='+tipoDoc)
		.then(response => response.json())
		.then(resObjeto => {
			
			if(resObjeto.lengh > 0){
				var ultimo = resObjeto[0];
				
				console.log(ultimo);
	
				/*if(ultimo.idDocumentoPedestal == 0){
					return promesa = Promise.resolve(true);
				}else{
					return promesa = Promise.resolve(false);
				}*/
				
			}else{
				
				console.log('No hay datos del ultimo');
				return promesa = Promise.resolve(false);
				
			}
			
			
			 
		})
		.catch( error =>{
	
			return promesa = Promise.reject(true);
		
		});



}


 function consultaUsuario(){
	
	return consultaData('usuario')
		.then(store =>{

		var promesa = new Promise(resolve=>{
			
			  store.openCursor().onsuccess = function (event) {

		            var cursor  = event.target.result;

		            if(cursor){
		            
		                var data = cursor.value;
		                
		                console.log(data);
		                
		                resolve(data);
		            }

		            
		        }
			
		});
		
		return promesa;
			
      });
}
 
 async function checkUser(){
		
	 await checkData().then(res =>{
		 if(res){
			 
			 console.log('Se redirecciona al index');
			 window.location.replace("/franquicia/central/pedestal.html");
			 
		 }else{
			 console.log('Se redirecciona al login');
			 
		 }	 
	 });	
	 
	
}


function checkData(){
	
	  return consultaData('usuario')
      .then(store =>{

          var promesa = new Promise(resolve=>{
              store.openCursor().onsuccess = function (event) {

                  var cursor  = event.target.result;
  
                  if(cursor){
  
                      var data = cursor.value;                      
                      resolve(data);          
                                  
                  }else{
                	  
                       resolve(null);
                       
                  }
  
                  
              }
  
          });

          return promesa;
      });

} 

