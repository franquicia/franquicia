
    $('.divOpcionesInicio .card').click(function(){
        $('.divOpcionesInicio .card').removeClass("activeCard");
        $(this).addClass("activeCard");
    });
    $('.documento').click(function(){
        $('.documento').removeClass("documentoSeleccionado");
        $(this).addClass("documentoSeleccionado");
    });

    $(document).ready(function(){
    
        $('.modalVisualizador_view').click(async function (e) {
        	
        	$("#iframePDF").attr('src','');
        	
            var tipoDoc = this.id;

            var documento = '';

            //Llama WS para saber que archivo es el vigente.
            await obtieneUltimoSucursal(4595,tipoDoc)
            	.then(res => {
            		
            	})
            	.catch(error =>{
            		
            		console.log('Error al obetener el utlimo archivo');
            	
            	});
            
            //       
            // Se modifica por el pdf seleccionado 
            await consultaData('documentos').then(store =>{
                store.openCursor().onsuccess = event=>{
                    var cursor = event.target.result;

                    if(cursor){
                        var current = cursor.value;

                        if(current.idTipoDocumento == tipoDoc ){

                            documento = current.nombre;

                            $("#iframePDF").attr('src',documento);

                            $('#modalVisualizador').modal();
                            
                            //console.log('Vamos actualizar a VISTOOOOOO el ID : '+current.id);
                            actualizaEstado(current.id,3)
                            .then(res =>{
                            	if(res){
                            		console.log('Se actualizo con estado 3');
                            	}else{
                            		console.log('No se puede actualizar a estado 3');
                            	}
                            })
                            .catch(error =>{
                            	console.log('No se puede actualizar a estado 3');
                            });
                            
                        }        

                        cursor.continue();

                    }

                };
                
            });

                        
            return false;
        })
        $('.divContactar').css("display", "none");
        

        $('.switch').click(function(){
            $('.divContactar').toggle();
        });
    });

     $('.btnBuzon').click(function(){
        $('.btnBuzon').removeClass("buzonActive");
        $(this).addClass("buzonActive");
     });
    
    


