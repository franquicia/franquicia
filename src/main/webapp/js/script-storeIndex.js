$body = $("body");
$(document).on({
	ajaxStart : function() {
		$body.addClass("loading");
	},
	ajaxStop : function() {
		$body.removeClass("loading");
	}
});


function myFunction() {
	$body.removeClass("loading");
	location.reload();
}

var banderaUsuario = false;
var bandera = true;
var bandera2 = true;
var bandera3 = true;

function comparaCon(x, y){
	flag=true;
	if(x!=y){
		flag=false;
	}
	return flag;
}

function valida(){
	
	banderaUsuario = false;
	bandera = true;
	
	document.getElementById('error').innerHTML = '';
	document.getElementById("usuario").value=document.getElementById("usuario").value.trim();
	if (document.getElementById("usuario").value == null
			|| document.getElementById("usuario").valuelength == 0
			|| /^\s*$/.test(document.getElementById("usuario").value)) {
		document.getElementById('error').innerHTML = 'El campo Usuario debe tener un valor';
		bandera = false;
	} else if (document.getElementById("llave").value == null
			|| document.getElementById("llave").valuelength == 0
			|| /^\s*$/.test(document.getElementById("llave").value)) {
		document.getElementById('error').innerHTML = 'El campo TOKEN debe tener un valor';
		bandera = false;
	} else{
		if(comparaCon(document.getElementById("usuario").value,document.getElementById("usuario2").value.trim()) ||
		
		comparaCon(document.getElementById("usuario").value,document.getElementById("usr2").value.trim())
		){
			document.getElementById('error').innerHTML = 'Los usuarios deben ser distintos';
		}
		else{
			if (!/^([0-9])*$/.test(document.getElementById("usuario").value)){
				alert("El valor del campo usuario debe ser numerico");
		    	document.getElementById("usuario").value = '';
		    	
			}
			else{
				
				if(document.getElementById("usuario").value>=9999999){
					alert("El campo \"usuario\" debe contener el numero de empleado del gerente");
					document.getElementById("usuario").value = '';
				}
				else{
					carga2();
					validaTokenX(document.getElementById("usuario").value , document.getElementById("llave").value);
				}
			}
			
			
		}
		
	}
	 return false;
}



function validaTokenX(usuario, firma){ 
	 var search = {}
	    search["datosUsuario"] = $("usuario").val();
	$.ajax({
		//url :"http://192.168.2.12:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/validaFirmaAzteca.json?id="+usuario+"&usuarioAdmin=331952&firma="+firma+"&key=1",
		url :"http://10.53.33.83/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/validaFirmaAzteca.json?id="+usuario+"&usuarioAdmin=331952&firma="+firma+"&key=1",
	    //url :"http://192.168.2.39:8080/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/validaFirmaAzteca2.json?id="+usuario+"&usuarioAdmin=1&firma="+firma+"&key=1",
	    contentType : "application/json; charset=utf-8",
		dataType : "json",
	    crossDomain: true,
		delimiter : ",",
	    success: function(json) {
	    	  var str = JSON.stringify(json);
	    	  var obj = json.datosUsuario;
        	
	    	  if ( obj == null ){
    		  	bandera = false;
    			document.getElementById('error').innerHTML = 'Usuario o Token Incorrecto';
    			document.getElementById("llave").value = '';
    			carga3();
    		  
    	  } else {
				bandera = true;
				document.getElementById('error').innerHTML = '<img id="palomita" width="25px" src="../images/Palomita_verde.png"/> Token Validado correctamente';
				document.getElementById("token1").value = 'OK';
				document.getElementById("usr").value=document.getElementById("usuario").value;
				document.getElementById("usuario").disabled=true;
				document.getElementById("llave").disabled=true;
				document.getElementById("botonEntrega").disabled=true;
				carga3();
				
    	  }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
	
}
function valida2(){
	bandera2 = true;
	
	document.getElementById('error2').innerHTML = '';

	document.getElementById("usuario2").value=document.getElementById("usuario2").value.trim();
	if (document.getElementById("usuario2").value == null
			|| document.getElementById("usuario2").valuelength == 0
			|| /^\s*$/.test(document.getElementById("usuario2").value)) {
		document.getElementById('error2').innerHTML = 'El campo Usuario debe tener un valor';
		bandera2 = false;
	} else if (document.getElementById("llave2").value == null
			|| document.getElementById("llave2").valuelength == 0
			|| /^\s*$/.test(document.getElementById("llave2").value)) {
		document.getElementById('error2').innerHTML = 'El campo TOKEN debe tener un valor';
		bandera2 = false;
	} else{
		if(comparaCon(document.getElementById("usuario2").value,document.getElementById("usuario").value.trim()) ||
				
				comparaCon(document.getElementById("usuario2").value,document.getElementById("usr").value.trim())  
				){
					document.getElementById('error2').innerHTML = 'Los usuarios deben ser distintos';
		}
		else{
			if (!/^([0-9])*$/.test(document.getElementById("usuario2").value)){
				alert("El valor del campo usuario debe ser numerico");
		    	document.getElementById("usuario2").value = '';
		    	
			}
			else{
				
				if(document.getElementById("usuario2").value>=9999999){
					alert("El campo \"usuario\" debe contener el numero de empleado del regional");
					document.getElementById("usuario2").value = '';
				}
				else{
					carga2();
					validaTokenX2(document.getElementById("usuario2").value , document.getElementById("llave2").value);
				}
			}
		}
	
	}
	
	 return false;
}

function validaTokenX2(usuario, firma){
	 //alert("LLEGO");  
	 var search = {}
	    search["datosUsuario"] = $("usuario").val();
	$.ajax({
		url :"http://10.53.33.83/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/validaFirmaAzteca.json?id="+usuario+"&usuarioAdmin=331952&firma="+firma+"&key=1",

	    contentType : "application/json; charset=utf-8",
		dataType : "json",
	    crossDomain: true,
		delimiter : ",",
	    success: function(json) {
	    	  var str = JSON.stringify(json);
	    	  var obj = json.datosUsuario;
        	
	    	  if ( obj == null ){
        	  	bandera = false;
    			document.getElementById('error2').innerHTML = 'Usuario o Token Incorrecto';
    			document.getElementById("llave2").value = '';
    			carga3();
    		  
    	  } else {
				bandera = true;
				document.getElementById('error2').innerHTML = '<img id="palomita" width="25px" src="../images/Palomita_verde.png"/> Token Validado correctamente';
				document.getElementById("token2").value = 'OK';
				document.getElementById("usr2").value=document.getElementById("usuario2").value;
				document.getElementById("usuario2").disabled=true;
				document.getElementById("llave2").disabled=true;
				document.getElementById("botonEntrega2").disabled=true;
				carga3();
    	  }
       },
       error: function (xhr, status, error) {
           console.log(xhr);
           console.log(status);
           console.log(error);
       }
   });
	
}


function valida3(){
	
	bandera3 = true;
	
	document.getElementById('error3').innerHTML = '';


	if (document.getElementById("usuario3").value == null
			|| document.getElementById("usuario3").valuelength == 0
			|| /^\s*$/.test(document.getElementById("usuario3").value)) {
		document.getElementById('error3').innerHTML = 'El campo Usuario debe tener un valor';
		bandera3 = false;
	} else if (document.getElementById("llave3").value == null
			|| document.getElementById("llave3").valuelength == 0
			|| /^\s*$/.test(document.getElementById("llave3").value)) {
		document.getElementById('error3').innerHTML = 'El campo TOKEN debe tener un valor';
		bandera3 = false;
	} else{
		if(comparaCon(document.getElementById("usuario3").value,document.getElementById("usuario").value.trim()) ||
				comparaCon(document.getElementById("usuario3").value,document.getElementById("usuario2").value.trim()) ||
				comparaCon(document.getElementById("usuario3").value,document.getElementById("usr").value.trim()) ||
				comparaCon(document.getElementById("usuario3").value,document.getElementById("usr2").value.trim()) 
				){
					document.getElementById('error3').innerHTML = 'Los usuarios deben ser distintos';
		}
		else{
			carga2();
			validaTokenX3(document.getElementById("usuario3").value , document.getElementById("llave3").value);
		}
		//carga2();
		//validaTokenX3(document.getElementById("usuario3").value , document.getElementById("llave3").value);
		
		
	}
	 return false;
}

function validaTokenX3(usuario, firma){
	   
	 var search = {}
	    search["datosUsuario"] = $("usuario").val();
	$.ajax({
		url :"http://10.53.33.83/migestion/ejecutaServiciosEncriptados/ejecutaServicio.json?service=servicios/validaFirmaAzteca.json?id="+usuario+"&usuarioAdmin=331952&firma="+firma+"&key=1",

	    contentType : "application/json; charset=utf-8",
		dataType : "json",
	    crossDomain: true,
		delimiter : ",",
	    success: function(json) {
	    	  var str = JSON.stringify(json);
	    	  var obj = json.datosUsuario;
        	
	    	  if ( obj == null ){
        	bandera = false;
  			document.getElementById('error3').innerHTML = 'Usuario o Token Incorrecto';
  			document.getElementById("llave3").value = '';
  			carga3();	
  		  
	  	  } else {
					bandera = true;
					document.getElementById('error3').innerHTML = '<img id="palomita" width="25px" src="../images/Palomita_verde.png" /> Token Validado correctamente';
					document.getElementById("token3").value = 'OK';
					document.getElementById("usr3").value=document.getElementById("usuario3").value;
					document.getElementById("usuario3").disabled=true;
					document.getElementById("llave3").disabled=true;
					document.getElementById("botonEntrega3").disabled=true;
					carga3();
					
	  	  }
      },
      error: function (xhr, status, error) {
          console.log(xhr);
          console.log(status);
          console.log(error);
      }
  });
	
}

function ajax(url,method,data)
{
    var xhr;//ajax object
    data = data || {};//default ajax request
    method = method || 'GET';
    url = url || 'default/url/ajax';
    try
    {//normal browsers
        ret = new XMLHttpRequest();
    }
    catch (error)
    {//older IE browsers, including dead IE6
        try
        {
            ret= new ActiveXObject('Msxml2.XMLHTTP');
        }
        catch(error)
        {
            try
            {
                ret= new ActiveXObject('Microsoft.XMLHTTP');
            }
            catch(error)
            {
                throw new Error('no Ajax support?');
            }
        }
    }
    if (typeof ret !== 'object')
    {//if fails (almost impossible)
        throw new Error('No Ajax, FFS');
    }
    ret.open(method, url, true);//open ajax connection
    ret.setResponseHeader("Access-Control-Allow-Origin", "*");
    ret.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    ret.setRequestHeader('Content-type', 'application/x-www-form-urlencode');
    ret.onreadystatechange = function()
    {
        if (this.readyState === 4 && this.status === 200)
        {
            var response = this.responseText;
            //code you want to see executed when the call was successful
        }
    };
    return ret.send(data);
}



function Hora() {
     var hora = new Date()
     var hrs = hora.getHours();
     var min = hora.getMinutes();
     var hoy = new Date();
     var m = new Array();
     var d = new Array()
     var an= hoy.getYear();
     m[0]="Enero";  m[1]="Febrero";  m[2]="Marzo";
     m[3]="Abril";   m[4]="Mayo";  m[5]="Junio";
     m[6]="Julio";    m[7]="Agosto";   m[8]="Septiembre";
     m[9]="Octubre";   m[10]="Noviembre"; m[11]="Diciembre";
     document.write(" "+hrs+":"+min+" ");
     var fecha = new Date();
     //alert("Día: "+fecha.getDate()+"\nMes: "+(fecha.getMonth()+1)+"\nAño: "+fecha.getFullYear());
     //alert("Hora: "+fecha.getHours()+"\nMinuto: "+fecha.getMinutes()+"\nSegundo: "+fecha.getSeconds()+"\nMilisegundo: "+fecha.getMilliseconds());
   }

function Dia() {
     var hora = new Date()
     var hrs = hora.getHours();
     var min = hora.getMinutes();
     var hoy = new Date();
     var m = new Array();
     var d = new Array()
     var an= hoy.getYear();
     m[0]="Enero";  m[1]="Febrero";  m[2]="Marzo";
     m[3]="Abril";   m[4]="Mayo";  m[5]="Junio";
     m[6]="Julio";    m[7]="Agosto";   m[8]="Septiembre";
     m[9]="Octubre";   m[10]="Noviembre"; m[11]="Diciembre";
     
     document.write(hoy.getDate());
     
   }
function Mes() {
     var hora = new Date()
     var hrs = hora.getHours();
     var min = hora.getMinutes();
     var hoy = new Date();
     var m = new Array();
     var d = new Array()
     var an= hoy.getYear();
     m[0]="Enero";  m[1]="Febrero";  m[2]="Marzo";
     m[3]="Abril";   m[4]="Mayo";  m[5]="Junio";
     m[6]="Julio";    m[7]="Agosto";   m[8]="Septiembre";
     m[9]="Octubre";   m[10]="Noviembre"; m[11]="Diciembre";
     
     document.write(m[hoy.getMonth()]);
   }
function Anio() {
     var hora = new Date()
     var hrs = hora.getHours();
     var min = hora.getMinutes();
     var hoy = new Date();
     var m = new Array();
     var d = new Array()
     var an= hoy.getYear();
     m[0]="Enero";  m[1]="Febrero";  m[2]="Marzo";
     m[3]="Abril";   m[4]="Mayo";  m[5]="Junio";
     m[6]="Julio";    m[7]="Agosto";   m[8]="Septiembre";
     m[9]="Octubre";   m[10]="Noviembre"; m[11]="Diciembre";
     
     document.write(hoy.getFullYear());
   }


function validaExpediente(){
	var flag1=false;
	var flag2=false;
	var flag3=false;
	var flag4=false;
	var flag5=false;
	var flag6=false;
	var cad="Los campos";
	var flags= new Array(4);
	
	validarSiNumero(document.getElementById("numRegional").value);
	

	if (document.getElementById("ciudad").value == null
			|| document.getElementById("ciudad").valuelength == 0
			|| /^\s*$/.test(document.getElementById("ciudad").value)) {
		
		flag1=false;
		flags[0]=false;
		cad=cad+" ciudad,"
	} 
	else{
		flag1=true;
		flags[0]=true;
	}
	if (document.getElementById("nomRegional").value == null
			|| document.getElementById("nomRegional").valuelength == 0
			|| /^\s*$/.test(document.getElementById("nomRegional").value)) {
		
		flag2=false;
		flags[1]=false;
		cad=cad+" nombre del Regional,"
	} 
	else{
		flags[1]=true;
		flag2=true;
	}
	if (document.getElementById("numRegional").value == null
			|| document.getElementById("numRegional").valuelength == 0
			|| /^\s*$/.test(document.getElementById("numRegional").value)) {
		
		flag3=false;
		flags[2]=false;
		cad=cad+" número de credencial del regional,"
	} 
	else{
		flags[2]=true;
		flag3=true;
	}
	
	if (document.getElementById("descripcion").value == null
			|| document.getElementById("descripcion").valuelength == 0
			|| /^\s*$/.test(document.getElementById("descripcion").value)) {
		
		flags[3]=false;
		cad=cad+" descripcion,"
	} 
	else{
		flags[3]=true;
	}
	cad=cad+" No pueden ser vacios."
	var flagA=true;
	for (i = 0; i < flags.length; i++) {
	    if(flags[i]==false){
	    	flagA=false;
	    }
	}
	
	if(flagA==false){
    	alert(cad);
    	return false;
    }
    else{
    	carga2();
    	carga();
    	return true;
    }
	
	
	 
}

function printDiv(nombreDiv) {
    var contenido= document.getElementById(nombreDiv).innerHTML;
    var contenidoOriginal= document.body.innerHTML;

    document.body.innerHTML = contenido;

    window.print();

    document.body.innerHTML = contenidoOriginal;
    muestra_oculta("btImprimir");
    muestra_oculta("botones");
    return false;
}

function muestra_oculta(id){
	if (document.getElementById){ 
		var el = document.getElementById(id); 
		el.style.display = (el.style.display == 'none') ? 'block' : 'none'; 
	}
	return false;
}

function recupera(){
	var radios = document.getElementsByTagName('input');
	var value;
	var flag=false;
	for (var i = 0; i < radios.length; i++) {
	    if (radios[i].type === 'radio' && radios[i].checked) {
	        // get value, set checked flag or do whatever you need to
	        value = radios[i].value;
	        flag=true;
	    }
	}
	if(!flag){
		alert("Debes seleccionar un id de expediente!!!");
		return false;
	}else{
		document.getElementById("idExpediente").value=value;
		return true;
	}
}

function validaFirmas(){
	var flags= new Array(2);
	
	if (document.getElementById("token1").value == null
			|| document.getElementById("token1").valuelength == 0
			|| /^\s*$/.test(document.getElementById("token1").value)) {
		flags[0]=false;
		document.getElementById('error').innerHTML = 'Usuario no logueado';
	}
	
	if (document.getElementById("token2").value == null
			|| document.getElementById("token2").valuelength == 0
			|| /^\s*$/.test(document.getElementById("token2").value)) {
		flags[1]=false;
		document.getElementById('error2').innerHTML = 'Usuario no logueado';
	}
	/*
	if (document.getElementById("token3").value == null
			|| document.getElementById("token3").valuelength == 0
			|| /^\s*$/.test(document.getElementById("token3").value)) {
		flags[2]=false;
		document.getElementById('error3').innerHTML = 'Usuario no logueado';
	}
	*/
	var flagA=true;
	for (i = 0; i < flags.length; i++) {
	    if(flags[i]==false){
	    	flagA=false;
	    }
	}
	
	if(flagA==false){
    	alert("Debes firmar el documento!!!");
    	return false;
    }
    else{
    	carga2();
    	carga();
    	return true;
    }
}

function carga() {
	 setTimeout("document.getElementById('cargando').style.display= 'none'", 3000);
	}
function carga2() {
	//alert("Entro");
	 document.getElementById('cargando').style.display= 'Block';
	}
     
function carga3() {
	document.getElementById('cargando').style.display= 'none';
	}

function validarSiNumero(numero){
	if (!/^([0-9])*$/.test(numero)){
		alert("El valor del \"campo número de credencial\" debe ser numerico");
    	document.getElementById("numRegional").value = '';
    	
	}
	else{
		var aux=""+document.getElementById("numRegional").toString();
		if(document.getElementById("numRegional").value>=9999999){
			alert("El campo \"número de credencial\" debe contener el numero de empleado del regional(6 digitos)");
			document.getElementById("numRegional").value = '';
		}
	}
	return false;
}


function validarSiNumero2(numero){
    if (!/^([0-9])*$/.test(numero)){ 
      alert("El valor del campo numero de credencial del representante legal debe ser numerico");
      document.getElementById("numLegal").value = '';
    }
    return false;
  }

function validarSiNumeroUSUARIO(numero){
	if (!/^([0-9])*$/.test(numero)){
		alert("El valor del campo usuario debe ser numerico");
    	document.getElementById("usuario").value = '';
    	
	}
	if(document.getElementById("numRegional").value>=9999999){
		alert("El campo \"usuario\" debe contener el numero de empleado");
		document.getElementById("usuario").value = '';
	}
	return false;
}

function validarSiNumeroUSUARIO2(numero){
	if (!/^([0-9])*$/.test(numero)){
		alert("El valor del campo usuario debe ser numerico");
    	document.getElementById("usuario2").value = '';
    	
	}
	if(document.getElementById("numRegional").value>=9999999){
		alert("El campo \"usuario\" debe contener el numero de empleado");
		document.getElementById("usuario2").value = '';
	}
	return false;
}

function validarSiNumeroUSUARIO3(numero){
	if (!/^([0-9])*$/.test(numero)){
		alert("El valor del campo usuario debe ser numerico");
    	document.getElementById("usuario").value = '';
    	
	}
	if(document.getElementById("numRegional").value>=9999999){
		alert("El campo \"usuario\" debe contener el numero de empleado");
		document.getElementById("usuario").value = '';
	}
	return false;
}


function validarSiNumero5(numero){
	if (!/^([0-9])*$/.test(numero)){
		alert("El valor del \"campo número de credencial\" debe ser numerico");
    	document.getElementById("numeroUsr").value = '';
    	
	}
	else{
		var aux=""+document.getElementById("numeroUsr").toString();
		if(document.getElementById("numeroUsr").value>=9999999){
			alert("El campo \"número de credencial\" debe contener el numero de empleado que recibe(6 digitos)");
			document.getElementById("numeroUsr").value = '';
		}
	}
	return false;
}

function validarSiLetra(numero){
	if (!/^[a-zA-ZÀ-ÿ\u00f1\u00d1 _]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/g.test(numero)){
		alert("El valor del \"campo nombre de usuario que recibe\" debe contener solo letras");
    	document.getElementById("nombreUsr").value = '';
    	
	}
	return false;
}

function validaArchiveros(){
	var flags= new Array(2);
		
	var flag1=false;
	var flag2=false;
	var flag3=false;
	var flag4=false;
	var flag5=false;
	var flag6=false;
	var cad="Los campos";
	var flags= new Array(4);
	
	if (document.getElementById("nombreUsr").value == null
			|| document.getElementById("nombreUsr").valuelength == 0
			|| /^\s*$/.test(document.getElementById("nombreUsr").value)) {
		
		flag1=false;
		flags[0]=false;
		cad=cad+" nombre de Usuario que recibe,"
	} 
	else{
		flag1=true;
		flags[0]=true;
	}
	/*if (document.getElementById("numeroUsr").value == null
			|| document.getElementById("numeroUsr").valuelength == 0
			|| /^\s*$/.test(document.getElementById("numeroUsr").value)) {
		
		flag2=false;
		flags[1]=false;
		cad=cad+" número de Usuario que recibe,"
	} 
	else{
		flags[1]=true;
		flag2=true;
	}*/
	
	if (document.getElementById("PlacaBD").value == null
			|| document.getElementById("PlacaBD").valuelength == 0
			|| /^\s*$/.test(document.getElementById("PlacaBD").value)) {
		
		alert("Debe agregar al menos un activo a la lista");
		return false;
		flag1=false;
	}
	else{
		flag1=true;
	}
	cad=cad+" no pueden ser vacío."
	var flagA=true;
	for (i = 0; i < flags.length; i++) {
	    if(flags[i]==false){
	    	flagA=false;
	    }
	}
	if(flag1==false){
		return false;
	}else{
		if(flagA==false){
			alert(cad);
			return false;
		}
		else{
			carga2();
			carga();
			return true;
		}
	}
	
}



