var pdDropzone;
var _maxFilezSize = 0.0;

$(document).ready(function() {

	var _dkTipoEnvio = new Dropkick('#tipoEnvio');
	var previewNode = document.querySelector('#onyx-dropzone-template');
	previewNode.id = '';

	var previewTemplate = previewNode.parentNode.innerHTML;
	previewNode.parentNode.removeChild(previewNode);

	Dropzone.autoDiscover = false;
	Dropzone.autoProcessQueue = false;
	Dropzone.parallelUploads = 12;

	var pdTarget = $('#pdDropzone').get(0);
	pdDropzone = new Dropzone(pdTarget, {
		autoDiscover: false,
		autoProcessQueue: false,
		parallelUploads: 12,
		url: ($(pdTarget).attr('action')) ? $(pdTarget).attr('action') : '/franquicia/central/pedestalDigital/upload/multiFileUploadPD.htm', // Check that our form has an action attr and if not, set one here
        maxFiles: 12,
		maxFilesize: 20,
		acceptedFiles: '.pdf', // '.pdf,.png,.jpg,.jpeg,.mp4', // acceptedFiles: 'image/*,application/pdf', // acceptedFiles: "image/*,application/pdf,.doc,.docx,.xls,.xlsx,.csv,.tsv,.ppt,.pptx,.pages,.odt,.rtf",
		previewTemplate: previewTemplate,
		previewsContainer: '#previews',
		clickable: false,
		createImageThumbnails: true,
		uploadMultiple: true,

		// The text used before any files are dropped.
		dictDefaultMessage: 'Arrastra el archivo al recuadro.', // Default: Drop files here to upload

		// The text that replaces the default message text it the browser is not supported.
		dictFallbackMessage: 'Tu navegador no soporta esta funci\u00F3n.', // Default: Your browser does not support drag'n'drop file uploads.

		// If the filesize is too big.
		// '{{filesize}}' and '{{maxFilesize}}' will be replaced with the respective configuration values.
		dictFileTooBig: 'El archivo es demasiado grande ({{filesize}}MB). El tama\u00F1o m\u00E1ximo es de {{maxFilesize}}MB.', // Default: File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.

		// If the file doesn't match the file type.
		dictInvalidFileType: "No puedes subir archivos con este formato, intenta nuevamente con otro archivo diferente.", // Default: You can't upload files of this type.

		// If the server response was invalid.
		// '{{statusCode}}' will be replaced with the servers status code.
		dictResponseError: "Server responded with {{statusCode}} code.", // Default: Server responded with {{statusCode}} code.

		// If 'addRemoveLinks' is true, the text to be used for the cancel upload link.
		dictCancelUpload: "Cancelar carga de archivos.", // Default: Cancel upload

		// The text that is displayed if an upload was manually canceled
		dictUploadCanceled: "Carga de archivos cancelada.", // Default: Upload canceled.

		// If 'addRemoveLinks' is true, the text to be used for confirmation when cancelling upload.
		dictCancelUploadConfirmation: "\u00BFEst\u00E1s seguro de que quieres interrumpir la carga del archivo?", // Default: Are you sure you want to cancel this upload?

		// If 'addRemoveLinks' is true, the text to be used to remove a file.
		dictRemoveFile: "Quitar archivo", // Default: Remove file

		// If this is not null, then the user will be prompted before removing a file.
		dictRemoveFileConfirmation: null, // Default: null

		// Displayed if 'maxFiles' is st and exceeded.
		// The string '{{maxFiles}}' will be replaced by the configuration value.
		dictMaxFilesExceeded: "Ya no puedes agregar mas archivos.", // Default: You can not upload any more files.

		// Allows you to translate the different units. Starting with 'tb' for terabytes and going down to 'b' for bytes.
		dictFileSizeUnits: {tb: "TB", gb: "GB", mb: "MB", kb: "KB", b: "b"}
	});

	pdDropzone.on('addedfile', function(file) {
		// console.log('DropzoneJS - addedfile');

		var fileType = file.name.split('.')[file.name.split('.').length - 1];

		if (fileType != 'pdf' && fileType != 'jpeg' && fileType != 'jpg' && fileType != 'png' && fileType != 'mp4') {
			this.removeFile(file);
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'La extensi\u00F3n del archivo no es v\u00E1lida. Solo se pueden agregar Im\u00E1genes en formatos JPG y PNG, V\u00EDdeos en formato MP4 y archivos PDF, por favor intente nuevamente.' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}

		var reader = new FileReader();
		reader.onload = function(event) {
			// console.log(event.target.result);
			dzDataList.push({
				filetype: event.target.result.split(';')[0].split(':')[1],
				filesize: event.total,
				data: event.target.result
			});
			$('#vistaPreviaImg').attr('src', event.target.result);
			console.log('Preview added.');
		};
		reader.readAsDataURL(file);

		// valida onyx-dropzone-info
			// Se obtiene el data y se guarda en un DOM oculto
		
		// Calcula el total de los archivos...
		_maxFilezSize += parseFloat((file.size / (1024 * 1024)));

		/**
		 * Se valida el tipo de carga seleccionado, para mostrar o no la alerta
		 * Carga instantanea: 20MB
		 * Batch / Calendarizada: 80MB
		 */

		// IF: tipo de carga - Instantaneo y _maxFilezSize <= 20.0
			// Se permite realizar la carga
		// ELSE IF: tipo de carga - Batch y _maxFilezSize <= 80.0
			// Se permite realizar la carga
		// ELSE IF: tipo de carga - Calendarizado y _maxFilezSize <= 80.0
			// Se permite realizar la carga
		// ELSE: 
			// Alerta - Tamaño máximo alcanzado.

		if (_maxFilezSize <= 20.0) {
			if (_dkTipoEnvio.selectedIndex == 0) {
				_dkTipoEnvio.select(2);
			}

			$('.preview-container').css('visibility', 'visible');
			file.previewElement.classList.add('type-' + file.name.split('.')[1] /**base.fileType(file.name)*/ ); // Add type class for this element's preview
			$(".no-files-uploaded").slideUp("easeInExpo");

			if (_dkTipoEnvio.selectedIndex != 2) {
				_dkTipoEnvio.select(1);
				_dkTipoEnvio.disable(0, true);
				_dkTipoEnvio.disable(2, true);

				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Ha sobrepasado el l\u00EDmite de 20MB permitido para cargas instant\u00E1neas.' +
						'Se ha seleccionado la opci\u00F3n Batch por defecto, para cargas mayores a 20MB solo se pueden de tipo Batch o Calendarizadas' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
			}
		} else if (_maxFilezSize > 20.0 && _maxFilezSize <= 80.0) {
			$('.preview-container').css('visibility', 'visible');
			file.previewElement.classList.add('type-' + file.name.split('.')[1] /**base.fileType(file.name)*/ ); // Add type class for this element's preview
			$(".no-files-uploaded").slideUp("easeInExpo");

			if (_dkTipoEnvio.selectedIndex != 1 || _dkTipoEnvio.selectedIndex != 3) {
				_dkTipoEnvio.select(1);
			}
		}

		/**
		else {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Uno o m\u00E1s archivos no fueron agregados, por que sobrepasa el m\u00E1ximo permitido de MB para carga de archivos.' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
		}
		*/

		if (_maxFilezSize > 80.0 && (_dkTipoEnvio.selectedIndex != 1 || _dkTipoEnvio.selectedIndex != 3)) {
			this.removeFile(file);
			// _maxFilezSize -= parseFloat((file.size / (1024 * 1024)));

			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Uno o m\u00E1s archivos no fueron agregados, por que sobrepasa el m\u00E1ximo permitido de MB para carga de archivos.' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
		} else {
			$('.preview-container').css('visibility', 'visible');
			file.previewElement.classList.add('type-' + file.name.split('.')[1] /**base.fileType(file.name)*/ ); // Add type class for this element's preview
			$(".no-files-uploaded").slideUp("easeInExpo");
		}
	});

	pdDropzone.on('removedfile', function(file) {
		if (_maxFilezSize > 0) {
			_maxFilezSize -= parseFloat((file.size / (1024 * 1024)));
		}
	});

	pdDropzone.on('uploadprogress', function(file, progress, bytesSent) {
		if (file.previewElement) {
			console.log('uploadprogress: ' + progress + '%');
		}
	});

	pdDropzone.on("maxfilesexceeded", function(file) {
		// console.log('El archivo ' + file.upload.filename + ' fue eliminado, por que sobrepasa el n\u00FAmero m\u00E1ximo de items en el Dropzone.');
		this.removeFile(file);
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Uno o m\u00E1s archivos no fueron agregados, ya que sobrepasan el n\u00FAmero m\u00E1ximo de archivos permitidos' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
	});

	pdDropzone.on('complete', function(file) {
		$('#pdDropzone')[0].dropzone.files.forEach(function(file) {
			if (file.status === 'success') {
				file.previewElement.remove();
			} else if (file.status === 'uploading') {
				console.log(file.upload.filename + ' is uploading...');
			} else {
				console.log('El archivo ' + file.upload.filename + ' no fue eliminado.');
			}
		});

		$('#pdDropzone').removeClass('dz-started');
	});

	pdDropzone.on('successmultiple', function(a, b) {
		console.log('successmultiple');

		try {
			var json = JSON.parse(b);
			if (json.idFolio != null && json.idFolio != undefined && json.idFolio != '0') {
				$('#listaLanzamientos').html('');
				Dropzone.forElement("#pdDropzone").removeAllFiles(true); // clear all files
				_maxFilezSize = 0;
				hideLoadingSpinner();

				var confirmAlert = Swal.mixin({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Se realiz\u00F3 la carga de archivos de manera exitosa.<br>' +
						'El folio es: <strong>' + json.idFolio + '</strong>' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});

				confirmAlert.fire().then((result) => {
					setTimeout(function() {
						location.reload();
					}, 500);
				});
			} else {
				$('#listaLanzamientos').html('');
				hideLoadingSpinner();
				var confirmAlert = Swal.mixin({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo realizar la carga del archivo' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});

				confirmAlert.fire().then((result) => {
					setTimeout(function() {
						location.reload();
					}, 500);
				});
			}
		} catch (e) {
			console.log(e);
		}
	});

	$('#btnEnviarDocs').on('click', function() {
		console.log('DropzoneJS - processQueue');

		// Validar si se encuentra marcado el check de vigencia final
		var _setVF = $('#checkVF').is(':checked');
		var _fechaVF = $('#dpVigenciaFinal').val();
		if (_setVF && _fechaVF == '') {
			Swal.fire({
				html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
				'Debe especificar una fecha de vigencia final antes de continuar.' +
				'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}

		var response = validateDocsBeforeUpload();
		if (response.haltProcess) {
			// TODO: Se detiene proceso;
			// Se muestra VENTANA MODAL con información de los documentos que devolvió el servicio
			// Se muestran dos opciones
			// 1.- Detener proceso
			// 2.- Continuar proceso, pero antes se deben cancelar todos los folios de los documentos mostrados en la alerta 
			var list = response.data;
			if (list.length > 0) {
				var x, str = '';
				for (x = 0; x < list.length; x++) {
					// console.log(list[x]);
					str += '<tr id="' + list[x].fiid_documento + '" style="display: inline-table; width: 100%;">' +
					'<td style="width: 16%;"><a href="#">' + list[x].fifolio + '</a></td>' +
					'<td style="width: 16%;">' + list[x].fcnombre_docto + '</td>' +
					'<td style="width: 16%;">' + list[x].desc_categoria + '</td>' +
					'<td style="width: 16%;">' + list[x].fctipo_envio + '</td>' +
					'<td style="width: 16%;">' + list[x].fdvigencia_ini + '</td>' +
					'<td style="width: 16%;">' + list[x].fcvisible_suc + '</td>' + '</tr>';
				}

				$('#listDocsBU').html(str);
				$('#btnCancelProcess').on('click', cancelLoadProcess);
				$('#btnCancelDocs').on('click', cancelDocs);
				$('#docsBUTitle').html('Detalle del folio');
				$('#modalDocsBU').modal({
					escClose: false,
				});
			}

			return false;
		} else {
			// function que obtiene los valores del contenedor de destinos
			saveDistribData();
			// function que valida el tipo de envio
			saveEnvData();
			if (validaDocs && validaEnv && validaDistrib) {
				$('#modalCarga2 .btnCerrar').click();
				showLoadingSpinner();
				pdDropzone.processQueue();
			} else {
				if (!validaDocs) {
					Swal.fire({
						html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No se pudo guardar la informaci\u00F3n de los documentos, intente nuevamente.' + '</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}
				if (!validaEnv) {
					Swal.fire({
						html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No se pudo guardar la informaci\u00F3n de los destinos, intente nuevamente.' + '</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}
				if (!validaDistrib) {
					Swal.fire({
						html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No se pudo guardar la informaci\u00F3n del tipo de envio, intente nuevamente.' + '</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}
			}
		}
	});

	$('#btnRemoveFiles').click(function () {
		var confirmAlert = Swal.mixin({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>¡Atención!</strong></div>' +
				'\u00BFEst\u00E1 seguro que quiere remover todos los archivos?' +
				'</div>',
			showCloseButton: false,
			showCancelButton: true,
			cancelButtonText: 'No',
			confirmButtonText: 'Si',
			confirmButtonColor: '#006341'
		});

		confirmAlert.fire().then((result) => {
			if (result.value) {
				Dropzone.forElement("#pdDropzone").removeAllFiles(true);
				_dkTipoEnvio.select(0);
				_dkTipoEnvio.disable(0, false);
				_dkTipoEnvio.disable(1, false);
				_dkTipoEnvio.disable(2, false);
				_dkTipoEnvio.disable(3, false);
				_maxFilezSize = 0;
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				return false;
			}
		});
	});
});