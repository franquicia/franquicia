angular.module('app', [])
	.controller('validarForm', ['$scope', function($scope) {
		$scope.tab = 1;
		$scope.setTab = function(newTab) {
			$scope.tab = newTab;
		};
		$scope.isSet = function(tabNum) {
			return $scope.tab === tabNum;
		};
	}])
	.controller('tabs', ['$scope', function($scope) {
		$scope.tab1 = 1;
		$scope.setTab1 = function(newTab1) {
			$scope.tab1 = newTab1;
		};
		$scope.isSet1 = function(tabNum1) {
			return $scope.tab1 === tabNum1;
		};
	}]);

	$(document).ready(function() {
		$('#effect').toggle(false);
	
		$(document).click(function() {
			$('#effect').toggle(false);
			$('#nivel2CargaArchivos').toggle(false);
			$('#nivel2AdminEnvios').toggle(false);
		});
	
		$('#effect').click(function(event) {
			event.stopPropagation();
		});
	
		$('#nivel2CargaArchivos').toggle(false);
		$('#nivel2AdminEnvios').toggle(false);
	
		$('#nivel2-1').click(function(event) {
			$('#nivel2CargaArchivos').toggle('slide');
			$('#nivel2AdminEnvios').toggle(false);
			event.stopPropagation();
		});
	
		$('#nivel2-2').click(function(event) {
			$('#nivel2CargaArchivos').toggle(false);
			$('#nivel2AdminEnvios').toggle('slide');
			event.stopPropagation();
		});
	
		$('#hamburger').click(function(event) {
			event.stopPropagation();
			$('#effect').toggle("slide");
		});
	
		// Modal resolución
		$('.modalDetalle_view').click(function(e) {
			$('#modalDetalle').modal();
			return false;
		});
	
		$('.modalEliminar_view').click(function(e) {
			$('#modalEliminar').modal();
			return false;
		});
	
		/**
		$('.modalPais_view').click(function(e) {
			$('#modalPais').modal();
			return false;
		});
		*/
	
		$('.modalEnviar_view').click(function(e) {
			$('#modalEnviar').modal();
			return false;
		});
	
		$('.modaPDFPrev_view').click(function(e) {
			$('#modaPDFPrev').modal();
			return false;
		});
	
		// Eliminar archivo de historial de archivos
		$('.tblHistorial tr td img.eliminarAccion').click(function(e) {
			$(this).parent().parent().parent().remove();
		});
	
		// Eliminar atchivo de historial de archivos
		$('.simplemodal-close').click(function(e) {
			$(this).parent().remove();
		});
	
		/*Menu User*/
		$('.MenuUser, .MenuUser1').hide();
		$('.imgShowMenuUser').click(function() {
			console.log('imgShowMenuUser - showLogout');
			$(".MenuUser, .MenuUser1").toggle("ver");
		});
	
		/********Para selects*******************/
		$("select").dropkick({
			mobile: true
		});
	
		$("#datepickerAdminEnv").datepicker({
			firstDay: 1,
			onSelect: function(date, datepicker) {
				// Inhabilita campo 'Buscar por folio' si se selecciona una fecha
				if ($('#buscarPorFolio').attr('disabled') === undefined) {
					$('#buscarPorFolio').prop('disabled', true);
				}
	
				var exist = false;
				var val = $('#datepickerAdminEnv').val();
				var splitDate = val.split('/');
				var ddMMyyyy = splitDate[0] + splitDate[1] + splitDate[2];
				var list = $('#listaLanzamientos').find('.divEliminar');
	
				if (list.length > 0) {
					$(list).each(function(index, _obj) {
						var id = $(_obj).attr('id');
						if (id.includes('F-.-.-')) {
							exist = true;
							if (id.includes(ddMMyyyy)) {
								return false;
							} else {
								var _a = $(_obj).find('a');
								_obj.id = 'F-.-.-' + ddMMyyyy;
								$(_a).html('Fecha de envío: ' + val);
								return false;
							}
						}
					});
	
					if (!exist) {
						$('#listaLanzamientos').prepend(
								'<div id="F-.-.-' + ddMMyyyy + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1% 5px 1%;">' +
								'<a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">Fecha de envío: ' + val + '</a></div>'
						);
					}
				}
			}
		});
});

/**********************************************************************/
/** Registra plug-in para mostrar siempre el tooltip usando Chart.js **/
/**********************************************************************/

Chart.pluginService.register({
	beforeRender: function(chart) {
		if (chart.config.options.showAllTooltips) {
			// create an array of tooltips
			// we can't use the chart tooltip because there is only one tooltip per chart
			chart.pluginTooltips = [];
			chart.config.data.datasets.forEach(function(dataset, i) {
				chart.getDatasetMeta(i).data.forEach(function(sector, j) {
					chart.pluginTooltips.push(new Chart.Tooltip({
						_chart: chart.chart,
						_chartInstance: chart,
						_data: chart.data,
						_options: chart.options.tooltips,
						_active: [sector]
					}, chart));
				});
			});

			// turn off normal tooltips
			chart.options.tooltips.enabled = false;
		}
	},
	afterDraw: function(chart, easing) {
		if (chart.config.options.showAllTooltips) {
			// we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
			if (!chart.allTooltipsOnce) {
				if (easing !== 1)
					return;
				chart.allTooltipsOnce = true;
			}

			// turn on tooltips
			chart.options.tooltips.enabled = true;
			Chart.helpers.each(chart.pluginTooltips, function(tooltip) {
				tooltip.initialize();
				tooltip.update();
				tooltip.pivot();

				var _title = tooltip._model.title ? tooltip._model.title[0] : 'Mes';
				tooltip._model.title[0] = _title.substring(0, 3);
				tooltip._model.width = 45;
				tooltip._model.data = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
				tooltip.transition(easing).draw();
			});
			chart.options.tooltips.enabled = false;
		}
	}
});

/**********************************************************************/

function isValidDate(dateString) {
	// https://stackoverflow.com/a/6178341
	// First check for the pattern
	if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
		return false;

	// Parse the date parts to integers
	var parts = dateString.split("/");
	var day = parseInt(parts[0], 10);
	var month = parseInt(parts[1], 10);
	var year = parseInt(parts[2], 10);

	// Check the ranges of month and year
	if (year < 1000 || year > 3000 || month == 0 || month > 12)
		return false;

	var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

	// Adjust for leap years
	if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
		monthLength[1] = 29;

	// Check the range of the day
	return day > 0 && day <= monthLength[month - 1];
}

// Valida buscador del menu de hamburgesa
function validaBusquedaPD(f) {
	if (f.busca.value == "") {
		alert("Es necesario que introduzca un valor");
	} else {
		return false;
	}
}

// Agregar
var contAdjunto1 = 0;
$('.agregarRenglon').click(function(e) {
	contAdjunto1 += 1;
	$(".divAdjuntar").append($("<div class='adjunto'><div><input type='text' placeholder='Nombre'></div><div><input type='file' id='carga" + contAdjunto1 + "' class='inputfile carga'/><label for='carga" + contAdjunto1 + "' class='btnAgregaFoto'><span>Adjuntar documento </span></label></div><div><img src='img/delete.svg' class='ico carga" + contAdjunto1 + "'></div></div>"));
	declararEventoAdjuntarFileInputs("carga" + contAdjunto1);
	eliminarFoto("carga" + contAdjunto1);
});

// Eliminar
function eliminarFoto(idElem) {
	$("." + idElem).click(function(e) {
		$(this).parent().parent().remove();
	});
}

// Agregar tr a tabla
var contAdjuntotabla = 0;
$('.agregarTr').click(function(e) {
	contAdjuntotabla += 1;
	$(".tblGeneral").append($("<tr><td><input type='text'></td><td><input type='text'></td><td><input type='text'></td><td><img src='img/icoEditar.svg'></td><td><img src='img/delete.svg' class='cargatbl" + contAdjuntotabla + "'></td></tr>"));
	eliminarFoto1("cargatbl" + contAdjuntotabla);
});

// Eliminar
function eliminarFoto1(idElem1) {
	$("." + idElem1).click(function(e) {
		$(this).parent().parent().remove();
	});
}

//Agregar / Elimina Divs
var contDiv = 1;
$('.agregarDiv').click(function(e) {
	contDiv += 1;
	$(".agregaDiv").append($("<div class='col4'><a  href='#' class='btnCerrar1 eliminaDiv" + contDiv + "' ></a>" + contDiv + " </div>"));
	eliminarFoto2("eliminaDiv" + contDiv);
});

function eliminarFoto2(idElem2) {
	$("." + idElem2).click(function(e) {
		$(this).parent().remove();
	});
}

// Seleccionar Checkbox
/**
$("#checktodos").change(function() {
	$("input:checkbox.checkM").prop('checked', $(this).prop("checked"));
});
*/

// CHIP
$(".chip input[type='checkbox']").change(function() {
	if ($(this).is(":checked")) {
		$(this).parent().addClass("chipActive");
	} else {
		$(this).parent().removeClass("chipActive");
	}
});

//Tag-it
$(".chipClose .closebtn").click(function() {
	this.parentElement.style.display = 'none';
});

function showLoadingSpinner() {
	$('.blackScreen').show();
	$('.loaderContainer').show();
}

function hideLoadingSpinner() {
	setTimeout(function() {
		$('.blackScreen').hide();
		$('.loaderContainer').hide();
	}, 500);
}

var allPanels = $('.accordion > dd').hide();
jQuery('.accordion > dt').on('click', function() {
	$this = $(this);
	//the target panel content
	$target = $this.next();
	jQuery('.accordion > dt').removeClass('accordion-active');
	if ($target.hasClass("in")) {
		$this.removeClass('accordion-active');
		$target.slideUp();
		$target.removeClass("in");
	} else {
		$this.addClass('accordion-active');
		jQuery('.accordion > dd').removeClass("in");
		$target.addClass("in");
		$(".subSeccion").show();
		jQuery('.accordion > dd').slideUp();
		$target.slideDown();
	}
});

var allPanels = $('.accordionM > dd').hide();
jQuery('.accordionM > dt').on('click', function() {
	$this = $(this);
	//the target panel content
	$target = $this.next();
	jQuery('.accordionM > dt').removeClass('accordion-active');
	if ($target.hasClass("in")) {
		$this.removeClass('accordion-active');
		$target.slideUp();
		$target.removeClass("in");
	} else {
		$this.addClass('accordion-active');
		jQuery('.accordionM > dd').removeClass("in");
		$target.addClass("in");
		$(".subSeccion").show();
		jQuery('.accordionM > dd').slideUp();
		$target.slideDown();
	}
});

//Detecta resolucion de pantalla
if (matchMedia) {
	const mq = window.matchMedia("(min-width: 780px)");
	mq.addListener(WidthChange);
	WidthChange(mq);
}

function WidthChange(mq) {
	if (mq.matches) {
		$("#menu ul").addClass("normal");
		$("#menu ul li").removeClass("in");
		$('ul.nivel1 >li > ul').slideUp();
		$('ul.nivel2 >li > ul').slideUp();
		$('ul.nivel1>li').off("click");
		$('ul.nivel2>li').off("click");
	} else {
		$("#menu ul").removeClass("normal");
		$('ul.nivel1>li').on('click', function(event) {
			event.stopPropagation();
			$target = $(this).children();
			if ($(this).hasClass("in")) {
				$('ul.nivel2').slideUp();
				$(this).removeClass("in");
				$('.flecha').removeClass("rotar");
			} else {
				$('ul.nivel1 > li').removeClass("in");
				$('ul.nivel2').slideUp();
				$('ul.nivel3').slideUp();
				$('ul.nivel2>li').removeClass("in");
				$(this).addClass("in");
				$target.slideDown();
				$('ul.nivel1 > li > a .flecha').addClass("rotar");
			}
		});
		$('ul.nivel2>li').on('click', function(event) {
			event.stopPropagation();
			$target = $(this).children();
			if ($(this).hasClass("in")) {
				$('ul.nivel3').slideUp();
				$(this).removeClass("in");
				$('ul.nivel2 > li > a .flecha').removeClass("rotar");
			} else {
				$('ul.nivel2 > li').removeClass("in");
				$('ul.nivel3').slideUp();
				$(this).addClass("in");
				$target.slideDown();
				$('ul.nivel2 > li > a .flecha').addClass("rotar");
			}
		});
		$('ul.nivel3>li').on('click', function(event) {
			event.stopPropagation();
		});
	}
}

/**********************************/
// Excel upload and convert to JSON

function uploadExcelsData(formId) {
	var fileUpload = document.getElementById(formId); // Reference the FileUpload element.
	var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/; // Validate whether File is valid Excel file.

	if (regex.test(fileUpload.value.toLowerCase())) {
		var fileName = fileUpload.value;
		if (typeof(FileReader) != "undefined") {
			var reader = new FileReader();

			// For Browsers other than IE.
			if (reader.readAsBinaryString) {
				reader.onload = function(e) {
					processExcel(e.target.result, fileName);
				};
				reader.readAsBinaryString(fileUpload.files[0]);
			} else {
				//For IE Browser.
				reader.onload = function(e) {
					var data = "";
					var bytes = new Uint8Array(e.target.result);
					for (var i = 0; i < bytes.byteLength; i++) {
						data += String.fromCharCode(bytes[i]);
					}
					processExcel(data, fileName);
				};
				reader.readAsArrayBuffer(fileUpload.files[0]);
			}
		} else {
			alert('Este navegador no soporta esta funci\u00F3n.');
			hideLoadingSpinner();
		}
	} else {
		alert('Favor de subir un archivo de Excel v\u00E1lido.');
		hideLoadingSpinner();
	}
}

function processExcel(data, fileName) {
	//Read the Excel File data.
	var workbook = XLSX.read(data, {
		type: 'binary'
	});

	var json = '';
	var extra = '';
	var _fileName = fileName.split('\\')[2].split('.')[0];

	try {
		json = '{';
		json += '"nombreArchivo": "' + _fileName + '",';
		json += '"etapas": [';

		for (var x = 0; x < workbook.SheetNames.length - 1; x++) {
			var sheet = workbook.SheetNames[x]; // Fetch the name a Sheet
			var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheet]); // Read all rows from First Sheet into an JSON array.

			json += '{';
			json += '"etapa": "' + sheet + '",';
			json += '"datos": [';

			for (var i = 0; i < excelRows.length; i++) {
				json += '{';

				json += '"nEconomico": "' + excelRows[i]['NO. ECONOMICO'] + '",';
				json += '"nombreSucursal": "' + excelRows[i]['NOMBRE SUCURSAL'] + '",';
				json += '"numSerieTableta": "' + excelRows[i]['NO. SERIE TABLETA'] + '",';
				json += '"fechaInstalacion": "' + excelRows[i]['FECHA DE INSTALACION'] + '",';

				if (excelRows[i]['FALLA DETECTADA'] === 'Sin falla') {
					json += '"fallaDetectada": "Sin falla",';
					json += '"fechaFalla": "",';
					json += '"folioFalla": "",';
					json += '"fechaCorreccion": "",';
					json += '"responsableSoporte": ""';
				} else {
					json += '"fallaDetectada": "' + excelRows[i]['FALLA DETECTADA'] + '",';
					json += '"fechaFalla": "' + excelRows[i]['FECHA DE FALLA'] + '",';
					json += '"folioFalla": "' + excelRows[i]['FOLIO DE FALLA'] + '",';
					json += '"fechaCorreccion": "' + excelRows[i]['FECHA CORRECCIÓN'] + '",';
					json += '"responsableSoporte": "' + excelRows[i]['RESPONSABLE SOPORTE'] + '"';
				}

				if ((excelRows.length - i) === 1) {
					json += '}';
				} else if (i < excelRows.length) {
					json += '},';
				}
			}

			json += ']';

			if ((workbook.SheetNames.length - 1 - x) === 1) {
				json += '}';
			} else if (x < workbook.SheetNames.length) {
				json += '},';
			}
		}

		json += ']';
		json += '}';
	} catch (e) {
		// Se oculta la animación de carga
		hideLoadingSpinner();

		// En caso de que ocurra un error al leer el archivo de Excel se imprime el error
		console.log(e);

		// Se muestra alerta al usuario
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se pudo leer el archivo de Excel.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
	}

	// Obtiene datos de hoja 'Extra'
	try {
		var _sheet = workbook.SheetNames[workbook.SheetNames.length - 1]; // Fetch the name a Sheet
		var _excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[_sheet]); // Read all rows from First Sheet into an JSON array.

		extra = '{';
		extra += '"extra": [';

		for (var x = 0; x < _excelRows.length; x++) {
			extra += '{';

			extra += '"etapa": "' + _excelRows[x]['Etapa'] + '",';
			extra += '"descripcion": "' + _excelRows[x]['Descripcion'] + '",';
			extra += '"instalaciones": "' + _excelRows[x]['Instalaciones'] + '"';

			if ((_excelRows.length - x) == 1) {
				extra += '}';
			} else if (x < _excelRows.length) {
				extra += '},';
			}
		}

		extra += ']}';
		// console.log(extra);
	} catch (e) {
		// Se oculta la animación de carga
		hideLoadingSpinner();

		// En caso de que ocurra un error al leer el archivo de Excel se imprime el error
		console.log(e);

		// Se muestra alerta al usuario
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se pudo leer el archivo de Excel.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
	}

	loadExcelsData(json, extra);
}

function loadExcelsData(json, extra) {
	var _json = JSON.parse(json);
	var totalTabletas = 1851;

	// Imprime etapas
	// console.log(_json.etapas);

	// Imprime la última etapa...
	// console.log(_json.etapas[_json.etapas.length - 1]);

	/** Calcular datos **/

	// Calcula total de tabletas instaladas

	var tabletasCount = 0;
	var etapasLength = _json.etapas.length;
	for (var i = 0; i < etapasLength; i++) {
		tabletasCount += _json.etapas[i]['datos'].length;
	}

	/** Asigna valores **/

	// Total de tabletas instaladas...
	console.log('Total de tabletas instaladas: ' + tabletasCount);
	$('#totalTabletas').html('<strong>' + tabletasCount + '</strong>');

	// Inicio de instalaciones...
	console.log('Inicio de instalaciones: ' + '10/10/2018');
	$('#inicioInstalaciones').html('Inicio de instalaciones:<br><b>' + '10/10/2018' + '</b>');

	// Última instalación...
	console.log('Última instalación: ' + '15/02/2019');
	$('#ultimaInstalacion').html('Última instalación:<br><b>' + '15/02/2019' + '</b>');

	// Avance total de instalación...
	console.log('Avance total de instalación: ' + parseFloat(tabletasCount/totalTabletas).toFixed(2) * 100 + '%');
	$('#porcentajeAvance').html('<strong>' + parseFloat(tabletasCount/totalTabletas).toFixed(2) * 100 + '%' + '</strong>');

	// Total final de instalaciones...
	console.log('Total final de instalaciones: ' +  totalTabletas);
	$('#totalFinal').html('Total final de instalaciones:<br><b>' + totalTabletas + '</b>');

	// Asigna nombre de la última etapa
	console.log('Etapa actual: ' + 'Etapa 3');
	$('#titleEtapaN').html('Etapa 3');

	// Asigna sucursales de etapa
	console.log('Sucursales de etapa: ' + 500);
	$('#etapaNVal').html(500);

	// Avance de etapa
	console.log('Avance de etapa: ' + parseFloat(4/500) * 100 + '%');
	$('#etapaNPorc').html(parseFloat(4/500) * 100 + '%');

	// hide loading gif
	// makeGraphic();
	hideLoadingSpinner();
}

function readCSVFile(formId) {
	var fileUpload = document.getElementById(formId); // Reference the FileUpload element.
	var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv)$/; // Validate whether File is valid Excel file.

	if (regex.test(fileUpload.value.toLowerCase())) {
		var fileName = fileUpload.value;
		if (typeof(FileReader) != "undefined") {
			var reader = new FileReader();

			// For Browsers other than IE.
			if (reader.readAsBinaryString) {
				reader.onload = function(e) {
					try {
						var foo = e.target.result; // .replace('\n', '');
						foo = foo.includes(',') ? foo.split(',') : foo.split('\n');
						foo = foo.filter(ceco => ceco.length > 0);

						if ((foo.length == 1 && foo[0].includes('')) || foo.length < 1) {
							Swal.fire({
								html: '<div style="width: 100%;">' +
									'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
									'El archivo no contiene sucursales o se encuentra vacio.' +
									'</div>',
								showCloseButton: false,
								confirmButtonText: 'Aceptar',
								confirmButtonColor: '#006341'
							});
							hideLoadingSpinner();
							return false;
						}

						if (formId === 'cargaCSV') {
							processCSV(foo.toString(), fileName);
						} else if (formId === 'cargaSucursalesFolio') {
							processCargaSucursalesFolio(foo.toString() /* e.target.result */ , fileName);
						} else {
							envioCSV(foo.toString() /* e.target.result */ , fileName);
						}
					} catch (e) {
						Swal.fire({
							html: '<div style="width: 100%;">' +
								'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
								'El archivo no contiene sucursales o se encuentra vacio.' +
								'</div>',
							showCloseButton: false,
							confirmButtonText: 'Aceptar',
							confirmButtonColor: '#006341'
						});
						hideLoadingSpinner();
						return false;
					}
				};
				reader.readAsBinaryString(fileUpload.files[0]);
			} else {
				//For IE Browser.
				reader.onload = function(e) {
					var data = "";
					var bytes = new Uint8Array(e.target.result);
					for (var i = 0; i < bytes.byteLength; i++) {
						data += String.fromCharCode(bytes[i]);
					}
					if (formId === 'cargaCSV') {
						processCSV(data, fileName);
					} else if (formId === 'cargaSucursalesFolio') {
						processCargaSucursalesFolio(data, fileName);
					} else {
						envioCSV(data, fileName);
					}
				};
				reader.readAsArrayBuffer(fileUpload.files[0]);
			}
		} else {
			alert('Este navegador no soporta esta funci\u00F3n.');
			hideLoadingSpinner();
		}
	} else {
		alert('Favor de subir un archivo en formato CSV.');
		hideLoadingSpinner();
	}
}

function processCSV(data, fileName) {
	//Read the Excel File data.
	var workbook = XLSX.read(data, {
		type: 'binary'
	});

	var json = '';
	var extra = '';
	var _fileName = fileName.split('\\')[2].split('.')[0];
	var listName = $('#listName').val();

	if (listName.trim().lenght < 1) {
		alert('Debe especificar un nombre antes de continuar.');
		return false;
	}

	try {
		var sheet = workbook.SheetNames[0]; // Fetch the name a Sheet
		var excelRows = XLSX.utils.make_csv(workbook.Sheets[sheet]); // Read all rows from First Sheet into an JSON array.
		var csvString = excelRows.substring(0, excelRows.length - 1);
		var restList = '';

		while (csvString.includes('\n')) {
			// console.log('split');
			csvString = csvString.split('\n');
		}

		// TODO: Validar que la lista de CSV que se va a mandar al servicio no sea mayor a 700
		// De ser asi se debe dividir en bloques de 700 elementos

		for (var x = 0; x < csvString.length; x++) {
			if (csvString[x].length === 2) {
				restList += '00' + csvString[x] + ',';
			} else if (csvString[x].length === 3) {
				restList += '0' + csvString[x] + ',';
			} else {
				restList += csvString[x] + ',';
			}
		}

		// Se quita la ultima coma de la cadena de caracteres
		restList = restList.substring(0, restList.length - 1);
		restList = restList.trim();

		var _negocio = $('#dk0-listNegocio .dk-option-selected').attr('data-value');

		if (_negocio === undefined || _negocio === null) {
			alert('_negocio is null or undefined');
			hideLoadingSpinner();
			return false;
		}

		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneCecosPorCSV.json',
			dataType: 'json',
			data: {
				csvList: restList,
				negocio: _negocio // TODO: Obtener negocio
			},
			success: function(data) {
				// console.log(data);

				$('#modalDistribList #theList').html('');
				$('#modalDistribList #theList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalDistribList #theList #listTable').append('<tr><th style="text-align: left;" class="valid" ><div class="listBox"><input type="checkbox" name="grupoDistribList" id="checkBoxTodos1" class="checkM" /><label for="checkBoxTodos1" >Seleccionar todo</label></div></th></tr>');

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (data[x].activo === 1) {
						_html += '<tr><td style="text-align: left;" class="valid"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoDistribList" id="' + data[x].idCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '" >' + '(' + data[x].idCeco + ') ' + data[x].nombre + '</label></div></td></tr>';
					} else {
						_html += '<tr><td style="text-align: left; background-color: #FF9A9A; pointer-events: none;" class="noValid" ><div class="listBox">';
						_html += '<input type="checkbox" name="grupoDistribList" id="' + data[x].idCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '" >' + '(' + data[x].idCeco + ') ' + data[x].nombre + '</label></div></td></tr>';
					}

					$('#modalDistribList #theList #listTable').append(_html);
				}

				$('#modalDistribList #listNameModal').val($('#listName').val());

				$('#modalDistribList #btnCancelarList').on('click', function() {
					closeCSVModalCA(); // cancelDocumentData();
				});

				$('#modalDistribList #btnGuardarList').on('click', function() {
					saveDistributionListByCSV();
				});

				$("#modalDistribList #checkBoxTodos1").change(function() {
					// $('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
					$('.valid input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				$('#modalDistribList #distribTitle').html('Crear lista de distribuci\u00F3n');
				$('#checkBoxTodos1').click();
				$('#modalDistribList').modal();
			},
			error: function(obj, errorText) {
				console.log('No se pudo insertar el registro.');
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	} catch (e) {
		// Se oculta la animación de carga
		hideLoadingSpinner();

		// En caso de que ocurra un error al leer el archivo de Excel se imprime el error
		console.log(e);

		// Se muestra alerta al usuario
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se pudo leer el archivo CSV.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
	}

	// loadExcelsData(json, extra);
}

function envioCSV(data, fileName) {
	//Read the Excel File data.
	var workbook = XLSX.read(data, {
		type: 'binary'
	});

	var json = '';
	var extra = '';
	var _fileName = fileName.split('\\')[2].split('.')[0];

	try {
		var sheet = workbook.SheetNames[0]; // Fetch the name a Sheet
		var excelRows = XLSX.utils.make_csv(workbook.Sheets[sheet]); // Read all rows from First Sheet into an JSON array.
		var csvString = excelRows.substring(0, excelRows.length - 1);
		var restList = '';
		var _cecoList = '';

		if (csvString.includes('\n')) {
			restList = csvString.split('\n');

			for (var x = 0; x < restList.length; x++) {
				if (restList[x].length > 1) {
					if (restList[x].length == 2) {
						_cecoList += '00' + restList[x] + ',';
					} else if (restList[x].length == 3) {
						_cecoList += '0' + restList[x] + ',';
					} else {
						_cecoList += restList[x] + ',';
					}
				}
			}

			// Se quita la ultima coma de la cadena de caracteres
			_cecoList = _cecoList.substring(0, _cecoList.length - 1);
			_cecoList = _cecoList.trim();
		} else {
			if (csvString.length == 2) {
				_cecoList += '00' + csvString;
			} else if (csvString.length == 3) {
				_cecoList += '0' + csvString;
			} else {
				_cecoList += csvString;
			}
		}

		// Obtiene negocio
		var _negocio = $('#negocio').val();
		if (_negocio === undefined || _negocio === null) {
			alert('_negocio is null or undefined');
			hideLoadingSpinner();
			return false;
		}

		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneCecosPorCSV.json',
			dataType: 'json',
			data: {
				csvList: _cecoList,
				negocio: _negocio // TODO: Obtener negocio
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'El archivo CSV no devolvi\u00F3 datos, valida los datos e intenta nuevamente' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#cargaCSVModal .titModal').html('Crear lista por carga de CSV');
				$('#cargaCSVModal #theList').html('');
				$('#cargaCSVModal #theList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#cargaCSVModal #theList #listTable').append('<tr><th style="text-align: left;" class="valid" ><div class="listBox"><input type="checkbox" name="grupoDistribList" id="checkBoxTodos2" class="checkM" /><label for="checkBoxTodos2" >Seleccionar todo</label></div></th></tr>');

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (data[x].activo === 1) {
						_html += '<tr><td style="text-align: left;" class="valid" ><div class="listBox">';
						_html += '<input type="checkbox" name="grupoDistribList" id="' + data[x].idCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '" >' + '(' + data[x].idCeco + ') ' + data[x].nombre + '</label></div></td></tr>';
					} else {
						_html += '<tr><td style="text-align: left; background-color: #FF9A9A; pointer-events: none;" class="noValid" ><div class="listBox">';
						_html += '<input type="checkbox" name="grupoDistribList" id="' + data[x].idCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '" >' + '(' + data[x].idCeco + ') ' + data[x].nombre + '</label></div></td></tr>';
					}

					$('#cargaCSVModal #theList #listTable').append(_html);
				}

				$('#cargaCSVModal #listNameModal').val(_fileName);
				$('#cargaCSVModal #btnGuardarList').on('click', function() {
					createDestinyBoxForCargaCSV(4);
				});

				$("#cargaCSVModal #checkBoxTodos2").change(function() {
					$('.valid input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Selecciona todos los cecos activos.
				$('#checkBoxTodos2').click();

				// Se muestra modal
				$('#cargaCSVModal').modal();
			},
			error: function(obj, errorText) {
				console.log('No se pudo insertar el registro.');
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	} catch (e) {
		// Se oculta la animación de carga
		hideLoadingSpinner();

		// En caso de que ocurra un error al leer el archivo de Excel se imprime el error
		console.log(e);

		// Se muestra alerta al usuario
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se pudo leer el archivo CSV.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
	}

	// loadExcelsData(json, extra);
}

function getIndicadoresInfo() {
	console.log('getIndicadoresInfo');
	showLoadingSpinner();

	var _indicadoresGeneral = JSON.parse(indicadoresGeneral);
	var totalTabletas = _indicadoresGeneral.totalTabletas;
	var totalInstalaciones = _indicadoresGeneral.totalInstalaciones;
	var avanceTotal = (totalTabletas / totalInstalaciones) * 100;
	var ultimaInstalacion = _indicadoresGeneral.ultimaInstalacion;
	var porInstalar = totalInstalaciones - totalTabletas;
	var porcentajePorInstalar = (porInstalar / totalInstalaciones) * 100;

	$('#genA').html(totalTabletas);
	$('#genB').html(avanceTotal.toFixed(2) + '%');
	$('#genC').html(ultimaInstalacion);
	$('#genD').html(porInstalar);
	$('#genE').html(porcentajePorInstalar.toFixed(2) + '%');
	$('#genF').html(totalInstalaciones);

	var _indicadoresPropios = JSON.parse(indicadoresPropios);
	var totalTabletasProp = _indicadoresPropios.totalTabletas;
	var totalInstalacionesProp = _indicadoresPropios.totalInstalaciones;
	var avanceTotalProp = (totalTabletasProp / totalInstalacionesProp) * 100;
	var ultimaInstalacionProp = _indicadoresPropios.ultimaInstalacion;
	var porInstalarProp = totalInstalacionesProp - totalTabletasProp;
	var porcentajePorInstalarProp = (porInstalarProp / totalInstalacionesProp) * 100;

	$('#propA').html(totalTabletasProp);
	$('#propB').html(avanceTotalProp.toFixed(2) + '%');
	$('#propC').html(ultimaInstalacionProp);
	$('#propD').html(porInstalarProp);
	$('#propE').html(porcentajePorInstalarProp.toFixed(2) + '%');
	$('#propF').html(totalInstalacionesProp);

	var _indicadoresTerceros = JSON.parse(indicadoresTerceros);
	var totalTabletasTer = _indicadoresTerceros.totalTabletas;
	var totalInstalacionesTer = _indicadoresTerceros.totalInstalaciones;
	var avanceTotalTer = (totalTabletasTer / totalInstalacionesTer) * 100;
	var ultimaInstalacionTer = _indicadoresTerceros.ultimaInstalacion;
	var porInstalarTer = totalInstalacionesTer - totalTabletasTer;
	var porcentajePorInstalarTer = (porInstalarTer / totalInstalacionesTer) * 100;

	$('#terA').html(totalTabletasTer);
	$('#terB').html(avanceTotalTer.toFixed(2) + '%');
	$('#terC').html(ultimaInstalacionTer);
	$('#terD').html(porInstalarTer);
	$('#terE').html(porcentajePorInstalarTer.toFixed(2) + '%');
	$('#terF').html(totalInstalacionesTer);

	/**
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getIndicadoresEG.json',
		dataType: 'json',
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			var totalTabletas = data[0].totalTabletas;
			var totalInstalaciones = data[0].totalInstalaciones;
			var avanceTotal = (totalTabletas / totalInstalaciones) * 100;
			var ultimaInstalacion = data[0].ultimaInstalacion;
			var porInstalar = totalInstalaciones - totalTabletas;
			var porcentajePorInstalar = (porInstalar / totalInstalaciones) * 100;

			$('#tabletasInstaladas').html('<strong>' + totalTabletas + '</strong>');
			$('#avanceInstalaciones').html('<strong>' + avanceTotal.toFixed(2) + '%</strong>');
			$('#ultimaInstalacion').html('Última instalación:<br><b>' + ultimaInstalacion +'</b>');
			$('#porInstalar').html('<strong>' + porInstalar + '</strong>');
			$('#porcentajePorInstalar').html('<strong>' + porcentajePorInstalar.toFixed(2) + '%</strong>');
			$('#totalInstalaciones').html('Total final de instalaciones:<br><b>' + totalInstalaciones + '</b>');
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informacion...');
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
	*/

}

function getIndicadoresInfoET() {
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getIndicadoresET.json',
		dataType: 'json',
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			var tabletasCorrectas = data[0].tabletasCorrectas;
			var tabletasIncorrectas = data[0].tabletasIncorrectas;
			var tabletasMantenimiento = data[0].tabletasMantenimiento;
			var total = tabletasCorrectas + tabletasIncorrectas;
			var b = (tabletasCorrectas / total) * 100;
			var c = (tabletasIncorrectas / total) * 100;

			$('#tabletasCorrectas').html('<strong>' + tabletasCorrectas + '</strong>');
			$('#tabletasPorcentaje').html('<strong>' + b.toFixed(2) + '%</strong>');
			$('#tabletasIncorrectas').html('<strong>' + tabletasIncorrectas + '</strong>');
			$('#tabletasIncorrectasPorcentaje').html('Porcentaje de tabletas con algún problema:<br><b>' + c.toFixed(2) + '%</b>');
			$('#tabletasMantenimiento').html('Tabletas en mantenimiento:<br><b>' + tabletasMantenimiento + '</b>');
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informacion...');
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function makeGraphic() {
	showLoadingSpinner();
	var _year = $('#tipoEnvio').val();

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getDatosGraficaEG.json',
		dataType: 'json',
		data: {
			anio: _year
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			var color = Chart.helpers.color;
			var _barData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			for (var x = 0; x < 12; x++) {
				for (var y = 0; y < data.length; y++) {
					if (data[y].mes === (x + 1)) {
						_barData.splice(x, 1, data[y].total);
					}
				}
			}

			var ctx = document.getElementById("canvas-vertical").getContext("2d");
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
					datasets: [
						{
							label: '',
							backgroundColor: [
								color(window.chartColors.verde).rgbString(),
								color(window.chartColors.verde1).rgbString(),
								color(window.chartColors.verde2).rgbString(),
								color(window.chartColors.verde3).rgbString(),
								color(window.chartColors.verde4).rgbString(),
								color(window.chartColors.verde).rgbString(),
								color(window.chartColors.verde1).rgbString(),
								color(window.chartColors.verde2).rgbString(),
								color(window.chartColors.verde3).rgbString(),
								color(window.chartColors.verde4).rgbString(),
								color(window.chartColors.verde).rgbString(),
								color(window.chartColors.verde1).rgbString(),
							],
							borderWidth: 1,
							data: _barData
						}
					]
				},
				options: {
					animation: false,
					hover: {
						mode: null
					},
					spanGaps: true,
					responsive: true,
					showAllTooltips: true,
					legend: {
						display: false,
						position: 'left'
					},
					months: {
						display: true
					},
					title: {
						display: true,
						text: 'Cronograma de instalaciones'
					},
					tooltips: {
						yAlign: 'bottom'
					},
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							}
						}]
					},
					onAnimationComplete: saveCanvasToURI
				}
			});
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informacion...');
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function makeGraphicGeneral() {
	console.log('makeGraphicGeneral');
	showLoadingSpinner();
	var _year = $('#yearSelect').val();
	$('#graficaGeneral').css('height', '400px');

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getDatosGraficaEG.json',
		dataType: 'json',
		data: {
			anio: _year,
			negocio: '41,96'
		},
		success: function (data) {
			var color = Chart.helpers.color;
			var _barData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			for (var x = 0; x < 12; x++) {
				for (var y = 0; y < data.length; y++) {
					if (data[y].mes === (x + 1)) {
						_barData.splice(x, 1, data[y].total);
					}
				}
			}

			var ctx = document.getElementById('graficaGeneral').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
					datasets: [{
						label: '',
						backgroundColor: [
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
						],
						borderWidth: 1,
						data: _barData
					}]
				},
				options: {
					animation: false,
					hover: {
						mode: null
					},
					spanGaps: true,
					responsive: true,
					showAllTooltips: true,
					legend: {
						display: false,
						position: 'left'
					},
					months: {
						display: true
					},
					title: {
						display: true,
						text: 'Cronograma de instalaciones'
					},
					tooltips: {
						yAlign: 'bottom'
					},
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							},
							splitLine: {
								show: false
							}
						}]
					},
					onAnimationComplete: saveCanvasToURIGeneral
				}
			});
		},
		error: function (obj, errorText) {
			console.log('No se pudo obtener la informacion...');
		},
		complete: function () {
			hideLoadingSpinner();
		}
	});
}

function makeGraphicPropios() {
	console.log('makeGraphicPropios');
	showLoadingSpinner();
	var _year = $('#yearSelect2').val();
	$('#graficaPropios').css('height', '400px');

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getDatosGraficaEG.json',
		dataType: 'json',
		data: {
			anio: _year,
			negocio: '41'
		},
		success: function (data) {
			var color = Chart.helpers.color;
			var _barData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			for (var x = 0; x < 12; x++) {
				for (var y = 0; y < data.length; y++) {
					if (data[y].mes === (x + 1)) {
						_barData.splice(x, 1, data[y].total);
					}
				}
			}

			var ctx = document.getElementById('graficaPropios').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
					datasets: [{
						label: '',
						backgroundColor: [
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
						],
						borderWidth: 1,
						data: _barData
					}]
				},
				options: {
					animation: false,
					hover: {
						mode: null
					},
					spanGaps: true,
					responsive: true,
					showAllTooltips: true,
					legend: {
						display: false,
						position: 'left'
					},
					months: {
						display: true
					},
					title: {
						display: true,
						text: 'Cronograma de instalaciones'
					},
					tooltips: {
						yAlign: 'bottom'
					},
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							},
							splitLine: {
								show: false
							}
						}]
					},
					onAnimationComplete: saveCanvasToURIPropios
				}
			});
		},
		error: function (obj, errorText) {
			console.log('No se pudo obtener la informacion...');
		},
		complete: function () {
			hideLoadingSpinner();
		}
	});
}

function makeGraphicTerceros() {
	console.log('makeGraphicTerceros');
	showLoadingSpinner();
	var _year = $('#yearSelect3').val();
	$('#graficaTerceros').css('height', '400px');

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getDatosGraficaEG.json',
		dataType: 'json',
		data: {
			anio: _year,
			negocio: '96'
		},
		success: function (data) {
			var color = Chart.helpers.color;
			var _barData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			for (var x = 0; x < 12; x++) {
				for (var y = 0; y < data.length; y++) {
					if (data[y].mes === (x + 1)) {
						_barData.splice(x, 1, data[y].total);
					}
				}
			}

			var ctx = document.getElementById('graficaTerceros').getContext('2d');
			window.myBar = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
					datasets: [{
						label: '',
						backgroundColor: [
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
							color(window.chartColors.verde5).rgbString(),
						],
						borderWidth: 1,
						data: _barData
					}]
				},
				options: {
					animation: false,
					hover: {
						mode: null
					},
					spanGaps: true,
					responsive: true,
					showAllTooltips: true,
					legend: {
						display: false,
						position: 'left'
					},
					months: {
						display: true
					},
					title: {
						display: true,
						text: 'Cronograma de instalaciones'
					},
					tooltips: {
						yAlign: 'bottom'
					},
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero: true
							},
							splitLine: {
								show: false
							}
						}]
					},
					onAnimationComplete: saveCanvasToURITerceros
				}
			});
		},
		error: function (obj, errorText) {
			console.log('No se pudo obtener la informacion...');
		},
		complete: function () {
			hideLoadingSpinner();
		}
	});
}

function saveCanvasToURIGeneral() {
	canvasURI = document.getElementById('graficaGeneral').toDataURL();
}

function saveCanvasToURIPropios() {
	canvasURI = document.getElementById('graficaPropios').toDataURL();
}

function saveCanvasToURITerceros() {
	canvasURI = document.getElementById('graficaTerceros').toDataURL();
}

function descargaDatosTablaEGGen() {
	console.log('descargaDatosTablaEGGen');
	// showLoadingSpinner();
	var _anio = $('#yearSelect').val();
	var _url = contextPath + '/central/pedestalDigital/getDataForExcelEG.json';

	$('#countdown').html(' ... ');
	$('#tamanoArchivo').html(' calculando tama\u00F1o del archivo ... ');
	$('.toastDescarga').addClass('active');

	// Se asigna evento al boton de cerrar
	$('.toastDescarga').find('.cerrar').on('click', function () {
		$(this).closest('.toastDescarga').removeClass('active');
	});

	$.ajax({
		progress: function(event) {
			var _lengthComputable = event.lengthComputable;
			var _total = event.total;
			var _loaded = event.loaded;

			console.log('_lengthComputable: ' + _lengthComputable);
			console.log('_total: ' + _total);
			console.log('_loaded: ' + _loaded);

			if (_lengthComputable) {
				$('#countdown').html('estatus_general.xls');
				if (_total < 1000) {
					$('#tamanoArchivo').html(_loaded + '/' + _total + ' bytes');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				} else if (_total >= 1000 && _total < 1000000) {	
					$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_total / 1000).toFixed(2) + ' KB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				} else if (_total > 1000000) {
					$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_total / 1000 / 1000).toFixed(3) + ' MB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				}
			} else if (_total === 0 && _loaded > 0) {
				$('#countdown').html('estatus_general.xls');
				if (_loaded < 1000) {
					$('#tamanoArchivo').html(_loaded + '/' + _loaded + ' bytes');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				} else if (_loaded >= 1000 && _loaded < 1000000) {	
					$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_loaded / 1000).toFixed(2) + ' KB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				} else if (_loaded > 1000000) {
					$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_loaded / 1000 / 1000).toFixed(3) + ' MB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				}
			}
		},
		cache: false,
		// contentType: false,
		data: {
			year: _anio,
			negocio: '41,96'
		},
		// processData: false,
		responseType: 'arraybuffer',
		type: 'POST',
		url: _url,
	}).done(function(data) {
		var blob = new Blob([data], {
			type: 'text/html'
		});
		saveAs(blob, 'estatus_general.xls');
	}).always(function() {
		// hideLoadingSpinner();
	});
}

function descargaDatosTablaEGProp() {
	console.log('descargaDatosTablaEGProp');
	// showLoadingSpinner();
	var _anio = $('#yearSelect2').val();
	var _url = contextPath + '/central/pedestalDigital/getDataForExcelEG.json';

	$('#countdown').html(' ... ');
	$('#tamanoArchivo').html(' calculando tama\u00F1o del archivo ... ');
	$('.toastDescarga').addClass('active');

	// Se asigna evento al boton de cerrar
	$('.toastDescarga').find('.cerrar').on('click', function () {
		$(this).closest('.toastDescarga').removeClass('active');
	});

	$.ajax({
		progress: function(event) {
			var _lengthComputable = event.lengthComputable;
			var _total = event.total;
			var _loaded = event.loaded;

			console.log('_lengthComputable: ' + _lengthComputable);
			console.log('_total: ' + _total);
			console.log('_loaded: ' + _loaded);

			if (_lengthComputable) {
				$('#countdown').html('estatus_general.xls');
				if (_total < 1000) {
					$('#tamanoArchivo').html(_loaded + '/' + _total + ' bytes');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				} else if (_total >= 1000 && _total < 1000000) {	
					$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_total / 1000).toFixed(2) + ' KB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				} else if (_total > 1000000) {
					$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_total / 1000 / 1000).toFixed(3) + ' MB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				}
			} else if (_total === 0 && _loaded > 0) {
				$('#countdown').html('estatus_general.xls');
				if (_loaded < 1000) {
					$('#tamanoArchivo').html(_loaded + '/' + _loaded + ' bytes');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				} else if (_loaded >= 1000 && _loaded < 1000000) {	
					$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_loaded / 1000).toFixed(2) + ' KB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				} else if (_loaded > 1000000) {
					$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_loaded / 1000 / 1000).toFixed(3) + ' MB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				}
			}
		},
		cache: false,
		// contentType: false,
		data: {
			year: _anio,
			negocio: '41'
		},
		// processData: false,
		responseType: 'arraybuffer',
		type: 'POST',
		url: _url,
	}).done(function(data) {
		var blob = new Blob([data], {
			type: 'text/html'
		});
		saveAs(blob, 'estatus_general.xls');
	}).always(function() {
		// hideLoadingSpinner();
	});
}

function descargaDatosTablaEGTer() {
	console.log('descargaDatosTablaEGTer');
	// showLoadingSpinner();
	var _anio = $('#yearSelect3').val();
	var _url = contextPath + '/central/pedestalDigital/getDataForExcelEG.json';

	$('#countdown').html(' ... ');
	$('#tamanoArchivo').html(' calculando tama\u00F1o del archivo ... ');
	$('.toastDescarga').addClass('active');

	// Se asigna evento al boton de cerrar
	$('.toastDescarga').find('.cerrar').on('click', function () {
		$(this).closest('.toastDescarga').removeClass('active');
	});

	$.ajax({
		progress: function(event) {
			var _lengthComputable = event.lengthComputable;
			var _total = event.total;
			var _loaded = event.loaded;

			console.log('_lengthComputable: ' + _lengthComputable);
			console.log('_total: ' + _total);
			console.log('_loaded: ' + _loaded);

			if (_lengthComputable) {
				$('#countdown').html('estatus_general.xls');
				if (_total < 1000) {
					$('#tamanoArchivo').html(_loaded + '/' + _total + ' bytes');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				} else if (_total >= 1000 && _total < 1000000) {	
					$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_total / 1000).toFixed(2) + ' KB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				} else if (_total > 1000000) {
					$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_total / 1000 / 1000).toFixed(3) + ' MB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				}
			} else if (_total === 0 && _loaded > 0) {
				$('#countdown').html('estatus_general.xls');
				if (_loaded < 1000) {
					$('#tamanoArchivo').html(_loaded + '/' + _loaded + ' bytes');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				} else if (_loaded >= 1000 && _loaded < 1000000) {	
					$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_loaded / 1000).toFixed(2) + ' KB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				} else if (_loaded > 1000000) {
					$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_loaded / 1000 / 1000).toFixed(3) + ' MB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				}
			}
		},
		cache: false,
		// contentType: false,
		data: {
			year: _anio,
			negocio: '96'
		},
		// processData: false,
		responseType: 'arraybuffer',
		type: 'POST',
		url: _url,
	}).done(function(data) {
		var blob = new Blob([data], {
			type: 'text/html'
		});
		saveAs(blob, 'estatus_general.xls');
	}).always(function() {
		// hideLoadingSpinner();
	});
}

function saveImageET() {
	showLoadingSpinner();
	var _canvas = document.getElementById('canvas-vertical');
	_canvas.toBlob(function(blob) {
		saveAs(blob, "grafica_estatus_general.png");
		hideLoadingSpinner();
	});
}

function saveImageETGen() {
	console.log('saveImageETGen');
	showLoadingSpinner();
	var _canvas = document.getElementById('graficaGeneral');
	_canvas.toBlob(function(blob) {
		saveAs(blob, 'grafica_estatus_general.png');
		hideLoadingSpinner();
	});
}

function saveImageETProp() {
	console.log('saveImageETProp');
	showLoadingSpinner();
	var _canvas = document.getElementById('graficaPropios');
	_canvas.toBlob(function(blob) {
		saveAs(blob, "grafica_estatus_general.png");
		hideLoadingSpinner();
	});
}

function saveImageETTer() {
	console.log('saveImageETTer');
	showLoadingSpinner();
	var _canvas = document.getElementById('graficaTerceros');
	_canvas.toBlob(function(blob) {
		saveAs(blob, "grafica_estatus_general.png");
		hideLoadingSpinner();
	});
}

function base64_encode(data) {
	// http://kevin.vanzonneveld.net
	// +   original by: Tyler Akins (http://rumkin.com)
	// +   improved by: Bayron Guevara
	// +   improved by: Thunder.m
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   bugfixed by: Pellentesque Malesuada
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: Rafal Kukawski (http://kukawski.pl)
	// *     example 1: base64_encode('Kevin van Zonneveld');
	// *     returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='
	// mozilla has this native
	// - but breaks in 2.0.0.12!
	//if (typeof this.window['btoa'] == 'function') {
	//    return btoa(data);
	//}
	var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
		ac = 0,
		enc = "",
		tmp_arr = [];

	if (!data) {
		return data;
	}

	do { // pack three octets into four hexets
		o1 = data.charCodeAt(i++);
		o2 = data.charCodeAt(i++);
		o3 = data.charCodeAt(i++);

		bits = o1 << 16 | o2 << 8 | o3;

		h1 = bits >> 18 & 0x3f;
		h2 = bits >> 12 & 0x3f;
		h3 = bits >> 6 & 0x3f;
		h4 = bits & 0x3f;

		// use hexets to index into b64, and append result to encoded string
		tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
	} while (i < data.length);

	enc = tmp_arr.join('');
	var r = data.length % 3;
	return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
}

function getCecoDataList() {
	showLoadingSpinner();

	if (_nivelC === 0) {
		_containerC = 'Territorios';
		$('#btnListSigNivel').show();
	} else if (_nivelC === 1) {
		_containerC = 'Zonas';
		$('#btnListSigNivel').show();
	} else if (_nivelC === 2) {
		_containerC = 'Regiones';
		$('#btnListSigNivel').show();
	} else if (_nivelC === 3) {
		_containerC = 'Sucursales';
		$('#btnListSigNivel').hide();
	}

	if (_cecos.length < 1 && _objBackup.length > 1 && _nivelC === 1) {
		$('.titModal').html('Lista de ' + _containerC);
		$('#modalList #theList').html('');
		$('#modalList #theList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
		$('#modalList #theList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos3" class="checkM" /><label for="checkBoxTodos3" >Seleccionar todo</label></div></th></tr>');

		// Aqui se agregan las zonas especiales
		if (_objBackup.length > 1 && _objNameBackup.length > 1) {
			var objList = _objBackup.split(',');
			var objNameList = _objNameBackup.split(',');

			for (var x = 0; x < objList.length; x++) {
				_html = '';
				_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
				_html += '<label for="' + objList[x] + '" ><strong>' + objNameList[x] + '</strong></label></div></td></tr>';
				_html += '<tr><td style="text-align: left;"><div class="listBox">';
				_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + objList[x] + '" class="checkM" />';
				_html += '<label for="' + objList[x] + '" >' + objNameList[x] + '</label></div></td></tr>';
				$('#modalList #theList #listTable').append(_html);
			}
		}

		_nivelC++;

		// Para la opción 'Seleccionar todos'
		$("#modalList #checkBoxTodos3").change(function() {
			$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
		});

		// Se agrega <br> para organizar mejor elementos
		$('#modalList #theList').append('<br>');

		// Se crea el modal
		$('#modalList').modal();

		hideLoadingSpinner();
	} else if (_cecos.length > 0) {

		if (_nivelC === 0) {
			_objBackup = '';
			_objNameBackup = '';
		}

		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/consultaCecos.json',
			dataType: 'json',
			data: {
				tipo: _tipoC,
				cecos: _cecos,
				nivel: _nivelC,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					$('#btnListSigNivel').show();
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('.titModal').html('Lista de ' + _containerC);
				$('#modalList #theList').html('');
				$('#modalList #theList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalList #theList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos3" class="checkM" /><label for="checkBoxTodos3" >Seleccionar todo</label></div></th></tr>');

				_nivelC = data[0].tipoCeco;

				var actualId = data[0].cecoSuperior;
				var actualDesc = data[0].cecoSuperiorDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelC > 1) {
						if (actualId != data[x].cecoSuperior) {
							actualId = data[x].cecoSuperior;
							actualDesc = data[x].cecoSuperiorDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].cecoSuperior + '" ><strong>' + data[x].cecoSuperiorDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" >' + data[x].nombre + '</label></div></td></tr>';
						$('#modalList #theList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" >' + data[x].nombre + '</label></div></td></tr>';
						$('#modalList #theList #listTable').append(_html);
					}
				}

				// Aqui se agregan las zonas especiales
				if (_objBackup.length > 1 && _objNameBackup.length > 1) {
					var objList = _objBackup.split(',');
					var objNameList = _objNameBackup.split(',');

					for (var x = 0; x < objList.length; x++) {
						_html = '';
						_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
						_html += '<label for="' + objList[x] + '" ><strong>' + objNameList[x] + '</strong></label></div></td></tr>';
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + objList[x] + '" class="checkM" />';
						_html += '<label for="' + objList[x] + '" >' + objNameList[x] + '</label></div></td></tr>';
						$('#modalList #theList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalList #checkBoxTodos3").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalList #theList').append('<br>');

				// Se crea el modal
				$('#modalList').modal();
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	}
}

function getGeoDataList() {
	showLoadingSpinner();

	if (_nivelT === 0) {
		_containerP = 'Pa\u00EDs';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 1) {
		_containerP = 'Estados';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 2) {
		_containerP = 'Municipios';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 3) {
		_containerP = 'Sucursales';
		$('#btnListSigNivel').hide();
	} else {
		_containerP = 'Pa\u00EDs';
		$('#btnListSigNivel').show();
	}

	var _title = $('#modalList .titModal').html();
	if (_title.includes('Municipios')) {
		_containerC = 'Sucursales';
		$('#btnListSigNivel').hide();
		_nivelT = 3;
	}

	if (_nivelT === 3) {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneCecosPorGeo.json',
			dataType: 'json',
			data: {
				geo: _cecos,
				tipo: _nivelT,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					$('#btnListSigNivel').show();
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#modalList .titModal').html('Lista de ' + _containerP);
				$('#modalList #theList').html('');
				$('#modalList #theList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalList #theList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos4" class="checkM" /><label for="checkBoxTodos4" >Seleccionar todo</label></div></th></tr>');

				// _nivelT = data[0].tipo;
				_nivelT = 4;

				var actualId = data[0].idGeo;
				var actualDesc = data[0].descGeo;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (actualId != data[x].idGeo) {
						actualId = data[x].idGeo;
						actualDesc = data[x].descGeo;
						makeDist = true;
					}

					if (makeDist) {
						_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
						_html += '<label for="' + data[x].idGeo + '" ><strong>' + data[x].descGeo + '</strong></label></div></td></tr>';
						makeDist = false;
					}

					_html += '<tr><td style="text-align: left;"><div class="listBox">';
					_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="2-.-.-' + data[x].idCeco + '-.-.-' + _nivelT + '" class="checkM" />';
					_html += '<label for="2-.-.-' + data[x].idCeco + '-.-.-' + _nivelT + '" >' + '(' + data[x].idCeco + ') ' + data[x].descCeco + '</label></div></td></tr>';
					$('#modalList #theList #listTable').append(_html);
				}

				// Para la opción 'Seleccionar todos'
				$("#modalList #checkBoxTodos4").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalList #theList').append('<br>');

				// Se crea el modal
				$('#modalList').modal();
			},
			error: function(obj, errorText) {
				// console.log('No se pudo obtener la informacion...');
				$('#btnListSigNivel').show();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			} 
		});
	} else {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/consultaPais.json',
			dataType: 'json',
			data: {
				tipo: _tipoT,
				cecos: _cecos,
				nivel: _nivelT,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					$('#btnListSigNivel').show();
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#modalList .titModal').html('Lista de ' + _containerP);
				$('#modalList #theList').html('');
				$('#modalList #theList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalList #theList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos4" class="checkM" /><label for="checkBoxTodos4" >Seleccionar todo</label></div></th></tr>');

				_nivelT = data[0].tipo;

				var actualId = data[0].idDependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelT > 1) {
						if (actualId != data[x].idDependeDe) {
							actualId = data[x].idDependeDe;
							actualDesc = data[x].dependeDeDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].idDependeDe + '" ><strong>' + data[x].dependeDeDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="2-.-.-' + data[x].id_geolocalizacion + '-.-.-' + _nivelT + '" class="checkM" />';
						_html += '<label for="2-.-.-' + data[x].id_geolocalizacion + '-.-.-' + _nivelT + '" >' + data[x].descripcion + '</label></div></td></tr>';
						$('#modalList #theList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="2-.-.-' + data[x].id_geolocalizacion + '-.-.-' + _nivelT + '" class="checkM" />';
						_html += '<label for="2-.-.-' + data[x].id_geolocalizacion + '-.-.-' + _nivelT + '" >' + data[x].descripcion + '</label></div></td></tr>';
						$('#modalList #theList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalList #checkBoxTodos4").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalList #theList').append('<br>');

				// Se crea el modal
				$('#modalList').modal();
			},
			error: function(obj, errorText) {
				// console.log('No se pudo obtener la informacion...');
				$('#btnListSigNivel').show();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			} 
		});
	}
}

function createDestinyBox(boxId) {
	// Obtiene valores y actualia variables
	var _list = $('#theList .listBox');
	var _title = $('#modalList .titModal').html();
	var _cecos = '';
	var _listName = '';

	if (_title.includes('Territorios') || _title.includes('Zonas') || _title.includes('Regiones') || _title.includes('Sucursales')) {
		for (var x = 1; x < _list.length; x++) {
			var _input = $(_list[x]).find('input');
			var _label = $(_list[x]).find('label');

			if ($(_input).is(':checked')) {
				var _id = _input[0].id;
				_cecos += _id.split('-.-.-')[0]+ ',';
				_listName += $(_label).html() + ',';
			}
		}
	} else if (_title.includes('Pa\u00EDs') || _title.includes('Estados') || _title.includes('Municipios')) {
		for (var x = 1; x < _list.length; x++) {
			var _input = $(_list[x]).find('input');
			var _label = $(_list[x]).find('label');

			if ($(_input).is(':checked')) {
				var _id = _input[0].id;
				_cecos += _id.split('-.-.-')[1]+ ',';
				_listName += $(_label).html() + ',';
			}
		}
	}

	if (_cecos.length < 1) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se ha seleccionado ning\u00FAn valor' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else {
		// Quita ultima coma de la cadena
		_cecos = _cecos.substring(0, _cecos.length - 1);
		_listName = _listName.substring(0, _listName.length - 1);
	}

	var _arrayCecos = _cecos.split(',');
	var _arrayNames = _listName.split(',');

	// listaLanzamientos
	for (var x = 0; x < _arrayCecos.length; x++) {
		if (_title.includes('Pa\u00EDs') || _title.includes('Estados') || _title.includes('Municipios')) {
			var _html = '';
			_html += '<div id="' + boxId + '-.-.-' + _arrayCecos[x] + '-.-.-' + _nivelT + '" class="col4 divEliminar" style="margin-bottom: 15px;">';
			_html += '<a href="#" class="btnCerrar1" onclick="removeDomItem(this);"></a>';
			_html += '<p>' + _arrayNames[x] + '</p></div>';
			$('#listaLanzamientos').append(_html);
		} else {
			var _html = '';
			_html += '<div id="' + boxId + '-.-.-' + _arrayCecos[x] + '" class="col4 divEliminar" style="margin-bottom: 15px;">';
			_html += '<a href="#" class="btnCerrar1" onclick="removeDomItem(this);"></a>';
			_html += '<p>' + _arrayNames[x] + '</p></div>';
			$('#listaLanzamientos').append(_html);
		}
	}

	// Inhabilita / habilita componentes de acuerdo a la opcion seleccionada
	if (boxId === 2) {
		// Pais
		$('#btnListTerritorio').prop('onclick', null).off('click');
		$('#btnListTerritorio').removeClass('btnDistrib');
		$('#btnListTerritorio').addClass('btnDistribDis');

		$('#btnListaDistribucion').prop('onclick', null).off('click');
		$('#btnListaDistribucion').removeClass('btnDistrib');
		$('#btnListaDistribucion').addClass('btnDistribDis');

		$('#btnCargaCSV').prop('onclick', null).off('click');
		$('#btnCargaCSV').removeClass('btnDistrib');
		$('#btnCargaCSV').addClass('btnDistribDis');
	} else {
		// Territorio
		$('#btnListPais').prop('onclick', null).off('click');
		$('#btnListPais').removeClass('btnDistrib');
		$('#btnListPais').addClass('btnDistribDis');
	}

	$('.modalCloseImg').click();
}

function createDestinyBoxForCargaCSV(boxId) {
	// Obtiene valores y actualia variables
	var _list = $('#cargaCSVModal #theList .listBox');
	var _cecos = '';
	var _listName = '';

	for (var x = 1; x < _list.length; x++) {
		var _input = $(_list[x]).find('input');
		var _label = $(_list[x]).find('label');

		if ($(_input).is(':checked')) {
			_cecos += _input[0].id + ',';
			_listName += $(_label).html() + ',';
		}
	}

	if (_cecos.length < 1) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se ha seleccionado ning\u00FAn valor' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else {
		// Quita ultima coma de la cadena
		_cecos = _cecos.substring(0, _cecos.length - 1);
		_listName = _listName.substring(0, _listName.length - 1);
	}

	var _arrayCecos = _cecos.split(',');
	var _arrayNames = _listName.split(',');
	var _html = '';

	_html += '<div id="' + boxId + '-.-.-' + _cecos + '" class="col4 divEliminar" style="margin-bottom: 15px;">';
	_html += '<a href="#" class="btnCerrar1" onclick="removeDomItem(this);"></a>';
	_html += '<p>' + $('#cargaCSVModal #listNameModal').val() + '</p></div>';

	$('#listaLanzamientos').append(_html);
	$('#cargaCSVModal .btnCerrar').click();
}

function nextLevelList() {
	showLoadingSpinner();

	var title = $('#modalList .titModal').html();
	var _list = $('#modalList #theList .listBox');

	_cecos = '';
	_listName = '';
	_objBackup = '';
	_objNameBackup = '';

	for (var x = 1; x < _list.length; x++) {
		var _input = $(_list[x]).find('input');
		var _label = $(_list[x]).find('label');

		if (title.includes('Territorios') || title.includes('Zonas') || title.includes('Regiones') || title.includes('Sucursales')) {
			if ($(_input).is(':checked') && _nivelC === 1) {
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');

				if (_nivelC === parseInt(_foo[1])) {
					_cecos += _foo[0] + ',';
					_listName += $(_label).html() + ',';
				} else {
					_objBackup += _foo[0] + ',';
					_objNameBackup += $(_label).html() + ',';
				}
			} else if ($(_input).is(':checked') && _nivelC > 1) {
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			}
		} else if (title.includes('Pa\u00EDs') || title.includes('Estados') || title.includes('Municipios')) {
			if ($(_input).is(':checked')) {
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');
				_cecos += _foo[1] + ',';
				_listName += $(_label).html() + ',';
			}
		}
	}

	if (_cecos.length < 1 && _objBackup.length < 1) {
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se ha seleccionado ning\u00FAn valor' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else {
		// Quita ultima coma de la cadena
		_cecos = _cecos.substring(0, _cecos.length - 1);
		_listName = _listName.substring(0, _listName.length - 1);
		_objBackup = _objBackup.substring(0, _objBackup.length - 1);
		_objNameBackup = _objNameBackup.substring(0, _objNameBackup.length - 1);

		// Actualiza variables y consume servicios
		if (title.includes('Territorios') || title.includes('Zonas') || title.includes('Regiones') /** title.includes('Sucursales') */ ) {
			getCecoDataList();
		} else if (title.includes('Pa\u00EDs') || title.includes('Estados') || title.includes('Municipios') /** title.includes('Municipios') */ ) {
			getGeoDataList();
		}
	}
}

function removeDomItem(obj) {
	console.log('removeDomItem');

	parent = obj.parentNode;
	parent.parentNode.removeChild(parent);
	var _list = $('#listaLanzamientos').find('.divEliminar');
	if (_list.length > 0) {
		// Dependiendo los elementos en la lista, son los elementos que se habilitan o se quedan inhabilitados
		var tList = pList = cList = 0;

		for (var x = 0; x < _list.length; x++) {
			var _item = $(_list[x]);
			var _id = $(_item).attr('id');

			if (_id.includes('-.-.-')) {
				var _idList = _id.split('-.-.-');

				if (_idList[0] === 'T') {
					tList++;
				} else if (_idList[0] === 'P') {
					pList++;
				} else if (_idList[0] === 'C') {
					cList++;
				}
			} else {
				// EN PROGRESO / ETC...
				// BATCH / ETC...
			}
		}

		if (tList > 0) {
			// TODO: Validar, posible combinación inválida
			if (pList > 0) {
				$('#buscarPorFolio').prop('disabled', true);
			}

			if (cList > 0) {
				$('#buscarPorFolio').prop('disabled', true);
			}
		}

		if (pList > 0) {
			console.log('pList: ' + pList);
			// TODO: Validar, posible combinación inválida
			if (tList > 0) {
				$('#buscarPorFolio').prop('disabled', true);
			}

			if (cList > 0) {
				$('#buscarPorFolio').prop('disabled', true);
			}
		}

		if (cList > 0) {
			console.log('cList: ' + cList);
			if (tList > 0) {
				$('#buscarPorFolio').prop('disabled', true);
			}

			if (pList > 0) {
				$('#buscarPorFolio').prop('disabled', true);
			}
		}
	} else {
		// Se habilitan todos los elementos
		$('#datepickerAdminEnv').prop('disabled', false);
		$('#estatusEnvio').prop('disabled', false);
		$('#tipoEnvio').prop('disabled', false);
		$('#buscarPorFolio').prop('disabled', false);
		$('#buscarPorSucursal').prop('disabled', false);

		$('#btnTerritorio').click(function(e) {
			_nivelT = 0;
			_cecos = '0';
			_boxId = 1;
			getAdminTerritorioList();
		});

		$('#btnPais').click(function(e) {
			_nivelP = 0;
			_cecos = '0';
			_boxId = 2;
			getAdminPaisList();
		});

		$('#btnCategoria').click(function(e) {
			_nivelC = 1;
			_cecos = '0';
			_boxId = 3;
			getAdminCategoriaList();
		});

		// Se habilitan botones de de Carga de archivos
		$('#btnListTerritorio').prop('disabled', false);
		$('#btnListTerritorio').removeClass('btnDistribDis');
		$('#btnListTerritorio').addClass('btnDistrib');

		$('#btnListPais').prop('disabled', false);
		$('#btnListPais').removeClass('btnDistribDis');
		$('#btnListPais').addClass('btnDistrib');

		$('#btnListaDistribucion').prop('disabled', false);
		$('#btnListaDistribucion').removeClass('btnDistribDis');
		$('#btnListaDistribucion').addClass('btnDistrib');

		$('#btnCargaCSV').prop('disabled', false);
		$('#btnCargaCSV').removeClass('btnDistribDis');
		$('#btnCargaCSV').addClass('btnDistrib');

		$('#btnListTerritorio').on('click', function(e) {
			_cecos = '0';
			_nivelC = 0;
			_boxId = 1;
			getCecoDataList();
		});

		$('#btnListPais').on('click', function(e) {
			_cecos = '0';
			_nivelT = 0;
			_boxId = 2;
			getGeoDataList();
		});

		$('#btnListaDistribucion').on('click', function() {
			showDistribListPopUp('listaDistribucionPD.htm', 'Listas de Distribuci\u00F3n', 1000, 600);
		});

		$('#btnCargaCSV').on('click', addFileToCargarCsvInput);
	}
}

function showDistribListPopUp(url, title, w, h) {
	var left = (screen.width / 2) - (w / 2);
	var top = (screen.height / 2) - (h / 2);
	return _window = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

function loadDistributionList(idLista, idUsuario, negocio) {
	$('#distribListContainer').html('');

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/consultaListaDistribucion.json',
		dataType: 'json',
		data: {
			idLista: idLista,
			idUsuario: idUsuario,
			negocio: negocio
		},
		success: function(data) {
			// console.log('success');

			for (var x = 0; x < data.length; x++) {
				var _html = '';
				_html += '<div id="' + data[x].id_listaDistribucion + '" class="col4 divEliminar" style="margin-bottom: 15px;" onclick="selectDistribListObj(this);">';
				// _html += '<a href="#" class="btnCerrar1" onclick="removeDomItem(this);"></a><br><p style="margin-top: 5px;">' + data[x].nombre + '</p></div>';
				_html += '<a href="#" class="simplemodal-close" onclick="removeDomItem(this);" style="position: absolute; right: 10px;"><img src="../../img/icoCerrar.svg" class="cerrar" style="margin: 0px; padding: 0px; width: 15px;"></a>';
				_html += '<br><p style="margin-top: 5px;">' + data[x].nombre + '</p></div>';
				$('#distribListContainer').append(_html);
			}
		},
		error: function(obj, errorText) {
			console.log(errorText);
			// console.log('error');
		},
		complete: function() {
			hideLoadingSpinner();
			// console.log('complete');
		}
	});
}

function saveDistributionListByCSV() {

	showLoadingSpinner();

	var _nombre = $('#listNameModal').val();
	var _negocio = $('#dk1-listNegocioModal .dk-option-selected').attr('data-value');
	var _list = $('#theList .listBox');

	if (_nombre.length < 1) {
		// Se muestra alerta al usuario
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Falta especificar el nombre de la lista de distribuci\u00F3n' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else if (_negocio.length < 1) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Falta especificar el negocio.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else if (_list.length < 1) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe seleccionar al menos un ceco para poder crear la lista de distribuci\u00F3n' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	var _cecos = '';
	for (var x = 1; x < _list.length; x++) {
		var _input = $(_list[x]).find('input');
		var _label = $(_list[x]).find('label');

		if ($(_input).is(':checked')) {
			_cecos += _input[0].id + ',';
		}
	}

	_cecos = _cecos.substring(0, _cecos.length - 1);

	if (_cecos.length > 0 && _nombre.length > 0 && _negocio > 0) {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/insertListaDistribucion.json',
			dataType: 'json',
			data: {
				nombre: _nombre,
				idUsuario: idUsuario,
				negocio: _negocio
			},
			success: function(data) {
				var _idLista = data[0].FIID_LISTA_DISTR;
				var __cecos = _cecos;

				$.ajax({
					type: 'get',
					cache: false,
					url: contextPath + '/central/pedestalDigital/insertaListaCecos.json',
					dataType: 'json',
					data: {
						idLista: _idLista,
						ceco: __cecos,
					},
					success: function(data) {
						Swal.fire({
							html: '<div style="width: 100%;">' +
								'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
								'Se cre\u00F3 la lista de distribuci\u00F3n con el nombre: ' + _nombre +
								'</div>',
							showCloseButton: false,
							confirmButtonText: 'Aceptar',
							confirmButtonColor: '#006341'
						});
						$('#btnCerrar').click();
						loadDistributionList(0, idUsuario, negocio);
					},
					error: function(obj, errorText) {
						// console.log('error');
						loadDistributionList(0, idUsuario, negocio);
						hideLoadingSpinner();
					},
					complete: function() {
						hideLoadingSpinner();
						// console.log('complete');
					}
				});

			},
			error: function(obj, errorText) {
				console.log(errorText);
				// console.log('error');
			},
			complete: function() {
				hideLoadingSpinner();
				// console.log('complete');
			}
		});
	}

	// Cerrar ventana modal
	$('.btnCerrar').click();
	hideLoadingSpinner();
}

function checkListName(event) {
	event = event || window.event;
	var listName = $('#listName').val();
	if (listName.trim().length < 1) {
		// Se muestra alerta al usuario
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe especificar un nombre para la lista de distribuci\u00F3n antes de continuar.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});

		event.preventDefault();
		return false;
	}
}

function selectDistribListObj(obj) {
	var parent = $(obj).parent();
	var list = $(parent).find('.divEliminar');

	for (var x = 0; x < list.length; x++) {
		list[x].style.backgroundColor = '';
		list[x].style.color = '';
		list[x].classList.remove('selectedList');
	}

	obj.style.backgroundColor = '#036341';
	obj.style.color = '#FFF';
	obj.classList.add('selectedList');

	// obj.style.backgroundColor = '#036341';
	// obj.style.color = '#FFF';
	// Quitar color verde
	// y letra blanca de todos los elementos de la lista;
	// Fondo verde y letra blanca para el elemento seleccionado (obj)
	// .addClass(); ????
}

function sendDistribListToFilter(boxId) {
	// Valida si se ha seleccionado alguna lista...
	var distribList = $('#distribListContainer .selectedList');

	if (distribList.length < 1) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe seleccionar una lista de distribuci\u00F3n para continuar' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	// Obtiene id y nombre del elemento
	var nombre = distribList[0].innerText.trim();
	var id = distribList[0].id;
	var _html = '';
	_html += '<div id="' + boxId + '-.-.-' + id + '" class="col4 divEliminar" style="margin-bottom: 15px;">';
	_html += '<a href="#" class="btnCerrar1" onclick="removeDomItem(this);"></a>';
	_html += '<p>' + nombre + '</p></div>';

	var listaLanzamientos = window.opener.document.getElementById('listaLanzamientos');
	$(listaLanzamientos).append(_html);
}

function editDistribList() {
	console.log('editDistribList()');

	var distribList = $('#distribListContainer .selectedList');
	if (distribList.length < 1) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe seleccionar una lista de distribuci\u00F3n para continuar' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	showLoadingSpinner();

	var nombre = distribList[0].innerText.trim();
	var id = distribList[0].id;

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/consultaListaCecos.json',
		dataType: 'json',
		data: {
			idLista: id,
			ceco: 0
		},
		success: function(data) {
			// console.log(data);

			// Se agrega opcion para seleccionar todos los cecos
			$('#modalDistribList #theList').html('');
			$('#modalDistribList #theList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
			$('#modalDistribList #theList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoDistribList" id="checkBoxTodos5" class="checkM" /><label for="checkBoxTodos5" >Seleccionar todo</label></div></th></tr>');

			for (var x = 0; x < data.length; x++) {
				var _html = '';

				if (data[x].activo === 1) {
					_html += '<tr><td style="text-align: left;"><div class="listBox">';
					_html += '<input type="checkbox" name="grupoDistribList" id="' + data[x].id_listaCeco + '" class="checkM" />';
					_html += '<label for="' + data[x].id_listaCeco + '" >' + '(' + data[x].id_ceco + ') ' + data[x].nombreCeco + '</label></div></td></tr>';
				} else {
					_html += '<tr><td style="text-align: left;"><div class="listBox">';
					_html += '<input type="checkbox" name="grupoDistribList" id="' + data[x].id_listaCeco + '" class="checkM" />';
					_html += '<label for="' + data[x].id_listaCeco + '" style="background-color: #FF9A9A;" >' + data[x].nombreCeco + '</label></div></td></tr>';
				}

				$('#modalDistribList #theList #listTable').append(_html);
			}

			$("#modalDistribList #checkBoxTodos5").change(function() {
				$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
			});

			$('#modalDistribList #distribTitle').html('Editar lista de distribuci\u00F3n');
			$('#modalDistribList #listNameModal').val($('.selectedList p').html());
			$('#modalDistribList').modal();
		},
		error: function(obj, errorText) {
			// console.log('error');
			console.log(errorText);
			loadDistributionList(0, idUsuario, negocio);
		},
		complete: function() {
			hideLoadingSpinner();
			// console.log('complete');
		}
	});
}

function saveChangesDistribList() {
	showLoadingSpinner();

	var _selectedList = $('#distribListContainer .selectedList');
	var _list = $('#theList .listBox');
	var _pass = true;

	if (_selectedList.length < 1) {
		_pass = false;
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe seleccionar una lista de distribuci\u00F3n para poder guardar los cambios' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	if (_list.length < 2) {
		_pass = false;
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe seleccionar al menos un elemento de la lista de distribuci\u00F3n para poder guardar los cambios' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	if (_pass) {
		var _idLista = _selectedList[0].id;
		var _nombre = $('#listNameModal').val();
		var _activo = 1;
		asyncSaveDistribChanges(_idLista, _nombre, _activo);
	}
}

function asyncSaveDistribChanges(_idLista, _nombre, _activo) {
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/actualizaListaDistribucion.json',
		dataType: 'json',
		data: {
			idLista: _idLista,
			nombre: _nombre,
			idUsuario: idUsuario,
			negocio: negocio, // Validar como se obtiene el negocio; obtenerlo de la opción seleccionada
			activo: _activo
		},
		success: function(data) {
			// console.log(data);
			if (data.status === '0') {
				// actualizaListaCecos.json

				var _list = $('#theList .listBox');
				var _idListaCecosEliminar = [];
				var _idListaCecosMantener = 0;

				for (var x = 1; x < _list.length; x++) {
					var _input = $(_list[x]).find('input');
					if (!$(_input).is(':checked')) {
						_idListaCecosEliminar.push(_input[0].id);
					} else {
						_idListaCecosMantener++;
					}
				}

				if (_idListaCecosMantener < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No es posible guardar una lista de distribui\u00F3n vac\u00EDa' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});

					// Validar
					hideLoadingSpinner();
					loadDistributionList(0, idUsuario, negocio);
					$('.btnCerrar').click();

					return false;
				} else {
					if (_idListaCecosEliminar.length > 0) {
						for (var x = 0; x < _idListaCecosEliminar.length; x++) {
							var _idListaCeco = _idListaCecosEliminar[x];
							var _selectedList = $('#distribListContainer .selectedList');
							var idLista = _selectedList[0].id;

							$.ajax({
								type: 'get',
								cache: false,
								url: contextPath + '/central/pedestalDigital/actualizaListaCecos.json',
								dataType: 'json',
								data: {
									idListaCecos: _idListaCeco,
									idLista: idLista,
									ceco: 0, // Se manda cero, por que se ignora el valor; Solo se toma en cuenta el valor idListaCeco
									activo: 0
								},
								success: function(data) {
									// console.log(data);
								},
								error: function(obj, errorText) {
									console.log('error');
								},
								complete: function() {
									console.log('complete');
								}
							});
						}
					}

					hideLoadingSpinner();
					loadDistributionList(0, idUsuario, negocio);
					$('.btnCerrar').click();
				}
			} else {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No fue posible actualizar los datos de la lista de distribui\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});

				console.log('Error al actualizar los datos de la lista de distribui\u00F3n');
				// loadDistributionList(0, idUsuario, negocio);
			}
		},
		error: function(obj, errorText) {
			// console.log('error');
			console.log('Error al actualizar los datos de la lista de distribui\u00F3n');
			// loadDistributionList(0, idUsuario, negocio);
		},
		complete: function() {
			// console.log('complete');
			hideLoadingSpinner();
		}
	});
}
function addFileToCargarCsvInput() {
	$('#cargaCsvInput').click();
}

function validaArchivosDropzone() {
	showLoadingSpinner();
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/obtieneCategorias.json',
		dataType: 'json',
		async: false,
		data: {
			nivel: 1,
			dependeDe: 0
		},
		success: function(data) {
			var list = $('.onyx-dropzone-info');
			var categories = data; // ['Categoría 1', 'Categoría 2', 'Categoría 3', 'Categoría 4', 'Categoría 5'];

			if (list.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Debes tener al menos un archivo agregado para poder continuar con el proceso' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			$('#modalCarga2 #cargaContainer').html('');
			$('#modalCarga2 #cargaContainer').append('<table id="listTable" class="tblGeneral" style="width: 94%;"></table>');
			$('#modalCarga2 #listTable').append('<thead><tr><th><strong>Nombre</strong></th><th><strong>Categor\u00EDa</strong></th><th><strong>Estatus</strong></th></tr></thead>');
			$('#modalCarga2 #listTable').append('<tbody id="tbody"><tbody>');

			for (var x = 0; x < list.length; x++) {
				var parent = $(list[x]).parent();
				var nombre = '';

				$('#modalCarga2 #tbody').append('<tr id="trRow' + (x+1) + '"></tr>');

				// TODO: Se agregan los nombres de los archivos a cargar
				if ($(parent).hasClass('type-jpg') || $(parent).hasClass('type-png')) {
					nombre = $(list[x]).find('img').attr('alt');
				} else if ($(parent).hasClass('type-pdf')) {
					nombre = $(list[x]).find('span').html();
				} else if ($(parent).hasClass('type-mp4')) {
					nombre = $(list[x]).find('span').html();
				}

				$('#modalCarga2 #trRow' + (x+1)).append('<td style="text-align: left; width: 40%;"><strong>' + nombre + '</strong></div></div></td>');

				// TODO: Se agregan las categorías para cada archivo...
				$('#modalCarga2 #trRow' + (x+1)).append('<td style="text-align: left; width: 45%;"><select id="listaCategorias' + (x+1) + '" style="display: inline-block;" onchange="getDocumentosPorCategoria(this, ' + (x+1) + ');">');

				for (var y = 0; y < categories.length; y++) {
					$('#modalCarga2 #listaCategorias' + (x+1)).append('<option value="' + categories[y].idTipoDoc + '-.-.-' + categories[y].nivel + '">' + categories[y].nombreDoc + '</option>');
				}

				$('#modalCarga2 #trRow' + (x+1)).append('</select></td>');

				// TODO: Se Agregan campos para el check
				$('#modalCarga2 #trRow' + (x+1)).append('<td id="check' + (x+1) + '" style="width: 15%;"></td>');
			}

			$('#modalCarga2 #cargaContainer').hide().show(0);
			$('#modalCarga2 #docContainer').hide();
			$('#modalCarga2').css('margin-top', '-25vh');
			$('#modalCarga2').css('font-size', '14px');

			getDocumentosPorCategoria($('#modalCarga2 #listaCategorias1')[0], 1);
			$('#modalCarga2').modal();
		},
		error: function(obj, errorText) {
			console.log(errorText);
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function showPreviewOfFile(obj) {
	// Obtiene indice de obj
	var index = $(obj).parent().index();
	var _url = blobToUrl(dzDataList[index].data);

	// TODO: Validar el resto de formatos permitidos
	if (dzDataList[$(obj).parent().index()].filetype === 'image/jpeg') {
		// Obtiene titulo y lo asigna a div#vpTitle
		$('#vpTitle').html('');
		$('#vpTitle').html($(obj).find('img').attr('alt'));

		// Crea elemento y se lo agrega a div#vpContainer
		$('#vpContainer').html('');
		$('#vpContainer').html('<img id="vpImg" src="" style="width: 80vw;" height: inherit;>');

		// Se agrega el data a img#vpImg
		$('#vpImg').attr('src', _url);
	} else if (dzDataList[$(obj).parent().index()].filetype === 'video/mp4') {
		// Obtiene titulo y lo asigna a div#vpTitle
		$('#vpTitle').html('');
		$('#vpTitle').html($(obj).find('.details span')[0].innerHTML);

		// Crea elemento y se lo agrega a div#vpContainer
		$('#vpContainer').html('');
		$('#vpContainer').html('<video width="85%" controls><source id="vpVideo" src="" type="video/mp4">Tu navegador no es compatible con la reproducci\u00F3n de v\u00EDdeos.</video>');

		// Se agrega el data a img#vpImg
		$('#vpVideo').attr('src', _url);
	} else if (dzDataList[$(obj).parent().index()].filetype === 'application/pdf') {
		// Obtiene titulo y lo asigna a div#vpTitle
		$('#vpTitle').html('');
		$('#vpTitle').html($(obj).find('.details span')[0].innerHTML);

		// Crea elemento y se lo agrega a div#vpContainer
		$('#vpContainer').html('');
		$('#vpContainer').html('<iframe id="vpPdf" src="" style="width: 75vw; height: 75vh;" />');

		// Se agrega el data a img#vpImg
		$('#vpPdf').attr('src', _url);
	} else {
		// Obtiene titulo y lo asigna a div#vpTitle
		$('#vpTitle').html('');
		$('#vpTitle').html($(obj).find('img').attr('alt'));

		// Crea elemento y se lo agrega a div#vpContainer
		$('#vpContainer').html('');
		$('#vpContainer').html('<img id="vpImg" src="" style="width: 75vw;">');

		// Se agrega el data a img#vpImg
		$('#vpImg').attr('src', _url);
	}

	// Muestra modal
	$('#vistaPrevia').modal();
}

function blobToUrl(str) {
	var base64str = str.split(',')[1];

	// decode base64 string, remove space for IE compatibility
	var binary = atob(base64str.replace(/\s/g, ''));
	var len = binary.length;
	var buffer = new ArrayBuffer(len);
	var view = new Uint8Array(buffer);

	for (var i = 0; i < len; i++) {
	    view[i] = binary.charCodeAt(i);
	}

	// create the blob object with content-type "application/pdf"               
	var blob = new Blob( [view], { type: "application/pdf" });
	// var url = URL.createObjectURL(blob);

	return URL.createObjectURL(blob);
}

function deleteFileDataFromArray(obj) {
	var index = $(obj).parent().parent().parent().parent().index();
	dzDataList.splice(index, 1);

	if ($('.onyx-dropzone-info').length === 1) {
		dzDataList = [];
	}
}

function getDocumentosPorCategoria(obj, id) {
	showLoadingSpinner();

	ref = id;
	var values = obj.options[obj.selectedIndex].value;
	var split = values.split('-.-.-');

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/obtieneCategorias.json',
		dataType: 'json',
		async: false,
		data: {
			dependeDe: split[0],
			nivel: (parseInt(split[1]) + 1)
		},
		success: function(data) {
			// console.log('success');

			$('#modalCarga2 #listDoc').html('');
			$('#modalCarga2 #listDoc').append('<table id="docTable" class="tblGeneral" style="width: 94%;"></table>');

			if (data.length > 0) {
				for (var x = 0; x < data.length; x++) {
					$('#modalCarga2 #listDoc #docTable').append('<tr><td style="text-align: left;"><div id="row' + (x+1) + '" class="listBox"></div></td></tr>');
					$('#modalCarga2 #listDoc #docTable #row' + (x+1)).append('<input type="radio" name="' + data[x].dependeDe + '" id="' + data[x].idTipoDoc + '">');
					$('#modalCarga2 #listDoc #docTable #row' + (x+1)).append('<label for="' + data[x].idTipoDoc + '" style="margin-left: 15px;"><strong>' + data[x].nombreDoc + '</strong></label>');
				}
			} else {
				$('#modalCarga2 #listDoc #docTable').append('<tr><td style="text-align: left;"><div id="row1" class="listBox"></div></td></tr>');
				$('#modalCarga2 #listDoc #docTable #row1').append('<input type="radio" name="empty" id="0">');
				$('#modalCarga2 #listDoc #docTable #row1').append('<label for="0" style="margin-left: 15px;"><strong>NO HAY DOCUMENTOS EN ESTA CATEGOR\u00CDA</strong></label>');
			}

			$('#modalCarga2 #docContainer').show();
		},
		error: function(obj, errorText) {
			hideLoadingSpinner();
			console.log(errorText);
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function saveSelectedDoc() {
	var isChecked = $('#listDoc').find('[type="radio"]').is(':checked');
	var hiddenList = $('#listDoc').find('[type="radio"]:checked');
	var docName = $('#docName').val();
	var list = $('#listDoc').find('[type="radio"]');

	_ref = ref;
	docName = docName.trim();

	if (hiddenList.length === 1 && $(hiddenList[0]).attr('id') === '0') {

		// Se valida que en el caso en el que la categoría no tenga documentos
		// Solo se pueda guardar la información del campo de texto;
		// Esto es, que solo se pueda agregar un documento a la categoría

		if (docName.length > 0) {
			// TODO: DOCUMENTO NUEVO
			console.log('Documento nuevo');
			var dependeDe = $('#listaCategorias1').val();
			dependeDe = dependeDe.split('-.-.-');
			$('#listTable #check' + _ref).html('<p id="' + docName +'-.-.-' + 0 + '-.-.-' + dependeDe[0] + '" style="color: #006341; margin-bottom: 0px;">&#10004;</p>');
			$('#docContainer').hide();
			$('#listDoc').html('');
			$('#docName').val('');
		} else {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Debe escribir un nombre v\u00E1lido en el cuadro de texto' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}
	} else {
		if (isChecked) {
			if (docName.length > 0) {
				// TODO: Se valida si se sustituye el documento o se agrega un nuevo documento
				var confirmAlert = Swal.mixin({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>¡Atención!</strong></div>' +
						'Debe seleccionar alguna opci\u00F3n de la lista de documentos disponibles, o en caso de ser un documento nuevo, escribir el nombre en el recuadro de abajo.' +
						'</div>',
					showCloseButton: false,
					showCancelButton: true,
					cancelButtonText: 'Sustituir documento',
					confirmButtonText: 'Documento nuevo',
					confirmButtonColor: '#006341'
				});

				confirmAlert.fire().then((result) => {
					if (result.value) {
						// TODO: DOCUMENTO NUEVO
						console.log('Documento nuevo');
						var dependeDe = $('#listaCategorias1').val();
						dependeDe = dependeDe.split('-.-.-');
						$('#listTable #check' + _ref).html('<p id="' + docName +'-.-.-' + 0 + '-.-.-' + dependeDe[0] + '" style="color: #006341; margin-bottom: 0px;">&#10004;</p>');
						$('#docContainer').hide();
						$('#listDoc').html('');
						$('#docName').val('');
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						// TODO: SUSTITUCIÓN DE DOCUMENTO
						console.log('Sustituir documento');
						docName = $('#listDoc').find('[type="radio"]:checked').parent().find('strong').html();
						$('#listTable #check' + _ref).html('<p id="' + docName +'-.-.-' + hiddenList[0].id + '" style="color: #006341; margin-bottom: 0px;">&#10004;</p>');
						$('#docContainer').hide();
						$('#listDoc').html('');
						$('#docName').val('');
					}
				});
			} else {
				// TODO: SUSTITUCIÓN DE DOCUMENTO
				console.log('Sustituir documento');
				docName = $('#listDoc').find('[type="radio"]:checked').parent().find('strong').html();
				$('#listTable #check' + _ref).html('<p id="' + docName +'-.-.-' + hiddenList[0].id + '" style="color: #006341; margin-bottom: 0px;">&#10004;</p>');
				$('#docContainer').hide();
				$('#listDoc').html('');
				$('#docName').val('');
			}
		} else {
			if (docName.length > 0) {
				// TODO: DOCUMENTO NUEVO
				console.log('Documento nuevo');
				var dependeDe = $('#listaCategorias1').val();
				dependeDe = dependeDe.split('-.-.-');
				$('#listTable #check' + _ref).html('<p id="' + docName +'-.-.-' + 0 + '-.-.-' + dependeDe[0] + '" style="color: #006341; margin-bottom: 0px;">&#10004;</p>');
				$('#docContainer').hide();
				$('#listDoc').html('');
				$('#docName').val('');
			} else {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Debe seleccionar alguna opci\u00F3n de la lista de documentos disponibles, o en caso de ser un documento nuevo, escribir el nombre en el recuadro de abajo.' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}
		}
	}
}

function validaTipoEnvio(obj) {
	var selected = $(obj).find(':checked');
	if (selected[0].index === 0) {
		// Tipo de envio
		$('#divFecha').hide();
		$('#divHora').hide();
		$('#divDPVF').hide();
	} else if (selected[0].index === 1) {
		// Instantaneo
		$('#divFecha').hide();
		$('#divHora').hide();
		$('#divDPVF').show();
	} else if (selected[0].index === 2) {
		// Batch
		$('#divFecha').hide();
		$('#divHora').hide();
		$('#divDPVF').show();
	} else if (selected[0].index === 3) {
		// Calendarizado
		$('#divFecha').show();
		$('#divHora').show();
		$('#divDPVF').show();
	}
}

function saveDocumentData() {
	var trList = $('#modalCarga2 #listTable').find('tbody tr');
	var check = 0;
	jsonDocs = [];

	for (var x = 0; x < trList.length; x++) {
		var tdCheck = $(trList[x]).find('td');
		
		if ($(tdCheck[2]).find('p').length > 0) {
			check++;
		}
	}

	if (trList.length === check) {
		// TODO: Almacena información de los documentos

		if (jsonDocs.length > 0) {
			jsonDocs = [];
		}

		for (var x = 0; x < trList.length; x++) {
			var tdObj = $(trList[x]).find('td');
			var date = new Date();
			var splitDN = $(tdObj[2]).find('p').attr('id');
			splitDN = splitDN.split('-.-.-');
			var splitFT = $(tdObj[0]).find('strong').html();
			splitFT = splitFT.split('.');

			var ogFileName = $(tdObj[0]).find('strong').html();
			var fileType = splitFT[1];
			var documentName = splitDN[0];
			var documentId = splitDN[1];
			var idCategory = $(tdObj[1]).find(':checked').attr('value').split('-.-.-')[0];
			var fileSize = dzDataList[x].filesize;
			var idCategoria = splitDN[2];

			var _json = {
					'ogFileName': ogFileName,
					'documentName': documentName,
					'documentId': documentId,
					'idCategoria': idCategoria,
					'idCategory': idCategory,
					'fileType': fileType,
					'fileSize': fileSize
			};

			jsonDocs.push(_json);
		}

		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/upload/saveDocDataInSession.json',
			dataType: 'json',
			async: false,
			data: {
				data: encodeURI(JSON.stringify(jsonDocs))
			},
			success: function(data) {
				var _json = data;
				if (_json.code === '0') {
					validaDocs = true;
					console.log(jsonDocs);
					$('#modalCarga2 .btnCerrar').click();
				} else {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No se pudo guardar la informaci\u00F3n de los documentos, intente nuevamente.' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo guardar la informaci\u00F3n de los documentos, intente nuevamente.' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	} else {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Se debe especificar la categor\u00EDa y el nombre de todos los documentos para poder continuar' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}
}

function saveDistribData() {
	jsonDistrib = [];

	// TODO: Crear JSON con datos de todos los destinos
		// Lista de Cecos
		// Listas de Listas de distribucion (plop)
		// Listas de id geograficos separados por nivel.

	var distribList = $('#listaLanzamientos').find('.divEliminar');
	if (distribList.length < 1) {
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe especificar primero un destino para poder continuar.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	for (var x = 0; x < distribList.length; x++) {
		// TODO: Se separan los objetos por tipo
		// TODO: se convierten en JSON y se almacenan en jsonDistrib

		var objId = $(distribList[x]).attr('id');
		var tipo = objId.split('-.-.-')[0];
		var data = objId.split('-.-.-')[1];

		// 1 - CECOs
		// 2 - GEOs - Nivel (tipo)
		// 3 - Lista de distribucion
		// 4 - CSV

		if (tipo === '1') {
			var jsonCeco = {
				'tipo': '1',
				'nivel': '0',
				'data': data
			};

			jsonDistrib.push(jsonCeco);
		} else if (tipo === '2') {
			var nivel = objId.split('-.-.-')[2];

			// Nivel puede ser:
				// 1 - Pais
				// 2 - Estado
				// 3 - Municipio
				// 4 - Sucursal (CECO). Nota: Nivel 4 tratar como lista de cecos (jsonCeco)

			var jsonGeo = {
				'tipo': '2',
				'nivel': nivel, 
				'data': data
			};

			jsonDistrib.push(jsonGeo);
		} else if (tipo === '3') {
			var jsonDistribList = {
				'tipo': '3',
				'nivel': '0',
				'data': data
			};

			jsonDistrib.push(jsonDistribList);
		} else if (tipo === '4') {
			var jsonCSV = {
				'tipo': '4',
				'nivel': '0',
				'data': data
			};

			jsonDistrib.push(jsonCSV);
		} else if (tipo == undefined) {
			hideLoadingSpinner();
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Se debe validar la informaci\u00F3n de los destinos para poder continuar.' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}
	}

	var _cadena = encodeURI(JSON.stringify(jsonDistrib));
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/upload/saveDistribDataInSession.json',
		dataType: 'json',
		async: false,
		data: {
			data: _cadena
		},
		success: function(data) {
			// console.log(data)
			if (data.code === '0') {
				validaDistrib = true;
				console.log(jsonDistrib);
			} else {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo guardar la informaci\u00F3n de los destinos, intente nuevamente.' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}
		},
		error: function(obj, errorText) {
			hideLoadingSpinner();
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo guardar la informaci\u00EDn de los destinos, intente nuevamente.' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			// hideLoadingSpinner();
		}
	});
}

function saveEnvData() {
	var tipoEnvIndex = $('#tipoEnvio').find(':checked').index();
	jsonEnv = [];

	if (tipoEnvIndex === 0) {
		validaEnv = false;
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Falta especificar un tipo de env\u00EDo v\u00E1lido para poder continuar.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else {		
		var json;
		var _fecha = new Date();

		if (tipoEnvIndex === 1) {
			// Batch
			fecha = _fecha.setDate(_fecha.getDate() + 1);
			fecha = new Date(fecha);

			var _fechaVF;
			var _checkVF = $('#checkVF').is(':checked');
			if (_checkVF) {
				var aux = $('#dpVigenciaFinal').val();
				_fechaVF = aux.replace('/', '').replace('/', '');
			} else {
				_fechaVF = '';
			}

			json = {
					'id': 'B',
					'desc': 'Batch',
					'fecha': (fecha.getDate() < 10 ? "0" + fecha.getDate().toString() : fecha.getDate().toString()) + ((fecha.getMonth() + 1) < 10 ? "0" + (fecha.getMonth() + 1).toString() : (fecha.getMonth() + 1).toString()) + fecha.getFullYear().toString(),
					'hora': '02:00',
					'fechaVF': _fechaVF
			};
			jsonEnv.push(json);
		} else if (tipoEnvIndex === 2) {
			// Instantaneo

			var _fechaVF;
			var _checkVF = $('#checkVF').is(':checked');
			if (_checkVF) {
				var aux = $('#dpVigenciaFinal').val();
				_fechaVF = aux.replace('/', '').replace('/', '');
			} else {
				_fechaVF = '';
			}

			json = {
					'id': 'I',
					'desc': 'Instantaneo',
					'fecha': (_fecha.getDate() < 10 ? "0" + _fecha.getDate().toString() : _fecha.getDate().toString()) + ((_fecha.getMonth() + 1) < 10 ? "0" + (_fecha.getMonth() + 1).toString() : (_fecha.getMonth() + 1).toString()) + _fecha.getFullYear().toString(),
					'hora': '00:00',
					'fechaVF': _fechaVF
			};
			jsonEnv.push(json);
		} else if (tipoEnvIndex === 3) {
			// Calendarizado
			var fecha = $('#datepickerPD').val();
			fecha = fecha.split('/');

			var _fechaVF;
			var _checkVF = $('#checkVF').is(':checked');
			if (_checkVF) {
				var aux = $('#dpVigenciaFinal').val();
				_fechaVF = aux.replace('/', '').replace('/', '');
			} else {
				_fechaVF = '';
			}

			json = {
					'id': 'C',
					'desc': 'Calendarizado',
					'fecha': fecha[0] + fecha[1] + fecha[2],
					'hora': '00:00',
					'fechaVF': _fechaVF
			};
			jsonEnv.push(json);
		}

		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/upload/saveEnvDataInSession.json',
			dataType: 'json',
			async: false,
			data: {
				data: encodeURI(JSON.stringify(jsonEnv))
			},
			success: function(data) {
				var _json = data;
				if (_json.code === '0') {
					validaEnv = true;
					console.log(jsonEnv);
				} else {
					hideLoadingSpinner();
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No se pudo guardar la informaci\u00F3n del tipo de envio, intente nuevamente.' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}
			},
			error: function(obj, errorText) {
				console.log('No se pudo insertar el registro.');
			},
			complete: function() {
				// hideLoadingSpinner();
			}
		});
	}
}

function getAdminTerritorioList() {
	showLoadingSpinner();

	if (_nivelT === 0) {
		_containerP = 'Territorios';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 1) {
		_containerP = 'Zonas';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 2) {
		_containerP = 'Regiones';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 3) {
		_containerP = 'Sucursales';
		$('#btnListSigNivel').hide();
	}

	if (_cecos.length < 1 && _objBackup.length > 1 && _nivelT === 1) {
		// Titulo de la ventana modal
		$('#modalAdminList #adminTitle').html('');
		$('#modalAdminList #adminTitle').html('Lista de ' + _containerP);

		// Se limpia la lista
		$('#modalAdminList #adminList').html('');
		$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
		$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos1" class="checkM" /><label for="checkBoxTodos1" >Seleccionar todo</label></div></th></tr>');

		// Aqui se agregan las zonas especiales
		if (_objBackup.length > 1 && _objNameBackup.length > 1) {
			var objList = _objBackup.split(',');
			var objNameList = _objNameBackup.split(',');

			for (var x = 0; x < objList.length; x++) {
				_html = '';
				_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
				_html += '<label for="' + objList[x] + '" ><strong>' + objNameList[x] + '</strong></label></div></td></tr>';
				_html += '<tr><td style="text-align: left;"><div class="listBox">';
				_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + objList[x] + '" class="checkM" />';
				_html += '<label for="' + objList[x] + '" >' + objNameList[x] + '</label></div></td></tr>';
				$('#modalAdminList #adminList #listTable').append(_html);
			}
		}

		_nivelT++;

		// Para la opción 'Seleccionar todos'
		$("#modalAdminList #checkBoxTodos1").change(function() {
			$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
		});

		$('#modalAdminList #theList').append('<br>');
		$('#modalAdminList').modal();

		hideLoadingSpinner();
	} else if (_cecos.length > 0) {
		if (_nivelT === 0) {
			_objBackup = '';
			_objNameBackup = '';
		}

		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/consultaCecos.json',
			dataType: 'json',
			data: {
				tipo: 0,
				cecos: _cecos,
				nivel: _nivelT,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					$('#btnListSigNivel').show();
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				// Titulo de la ventana modal
				$('#modalAdminList #adminTitle').html('');
				$('#modalAdminList #adminTitle').html('Lista de ' + _containerP);

				// Se limpia la lista
				$('#modalAdminList #adminList').html('');
				$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos1" class="checkM" /><label for="checkBoxTodos1" >Seleccionar todo</label></div></th></tr>');

				_nivelT = data[0].tipoCeco;

				var actualId = data[0].cecoSuperior;
				var actualDesc = data[0].cecoSuperiorDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelC > 1) {
						if (actualId != data[x].cecoSuperior) {
							actualId = data[x].cecoSuperior;
							actualDesc = data[x].cecoSuperiorDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].cecoSuperior + '" ><strong>' + data[x].cecoSuperiorDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" >' + data[x].nombre + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" >' + data[x].nombre + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					}
				}

				// Aqui se agregan las zonas especiales
				if (_objBackup.length > 1 && _objNameBackup.length > 1) {
					var objList = _objBackup.split(',');
					var objNameList = _objNameBackup.split(',');

					for (var x = 0; x < objList.length; x++) {
						_html = '';
						_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
						_html += '<label for="' + objList[x] + '" ><strong>' + objNameList[x] + '</strong></label></div></td></tr>';
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + objList[x] + '" class="checkM" />';
						_html += '<label for="' + objList[x] + '" >' + objNameList[x] + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalAdminList #checkBoxTodos1").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalAdminList #theList').append('<br>');

				// Se muestra la ventana modal
				$('#modalAdminList').modal();
			},
			error: function(obj, errorText) {
				// console.log('No se pudo obtener la informacion...');
				$('#btnListSigNivel').show();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	}
}

function getAdminPaisList() {
	showLoadingSpinner();

	if (_nivelP === 0) {
		_containerP = 'Pa\u00EDs';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 1) {
		_containerP = 'Estados';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 2) {
		_containerP = 'Municipios';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 3) {
		_containerP = 'Sucursales';
		$('#btnListSigNivel').hide();
	} else {
		_containerP = 'Pa\u00EDs';
		$('#btnListSigNivel').show();
	}

	if (_nivelP === 3) {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneCecosPorGeo.json',
			dataType: 'json',
			data: {
				geo: _cecos,
				tipo: _nivelP,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					$('#btnListSigNivel').show();
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#modalAdminList .titModal').html('Lista de ' + _containerP);
				$('#modalAdminList #adminList').html('');
				$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos2" class="checkM" /><label for="checkBoxTodos2" >Seleccionar todo</label></div></th></tr>');

				_nivelP = data[0].tipo;

				var actualId = data[0].idDependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (actualId != data[x].idGeo) {
						actualId = data[x].idGeo;
						actualDesc = data[x].descGeo;
						makeDist = true;
					}

					if (makeDist) {
						_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
						_html += '<label for="' + data[x].idGeo + '" ><strong>' + data[x].descGeo + '</strong></label></div></td></tr>';
						makeDist = false;
					}

					_html += '<tr><td style="text-align: left;"><div class="listBox">';
					_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + data[x].idCeco + '-.-.-' + _nivelP + '" class="checkM" />';
					_html += '<label for="' + data[x].idCeco + '" >' + '(' + data[x].idCeco + ') ' + data[x].descCeco + '</label></div></td></tr>';
					$('#modalAdminList #adminList #listTable').append(_html);
				}

				// Para la opción 'Seleccionar todos'
				$("#modalAdminList #checkBoxTodos2").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalAdminList #adminList').append('<br>');

				// Se crea el modal
				$('#modalAdminList').modal();
			},
			error: function(obj, errorText) {
				//console.log('No se pudo obtener la informacion...');
				$('#btnListSigNivel').show();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			} 
		});
	} else {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/consultaPais.json',
			dataType: 'json',
			data: {
				tipo: 1,
				cecos: _cecos,
				nivel: _nivelP,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					$('#btnListSigNivel').show();
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#modalAdminList .titModal').html('Lista de ' + _containerP);
				$('#modalAdminList #adminList').html('');
				$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos2" class="checkM" /><label for="checkBoxTodos2" >Seleccionar todo</label></div></th></tr>');

				_nivelP = data[0].tipo;

				var actualId = data[0].idDependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelP > 1) {
						if (actualId != data[x].idDependeDe) {
							actualId = data[x].idDependeDe;
							actualDesc = data[x].dependeDeDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].idDependeDe + '" ><strong>' + data[x].dependeDeDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + _nivelP + '-.-.-' + data[x].id_geolocalizacion + '" class="checkM" />';
						_html += '<label for="' + _nivelP + '-.-.-' + data[x].id_geolocalizacion + '" >' + data[x].descripcion + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + _nivelP + '-.-.-' + data[x].id_geolocalizacion + '" class="checkM" />';
						_html += '<label for="' + _nivelP + '-.-.-' + data[x].id_geolocalizacion + '" >' + data[x].descripcion + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalAdminList #checkBoxTodos2").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalAdminList #adminList').append('<br>');

				// Se crea el modal
				$('#modalAdminList').modal();
			},
			error: function(obj, errorText) {
				// console.log('No se pudo obtener la informacion...');
				$('#btnListSigNivel').show();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			} 
		});
	}
}

function getAdminCategoriaList() {
	showLoadingSpinner();
	_containerC = 'Categor\u00EDas';

	if (_nivelC === 1) {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneCategorias.json',
			dataType: 'json',
			async: false,
			data: {
				nivel: _nivelC,
				dependeDe: 0
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				var categories = data;

				$('#modalAdminList .titModal').html('Lista de ' + _containerC);
				$('#modalAdminList #adminList').html('');
				$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos4" class="checkM" /><label for="checkBoxTodos4" >Seleccionar todo</label></div></th></tr>');

				_nivelC = data[0].nivel;

				var actualId = data[0].dependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelC > 1) {
						if (actualId != data[x].dependeDe) {
							actualId = data[x].dependeDe;
							actualDesc = data[x].dependeDeDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].dependeDe + '" ><strong>' + data[x].dependeDeDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" class="checkM" />';
						_html += '<label for="' + data[x].idTipoDoc + '-.-.-' + _nivelT + '" >' + data[x].nombreDoc + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" class="checkM" />';
						_html += '<label for="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" >' + data[x].nombreDoc + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalAdminList #checkBoxTodos4").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalAdminList #adminList').append('<br>');

				// Se crea el modal
				$('#modalAdminList').modal();
			},
			error: function(obj, errorText) {
				// console.log(errorText);
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	} else if (_nivelC === 2) {
		var _list = $('#modalAdminList #adminList').find('div.listBox');
		var _cecos = '';

		for (var x = 1; x < _list.length; x++) {
			var _input = $(_list[x]).find('input');

			if ($(_input).is(':checked')) {
				_cecos += _input[0].id.split('-.-.-')[0] + ',';
			}
		}

		if (_cecos.length < 1) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se ha seleccionado ning\u00FAn valor' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		} else {
			// Quita ultima coma de la cadena
			_cecos = _cecos.substring(0, _cecos.length - 1);
		}

		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneDocumentos.json',
			dataType: 'json',
			async: false,
			data: {
				nivel: _nivelC,
				dependeDe: _cecos
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				var categories = data;

				$('#modalAdminList .titModal').html('Lista de ' + _containerC);
				$('#modalAdminList #adminList').html('');
				$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos4" class="checkM" /><label for="checkBoxTodos4" >Seleccionar todo</label></div></th></tr>');

				_nivelC = data[0].nivel;

				var actualId = data[0].dependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelC > 1) {
						if (actualId != data[x].dependeDe) {
							actualId = data[x].dependeDe;
							actualDesc = data[x].dependeDeDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].dependeDe + '" ><strong>' + data[x].dependeDeDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" class="checkM" />';
						_html += '<label for="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" >' + data[x].nombreDoc + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" class="checkM" />';
						_html += '<label for="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" >' + data[x].nombreDoc + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalAdminList #checkBoxTodos4").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se oculta btnListSigNivel
				$('#btnListSigNivel').hide();

				// Se agrega <br> para organizar mejor elementos
				$('#modalAdminList #adminList').append('<br>');

				// Se crea el modal
				$('#modalAdminList').modal();
			},
			error: function(obj, errorText) {
				// console.log(errorText);
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	}
}

function nextLevelAdminList() {
	showLoadingSpinner();

	var _title = $('#modalAdminList #adminTitle').html();
	if (_title.includes('Territorios') || _title.includes('Zonas') || _title.includes('Regiones') || _title.includes('Sucursales')) {
		// Obtiene valores y actualia variables
		var _list = $('#modalAdminList #adminList').find('div.listBox');

		_cecos = '';
		_listName = '';
		_objBackup = '';
		_objNameBackup = '';
		_nivelC = _nivelT;

		for (var x = 1; x < _list.length; x++) {
			if ($(_list[x]).find('.checkM').length > 0 && $(_list[x]).find(':checked').length > 0 && _nivelC === 1) {
				var _input = $(_list[x]).find('input');
				var _label = $(_list[x]).find('label');
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');

				if (_nivelC === parseInt(_foo[1])) {
					_cecos += _foo[0] + ',';
					_listName += $(_label).html() + ',';
				} else {
					_objBackup += _foo[0] + ',';
					_objNameBackup += $(_label).html() + ',';
				}
			} else if ($(_list[x]).find('.checkM').length > 0 && $(_list[x]).find(':checked').length > 0 && _nivelC > 1) {
				var _input = $(_list[x]).find('input');
				var _label = $(_list[x]).find('label');
				var _foo = _input[0].id;
				// _foo = _foo.split('-.-.-');
				_cecos += _foo + ',';
				_listName += $(_label).html() + ',';
			}
		}
	} else if (_title.includes('Pa\u00EDs') || _title.includes('Estados') || _title.includes('Municipios')) {
		// Obtiene valores y actualia variables
		var _list = $('#modalAdminList #adminList').find('div.listBox');
		_cecos = '';
		_listName = '';

		for (var x = 1; x < _list.length; x++) {
			if ($(_list[x]).find('.checkM').length > 0 && $(_list[x]).find(':checked').length && _nivelP) {
				var _input = $(_list[x]).find('input');
				var _label = $(_list[x]).find('label');
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			}
		}
	} else {
		var _list = $('#modalAdminList #adminList').find('div.listBox');
		_cecos = '';
		_listName = '';

		for (var x = 1; x < _list.length; x++) {
			var _input = $(_list[x]).find('input');
			var _label = $(_list[x]).find('label');
			var _foo = _input[0].id;
			_foo = _foo.split('-.-.-');
			_cecos += _foo + ',';
			_listName += $(_label).html() + ',';
		}
	}

	// Obtiene valores y actualia variables
	/**
	var _list = $('#modalAdminList #adminList').find('div.listBox');
	var _title = $('#modalAdminList #adminTitle').html();

	_cecos = '';
	_listName = '';
	_objBackup = '';
	_objNameBackup = '';
	_nivelC = _nivelT;

	for (var x = 1; x < _list.length; x++) {
		var _input = $(_list[x]).find('input');
		var _label = $(_list[x]).find('label');

		if (_title.includes('Territorios') || _title.includes('Zonas') || _title.includes('Regiones') || _title.includes('Sucursales')) {
			var _foo = _input[0].id;
			_foo = _foo.split('-.-.-');

			if (_nivelC === parseInt(_foo[1])) {
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			} else {
				_objBackup += _foo[0] + ',';
				_objNameBackup += $(_label).html() + ',';
			}
		} else if (_title.includes('Pa\u00EDs') || _title.includes('Estados') || _title.includes('Municipios')) {
			var _foo = $(_input).attr('id');
			_foo = _foo.split('-.-.-');
			_cecos += _foo[0] + ',';
			_listName += $(_label).attr('id');
		}
	}
	*/

	if (_cecos.length < 1 && _objBackup.length < 1) {
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se ha seleccionado ning\u00FAn valor' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else {
		// Quita ultima coma de la cadena
		_cecos = _cecos.substring(0, _cecos.length - 1);
		_listName = _listName.substring(0, _listName.length - 1);
		_objBackup = _objBackup.substring(0, _objBackup.length - 1);
		_objNameBackup = _objNameBackup.substring(0, _objNameBackup.length - 1);

		var parent = $('#modalAdminList #adminList').parent();
		var title = $(parent).find('.titModal').html();

		if (title.includes('Categor\u00EDas')) {
			_nivelC++;
			getAdminCategoriaList();
		} else {
			if (title.includes('Territorios') || title.includes('Zonas') || title.includes('Regiones') || title.includes('Sucursales')) {
				getTerritorioAdminDataList();
			} else if (title.includes('Pa\u00EDs') || title.includes('Estados') || title.includes('Municipios')) {
				getGeoAdminDataList();
			}
		}
	}
}

function createAdminDestinyBox(boxId) {
	// Obtiene valores y actualia variables

	var _title = $('#modalAdminList #adminTitle').html();
	if (_title.includes('Territorios') || _title.includes('Zonas') || _title.includes('Regiones') || _title.includes('Sucursales')) {
		var _list = $('#modalAdminList #adminList').find('div.listBox'); // $('#modalAdminList #adminList').find(':checked');
		var _cecos = '';
		var _listName = '';
		_objBackup = '';
		_objNameBackup = '';
		_nivelC = _nivelT;

		for (var x = 1; x < _list.length; x++) {
			var _input = $(_list[x]).find('input');
			var _label = $(_list[x]).find('label');

			if (boxId === 1) {
				if ($(_input).is(':checked') && _nivelC === 1) {
					var _foo = _input[0].id;
					_foo = _foo.split('-.-.-');

					if (_nivelC === parseInt(_foo[1])) {
						_cecos += _foo[0] + ',';
						_listName += $(_label).html() + ',';
					} else {
						_objBackup += _foo[0] + ',';
						_objNameBackup += $(_label).html() + ',';
					}
				} else if ($(_input).is(':checked') && _nivelC > 1) {
					var _foo = _input[0].id;
					_foo = _foo.split('-.-.-');
					_cecos += _foo[0] + ',';
					_listName += $(_label).html() + ',';
				}
			} else if (boxId === 2) {
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			} else if (boxId === 3) {
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			}
		}
	} else if (_title.includes('Pa\u00EDs') || _title.includes('Estados') || _title.includes('Municipios')) {
		var _list = $('#modalAdminList #adminList').find('div.listBox');
		var _cecos = '';
		var _listName = '';

		for (var x = 1; x < _list.length; x++) {
			var _input = $(_list[x]).find('input');
			var _label = $(_list[x]).find('label');

			if ($(_input).is(':checked') && _nivelP) {
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			}
		}
	} else {

		var _list = $('#modalAdminList #adminList').find('div.listBox');
		_cecos = '';
		_listName = '';

		for (var x = 1; x < _list.length; x++) {
			var _input = $(_list[x]).find('input');
			var _label = $(_list[x]).find('label');

			if ($(_input).is(':checked')) {
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			}
		}
	}

	if (_cecos.length < 1 && _objBackup.length < 1) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se ha seleccionado ning\u00FAn valor' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else {
		// Quita ultima coma de la cadena
		_cecos = _cecos.substring(0, _cecos.length - 1);
		_listName = _listName.substring(0, _listName.length - 1);
		_objBackup = _objBackup.substring(0, _objBackup.length - 1);
		_objNameBackup = _objNameBackup.substring(0, _objNameBackup.length - 1);
	}

	var _arrayCecos = _cecos.split(',');
	var _arrayNames = _listName.split(',');

	for (var x = 0; x < _arrayCecos.length; x++) {
		var _html = '';

		if (boxId === 1) {
			_html += '<div id="T-.-.-' + boxId + '-.-.-' + _arrayCecos[x] + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1%;">';
		} else if (boxId === 2) {
			_html += '<div id="P-.-.-' + _arrayCecos[x] + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1%;">';
		} else if (boxId === 3) {
			_html += '<div id="C-.-.-' + _arrayCecos[x] + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1%;">';
		}

		_html += '<a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">' + _arrayNames[x] + '</a>';
		$('#listaLanzamientos').append(_html);
	}

	if (boxId === 1) {
		// Inhabilita input para busqueda por folio y por sucursal
		$('#buscarPorFolio').prop('disabled', true);
		$('#buscarPorSucursal').prop('disabled', true);

		// Inhabilita la función click en botones
		$('#btnPais').off('click');

		// Se cambian clases de elementos, para mostrar que se encuentran inhabilitados
		$('#btnPais').removeClass('btnDistrib');
		$('#btnPais').addClass('btnDistribDis');
	} else if (boxId === 2) {
		// Inhabilita input para busqueda por folio y por sucursal
		$('#buscarPorFolio').prop('disabled', true);
		$('#buscarPorSucursal').prop('disabled', true);

		// Inhabilita la función click en botones
		$('#btnTerritorio').off('click');
		$('#btnPais').off('click');

		// Se cambian clases de elementos, para mostrar que se encuentran inhabilitados
		$('#btnTerritorio').removeClass('btnDistrib');
		$('#btnTerritorio').addClass('btnDistribDis');
		$('#btnPais').removeClass('btnDistrib');
		$('#btnPais').addClass('btnDistribDis');
	} else if (boxId === 3) {
		// Inhabilita input para busqueda por folio y por sucursal
		$('#buscarPorFolio').prop('disabled', true);
	}

	$('.modalCloseImg').click();
}

function getTerritorioAdminDataList() {
	showLoadingSpinner();

	if (_nivelT === 0) {
		_containerP = 'Territorios';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 1) {
		_containerP = 'Zonas';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 2) {
		_containerP = 'Regiones';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 3) {
		_containerP = 'Sucursales';
		$('#btnListSigNivel').hide();
	}

	if (_cecos.length < 1 && _objBackup.length > 1 && _nivelT === 1) {
		$('.titModal').html('Lista de ' + _containerP);
		$('#modalAdminList #adminList').html('');
		$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
		$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerP + '" id="checkBoxTodos1" class="checkM" /><label for="checkBoxTodos1" >Seleccionar todo</label></div></th></tr>');

		// Aqui se agregan las zonas especiales
		if (_objBackup.length > 1 && _objNameBackup.length > 1) {
			var objList = _objBackup.split(',');
			var objNameList = _objNameBackup.split(',');

			for (var x = 0; x < objList.length; x++) {
				_html = '';
				_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
				_html += '<label for="' + objList[x] + '" ><strong>' + objNameList[x] + '</strong></label></div></td></tr>';
				_html += '<tr><td style="text-align: left;"><div class="listBox">';
				_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + objList[x] + '" class="checkM" />';
				_html += '<label for="' + objList[x] + '" >' + objNameList[x] + '</label></div></td></tr>';
				$('#modalAdminList #adminList #listTable').append(_html);
			}
		}

		_nivelT++;

		// Para la opción 'Seleccionar todos'
		$('#modalAdminList #checkBoxTodos1').change(function() {
			$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
		});

		// Se agrega <br> para organizar mejor elementos
		$('#modalAdminList #adminList').append('<br>');

		// Se crea el modal
		$('#modalAdminList').modal();

		hideLoadingSpinner();
	} else if (_cecos.length > 0) {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/consultaCecos.json',
			dataType: 'json',
			data: {
				tipo: 1,
				cecos: _cecos,
				nivel: _nivelT,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					$('#btnListSigNivel').show();
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('.titModal').html('Lista de ' + _containerP);
				$('#modalAdminList #adminList').html('');
				$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerP + '" id="checkBoxTodos1" class="checkM" /><label for="checkBoxTodos1" >Seleccionar todo</label></div></th></tr>');

				_nivelT = data[0].tipoCeco;

				var actualId = data[0].cecoSuperior;
				var actualDesc = data[0].cecoSuperiorDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelT > 1) {
						if (actualId != data[x].cecoSuperior) {
							actualId = data[x].cecoSuperior;
							actualDesc = data[x].cecoSuperiorDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].cecoSuperior + '" ><strong>' + data[x].cecoSuperiorDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + data[x].idCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '" >' + data[x].nombre + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + data[x].idCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '" >' + data[x].nombre + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					}
				}

				// Aqui se agregan las zonas especiales
				if (_objBackup.length > 1 && _objNameBackup.length > 1) {
					var objList = _objBackup.split(',');
					var objNameList = _objNameBackup.split(',');

					for (var x = 0; x < objList.length; x++) {
						_html = '';
						_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
						_html += '<label for="' + objList[x] + '" ><strong>' + objNameList[x] + '</strong></label></div></td></tr>';
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + objList[x] + '" class="checkM" />';
						_html += '<label for="' + objList[x] + '" >' + objNameList[x] + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$('#modalAdminList #checkBoxTodos1').change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalAdminList #adminList').append('<br>');

				// Se crea el modal
				$('#modalAdminList').modal();
			},
			error: function(obj, errorText) {
				console.log('No se pudo obtener la informacion...');
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	}
}

function getGeoAdminDataList() {
	showLoadingSpinner();

	if (_nivelP === 0) {
		_containerP = 'Pa\u00EDs';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 1) {
		_containerP = 'Estados';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 2) {
		_containerP = 'Municipios';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 3) {
		_containerP = 'Sucursales';
		$('#btnListSigNivel').hide();
	} else {
		_containerP = 'Pa\u00EDs';
		$('#btnListSigNivel').show();
	}

	if (_nivelP === 3) {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneCecosPorGeo.json',
			dataType: 'json',
			data: {
				geo: _cecos,
				tipo: _nivelP,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#modalAdminList .titModal').html('Lista de ' + _containerP);
				$('#modalAdminList #adminList').html('');
				$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos3" class="checkM" /><label for="checkBoxTodos3" >Seleccionar todo</label></div></th></tr>');

				_nivelP = data[0].tipo;

				var actualId = data[0].idDependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (actualId != data[x].idGeo) {
						actualId = data[x].idGeo;
						actualDesc = data[x].descGeo;
						makeDist = true;
					}

					if (makeDist) {
						_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
						_html += '<label for="' + data[x].idGeo + '" ><strong>' + data[x].descGeo + '</strong></label></div></td></tr>';
						makeDist = false;
					}

					_html += '<tr><td style="text-align: left;"><div class="listBox">';
					_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + data[x].idCeco + '-.-.-' + _nivelP + '" class="checkM" />';
					_html += '<label for="' + data[x].idCeco + '" >' + '(' + data[x].idCeco + ') ' + data[x].descCeco + '</label></div></td></tr>';
					$('#modalAdminList #adminList #listTable').append(_html);
				}

				// Para la opción 'Seleccionar todos'
				$("#modalAdminList #checkBoxTodos3").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalAdminList #adminList').append('<br>');

				// Se crea el modal
				$('#modalAdminList').modal();
			},
			error: function(obj, errorText) {
				// Se habilita el botón de Siguiente, en los casos donde el servicio devuelve error
				$('#btnListSigNivel').show();

				console.log('No se pudo obtener la informacion: ' + errorText);
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			} 
		});
	} else {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/consultaPais.json',
			dataType: 'json',
			data: {
				tipo: 1,
				cecos: _cecos,
				nivel: _nivelP,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#modalAdminList .titModal').html('Lista de ' + _containerP);
				$('#modalAdminList #adminList').html('');
				$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos3" class="checkM" /><label for="checkBoxTodos3" >Seleccionar todo</label></div></th></tr>');

				_nivelP = data[0].tipo;

				var actualId = data[0].idDependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelP > 1) {
						if (actualId != data[x].idDependeDe) {
							actualId = data[x].idDependeDe;
							actualDesc = data[x].dependeDeDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].idDependeDe + '" ><strong>' + data[x].dependeDeDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + data[x].id_geolocalizacion + '-.-.-' + _nivelP + '" class="checkM" />';
						_html += '<label for="' + data[x].id_geolocalizacion + '-.-.-' + _nivelP + '" >' + data[x].descripcion + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + data[x].id_geolocalizacion + '-.-.-' + _nivelP + '" class="checkM" />';
						_html += '<label for="' + data[x].id_geolocalizacion + '-.-.-' + _nivelP + '" >' + data[x].descripcion + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalAdminList #checkBoxTodos3").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalAdminList #adminList').append('<br>');

				// Se crea el modal
				$('#modalAdminList').modal();
			},
			error: function(obj, errorText) {
				console.log('No se pudo obtener la informacion: ' + errorText);
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			} 
		});
	}
}

function validaEstatusDeEnvio(obj) {
	var _val;
	var _exist = false;
	if (obj.value === 'En progreso') {
		_val = 'EN PROGRESO';
	} else if (obj.value === 'Completado') {
		_val = 'COMPLETADO';
	} else {
		_val = 'CANCELADO';
	}

	$('#listaLanzamientos').find('.divEliminar').each(function(index, _obj) {
		if (_obj.id === 'EN PROGRESO' || _obj.id === 'COMPLETADO' || _obj.id === 'CANCELADO') {
			var _a = $(_obj).find('a');
			_obj.id = _val;
			$(_a).html(obj.value);
			_exist = true;
		}
	});

	if (!_exist) {
		$('#listaLanzamientos').prepend('<div id="' + _val + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1% 5px 1%;"><a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">' + obj.value + '</a></div>');
		// $('#listaLanzamientos').append('<div id="' + _val + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1% 5px 1%;"><a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">' + obj.value + '</a></div>');
	}
}

function validaTipoDeEnvio(obj) {
	var _val;
	var _exist = false;
	var _list = $('#listaLanzamientos').find('.divEliminar');

	if (obj.value === 'En batch') {
		_val = 'BATCH';
	} else if (obj.value === 'Instantaneo' || obj.value === 'Instant\u00E1neo') {
		_val = 'INSTANTANEO';
	} else {
		_val = 'CALENDARIZADO';
	}

	_list.each(function(index, _obj) {
		if (_obj.id === 'BATCH' || _obj.id === 'INSTANTANEO' || _obj.id === 'CALENDARIZADO') {
			var _a = $(_obj).find('a');
			_obj.id = _val;
			$(_a).html(obj.value);
			_exist = true;
		}
	});

	if (!_exist) {
		$('#listaLanzamientos').prepend('<div id="' + _val + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1% 5px 1%;"><a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">' + obj.value + '</a></div>');
		// $('#listaLanzamientos').append('<div id="' + _val + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1% 5px 1%;"><a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">' + obj.value + '</a></div>');
	}
}

function validateFilters() {
	showLoadingSpinner();

	var buscarPorFolio = $('#buscarPorFolio').val();
	var buscarPorSucursal = $('#buscarPorSucursal').val();
	var list = $('#listaLanzamientos').find('.divEliminar');

	if (buscarPorFolio.trim().length > 0 || buscarPorSucursal.trim().length > 0) {
		if (buscarPorFolio.length > 0) {
			// busqueda por folio
			console.log('filterByFolio(' + buscarPorFolio + ')');
			filterByFolio(buscarPorFolio);
		} else if (buscarPorSucursal.length > 0) {
			// busqueda por sucursal
			console.log('filterBySucursal(' + buscarPorSucursal + ')');
			filterBySucursal(buscarPorSucursal);
		}
	} else {
		var a, b, c;
		$.each(list, function(key, val) {
			if ($(val).attr('id').includes('T-.-.-')) {
				a = true;
				return false;
			} else if ($(val).attr('id').includes('P-.-.-')) {
				b = true;
				return false;
			} else {
				c = true;
				// return false;
			}
		});

		if (a) {
			// TODO: Filtro para obtener folios por territorio
			setTimeout(function() {
				console.log('filterByTerritorio(a)');
				filterByTerritorio();
			}, 250);
		} else if (b) {
			// TODO: Filtro para obtener folios por país
			setTimeout(function() {
				console.log('filterByPais(b)');
				filterByPais();
			}, 250);
		} else if (c) {
			setTimeout(function() {
				console.log('filterByTerritorio(c)');
				filterByTerritorio();
			}, 250);
		}
	}
}

function filterByFolio(idFolio) {
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getFolioData.json',
		dataType: 'json',
		data: {
			idFolio: idFolio
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			} else {
				$('#tblFolios').html('');
				$('#tblFolios').html(
						'<thead style="display: block; width: 100%;">' +
						'<tr style="display: inline-table; width: 100%">' +
							'<th style="width: 15%;">Folio de envío</th>' +
							'<th style="width: 20%;">Tipo de envío</th>' +
							'<th style="width: 20%;">Fecha / Hora de creación de folio</th>' +
							'<th style="width: 20%;">Sucursales afectadas</th>' +
							'<th style="width: 15%;">Estatus de envío</th>' +
						'</tr>' +
					'</thead>' +
					'<tbody id="tblBody" style="display: block; width: 100%; max-height: 240px; overflow-y: scroll;"></tbody>'
						);

				if (data[0].idFolio > 0) {
					for (var x = 0; x < data.length; x++) {
						$('#tblFolios #tblBody').append(
								'<tr id="' + data[x].idFolio + '" style="display: inline-table; width: 100%">' +
								'<td style="width: 15%;"><a href="#" style="cursor: pointer;" onclick="showFolioDetail(' + data[x].idFolio + ')">' + data[x].idFolio + '</a></td>' +
								'<td style="width: 20%;">' + data[x].tipoEnvio + '</td>' +
								'<td style="width: 20%;">' + data[x].fechaEnvio + ' Hrs</td>' +
								'<td style="width: 20%;">' + data[x].sucursalesAfectadas + ' sucursales</td>' +
								'<td style="width: 15%;">' + data[x].estatus + '</td>' +
							'</tr>'
							);
					}

					// Se habilitan todos los elementos
					$('#buscarPorFolio').prop('disabled', false);
					$('#buscarPorSucursal').prop('disabled', false);
					$('#datepickerAdminEnv').prop('disabled', false);
					$('#estatusEnvio').prop('disabled', false);
					$('#tipoEnvio').prop('disabled', false);

					// Se habilitan botones.
					$('#btnTerritorio').click(function(e) {
						_nivelT = 0;
						_cecos = '0';
						_boxId = 1;
						getAdminTerritorioList();
					});

					$('#btnPais').click(function(e) {
						_nivelP = 0;
						_cecos = '0';
						_boxId = 2;
						getAdminPaisList();
					});

					$('#btnCategoria').click(function(e) {
						_nivelC = 1;
						_cecos = '0';
						_boxId = 3;
						getAdminCategoriaList();
					});

					// Se cambian clases para mostrar que el elemento se encuentra habilitado
					$('#btnTerritorio').addClass('btnDistrib');
					$('#btnTerritorio').removeClass('btnDistribDis');
					$('#btnPais').addClass('btnDistrib');
					$('#btnPais').removeClass('btnDistribDis');
					$('#btnCategoria').addClass('btnDistrib');
					$('#btnCategoria').removeClass('btnDistribDis');
				}
			}
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informacion: ' + errorText);
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function filterBySucursal(idSucursal) {
	var idSucursal = $('#buscarPorSucursal').val();

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/convertEconNumToCeco.json',
		dataType: 'json',
		data: {
			numEco: idSucursal
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			} else {
				var _ceco = '';
				var _categoria = '';
				var _fecha = $('#datepickerAdminEnv').val();
				var _estatus = $('#estatusEnvio').val();
				var _tipo = $('#tipoEnvio').val();

				for (var x = 0; x < data.length; x++) {
					_ceco += data[x] + ',';
				}

				_ceco = _ceco.slice(0, -1);

				//TODO: Obtiene id de categorias
				var _catList = $('#listaLanzamientos').find('.divEliminar');
				if (_catList.length > 0) {
					for (var x = 0; x < _catList.length; x++) {
						var _catObj = $(_catList[x]);
						var _catId = $(_catObj).attr('id');

						if (_catId.includes('C-.-.-')) {
							_catIdList = _catId.split('-.-.-');
							_categoria += _catIdList[2] + ',';
						}
					}

					_categoria = _categoria.substring(0, _categoria.length - 1);
				} else {
					_categoria = '';
				}

				// TODO: Valida fecha
				if (_fecha.length > 0) {
					if (!isValidDate(_fecha)) {
						Swal.fire({
							html: '<div style="width: 100%;">' +
								'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
								'Necesita insertar una fecha v\u00E1lida' +
								'</div>',
							showCloseButton: false,
							confirmButtonText: 'Aceptar',
							confirmButtonColor: '#006341'
						});
						return false;
					}

					// TODO: Modificar fecha para tener formato ddMMyyyy
					var _fechaList = _fecha.split('/');
					_fecha = _fechaList[0] + _fechaList[1] + _fechaList[2];
				} else {
					_fecha = '';
				}

				// TODO: Valida Tipo
				if (_tipo === 'Batch') {
					_tipo = 'B';
				} else if (_tipo === 'Instantaneo' || _tipo === 'Instant\u00E1neo') {
					_tipo = 'I';
				} else if (_tipo === 'Calendarizado') {
					_tipo = 'C';
				} else {
					_tipo = "B";
				}

				if (_estatus === null || _estatus === undefined) {
					_estatus = 'En progreso';
				}

				$.ajax({
					type: 'get',
					cache: false,
					url: contextPath + '/central/pedestalDigital/getFolioDataPorTerritorio.json',
					dataType: 'json',
					data: {
						ceco: _ceco,
						categoria: _categoria,
						fecha: _fecha,
						estatus: _estatus.toUpperCase(),
						tipo: _tipo
					},
					success: function(data) {
						if (data.length < 1) {
							Swal.fire({
								html: '<div style="width: 100%;">' +
									'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
									'No hay datos que mostrar' +
									'</div>',
								showCloseButton: false,
								confirmButtonText: 'Aceptar',
								confirmButtonColor: '#006341'
							});
							return false;
						} else {
							if (data[0].idFolio === 0) {
								$('#tblFolios').html('');
								$('#tblFolios').html(
										'<thead style="display: block; width: 100%;">' +
										'<tr style="display: inline-table; width: 100%">' +
											'<th style="width: 15%;">Folio de envío</th>' +
											'<th style="width: 20%;">Tipo de envío</th>' +
											'<th style="width: 20%;">Fecha / Hora de creación de folio</th>' +
											'<th style="width: 20%;">Sucursales afectadas</th>' +
											'<th style="width: 15%;">Estatus de envío</th>' +
										'</tr>' +
									'</thead>' +
									'<tbody id="tblBody" style="display: block; width: 100%; max-height: 240px; overflow-y: scroll;"></tbody>'
										);
							} else {
								$('#tblFolios').html('');
								$('#tblFolios').html(
										'<thead style="display: block; width: 100%;">' +
										'<tr style="display: inline-table; width: 100%">' +
											'<th style="width: 15%;">Folio de envío</th>' +
											'<th style="width: 20%;">Tipo de envío</th>' +
											'<th style="width: 20%;">Fecha / Hora de creación de folio</th>' +
											'<th style="width: 20%;">Sucursales afectadas</th>' +
											'<th style="width: 15%;">Estatus de envío</th>' +
										'</tr>' +
									'</thead>' +
									'<tbody id="tblBody" style="display: block; width: 100%; max-height: 240px; overflow-y: scroll;"></tbody>'
										);

								for (var x = 0; x < data.length; x++) {
									$('#tblFolios #tblBody').append(
											'<tr id="' + data[x].idFolio + '" style="display: inline-table; width: 100%;">' +
											'<td style="width: 15%;"><a href="#" onclick="showFolioDetail(' + data[x].idFolio + ')">' + data[x].idFolio + '</a></td>' +
											'<td style="width: 20%;">' + data[x].tipoEnvio + '</td>' +
											'<td style="width: 20%;">' + data[x].fechaEnvio + ' Hrs</td>' +
											'<td style="width: 20%;">' + data[x].sucursalesAfectadas + ' sucursales</td>' +
											'<td style="width: 15%;">' + data[x].estatus + '</td>' +
										'</tr>'
										);
								}
							}

							// Si el campo se encuentra vacío, se habilitan todos los elementos
							$('#buscarPorFolio').prop('disabled', false);
							$('#buscarPorSucursal').prop('disabled', false);
							$('#estatusEnvio').prop('disabled', false);
							$('#tipoEnvio').prop('disabled', false);

							// Se habilitan botones.
							$('#btnTerritorio').click(function(e) {
								_nivelT = 0;
								_cecos = '0';
								_boxId = 1;
								getAdminTerritorioList();
							});

							$('#btnPais').click(function(e) {
								_nivelP = 0;
								_cecos = '0';
								_boxId = 2;
								getAdminPaisList();
							});

							$('#btnCategoria').click(function(e) {
								_nivelC = 1;
								_cecos = '0';
								_boxId = 3;
								getAdminCategoriaList();
							});

							// Se cambian clases para mostrar que el elemento se encuentra habilitado
							$('#btnTerritorio').addClass('btnDistrib');
							$('#btnTerritorio').removeClass('btnDistribDis');
							$('#btnPais').addClass('btnDistrib');
							$('#btnPais').removeClass('btnDistribDis');
							$('#btnCategoria').addClass('btnDistrib');
							$('#btnCategoria').removeClass('btnDistribDis');
						}
					},
					error: function(obj, errorText) {
						console.log('No se pudo obtener la informaci\u00F3n: ' + errorText);
						Swal.fire({
							html: '<div style="width: 100%;">' +
								'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
								'No se pudo obtener la informaci\u00F3n' +
								'</div>',
							showCloseButton: false,
							confirmButtonText: 'Aceptar',
							confirmButtonColor: '#006341'
						});
						return false;
					},
					complete: function() {
						hideLoadingSpinner();
					}
				});
			}
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informaci\u00F3n: ' + errorText);
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function filterByTerritorio() {
	showLoadingSpinner();

	var _ceco = '';
	var _categoria = '';
	var _fecha = $('#datepickerAdminEnv').val();
	var _estatus = $('#estatusEnvio').val();
	var _tipo = $('#tipoEnvio').val();

	// TODO: Valida fecha
	if (_fecha.length > 0) {
		if (!isValidDate(_fecha)) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Necesita insertar una fecha v\u00E1lida' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}

		// TODO: Modificar fecha para tener formato ddMMyyyy
		var _fechaList = _fecha.split('/');
		_fecha = _fechaList[0] + _fechaList[1] + _fechaList[2];
	} else {
		_fecha = '';
	}

	// TODO: Valida Tipo
	if (_tipo === 'Batch') {
		_tipo = 'B';
	} else if (_tipo === 'Instantaneo' || _tipo === 'Instant\u00E1neo') {
		_tipo = 'I';
	} else if (_tipo === 'Calendarizado') {
		_tipo = 'C';
	} else {
		_tipo = "B";
	}

	if (_estatus === null || _estatus === undefined) {
		_estatus = 'En progreso';
	}

	//TODO: Obtiene id de categorias y cecos
	var _catList = $('#listaLanzamientos').find('.divEliminar');
	if (_catList.length > 0) {
		for (var x = 0; x < _catList.length; x++) {
			var _catObj = $(_catList[x]);
			var _catId = $(_catObj).attr('id');

			if (_catId.includes('C-.-.-')) {
				_catIdList = _catId.split('-.-.-');
				_categoria += _catIdList[1] + ',';
			}

			if (_catId.includes('T-.-.-')) {
				_catIdList = _catId.split('-.-.-');
				_ceco += _catIdList[2] + ',';
			}
		}

		_categoria = _categoria.substring(0, _categoria.length - 1);
		_ceco = _ceco.substring(0, _ceco.length - 1);
	} else {
		_categoria = '';
		_ceco = '';
	}

	// TODO: Crea tabla vacía
	$('#tblFolios').html('');
	$('#tblFolios').html(
			'<thead style="display: block; width: 100%;">' +
			'<tr style="display: inline-table; width: 100%">' +
				'<th style="width: 15%;">Folio de envío</th>' +
				'<th style="width: 20%;">Tipo de envío</th>' +
				'<th style="width: 20%;">Fecha / Hora de creación de folio</th>' +
				'<th style="width: 20%;">Sucursales afectadas</th>' +
				'<th style="width: 15%;">Estatus de envío</th>' +
			'</tr>' +
		'</thead>' +
		'<tbody id="tblBody" style="display: block; width: 100%; max-height: 240px; overflow-y: scroll;"></tbody>'
			);

	var _listLength = 400;
	var _cecoList = _ceco.split(',');
	if (_cecoList.length > _listLength) {
		// TODO: Divide la cadena de cecos en grupos de N cecos (definido por _listLength)

		var number = Math.ceil(_cecoList.length / _listLength);
		var _cecosArray = [];
		var _cl = '';
		var acc = 0;
		var count = 0;

		for (acc; acc < _cecoList.length; acc++) {
			_cl += _cecoList[acc] + ',';
			count++;

			if (count === _listLength || acc === _cecoList.length -1) {
				count = 0;
				_cl = _cl.substring(0, _cl.length - 1);
				_cecosArray.push(_cl);
				_cl = '';
			}
		}
		$.each(_cecosArray, function (key, val) {
			makeTableWithFolioTerri(val, _categoria, _fecha, _estatus.toUpperCase(), _tipo);
		});
		hideLoadingSpinner();
	} else {
		$.each([1], function (key, val) {
			makeTableWithFolioTerri(_ceco, _categoria, _fecha, _estatus.toUpperCase(), _tipo);
		});
		hideLoadingSpinner();
	}
}

function filterByPais() {
	showLoadingSpinner();

	var _geo = '';
	var _nivel = 1;
	var _categoria = '';
	var _fecha = $('#datepickerAdminEnv').val();
	var _estatus = $('#estatusEnvio').val();
	var _tipo = $('#tipoEnvio').val();

	// TODO: Valida fecha
	if (_fecha.length > 0) {
		if (!isValidDate(_fecha)) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Necesita insertar una fecha v\u00E1lida' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}

		// TODO: Modificar fecha para tener formato ddMMyyyy
		var _fechaList = _fecha.split('/');
		_fecha = _fechaList[0] + _fechaList[1] + _fechaList[2];
	} else {
		_fecha = '';
	}

	// TODO: Valida Tipo
	if (_tipo === 'Batch') {
		_tipo = 'B';
	} else if (_tipo === 'Instantaneo' || _tipo === 'Instant\u00E1neo') {
		_tipo = 'I';
	} else if (_tipo === 'Calendarizado') {
		_tipo = 'C';
	} else {
		_tipo = "B";
	}

	if (_estatus === null || _estatus === undefined) {
		_estatus = 'En progreso';
	}

	//TODO: Obtiene id de categorias y cecos
	var _geoList = $('#listaLanzamientos').find('.divEliminar');
	if (_geoList.length > 0) {
		for (var x = 0; x < _geoList.length; x++) {
			var _catObj = $(_geoList[x]);
			var _catId = $(_catObj).attr('id');

			if (_catId.includes('C-.-.-')) {
				_catIdList = _catId.split('-.-.-');
				_categoria += _catIdList[1] + ',';
			}

			if (_catId.includes('P-.-.-')) {
				_catIdList = _catId.split('-.-.-');
				_geo += _catIdList[1] + ',';
			}
		}

		_categoria = _categoria.substring(0, _categoria.length - 1);
		_geo = _geo.substring(0, _geo.length - 1);
	} else {
		_categoria = '';
		_geo = '';
	}

	// TODO: Obtiene nivel
	var a;
	var list = $('#listaLanzamientos').find('.divEliminar');
	$.each(list, function(key, val) {
		if ($(val).attr('id').includes('P-.-.-')) {
			var b = $(val).attr('id').split('-.-.-');
			_nivel = b[2];
			return false;
		}
	});

	// TODO: Crea tabla vacía
	$('#tblFolios').html('');
	$('#tblFolios').html(
			'<thead style="display: block; width: 100%;">' +
			'<tr style="display: inline-table; width: 100%">' +
				'<th style="width: 15%;">Folio de envío</th>' +
				'<th style="width: 20%;">Tipo de envío</th>' +
				'<th style="width: 20%;">Fecha / Hora de creación de folio</th>' +
				'<th style="width: 20%;">Sucursales afectadas</th>' +
				'<th style="width: 15%;">Estatus de envío</th>' +
			'</tr>' +
		'</thead>' +
		'<tbody id="tblBody" style="display: block; width: 100%; max-height: 240px; overflow-y: scroll;"></tbody>'
			);

	var _listLength = 400;
	var _geoList = _geo.split(',');
	if (_geoList.length > _listLength) {
		// TODO: Divide la cadena de cecos en grupos de N cecos (definido por _listLength)

		var number = Math.ceil(_geoList.length / _listLength);
		var _geoArray = [];
		var _cl = '';
		var acc = 0;
		var count = 0;

		for (acc; acc < _geoList.length; acc++) {
			_cl += _geoList[acc] + ',';
			count++;

			if (count === _listLength || acc === _geoList.length -1) {
				count = 0;
				_cl = _cl.substring(0, _cl.length - 1);
				_geoArray.push(_cl);
				_cl = '';
			}
		}
		$.each(_geoArray, function (key, val) {
			makeTableWithFolioGeo(val, 0, _categoria, _fecha, _estatus.toUpperCase(), _tipo);
		});
		hideLoadingSpinner();
	} else {
		// TODO: Ejecuta function que construye los datos de la tabla
		$.each([1], function (key, val) {
			makeTableWithFolioGeo(_geo, 0, _categoria, _fecha, _estatus.toUpperCase(), _tipo);
		});
		hideLoadingSpinner();
	}
}

function makeTableWithFolioGeo(geo, nivel, categorias, fecha, estatus, tipo) {
	$.ajax({
		type: 'get',
		async: false,
		cache: false,
		url: contextPath + '/central/pedestalDigital/getFolioDataPorGeografia.json',
		dataType: 'json',
		data: {
			geo: geo,
			nivel: nivel,
			categoria: categorias,
			fecha: fecha,
			estatus: estatus,
			tipo: tipo
		},
		success: function(data) {
			if (data.length > 0 && data[0].idFolio > 0) {
				for (var x = 0; x < data.length; x++) {
					var pass = true;
					var list = $(' #tblBody').find('tr');
					if (list.length > 1) {
						for (var y = 1; y < list.length; y++) {
							var folio = $(list[y]).attr('id');
							if (parseInt(folio) === data[x].idFolio) {
								// console.log('El folio ' + folio + ' ya se encuentra en la lista');
								pass = false;
								break;
							}
						}
					}

					if (pass) {
						$('#tblFolios #tblBody').append(
								'<tr id="' + data[x].idFolio + '" style="display: inline-table; width: 100%;">' +
								'<td style="width: 15%;"><a href="#" onclick="showFolioDetail(' + data[x].idFolio + ')">' + data[x].idFolio + '</a></td>' +
								'<td style="width: 20%;">' + data[x].tipoEnvio + '</td>' +
								'<td style="width: 20%;">' + data[x].fechaEnvio + ' Hrs</td>' +
								'<td style="width: 20%;">' + data[x].sucursalesAfectadas + ' sucursales</td>' +
								'<td style="width: 15%;">' + data[x].estatus + '</td>' +
								'</tr>'
							);
					}
				}

				exportData = {
					tipoFiltro: 'G',
					geo: geo,
					nivel: nivel,
					categoria: categorias,
					fecha: fecha,
					estatus: estatus,
					tipo: tipo
				};
			}

			// Si el campo se encuentra vacío, se habilitan todos los elementos
			$('#buscarPorFolio').prop('disabled', false);
			$('#buscarPorSucursal').prop('disabled', false);
			$('#datepickerAdminEnv').prop('disabled', false);
			$('#estatusEnvio').prop('disabled', false);
			$('#tipoEnvio').prop('disabled', false);

			// Se habilitan botones.
			$('#btnTerritorio').click(function(e) {
				_nivelT = 0;
				_cecos = '0';
				_boxId = 1;
				getAdminTerritorioList();
			});

			$('#btnPais').click(function(e) {
				_nivelP = 0;
				_cecos = '0';
				_boxId = 2;
				getAdminPaisList();
			});

			$('#btnCategoria').click(function(e) {
				_nivelC = 1;
				_cecos = '0';
				_boxId = 3;
				getAdminCategoriaList();
			});

			// Se cambian clases para mostrar que el elemento se encuentra habilitado
			$('#btnTerritorio').addClass('btnDistrib');
			$('#btnTerritorio').removeClass('btnDistribDis');
			$('#btnPais').addClass('btnDistrib');
			$('#btnPais').removeClass('btnDistribDis');
			$('#btnCategoria').addClass('btnDistrib');
			$('#btnCategoria').removeClass('btnDistribDis');
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informaci\u00F3n: ' + errorText);
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			$('#listaLanzamientos').html('');
			$('#datepickerAdminEnv').val('');
			$('#estatusEnvio').val('');
			$('#tipoEnvio').val('');
			$('#buscarPorFolio').val('');
			$('#buscarPorSucursal').val('');

			var _dkEstatusEnvio = new Dropkick('#estatusEnvio');
			var _dkTipoEnvio = new Dropkick('#tipoEnvio');
			_dkTipoEnvio.select(0);
			_dkEstatusEnvio.select(0);

			// hideLoadingSpinner();
		}
	});
}

function makeTableWithFolioTerri(cecos, categorias, fecha, estatus, tipo) {
	$.ajax({
		type: 'get',
		async: false,
		cache: false,
		url: contextPath + '/central/pedestalDigital/getFolioDataPorTerritorio.json',
		dataType: 'json',
		data: {
			ceco: cecos,
			categoria: categorias,
			fecha: fecha,
			estatus: estatus,
			tipo: tipo
		},
		success: function(data) {
			if (data.length > 0  && data[0].idFolio > 0) {
				for (var x = 0; x < data.length; x++) {
					var pass = true;
					var list = $(' #tblBody').find('tr');
					if (list.length > 1) {
						for (var y = 1; y < list.length; y++) {
							var folio = $(list[y]).attr('id');
							if (parseInt(folio) === data[x].idFolio) {
								// console.log('El folio ' + folio + ' ya se encuentra en la lista');
								pass = false;
								break;
							}
						}
					}

					if (pass) {
						$('#tblFolios #tblBody').append(
								'<tr id="' + data[x].idFolio + '" style="display: inline-table; width: 100%;">' +
								'<td style="width: 15%;"><a href="#" onclick="showFolioDetail(' + data[x].idFolio + ')">' + data[x].idFolio + '</a></td>' +
								'<td style="width: 20%;">' + data[x].tipoEnvio + '</td>' +
								'<td style="width: 20%;">' + data[x].fechaEnvio + ' Hrs</td>' +
								'<td style="width: 20%;">' + data[x].sucursalesAfectadas + ' sucursales</td>' +
								'<td style="width: 15%;">' + data[x].estatus + '</td>' +
								'</tr>'
							);
					}
				}

				exportData = {
					tipoFiltro: 'T',
					ceco: cecos,
					categoria: categorias,
					fecha: fecha,
					estatus: estatus,
					tipo: tipo
				};
			}

			// Si el campo se encuentra vacío, se habilitan todos los elementos
			$('#buscarPorFolio').prop('disabled', false);
			$('#buscarPorSucursal').prop('disabled', false);
			$('#datepickerAdminEnv').prop('disabled', false);
			$('#estatusEnvio').prop('disabled', false);
			$('#tipoEnvio').prop('disabled', false);

			// Se habilitan botones.
			$('#btnTerritorio').click(function(e) {
				_nivelT = 0;
				_cecos = '0';
				_boxId = 1;
				getAdminTerritorioList();
			});

			$('#btnPais').click(function(e) {
				_nivelP = 0;
				_cecos = '0';
				_boxId = 2;
				getAdminPaisList();
			});

			$('#btnCategoria').click(function(e) {
				_nivelC = 1;
				_cecos = '0';
				_boxId = 3;
				getAdminCategoriaList();
			});

			// Se cambian clases para mostrar que el elemento se encuentra habilitado
			$('#btnTerritorio').addClass('btnDistrib');
			$('#btnTerritorio').removeClass('btnDistribDis');
			$('#btnPais').addClass('btnDistrib');
			$('#btnPais').removeClass('btnDistribDis');
			$('#btnCategoria').addClass('btnDistrib');
			$('#btnCategoria').removeClass('btnDistribDis');
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informaci\u00F3n: ' + errorText);
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			// hideLoadingSpinner();
		}
	});
}

function showFolioDetail(idFolio) {
	console.log('showFolioDetail');
	showLoadingSpinner();

	var _idFolio = idFolio;
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getFolioDetail.json',
		dataType: 'json',
		data: {
			idFolio: _idFolio,
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay documentos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			// Se limpia la lista
			$('#modalDetalle #tbodyFolioDetail').html('');
			var arrDocs = [];
			var listaDocumentos = [];
			var idDocs = '';
			_jsonData = data;

			for (var x = 0; x < data.length; x++) {
				var _html = '';
				var totalSucursales = parseInt(data[x].totalSucursal);
				var a = parseInt(data[x].docVisSuc);
				var b = parseInt(data[x].docNVisSuc);
				var c = (a / totalSucursales) * 100;
				var ruta = '/' + data[x].idCategoria + '/' + data[x].nombreArchivo;
				var formato = data[x].nombreArchivo.split('.')[1];

				var _json = {
					ruta: ruta,
					formato: formato,
					documento: data[x].nombreDocumento
				};

				arrDocs.push({
					visible: data[x].visibleSucursal,
					fecha: data[x].fechaVisualizacion,
					fechaFin: data[x].fechaFin
				});

				listaDocumentos.push(data[x].nombreDocumento);

				idDocs += data[x].idDocumento + ',';

				_html += '<tr id="row' + x + '">';
				_html += '<td><a href="#" style="cursor: pointer;" id="vpBtn">' + data[x].nombreDocumento + '</a></td>';
				_html += '<td>' + data[x].categoriaDesc + '</td>';

				if (c < 100) {
					_html += '<td><div class="avance"><div class="barra"><div style="width: ' + c.toFixed(2) + '% !important;"><div class="bgAma1"></div></div></div>' + c.toFixed(2) + '%</div></td>';
				} else if (c === 100) {
					_html += '<td><div class="avance"><div class="barra"><div style="width: ' + c.toFixed(2) + '% !important;"><div class="bgAma"></div></div></div>' + c.toFixed(2) + '%</div></td>';
				}

				/**
				else if (c < 30) {
					_html += '<td><div class="avance"><div class="barra"><div style="width: ' + c.toFixed(2) + '% !important;"><div class="bgAma3"></div></div></div>' + c.toFixed(2) + '%</div></td>';
				}
				*/

				_html += '<td>' + data[x].visibleSucursalDesc + '</td>';
				_html += '<td>' + data[x].fechaVisualizacion + ' Hrs</td>';
				_html += '</tr>';

				$('#modalDetalle #tbodyFolioDetail').append(_html);
				$('#modalDetalle #tbodyFolioDetail #row' + x + ' #vpBtn').click({ _json }, showPreviewFolioDetail);
			}

			var _jsonFA = {
				idFolio: _idFolio,
				documentos: arrDocs
			}

			idDocs = idDocs.substring(0, idDocs.length - 1);

			$('#exportarHistorialModal').off();
			$('#exportarHistorialModal').click({ _idFolio }, exportFolioDetailExcel);

			$('#modificarFolioDetail').off();
			$('#modificarFolioDetail').click({ _idFolio: _idFolio, jsonFA: _jsonFA, listaDocumentos: listaDocumentos }, showModalEditFolio);

			$('#modalDetalle #sIdFolio').html('');
			$('#modalDetalle #sIdFolio').html(_idFolio);
			$('#modalDetalle #faIdDocs').val(idDocs);
			$('#modalDetalle').modal({
				escClose: false
			});
		},
		error: function(obj, errorText) {
			_jsonData = '';
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No hay documentos que mostrar' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function valInputFilter(obj) {
	var _id = $(obj).attr('id');
	var _val = $(obj).val();

	var _datepickerAdminEnv = $('#datepickerAdminEnv');
	var _estatusEnvio = $('#estatusEnvio');
	var _tipoEnvio = $('#tipoEnvio');

	if (_id === 'buscarPorFolio') {
		// TODO: buscarPorFolio
		if (_val.trim().length > 0) {
			// Si el campo NO se encuentra vacío, se inhabilitan el resto de los elementos
			$('#buscarPorFolio').prop('disabled', false);
			$('#buscarPorSucursal').prop('disabled', true);
			$('#datepickerAdminEnv').prop('disabled', true);
			$('#estatusEnvio').prop('disabled', true);
			$('#tipoEnvio').prop('disabled', true);

			// Inhabilita la función click en botones
			$('#btnTerritorio').off('click');
			$('#btnPais').off('click');
			$('#btnCategoria').off('click');

			// Se cambian clases de elementos, para mostrar que se encuentran inhabilitados
			$('#btnTerritorio').removeClass('btnDistrib');
			$('#btnTerritorio').addClass('btnDistribDis');
			$('#btnPais').removeClass('btnDistrib');
			$('#btnPais').addClass('btnDistribDis');
			$('#btnCategoria').removeClass('btnDistrib');
			$('#btnCategoria').addClass('btnDistribDis');
		} else {
			// Si el campo se encuentra vacío, se habilitan todos los elementos
			$('#buscarPorFolio').prop('disabled', false);
			$('#buscarPorSucursal').prop('disabled', false);
			$('#datepickerAdminEnv').prop('disabled', false);
			$('#estatusEnvio').prop('disabled', false);
			$('#tipoEnvio').prop('disabled', false);

			// Se habilitan botones.
			$('#btnTerritorio').click(function(e) {
				_nivelT = 0;
				_cecos = '0';
				_boxId = 1;
				getAdminTerritorioList();
			});

			$('#btnPais').click(function(e) {
				_nivelP = 0;
				_cecos = '0';
				_boxId = 2;
				getAdminPaisList();
			});

			$('#btnCategoria').click(function(e) {
				_nivelC = 1;
				_cecos = '0';
				_boxId = 3;
				getAdminCategoriaList();
			});

			// Se cambian clases para mostrar que el elemento se encuentra habilitado
			$('#btnTerritorio').addClass('btnDistrib');
			$('#btnTerritorio').removeClass('btnDistribDis');
			$('#btnPais').addClass('btnDistrib');
			$('#btnPais').removeClass('btnDistribDis');
			$('#btnCategoria').addClass('btnDistrib');
			$('#btnCategoria').removeClass('btnDistribDis');
		}
	} else if (_id === 'buscarPorSucursal') {
		// TODO: buscarPorSucursal
		if (_val.trim().length > 0) {
			$('#buscarPorSucursal').prop('disabled', false);
			$('#buscarPorFolio').prop('disabled', true);

			// Inhabilita la función click en botones
			$('#btnTerritorio').off('click');
			$('#btnPais').off('click');

			// Se cambian clases de elementos, para mostrar que se encuentran inhabilitados
			$('#btnTerritorio').removeClass('btnDistrib');
			$('#btnTerritorio').addClass('btnDistribDis');
			$('#btnPais').removeClass('btnDistrib');
			$('#btnPais').addClass('btnDistribDis');
		} else {
			$('#buscarPorFolio').prop('disabled', false);
			$('#buscarPorSucursal').prop('disabled', false);

			// Se habilitan botones.
			$('#btnTerritorio').click(function(e) {
				_nivelT = 0;
				_cecos = '0';
				_boxId = 1;
				getAdminTerritorioList();
			});

			$('#btnPais').click(function(e) {
				_nivelP = 0;
				_cecos = '0';
				_boxId = 2;
				getAdminPaisList();
			});

			// Se cambian clases para mostrar que el elemento se encuentra habilitado
			$('#btnTerritorio').addClass('btnDistrib');
			$('#btnTerritorio').removeClass('btnDistribDis');
			$('#btnPais').addClass('btnDistrib');
			$('#btnPais').removeClass('btnDistribDis');
		}
	}
}

function nuevaBusqueda() {
	// Si el campo se encuentra vacío, se habilitan todos los elementos
	$('#buscarPorFolio').prop('disabled', false);
	$('#buscarPorSucursal').prop('disabled', false);
	$('#datepickerAdminEnv').prop('disabled', false);
	$('#estatusEnvio').prop('disabled', false);
	$('#tipoEnvio').prop('disabled', false);

	// Se habilitan botones.
	$('#btnTerritorio').click(function(e) {
		_nivelT = 0;
		_cecos = '0';
		_boxId = 1;
		getAdminTerritorioList();
	});

	$('#btnPais').click(function(e) {
		_nivelP = 0;
		_cecos = '0';
		_boxId = 2;
		getAdminPaisList();
	});

	$('#btnCategoria').click(function(e) {
		_nivelC = 1;
		_cecos = '0';
		_boxId = 3;
		getAdminCategoriaList();
	});

	// Se cambian clases para mostrar que el elemento se encuentra habilitado
	$('#btnTerritorio').addClass('btnDistrib');
	$('#btnTerritorio').removeClass('btnDistribDis');
	$('#btnPais').addClass('btnDistrib');
	$('#btnPais').removeClass('btnDistribDis');
	$('#btnCategoria').addClass('btnDistrib');
	$('#btnCategoria').removeClass('btnDistribDis');

	// Se seleccionan los valores por defecto, se limpiam campos.
	$('#buscarPorFolio').val('');
	$('#buscarPorSucursal').val('');
	$('#datepickerAdminEnv').val('');

	var dkEstatusEnvio = new Dropkick('#estatusEnvio');
	var dkTipoEnvio = new Dropkick('#tipoEnvio');

	dkTipoEnvio.select(0);
	dkEstatusEnvio.select(0);

	$('#listaLanzamientos').html('');
	$('#listaLanzamientos').append('<div id="EN PROGRESO" class="col3 divEliminar" style="height: 30px; margin: 10px 1% 5px 1%;"><a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">En progreso</a></div>');
	$('#listaLanzamientos').append('<div id="INSTANTANEO" class="col3 divEliminar" style="height: 30px; margin: 10px 1% 5px 1%;"><a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">Instant\u00E1neo</a></div>');

	$('#tblFolios').html('');
	$('#tblFolios').html(
			'<thead style="display: block; width: 100%;">' +
			'<tr style="display: inline-table; width: 100%">' +
				'<th style="width: 15%;">Folio de envío</th>' +
				'<th style="width: 20%;">Tipo de envío</th>' +
				'<th style="width: 20%;">Fecha / Hora de creación de folio</th>' +
				'<th style="width: 20%;">Sucursales afectadas</th>' +
				'<th style="width: 15%;">Estatus de envío</th>' +
			'</tr>' +
		'</thead>' +
		'<tbody id="tblBody" style="display: block; width: 100%; max-height: 240px; overflow-y: scroll;"></tbody>'
			);
}

function getTerriListEstTableta() {
	showLoadingSpinner();

	if (_nivelT === 0) {
		_containerT = 'Territorios';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 1) {
		_containerT = 'Zonas';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 2) {
		_containerT = 'Regiones';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 3) {
		_containerT = 'Sucursales';
		$('#btnListSigNivel').hide();
	}

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/consultaCecos.json',
		dataType: 'json',
		data: {
			tipo: 0,
			cecos: _cecos,
			nivel: _nivelT,
			negocio: _negocio
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			// Titulo de la ventana modal
			$('#modalAdminList #adminTitle').html('');
			$('#modalAdminList #adminTitle').html('Lista de ' + _containerT);

			// Se limpia la lista
			$('#modalAdminList #adminList').html('');
			$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
			$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerT + '" id="checkBoxTodos1" class="checkM" /><label for="checkBoxTodos1" >Seleccionar todo</label></div></th></tr>');

			_nivelT = data[0].tipoCeco;

			var actualId = data[0].cecoSuperior;
			var actualDesc = data[0].cecoSuperiorDesc;
			var makeDist = true;

			for (var x = 0; x < data.length; x++) {
				var _html = '';

				if (_nivelT > 1) {
					if (actualId != data[x].cecoSuperior) {
						actualId = data[x].cecoSuperior;
						actualDesc = data[x].cecoSuperiorDesc;
						makeDist = true;
					}

					if (makeDist) {
						_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
						_html += '<label for="' + data[x].cecoSuperior + '" ><strong>' + data[x].cecoSuperiorDesc + '</strong></label></div></td></tr>';
						makeDist = false;
					}

					_html += '<tr><td style="text-align: left;"><div class="listBox">';
					_html += '<input type="checkbox" name="grupoList' + _containerT + '" id="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" class="checkM" />';
					_html += '<label for="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" >' + data[x].nombre + '</label></div></td></tr>';
					$('#modalAdminList #adminList #listTable').append(_html);
				} else {
					_html += '<tr><td style="text-align: left;"><div class="listBox">';
					_html += '<input type="checkbox" name="grupoList' + _containerT + '" id="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" class="checkM" />';
					_html += '<label for="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" >' + data[x].nombre + '</label></div></td></tr>';
					$('#modalAdminList #adminList #listTable').append(_html);
				}
			}

			// Para la opción 'Seleccionar todos'
			$("#modalAdminList #checkBoxTodos1").change(function() {
				$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
			});

			// Se agrega <br> para organizar mejor elementos
			$('#modalAdminList #theList').append('<br>');

			// Se muestra la ventana modal
			$('#modalAdminList').modal();
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No hay datos que mostrar' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function getPaisListEstTableta() {
	showLoadingSpinner();

	if (_nivelP === 0) {
		_containerP = 'Pa\u00EDs';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 1) {
		_containerP = 'Estados';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 2) {
		_containerP = 'Municipios';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 3) {
		_containerP = 'Sucursales';
		$('#btnListSigNivel').hide();
	} else {
		_containerP = 'Pa\u00EDs';
		$('#btnListSigNivel').show();
	}

	if (_nivelP === 3) {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneCecosPorGeo.json',
			dataType: 'json',
			data: {
				geo: _cecos,
				tipo: _nivelP,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#modalAdminList .titModal').html('Lista de ' + _containerP);
				$('#modalAdminList #adminList').html('');
				$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos2" class="checkM" /><label for="checkBoxTodos2" >Seleccionar todo</label></div></th></tr>');

				_nivelP = data[0].tipo;

				var actualId = data[0].idDependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (actualId != data[x].idGeo) {
						actualId = data[x].idGeo;
						actualDesc = data[x].descGeo;
						makeDist = true;
					}

					if (makeDist) {
						_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
						_html += '<label for="' + data[x].idGeo + '" ><strong>' + data[x].descGeo + '</strong></label></div></td></tr>';
						makeDist = false;
					}

					_html += '<tr><td style="text-align: left;"><div class="listBox">';
					_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + data[x].idCeco + '-.-.-' + _nivelP + '" class="checkM" />';
					_html += '<label for="' + data[x].idCeco + '" >' + '(' + data[x].idCeco + ') ' + data[x].descCeco + '</label></div></td></tr>';
					$('#modalAdminList #adminList #listTable').append(_html);
				}

				// Para la opción 'Seleccionar todos'
				$("#modalAdminList #checkBoxTodos2").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalAdminList #adminList').append('<br>');

				// Se crea el modal
				$('#modalAdminList').modal();
			},
			error: function(obj, errorText) {
				console.log('No se pudo obtener la informacion...');
			},
			complete: function() {
				hideLoadingSpinner();
			} 
		});
	} else {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/consultaPais.json',
			dataType: 'json',
			data: {
				tipo: 1,
				cecos: _cecos,
				nivel: _nivelP,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#modalAdminList .titModal').html('Lista de ' + _containerP);
				$('#modalAdminList #adminList').html('');
				$('#modalAdminList #adminList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalAdminList #adminList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos2" class="checkM" /><label for="checkBoxTodos2" >Seleccionar todo</label></div></th></tr>');

				_nivelP = data[0].tipo;

				var actualId = data[0].idDependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelP > 1) {
						if (actualId != data[x].idDependeDe) {
							actualId = data[x].idDependeDe;
							actualDesc = data[x].dependeDeDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].idDependeDe + '" ><strong>' + data[x].dependeDeDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + _nivelP + '-.-.-' + data[x].id_geolocalizacion + '" class="checkM" />';
						_html += '<label for="' + _nivelP + '-.-.-' + data[x].id_geolocalizacion + '" >' + data[x].descripcion + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + _nivelP + '-.-.-' + data[x].id_geolocalizacion + '" class="checkM" />';
						_html += '<label for="' + _nivelP + '-.-.-' + data[x].id_geolocalizacion + '" >' + data[x].descripcion + '</label></div></td></tr>';
						$('#modalAdminList #adminList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalAdminList #checkBoxTodos2").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				// Se agrega <br> para organizar mejor elementos
				$('#modalAdminList #adminList').append('<br>');

				// Se crea el modal
				$('#modalAdminList').modal();
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			} 
		});
	}
}

function createEstTabletDestinyBox(boxId) {

	// Obtiene valores y actualia variables
	var _list = $('#modalAdminList #adminList').find('div.listBox');
	var _cecos = '';
	var _listName = '';

	for (var x = 1; x < _list.length; x++) {
		var _input = $(_list[x]).find('input');
		var _label = $(_list[x]).find('label');

		if ($(_input).is(':checked')) {
			var _id = _input[0].id;
			_cecos += _id.split('-.-.-')[0]+ ',';
			_listName += $(_label).html() + ',';
		}
	}

	if (_cecos.length < 1) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se ha seleccionado ning\u00FAn valor' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else {
		// Quita ultima coma de la cadena
		_cecos = _cecos.substring(0, _cecos.length - 1);
		_listName = _listName.substring(0, _listName.length - 1);
	}

	var _arrayCecos = _cecos.split(',');
	var _arrayNames = _listName.split(',');

	for (var x = 0; x < _arrayCecos.length; x++) {
		var _html = '';

		if (boxId === 1) {
			_html += '<div id="T-.-.-' + boxId + '-.-.-' + _arrayCecos[x] + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1%;">';
		} else if (boxId === 2) {
			_html += '<div id="P-.-.-' + _arrayCecos[x] + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1%;">';
		} else if (boxId === 3) {
			_html += '<div id="C-.-.-' + _arrayCecos[x] + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1%;">';
		}

		_html += '<a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">' + _arrayNames[x] + '</a>';
		$('#listaLanzamientos').append(_html);
	}

	if (boxId === 1) {
		$('#btnPais').off('click');
		$('#btnPais').removeClass('btnDistrib');
		$('#btnPais').addClass('btnDistribDis');

		$('#buscarPorSucursal').prop('disabled', true);
	} else if (boxId === 2) {
		$('#btnTerritorio').off('click');
		$('#btnTerritorio').removeClass('btnDistrib');
		$('#btnTerritorio').addClass('btnDistribDis');

		$('#buscarPorSucursal').prop('disabled', true);
	}

	$('.modalCloseImg').click();
}

function nextLevelEstTabletList() {
	showLoadingSpinner();

	// Obtiene valores y actualia variables
	var _list = $('#modalAdminList #adminList').find('div.listBox');
	var _title = $('#modalAdminList #adminTitle').html();

	_cecos = '';
	_listName = '';
	_nivelC = _nivelT;
	_objBackup = '';
	_objNameBackup = '';

	for (var x = 1; x < _list.length; x++) {
		var _input = $(_list[x]).find('input');
		var _label = $(_list[x]).find('label');

		if (_title.includes('Territorios') || _title.includes('Zonas') || _title.includes('Regiones') || _title.includes('Sucursales')) {
			if ($(_input).is(':checked') && _nivelC === 1) {
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');

				if (_nivelC === parseInt(_foo[1])) {
					_cecos += _foo[0] + ',';
					_listName += $(_label).html() + ',';
				} else {
					_objBackup += _foo[0] + ',';
					_objNameBackup += $(_label).html() + ',';
				}
			} else if ($(_input).is(':checked') && _nivelC > 1) {
				_cecos += _input[0].id + ',';
				_listName += $(_label).html() + ',';
			}
		} else if (_title.includes('Pa\u00EDs') || _title.includes('Estados') || _title.includes('Municipios')) {
			if ($(_input).is(':checked')) {
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			}
		}

		/**
		if ($(_input).is(':checked')) {
			var _foo = _input[0].id;
			_foo = _foo.split('-.-.-');

			if (_nivelC === parseInt(_foo[1])) {
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			} else {
				_objBackup += _foo[0] + ',';
				_objNameBackup += $(_label).html() + ',';
			}
		} else if ($(_input).is(':checked')) {
			_cecos += _input[0].id + ',';
			_listName += $(_label).html() + ',';
		}
		*/

	}

	if (_cecos.length < 1 && _objBackup.length < 1) {
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se ha seleccionado ning\u00FAn valor' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else {
		// Quita ultima coma de la cadena
		_cecos = _cecos.substring(0, _cecos.length - 1);
		_listName = _listName.substring(0, _listName.length - 1);

		_objBackup = _objBackup.substring(0, _objBackup.length - 1);
		_objNameBackup = _objNameBackup.substring(0, _objNameBackup.length - 1);

		var parent = $('#modalAdminList #adminList').parent();
		var title = $(parent).find('.titModal').html();

		if (title.includes('Categor\u00EDas')) {
			_nivelC++;
			getAdminCategoriaList();
		} else {
			if (title.includes('Territorios') || title.includes('Zonas') || title.includes('Regiones') || title.includes('Sucursales')) {
				getTerritorioAdminDataList();
			} else if (title.includes('Pa\u00EDs') || title.includes('Estados') || title.includes('Municipios')) {
				getGeoAdminDataList();
			}
		}
	}
}

function estTabletExportarHistorial() {
	// showLoadingSpinner();

	var _data = foliosJsonData != undefined ? foliosJsonData : '';
	if (foliosJsonData != undefined && foliosJsonData != null) {

		/********************************/
		/**
		$.post(contextPath + '/central/pedestalDigital/exportFilterDataToExcel.json?json=' + encodeURI(JSON.stringify(_data)), function(data) {
			var blob = new Blob([data], {
				type: 'text/html'
			});
			saveAs(blob, 'estatus_tabletas.xls');
			// hideLoadingSpinner();
		});
		*/
		/*********************************/

		$('#countdown').html(' ... ');
		$('#tamanoArchivo').html(' calculando tama\u00F1o del archivo ... ');
		$('.toastDescarga').addClass('active');

		// Se asigna evento al boton de cerrar
		$('.toastDescarga').find('.cerrar').on('click', function () {
			// clearInterval(countdownTimer);
			$(this).closest('.toastDescarga').removeClass('active');
		});

		var _url = contextPath + '/central/pedestalDigital/exportFilterDataToExcel.json';
		$.ajax({
			progress: function(event) {
				var _lengthComputable = event.lengthComputable;
				var _total = event.total;
				var _loaded = event.loaded;

				console.log('_lengthComputable: ' + _lengthComputable);
				console.log('_total: ' + _total);
				console.log('_loaded: ' + _loaded);

				if (_lengthComputable) {
					$('#countdown').html('estatus_tabletas.xls'); // nombre del archivo
					if (_total < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _total + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total >= 1000 && _total < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_total / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_total / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					}
				} else if (_total === 0 && _loaded > 0) {
					$('#countdown').html('estatus_tabletas.xls'); // nombre del archivo
					if (_loaded < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _loaded + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded >= 1000 && _loaded < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_loaded / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_loaded / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					}
				}
			},
			cache: false,
			responseType: 'arraybuffer',
			type: 'post',
			url: _url,
			data: {
				json: encodeURI(JSON.stringify(_data))
			},
			success: function(data) {
				var blob = new Blob([data], {
					type: 'text/html'
				});
				saveAs(blob, 'estatus_tabletas.xls');
			},
			error: function(obj, errorText) {
				$('.toastDescarga').find('.cerrar').click(); // Se cierra toast

				hideLoadingSpinner();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
						'error' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});

	} else {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe realizar alguna consulta para poder exportar el historial' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		// hideLoadingSpinner();
	}
}

function validateFiltersET() {
	showLoadingSpinner();

	// var buscarPorFolio = $('#buscarPorFolio').val();
	var buscarPorSucursal = $('#buscarPorSucursal').val();
	var list = $('#listaLanzamientos').find('.divEliminar');

	if (buscarPorSucursal.trim().length > 0) {
		filterBySucursalET(buscarPorSucursal);
	} else {
		var a, b;
		$.each(list, function(key, val) {
			if ($(val).attr('id').includes('T-.-.-')) {
				a = true;
				return false;
			} else if ($(val).attr('id').includes('P-.-.-')) {
				b = true;
				return false;
			}
		});

		if (a) {
			// TODO: Filtro para obtener folios por territorio
			setTimeout(function() {
				filterByTerritorioET();
			}, 250);
		} else if (b) {
			// TODO: Filtro para obtener folios por país
			setTimeout(function() {
				filterByPaisET();
			}, 250);
		} else {
			// TODO: Filtro para obtener folios por territorio (Default)
			setTimeout(function() {
				filterByTerritorioET();
			}, 250);
		}
	}
}

function filterBySucursalET(idSucursal) {
	var _idSucursal = $('#buscarPorSucursal').val();

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getTabletBySucursal.json',
		dataType: 'json',
		data: {
			numSucursal: _idSucursal
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			} else {
				foliosJsonData = {
						tipoFiltro: 'S',
						numSucursal: _idSucursal
				};

				if (data[0].idFolio === 0) {
					$('#tblTabletas').html('');
					$('#tblTabletas').html(
						'<thead style="display: block;">' +
							'<tr style="display: inline-table; width: 100%;">' +
								'<th style="width: 13%;">Número económico</th>' +
								'<th style="width: 35%;">Sucursal</th>' +
								'<th style="width: 15%;">Fecha de instalación</th>' +
								'<th style="width: 15%;">Última actualización</th>' +
								'<th style="width: 15%;">Estatus de la tableta</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody id="tblBody" style="display: block; max-height: 240px; overflow-y: auto;">' +
						'</tbody>'
					);
				} else {
					$('#tblTabletas').html('');
					$('#tblTabletas').html(
						'<thead style="display: block;">' +
							'<tr style="display: inline-table; width: 100%;">' +
								'<th style="width: 13%;">Número económico</th>' +
								'<th style="width: 35%;">Sucursal</th>' +
								'<th style="width: 15%;">Fecha de instalación</th>' +
								'<th style="width: 15%;">Última actualización</th>' +
								'<th style="width: 15%;">Estatus de la tableta</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody id="tblBody" style="display: block; max-height: 240px; overflow-y: auto;">' +
						'</tbody>'
					);

					for (var x = 0; x < data.length; x++) {
						$('#tblTabletas #tblBody').append(
								'<tr id="' + data[x].sucursal + '" style="display: inline-table; width: 100%;">' +
								'<td style="width: 13%;"><a href="loadTabletInfo.htm?idTableta=' + data[x].idTableta + '" >' + data[x].sucursal + '</a></td>' +
								'<td style="width: 35%;">' + data[x].nombre + '</td>' +
								'<td style="width: 15%;">' + data[x].fechaInstalacion + '</td>' +
								'<td style="width: 15%;">' + data[x].ultimaActualizacion + ' Hrs</td>' +
								'<td style="width: 15%;">' + data[x].descIdEstatus + '</td>' +
							'</tr>'
							);
					}
				}

				// Si el campo se encuentra vacío, se habilitan todos los elementos
				$('#buscarPorSucursal').prop('disabled', false);

				// Se habilitan botones.
				$('#btnTerritorio').click(function(e) {
					_nivelT = 0;
					_cecos = '0';
					_boxId = 1;
					getTerriListEstTableta();
				});

				$('#btnPais').click(function(e) {
					_nivelP = 0;
					_cecos = '0';
					_boxId = 2;
					getPaisListEstTableta();
				});

				// Se cambian clases para mostrar que el elemento se encuentra habilitado
				$('#btnTerritorio').addClass('btnDistrib');
				$('#btnTerritorio').removeClass('btnDistribDis');
				$('#btnPais').addClass('btnDistrib');
				$('#btnPais').removeClass('btnDistribDis');
			}
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informaci\u00F3n: ' + errorText);
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function filterByTerritorioET() {
	var _ceco = '';
	var _catIdList = '';
	var _estatus = $('#estadoTableta').val();
	var _rendimiento = $('#rendimientoTableta').val();
	var _catList = $('#listaLanzamientos').find('.divEliminar');

	if (_catList.length > 0) {
		for (var x = 0; x < _catList.length; x++) {
			var _catObj = $(_catList[x]);
			var _catId = $(_catObj).attr('id');

			if (_catId.includes('T-.-.-')) {
				_catIdList = _catId.split('-.-.-');
				_ceco += _catIdList[2] + ',';
			}
		}

		_ceco = _ceco.substring(0, _ceco.length - 1);
	} else {
		_ceco = '';
	}

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getTabletByTerritorio.json',
		dataType: 'json',
		data: {
			ceco: _ceco,
			estatus: _estatus,
			rendimiento: _rendimiento
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			} else {
				foliosJsonData = {
						tipoFiltro: 'T',
						ceco: _ceco,
						estatus: _estatus,
						rendimiento: _rendimiento
				};

				if (data[0].idFolio === 0) {
					$('#tblTabletas').html('');
					$('#tblTabletas').html(
						'<thead style="display: block;">' +
							'<tr style="display: inline-table; width: 100%;">' +
								'<th style="width: 13%;">Número económico</th>' +
								'<th style="width: 35%;">Sucursal</th>' +
								'<th style="width: 15%;">Fecha de instalación</th>' +
								'<th style="width: 15%;">Última actualización</th>' +
								'<th style="width: 15%;">Estatus de la tableta</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody id="tblBody" style="display: block; max-height: 240px; overflow-y: auto;">' +
						'</tbody>'
					);
				} else {
					$('#tblTabletas').html('');
					$('#tblTabletas').html(
						'<thead style="display: block;">' +
							'<tr style="display: inline-table; width: 100%;">' +
								'<th style="width: 13%;">Número económico</th>' +
								'<th style="width: 35%;">Sucursal</th>' +
								'<th style="width: 15%;">Fecha de instalación</th>' +
								'<th style="width: 15%;">Última actualización</th>' +
								'<th style="width: 15%;">Estatus de la tableta</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody id="tblBody" style="display: block; max-height: 240px; overflow-y: auto;">' +
						'</tbody>'
					);

					for (var x = 0; x < data.length; x++) {
						$('#tblTabletas #tblBody').append(
								'<tr id="' + data[x].sucursal + '" style="display: inline-table; width: 100%;">' +
								'<td style="width: 13%;"><a href="loadTabletInfo.htm?idTableta=' + data[x].idTableta + '" >' + data[x].sucursal + '</a></td>' +
								'<td style="width: 35%;">' + data[x].nombre + '</td>' +
								'<td style="width: 15%;">' + data[x].fechaInstalacion + '</td>' +
								'<td style="width: 15%;">' + data[x].ultimaActualizacion + ' Hrs</td>' +
								'<td style="width: 15%;">' + data[x].descIdEstatus + '</td>' +
							'</tr>'
							);
					}
				}

				// Si el campo se encuentra vacío, se habilitan todos los elementos
				$('#buscarPorSucursal').prop('disabled', false);

				// Se habilitan botones.
				$('#btnTerritorio').click(function(e) {
					_nivelT = 0;
					_cecos = '0';
					_boxId = 1;
					getTerriListEstTableta();
				});

				$('#btnPais').click(function(e) {
					_nivelP = 0;
					_cecos = '0';
					_boxId = 2;
					getPaisListEstTableta();
				});

				// Se cambian clases para mostrar que el elemento se encuentra habilitado
				$('#btnTerritorio').addClass('btnDistrib');
				$('#btnTerritorio').removeClass('btnDistribDis');
				$('#btnPais').addClass('btnDistrib');
				$('#btnPais').removeClass('btnDistribDis');
			}
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informaci\u00F3n: ' + errorText);
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function filterByPaisET() {
	var _geo = '';
	var _catIdList = '';
	var _estatus = $('#estadoTableta').val();
	var _rendimiento = $('#rendimientoTableta').val();
	var _catList = $('#listaLanzamientos').find('.divEliminar');

	if (_catList.length > 0) {
		for (var x = 0; x < _catList.length; x++) {
			var _catObj = $(_catList[x]);
			var _catId = $(_catObj).attr('id');

			if (_catId.includes('P-.-.-')) {
				_catIdList = _catId.split('-.-.-');
				_geo += _catIdList[1] + ',';
			}
		}

		_geo = _geo.substring(0, _geo.length - 1);
	} else {
		_geo = '';
	}

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getTabletByPais.json',
		dataType: 'json',
		data: {
			geo: _geo,
			estatus: _estatus,
			rendimiento: _rendimiento
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			} else {
				foliosJsonData = {
						tipoFiltro: 'P',
						geo: _geo,
						estatus: _estatus,
						rendimiento: _rendimiento
				};

				if (data[0].idFolio === 0) {
					$('#tblTabletas').html('');
					$('#tblTabletas').html(
						'<thead style="display: block;">' +
							'<tr style="display: inline-table; width: 100%;">' +
								'<th style="width: 13%;">Número económico</th>' +
								'<th style="width: 35%;">Sucursal</th>' +
								'<th style="width: 15%;">Fecha de instalación</th>' +
								'<th style="width: 15%;">Última actualización</th>' +
								'<th style="width: 15%;">Estatus de la tableta</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody id="tblBody" style="display: block; max-height: 240px; overflow-y: auto;">' +
						'</tbody>'
					);
				} else {
					$('#tblTabletas').html('');
					$('#tblTabletas').html(
						'<thead style="display: block;">' +
							'<tr style="display: inline-table; width: 100%;">' +
								'<th style="width: 13%;">Número económico</th>' +
								'<th style="width: 35%;">Sucursal</th>' +
								'<th style="width: 15%;">Fecha de instalación</th>' +
								'<th style="width: 15%;">Última actualización</th>' +
								'<th style="width: 15%;">Estatus de la tableta</th>' +
							'</tr>' +
						'</thead>' +
						'<tbody id="tblBody" style="display: block; max-height: 240px; overflow-y: auto;">' +
						'</tbody>'
					);

					for (var x = 0; x < data.length; x++) {
						$('#tblTabletas #tblBody').append(
								'<tr id="' + data[x].sucursal + '" style="display: inline-table; width: 100%;">' +
								'<td style="width: 13%;"><a href="loadTabletInfo.htm?idTableta=' + data[x].idTableta + '" >' + data[x].sucursal + '</a></td>' +
								'<td style="width: 35%;">' + data[x].nombre + '</td>' +
								'<td style="width: 15%;">' + data[x].fechaInstalacion + '</td>' +
								'<td style="width: 15%;">' + data[x].ultimaActualizacion + ' Hrs</td>' +
								'<td style="width: 15%;">' + data[x].descIdEstatus + '</td>' +
							'</tr>'
							);
					}
				}

				// Si el campo se encuentra vacío, se habilitan todos los elementos
				$('#buscarPorSucursal').prop('disabled', false);

				// Se habilitan botones.
				$('#btnTerritorio').click(function(e) {
					_nivelT = 0;
					_cecos = '0';
					_boxId = 1;
					getTerriListEstTableta();
				});

				$('#btnPais').click(function(e) {
					_nivelP = 0;
					_cecos = '0';
					_boxId = 2;
					getPaisListEstTableta();
				});

				// Se cambian clases para mostrar que el elemento se encuentra habilitado
				$('#btnTerritorio').addClass('btnDistrib');
				$('#btnTerritorio').removeClass('btnDistribDis');
				$('#btnPais').addClass('btnDistrib');
				$('#btnPais').removeClass('btnDistribDis');
			}
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function onChangeEstatusTabletaET() {
	var et = $('#estadoTableta').val();
	var rt = $('#rendimientoTableta').val();
	var dket = new Dropkick('#estadoTableta');
	var dkrt = new Dropkick('#rendimientoTableta');

	if (et === '0') {
		dkrt.disabled = false;
		dkrt.disable(0, false);
		dkrt.disable(1, false);
		dkrt.disable(2, false);
		dkrt.disable(3, false);
	} else if (et === '1') {
		dkrt.disabled = false;
		dkrt.disable(0, false);
		dkrt.disable(1, false);
		dkrt.disable(2, false);
		dkrt.disable(3, true);

		if (rt === '2' || rt === '3') {
			dkrt.select(1);
		}
	} else if (et === '2') {
		dkrt.disabled = false;
		dkrt.disable(0, false);
		dkrt.disable(1, true);
		dkrt.disable(2, true);
		dkrt.disable(3, false);

		if (rt === '1') {
			dkrt.select(0);
		}
	} else if (et === '3') {
		dkrt.disabled = true;
		dkrt.select(0);
	}
}

function onChangeRendimientoTabletaET() {
	var et = $('#estadoTableta').val();
	var rt = $('#rendimientoTableta').val();
	var dket = new Dropkick('#estadoTableta');
	var dkrt = new Dropkick('#rendimientoTableta');

	if (rt === '0') {
		dket.disabled = false;
		dket.disable(0, false);
		dket.disable(1, false);
		dket.disable(2, false);
		dket.disable(3, false);
	} else if (rt === '1' || rt === '2') {
		dket.disabled = false;
		dket.disable(0, false);
		dket.disable(1, true);
		dket.disable(2, false);
		dket.disable(3, false);

		if (et === '3') {
			dket.select(1);
		}
	} else if (rt === '3') {
		dket.disabled = false;
		dket.disable(0, false);
		dket.disable(1, true);
		dket.disable(2, false);
		dket.disable(3, false);

		if (et === '1') {
			dket.select(0);
		}
	}
}

function loadTabletData(tf, tp) {
	$('#toSucursalInfo').on('click', function () {
		window.location = 'loadSucursalInfo.htm?idTableta=' + _idTableta;
	});

	if (tf.length > 0) {
		$('#tblInformacion').html('');
		$('#tblInformacion').html(
				'<div class="titNotificaciones">Informaci&oacute;n T&eacute;cnica</div>' +
				'<table class="tblGeneral tblInfoTecnica" id="tblInformacion">' +
				'<thead>' +
					'<tr>' +
						'<th>Situaci&oacute;n</th>' +
						'<th>Descripci&oacute;n</th>' +
					'</tr>' +
				'</thead>' +
				'<tbody id="tblBody"></tbody>' +
			'</table>'
		);

		for (var x = 0; x < tf.length; x++) {
			var estatus;
			if (tf[x].FIID_ESTATUS === 1) {
				estatus = 'Correcto';
			} else if (tf[x].FIID_ESTATUS === 2) {
				estatus = 'Con problema';
			} else if (tf[x].FIID_ESTATUS === 3) {
				estatus = 'En mantenimiento';
			}

			$('#datosSucursal').html('<span class="titVerde">Sucursal: <strong>' + tf[x].FISUCURSAL + ' ' + tf[x].FCNOMBRE + '</strong></span>');
			$('#fechaInstalacion').html('<strong>Fecha de instalación: </strong>: ' + tf[x].FDFECHA_INSTALA);
			$('#estatusTableta').html('<strong>Estatus de la tableta: </strong>' + estatus);

			if (tf[x].FCFABRICANTE === 'samsung') {
				$('#imgTableta').attr('src', '../../img/galaxy_tab_a.png');
			} else if (tf[x].FCFABRICANTE === 'AOC') {
				$('#imgTableta').attr('src', '../../img/aoc.png');
			} else {
				$('#imgTableta').attr('src', '../../img/galaxy_tab_a.png');
			}

			var espacio = parseFloat(tf[x].FIESPACIO_DISP/tf[x].FIESPACIO_TOT).toFixed(2) * 100;
			var ram = parseFloat(tf[x].FIRAM_DISP/tf[x].FIRAM_TOTAL).toFixed(2) * 100;
			var intensidad = (tf[x].INTE_SENAL).substr(0,1) + (tf[x].INTE_SENAL).substr(1).toLowerCase();

			$('#tblInformacion #tblBody').append(
					'<tr>' +
						'<td>Fabricante</td>' +
						'<td>' + tf[x].FCFABRICANTE + '</td>' +
					'</tr>' +
					'<tr>' +
						'<td>Modelo</td>' +
						'<td>' + tf[x].FCMODELO + '</td>' +
					'</tr>' +
					'<tr>' +
						'<td>Número de serie</td>' +
						'<td>' + tf[x].FCNUM_SERIE + '</td>' +
					'</tr>' +
					'<tr>' +
						'<td>Sistema operativo</td>' +
						'<td>' + tf[x].FCSISITEMA_O + '</td>' +
					'</tr>' +
					'<tr>' +
						'<td>Versión de la App</td>' +
						'<td>' + tf[x].FCVERSION_APP + '</td>' +
					'</tr>' +
					'<tr>' +
						'<td>Intensidad de señal</td>' +
						'<td>' + intensidad + '</td>' +
					'</tr>' +
					'<tr>' +
						'<td>Espacio disponible</td>' +
						'<td>' + espacio.toFixed(0) + '%</td>' +
					'</tr>' +
					'<tr>' +
						'<td>RAM disponible</td>' +
						'<td>' + ram.toFixed(0) + '%</td>' +
					'</tr>'
			);
		}
	}

	if (tp.length > 0 && tp.length == 5) {
		$('#ind1A').html((tp[0].DESC_JSON_E).substr(0, 1) + (tp[0].DESC_JSON_E).substr(1).toLowerCase());
		$('#ind1B').html(tp[0].ULTIMA_ACTUALIZA != undefined ? tp[0].ULTIMA_ACTUALIZA : '00:00 Hrs');

		if ((tp[0].DESC_JSON_E).substr(0, 1) + (tp[0].DESC_JSON_E).substr(1).toLowerCase() == 'Conectado') {
			$('#ind1C').attr('src', '../../img/iconos/alimentacionIco.svg');
		} else {
			$('#ind1C').attr('src', '../../img/iconos/malaAlimentacionIco.svg');
		}

		$('#ind2A').html((tp[1].DESC_JSON_E).substr(0, 1) + (tp[1].DESC_JSON_E).substr(1).toLowerCase());
		$('#ind2B').html(tp[1].ULTIMA_ACTUALIZA != undefined ? tp[1].ULTIMA_ACTUALIZA : '00:00 Hrs');

		if ((tp[1].DESC_JSON_E).substr(0, 1) + (tp[1].DESC_JSON_E).substr(1).toLowerCase() == 'Conectado') {
			$('#ind2C').attr('src', '../../img/iconos/conexionIco.svg');
		} else {
			$('#ind2C').attr('src', '../../img/iconos/malaConexionIco.svg');
		}

		$('#ind3A').html((tp[3].DESC_JSON_E).substr(0, 1) + (tp[3].DESC_JSON_E).substr(1).toLowerCase());
		$('#ind3B').html(tp[3].ULTIMA_ACTUALIZA != undefined ? tp[3].ULTIMA_ACTUALIZA : '00:00 Hrs');

		if (tp[3].FCDESC_WEB == 'Moderado' || tp[3].FCDESC_WEB == 'Bueno') {
			$('#ind3C').attr('src', '../../img/iconos/alimentacionIco.svg');
		} else {
			$('#ind3C').attr('src', '../../img/iconos/malaBateriaIco.svg');
		}

		$('#ind4A').html((tp[4].DESC_JSON_E).substr(0, 1) + (tp[4].DESC_JSON_E).substr(1).toLowerCase());
		$('#ind4B').html(tp[4].ULTIMA_ACTUALIZA != undefined ? tp[4].ULTIMA_ACTUALIZA : '00:00 Hrs');

		if (tp[4].FCDESC_WEB == 'Bueno' || tp[4].FCDESC_WEB == 'Moderado') {
			$('#ind4C').attr('src', '../../img/iconos/temperaturaIco.svg');
		} else {
			$('#ind4C').attr('src', '../../img/iconos/malaTemperaturaIco.svg');
		}
	} else {
		$('#ind1A').html('');
		$('#ind1B').html('--/--/-- --:-- Hrs');
		$('#ind1C').attr('src', '../../img/iconos/malaAlimentacionIco.svg');
		$('#ind2A').html('');
		$('#ind2B').html('--/--/-- --:-- Hrs');
		$('#ind2C').attr('src', '../../img/iconos/malaConexionIco.svg');
		$('#ind3A').html('');
		$('#ind3B').html('--/--/-- --:-- Hrs');
		$('#ind3C').attr('src', '../../img/iconos/malaBateriaIco.svg');
		$('#ind4A').html('');
		$('#ind4B').html('--/--/-- --:-- Hrs');
		$('#ind4C').attr('src', '../../img/iconos/malaTemperaturaIco.svg');
	}

	hideLoadingSpinner()
}

function loadSucursalData(si, st) {
	if (si.length > 0) {
		for (var x = 0; x < si.length; x++) {
			$('#toInfoSucursal').attr('href', 'infoSucursalPD.htm?idTableta=' + si[x].idTableta);
			$('#downloadSucursalTable').attr('href', 'downloadExcelST.json?idTableta=' + si[x].idTableta + '&cecoInfo=' + si[0].sucursal);
			$('#indiSucursal').html('<strong>' + si[x].sucursal + '</strong>');
			$('#indiCeco').html('<strong>' + si[x].idCeco + '</strong>');
			$('#indiDireccion').html('<strong>' + si[x].direccion + '</strong>');
			$('#indiFechaInstalacion').html('<strong>' + si[x].fechaInstalacion + '</strong>');
		}
	}

	if (st.length > 0) {
		for (var x = 0; x < st.length; x++) {
			$('#tblSucursal #tblBody').append(
				'<tr>' +
					'<td>' + st[x].folio + '</td>' +
					'<td>' + st[x].nombreDocumento + '</td>' +
					'<td>' + st[x].tipoEnvio + '</td>' +
					'<td>' + st[x].estatusDocumentoSucursal + '</td>' +
					'<td>' + st[x].fechaCreacionDocumento + ' Hrs</td>' +
					'<td>' + st[x].fechaRecepcion + ' Hrs</td>' +
					'<td>' + st[x].fechaVisualizacionDocumento + ' Hrs</td>' +
				'</tr>'
			);

		}
	}

	hideLoadingSpinner()
}

function saveExcelST(idTableta) {
	var _url = contextPath + '/central/pedestalDigital/downloadExcelST.json';
	$.ajax({
		cache: false,
		contentType: false,
		data: {
			idTableta: idTableta
		},
		processData: false,
		responseType: 'arraybuffer',
		type: 'POST',
		url: _url,
	}).done(function (data) {
		var blob = new Blob([data], {
			type: 'text/html'
		});
		saveAs(blob, 'lista_archivos_sucursal.xls');
	});
}

/***********************************************/
/***** functions para vista de reportería ******/
/***********************************************/

function getTerritorioReporteria(id) {
	showLoadingSpinner();

	if (_nivelT === 0) {
		_containerT = 'Territorios';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 1) {
		_containerT = 'Zonas';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 2) {
		_containerT = 'Regiones';
		$('#btnListSigNivel').show();
	} else if (_nivelT === 3) {
		_containerT = 'Sucursales';
		$('#btnListSigNivel').hide();
	}

	if (_cecos.length < 1 && _objBackup.length > 1 && _nivelT === 1) {
		// Titulo de la ventana modal
		$('#modalReport #reportTitle').html('');
		$('#modalReport #reportTitle').html('Lista de ' + _containerT);

		// Se limpia la lista
		$('#modalReport #reportList').html('');
		$('#modalReport #reportList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
		$('#modalReport #reportList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _nivelT + '" id="checkBoxTodos1" class="checkM" /><label for="checkBoxTodos1" >Seleccionar todo</label></div></th></tr>');

		// Aqui se agregan las zonas especiales
		if (_objBackup.length > 1 && _objNameBackup.length > 1) {
			var objList = _objBackup.split(',');
			var objNameList = _objNameBackup.split(',');

			for (var x = 0; x < objList.length; x++) {
				_html = '';
				_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
				_html += '<label for="' + objList[x] + '" ><strong>' + objNameList[x] + '</strong></label></div></td></tr>';
				_html += '<tr><td style="text-align: left;"><div class="listBox">';
				_html += '<input type="checkbox" name="grupoList' + _containerT + '" id="' + objList[x] + '" class="checkM" />';
				_html += '<label for="' + objList[x] + '" >' + objNameList[x] + '</label></div></td></tr>';
				$('#modalReport #reportList #listTable').append(_html);
			}
		}

		_nivelT++;

		// Para la opción 'Seleccionar todos'
		$("#modalReport #checkBoxTodos1").change(function() {
			$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
		});

		$('#modalReport #listTable').append('<br>');
		$('#modalReport').modal();

		hideLoadingSpinner();
	} else if (_cecos.length > 0) {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/consultaCecos.json',
			dataType: 'json',
			data: {
				tipo: 0,
				cecos: _cecos,
				nivel: _nivelT,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				// Titulo de la ventana modal
				$('#modalReport #reportTitle').html('');
				$('#modalReport #reportTitle').html('Lista de ' + _containerT);

				// Se limpia la lista
				$('#modalReport #reportList').html('');
				$('#modalReport #reportList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalReport #reportList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerT + '" id="checkBoxTodos1" class="checkM" /><label for="checkBoxTodos1" >Seleccionar todo</label></div></th></tr>');

				_nivelT = data[0].tipoCeco;

				var actualId = data[0].cecoSuperior;
				var actualDesc = data[0].cecoSuperiorDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelT > 1) {
						if (actualId != data[x].cecoSuperior) {
							actualId = data[x].cecoSuperior;
							actualDesc = data[x].cecoSuperiorDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].cecoSuperior + '" ><strong>' + data[x].cecoSuperiorDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerT + '" id="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" >' + data[x].nombre + '</label></div></td></tr>';
						$('#modalReport #reportList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerT + '" id="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" class="checkM" />';
						_html += '<label for="' + data[x].idCeco + '-.-.-' + data[x].tipoCeco + '" >' + data[x].nombre + '</label></div></td></tr>';
						$('#modalReport #reportList #listTable').append(_html);
					}
				}

				// Aqui se agregan las zonas especiales
				if (_objBackup.length > 1 && _objNameBackup.length > 1) {
					var objList = _objBackup.split(',');
					var objNameList = _objNameBackup.split(',');

					for (var x = 0; x < objList.length; x++) {
						_html = '';
						_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
						_html += '<label for="' + objList[x] + '" ><strong>' + objNameList[x] + '</strong></label></div></td></tr>';
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerT + '" id="' + objList[x] + '" class="checkM" />';
						_html += '<label for="' + objList[x] + '" >' + objNameList[x] + '</label></div></td></tr>';
						$('#modalReport #reportList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalReport #checkBoxTodos1").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				$('#modalReport #listTable').append('<br>');
				$('#modalReport').modal();
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	}
}

function getGeografiaReporteria(id) {
	showLoadingSpinner();

	if (_nivelP === 0) {
		_containerP = 'Pa\u00EDs';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 1) {
		_containerP = 'Estados';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 2) {
		_containerP = 'Municipios';
		$('#btnListSigNivel').show();
	} else if (_nivelP === 3) {
		_containerP = 'Sucursales';
		$('#btnListSigNivel').hide();
	} else {
		_containerP = 'Pa\u00EDs';
		$('#btnListSigNivel').show();
	}

	if (_nivelP === 3) {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneCecosPorGeo.json',
			dataType: 'json',
			data: {
				geo: _cecos,
				tipo: _nivelP,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#modalReport #reportTitle').html('Lista de ' + _containerP);
				$('#modalReport #reportList').html('');
				$('#modalReport #reportList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalReport #reportList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerP + '" id="checkBoxTodos2" class="checkM" /><label for="checkBoxTodos2" >Seleccionar todo</label></div></th></tr>');

				_nivelP = data[0].tipo;

				var actualId = data[0].idDependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (actualId != data[x].idGeo) {
						actualId = data[x].idGeo;
						actualDesc = data[x].descGeo;
						makeDist = true;
					}

					if (makeDist) {
						_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
						_html += '<label for="' + data[x].idGeo + '" ><strong>' + data[x].descGeo + '</strong></label></div></td></tr>';
						makeDist = false;
					}

					_html += '<tr><td style="text-align: left;"><div class="listBox">';
					_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + data[x].idCeco + '-.-.-' + _nivelP + '" class="checkM" />';
					_html += '<label for="' + data[x].idCeco + '" >' + '(' + data[x].idCeco + ') ' + data[x].descCeco + '</label></div></td></tr>';
					$('#modalReport #reportList #listTable').append(_html);
				}

				// Para la opción 'Seleccionar todos'
				$("#modalReport #checkBoxTodos2").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				$('#modalReport #reportList').append('<br>');
				$('#modalReport').modal();
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	} else {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/consultaPais.json',
			dataType: 'json',
			data: {
				tipo: 1,
				cecos: _cecos,
				nivel: _nivelP,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#modalReport #reportTitle').html('Lista de ' + _containerP);
				$('#modalReport #reportList').html('');
				$('#modalReport #reportList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalReport #reportList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos2" class="checkM" /><label for="checkBoxTodos2" >Seleccionar todo</label></div></th></tr>');

				_nivelP = data[0].tipo;

				var actualId = data[0].idDependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelP > 1) {
						if (actualId != data[x].idDependeDe) {
							actualId = data[x].idDependeDe;
							actualDesc = data[x].dependeDeDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].idDependeDe + '" ><strong>' + data[x].dependeDeDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + data[x].id_geolocalizacion + '-.-.-' + _nivelP + '" class="checkM" />';
						_html += '<label for="' + data[x].id_geolocalizacion + '-.-.-' + _nivelP + '" >' + data[x].descripcion + '</label></div></td></tr>';
						$('#modalReport #reportList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerP + '" id="' + data[x].id_geolocalizacion + '-.-.-' + _nivelP + '" class="checkM" />';
						_html += '<label for="' + data[x].id_geolocalizacion + '-.-.-' + _nivelP + '" >' + data[x].descripcion + '</label></div></td></tr>';
						$('#modalReport #reportList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalReport #checkBoxTodos2").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				$('#modalReport #reportList').append('<br>');
				$('#modalReport').modal();
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	}
}

function getCaategoriasReporteria() {
	showLoadingSpinner();
	_containerC = 'Categor\u00EDas';

	if (_nivelC === 1) {
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneCategorias.json',
			dataType: 'json',
			async: false,
			data: {
				nivel: _nivelC,
				dependeDe: 0
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				var categories = data;

				$('#modalReport #reportTitle').html('Lista de ' + _containerC);
				$('#modalReport #reportList').html('');
				$('#modalReport #reportList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalReport #reportList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos4" class="checkM" /><label for="checkBoxTodos4" >Seleccionar todo</label></div></th></tr>');

				_nivelC = data[0].nivel;

				var actualId = data[0].dependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelC > 1) {
						if (actualId != data[x].dependeDe) {
							actualId = data[x].dependeDe;
							actualDesc = data[x].dependeDeDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].dependeDe + '" ><strong>' + data[x].dependeDeDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" class="checkM" />';
						_html += '<label for="' + data[x].idTipoDoc + '-.-.-' + _nivelT + '" >' + data[x].nombreDoc + '</label></div></td></tr>';
						$('#modalReport #reportList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" class="checkM" />';
						_html += '<label for="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" >' + data[x].nombreDoc + '</label></div></td></tr>';
						$('#modalReport #reportList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalReport #checkBoxTodos4").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				$('#modalReport #reportList').append('<br>');
				$('#modalReport').modal();
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	} else if (_nivelC > 1) {
		var _list = $('#modalReport #reportList').find('div.listBox');
		var _cecos = '';

		for (var x = 1; x < _list.length; x++) {
			var _input = $(_list[x]).find('input');

			if ($(_input).is(':checked')) {
				_cecos += _input[0].id.split('-.-.-')[0] + ',';
			}
		}

		if (_cecos.length < 1) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se ha seleccionado ning\u00FAn valor' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		} else {
			// Quita ultima coma de la cadena
			_cecos = _cecos.substring(0, _cecos.length - 1);
		}

		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneDocumentos.json',
			dataType: 'json',
			async: false,
			data: {
				nivel: _nivelC,
				dependeDe: _cecos
			},
			success: function(data) {
				if (data.length < 1) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No hay datos que mostrar' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				var categories = data;

				$('#modalReport #reportTitle').html('Lista de ' + _containerC);
				$('#modalReport #reportList').html('');
				$('#modalReport #reportList').html('<table class="tblGeneral"><tbody id="listTable"></tbody></table>');
				$('#modalReport #reportList #listTable').append('<tr><th style="text-align: left;"><div class="listBox"><input type="checkbox" name="grupoList' + _containerC + '" id="checkBoxTodos4" class="checkM" /><label for="checkBoxTodos4" >Seleccionar todo</label></div></th></tr>');

				_nivelC = data[0].nivel;

				var actualId = data[0].dependeDe;
				var actualDesc = data[0].dependeDeDesc;
				var makeDist = true;

				for (var x = 0; x < data.length; x++) {
					var _html = '';

					if (_nivelC > 1) {
						if (actualId != data[x].dependeDe) {
							actualId = data[x].dependeDe;
							actualDesc = data[x].dependeDeDesc;
							makeDist = true;
						}

						if (makeDist) {
							_html += '<tr><td style="text-align: left;"><div class="listBox" style="padding-left: 25px;">';
							_html += '<label for="' + data[x].dependeDe + '" ><strong>' + data[x].dependeDeDesc + '</strong></label></div></td></tr>';
							makeDist = false;
						}

						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" class="checkM" />';
						_html += '<label for="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" >' + data[x].nombreDoc + '</label></div></td></tr>';
						$('#modalReport #reportList #listTable').append(_html);
					} else {
						_html += '<tr><td style="text-align: left;"><div class="listBox">';
						_html += '<input type="checkbox" name="grupoList' + _containerC + '" id="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" class="checkM" />';
						_html += '<label for="' + data[x].idTipoDoc + '-.-.-' + _nivelC + '" >' + data[x].nombreDoc + '</label></div></td></tr>';
						$('#modalReport #reportList #listTable').append(_html);
					}
				}

				// Para la opción 'Seleccionar todos'
				$("#modalReport #checkBoxTodos4").change(function() {
					$('input:checkbox.checkM').prop('checked', $(this).prop('checked'));
				});

				$('#btnListSigNivel').hide();
				$('#modalReport #reportList').append('<br>');
				$('#modalReport').modal();
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	}
}

function createFilterBoxReporteria() {
	// Obtiene valores y actualia variables
	var _list = $('#modalReport #reportList').find('div.listBox')
	var _title = $('#modalReport #reportTitle').html();

	var _cecos = '';
	var _listName = '';
	_objBackup = '';
	_objNameBackup = '';

	if (_tabId === 1) {
		// Consulta general

		if (_boxId === 1) {
			$('#btnPaisA').off('click');
			$('#btnPaisA').removeClass('btnDistrib');
			$('#btnPaisA').addClass('btnDistribDis');
			$('#psA').addClass('desac');
			$('#buscarPorSucursalA').val('');
		} else if (_boxId === 2) {
			$('#btnTerritorioA').off('click');
			$('#btnTerritorioA').removeClass('btnDistrib');
			$('#btnTerritorioA').addClass('btnDistribDis');
			$('#psA').addClass('desac');
			$('#buscarPorSucursalA').val('');
		} else if (_boxId === 3) {
			// categoría no inhabilita componentes
		}
	} else if (_tabId === 2) {
		// Consulta detallada

		if (_boxId === 1) {
			$('#btnPaisB').off('click');
			$('#btnPaisB').removeClass('btnDistrib');
			$('#btnPaisB').addClass('btnDistribDis');
			$('#psB').addClass('desac');
			$('#buscarPorSucursalB').val('');
		} else if (_boxId === 2) {
			$('#btnTerritorioB').off('click');
			$('#btnTerritorioB').removeClass('btnDistrib');
			$('#btnTerritorioB').addClass('btnDistribDis');
			$('#psB').addClass('desac');
			$('#buscarPorSucursalB').val('');
		} else if (_boxId === 3) {
			// categoría no inhabilita componentes
		}
	}

	for (var x = 1; x < _list.length; x++) {
		var _input = $(_list[x]).find('input');
		var _label = $(_list[x]).find('label');

		if ($(_input).is(':checked')) {
			var _foo = _input[0].id;
			_foo = _foo.split('-.-.-');
			_cecos += _foo[0] + ',';
			_listName += $(_label).html() + ',';
		}
	}

	if (_cecos.length < 1 && _objBackup.length < 1) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se ha seleccionado ning\u00FAn valor' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else {
		// Quita ultima coma de la cadena
		_cecos = _cecos.substring(0, _cecos.length - 1);
		_listName = _listName.substring(0, _listName.length - 1);
		_objBackup = _objBackup.substring(0, _objBackup.length - 1);
		_objNameBackup = _objNameBackup.substring(0, _objNameBackup.length - 1);
	}

	var _arrayCecos = _cecos.split(',');
	var _arrayNames = _listName.split(',');

	for (var x = 0; x < _arrayCecos.length; x++) {
		var _html = '';

		if (_boxId === 1) {
			_html += '<div id="T-.-.-' + _arrayCecos[x] + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1%;">';
		} else if (_boxId === 2) {
			_html += '<div id="P-.-.-' + _arrayCecos[x] + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1%;">';
		} else if (_boxId === 3) {
			_html += '<div id="C-.-.-' + _arrayCecos[x] + '" class="col3 divEliminar" style="height: 30px; margin: 10px 1%;">';
		}

		_html += '<a href="#" class="btnCerrar1" onclick="removeDomItem(this);" style="color: black; margin-left: 5px; margin-top: 5px;">' + _arrayNames[x] + '</a>';

		if (_tabId === 1) {
			$('#listaLanzamientosA').append(_html);
		} else if (_tabId === 2) {
			$('#listaLanzamientosB').append(_html);
		}
	}

	// TODO: Definir los elementos que se habilitan o inhabilitan
	$('.modalCloseImg').click();
}

function nextLevelReports() {
	showLoadingSpinner();

	var _title = $('#modalReport #reportTitle').html();
	if (_title.includes('Territorios') || _title.includes('Zonas') || _title.includes('Regiones') || _title.includes('Sucursales')) {
		var _list = $('#modalReport #reportList').find('div.listBox');
		_cecos = '';
		_listName = '';
		_objBackup = '';
		_objNameBackup = '';
		_nivelC = _nivelT;

		for (var x = 1; x < _list.length; x++) {
			var _input = $(_list[x]).find('input');
			var _label = $(_list[x]).find('label');

			if ($(_input).is(':checked') && _nivelC === 1) {
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');

				if (_nivelC === parseInt(_foo[1])) {
					_cecos += _foo[0] + ',';
					_listName += $(_label).html() + ',';
				} else {
					_objBackup += _foo[0] + ',';
					_objNameBackup += $(_label).html() + ',';
				}
			} else if ($(_input).is(':checked') && _nivelC > 1) {
				var _input = $(_list[x]).find('input');
				var _label = $(_list[x]).find('label');
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			}
		}
	} else if (_title.includes('Pa\u00EDs') || _title.includes('Estados') || _title.includes('Municipios')) {
		var _list = $('#modalReport #reportList').find('div.listBox');
		_cecos = '';
		_listName = '';

		for (var x = 1; x < _list.length; x++) {
			if ($(_list[x]).find('.checkM').length > 0 && $(_list[x]).find(':checked').length && _nivelP === 1) {
				var _input = $(_list[x]).find('input');
				var _label = $(_list[x]).find('label');
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			} else if ($(_list[x]).find('.checkM').length > 0 && $(_list[x]).find(':checked').length && _nivelP > 1) {
				var _input = $(_list[x]).find('input');
				var _label = $(_list[x]).find('label');
				var _foo = _input[0].id;
				_foo = _foo.split('-.-.-');
				_cecos += _foo[0] + ',';
				_listName += $(_label).html() + ',';
			}
		}
	} else {
		var _list = $('#modalReport #reportList').find('div.listBox');
		_cecos = '';
		_listName = '';

		for (var x = 1; x < _list.length; x++) {
			var _input = $(_list[x]).find('input');
			var _label = $(_list[x]).find('label');
			var _foo = _input[0].id;
			_foo = _foo.split('-.-.-');
			_cecos += _foo[1] + ',';
			_listName += $(_label).html() + ',';
		}
	}

	if (_cecos.length < 1 && _objBackup.length < 1) {
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se ha seleccionado ning\u00FAn valor' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	} else {
		// Quita ultima coma de la cadena
		_cecos = _cecos.substring(0, _cecos.length - 1);
		_listName = _listName.substring(0, _listName.length - 1);
		_objBackup = _objBackup.substring(0, _objBackup.length - 1);
		_objNameBackup = _objNameBackup.substring(0, _objNameBackup.length - 1);

		var parent = $('#modalReport #reportList').parent();
		var title = $(parent).find('.titModal').html();

		if (title.includes('Categor\u00EDas')) {
			_nivelC++;
			getCaategoriasReporteria(_tabId);
		} else {
			if (title.includes('Territorios') || title.includes('Zonas') || title.includes('Regiones') || title.includes('Sucursales')) {
				getTerritorioReporteria(_tabId);
			} else if (title.includes('Pa\u00EDs') || title.includes('Estados') || title.includes('Municipios')) {
				getGeografiaReporteria(_tabId);
			}
		}
	}
}

function exportarTablaFolios() {
	// showLoadingSpinner();

	var _data = exportData != undefined ? exportData : '';
	if (exportData != undefined && exportData != null) {

		/**
		$.post(contextPath + '/central/pedestalDigital/exportTableFolioExcel.json?json=' + encodeURI(JSON.stringify(_data)), function(data) {
			var blob = new Blob([data], {
				type: 'text/html'
			});
			saveAs(blob, 'admin_folio.xls');
			// hideLoadingSpinner();
		});
		*/

		$('#countdown').html(' ... ');
		$('#tamanoArchivo').html(' calculando tama\u00F1o del archivo ... ');
		$('.toastDescarga').addClass('active');

		// Se asigna evento al boton de cerrar
		$('.toastDescarga').find('.cerrar').on('click', function () {
			// clearInterval(countdownTimer);
			$(this).closest('.toastDescarga').removeClass('active');
		});

		var _url = contextPath + '/central/pedestalDigital/exportTableFolioExcel.json';
		$.ajax({
			progress: function(event) {
				var _lengthComputable = event.lengthComputable;
				var _total = event.total;
				var _loaded = event.loaded;

				console.log('_lengthComputable: ' + _lengthComputable);
				console.log('_total: ' + _total);
				console.log('_loaded: ' + _loaded);

				if (_lengthComputable) {
					$('#countdown').html('admin_folio.xls'); // nombre del archivo
					if (_total < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _total + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total >= 1000 && _total < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_total / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_total / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					}
				} else if (_total === 0 && _loaded > 0) {
					$('#countdown').html('admin_folio.xls'); // nombre del archivo
					if (_loaded < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _loaded + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded >= 1000 && _loaded < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_loaded / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_loaded / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					}
				}
			},
			cache: false,
			responseType: 'arraybuffer',
			type: 'post',
			url: _url,
			data: {
				json: encodeURI(JSON.stringify(_data))
			},
			success: function(data) {
				var blob = new Blob([data], {
					type: 'text/html'
				});
				saveAs(blob, 'admin_folio.xls');
			},
			error: function(obj, errorText) {
				$('.toastDescarga').find('.cerrar').click(); // Se cierra toast

				hideLoadingSpinner();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
						'error' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});

	} else {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe realizar alguna consulta para poder exportar el historial' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		// hideLoadingSpinner();
	}
}

function showPreviewFolioDetail(json) {
	var _json = json.data._json;
	var _url = _repoUrl + 'franquicia/pedestal_digital/cargas' + _json.ruta;

	// TODO: Validar comportamiento de la ventana modal.
	// Las ventanas modales que sean cerradas, ya no podrán mostrarse.

	$.modal.close();

	if (_json.formato === 'jpg' || _json.formato === 'png') {
		$('#vpTitle').html('');
		$('#vpTitle').html(_json.documento);
		$('#vpContainer').html('');
		$('#vpContainer').html('<img id="vpImg" src="" style="width: inherit;">');
		$('#vpImg').attr('src', _json.ruta);
	} else if (_json.formato === 'mp4') {
		$('#vpTitle').html('');
		$('#vpTitle').html(_json.documento);
		$('#vpContainer').html('');
		$('#vpContainer').html('<video width="85%" controls><source id="vpVideo" src="" type="video/mp4">Tu navegador no es compatible con la reproducci\u00F3n de v\u00EDdeos.</video>');
		$('#vpVideo').attr('src', _url);
	} else if (_json.formato === 'pdf') {
		$('#vpTitle').html('');
		$('#vpTitle').html(_json.documento);
		$('#vpContainer').html('');
		$('#vpContainer').html('<iframe id="vpPdf" src="" style="width: inherit; height: inherit;" />');
		$('#vpPdf').attr('src', _url);
	} else {
		$('#vpTitle').html('');
		$('#vpTitle').html(_json.documento);
		$('#vpContainer').html('');
		$('#vpContainer').html('<img id="vpImg" src="" style="width: inherit;">');
		$('#vpImg').attr('src', _json.ruta);
	}

	$('#vistaPrevia').modal({
		escClose: false
	});
}

function vpBtnCerrar() {
	$.modal.close();
	$('#modalDetalle').modal({
		escClose: false
	});
}

function exportFolioDetailExcel(event) {
	var _idFolio = event.data._idFolio;
	var _url = contextPath + '/central/pedestalDigital/filtroRepDetaIdFolioExcel.json';

	$('#countdown').html(' ... ');
	$('#tamanoArchivo').html(' calculando tama\u00F1o del archivo ... ');
	$('.toastDescarga').addClass('active');

	// Se asigna evento al boton de cerrar
	$('.toastDescarga').find('.cerrar').on('click', function () {
		// clearInterval(countdownTimer);
		$(this).closest('.toastDescarga').removeClass('active');
	});

	$.ajax({
		progress: function(event) {
			var _lengthComputable = event.lengthComputable;
			var _total = event.total;
			var _loaded = event.loaded;

			console.log('_lengthComputable: ' + _lengthComputable);
			console.log('_total: ' + _total);
			console.log('_loaded: ' + _loaded);

			if (_lengthComputable) {
				$('#countdown').html('detalle_folio_' + _idFolio + '.xls'); // nombre del archivo
				if (_total < 1000) {
					$('#tamanoArchivo').html(_loaded + '/' + _total + ' bytes');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				} else if (_total >= 1000 && _total < 1000000) {	
					$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_total / 1000).toFixed(2) + ' KB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				} else if (_total > 1000000) {
					$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_total / 1000 / 1000).toFixed(3) + ' MB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
				}
			} else if (_total === 0 && _loaded > 0) {
				$('#countdown').html('detalle_folio_' + _idFolio + '.xls'); // nombre del archivo
				if (_loaded < 1000) {
					$('#tamanoArchivo').html(_loaded + '/' + _loaded + ' bytes');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				} else if (_loaded >= 1000 && _loaded < 1000000) {	
					$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_loaded / 1000).toFixed(2) + ' KB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				} else if (_loaded > 1000000) {
					$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_loaded / 1000 / 1000).toFixed(3) + ' MB');
					$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
				}
			}
		},
		cache: false,
		responseType: 'arraybuffer',
		type: 'post',
		url: _url,
		data: {
			idFolio: _idFolio
		},
		success: function(data) {
			var blob = new Blob([data], {
				type: 'text/html'
			});
			saveAs(blob, 'detalle_folio_' + _idFolio + '.xls');
		},
		error: function(obj, errorText) {
			$('.toastDescarga').find('.cerrar').click(); // Se cierra toast

			hideLoadingSpinner();
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
					'error' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});

	/**
	$.post(contextPath + '/central/pedestalDigital/filtroRepDetaIdFolioExcel.json?idFolio=' + idFolio, function(data) {
		var blob = new Blob([data], {
			type: 'text/html'
		});
		saveAs(blob, 'detalle_folio_' + idFolio + '.xls');
	});
	*/
}

function showModalEditFolio(event) {
	// var idFolio = event.data._idFolio; // var idFolio = event.data._idFolio.match('[a-zA-Z0-9\\-_+]*')[0];
	var idFolio = $.encoder.encodeForHTML(event.data._idFolio);
	var jsonFA = event.data.jsonFA;
	var listaDocumentos = event.data.listaDocumentos;

	// Validar componentes de jsonFA para saber si se puede actualizar la fecha de visualizacion de los documentos del folio
	var _arr = jsonFA.documentos;
	var faPass = true;
	var faPass4 = true;

	for (var x = 0; x < _arr.length; x++) {
		var today = new Date();
		var a = (_arr[x].fecha).split(' ')[0];
		a = a.split('/');
		var b = (_arr[x].fecha).split(' ')[1];
		b = b.split(':');
		var fecha = new Date(a[2], a[1]-1, a[0], b[0], b[1], 00);

		// Valida si se puede modificar la fecha de visualizacion
		if (today >= fecha || _arr[x].visible === 'V' /** || (_arr[x].fechaFin != null || _arr[x].fechaFin != undefined) */ ) {
			faPass = false;
		}

		// Valida si se puede cancelar un folio
		if (_arr[x].visible === 'V') {
			faPass4 = false;
		}
	}

	var docList = '';
	for (var x = 0; x < listaDocumentos.length; x++) {
		docList += listaDocumentos[x] + ',';
	}

	docList = docList.substring(0, docList.length - 1);

	if (faPass) {
		$('#modalFolioDetail #faPass').val(0);
	}

	if (faPass4) {
		$('#modalFolioDetail #faPass4').val(0);
	}

	$('#modalFolioDetail #faIdDocs').val($('#modalDetalle #faIdDocs').val());
	$('#modalFolioDetail #fdFechaActual').val(a[0] + '/' + a[1] + '/' + a[2]);
	$('#modalFolioDetail #fdDocList').val(docList);

	$.modal.close();
	$('#fdIdFolio').html(idFolio);
	$('#modalFolioDetail #fd1').click();
	$('#modalFolioDetail').modal({
		escClose: false
	});
}

function selectOptionEditFolio() {
	var op = $('#fdOptions').find(':checked')[0].id;
	var faPass = $('#modalFolioDetail #faPass').val();
	var faPass4 = $('#modalFolioDetail #faPass4').val();

	if (op === 'fd1') {
		// Agregar sucursales

		$.modal.close();
		$('#modalAgregaSucursalesFolio #fdIdFolio').html($('#modalFolioDetail #fdIdFolio').html());
		$('#modalAgregaSucursalesFolio #fdIdFolioH').val($('#modalFolioDetail #fdIdFolio').html());
		$('#modalAgregaSucursalesFolio #faIdDocs').val($('#modalFolioDetail #faIdDocs').val());
		$('#modalAgregaSucursalesFolio #btnCancelarAgrSuc').show();
		$('#modalAgregaSucursalesFolio #btnAgregarSucursales').show();
		$('#modalAgregaSucursalesFolio #btnAceptarAgrSuc').hide();
		$('#modalAgregaSucursalesFolio').modal();
	} else if (op === 'fd2') {
		// Modificar fecha de visualizacion

		if (faPass === '1') { // TODO: Valor correcto es uno (1)
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se puede modificar la fecha de visualizaci\u00F3n, ya que los documentos del folio se encuentran visibles en las sucursales' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}

		$.modal.close();
		$('#modalCambiaFechaFolio #fdIdFolio').html($('#modalFolioDetail #fdIdFolio').html());
		$('#modalCambiaFechaFolio #fdFechaActual').val($('#modalFolioDetail #fdFechaActual').val());
		$('#modalCambiaFechaFolio').modal({
			zIndex: 300,
			escClose: false,
			onShow: function() {
				$("#dpNuevaFecha").datepicker({
					firstDay: 1
				});
			}
		});
	} else if (op === 'fd3') {
		// Sustituir documento

		var docList = $('#modalFolioDetail #fdDocList').val();
		docList = docList.split(',');

		if (docList.length > 0) {
			$('#modalSustituyeDocumentoFolio #fdDocOptions').html('');

			for (var x = 0; x < docList.length; x++) {
				var html = '<div style="margin-left: 15px; text-align: left; margin-bottom: 5px;">';
				html += '<input type="radio" name="rdDocList" id="doc' + x + '">';
				html += '<label for="doc' + x + '" id="' + encodeURI(JSON.stringify(_jsonData[x])) + '">' + docList[x] + '</label>';
				html += '</div>';
				$('#modalSustituyeDocumentoFolio #fdDocOptions').append(html);
			}
		}

		$.modal.close();
		$('#modalSustituyeDocumentoFolio #fdIdFolio').val($('#modalFolioDetail #fdIdFolio').html());
		$('#modalSustituyeDocumentoFolio #fdFechaActual').val($('#modalFolioDetail #fdFechaActual').val());
		$('#fdDocOptions #doc0').click();
		$('#modalSustituyeDocumentoFolio').modal({
			zIndex: 300,
			escClose: false,
		});
	} else if (op === 'fd4') {
		// Cancelar folio

		if (faPass4 === '1') {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se puede cancelar el folio por que ya tiene documentos visibles en sucursal' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		} else {
			var confirmAlert = Swal.mixin({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>¡Atención!</strong></div>' +
					'\u00BFEst\u00E1s seguro que deseas cancelar el folio? Ya no se podr\u00E1n visualizar los documentos del folio en sucursal' +
					'</div>',
				showCloseButton: false,
				showCancelButton: true,
				cancelButtonText: 'Regresar',
				confirmButtonText: 'Cancelar folio',
				confirmButtonColor: '#006341'
			});

			confirmAlert.fire().then((result) => {
				if (result.value) {
					var _idDocs = $('#modalFolioDetail #faIdDocs').val();
					var _idFolio = $('#modalFolioDetail #fdIdFolio').html();

					$.ajax({
						type: 'get',
						cache: false,
						url: contextPath + '/central/pedestalDigital/inactivaLanzamientosDocFolio.json',
						dataType: 'json',
						async: false,
						data: {
							idFolio: _idFolio,
							idDocs: _idDocs
						},
						success: function(data) {
							if (data.msj === 1) {
								Swal.fire({
									html: '<div style="width: 100%;">' +
										'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
										'Algo sucedi\u00F3 y no se pudo cancelar el folio' +
										'</div>',
									showCloseButton: false,
									confirmButtonText: 'Aceptar',
									confirmButtonColor: '#006341'
								});
								return false;
							} else {
								// TODO: Obtener e iterar lista de folios, quitar de la tabla el folio recien cancelado
								var trList = $('#tblBody').find('tr');
								var _idFolio = $('#modalFolioDetail #fdIdFolio').html();

								if (trList.length > 0) {
									for (var x = 0; x < trList.length; x++) {
										var _id = trList[x].id;

										if (_id === _idFolio) {
											$(trList[x]).remove();
											$.modal.close();
										}
									}
								}
							}
						},
						error: function(obj, errorText) {
							Swal.fire({
								html: '<div style="width: 100%;">' +
									'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
									'Algo sucedi\u00F3 y no se pudo cancelar el folio' +
									'</div>',
								showCloseButton: false,
								confirmButtonText: 'Aceptar',
								confirmButtonColor: '#006341'
							});
							return false;
						},
						complete: function() {
							hideLoadingSpinner();
						}
					});
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					// Cancel
				}
			});
		}
	}
}

function vpBtnCerrarNuevaFecha() {
	var confirmAlert = Swal.mixin({
		html: '<div style="width: 100%;">' +
			'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>¡Atención!</strong></div>' +
			'\u00BFEst\u00E1s seguro que deseas salir? Se mantendr\u00E1 la fecha de visualizaci\u00F3n actual.' +
			'</div>',
		showCloseButton: false,
		showCancelButton: true,
		cancelButtonText: 'Salir',
		confirmButtonText: 'Quedarse',
		confirmButtonColor: '#006341'
	});

	confirmAlert.fire().then((result) => {
		if (result.value) {
			// ????????
		} else if (result.dismiss === Swal.DismissReason.cancel) {
			$.modal.close();
			$('#modalFolioDetail').modal();
		}
	});
}

function updateFechaVisualizacionFolio() {
	showLoadingSpinner();

	var _idFolio = $('#modalCambiaFechaFolio #fdIdFolio').html();
	var _fechaVisualizacion = $('#dpNuevaFecha').val();
	var _fechaActual = $('#modalCambiaFechaFolio #fdFechaActual').val();

	_idFolio = parseInt(_idFolio);

	if (_fechaVisualizacion.length < 8) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe seleccionar una fecha para poder modificar la fecha de visualizaci\u00F3n' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	_fechaVisualizacion = _fechaVisualizacion.split('/');
	_fechaActual = _fechaActual.split('/');

	var _date1 = new Date(_fechaVisualizacion[2], _fechaVisualizacion[1]-1, _fechaVisualizacion[0]);
	var _date2 = new Date(_fechaActual[2], _fechaActual[1]-1, _fechaActual[0]);
	var _fecha = _fechaVisualizacion[0] + '/' + _fechaVisualizacion[1] + '/' + _fechaVisualizacion[2];

	if (_date1 <= _date2) {
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Fecha no v\u00E1lida, debe seleccionar una fecha posterior a la fecha de visualizaci\u00F3n actual para poder continuar' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/updateFechaVisualizacionFolio.json',
		dataType: 'json',
		async: false,
		data: {
			idFolio: _idFolio,
			fecha: _fecha
		},
		success: function(data) {
			if (data.length < 1 || data.idFolio === '0') {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo actualizar la fecha de visualizaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			} else {
				var confirmAlert = Swal.mixin({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Se actualiz\u00F3 de manera correcta la fecha de visualizaci\u00F3n del folio' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});

				confirmAlert.fire().then((result) => {
					if (result.value) {
						$.modal.close();
						location.reload();
						// showFolioDetail(_idFolio);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						return false;
					}
				});
			}
		},
		error: function(obj, errorText) {
			console.log(errorText);
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo actualizar la fecha de visualizaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function vpBtnCerrarSustituyeDoc() {
	var confirmAlert = Swal.mixin({
		html: '<div style="width: 100%;">' +
			'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>¡Atención!</strong></div>' +
			'\u00BFEst\u00E1s seguro que deseas salir? No se reemplazar\u00E1 ning\u00FAn documento' +
			'</div>',
		showCloseButton: false,
		showCancelButton: true,
		cancelButtonText: 'Salir',
		confirmButtonText: 'Quedarse',
		confirmButtonColor: '#006341'
	});

	confirmAlert.fire().then((result) => {
		if (result.value) {
			// ????????
		} else if (result.dismiss === Swal.DismissReason.cancel) {
			$.modal.close();
			$('#modalFolioDetail').modal();
		}
	});
}

function changeDocOfFolio() {
	var _list = $('#modalSustituyeDocumentoFolio #fdDocOptions').find(':checked');
	var _id = $('#modalSustituyeDocumentoFolio #fdDocOptions').find(':checked').attr('id');
	_id = _id.substring(3);

	// Valida que se haya seleccionado un documento 
	if (_list.length < 1 && _id.length < 1) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe seleccionar un documento de la lista para poder continuar' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	// Valida que el formulario tenga un archivo seleccionado
	if ($('#cargaDocFolio').get(0).files.length < 1) {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe adjuntar un archivo para poder continuar' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	var _idFolio = $('#formChangeDocFolio #fdIdFolio').val();
	var formData = new FormData($('#formChangeDocFolio')[0]);
	_jsonData = $($('#fdDocOptions').find('label')[_id]).attr('id');

	$.ajax({
        cache: false,
        contentType: false,
		data: {
			// formData: formData,
			idFolio: _idFolio,
			jsonData: _jsonData
		},
        processData: false,
		type: 'post',
		url: contextPath + '/central/pedestalDigital/sustituyeDocumentoFolio.htm',
		success: function(data) {
			if (data.length < 1 || data.idFolio === '0') {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo actualizar la fecha de visualizaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			} else {
				var confirmAlert = Swal.mixin({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Se reemplaz\u00F3 de manera correcta el documento del folio' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});

				confirmAlert.fire().then((result) => {
					if (result.value) {
						$.modal.close();
						showFolioDetail(_idFolio);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						return false;
					}
				});
			}
		},
		error: function(obj, errorText) {
			console.log(errorText);
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});

	// trigger submit button in form
	/**
	$("#formChangeDocFolio").submit(function(event) {
		// console.log('done');
		var postData = $(this).serializeArray();
		var formURL = $(this).attr("action");

		event.preventDefault();
	});
	*/
}

function vpBtnCerrarAgregaSucursales() {
	var confirmAlert = Swal.mixin({
		html: '<div style="width: 100%;">' +
			'<div class="titModalSwal" style="width: 100%; text-align: center;"><br><strong>¡Atención!</strong></div>' +
			'\u00BFEst\u00E1s seguro que deseas salir? No se van a agregar sucursales a este folio' +
			'</div>',
		showCloseButton: false,
		showCancelButton: true,
		cancelButtonText: 'Salir',
		confirmButtonText: 'Quedarse',
		confirmButtonColor: '#006341'
	});

	confirmAlert.fire().then((result) => {
		if (result.value) {
			// ????????
		} else if (result.dismiss === Swal.DismissReason.cancel) {
			$.modal.close();
			// $('#modalAgregaSucursalesFolio #agregaSucursalesContainer').html('');
			// $('#modalAgregaSucursalesFolio #agregaSucursalesContainer').html(_backupHTML);
			$('#modalAgregaSucursalesFolio #btnAgregarSucursales').addClass('desac');
			$('#modalAgregaSucursalesFolio #btnAgregarSucursales').show();
			$('#modalAgregaSucursalesFolio #btnCancelarAgrSuc').show();
			$('#modalAgregaSucursalesFolio #btnAceptarAgrSuc').hide();
			$('#modalFolioDetail').modal();
		}
	});
}

function processCargaSucursalesFolio(data, fileName) {
	//Read the Excel File data.
	var workbook = XLSX.read(data, {
		type: 'binary'
	});

	var json = '';
	var extra = '';
	var _fileName = fileName.split('\\')[2].split('.')[0];

	try {
		var sheet = workbook.SheetNames[0]; // Fetch the name a Sheet
		var excelRows = XLSX.utils.make_csv(workbook.Sheets[sheet]); // Read all rows from First Sheet into an JSON array.

		while(excelRows.includes('\n')) {
		    excelRows = excelRows.replace('\n', '');
		}

		var aux = excelRows.split(',');
		aux = aux.filter(ceco => ceco.length > 0);
		var _totalInList = aux.length;

		if (aux.length > 350) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
					'El archivo es muy grande, no puede contener mas de 350 sucursales.' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});

			hideLoadingSpinner();
			return false;
		}

		// Obtiene negocio
		var _negocio = 41; // $('#negocio').val(); // test only
		var _idFolio = $('#modalAgregaSucursalesFolio #fdIdFolioH').val();
		if (_negocio === undefined || _negocio === null) {
			alert('_negocio is null or undefined');
			hideLoadingSpinner();
			return false;
		}

		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/getListaCecosFD.json',
			dataType: 'json',
			data: {
				csvList: excelRows,
				negocio: _negocio, // TODO: Obtener negocio
				idFolio: _idFolio
			},
			success: function(data) {
				var _totalListCecos = data.length;
				var _idDocs = $('#modalAgregaSucursalesFolio #faIdDocs').val();
				_backupHTML = $('#modalAgregaSucursalesFolio #agregaSucursalesContainer').html();
				$('#modalAgregaSucursalesFolio #agregaSucursalesContainer').html('');

				if (data.length === 0) {
					$('#modalAgregaSucursalesFolio #agregaSucursalesContainer').html('<input id="faIdDocs" name="faIdDocs" type="hidden" value="' + _idDocs + '">');
					$('#modalAgregaSucursalesFolio #agregaSucursalesContainer').append('<p style="text-align: center; margin-bottom: 20px;"><strong>Las sucursales del CSV no se agregaron pues ya existen en el folio</strong></p>');

					// TODO: Agregar funcionalidad al botón 'Agregar sucursales'
					$('#modalAgregaSucursalesFolio #btnAgregarSucursales').addClass('desac');
					$('#modalAgregaSucursalesFolio #btnAgregarSucursales').hide();
					$('#modalAgregaSucursalesFolio #btnCancelarAgrSuc').hide();
					$('#modalAgregaSucursalesFolio #btnAceptarAgrSuc').show();
				} else {
					$('#modalAgregaSucursalesFolio #agregaSucursalesContainer').html('<input id="faIdDocs" name="faIdDocs" type="hidden" value="' + _idDocs + '">');
					$('#modalAgregaSucursalesFolio #agregaSucursalesContainer').append('<p style="text-align: left; margin-bottom: 5px;"><strong>Selecciona las sucursales</strong></p>');
					$('#modalAgregaSucursalesFolio #agregaSucursalesContainer').append('<table class="tblGeneral" style="display: block; width: 100%;"><tbody id="listTable" style="display: block; width: 100%; overflow-y: scroll; max-height: 268px;"></tbody></table>');
					$('#modalAgregaSucursalesFolio #agregaSucursalesContainer #listTable').append('<tr style="display: inline-table; width: 100%;"><th style="text-align: left;" class="valid" ><div class="listBox"><input type="checkbox" name="grupoDistribList" id="checkBoxTodos2" class="checkM" /><label for="checkBoxTodos2" >Seleccionar todo</label></div></th></tr>');

					var _noHtml = '';
					for (var x = 0; x < data.length; x++) {
						var _html = '';

						if (data[x].activo === 1) {
							_html += '<tr style="display: inline-table; width: 100%;"><td style="text-align: left;" class="valid" ><div class="listBox">';
							_html += '<input type="checkbox" name="grupoDistribList" id="' + data[x].idCeco + '" class="checkM" />';
							_html += '<label for="' + data[x].idCeco + '" >' + '(' + data[x].idCeco + ') ' + data[x].nombre + '</label></div></td></tr>';
						} else if (data[x].activo === 0) {
							_noHtml += '<tr style="display: inline-table; width: 100%;"><td style="text-align: left; background-color: #FF9A9A; pointer-events: none;" class="noValid" ><div class="listBox">';
							_noHtml += '<input type="checkbox" name="grupoDistribList" id="' + data[x].idCeco + '" class="checkM" />';
							_noHtml += '<label for="' + data[x].idCeco + '" >' + '(' + data[x].idCeco + ') ' + data[x].nombre + '</label></div></td></tr>';
							_totalListCecos--;
						}

						$('#modalAgregaSucursalesFolio #agregaSucursalesContainer #listTable').append(_html);
					}

					if (_noHtml.length > 0) {
						// Si existen cecos inactivos, se agregan al final
						$('#modalAgregaSucursalesFolio #agregaSucursalesContainer #listTable').append(_noHtml);
					}

					// Agrega valores
					$('#totalContainer #totalCargado').html('<strong>Sucursales en el archivo: ' + _totalInList + '</strong>');
					$('#totalContainer #totalMostrado').html('<strong>Sucursales válidas: ' + _totalListCecos + '</strong>');

					// Funcionalidad de 'seleccionar todos'
					$("#modalAgregaSucursalesFolio #checkBoxTodos2").change(function() {
						$('.valid input:checkbox.checkM').prop('checked', $(this).prop('checked'));
					});

					// Selecciona todos los cecos activos.
					$('#modalAgregaSucursalesFolio #checkBoxTodos2').click();

					// TODO: Agregar funcionalidad al botón 'Agregar sucursales'
					$('#modalAgregaSucursalesFolio #btnAgregarSucursales').removeClass('desac');
					$('#modalAgregaSucursalesFolio #btnAgregarSucursales').show();
					$('#modalAgregaSucursalesFolio #btnCancelarAgrSuc').show();
					$('#modalAgregaSucursalesFolio #btnAceptarAgrSuc').hide();
				}
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No fue posible agregar las sucursales al folio' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	} catch (e) {
		// Se oculta la animación de carga
		hideLoadingSpinner();

		// En caso de que ocurra un error al leer el archivo de Excel se imprime el error
		console.log(e);

		// Se muestra alerta al usuario
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se pudo leer el archivo CSV.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
	}
}

function agregaSucursalesFolio() {
	showLoadingSpinner();

	var _list = $('#agregaSucursalesContainer .listBox').find(':checked'); // $('#agregaSucursalesContainer .checkM');

	if (_list.length < 2) {
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se pudo realizar la acci\u00F3n' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	var _cecoList = '';

	for (var x = 1; x < _list.length; x++) {
		_cecoList += $(_list[x]).attr('id') + ',';
	}

	_cecoList = _cecoList.substring(0, _cecoList.length - 1);
	var _idFolio = $('#modalAgregaSucursalesFolio #fdIdFolio').html();
	var _idDocs = $('#modalAgregaSucursalesFolio #faIdDocs').val();

	// TODO: Agregar nuevo servicio que se encarga de realizar las inserciones.

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/insertLanzCecoFD.json',
		dataType: 'json',
		data: {
			cecos: _cecoList,
			negocio: 41, // test only
			idDocs: _idDocs
		},
		success: function(data) {
			if (data.respuesta === '1') {
				hideLoadingSpinner();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo realizar la acci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			$('#modalAgregaSucursalesFolio #agregaSucursalesContainer').html('');
			$('#modalAgregaSucursalesFolio #agregaSucursalesContainer').html('<p style="text-align: center; margin-bottom: 20px;"><strong>Se agregaron las sucursales exitosamente!</strong></p>');
			$('#totalContainer').hide();

			// TODO: Mostrar botón 'Aceptar'
			$('#modalAgregaSucursalesFolio #btnCancelarAgrSuc').hide();
			$('#modalAgregaSucursalesFolio #btnAgregarSucursales').hide();
			$('#modalAgregaSucursalesFolio #btnAceptarAgrSuc').show();
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});	
}

/**************************/
/** quejas y comentarios **/
/**************************/

function loadQuejasTable() {
	// TODO: AJAX function
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getQuejas.json',
		dataType: 'json',
		data: {
			idComentario: 0 // Para obtener listado con todas las quejas y comentarios
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se encontraron resultados para esta consulta' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			// Construir tabla
			$('#tblQuejas').html('');
			$('#tblQuejas').html(
					'<thead style="display: block; width: 100%;">' +
					'<tr style="display: inline-table; width: 100%;">' +
					'<th style="width: 20%;">Sucursal</th>' +
					'<th style="width: 10%;">Cliente</th>' +
					'<th style="width: 20%;">Descripción</th>' +
					'<th style="width: 20%;">Comentario</th>' +
					'<th style="width: 10%;">Teléfono</th>' +
					'<th style="width: 10%;">Email</th>' +
					'<th style="width: 10%;">Fecha</th>' +
					'</tr>' +
					'</thead>' +
					'<tbody id="tblBody" style="display: block; width: 100%; max-height: 540px; overflow-y: scroll;"></tbody>'
					);

			for (var x = 0; x < data.length; x++) {
				var html = '';

				html +=	'<tr id="' + data[x].idComentario + '" style="display: inline-table; width: 100%">';
				html +=		'<td style="width: 20%;">' + data[x].descCeco + '</td>';
				html +=		'<td style="width: 10%;">' + data[x].nombreCliente + '</td>';
				html +=		'<td style="width: 20%;">' + data[x].desc + '</td>';
				html +=		'<td style="width: 20%;">' + data[x].comentario + '</td>';

				if (data[x].idContacto == 1) {
					html +=		'<td style="width: 10%;">' + data[x].telefono + '</td>';
					html +=		'<td style="width: 10%;">' + data[x].email + '</td>';
				} else {
					html +=		'<td style="width: 10%;">-</td>';
					html +=		'<td style="width: 10%;">-</td>';
				}

				html +=		'<td style="width: 10%;">' + data[x].fecha + '</td>';
				html += '</tr>';

				$('#tblQuejas #tblBody').append(html);
			}
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function exportQuejas() {
	console.log('exportQuejas');
	// showLoadingSpinner();

	if ($('#tblBody').find('tr').length > 0) {

		/**
		$.post(contextPath + '/central/pedestalDigital/exportQuejas.json', function(data) {
			var blob = new Blob([data], {
				type: 'text/html'
			});
			saveAs(blob, "listado_quejas.xls");
			// hideLoadingSpinner();
		});
		*/

		$('#countdown').html(' ... ');
		$('#tamanoArchivo').html(' calculando tama\u00F1o del archivo ... ');
		$('.toastDescarga').addClass('active');

		// Se asigna evento al boton de cerrar
		$('.toastDescarga').find('.cerrar').on('click', function () {
			// clearInterval(countdownTimer);
			$(this).closest('.toastDescarga').removeClass('active');
		});

		var _url = contextPath + '/central/pedestalDigital/exportQuejas.json';
		$.ajax({
			progress: function(event) {
				var _lengthComputable = event.lengthComputable;
				var _total = event.total;
				var _loaded = event.loaded;

				console.log('_lengthComputable: ' + _lengthComputable);
				console.log('_total: ' + _total);
				console.log('_loaded: ' + _loaded);

				if (_lengthComputable) {
					$('#countdown').html('listado_quejas.xls'); // nombre del archivo
					if (_total < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _total + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total >= 1000 && _total < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_total / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_total / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					}
				} else if (_total === 0 && _loaded > 0) {
					$('#countdown').html('listado_quejas.xls'); // nombre del archivo
					if (_loaded < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _loaded + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded >= 1000 && _loaded < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_loaded / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_loaded / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					}
				}
			},
			cache: false,
			responseType: 'arraybuffer',
			type: 'post',
			url: _url,
			success: function(data) {
				var blob = new Blob([data], {
					type: 'text/html'
				});
				saveAs(blob, "listado_quejas.xls");
				hideLoadingSpinner();
			},
			error: function(obj, errorText) {
				$('.toastDescarga').find('.cerrar').click(); // Se cierra toast

				hideLoadingSpinner();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
						'error' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	} else {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No de pudo descargar el archivo excel' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		// hideLoadingSpinner();
	}
}

function validateFilterReport(id) {
	showLoadingSpinner();

	// Inicializa variables
	var _fechaIni = '';
	var _fechaEnv = '';
	var _doctoVis = '';
	var _categoria = '';
	var _tipoEnvio = '';
	var _ceco = '';
	var _geo = '';

	// Valida el tipo de consulta (1 = General || 2 = Detallada)
	if (id === 1) {
		// Consulta general
		var _list = $('#listaLanzamientosA').find('.divEliminar');
		if (_list.length > 0) {
			var isT, isP, isC;
			for (var x = 0; x < _list.length; x++) {
				var _id = _list[x].id;
				if (_id.includes('T-.-.-')) {
					// Búsqueda por ceco
					isT = true;
					isP = false;
					break;
				} else if (_id.includes('P-.-.-')) {
					// Búsqueda por geografia
					isP = true;
					isT = false;
					break;
				}
			}

			// Si encuentra alguno de tipo T o tipo P rompe el ciclo
			// Especifica el flujo para el tipo de servicio que se va a usar

			_fechaIni = $('#dpFechaVisibleA').val();
			_fechaEnv = $('#dpFechaEnvioA').val();
			_doctoVis = $('#visibleSucursalA').val();

			if (isT) {
				// buscar elementos de tipo T y C
				_ceco = '';
				_categoria = '';

				for (var x = 0; x < _list.length; x++) {
					var _id = _list[x].id;
					_id = _id.split('-.-.-');

					if (_id[0] === 'T') {
						_ceco += _id[1] + ',';
					} else if (_id[0] === 'C') {
						_categoria += _id[1] + ',';
					}
				}

				_ceco = _ceco.substring(0, _ceco.length - 1);
				_categoria = _categoria.substring(0, _categoria.length - 1);

				if (_categoria.length < 1) {
					_categoria = '0';
				}

				_jsonReport = {
						tipo: 1,
						fechaIni: _fechaIni,
						fechaEnv: _fechaEnv,
						doctoVis: _doctoVis,
						categoria: _categoria,
						ceco: _ceco
				};

				createTableReportCeco(_fechaIni, _fechaEnv, _doctoVis, _categoria, _ceco);
			} else if (isP) {
				// buscar elementos de tipo P y C
				_geo = '';
				_categoria = '';

				for (var x = 0; x < _list.length; x++) {
					var _id = _list[x].id;
					_id = _id.split('-.-.-');

					if (_id[0] === 'P') {
						_geo += _id[1] + ',';
					} else if (_id[0] === 'C') {
						_categoria += _id[1] + ',';
					}
				}

				_geo = _geo.substring(0, _geo.length - 1);
				_categoria = _categoria.substring(0, _categoria.length - 1);

				if (_categoria.length < 1) {
					_categoria = '0';
				}

				_jsonReport = {
						tipo: 2,
						fechaIni: _fechaIni,
						fechaEnv: _fechaEnv,
						doctoVis: _doctoVis,
						categoria: _categoria,
						geo: _geo
				};

				createTableReportGeo(_fechaIni, _fechaEnv, _doctoVis, _categoria, _geo);
			} else {
				// buscar elementos de tipo C
				_categoria = '';
				_ceco = '';

				for (var x = 0; x < _list.length; x++) {
					var _id = _list[x].id;
					_id = _id.split('-.-.-');

					if (_id[0] === 'C') {
						_categoria += _id[1] + ',';
					}
				}

				_categoria = _categoria.substring(0, _categoria.length - 1);

				if (_categoria.length < 1) {
					_categoria = '0';
				}

				_jsonReport = {
						tipo: 1,
						fechaIni: _fechaIni,
						fechaEnv: _fechaEnv,
						doctoVis: _doctoVis,
						categoria: _categoria,
						ceco: _ceco
				};

				createTableReportCeco(_fechaIni, _fechaEnv, _doctoVis, _categoria, _ceco);
			}
		} else {
			// búsqueda por folio
			_fechaIni = $('#dpFechaVisibleA').val();
			_fechaEnv = $('#dpFechaEnvioA').val();
			_doctoVis = $('#visibleSucursalA').val();
			_categoria = '0'; // Se manda cero como valor por default
			_ceco = $('#buscarPorSucursalA').val();

			_jsonReport = {
					tipo: 1,
					fechaIni: _fechaIni,
					fechaEnv: _fechaEnv,
					doctoVis: _doctoVis,
					categoria: _categoria,
					ceco: _ceco
			};

			if (_ceco.length > 0) {
				// obtieneCecosPorCSV.json

				$.ajax({
					type: 'get',
					cache: false,
					url: contextPath + '/central/pedestalDigital/obtieneCecosPorCSV.json',
					dataType: 'json',
					data: {
						csvList: _ceco,
						negocio: 41 // Validar negocio
					},
					success: function(data) {
						if (data.length > 0) {
							var _cecos = '';
							for (var x = 0; x < data.length; x++) {
								_cecos += data[x].idCeco + ',';
							}

							_cecos = _cecos.substring(0, _cecos.length - 1);
							
							createTableReportCeco(_fechaIni, _fechaEnv, _doctoVis, _categoria, _cecos);
						}
					},
					error: function(obj, errorText) {
						// console.log('No se pudo insertar el registro.');
						Swal.fire({
							html: '<div style="width: 100%;">' +
								'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
								'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
								'</div>',
							showCloseButton: false,
							confirmButtonText: 'Aceptar',
							confirmButtonColor: '#006341'
						});
						return false;
					},
					complete: function() {
						hideLoadingSpinner();
					}
				});
			} else {
				createTableReportCeco(_fechaIni, _fechaEnv, _doctoVis, _categoria, _ceco);
			}
		}
	} else if (id === 2) {
		// Consulta detallada
		var _numFolio = $('#buscarPorFolioB').val();
		if (_numFolio.length > 0) {
			_jsonReport = {
					tipo: 3,
					idFolio: _numFolio
			};

			// búsqueda por folio
			$.ajax({
				type: 'get',
				cache: false,
				url: contextPath + '/central/pedestalDigital/filtroRepDetaIdFolio.json',
				dataType: 'json',
				data: {
					idFolio: _numFolio
				},
				success: function(data) {
					// function que construye tabla en reporteria...
					createReporteriaTableDeta(data);
				},
				error: function(obj, errorText) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				},
				complete: function() {
					hideLoadingSpinner();
				}
			});
		} else {
			var _list = $('#listaLanzamientosB').find('.divEliminar');
			if (_list.length > 0) {
				var isT, isP, isC;
				for (var x = 0; x < _list.length; x++) {
					var _id = _list[x].id;
					if (_id.includes('T-.-.-')) {
						// Búsqueda por ceco
						isT = true;
						isP = false;
						break;
					} else if (_id.includes('P-.-.-')) {
						// Búsqueda por geografia
						isP = true;
						isT = false;
						break;
					}
				}

				// Si encuentra alguno de tipo T o tipo P rompe el ciclo
				// Especifica el flujo para el tipo de servicio que se va a usar

				_fechaIni = $('#dpFechaVisibleB').val();
				_fechaEnv = $('#dpFechaEnvioB').val();
				_doctoVis = $('#visibleSucursalB').val();
				_tipoEnvio = $('#tipoEnvioB').val();

				if (isT) {
					// buscar elementos de tipo T y C
					_ceco = '';
					_categoria = '';

					for (var x = 0; x < _list.length; x++) {
						var _id = _list[x].id;
						_id = _id.split('-.-.-');

						if (_id[0] === 'T') {
							_ceco += _id[1] + ',';
						} else if (_id[0] === 'C') {
							_categoria += _id[1] + ',';
						}
					}

					_ceco = _ceco.substring(0, _ceco.length - 1);
					_categoria = _categoria.substring(0, _categoria.length - 1);

					if (_categoria.length < 1) {
						_categoria = '0';
					}

					_jsonReport = {
							tipo: 1,
							fechaIni: _fechaIni,
							fechaEnv: _fechaEnv,
							doctoVis: _doctoVis,
							categoria: _categoria,
							tipoEnvio: _tipoEnvio,
							ceco: _ceco
					};

					createTableReportDetalladoCeco(_fechaIni, _fechaEnv, _doctoVis, _categoria, _tipoEnvio, _ceco);
				} else if (isP) {
					// buscar elementos de tipo P y C
					_geo = '';
					_categoria = '';

					for (var x = 0; x < _list.length; x++) {
						var _id = _list[x].id;
						_id = _id.split('-.-.-');

						if (_id[0] === 'P') {
							_geo += _id[1] + ',';
						} else if (_id[0] === 'C') {
							_categoria += _id[1] + ',';
						}
					}

					_geo = _geo.substring(0, _geo.length - 1);
					_categoria = _categoria.substring(0, _categoria.length - 1);

					if (_categoria.length < 1) {
						_categoria = '0';
					}

					_jsonReport = {
							tipo: 2,
							fechaIni: _fechaIni,
							fechaEnv: _fechaEnv,
							doctoVis: _doctoVis,
							categoria: _categoria,
							tipoEnvio: _tipoEnvio,
							geo: _geo
					};

					createTableReportDetalladoGeo(_fechaIni, _fechaEnv, _doctoVis, _categoria, _tipoEnvio, _geo);
				} else {
					// buscar elementos de tipo C
					_categoria = '';
					_ceco = '';

					for (var x = 0; x < _list.length; x++) {
						var _id = _list[x].id;
						_id = _id.split('-.-.-');

						if (_id[0] === 'C') {
							_categoria += _id[1] + ',';
						}
					}

					_categoria = _categoria.substring(0, _categoria.length - 1);

					if (_categoria.length < 1) {
						_categoria = '0';
					}

					_jsonReport = {
							tipo: 1,
							fechaIni: _fechaIni,
							fechaEnv: _fechaEnv,
							doctoVis: _doctoVis,
							categoria: _categoria,
							tipoEnvio: _tipoEnvio,
							ceco: _ceco
					};

					createTableReportDetalladoCeco(_fechaIni, _fechaEnv, _doctoVis, _categoria, _tipoEnvio, _ceco);
				}
			} else {
				var _sucursal = $('#buscarPorSucursalB').val();
				_fechaIni = $('#dpFechaVisibleB').val();
				_fechaEnv = $('#dpFechaEnvioB').val();
				_doctoVis = $('#visibleSucursalB').val();
				_tipoEnvio = $('#tipoEnvioB').val();
				_categoria = '0'; // Se manda cero como valor por default

				if (_sucursal.length > 0) {
					_ceco = _sucursal;
					_jsonReport = {
							tipo: 1,
							fechaIni: _fechaIni,
							fechaEnv: _fechaEnv,
							doctoVis: _doctoVis,
							categoria: _categoria,
							tipoEnvio: _tipoEnvio,
							ceco: _ceco
					};

					$.ajax({
						type: 'get',
						cache: false,
						url: contextPath + '/central/pedestalDigital/obtieneCecosPorCSV.json',
						dataType: 'json',
						data: {
							csvList: _ceco,
							negocio: 41 // Validar negocio
						},
						success: function(data) {
							if (data.length > 0) {
								var _cecos = '';
								for (var x = 0; x < data.length; x++) {
									_cecos += data[x].idCeco + ',';
								}

								_cecos = _cecos.substring(0, _cecos.length - 1);
								createTableReportDetalladoCeco(_fechaIni, _fechaEnv, _doctoVis, _categoria, _tipoEnvio, _cecos);
							}
						},
						error: function(obj, errorText) {
							// console.log('No se pudo insertar el registro.');
							Swal.fire({
								html: '<div style="width: 100%;">' +
									'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
									'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
									'</div>',
								showCloseButton: false,
								confirmButtonText: 'Aceptar',
								confirmButtonColor: '#006341'
							});
							return false;
						},
						complete: function() {
							hideLoadingSpinner();
						}
					});
				} else {
					_jsonReport = {
							tipo: 1,
							fechaIni: _fechaIni,
							fechaEnv: _fechaEnv,
							doctoVis: _doctoVis,
							categoria: _categoria,
							tipoEnvio: _tipoEnvio,
							ceco: _ceco
					};

					createTableReportDetalladoCeco(_fechaIni, _fechaEnv, _doctoVis, _categoria, _tipoEnvio, _ceco);
				}
			}
		}
	}
}

function reporteriaNuevaBusq(id) {
	_objBackup = '';
	_objNameBackup = '';
	_jsonReport = '';

	if (id === 1) {
		var visibleSucursalA = new Dropkick('#visibleSucursalA');
		visibleSucursalA.select(0);

		$('#dpFechaVisibleA').val('');
		$('#dpFechaEnvioA').val('');
		$('#tblReporteriaA #tblBody').html('');
		$('#listaLanzamientosA').html('');

		// Se cambian clases de elementos, para mostrar que se encuentran inhabilitados
		$('#btnTerritorioA').removeClass('btnDistribDis');
		$('#btnTerritorioA').addClass('btnDistrib');
		$('#btnPaisA').removeClass('btnDistribDis');
		$('#btnPaisA').addClass('btnDistrib');
		$('#btnCategoriaA').removeClass('btnDistribDis');
		$('#btnCategoriaA').addClass('btnDistrib');

		$('#psA').removeClass('desac');
		$('#buscarPorSucursalA').val('');

		$('#btnTerritorioA').click(function(e) {
			_nivelT = 0;
			_cecos = '0';
			_boxId = 1;
			_tabId = 1;
			_objBackup = '';
			_objNameBackup = '';
			getTerritorioReporteria(1);
		});
		$('#btnPaisA').click(function(e) {
			_nivelP = 0;
			_cecos = '0';
			_boxId = 2;
			_tabId = 1;
			getGeografiaReporteria(1);
		});
		$('#btnCategoriaA').click(function(e) {
			_nivelC = 1;
			_cecos = '0';
			_boxId = 3;
			_tabId = 1;
			getCaategoriasReporteria(1);
		});
	} else if (id === 2) {
		var visibleSucursalB = new Dropkick('#visibleSucursalB');
		visibleSucursalB.select(0);
		var tipoEnvioB = new Dropkick('#tipoEnvioB');
		tipoEnvioB.select(0);

		$('#dpFechaVisibleB').val('');
		$('#dpFechaEnvioB').val('');
		// $('#tblReporteriaB #tblBody').html('');
		$('#tblReporteriaB').html('');
		$('#listaLanzamientosB').html('');

		// Se cambian clases de elementos, para mostrar que se encuentran inhabilitados
		$('#btnTerritorioB').removeClass('btnDistribDis');
		$('#btnTerritorioB').addClass('btnDistrib');
		$('#btnPaisB').removeClass('btnDistribDis');
		$('#btnPaisB').addClass('btnDistrib');
		$('#btnCategoriaB').removeClass('btnDistribDis');
		$('#btnCategoriaB').addClass('btnDistrib');

		$('#psB').removeClass('desac');
		$('#buscarPorSucursalB').val('');

		$('#pfB').removeClass('desac');
		$('#buscarPorFolioB').val('');

		$('#btnTerritorioB').click(function(e) {
			_nivelT = 0;
			_cecos = '0';
			_boxId = 1;
			_tabId = 2;
			_objBackup = '';
			_objNameBackup = '';
			getTerritorioReporteria(2);
		});
		$('#btnPaisB').click(function(e) {
			_nivelP = 0;
			_cecos = '0';
			_boxId = 2;
			_tabId = 2;
			getGeografiaReporteria(2);
		});
		$('#btnCategoriaB').click(function(e) {
			_nivelC = 1;
			_cecos = '0';
			_boxId = 3;
			_tabId = 2;
			getCaategoriasReporteria(2);
		});
	}
}

function createTableReportCeco(_fechaIni, _fechaEnv, _doctoVis, _categoria, _ceco) {
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/filtroRepGralCec.json',
		dataType: 'json',
		data: {
			fechaIni: _fechaIni,
			fechaEnv: _fechaEnv,
			doctoVis: _doctoVis,
			categoria: _categoria,
			ceco: _ceco
		},
		success: function(data) {
			// function que construye tabla en reporteria...
			createReporteriaTable(data);
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function createTableReportGeo(_fechaIni, _fechaEnv, _doctoVis, _categoria, _geo) {
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/filtroRepGralGeo.json',
		dataType: 'json',
		data: {
			fechaIni: _fechaIni,
			fechaEnv: _fechaEnv,
			doctoVis: _doctoVis,
			categoria: _categoria,
			geo: _geo
		},
		success: function(data) {
			// function que construye tabla en reporteria...
			createReporteriaTable(data);
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function createReporteriaTable(data) {
	// Construir tabla vacía
	$('#tblReporteriaA #tblBody').html('');

	if (data.length < 1) {
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No hay datos que mostrar.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	$('#tblReporteriaA').html(
			'<thead style="display: block; width: 100%;">' +
			'<tr style="display: inline-table; width: 100%;">' +
				'<th style="width: 25%;">Nombre del documento</th>' +
				'<th style="width: 15%;">Categor\u00EDa</th>' +
				'<th style="width: 10%;">Vigente</th>' +
				'<th style="width: 10%;">Estatus</th>' +
				'<th style="width: 20%;">Fecha env\u00EDo</th>' +
				'<th style="width: 20%;">Fecha visualización</th>' +
			'</tr>' +
		'</thead>' +
		'<tbody id="tblBody" style="display: block; width: 100%; max-height: 240px; overflow-y: scroll;"></tbody>'
	);

	for (var x = 0; x < data.length; x++) {
		var html = '';

		html +=	'<tr id="' + data[x].idDocumento + '" style="display: inline-table; width: 100%">';
		html +=		'<td style="width: 25%;">' + data[x].nombreDoc + '</td>';
		html +=		'<td style="width: 15%;">' + data[x].descCategoria + '</td>';
		html +=		'<td style="width: 10%;">' + data[x].descVisibleSuc + '</td>';
		html +=		'<td style="width: 10%;">' + data[x].descEstatus + '</td>';
		html +=		'<td style="width: 20%;">' + data[x].fechaEnv + '</td>';
		html +=		'<td style="width: 20%;">' + data[x].vigenciaIni + '</td>';
		html += '</tr>';

		$('#tblReporteriaA #tblBody').append(html);
	}
}

function createReporteriaTableDeta(data) {
	// Construir tabla vacía
	$('#tblReporteriaB').html('');

	if (data.length < 1) {
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	$('#tblReporteriaB').html(
			'<thead style="display: block; width: 100%;">' +
			'<tr style="display: inline-table; width: 100%;">' +
				'<th style="width: 7%;">Territorio</th>' +
				'<th style="width: 7%;">Zona</th>' +
				'<th style="width: 7%;">Región</th>' +
				'<th style="width: 7%;">País</th>' +
				'<th style="width: 7%;">Estado</th>' +
				'<th style="width: 7%;">Municipio</th>' +
				'<th style="width: 11%;">Sucursal</th>' +
				'<th style="width: 7%;">Categor\u00EDa</th>' +
				'<th style="width: 12%;">Documento</th>' +
				'<th style="width: 7%;">Estatus del documento</th>' +
				'<th style="width: 7%;">Fecha de envío</th>' +
				'<th style="width: 7%;">Fecha de visualización</th>' +
			'</tr>' +
		'</thead>' +
		'<tbody id="tblBody" style="display: block; width: 100%; max-height: 300px; overflow-y: scroll;"></tbody>'
	);

	for (var x = 0; x < data.length; x++) {
		var html = '';

		html +=	'<tr id="' + data[x].idDocumento + '" style="display: inline-table; width: 100%">';
		html +=		'<td style="width: 7%;">' + data[x].territorio + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].zona + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].region + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].pais + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].estado + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].municipio + '</td>';
		html +=		'<td style="width: 11%;"> (' + data[x].idSucursal + ') ' + data[x].nombre + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].descCategoria + '</td>';
		html +=		'<td style="width: 12%;">' + data[x].nombreDoc + '</td>';
		// html +=		'<td style="width: 7%;">' + data[x].envio + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].descVisibleSuc + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].fechaEnv + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].vigenciaIni + '</td>';
		html += '</tr>';

		$('#tblReporteriaB #tblBody').append(html);
	}
}

function createReporteriaTableDetaCeco(data) {
	// Construir tabla vacía
	$('#tblReporteriaB').html('');

	if (data.length < 1) {
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	$('#tblReporteriaB').html('');
	$('#tblReporteriaB').html(
			'<thead style="display: block; width: 100%;">' +
			'<tr style="display: inline-table; width: 100%;">' +
				'<th style="width: 8%;">Territorio</th>' +
				'<th style="width: 8%;">Zona</th>' +
				'<th style="width: 8%;">Región</th>' +
				'<th style="width: 8%;">País</th>' +
				'<th style="width: 8%;">Estado</th>' +
				'<th style="width: 8%;">Municipio</th>' +
				'<th style="width: 11%;">Sucursal</th>' +
				'<th style="width: 8%;">Categor\u00EDa</th>' +
				'<th style="width: 12%;">Documento</th>' +
				'<th style="width: 7%;">Visible en sucursal</th>' +
				'<th style="width: 7%;">Fecha de envío</th>' +
				'<th style="width: 7%;">Fecha de visualización</th>' +
			'</tr>' +
		'</thead>' +
		'<tbody id="tblBody" style="display: block; width: 100%; max-height: 300px; overflow-y: scroll;"></tbody>'
	);

	for (var x = 0; x < data.length; x++) {
		var html = '';

		html +=	'<tr id="' + data[x].idDocumento + '" style="display: inline-table; width: 100%">';
		html +=		'<td style="width: 8%;">' + data[x].territorio + '</td>';
		html +=		'<td style="width: 8%;">' + data[x].zona + '</td>';
		html +=		'<td style="width: 8%;">' + data[x].region + '</td>';
		html +=		'<td style="width: 8%;">' + data[x].pais + '</td>';
		html +=		'<td style="width: 8%;">' + data[x].estado + '</td>';
		html +=		'<td style="width: 8%;">' + data[x].municipio + '</td>';
		html +=		'<td style="width: 11%;"> (' + data[x].idSucursal + ') ' + data[x].nombre + '</td>';
		html +=		'<td style="width: 8%;">' + data[x].descCategoria + '</td>';
		html +=		'<td style="width: 12%;">' + data[x].nombreDoc + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].descVisibleSuc + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].fechaEnv + '</td>';
		html +=		'<td style="width: 7%;">' + data[x].vigenciaIni + '</td>';
		html += '</tr>';

		$('#tblReporteriaB #tblBody').append(html);
	}
}

function createTableReportDetalladoCeco(_fechaIni, _fechaEnv, _doctoVis, _categoria, _tipoEnvio, _ceco) {
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/filtroRepDetaCec.json',
		dataType: 'json',
		data: {
			fechaIni: _fechaIni,
			fechaEnv: _fechaEnv,
			doctoVis: _doctoVis,
			categoria: _categoria,
			tipoEnvio: _tipoEnvio,
			ceco: _ceco
		},
		success: function(data) {
			// function que construye tabla en reporteria...
			createReporteriaTableDetaCeco(data);
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function createTableReportDetalladoGeo(_fechaIni, _fechaEnv, _doctoVis, _categoria, _tipoEnvio, _geo) {
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/filtroRepDetaGeo.json',
		dataType: 'json',
		data: {
			fechaIni: _fechaIni,
			fechaEnv: _fechaEnv,
			doctoVis: _doctoVis,
			categoria: _categoria,
			tipoEnvio: _tipoEnvio,
			geo: _geo
		},
		success: function(data) {
			// function que construye tabla en reporteria...
			createReporteriaTableDeta(data);
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function exportReporteGeneral() {
	console.log('exportReporteGeneral');
	// showLoadingSpinner();

	var _data = _jsonReport != undefined ? _jsonReport : '';
	if (_jsonReport != undefined && _jsonReport != null && _jsonReport != '') {

		/**
		$.post(contextPath + '/central/pedestalDigital/exportTablaReporteria.json?data=' + encodeURIComponent(JSON.stringify(_data)), function(data) {
			var blob = new Blob([data], {
				type: 'text/html'
			});
			saveAs(blob, "reporte_general.xls");
			// hideLoadingSpinner();
		});
		*/

		$('#countdown').html(' ... ');
		$('#tamanoArchivo').html(' calculando tama\u00F1o del archivo ... ');
		$('.toastDescarga').addClass('active');

		// Se asigna evento al boton de cerrar
		$('.toastDescarga').find('.cerrar').on('click', function () {
			// clearInterval(countdownTimer);
			$(this).closest('.toastDescarga').removeClass('active');
		});

		var _url = contextPath + '/central/pedestalDigital/exportTablaReporteria.json';
		$.ajax({
			progress: function(event) {
				var _lengthComputable = event.lengthComputable;
				var _total = event.total;
				var _loaded = event.loaded;

				console.log('_lengthComputable: ' + _lengthComputable);
				console.log('_total: ' + _total);
				console.log('_loaded: ' + _loaded);

				if (_lengthComputable) {
					$('#countdown').html('reporte_general.xls'); // nombre del archivo
					if (_total < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _total + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total >= 1000 && _total < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_total / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_total / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					}
				} else if (_total === 0 && _loaded > 0) {
					$('#countdown').html('reporte_general.xls'); // nombre del archivo
					if (_loaded < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _loaded + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded >= 1000 && _loaded < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_loaded / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_loaded / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					}
				}
			},
			cache: false,
			responseType: 'arraybuffer',
			type: 'post',
			url: _url,
			data: {
				json: encodeURIComponent(JSON.stringify(_data))
			},
			success: function(data) {
				var blob = new Blob([data], {
					type: 'text/html'
				});
				saveAs(blob, 'reporte_general.xls');
			},
			error: function(obj, errorText) {
				$('.toastDescarga').find('.cerrar').click(); // Se cierra toast

				hideLoadingSpinner();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
						'error' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	} else {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe realizar una consulta antes de poder exportar los datos' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		// hideLoadingSpinner();
	}
}

function exportReporteDetallado() {
	console.log('exportReporteDetallado');

	// showLoadingSpinner();
	if (_jsonReport != undefined && _jsonReport != null && _jsonReport != '') {

		/**
		$.post(contextPath + '/central/pedestalDigital/exportReporteriaDetallada.json?data=' + JSON.stringify(_jsonReport), function(data) {
			var blob = new Blob([data], {
				type: 'text/html'
			});
			saveAs(blob, "reporte_detallado.xls");
		});
		*/

		$('#countdown').html(' ... ');
		$('#tamanoArchivo').html(' calculando tama\u00F1o del archivo ... ');
		$('.toastDescarga').addClass('active');

		// Se asigna evento al boton de cerrar
		$('.toastDescarga').find('.cerrar').on('click', function () {
			// clearInterval(countdownTimer);
			$(this).closest('.toastDescarga').removeClass('active');
		});

		var _url = contextPath + '/central/pedestalDigital/exportReporteriaDetallada.json';
		$.ajax({
			progress: function(event) {
				var _lengthComputable = event.lengthComputable;
				var _total = event.total;
				var _loaded = event.loaded;

				console.log('_lengthComputable: ' + _lengthComputable);
				console.log('_total: ' + _total);
				console.log('_loaded: ' + _loaded);

				if (_lengthComputable) {
					$('#countdown').html('reporte_detallado.xls'); // nombre del archivo
					if (_total < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _total + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total >= 1000 && _total < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_total / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_total / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					}
				} else if (_total === 0 && _loaded > 0) {
					$('#countdown').html('reporte_detallado.xls'); // nombre del archivo
					if (_loaded < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _loaded + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded >= 1000 && _loaded < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_loaded / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_loaded / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					}
				}
			},
			cache: false,
			responseType: 'arraybuffer',
			type: 'post',
			url: _url,
			data: {
				data: encodeURIComponent(JSON.stringify(_jsonReport)) // JSON.stringify(_jsonReport)
			},
			success: function(data) {
				console.log('exportReporteDetallado - success');
				var blob = new Blob([data], {
					type: 'text/html'
				});
				saveAs(blob, 'reporte_detallado.xls');
			},
			error: function(obj, errorText) {
				console.log('exportReporteDetallado - error');
				$('.toastDescarga').find('.cerrar').click(); // Se cierra toast
				hideLoadingSpinner();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
						'No se pudo descargar el archivo de Excel' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	} else {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe realizar una consulta antes de poder exportar los datos' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		hideLoadingSpinner();
	}
}

function cleanNuevaCarga() {
	$('#listaLanzamientos').html('');

	var _dkTipoEnvio = new Dropkick('#tipoEnvio');
	_dkTipoEnvio.select(0);
	_dkTipoEnvio.disable(0, false);
	_dkTipoEnvio.disable(1, false);
	_dkTipoEnvio.disable(2, false);
	_dkTipoEnvio.disable(3, false);
	_maxFilezSize = 0;

	$('#btnListTerritorio').removeClass('btnDistribDis');
	$('#btnListTerritorio').addClass('btnDistrib');
	$('#btnListTerritorio').click(function(e) {
		_cecos = '0';
		_nivelC = 0;
		_boxId = 1;
		getCecoDataList();
	});

	$('#btnListPais').removeClass('btnDistribDis');
	$('#btnListPais').addClass('btnDistrib');
	$('#btnListPais').click(function(e) {
		_cecos = '0';
		_nivelT = 0;
		_boxId = 2;
		getGeoDataList();
	});

	$('#btnListaDistribucion').removeClass('btnDistribDis');
	$('#btnListaDistribucion').addClass('btnDistrib');
	$('#btnListaDistribucion').click(function() {
		showDistribListPopUp('listaDistribucionPD.htm', 'Listas de Distribuci\u00F3n', 1000, 600);
	});

	$('#btnCargaCSV').removeClass('btnDistribDis');
	$('#btnCargaCSV').addClass('btnDistrib');
	$('#btnCargaCSV').click(function() {
		addFileToCargarCsvInput();
	});
}

function closeCSVModalCA() {
	$('.modalCloseImg').click();
}

function cancelDocumentData() {
	// TODO: Validar si se debe eliminar toda la información relacionada a las categorias y sus documentos.
	$('#modalCarga2 .btnCerrar').click();
}

function updateStatusTablets() {
	console.log('updateStatusTablets');
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/updateTabletsEstatus.json',
		dataType: 'json',
		success: function(data) {
			// console.log(data);
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Algo ocurri\u00F3 al actualizar la informaci\u00F3n de las tabletas' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

/*********************************/

function run() {
	Array.from(document.querySelectorAll('.circle-meter')).forEach(CircleMeter);
}

function CircleMeter(meterElement) {
	const circle = meterElement.querySelector('svg > circle + circle');
	const numberElement = meterElement.querySelector('.softskills-percentage');
	const score = meterElement.dataset.score;
	const normalizedScore = (100 - meterElement.dataset.score) / 100;
	const maxDuration = 13;
	//set initial stroke offset
	circle.style.strokeDashoffset = 1;
	const duration = Math.floor(Math.random() * Math.floor(maxDuration));
	const transitionEnd = event => {
		circle.removeEventListener('transitionend', transitionEnd);
		meterElement.classList.remove('animatable');
	}
	circle.addEventListener('transitionend', transitionEnd);
	setTimeout(() => {
		meterElement.classList.add('animatable');
		let transitionDuration = window.getComputedStyle(circle).transitionDuration;
		increaseNumber(numberElement, score, duration);
		circle.style.transitionDuration = `${duration}s`;
		circle.style.strokeDashoffset = normalizedScore;
	}, 1000);
}

function increaseNumber(numberElement, score, duration) {
	const startTime = Date.now();
	const callback = function() {
		const timePassed = (Date.now() - startTime) / 1000;
		const currentScore = Math.floor(score * (timePassed / duration), score);
		numberElement.textContent = `${currentScore}%`;
		if (timePassed < duration) {
			requestAnimationFrame(callback);
		}
	}
	requestAnimationFrame(callback);
}

function enabledivDPVF(obj) {
	var _checked = $(obj).is(':checked');
	if (_checked) {
		$('#dpVigenciaFinal').show();
	} else {
		$('#dpVigenciaFinal').hide();
	}
}

function validateDocsBeforeUpload() {
	console.log('validateDocsBeforeUpload');

	var _docsList = jsonDocs;
	if (_docsList.length > 0) {
		var x;
		var haltProcess = false;
		var docs = [];

		$.each(_docsList, function(key, val) {
			var docId = val.documentId;
			var ajaxCall = $.ajax({
				type: 'get',
				cache: false,
				url: 'getDocsBeforeUpload.json',
				dataType: 'json',
				async: false,
				data: {
					op: 0,
					tipoDoc: docId
				},
				success: function(data) {
					// console.log(data);

					// TODO: Validar si devuelve documentos para interrumpir el
					// proceso de carga
					if (data.length > 0) {
						var i;
						for (i = 0; i < data.length; i++) {
							docs.push(data[i]);
						}
						haltProcess = true;
					}
				},
				error: function(obj, errorText) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
							'Debe agregar documentos antes de continuar.' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
				},
				complete: function() {
					hideLoadingSpinner();
					return haltProcess;
				}
			});
		});
		// console.log(docs);
		return {
			data: docs,
			haltProcess: haltProcess
			};
	} else {
		// Interrumpir proceso de carga, no se han cargado documentos
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
				'Algo ocurri\u00F3 al obtener la informaci\u00F3n.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}
}

function cancelLoadProcess() {
	console.log('cancelLoadProcess');
	$.modal.close();

	Swal.fire({
		html: '<div style="width: 100%;">' +
			'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
			'El proceso de carga de archivos fue cancelado.' +
			'</div>',
		showCloseButton: false,
		confirmButtonText: 'Aceptar',
		confirmButtonColor: '#006341'
	});
	// location.reload();
}

function cancelDocs() {
	console.log('cancelDocs');

	// Inactivar todos los documentos de la tabla y continuar con el proceso de carga...
	/******************************************************************/

	var centinel = false;
	var list = $('#listDocsBU').find('tr');
	if (list.length > 0) {
		$.each(list, function (key, val) {
			// console.log($(this).attr('id'));
			var _id = $(this).attr('id');
			$.ajax({
				type: 'get',
				cache: false,
				url: 'updateDocto.json',
				dataType: 'json',
				data: {
					idDocumento: _id,
					idFolio: 0,
					nombreDocto: null,
					idTipoDocto: 0,
					idUsuario: 0,
					pesoBytes: 0,
					origen: null,
					vigenciaIni: null,
					vigenciaFin: null,
					visibleSuc: null,
					idEstatus: 0,
					activo: 0
				},
				async: false,
				success: function(data) {
					// console.log(data);
					if (data.respuesta == '0') {
						centinel = true;
					}
				},
				error: function(obj, errorText) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
							'Algo ocurri\u00F3 al inactivar los documentos.' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
				},
				complete: function() {
					hideLoadingSpinner();
				}
			});
		});

		if (centinel) {
			// function que obtiene los valores del contenedor de destinos
			saveDistribData();
			// function que valida el tipo de envio
			saveEnvData();
			if (validaDocs && validaEnv && validaDistrib) {
				$('#modalCarga2 .btnCerrar').click();
				showLoadingSpinner();
				pdDropzone.processQueue();
			} else {
				if (!validaDocs) {
					Swal.fire({
						html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No se pudo guardar la informaci\u00F3n de los documentos, intente nuevamente.' + '</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}
				if (!validaEnv) {
					Swal.fire({
						html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No se pudo guardar la informaci\u00F3n de los destinos, intente nuevamente.' + '</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}
				if (!validaDistrib) {
					Swal.fire({
						html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No se pudo guardar la informaci\u00F3n del tipo de envio, intente nuevamente.' + '</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}
			}
		} else {
			Swal.fire({
				html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se pudieron inactivar los documentos, se interrumpe el proceso. Favor de intentar mas tarde.' +
				'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}
	} else {
		// NO HAY ELEMENTOS EN LA TABLA ?????
		return false;
	}

	return false;

	/******************************************************************/

	// function que obtiene los valores del contenedor de destinos
	saveDistribData();

	// function que valida el tipo de envio
	saveEnvData();

	if (validaDocs && validaEnv && validaDistrib) {
		$('#modalCarga2 .btnCerrar').click();
		showLoadingSpinner();
		pdDropzone.processQueue();
	} else {
		if (!validaDocs) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo guardar la informaci\u00F3n de los documentos, intente nuevamente.' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}

		if (!validaEnv) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo guardar la informaci\u00F3n de los destinos, intente nuevamente.' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}

		if (!validaDistrib) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo guardar la informaci\u00F3n del tipo de envio, intente nuevamente.' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		}
	}
}

function loadNotificacionCenter() {
	console.log('loadNotificacionCenter');
	showLoadingSpinner();

	var nNotif = 0;
	// Para documentos por expirar
	// getExpiringDocs.json

	$.ajax({
		type: 'get',
		cache: false,
		url: 'getExpiringDocs.json',
		dataType: 'json',
		async: false,
		success: function(data) {
			// console.log(data);
			nNotif += data.length;
			if (data.length > 0) {
				for (var i = 0; i < data.length; i++) {
					var fecha = data[i].fdvigencia_fin;
					fecha = fecha.substring(0, 10);
					var _day = fecha.substring(0, 2);
					var _month = fecha.substring(3, 5);
					var _year = fecha.substring(6, 10);
					var date1 = new Date(_year + '-' + _month + '-' + _day + 'CST');
					var date2 = new Date();
					var numDias = Math.floor((Date.UTC(date1.getFullYear(), date1.getMonth(), date1.getDate()) - Date.UTC(date2.getFullYear(), date2.getMonth(), date2.getDate())) / (1000 * 60 * 60 * 24)); // date1.getDate() - date2.getDate();

					var _idFolio = '';
					var cadena = '';
					var z;

					if ((i+1 < data.length) && (data[i].fifolio == data[i + 1].fifolio)) {
						cadena = '';
						for (z = i; z < data.length; z++) {
							if (z == i) {
								_idFolio = data[z].fifolio;
								cadena += data[z].fifolio + ',,' + data[z].fcnombre_docto + ',,' + data[z].desc_categoria + ',,' + data[z].fdvigencia_ini + ',,' + data[z].fdvigencia_fin + ',,' + data[z].fcvisible_suc + ',,' + data[z].fctipo_envio + ',,' + data[z].fdfecha_envio + '--';
							}

							if (data[z].fifolio == data[z + 1].fifolio) {
								nNotif--;
								cadena += data[z+1].fifolio + ',,' + data[z+1].fcnombre_docto + ',,' + data[z+1].desc_categoria + ',,' + data[z+1].fdvigencia_ini + ',,' + data[z+1].fdvigencia_fin + ',,' + data[z+1].fcvisible_suc + ',,' + data[z+1].fctipo_envio + ',,' + data[z+1].fdfecha_envio + '--';
							} else {
								i = i + z;
								z = data.length;
							}
						}
					} else {
						_idFolio = data[i].fifolio;
						cadena += data[i].fifolio + ',,' + data[i].fcnombre_docto + ',,' + data[i].desc_categoria + ',,' + data[i].fdvigencia_ini + ',,' + data[i].fdvigencia_fin + ',,' + data[i].fcvisible_suc + ',,' + data[i].fctipo_envio + ',,' + data[i].fdfecha_envio;
					}

					if (cadena.substring(cadena.length-2) == '--') {
						cadena = cadena.substring(0, cadena.length-2);
					}

					// console.log('alertsList: ' + cadena);
					$('#alertsList').append('<li class="active"><a href="javascript: void(0);" onclick="showModalNotif(1,\'' + cadena + '\');">El folio ' + _idFolio + ' vence en ' + numDias + ' días' + '</a></li>');
				}
			} else {
				$('#alertsList').html('<p style="text-align: center; margin-top: 30px;">No hay documentos por vencer</p>');
			}
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' + 'Algo ocurri\u00F3 al obtener las notificaciones.' + '</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
		},
		complete: function() {
			$.ajax({
				type: 'get',
				cache: false,
				url: 'getHistoryOfDocs.json',
				dataType: 'json',
				async: false,
				success: function(data) {
					// console.log(data);
					nNotif += data.length;
					if (data.length > 0) {
						for (var i = 0; i < data.length; i++) {
							var _idFolio = '';
							var _fecha = '';
							var cadena = '';
							var z;

							if ((i+1 < data.length) && (data[i].fifolio == data[i + 1].fifolio)) {
								cadena = '';
								for (z = i; z < data.length; z++) {
									if (z == i) {
										_idFolio = data[z].fifolio;
										_fecha = data[z].fdfecha_envio.substring(0, 10);
										cadena += data[z].fifolio + ',,' + data[z].fcnombre_docto + ',,' + data[z].desc_categoria + ',,' + data[z].fdvigencia_ini + ',,' + data[z].fdvigencia_fin + ',,' + data[z].fcvisible_suc + ',,' + data[z].fctipo_envio + ',,' + data[z].fdfecha_envio + '--';
									}

									if (data[z].fifolio == data[z + 1].fifolio) {
										nNotif--;
										cadena += data[z+1].fifolio + ',,' + data[z+1].fcnombre_docto + ',,' + data[z+1].desc_categoria + ',,' + data[z+1].fdvigencia_ini + ',,' + data[z+1].fdvigencia_fin + ',,' + data[z+1].fcvisible_suc + ',,' + data[z+1].fctipo_envio + ',,' + data[z+1].fdfecha_envio + '--';
									} else {
										i = i + z;
										z = data.length;
									}
								}
							} else {
								_idFolio = data[i].fifolio;
								_fecha = data[i].fdfecha_envio.substring(0, 10);
								cadena += data[i].fifolio + ',,' + data[i].fcnombre_docto + ',,' + data[i].desc_categoria + ',,' + data[i].fdvigencia_ini + ',,' + data[i].fdvigencia_fin + ',,' + data[i].fcvisible_suc + ',,' + data[i].fctipo_envio + ',,' + data[i].fdfecha_envio;
							}

							if (cadena.substring(cadena.length-2) == '--') {
								cadena = cadena.substring(0, cadena.length-2);
							}

							// console.log('foliosList: ' + cadena);
							$('#foliosList').append('<li class="active"><a href="javascript: void(0);" onclick="showModalNotif(2,\'' + cadena + '\');">Se creó el folio ' + _idFolio + ' el ' + _fecha + '</a></li>');
						}
					} else {
						$('#foliosList').html('<p style="text-align: center; margin-top: 30px;">No hay folios creados esta semana</p>');
					}

					$('#divHeadNot').html('<div>Notificaciones</div><div>' + nNotif + ' nuevas</div>');
					$('#numNotifPD').html(nNotif);
				},
				error: function(obj, errorText) {
					Swal.fire({
						html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' + 'Algo ocurri\u00F3 al obtener las notificaciones.' + '</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
				},
				complete: function() {
					hideLoadingSpinner();
				}
			});
		}
	});
}

function showModalNotif(type, data) {
	console.log('showModalNotif');

	var arrData,
		aux,
		list = [];
	if (data.includes('--')) {
		arrData = data.split('--');
		var x;
		for (x = 0; x < arrData.length; x++) {
			aux = arrData[x].split(',,');
			list.push(aux);
		}
	} else {
		aux = data.split(',,');
		list.push(aux);
	}

	if (type == 1) {
		$('#notifData').html('');

		var z;
		for (z = 0; z < list.length; z++) {
			var _aux = list[z];
			$('#notifData').append('<tr id="notif' + z + '"></tr>');
			$('#notif' + z).append('<td>' + _aux[0] + '</td>');
			$('#notif' + z).append('<td>' + _aux[1] + '</td>');
			$('#notif' + z).append('<td>' + _aux[2] + '</td>');

			if (_aux[7] != 'null' && _aux[7] != null && _aux[7] != undefined && _aux[7] != '') {
				$('#notif' + z).append('<td>' + _aux[7] + '</td>');
			} else {
				$('#notif' + z).append('<td>-</td>');
			}

			if (_aux[3] != 'null' && _aux[3] != null && _aux[3] != undefined && _aux[3] != '') {
				$('#notif' + z).append('<td>' + _aux[3] + '</td>');
			} else {
				$('#notif' + z).append('<td>-</td>');
			}

			if (_aux[4] != 'null' && _aux[4] != null && _aux[4] != undefined && _aux[4] != '') {
				$('#notif' + z).append('<td>' + _aux[4] + '</td>');
			} else {
				$('#notif' + z).append('<td>-</td>');
			}

			$('#notif' + z).append('<td>' + _aux[6] + '</td>');
			$('#notif' + z).append('<td>' + _aux[5] + '</td>');
		}

		$('#modalNotif').modal();
	} else if (type == 2) {
		$('#notifData2').html('');

		var z;
		for (z = 0; z < list.length; z++) {
			var _aux = list[z];
			$('#notifData2').append('<tr id="fol' + z + '"></tr>');
			$('#fol' + z).append('<td>' + _aux[0] + '</td>');
			$('#fol' + z).append('<td>' + _aux[1] + '</td>');
			$('#fol' + z).append('<td>' + _aux[2] + '</td>');

			if (_aux[7] != 'null' && _aux[7] != null && _aux[7] != undefined && _aux[7] != '') {
				$('#fol' + z).append('<td>' + _aux[7] + '</td>');
			} else {
				$('#fol' + z).append('<td>-</td>');
			}

			if (_aux[3] != 'null' && _aux[3] != null && _aux[3] != undefined && _aux[3] != '') {
				$('#fol' + z).append('<td>' + _aux[3] + '</td>');
			} else {
				$('#fol' + z).append('<td>-</td>');
			}

			if (_aux[4] != 'null' && _aux[4] != null && _aux[4] != undefined && _aux[4] != '') {
				$('#fol' + z).append('<td>' + _aux[4] + '</td>');
			} else {
				$('#fol' + z).append('<td>-</td>');
			}

			$('#fol' + z).append('<td>' + _aux[6] + '</td>');
			$('#fol' + z).append('<td>' + _aux[5] + '</td>');
		}

		$('#modalNotif2').modal();
	}
}

function loadHAF() {
	console.log('loadHAF');
	// ajax call
	$.ajax({
		type: 'get',
		cache: false,
		url: 'getAllFoliosData.json',
		dataType: 'json',
		async: false,
		success: function(data) {
			// console.log(data);
			if (data.length > 0) {
				var z;
				for (z = 0; z < data.length; z++) {
					// console.log(data[z]);
					$('#htContainer').append('<tr id="td' + z + '"></tr>');
					$('#td' + z).append('<td>' + data[z].fifolio + '</td>');
					$('#td' + z).append('<td>' + data[z].fcnombre_docto + '</td>');
					$('#td' + z).append('<td>' + data[z].desc_categoria + '</td>');
					if (data[z].fdfecha_envio != 'null' && data[z].fdfecha_envio != null && data[z].fdfecha_envio != undefined && data[z].fdfecha_envio != '') {
						$('#td' + z).append('<td>' + data[z].fdfecha_envio + '</td>');
					} else {
						$('#td' + z).append('<td>-</td>');
					}
					if (data[z].fdvigencia_ini != 'null' && data[z].fdvigencia_ini != null && data[z].fdvigencia_ini != undefined && data[z].fdvigencia_ini != '') {
						$('#td' + z).append('<td>' + data[z].fdvigencia_ini + '</td>');
					} else {
						$('#td' + z).append('<td>-</td>');
					}
					if (data[z].fdvigencia_fin != 'null' && data[z].fdvigencia_fin != null && data[z].fdvigencia_fin != undefined && data[z].fdvigencia_fin != '') {
						$('#td' + z).append('<td>' + data[z].fdvigencia_fin + '</td>');
					} else {
						$('#td' + z).append('<td>-</td>');
					}
					$('#td' + z).append('<td>' + data[z].fctipo_envio + '</td>');
					$('#td' + z).append('<td>' + data[z].fcvisible_suc + '</td>');
				}

				$('#historicTable').DataTable({
					searching: false,
					info: false,
					pageLength: 25,
					ordering: false,
					"pagingType": "full_numbers",
					language: {
						oPaginate: {
							sNext: '<img class="iconosPag2" src="../../img/iconos/nextIco.svg">',
							sPrevious: '<img class="iconosPag2" src="../../img/iconos/firstIco.svg">',
							sFirst: '<img class="iconosPag" src="../../img/iconos/previousIco.svg">',
							sLast: '<img class="iconosPag" src="../../img/iconos/lastIco.svg">'
						}
					}
				});
			} else {
				// No hay folios
			}
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
				'Algo ocurri\u00F3 al obtener el histórico de los folios.' +
				'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function loadLicenciasSuc(suc) {
	console.log('loadLicenciasSuc');

	var _suc;
	if (suc.length > 0) {
		_suc = suc[0].idCeco.substring(2);
		$.ajax({
			type: 'get',
			cache: false,
			url: 'getLicenciasSuc.json',
			dataType: 'json',
			async: false,
			data: {
				sucursal: _suc
			},
			success: function(data) {
				// console.log(data);
				if (data.length > 0) {
					$('#listLicDocs').html('');
					$('#listContainer').html('');

					for (var x = 0; x < data.length; x++) {
						if (x == 0) {
							$('#listLicDocs').append('<div class="docTab active" tab="1"><div style="margin: 0 auto;text-align: left;width: 100%;">' + data[x].tipo + '</div></div>');
							if (data[x].fileBase64.length > 0) {
								$('#listContainer').append(
										'<div class="tabCont active" tab="1">' +
										'<div class="tit">' + data[x].tipo + '</div>' +
										'<iframe src="data:application/pdf;base64,' + data[x].fileBase64 + '" frameborder="0" height="100%" width="100%"></iframe>' +
										'</div>'
								);
							} else {
								// documento vacio
								$('#listContainer').append(
										'<div class="tabCont active" tab="1">' +
										'<div class="docError">' +
										'<div class="errorWrap">' +
										'<img src="../../img/pdfError.svg" alt="">' +
										'<div class="tituloError">Oops!</div>' +
										'<div class="mensajeError">' +
										'<div>Parece que el documento no se encuentra.</div>' +
										'<div style="margin-top: 20px">' + data[x].mensaje + '</div>' +
										'</div>' +
										'</div>' +
										'</div>' +
										'</div>'
								);
							}
						} else {
							$('#listLicDocs').append('<div class="docTab" tab="' + (x+1) + '"><div style="margin: 0 auto;text-align: left;width: 100%;">' + data[x].tipo + '</div></div>');
							if (data[x].fileBase64.length > 0) {
								$('#listContainer').append(
										'<div class="tabCont" tab="' + (x+1) + '">' +
										'<div class="tit">' + data[x].tipo + '</div>' +
										'<iframe src="data:application/pdf;base64,' + data[x].fileBase64 + '" frameborder="0" height="100%" width="100%"></iframe>' +
										'</div>'
								);
							} else {
								// documento vacio
								$('#listContainer').append(
										'<div class="tabCont" tab="' + (x+1) + '">' +
										'<div class="docError">' +
										'<div class="errorWrap">' +
										'<img src="../../img/pdfError.svg" alt="">' +
										'<div class="tituloError">Oops!</div>' +
										'<div class="mensajeError">' +
										'<div>Parece que el documento no se encuentra.</div>' +
										'<div style="margin-top: 20px">' + data[x].mensaje + '</div>' +
										'</div>' +
										'</div>' +
										'</div>' +
										'</div>'
								);
							}
						}
					}
				}
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
					'Algo ocurri\u00F3 al las licencias de funcionamiento.' +
					'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	}
}

function getIndicadoresEstTab() {
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getIndiByNeg.json',
		dataType: 'json',
		data: {
			negocio: generalNegVal
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}
			
			$('#generalEst01').html(data[0].tabletasCorrectas);
			$('#generalEst02').html(data[0].tabletasIncorrectas);
			$('#generalEst03').html(data[0].tabletasMantenimiento);
			$('#numTabMantGen').html(data[0].tabletasMantenimiento);

			var _total = data[0].tabletasCorrectas + data[0].tabletasIncorrectas;
			var _green = Math.round(data[0].tabletasCorrectas / _total * 100.0);
			var _red = Math.round(data[0].tabletasIncorrectas / _total * 100.0);
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informacion...');
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getIndiByNeg.json',
		dataType: 'json',
		data: {
			negocio: propiosNegVal
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			$.ajax({
				type: 'get',
				cache: false,
				url: contextPath + '/central/pedestalDigital/getCecoInfoGraph.json',
				dataType: 'json',
				data: {
					op: 0,
					negocio: propiosNegVal
				},
				success: function(data) {
					if (data.length < 1) {
						Swal.fire({
							html: '<div style="width: 100%;">' +
								'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
								'No hay datos que mostrar' +
								'</div>',
							showCloseButton: false,
							confirmButtonText: 'Aceptar',
							confirmButtonColor: '#006341'
						});
						return false;
					}

					$('#propiosEst01').html(data[0].n_suc_ok);
					$('#propiosEst02').html(data[0].n_suc_probl);
					$('#propiosEst03').html(data[0].n_suc_mante);

					$('#numTabMantGen2A').html(data[0].n_ceco_nivel_infe);
					$('#numTabMantGen2B').html(data[0].n_suc_total);
					$('#numTabMantGen2C').html(data[0].n_suc_mante);

					var _total = data[0].n_suc_total;
					var _green = Math.round(data[0].n_suc_ok / _total * 100.0);
					var _red = Math.round(data[0].n_suc_probl / _total * 100.0);

					var dom = document.getElementById("grafGen02");
					var grafGen02 = echarts.init(dom);
					var app = {};
					option = {
						tooltip: {
							trigger: 'item',
							formatter: '{c}%'
						},
						legend: {
							show: false,
						},
						series: [{
							type: 'pie',
							radius: ['50%', '70%'],
							avoidLabelOverlap: false,
							label: {
								normal: {
									show: false,
									position: 'center'
								},
								emphasis: {
									show: true,
									textStyle: {
										fontSize: '30',
										fontWeight: 'bold'
									}
								}
							},
							labelLine: {
								normal: {
									show: false
								}
							},
							data: [{
								value: _green,
								itemStyle: {
									color: verde1
								},
							}, {
								value: _red,
								itemStyle: {
									color: rojo
								},
							}]
						}]
					};
					if (option && typeof option === 'object') {
						grafGen02.setOption(option, true);
					}
					$('.tab').on('click', function() {
						grafGen02.resize();
					});
					$('.btnPropios').on('click', function () {
						grafGen02.resize();
					});
					$('.btnTerceros').on('click', function () {
						grafGen02.resize();
					});
					$('.btnRegresarZona').on('click', function() {
						grafGen02.resize();
					});
					window.onresize = function() {
						grafGen02.resize();
					};

				},
				error: function(obj, errorText) {
					console.log('No se pudo obtener la informacion...');
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No se pudo obtener la informaci\u00F3n' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				},
				complete: function() {
					hideLoadingSpinner();
				}
			});

			loadGraphicsInPropios();
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informacion...');
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getIndiByNeg.json',
		dataType: 'json',
		data: {
			negocio: tercerosNegVal
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			$.ajax({
				type: 'get',
				cache: false,
				url: contextPath + '/central/pedestalDigital/getCecoInfoGraph.json',
				dataType: 'json',
				data: {
					op: 0,
					negocio: tercerosNegVal
				},
				success: function(data) {
					if (data.length < 1) {
						Swal.fire({
							html: '<div style="width: 100%;">' +
								'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
								'No hay datos que mostrar' +
								'</div>',
							showCloseButton: false,
							confirmButtonText: 'Aceptar',
							confirmButtonColor: '#006341'
						});
						return false;
					}

					$('#tercerosEst01').html(data[0].n_suc_ok);
					$('#tercerosEst02').html(data[0].n_suc_probl);
					$('#tercerosEst03').html(data[0].n_suc_mante);

					$('#numTabMantGen3A').html(data[0].n_ceco_nivel_infe);
					$('#numTabMantGen3B').html(data[0].n_suc_total);
					$('#numTabMantGen3C').html(data[0].n_suc_mante);

					var _total = data[0].n_suc_total;
					var _green = Math.round(data[0].n_suc_ok / _total * 100.0);
					var _red = Math.round(data[0].n_suc_probl / _total * 100.0);

					var dom = document.getElementById("grafGen03");
					var grafGen03 = echarts.init(dom);
					var app = {};
					option = {
						tooltip: {
							trigger: 'item',
							formatter: '{c}%'
						},
						legend: {
							show: false,
						},
						series: [{
							type: 'pie',
							radius: ['50%', '70%'],
							avoidLabelOverlap: false,
							label: {
								normal: {
									show: false,
									position: 'center'
								},
								emphasis: {
									show: true,
									textStyle: {
										fontSize: '30',
										fontWeight: 'bold'
									}
								}
							},
							labelLine: {
								normal: {
									show: false
								}
							},
							data: [{
								value: _green,
								itemStyle: {
									color: verde1
								},
							}, {
								value: _red,
								itemStyle: {
									color: rojo
								},
							}]
						}]
					};
					if (option && typeof option === 'object') {
						grafGen03.setOption(option, true);
					}
					$('.tab').on('click', function() {
						grafGen03.resize();
					});
					$('.btnPropios').on('click', function () {
						grafGen03.resize();
					});
					$('.btnTerceros').on('click', function () {
						grafGen03.resize();
					});
					$('.btnRegresarZona').on('click', function() {
						grafGen03.resize();
					});
					window.onresize = function() {
						grafGen03.resize();
					};

				},
				error: function(obj, errorText) {
					console.log('No se pudo obtener la informacion...');
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'No se pudo obtener la informaci\u00F3n' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				},
				complete: function() {
					hideLoadingSpinner();
				}
			});

			loadGraphicsInTerceros();
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informacion...');
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function loadGraphicsInPropios() {
	console.log('loadGraphicsInPropios');
	showLoadingSpinner();

	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getCecosNegocio.json',
		dataType: 'json',
		data: {
			tipo: 0,
			negocio: propiosNegVal
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			// TODO: Construir graficas con cada iteracion
			$('#graficasPropiosContainer').html('');
			for (var x = 0; x < data.length; x++) {
				var aux = data[x];
				$.ajax({
					async: false,
					type: 'get',
					cache: false,
					url: contextPath + '/central/pedestalDigital/getCecoInfoGraph.json',
					dataType: 'json',
					data: {
						op: 1,
						ceco: aux.fcid_ceco
					},
					success: function(data) {
						if (data.length < 1) {
							Swal.fire({
								html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No hay datos que mostrar' + '</div>',
								showCloseButton: false,
								confirmButtonText: 'Aceptar',
								confirmButtonColor: '#006341'
							});
							return false;
						}

						$('#graficasPropiosContainer').append('<div class="contPie" id="propGraph' + x + '"></div>');
						$('#propGraph' + x).append('<div class="divTablaGraficas">' + '<table class="tblInfoGraficas">' + '<thead>' + '<tr>' + '<th>Total de Zonas</th>' + '<th>Total de Sucursales</th>' + '<th>En mantenimiento</th>' + '</tr>' + '</thead>' + '<tbody></tbody>' + '</table>' + '</div>');
						$('#propGraph' + x + ' .tblInfoGraficas tbody').append('<tr>' + '<td class="verdeClaro2 bold">' + data[0].n_ceco_nivel_infe + '</td>' + '<td class="verdeOscuro2 bold">' + data[0].n_suc_total + '</td>' + '<td class="gris3 bold">' + data[0].n_suc_mante + '</td>' + '</tr>');
						$('#propGraph' + x + ' .divTablaGraficas').append('<a class="btnVerDetalle btnCentro" onclick="loadNewCecoLevel(1, ' + aux.fcid_ceco + ',' + aux.fitipo_ceco + ',' + propiosNegVal + ');"><strong>' + aux.fcnombre + '</strong></a>');
						$('#propGraph' + x + ' .divTablaGraficas').append('<div id="propGraphContainer' + x + '" style="height: 250px; width: 100%;"></div>' + '<div class="clear"></div>');

						var _total = data[0].n_suc_ok + data[0].n_suc_probl;
						var _green = Math.round(data[0].n_suc_ok / _total * 100.0);
						var _red = Math.round(data[0].n_suc_probl / _total * 100.0);

						var dom = document.getElementById('propGraphContainer' + x);
						var graphX = echarts.init(dom);
						var app = {};
						option = {
							tooltip: {
								trigger: 'item',
								formatter: '{c}%'
							},
							legend: {
								show: false,
							},
							series: [{
								type: 'pie',
								radius: ['50%', '70%'],
								avoidLabelOverlap: false,
								label: {
									normal: {
										show: false,
										position: 'center'
									},
									emphasis: {
										show: true,
										textStyle: {
											fontSize: '30',
											fontWeight: 'bold'
										}
									}
								},
								labelLine: {
									normal: {
										show: false
									}
								},
								data: [{
									value: _green,
									itemStyle: {
										color: verde1
									},
								}, {
									value: _red,
									itemStyle: {
										color: rojo
									},
								}]
							}]
						};
						if (option && typeof option === 'object') {
							graphX.setOption(option, true);
						}
						$('.tab').on('click', function() {
							graphX.resize();
						});
						$('.btnPropios').on('click', function () {
							graphX.resize();
						});
						$('.btnTerceros').on('click', function () {
							graphX.resize();
						});
						$('.btnRegresarZona').on('click', function() {
							graphX.resize();
						});
						window.onresize = function() {
							graphX.resize();
						};
					},
					error: function(obj, errorText) {
						console.log('No se pudo obtener la informacion...');
						Swal.fire({
							html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No se pudo obtener la informaci\u00F3n' + '</div>',
							showCloseButton: false,
							confirmButtonText: 'Aceptar',
							confirmButtonColor: '#006341'
						});
						return false;
					},
					complete: function() {
						hideLoadingSpinner();
					}
				});
			}
			
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informacion...');
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function loadGraphicsInTerceros() {
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getCecosNegocio.json',
		dataType: 'json',
		data: {
			tipo: 0,
			negocio: tercerosNegVal
		},
		success: function(data) {
			if (data.length < 1) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay datos que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			// TODO: Construir graficas con cada iteracion
			$('#graficasTercerosContainer').html('');
			for (var x = 0; x < data.length; x++) {
				var aux = data[x];
				$.ajax({
					async: false,
					type: 'get',
					cache: false,
					url: contextPath + '/central/pedestalDigital/getCecoInfoGraph.json',
					dataType: 'json',
					data: {
						op: 1,
						ceco: aux.fcid_ceco
					},
					success: function(data) {
						if (data.length < 1) {
							Swal.fire({
								html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No hay datos que mostrar' + '</div>',
								showCloseButton: false,
								confirmButtonText: 'Aceptar',
								confirmButtonColor: '#006341'
							});
							return false;
						}

						$('#graficasTercerosContainer').append('<div class="contPie" id="tercGraph' + x + '"></div>');
						$('#tercGraph' + x).append('<div class="divTablaGraficas">' + '<table class="tblInfoGraficas">' + '<thead>' + '<tr>' + '<th>Total de Zonas</th>' + '<th>Total de Sucursales</th>' + '<th>En mantenimiento</th>' + '</tr>' + '</thead>' + '<tbody></tbody>' + '</table>' + '</div>');
						$('#tercGraph' + x + ' .tblInfoGraficas tbody').append('<tr>' + '<td class="verdeClaro2 bold">' + data[0].n_ceco_nivel_infe + '</td>' + '<td class="verdeOscuro2 bold">' + data[0].n_suc_total + '</td>' + '<td class="gris3 bold">' + data[0].n_suc_mante + '</td>' + '</tr>');
						$('#tercGraph' + x + ' .divTablaGraficas').append('<a class="btnVerDetalle btnCentro" onclick="loadNewCecoLevel(2, ' + aux.fcid_ceco + ',' + aux.fitipo_ceco + ',' + tercerosNegVal + ');"><strong>' + aux.fcnombre + '</strong></a>');
						$('#tercGraph' + x + ' .divTablaGraficas').append('<div id="tercGraphContainer' + x + '" style="height: 250px; width: 100%;"></div>' + '<div class="clear"></div>');

						var _total = data[0].n_suc_ok + data[0].n_suc_probl;
						var _green = Math.round(data[0].n_suc_ok / _total * 100.0);
						var _red = Math.round(data[0].n_suc_probl / _total * 100.0);

						var dom = document.getElementById('tercGraphContainer' + x);
						var graphX = echarts.init(dom);
						var app = {};
						option = {
							tooltip: {
								trigger: 'item',
								formatter: '{c}%'
							},
							legend: {
								show: false,
							},
							series: [{
								type: 'pie',
								radius: ['50%', '70%'],
								avoidLabelOverlap: false,
								label: {
									normal: {
										show: false,
										position: 'center'
									},
									emphasis: {
										show: true,
										textStyle: {
											fontSize: '30',
											fontWeight: 'bold'
										}
									}
								},
								labelLine: {
									normal: {
										show: false
									}
								},
								data: [{
									value: _green,
									itemStyle: {
										color: verde1
									},
								}, {
									value: _red,
									itemStyle: {
										color: rojo
									},
								}]
							}]
						};
						if (option && typeof option === 'object') {
							graphX.setOption(option, true);
						}
						$('.tab').on('click', function() {
							graphX.resize();
						});
						$('.btnPropios').on('click', function () {
							graphX.resize();
						});
						$('.btnTerceros').on('click', function () {
							graphX.resize();
						});
						$('.btnRegresarZona').on('click', function() {
							graphX.resize();
						});
						window.onresize = function() {
							graphX.resize();
						};
					},
					error: function(obj, errorText) {
						console.log('No se pudo obtener la informacion...');
						Swal.fire({
							html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No se pudo obtener la informaci\u00F3n' + '</div>',
							showCloseButton: false,
							confirmButtonText: 'Aceptar',
							confirmButtonColor: '#006341'
						});
						return false;
					},
					complete: function() {
						hideLoadingSpinner();
					}
				});
			}
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informacion...');
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function loadNewCecoLevel(type, fcid_ceco, fitipo_ceco, negocio) {
	console.log('loadNewCecoLevel');
	showLoadingSpinner();

	var _type = 0;
	if (type == 1) {
		_type = 1;
	} else if (type == 2) {
		_type = 2;
	}

	$('#graphContainer' + _type).hide();
	$('#zoneContainer' + _type).show();
	$('#btnContainer' + _type).show();

	// llenar data en zoneContainer1:
	$('#zoneContainer' + _type).html('');
	$('#zoneContainer' + _type).append('<table id="tableContainer' + _type + '" class="tblGeneral tblEstatusZona display scroll"></table>');
	$('#tableContainer' + _type).append(
			'<thead>' +
			'<tr><th>&nbsp;</th>' +
			'<th>Zonas</th><th>Regiones</th><th>Total de sucursales</th>' +
			'<th>Funcionando correctamente</th><th>Presentando Problemas</th>' +
			'<th>En mantenimiento</th></tr>' +
			'</thead><tbody></tbody>');

	var _tipo = fitipo_ceco, _ceco = fcid_ceco, _negocio = negocio;
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getCecosNegocio.json',
		dataType: 'json',
		data: {
			tipo: _tipo,
			ceco: _ceco,
			negocio: _negocio
		},
		success: function(data) {
			if (data.length > 0) {
				for (var x = 0; x < data.length; x++) {
					var sucursal = data[x];
					$.ajax({
						async: false,
						type: 'get',
						cache: false,
						url: contextPath + '/central/pedestalDigital/getCecoInfoGraph.json',
						dataType: 'json',
						data: {
							op: 1,
							ceco: sucursal.fcid_ceco
						},
						success: function(data) {
							if (data.length < 1) {
								Swal.fire({
									html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No hay datos que mostrar' + '</div>',
									showCloseButton: false,
									confirmButtonText: 'Aceptar',
									confirmButtonColor: '#006341'
								});
								return false;
							}

							$('#tableContainer' + _type + ' tbody').append(
									'<tr>' +
									'<td></td>' +
									'<td>' + sucursal.fcnombre + '</td>' +
									'<td>' + data[0].n_ceco_nivel_infe + '</td>' +
									'<td>' + data[0].n_suc_total + '</td>' +
									'<td class="cantidadVerde">' + data[0].n_suc_ok + '</td>' +
									'<td class="cantidadRojo">' + data[0].n_suc_probl + '</td>' +
									'<td class="cantidadVerdeClaro">' + data[0].n_suc_mante + '</td>' +
									'<input id="fcid_ceco_' + x + '" type="hidden" value="' + sucursal.fcid_ceco + '">' + 
									'<input id="fiid_tipo_' + x + '" type="hidden" value="' + (parseInt(_tipo) + 1) + '">' + 
									'<input id="negocio_' + x + '" type="hidden" value="' + _negocio + '">' + 
									'</tr>'
									);
						},
						error: function(obj, errorText) {
							Swal.fire({
								html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No se pudo obtener la informaci\u00F3n' + '</div>',
								showCloseButton: false,
								confirmButtonText: 'Aceptar',
								confirmButtonColor: '#006341'
							});
							return false;
						},
						complete: function() {
							hideLoadingSpinner();
						}
					});
				}

				var dataSrc = [];
				var table = $('#tableContainer' + _type).DataTable({
					'initComplete': function () {
						var api = this.api();
						// Populate a dataset for autocomplete functionality
						// using data from first, second and third columns
						api.cells('tr', [0, 1, 2]).every(function () {
							// Get cell data as plain text
							var data = $('<div>').html(this.data()).text();
							if (dataSrc.indexOf(data) === -1) {
								dataSrc.push(data);
							}
						});
						// Sort dataset alphabetically
						dataSrc.sort();
						// Initialize Typeahead plug-in
						$('.dataTables_filter input[type="search"]', api.table().container()).typeahead({
							source: dataSrc,
							afterSelect: function (value) {
								api.search(value).draw();
							}
						});
					},
					language: {
						paginate: {
							first: 'Premier',
							previous: '<img src="../../img/iconos/firstIco.svg" class="flechas1" />',
							next: '<img src="../../img/iconos/lastIco.svg" class="flechas2" />',
							last: 'Dernier',
						},
						search: 'Buscar:',
					},
					responsive: true,
					// 'ajax': 'https://api.myjson.com/bins/16lp6',
					'columns': [{
						'className': 'details-control',
						'orderable': false,
						'data': null,
						'defaultContent': ''
					}, {
						'data': 'Zonas'
					}, {
						'data': 'Regiones'
					}, {
						'data': 'Total de sucursales'
					}, {
						'data': 'Funcionando correctamente'
					}, {
						'data': 'Presentando Problemas'
					}, {
						'data': 'En mantenimiento'
					}, ],
					'order': [
						[1, 'asc']
					]
				});
				// Add event listener for opening and closing details
				$('#tableContainer' + _type + ' tbody').on('click', 'td.details-control', function () {
					var tr = $(this).closest('tr');
					var row = table.row(tr);

					if (row.child.isShown()) {
						// This row is already open - close it
						row.child.hide();
						tr.removeClass('shown');
					} else {
						var rowParent = $(this).parent();
						var inputList = $(rowParent).find('input');
						var a = inputList[0].value, b = inputList[1].value, c = '' + inputList[2].value;

						// Open this row
						loadRegion(row, tr, row.data(), a, b, c);
						// row.child(loadRegion(row, row.data(), a, b, c)).show();
						// tr.addClass('shown');
					}
				});
				// Handle click on "Expand All" button
				$('#btn-show-all-children').on('click', function () {
					// Enumerate all rows
					table.rows().every(function () {
						// If row has details collapsed
						if (!this.child.isShown()) {
							// Open this row
							var rowParent = $(this).parent();
							var inputList = $(rowParent).find('input');
							var a = inputList[0].value, b = inputList[1].value, c = '' + inputList[2].value;

							loadRegion(this, $(this.node()), this.data(), a, b, c);
							// this.child(loadRegion(this, this.data(), a, b, c)).show();
							// $(this.node()).addClass('shown');
						}
					});
				});
				// Handle click on "Collapse All" button
				$('#btn-hide-all-children').on('click', function () {
					// Enumerate all rows
					table.rows().every(function () {
						// If row has details expanded
						if (this.child.isShown()) {
							// Collapse row details
							this.child.hide();
							$(this.node()).removeClass('shown');
						}
					});
				});

				$('#btnContainer' + _type + ' .btnRegresarZona').off();
				$('#btnContainer' + _type + ' .btnRegresarZona').on('click', function () {
					console.log('.btnRegresar1-' + _type);
					$('#graphContainer' + _type).show();
					$('#zoneContainer' + _type).hide();
					$('#btnContainer' + _type).hide();
				});

				$('#btnContainer' + _type + ' .expStatus3').off();
				$('#btnContainer' + _type + ' .expStatus3').on('click', function () {
					exportDataCecoExcel('2', _ceco);
				});
			}
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informacion...');
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function loadRegion(object, object2, data, a, b, c) {
	console.log('loadRegion');

	// TODO: return string with the necesary html
		// in order to keep building the table...

	showLoadingSpinner();
	var _row = object;
	var _tr = object2;
	var _ceco = a, _tipo = b, _negocio = c;
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getCecosNegocio.json',
		dataType: 'json',
		data: {
			tipo: _tipo,
			ceco: _ceco,
			negocio: _negocio
		},
		success: function(data) {
			if (data.length > 0) {
				var _str = '<div id="zone_' + _ceco + '" class="divCol4 divCol4M tLeft">';
				for (var x = 0; x < data.length; x++) {
					var sucursal = data[x];
					$.ajax({
						async: false,
						type: 'get',
						cache: false,
						url: contextPath + '/central/pedestalDigital/getCecoInfoGraph.json',
						dataType: 'json',
						data: {
							op: 1,
							ceco: sucursal.fcid_ceco
						},
						success: function(data) {
							if (data.length > 0) {
								_str +=
									'<div class="col4">' +
									'<a href="#" class="linkN linkRegion" onclick="loadSucursalesTable(' + (parseInt(_tipo) + 1) + ',' + sucursal.fcid_ceco + ',\'' + _negocio + '\');">' + sucursal.fcnombre + '</a>' +
									'<div class="estadisticaTabletas">' +
									'<div><div class="verde"></div>' + data[0].n_suc_ok + '</div>' +
									'<div><div class="rojo"></div>' + data[0].n_suc_probl + '</div>' +
									'<div><div class="verdeClaro"></div>' + data[0].n_suc_mante + '</div>' +
									'</div>' +
									'<div class="divTotalS">Total de Sucursales: <span class="demiBold">' + data[0].n_suc_total + '</span></div>' +
									'</div>';
							}
						},
						error: function(obj, errorText) {
							console.log('No se pudo obtener la informacion...');
							Swal.fire({
								html: '<div style="width: 100%;">' + '<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' + 'No se pudo obtener la informaci\u00F3n' + '</div>',
								showCloseButton: false,
								confirmButtonText: 'Aceptar',
								confirmButtonColor: '#006341'
							});
							return false;
						},
						complete: function() {
							hideLoadingSpinner();
						}
					});
				}

				_str += '</div>';
				_row.child(_str).show();
				_tr.addClass('shown');
			} else {
				_row.child('<br><br>NO HAY INFORMACI&Oacute;N QUE MOSTRAR<br><br>').show();
				_tr.addClass('shown');
			}
		},
		error: function(obj, errorText) {
			console.log('No se pudo obtener la informacion...');
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function loadSucursalesTable(tipo, ceco, negocio) {
	console.log('loadSucursalesTable');
	showLoadingSpinner();

	var _ceco = ceco;
	var tabs = $('.contenedorTabs').find('.active');
	var _id = 0;
	if (tabs.hasClass('tabPropios')) {
		_id = 1;
	} else if (tabs.hasClass('tabTerceros')) {
		_id = 2;
	}

	$('#sucursalContainer' + _id).html('');
	$.ajax({
		type: 'get',
		cache: false,
		url: contextPath + '/central/pedestalDigital/getSucEstTabByCeco.json',
		dataType: 'json',
		data: {
			op: 2,
			ceco: _ceco
		},
		success: function(data) {
			if (data.length > 0) {
				var _str = '<table id="region_' + _ceco + '" class="tblGeneral display scroll" style="overflow-x: scroll; max-height: 300px;">';
				_str +=
					'<thead><tr>' +
					'<th>Sucursal</th>' +
					'<th>N&uacute;mero econ&oacute;mico</th>' +
					'<th>Fecha de instalaci&oacute;n</th>' +
					'<th>&Uacute;ltima actualizaci&oacute;n</th>' +
					'</tr></thead><tbody>';

				for (var x = 0; x < data.length; x++) {
					if (!data[x].fiid_estatus == 0) {
						_str += '<tr>';

						if (data[x].fiid_estatus == 1) {
							_str += '<td class="bordeVerde">' + data[x].nom_ceco + '</td>';
						} else if (data[x].fiid_estatus == 2) {
							_str += '<td class="bordeRojo">' + data[x].nom_ceco + '</td>';
						} else if (data[x].fiid_estatus == 3) {
							_str += '<td class="bordeVerde2">' + data[x].nom_ceco + '</td>';
						}

						_str += '<td><a href="loadTabletInfo.htm?idTableta=' + data[x].fiid_tableta + '" class="liga">' + data[x].sucursal + '</a></td>';
						_str += '<td>' + data[x].fdfecha_instala + '</td>';
						_str += '<td>' + data[x].ultima_actualizacion + '</td>';
						_str += '</tr>';
					}
				}

				_str += '</tbody></table>';
				$('#sucursalContainer' + _id).append(_str);

				$('#btnContainer' + _id + ' .btnRegresarZona').off();
				$('#btnContainer' + _id + ' .btnRegresarZona').on('click', function () {
					$('#zoneContainer' + _id).show();
					$('#btnContainer' + _id).show();
					$('#graphContainer' + _id).hide();
					$('#sucursalContainer' + _id).hide();

					$('#btnContainer' + _id + ' .btnRegresarZona').off();
					$('#btnContainer' + _id + ' .btnRegresarZona').on('click', function () {
						$('#graphContainer' + _id).show();
						$('#zoneContainer' + _id).hide();
						$('#btnContainer' + _id).hide();
					});
				});

				$('#btnContainer' + _id + ' .expStatus3').off();
				$('#btnContainer' + _id + ' .expStatus3').on('click', function () {
					exportDataCecoExcel('2', _ceco);
				});

				/**
				var dataSrc = [];
				$('#region_' + _ceco).DataTable({
					'initComplete': function() {
						var api = this.api();
						// Populate a dataset for autocomplete functionality
						// using data from first, second and third columns
						api.cells('tr', [0, 1, 2]).every(function() {
							// Get cell data as plain text
							var data = $('<div>').html(this.data()).text();
							if (dataSrc.indexOf(data) === -1) {
								dataSrc.push(data);
							}
						});
						// Sort dataset alphabetically
						dataSrc.sort();
						// Initialize Typeahead plug-in
						$('.dataTables_filter input[type="search"]', api.table().container()).typeahead({
							source: dataSrc,
							afterSelect: function(value) {
								api.search(value).draw();
							}
						});
					},
					responsive: true,
					language: {
						paginate: {
							first: 'Premier',
							previous: '<img src="../../img/iconos/firstIco.svg" class="flechas1" />',
							next: '<img src="../../img/iconos/lastIco.svg" class="flechas2" />',
							last: 'Dernier',
						},
						search: 'Buscar:',
					}
				});
				*/

			} else {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No hay sucursales que mostrar' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			}

			$('#graphContainer' + _id).hide();
			$('#zoneContainer' + _id).hide();
			$('#sucursalContainer' + _id).show();
		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'No se pudo obtener la informaci\u00F3n' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function loadSucInfoDetail(obj) {
	console.log('loadSucInfoDetail');
	showLoadingSpinner();

	if (obj.innerText != undefined || obj.innerText != '') {
		window.location = 'loadTabletInfo.htm?idTableta=' + obj.innerText;
		return false;
	}
}

function loadSucInfoDetailBySearch(suc) {
	console.log('loadSucInfoDetailBySearch');
	showLoadingSpinner();

	var _suc = suc;
	if (_suc != undefined || _suc != '') {

		var _url = 
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/getTabletBySucursal.json',
			dataType: 'json',
			data: {
				numSucursal: _suc
			},
			success: function(data) {
				if (data.length > 0) {
					window.location = 'loadTabletInfo.htm?idTableta=' + data[0].idTableta;
					return false;
				}
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	}
}

function exportDataCecoExcel(op, ceco) {
	console.log('exportDataCecoExcel');

	var _op = op;
	var _ceco = ceco;
	if (_ceco != undefined && _ceco != null) {

		$('#countdown').html(' ... ');
		$('#tamanoArchivo').html(' calculando tama\u00F1o del archivo ... ');
		$('.toastDescarga').addClass('active');

		// Se asigna evento al boton de cerrar
		$('.toastDescarga').find('.cerrar').on('click', function () {
			// clearInterval(countdownTimer);
			$(this).closest('.toastDescarga').removeClass('active');
		});

		var _url = contextPath + '/central/pedestalDigital/exportDataCecoExcel.json';
		$.ajax({
			progress: function(event) {
				var _lengthComputable = event.lengthComputable;
				var _total = event.total;
				var _loaded = event.loaded;

				console.log('_lengthComputable: ' + _lengthComputable);
				console.log('_total: ' + _total);
				console.log('_loaded: ' + _loaded);

				if (_lengthComputable) {
					$('#countdown').html('estatus_tabletas.xls'); // nombre del archivo
					if (_total < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _total + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total >= 1000 && _total < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_total / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					} else if (_total > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_total / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _total * 100)));
					}
				} else if (_total === 0 && _loaded > 0) {
					$('#countdown').html('estatus_tabletas.xls'); // nombre del archivo
					if (_loaded < 1000) {
						$('#tamanoArchivo').html(_loaded + '/' + _loaded + ' bytes');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded >= 1000 && _loaded < 1000000) {	
						$('#tamanoArchivo').html((_loaded / 1000).toFixed(2) + '/' + (_loaded / 1000).toFixed(2) + ' KB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					} else if (_loaded > 1000000) {
						$('#tamanoArchivo').html((_loaded / 1000 / 1000).toFixed(3) + '/' + (_loaded / 1000 / 1000).toFixed(3) + ' MB');
						$('.circle-gauge').find('a').attr('style', '--gauge-value:' + (Math.floor(_loaded / _loaded * 100)));
					}
				}
			},
			cache: false,
			responseType: 'arraybuffer',
			type: 'post',
			url: _url,
			data: {
				op: _op,
				ceco: _ceco
			},
			success: function(data) {
				var blob = new Blob([data], {
					type: 'text/html'
				});
				saveAs(blob, 'estatus_tabletas.xls');
			},
			error: function(obj, errorText) {
				$('.toastDescarga').find('.cerrar').click(); // Se cierra toast

				hideLoadingSpinner();
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>\u00A1Atenci\u00F3n!</strong></div>' +
						'error' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});

	} else {
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Debe realizar alguna consulta para poder exportar el historial' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
	}
}

function loadSucursalesBySearch(tipo, ceco, negocio) {
	console.log('loadSucursalesTable');
	showLoadingSpinner();

	var _ceco = ceco;
	var tabs = $('.contenedorTabs').find('.active');
	var _id = 0;
	if (tabs.hasClass('tabPropios')) {
		_id = 1;
	} else if (tabs.hasClass('tabTerceros')) {
		_id = 2;
	}

	var _negocio = negocio;
	if (_ceco != undefined || _ceco != '') {

		var _url = 
		$.ajax({
			type: 'get',
			cache: false,
			url: contextPath + '/central/pedestalDigital/obtieneCecosPorCSV.json',
			dataType: 'json',
			data: {
				csvList: _ceco,
				negocio: _negocio
			},
			success: function(data) {
				if (data.length > 0) {
					var _ceco = data[0].idCeco;
					$('#sucursalContainer' + _id).html('');
					$.ajax({
						type: 'get',
						cache: false,
						url: contextPath + '/central/pedestalDigital/getSucEstTabByCeco.json',
						dataType: 'json',
						data: {
							op: 2,
							ceco: _ceco
						},
						success: function(data) {
							if (data.length > 0) {
								var _str = '<table id="region_' + _ceco + '" class="tblGeneral display scroll" style="overflow-x: scroll; max-height: 300px;">';
								_str +=
									'<thead><tr>' +
									'<th>Sucursal</th>' +
									'<th>N&uacute;mero econ&oacute;mico</th>' +
									'<th>Fecha de instalaci&oacute;n</th>' +
									'<th>&Uacute;ltima actualizaci&oacute;n</th>' +
									'</tr></thead><tbody>';

								for (var x = 0; x < data.length; x++) {
									if (!data[x].fiid_estatus == 0) {
										_str += '<tr>';

										if (data[x].fiid_estatus == 1) {
											_str += '<td class="bordeVerde">' + data[x].nom_ceco + '</td>';
										} else if (data[x].fiid_estatus == 2) {
											_str += '<td class="bordeRojo">' + data[x].nom_ceco + '</td>';
										} else if (data[x].fiid_estatus == 3) {
											_str += '<td class="bordeVerde2">' + data[x].nom_ceco + '</td>';
										}

										_str += '<td><a href="loadTabletInfo.htm?idTableta=' + data[x].fiid_tableta + '" class="liga">' + data[x].sucursal + '</a></td>';
										_str += '<td>' + data[x].fdfecha_instala + '</td>';
										_str += '<td>' + data[x].ultima_actualizacion + '</td>';
										_str += '</tr>';
									}
								}

								_str += '</tbody></table>';
								$('#sucursalContainer' + _id).append(_str);

								if (_id == 0) {
									$('#expStatusA').off();
									$('#expStatusA').on('click', function () {
										$('#contenedorGrafica0').show();
										$('#sucursalContainer0').hide();
										$('#btnContainer0').hide();
									});
								} else {
									$('#btnContainer' + _id + ' .btnRegresarZona').off();
									$('#btnContainer' + _id + ' .btnRegresarZona').on('click', function () {
										if ($('#sucursalContainer' + _id).is(':visible')) {
											$('#graphContainer' + _id).show();
											$('#zoneContainer' + _id).hide();
											$('#sucursalContainer' + _id).hide();
											$('#btnContainer' + _id).hide();
										} else {
											$('#zoneContainer' + _id).show();
											$('#btnContainer' + _id).show();
											$('#graphContainer' + _id).hide();
											$('#sucursalContainer' + _id).hide();
										}

										/**
										$('#btnContainer' + _id + ' .expStatus3').off();
										$('#btnContainer' + _id + ' .expStatus3').on('click', function () {
											exportDataCecoExcel('2', _ceco);
										});
										*/
									});

									$('#btnContainer' + _id + ' .expStatus3').off();
									$('#btnContainer' + _id + ' .expStatus3').on('click', function () {
										exportDataCecoExcel('2', _ceco);
									});
								}
							} else {
								if (_id == 0) {
									$('#contenedorGrafica0').show();
									$('#sucursalContainer0').hide();
									$('#btnContainer0 .expStatus00').off();
								} else if (_id == 1) {
									$('#zoneContainer1').show();
									$('#sucursalContainer1').hide();
								} else if (_id == 2) {
									$('#zoneContainer2').show();
									$('#sucursalContainer2').hide();
								}

								Swal.fire({
									html: '<div style="width: 100%;">' +
										'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
										'No hay sucursales que mostrar' +
										'</div>',
									showCloseButton: false,
									confirmButtonText: 'Aceptar',
									confirmButtonColor: '#006341'
								});
								return false;
							}

							$('#graphContainer' + _id).hide();
							$('#zoneContainer' + _id).hide();
							$('#sucursalContainer' + _id).show();
						},
						error: function(obj, errorText) {
							Swal.fire({
								html: '<div style="width: 100%;">' +
									'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
									'No se pudo obtener la informaci\u00F3n' +
									'</div>',
								showCloseButton: false,
								confirmButtonText: 'Aceptar',
								confirmButtonColor: '#006341'
							});
							return false;
						},
						complete: function() {
							hideLoadingSpinner();
						}
					});

					return false;
				}
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo obtener la informaci\u00F3n' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	}
}