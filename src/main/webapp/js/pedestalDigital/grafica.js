// Vertical 
window.onload = function() {
	var ctx2 = document.getElementById("canvas-vertical2").getContext("2d");
	window.myBar = new Chart(ctx2, {
		type: 'bar',
		data: barChartData1,
		options: {
			spanGaps: false,
			legend: {
				display: true
			}, // quita cuadros
			responsive: true,
			legend: {
				position: 'right',
			},
			title: {
				display: true,
				text: 'Grafica 4'
			},
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			}
		}
	});

	//Grafica Lineal
	var chart = new Chart('canvas-lineal', {
		type: 'line',
		data: data,
		options: options
	});

	// Donas
	var circulo1 = document.getElementById("chart-dona").getContext("2d");
	window.myDoughnut = new Chart(circulo1, config0);

	var circulo2 = document.getElementById("chart-dona2").getContext("2d");
	window.myDoughnut = new Chart(circulo2, config1);
};


// Horizontal
/**
var horizontal = document.getElementById("canvas-horizonatal").getContext("2d");
window.myHorizontalBar = new Chart(horizontal, {
	type: 'horizontalBar',
	data: horizontalBarChartData,
	options: {
		responsive: true,
		legend: {
			display: true
		},
		title: {
			display: true,
			text: 'Gráfica 6'
		}

	}
});
*/

//Pie
/**
var ctx = document.getElementById("canvas-pie").getContext("2d");
window.myPie = new Chart(ctx, configPie);
*/

//Donas 1
/**
var config0 = {
	type: 'doughnut',
	data: {
		datasets: [{
			data: [30, 40, 30],
			backgroundColor: [
				window.chartColors.verde,
				window.chartColors.verde1,
				window.chartColors.verde2
			],
		}],
		labels: ["Valor 1", "Valor 2", "Valor 3"]
	},
	options: {
		spanGaps: false,
		legend: {
			display: true // quita cuadrilla
		},
		responsive: true,
		legend: {
			position: 'right',
		},
		title: {
			display: true,
			text: 'Gráfica 1'
		},
		animation: {
			animateScale: true,
			animateRotate: true
		}
	}
};
*/

// Donas 2
/**
var config1 = {
	type: 'doughnut',
	data: {
		datasets: [{
			data: [40, 25, 35],
			backgroundColor: [
				window.chartColors.verde2,
				window.chartColors.verde,
				window.chartColors.verde1
			],
		}],
		labels: ["Red", "Green", "Blue"]
	},
	options: {
		spanGaps: false,
		legend: {
			display: true // quita cuadrilla
		},
		responsive: true,
		legend: {
			position: 'top',
		},
		title: {
			display: true,
			text: 'Gráfica 2'
		},
		animation: {
			animateScale: true,
			animateRotate: true
		}
	}
};
*/

// Vertical 2
/**
var barChartData1 = {
	labels: ["", "", ""],
	datasets: [{
		label: 'Dataset 1',
		backgroundColor: color(window.chartColors.verde1).rgbString(),
		borderColor: window.chartColors.verde1,
		borderWidth: 1,
		data: [10]
	}, {
		label: 'Dataset 2',
		backgroundColor: color(window.chartColors.verde).rgbString(),
		borderColor: window.chartColors.verde,
		borderWidth: 1,
		data: [13]
	}, {
		label: 'Dataset 3',
		backgroundColor: color(window.chartColors.verde2).rgbString(),
		borderColor: window.chartColors.verde2,
		borderWidth: 1,
		data: [8]
	}]
};
*/

// Grafica Lineal
/**
var presets = window.chartColors;
var utils = Samples.utils;
var inputs = {};

utils.srand(42);

var data = {
	labels: [2015, 2016, 2017, 2018, 2019, 2020],
	datasets: [{
		backgroundColor: utils.transparentize(presets.verde1),
		borderColor: presets.verde1,
		data: [2, 10, 2, 8, 5, 10],
		label: 'Dato1',
		fill: ''
	}, {
		backgroundColor: utils.transparentize(presets.verde2),
		borderColor: presets.verde2,
		data: [4, 10, 4, 8, 5, 10],
		label: 'Dato2',
		fill: ''
	}, {
		backgroundColor: utils.transparentize(presets.verde),
		borderColor: presets.verde,
		data: [8, 10, 8, 8, 5, 10],
		label: 'Dato3',
		fill: ''
	}]
};

var options = {
	spanGaps: false,
	legend: {
		display: true // quita cuadrilla
	},
	legend: {
		position: 'bottom',
	},
	elements: {
		line: {
			tension: 0.000001
		}
	},
	scales: {
		yAxes: [{
			stacked: true
		}]
	},
	title: {
		display: true,
		text: 'Gráfica 5'
	}
};
*/

// horizontal
/**
var color = Chart.helpers.color;
var horizontalBarChartData = {
	labels: ["Type A", "Type C", "Type D", "Type C", "Type D"],
	datasets: [{
		label: 'Dataset 1',
		backgroundColor: color(window.chartColors.verde1).rgbString(),
		borderColor: window.chartColors.verde1,
		borderWidth: 1,
		data: [10, 11, 12, 13, 14, 15, 16]
	}, {
		label: 'Dataset 2',
		backgroundColor: color(window.chartColors.verde2).rgbString(),
		borderColor: window.chartColors.verde2,
		data: [3, 11, 12, 13, 14, 15, 16]
	}, {
		label: 'Dataset 3',
		backgroundColor: color(window.chartColors.verde).rgbString(),
		borderColor: window.chartColors.verde,
		data: [3, 5, 4, 3, 14, 5, 10]
	}]

};
*/

// Pie
/**
var configPie = {
	type: 'pie',
	data: {
		datasets: [{
			data: [
				15,
				10,
				5,
				30,
				40,
			],
			backgroundColor: [
				window.chartColors.verde,
				window.chartColors.verde1,
				window.chartColors.verde2,
				window.chartColors.verde3,
				window.chartColors.verde4,
			],
			label: 'Dataset 1'
		}],
		labels: [
			"Red",
			"Orange",
			"Yellow",
			"Green",
			"Blue"
		]
	},
	options: {
		responsive: true,
		pieceLabel: [{
				mode: 'percentage',
				precision: 2,
				arc: true,
				fontColor: '#000',
				position: 'default'
			},
			{
				mode: 'label',
				fontColor: '#000',
				arc: true,
				position: 'outside'
			}
		],
		title: {
			display: true,
			text: 'Grafica 7'
		} //percentage
	}
};
*/