function betweenInsertAndUpdate(obj) {
	var name = $(obj).attr('id');
	if (name === 'radio3') {
		// TODO: Insertar
		$('#btnCecos').val('Insertar');
		$('#btnCecos').prop("onclick", null).off("click");
		$('#btnCecos').on('click', function() {
			insertCecosInfo();
		});
	} else {
		// TODO: Actualizar
		$('#btnCecos').val('Actualizar');
		$('#btnCecos').prop("onclick", null).off("click");
		$('#btnCecos').on('click', function() {
			updateCecosInfo();
		});
	}
}

function insertCecosInfo() {
	console.log('insertCecosInfo');
}

function updateCecosInfo() {
	console.log('updateCecosInfo');
}

function sendCecosData() {
	showLoadingSpinner();
	var _data = $('#data').val();

	if (_data.length < 1) {
		hideLoadingSpinner();
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'Algo ocurri\u00F3 al obtener la informaci\u00F3n' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}

	$.ajax({
		type: 'POST',
		cache: false,
		url: contextPath + '/central/pedestalDigital/insertCecoData.json',
		dataType: 'json',
		data: {
			data: _data
		},
		success: function(data) {
			console.log(data);

			/**
			if (data.includes === 'si') {
				var str = data.split('-.-.-');
				$('#grisContainer').append('<div class="titSec"><br>Log de cecos</div><br><div id="cecosError" style="max-height: 300px; overflow-y: scroll;"></div>');
				$('#cecosError').html('CECOs no actualizados: ' + str[1]);

				var _cecosError = data.cecosError;
				_cecosError = JSON.stringify(_cecosError);
				_cecosError = _cecosError.replace('"', '');
				_cecosError = _cecosError.replace('"', '');

				if (_cecosError.lenght > 0) {
					var _cecosErrorList = _cecosError.split(',');
					for (var x = 0; x < _cecosErrorList.length; x++) {
						$('#cecosError').append('- ' + _cecosErrorList[x] + '<br>');
					}
				}
			} else if (data.includes === 'no') {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'Algo ocurri\u00F3 y no se pudo ejecutar el proceso de carga de CECOs' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;

				/**
				$('#grisContainer').append('<div class="titSec"><br>Log de cecos</div><br><div id="cecosError" style="max-height: 300px; overflow-y: scroll;"></div>');
				$('#grisContainer').append('<div class="titSec"><br>Log de geografia</div><br><div id="geoError" style="max-height: 300px; overflow-y: scroll;"></div>');

				var _cecosError = data.cecosError;
				_cecosError = JSON.stringify(_cecosError);
				_cecosError = _cecosError.replace('"', '');
				_cecosError = _cecosError.replace('"', '');

				if (_cecosError.length > 0) {
					var _cecosErrorList = _cecosError.split(',');
					for (var x = 0; x < _cecosErrorList.length; x++) {
						$('#cecosError').append('- ' + _cecosErrorList[x] + '<br>');
					}
				}

				var _geoError = data.geoError;
				_geoError = JSON.stringify(_geoError);
				_geoError = _geoError.replace('"', '');
				_geoError = _geoError.replace('"', '');

				if (_geoError.length > 0) {
					var _geoError = _geoError.split(',');
					for (var x = 0; x < _geoError.length; x++) {
						$('#geoError').append('- ' + _geoError[x] + '<br>');
					}
				}
			}
			*/

		},
		error: function(obj, errorText) {
			Swal.fire({
				html: '<div style="width: 100%;">' +
					'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
					'Algo ocurri\u00F3 y no se pudo ejecutar el proceso de carga de CECOs' +
					'</div>',
				showCloseButton: false,
				confirmButtonText: 'Aceptar',
				confirmButtonColor: '#006341'
			});
			return false;
		},
		complete: function() {
			hideLoadingSpinner();
		}
	});
}

function readExcelFile() {
	var fileUpload = document.getElementById('cargaGeoExcel'); // Reference the FileUpload element.
	var regexXLSX = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx)$/; // Validate whether File is valid Excel file.
	var regexXLS = /^([a-zA-Z0-9\s_\\.\-:])+(.xls)$/; // Validate whether File is valid Excel file.

	try {
		if (regexXLSX.test(fileUpload.value.toLowerCase()) || regexXLS.test(fileUpload.value.toLowerCase())) {
			if (typeof(FileReader) != "undefined") {
				var reader = new FileReader();

				// For Browsers other than IE.
				if (reader.readAsBinaryString) {
					reader.onload = function(e) {
						processFile(e.target.result);
					};
					reader.readAsBinaryString(fileUpload.files[0]);
				} else {
					//For IE Browser.
					reader.onload = function(e) {
						var data = "";
						var bytes = new Uint8Array(e.target.result);
						for (var i = 0; i < bytes.byteLength; i++) {
							data += String.fromCharCode(bytes[i]);
						}

						processFile(e.target.result);
					};
					reader.readAsArrayBuffer(fileUpload.files[0]);
				}
			} else {
				alert('Este navegador no soporta esta funci\u00F3n.');
				hideLoadingSpinner();
			}
		} else {
			alert('Favor de subir un archivo en formato Excel.');
			hideLoadingSpinner();
		}
	} catch (e) {
		// Se oculta la animación de carga
		hideLoadingSpinner();

		// En caso de que ocurra un error al leer el archivo de Excel se imprime el error
		console.log(e);

		// Se muestra alerta al usuario
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se pudo leer el archivo Excel.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}
}

function processFile(data) {
	//Read the Excel File data.
	var workbook = XLSX.read(data, {
		type: 'binary'
	});

	try {
		var sheet = workbook.SheetNames[0]; // Fetch the name a Sheet
		var excelRows = XLSX.utils.make_json(workbook.Sheets[sheet]); // Read all rows from First Sheet into an JSON array.
		var _data = JSON.stringify(excelRows);

		// var csvString = excelRows.substring(0, excelRows.length - 1);

		$.ajax({
			type: 'post',
			cache: false,
			url: contextPath + '/central/pedestalDigital/soporte/readExcelForGeoData.json',
			dataType: 'json',
			data: {
				geoData: _data
			},
			success: function(data) {
				if (data.msj != 0) {
					Swal.fire({
						html: '<div style="width: 100%;">' +
							'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
							'Algo ocurri\u00F3 durante el proceso de actualizaci\u00F3n de datos de geograf\u00EDa' +
							'</div>',
						showCloseButton: false,
						confirmButtonText: 'Aceptar',
						confirmButtonColor: '#006341'
					});
					return false;
				}

				$('#geoContainer').html('');
				$('#geoContainer').html('<div class="titSec">Log de actualización de datos de geografía</div><div id="geoSubContainer" style="max-height: 300px; overflow-y: scroll;"></div>');
				$('#geoSubContainer').append('<p>Número de registros insertados: ' + data.nGeoInserts + '</p>');
				$('#geoSubContainer').append('<p>Número de registros actualizados: ' + data.nGeoUpdates + '</p>');
				$('#geoSubContainer').append('<p>Número de registros que tuvieron error al intentar ser insertados: ' + data.nGeoInsertsErrors + '</p>');
				$('#geoSubContainer').append('<p>Número de registros que tuvieron error al intentar ser actualizados: ' + data.nGeoUpdatesErrors + '</p>');

				// descGeoInsertsErrors
				// descGeoInsertsErrors
			},
			error: function(obj, errorText) {
				Swal.fire({
					html: '<div style="width: 100%;">' +
						'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
						'No se pudo leer el archivo Excel.' +
						'</div>',
					showCloseButton: false,
					confirmButtonText: 'Aceptar',
					confirmButtonColor: '#006341'
				});
				return false;
			},
			complete: function() {
				hideLoadingSpinner();
			}
		});
	} catch (e) {
		// Se oculta la animación de carga
		hideLoadingSpinner();

		// En caso de que ocurra un error al leer el archivo de Excel se imprime el error
		console.log(e);

		// Se muestra alerta al usuario
		Swal.fire({
			html: '<div style="width: 100%;">' +
				'<div class="titModalSwal" style="width: 100%;"><br><strong>¡Atención!</strong></div>' +
				'No se pudo leer el archivo Excel.' +
				'</div>',
			showCloseButton: false,
			confirmButtonText: 'Aceptar',
			confirmButtonColor: '#006341'
		});
		return false;
	}
}